package isoft.com.util;

import java.io.Serializable;
/*-------Log of Chanegs---------------------------------------------------------

Ver     Programmer       Date        Description
------- ---------------  ----------  -------------------------------------------
1.0.4.1 Husam A. Barham  18-03-2006  - adding is_credit_center member.
------------------------------------------------------------------------------*/
public interface Profile extends Serializable
{
    public final short USER_NAME            = 1;
    public final short USER_ID              = 2;
    public final short USER_GENDER          = 3;
    public final short USER_GENDER_DESC     = 4;
    public final short USER_CENTER_NAME     = 5;
    public final short EMPLOYEE_NAME        = 6;
    public final short USER_GROUP_ID        = 7;
    public final short EMPLOYEE_NUMBER      = 8;
    public final short EMPLOYEE_ID          = 9;
    public final short LANG_CODE            = 10;   
    public final short USER_GROUPS          = 11;   
    public final short EMPLOYEE_NAME_E      = 12;
   
    
    public Object getAttribute(short attCode);
    public void setAttribute(short attCode, Object value);
}