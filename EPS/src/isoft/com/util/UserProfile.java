/*
 * Copyright (c) i-Soft 2004.
 * Ferdous Tower (Takreer Building) , Salam Street 
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * Creation Date: UN-KNOWN
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00      UN-KNOWN        UN-KNOWN   - File created.
 * 
 * 1.01  Husam A. Barham    18/03/2006  - adding is_credit_center member.
 * 
 * 1.02  Eng. Ayman Atiyeh  08/04/2006  - Adding directPrint field.
 *                                      - Updating class comments format
 *                                      
 * 1.03  Khalil Qadous      27/06/2007  - Add new property userLevel and some
 *                                        required implementation
 *
 * 1.04  Eng. Ayman Atiyeh  29/03/2009  - Adding multipleLoginMode field.
 */

package isoft.com.util;


import ae.eis.util.vo.Choice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserProfile implements Profile {
    
    /*
     * Constants and class variables.
     */
     
    /** constant value to represent true */
    public static final Integer TRUE  = new Integer(2);
    
    /*
     * Instance variables.
     */
    private String  m_strUserName;
    private Integer m_intUserId;
    private String  m_strGenderDesc;
    private Integer m_intGender;
    private String  m_strEmployeeName;
    private String  m_strEmployeeName_E;
    private Integer m_intUserGroupId;


    private String  m_intEmployeeNumber;
    private Integer m_intEmployeeId;
    private String  lanCode;



    private ArrayList m_UserGroups   = null;    


        
    
    public  UserProfile()
    {
    }

    public Object getAttribute(short attCode)
    {
        switch( attCode )
        {
            case USER_NAME:
                return getUserName();
            case USER_GENDER_DESC:
                return getGenderDesc();
            case USER_GENDER:
                return getGender();
            case USER_ID:
                return getUserId();
            case EMPLOYEE_NAME:
                return getEmployeeName();
            case USER_GROUP_ID:
                return getUserGroupId();
            case EMPLOYEE_NUMBER:
                return getEmployeeNumber();
            case EMPLOYEE_ID:
                return getEmployeeId();          
            case USER_GROUPS:
                return getUserGroups();                                               
           case LANG_CODE:
                return getLanCode();
           case EMPLOYEE_NAME_E:
                return getEmployeeName_E();
            default:
                throw new RuntimeException("not supported attribute");
        }
    }
    
    
    public void setAttribute(short attCode, Object value)
    {
        switch( attCode )
        {
            case USER_NAME:
                setUserName(value);
                break;
            case USER_GENDER_DESC:
                setGenderDesc(value);
                break;
            case USER_GENDER:
                setGender(value);
                break;
            case USER_ID:
                setUserId(value);
                break;
            case EMPLOYEE_NAME:
                setEmployeeName(value);
                break;
            case USER_GROUP_ID:
                setUserGroupId(value);
                break;
            case EMPLOYEE_NUMBER:
                setEmployeeNumber(value);
                break;
            case EMPLOYEE_ID:
                setEmployeeId(value);
                break;        
            case USER_GROUPS:
                setUserGroups( value );                                               
                break;                                
           case LANG_CODE :
                setLanCode(value.toString());
                break;  
           case EMPLOYEE_NAME_E:
                 setEmployeeName_E(value.toString());
                 break;
           default:
                throw new RuntimeException("not supported attribute");
        }
    }

    /**
     * accessor Methods
     * Getter methods
     */
    public String getUserName()
    {
        return m_strUserName;
    }


    
    
    public Integer getUserId()
    {
        return m_intUserId;
    }

    private Integer getGender()
    {
        return m_intGender;
    }

    private String getGenderDesc()
    {
        return m_strGenderDesc;
    }
    
    private String getEmployeeName()
    {
        return m_strEmployeeName;
    }

    private Integer getUserGroupId()
    {
        return m_intUserGroupId;
    }

    private String getEmployeeNumber()
    {
        return m_intEmployeeNumber;
    }

    private Integer getEmployeeId()
    {
        return m_intEmployeeId;
    }

    private ArrayList getUserGroups()    
    {
        return m_UserGroups;
    }
    
    
    /**
     * accessor Methods
     * Setter methods
     */
    private void setUserName(Object value)
    {
        m_strUserName = (String)value;
    }


    
    
    private void setUserId(Object value)
    {
        m_intUserId = (Integer)value;
    }

    private void setGender(Object value)
    {
        m_intGender = (Integer)value;
    }

    private void setGenderDesc(Object value)
    {
        m_strGenderDesc = (String)value;
    }
    
    private void setEmployeeName(Object value)
    {
        m_strEmployeeName = (String)value;
    }

    private void setUserGroupId(Object value)
    {
        m_intUserGroupId = (Integer)value;
    }
             

    private void setEmployeeNumber(Object value)
    {
        m_intEmployeeNumber = (String)value;
    }

    private void setEmployeeId(Object value)
    {
        m_intEmployeeId = (Integer)value;
    }

    private void setUserGroups(Object value)
    {
        m_UserGroups= (ArrayList) value ;
    }

    /**
     * Set lanCode.
     * 
     * @param lanCode  lan Code.
     */
    public void setLanCode(String lanCode) {
        this.lanCode = lanCode;
    }

    /**
     * Get lanCode.
     * 
     * @return lanCode lan Code.
     */
    public String getLanCode() {
        return lanCode;
    }


    public void setEmployeeName_E(String m_strEmployeeName_E) {
        this.m_strEmployeeName_E = m_strEmployeeName_E;
    }

    public String getEmployeeName_E() {
        return m_strEmployeeName_E;
    }

}
