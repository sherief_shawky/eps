/*
 * Copyright (c) i-Soft 2004.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  15/11/2006  - File created.
 */

package isoft.com.util.dao;

import isoft.com.util.jdbc.JdbcFactory;
import isoft.com.util.common.ServiceLocator;
import ae.eis.util.dao.DataAccessException;

import java.sql.Timestamp;
import java.util.List;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;

import java.io.InputStream;

/**
 * Defines the common functionality for data access objects. All DAO classes 
 * must extend this class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public abstract class DataAccessObject {
    /*
     * Constants and class variables.
     */

    /*
     * Fields.
     */

    /** JDBC connection object. */
    private Connection con;

  

    /*
     * Constructors
     */

    /**
     * Construct new DataAccessObject instance. This method will initialize the
     * connection object.
     */
    public DataAccessObject() {
        try {
            // Get new connection
            this.con = JdbcFactory.getConnection();

            // Set auto-commit mode to false
            con.setAutoCommit(false);

        } catch (SQLException ex) {
            throw new DataAccessException(
                   "Failed to create DataAccessObject", ex);
        }
    }

    /**
     * Construct and initialize new DataAccessObject instance.
     * 
     * @param connection Connection object.
     */
    public DataAccessObject(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException("NULL connection");
        }

        this.con = connection;
    }

     

    /*
     * Methods
     */

    

    /**
     * Get DAO connection.
     * 
     * @return DAO connection object.
     */
    public Connection getConnection() {
        return this.con;
    }
 

    

  

    /**
     * Generate new sequence number.
     * 
     * @param sequenceName Sequence name.
     * @return new sequence number.
     */
    public long generateSequenceNumber(String sequenceName)  {
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ").append(sequenceName).append(".NEXTVAL FROM DUAL");

        ResultSet rs = null;
        PreparedStatement pstm = null;
        try {
            pstm = getConnection().prepareStatement(sql.toString());
            rs = pstm.executeQuery();
            if (! rs.next()) {
                throw new DataAccessException(new StringBuffer(
                  "Failed to generate new sequence: ").append(sequenceName)
                    .toString());
            }

            return rs.getLong(1);

        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(pstm);
        }
    }

    /**
     * Close the JDBC connection. This method used to enhance code clarity
     * and readability.
     */
    public void close() {
        try {            
                getConnection().close();        

        } catch (SQLException ex) {
            throw new DataAccessException("Failed to close connection", ex);
        }
    }

    /**
     * Rollback the transaction and ignore any generated exceptions.
     */
    public void rollback() {
        try {            
                this.con.rollback();
            
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to rollback connection", ex);
        }
    }

    /**
     * Commit transaction.
     */
    public void commit() {
        try {            
                getConnection().commit();         
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to commit connection", ex);
        }
    }
    
   

    /**
     * Set auto commit mode.
     * 
     * @param mode auto commit mode.
     */
    public void setAutoCommit(boolean mode) {
       

        try {
            getConnection().setAutoCommit(mode);
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to setAutoCommit", ex);
        }
    }

    /**
     * Close the JDBC Statement. This method used to enhance code clarity and 
     * readability.
     * 
     * @param stm CallableStatement to be closed.
     */
    protected boolean close(Statement stm) {
        if (stm == null) {
            return false;
        }

        try {stm.close();} 
        catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * Close InputStream object (Used for BLOB's). This method used to enhance 
     * code clarity and readability.
     * 
     * @param in InputStream to be closed.
     */
    protected boolean close(InputStream in) {
        if (in == null) {
            return false;
        }

        try {in.close();} 
        catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * Close the JDBC ResultSet. This method used to enhance code clarity 
     * and readability.
     * 
     * @param rs ResultSet to be closed.
     */
    protected boolean close(ResultSet rs) {
        if (rs == null) {
            return false;
        }

        try {rs.close();} 
        catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * Set long parameter if the value is greater than zero else, set this 
     * value by null.
     * 
     * @param stm PreparedStatement object to be initialized.
     * @param index parameter index.
     * @param value parameter value.
     */
    public void setLong(PreparedStatement stm, int index, long value) 
           throws SQLException {
        if (value > 0) {
            stm.setLong(index, value);
        } else {
            stm.setString(index, null);
        }
    }

    /**
     * Set int parameter if the value is greater than zero else, set this 
     * value by null.
     * 
     * @param stm PreparedStatement object to be initialized.
     * @param index parameter index.
     * @param value parameter value.
     */
    public void setInt(PreparedStatement stm, int index, int value) 
           throws SQLException {
        if (value > 0) {
            stm.setInt(index, value);
        } else {
            stm.setString(index, null);
        }
    }

    
    
    /**
     * Set prepared statement parameter.
     * 
     * @param stm Prepared statement.
     * @param index Parameter index.
     * @param val Parameter value.
     */
    public void setLong(PreparedStatement stm, int index, Long val) throws SQLException {
        if (val == null) {
            stm.setString(index, null);
        } else {
            stm.setLong(index, val.longValue());
        }
    }

    /**
     * Set prepared statement parameter.
     * 
     * @param stm Prepared statement.
     * @param index Parameter index.
     * @param val Parameter value.
     */
    public void setInteger(PreparedStatement stm, int index, Integer val) throws SQLException {
        if (val == null) {
            stm.setString(index, null);
        } else {
            stm.setInt(index, val.intValue());
        }
    }
    
    /**
     * Set prepared statement parameter.
     * 
     * @param stm Prepared statement.
     * @param index Parameter index.
     * @param val Parameter value.
     */
    public void setDouble(PreparedStatement stm, int index, Double val) throws SQLException {
        if (val == null) {
            stm.setString(index, null);
        } else {
            stm.setDouble(index, val.doubleValue());
        }
    }

    /**
     * Set Timestamp object
     * 
     * @param stm Prepared statement.
     * @param index Parameter index.
     * @param date Parameter date.
     * @throws java.sql.SQLException
     */
    public void setTimestamp(PreparedStatement stm, int index,java.util.Date date) throws SQLException {
        if(date == null) {
            stm.setTimestamp(index,null);
        } else {
            stm.setTimestamp(index,new Timestamp(date.getTime()));
        }
    }

    /**
     * Get Long object
     * 
     * @param rs result set
     * @param name column name
     * @return Long object
     * @throws java.sql.SQLException
     */
    public Long getLong(ResultSet rs, String name) throws SQLException{
        String val = rs.getString(name);
        if (val == null) {
            return null;
        } else {
            return new Long(val);
        }
    }      

    /**
     * Get Integer object
     * 
     * @param name column name
     * @param rs result set
     * @return Integer object 
     * @throws java.sql.SQLException
     */    
    public Integer getInteger(ResultSet rs, String name) throws SQLException {
        String val = rs.getString(name);
        if (val == null) {
            return null;
        } else {
            return new Integer(val);
        }
    }    

    /**
     * Get encryption key.
     * 
     * @return encryption key.
     */
    public static String getEncryptionKey() {
        return "1234567891";
    }

    /**
     * This method used to exclude and replace special characters such as 
     * "\n, ', <, >" before saving data to database to avoid SQL injection and
     * cross-site scripting.
     * 
     * @param str String to be formatted.
     * @return formatted String.
     */
    public static String formatString(String str) {
        if (str == null) { 
            return null;
        }

        str = str.trim();
        str = str.replaceAll("\n", " ");
        str = str.replaceAll("\r", " ");

        while (str.indexOf("  ") >= 0) {
            str = str.replaceAll("  ", " ");
        }
        
        return str;
    }

    /**
     * Set prepared statement parameter.
     * 
     * @param stm Prepared statement.
     * @param index Parameter index.
     * @param val String Parameter value.
     */
    public void setString(PreparedStatement stm, int index, String val)
           throws SQLException {
        if (val == null) {
            stm.setString(index, null);
        } else {
            stm.setString(index, formatString(val));
        }
    }

    /**
     * Set PreparedStatement parameters.
     * 
     * @param stm PreparedStatement to be initialized.
     * @param parameters Search query parameters.
     */
    protected void setQueryParameters(PreparedStatement stm, List parameters) 
             throws SQLException {
        // Validate query parameters
        if (parameters == null || parameters.size() == 0) {
            return;
        }

        for (int i = 0; i < parameters.size(); i++) {
            Object param = parameters.get(i);
            int index = (i + 1);

            if (param instanceof java.sql.Date) {
                stm.setDate(index, (java.sql.Date) param);

            } else if (param instanceof java.util.Date) {
                java.util.Date date = (java.util.Date) param;
                stm.setDate(index, new java.sql.Date(date.getTime()));

            } else if (param instanceof Integer) {
                stm.setInt(index, ((Integer) param).intValue());

            } else if (param instanceof Long) {
            
                stm.setLong(index, ((Long) param).longValue());
            } else if (param instanceof Float) {
            
                stm.setFloat(index,((Float) param).floatValue());
            }else if (param instanceof Double) {
            
                stm.setDouble(index,((Double) param).doubleValue());
            }else if(param!=null) {
            
                setString(stm,index, param.toString());
            }else {
                stm.setString(index, null);
            }
        }
    }

      
}