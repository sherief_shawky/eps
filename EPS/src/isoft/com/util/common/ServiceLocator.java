/*
 * Copyright (c) i-Soft 2006.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  19/09/2006  - File created.
 */

package isoft.com.util.common;


import ae.eis.util.common.GlobalUtilities;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;

/**
 * Centralize distributed service objects lookups and provides a centralized
 * point of control for locating services and resources.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ServiceLocator {
    /*
     * Fields.
     */

    /** Initial context reference. */
    private InitialContext context;

    /** Cashed resources map. */
    private ConcurrentMap<String, Object> cache;

    /** Singleton object. */
    private static ServiceLocator singleton;

    /** Logger object. */
    private static final Logger logger = Logger.getLogger(ServiceLocator.class.getName());

    
    /*
     * Constructors
     */

    /**
     * Construct and initialize ServiceLocator singleton object.
     */
    private ServiceLocator() throws ServiceLocatorException {
        try {
        // Create initial context
        context = new InitialContext();            
        cache = new ConcurrentHashMap<String,Object>();
        
      } catch (NamingException ex) {
            logger.log(Level.SEVERE, "Failed to InitialContext", ex);
            throw new ServiceLocatorException(ex);
       }
    }

    /*
     * Methods
     */

    /**
     * This methods will throw CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("ServiceLocator cannot be cloned");
    }

    /**
     * Get ServiceLocator reference.
     * 
     * @return ServiceLocator reference.
     */
    public synchronized static ServiceLocator getInstance() 
                        throws ServiceLocatorException {
        if (singleton == null) {
            singleton = new ServiceLocator();
        }

        return singleton;
    }

 

    /**
     * Get DataSource reference.
     * 
     * @return DataSource
     */
    public DataSource getDataSource() {
        if (cache.containsKey(JNDINames.DATA_SOURCE)) {
            return (DataSource) cache.get(JNDINames.DATA_SOURCE);
        }

        try {
            logger.info("Lookup DataSource");
            String jndiName = "jdbc/Dubai_DM";
            DataSource ds = (DataSource) context.lookup(jndiName);

            cache.put(JNDINames.DATA_SOURCE, ds);
            return ds;

        } catch (NamingException ex) {
            String err = "Failed to lookup DataSource";
            logger.log(Level.SEVERE, err, ex);
            throw new ServiceLocatorException(err, ex);
        }
    }

    /**
     * Get DataSource reference.
     * 
     * @return DataSource
     */
    public DataSource getDataSourceXA() {
        if (cache.containsKey(JNDINames.DATA_SOURCE_XA)) {
            return (DataSource) cache.get(JNDINames.DATA_SOURCE_XA);
        }

        try {
            logger.info("Lookup XA DataSource");
            String jndiName = "jdbc/Dubai_DM_XADS";
            DataSource ds = (DataSource) context.lookup(jndiName);

            cache.put(JNDINames.DATA_SOURCE_XA, ds);
            return ds;

        } catch (NamingException ex) {
            String err = "Failed to lookup XA DataSource";
            logger.log(Level.SEVERE, err, ex);
            throw new ServiceLocatorException(err, ex);
        }
    }

    
    /**
     * Lookup EJB class using JNDI lookup. The naming convention assumed for EJB JNDI name is the same class qualified
     * name with '-' insted of '.' package seperator. For example, class 'ae.rta.cis.sys.bus.SystemFacade' EJB will 
     * have the following JNDI name: 'ae-rta-cis-sys-bus-SystemFacade'.
     * 
     * @param localRef EJB local reference class.
     * @return EJB local reference.
     */
    private Object lookupEJB(Class localRef) {
        try {
            // Lookup EJB
            return context.lookup("java:comp/env/" + GlobalUtilities.getEjbJndiName(localRef));

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "[FATAL] Failed to lookup "+ localRef.getName(),ex);
            throw new ServiceLocatorException("Failed to lookup "+ localRef.getName(), ex);
        }
    }
    /**
     * Get hibernate session factory.
     * 
     * @return hibernate session factory.
     */
    private SessionFactory loadHibernateSessionFactory() {
        return new org.hibernate.cfg.Configuration()
                                    .configure()
                                    .buildSessionFactory();
    }
    /**
     * Get hibernate session factory.
     * 
     * @return hibernate session factory.
     */
    public SessionFactory getHibernateSessionFactory() {
        if (cache.containsKey(JNDINames.HIBERNATE_SESSION_FACTORY) && cache.get(JNDINames.HIBERNATE_SESSION_FACTORY) != null) {
            return (SessionFactory) cache.get(JNDINames.HIBERNATE_SESSION_FACTORY);
        }

        try {
            logger.info("Load hibernate SessionFactory...");
            SessionFactory factory = loadHibernateSessionFactory();
            
            cache.put(JNDINames.HIBERNATE_SESSION_FACTORY, factory);
            return factory;

        } catch (Exception ex) {
            String err = "Failed to load hibernate SessionFactory";
            logger.log(Level.SEVERE, err, ex);
            throw new ServiceLocatorException(err, ex);
        } 
    }
    



}