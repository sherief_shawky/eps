/*
 * Copyright (c) i-Soft 2006.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  07/12/2006  - File created.
 */

package isoft.com.util.common;


/**
 * This class is the central location to store the internal JNDI names.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface JNDINames {
    


    /** DataSource JNDI name. */
    String DATA_SOURCE = "ae.gov.dphq.traffic.jdbc.dataSource";

    /** DataSource XA JNDI name. */
    String DATA_SOURCE_XA = "ae.gov.dphq.traffic.jdbc.dataSourceXA";

    /** Data Source XA JNDI name. */
    String JNDI_DATA_SOURCE_XA = "jdbc/Dubai_Traffic_XADS";

    /** Hibernate SessionFactory JNDI name. */
    String HIBERNATE_SESSION_FACTORY = "ae.gov.dphq.eps.jndi.hibernate.SessionFactory";

    /**

    
    
    /*
     * WS JNDI to unifi JNDI Names
     */
    String JDBC_DATA_SOURCE = "ae.gov.trf.jndi.jdbc.dataSource";
    
   
}