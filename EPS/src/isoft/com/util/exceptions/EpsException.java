/*
 * Copyright (c) i-Soft 2004.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * 
 * Creation Date: 10/01/2005
 * 
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/01/2005  - File created.
 */

package isoft.com.util.exceptions;


/**
 * Used as a general exception for traffic application and used to
 * hide/encapsulate application exceptions generated in different tiers and
 * application layers.
 *
 * @version 1.00 - 01/01/2005
 * @author Eng. Ayman Atiyeh
 */
public class EpsException extends NestingException {
    /*
     * Constructors
     */

    /**
     * Creates new TrafficException object
     */
    public EpsException() {
        super();
    }

    /**
     * Creates new TrafficException object with custom error message.
     */
    public EpsException(String msg) {
        super(msg);
    }

    /**
     * Creates new TrafficException object and initializes its root cause to the
     * parameter value.
     */
    public EpsException(Throwable t) {
        super(t);
    }

    /**
     * Creates a new TrafficException object and initializes its message and root
     * cause to the parameter values.
     */
    public EpsException(String msg, Throwable t) {
        super(msg, t);
    }
}