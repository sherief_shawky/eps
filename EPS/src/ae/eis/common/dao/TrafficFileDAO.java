/*
 * Copyright (c) i-Soft 2007.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00 Bassem R. Zohdy     05/02/2008  - Add isBlackListed(Long trfID).
 * 
 * 1.01 Hamzeh Abu Lawi     27/02/2008  -Add getPersonTrafficFile 
 * 
 * 1.02 Waseem N.Zawaideh   22/03/2008  -Add generateTrafficNoForOrg
 * 
 * 1.03 Rami Nassar         18/07/2012  - Add method getByClassicPlateNo.
 * 
 * 1.04 Sami Abudayeh       28/09/2012  - Adding getOwnerInfoByTrfNo(Long trafficFileNumber) 
 *                                      - Adding getOwnerInfoLicenseInfo(Long licenseNumber)
 *                                      - Adding getOwnerInfoByTicketNo(Long ticketNo)
 *                                      - Adding getOwnerInfoByPlateInfo(ae.rta.vhl.vo.PlateVO plateVO)
 *                                      
 * 1.05 Mahmoud Atiyeh      20/04/2013  - Added isRelatedToTransaction(Long trafficNo, Long transactionId)
 */

package ae.eis.common.dao;

import ae.eis.common.vo.TrafficFileVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile;

import java.util.HashMap;
import java.util.List;

/**
 * Traffic File Data Access Object
 *
 * @auther Bassem R. Zohdy
 * @vesion 1.04
 */
public interface TrafficFileDAO extends DataAccessObject   {


    /**
     * Get Traffic File By Id
     * 
     * @param id : Traffic file Id
     * @return Traffic file Value Object
     */
    TrafficFileVO getTrfFileById(Long id);
    

        
    /**
    * Get traffic file value object using traffic file number for database searching.
    * @since 1.0
    * @return traffic file value object.
    * @param trafficFileVO
    */
    TrafficFileVO getTrfFileByTrfNo(TrafficFileVO  trafficFileVO);
        
        
  

}
