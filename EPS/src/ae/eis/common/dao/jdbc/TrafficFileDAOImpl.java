/*
 * Copyright (c) i-Soft 2008.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00 Bassem R. Zohdy     05/02/2008  - Add isBlackListed(Long trfID).
 * 
 * 1.01 Hamzeh Abu Lawi     27/02/2008  - Add getPersonTrafficFile 
 * 
 * 1.02 Hamzeh Abu Lawi     03/03/2008  - update getTrafficFileById(Long trfId)
                                          Dont't fetch traffic file details
 *                                        when the traffic file 
 *                                        is equal to 19999999
 *                                       
 * 1.03 Waseem N.Zawaideh   22/03/2008  - Add generateTrafficNoForOrg
 * 
 * 1.04 Eng. Ayman Atiyeh   25/03/2008  - Updating queries modifieres from
 *                                        public to private
 *                                      - Delete FIND_ORG_BY_NAME (not used)
 *                                      
 * 1.05 Rami Nassar         18/07/2012  - Add method getByClassicPlateNo.
 * 
 * 1.06 Sami Abudayeh       28/09/2012  - Adding getOwnerInfoByTrfNo(Long trafficFileNumber) 
 *                                      - Adding getOwnerInfoLicenseInfo(Long licenseNumber)
 *                                      - Adding getOwnerInfoByTicketNo(Long ticketNo)
 *                                      - Adding getOwnerInfoByPlateInfo(ae.rta.vhl.vo.PlateVO plateVO)
 *                                      - Adding getOwnerInfoByCircularTicketInfo(FineVO fine)
 *                                      
 * 1.07 Mahmoud Atiyeh      20/04/2013  - Added isRelatedToTransaction(Long trafficNo, Long transactionId)
 */
package ae.eis.common.dao.jdbc;

import isoft.com.util.jdbc.JdbcAdapter;
import isoft.com.util.jdbc.JdbcFactory;
import isoft.com.util.common.ServiceLocator;

 
import ae.eis.common.dao.TrafficFileDAO;
import ae.eis.common.vo.TrafficFileVO;
import ae.eis.util.bus.PreferencesHandler;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sun.misc.BASE64Encoder;

/**
 * Traffic File Data Access Object Implementaion
 *
 * @auther Bassem R. Zohdy
 * @version 1.06
 */
public class TrafficFileDAOImpl extends JdbcDataAccessObject implements TrafficFileDAO {


    /**
     * Get traffic file info by ID.
     * 
     * @param id Traffic file ID.
     * @return traffic file info.
     */
    public TrafficFileVO getTrfFileById(Long id){
    
        NamedQuery query = getNamedQuery("ae.rta.common.dao.jdbc.TrafficFile.getTrfFileById");
        
        query.setParameter("trafficFileId", id);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;
        
        try {
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();

            if (! resultSet.next()) {
                return null;
            }

            TrafficFileVO trfVO = new TrafficFileVO();
            
            trfVO.setId(new Long(resultSet.getLong("ID")));
            trfVO.setTrafficNo(new Long(resultSet.getLong("TRAFFIC_NO")));
            trfVO.setOwnerNameAr(getString(resultSet, "NAME_A")); 
            trfVO.setOwnerNameEn(getString(resultSet, "NAME_E")); 
            trfVO.setPreferedShipmentEmail(getString(resultSet, "SHIP_PREFERRED_E_MAIL"));
            trfVO.setPreferedShipmentMobile(getString(resultSet, "SHIP_PREFERRED_MOBILE"));
            trfVO.setPreferedShipmentPhone(getString(resultSet, "SHIP_PREFERRED_PHONE"));
            trfVO.setPreferedFirstAddress(getString(resultSet, "SHIP_PREFERRED_ADDRESS1"));
            trfVO.setPreferedSecondAddress(getString(resultSet, "SHIP_PREFERRED_ADDRESS2"));
            trfVO.setPreferedEmirate(getString(resultSet, "SHIP_PREFERRED_EMI_CODE"));
            trfVO.setPreferedFedexLocation(getString(resultSet, "SHIP_PREFERRED_FDL_ID"));
            trfVO.setPreferedShipmentName(getString(resultSet, "SHIP_PREFERRED_NAME"));
            trfVO.setPreferedShipmentNameEn(getString(resultSet, "SHIP_PREFERRED_NAME_EN"));
            trfVO.setTrafficFileType(getInteger(resultSet,"TRF_TYPE"));
            //trfVO.setOpenFileReason(getInteger(resultSet,"OPEN_FILE_REASON"));
            trfVO.setUtsTrafficFile(getLong(resultSet,"UTS_TRAFFIC_NO"));
            trfVO.setSendEMail(getChoice(resultSet, "SEND_E_MAIL"));
            trfVO.setSendSms(getChoice(resultSet, "SEND_SMS"));
            trfVO.setRequiredENOC(getChoice(resultSet, "REQUIRED_ENOC"));
            trfVO.setPreferredLanCode(getString(resultSet,"PREFERRED_LAN_CODE"));  
            trfVO.setFeeCollectionStatus(resultSet.getInt("FEE_COLLECTION_STATUS"));
            trfVO.setTrafficFileTrsId(getLong(resultSet, "TRAFFIC_FILE_TRS_ID"));
            trfVO.setCreationDate(getDate(resultSet,"CREATION_DATE"));
            
             
            Long orgId = getLong(resultSet, "ORG_ID");
            
            
            trfVO.setUserLanguage(getString(resultSet,"USER_LANGUAGE"));
            
            return trfVO;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }
    }
    
       
        
        
    /**
    * Get traffic file value object using traffic file number for database searching.
    * @since 1.0
    * @return traffic file value object.
    * @param trafficFileVO
    */
    public TrafficFileVO getTrfFileByTrfNo(TrafficFileVO  trafficFileVO){
    
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;
        NamedQuery query = getNamedQuery("ae.rta.common.dao.jdbc.TrafficFile.getTrfFileByTrfNo");
        //StringBuffer sqlQuery   = new StringBuffer(FIND_TRAFFFIC_FILE_BY_TRF_NO);
        
        query.setParameter("trafficNo", trafficFileVO.getTrafficNo());
        
        try{
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
           
            TrafficFileVO  tempTrafficFileVO=null;
            while(resultSet.next()){ 
                tempTrafficFileVO=new TrafficFileVO();
                
                tempTrafficFileVO.setId(GlobalUtilities.getLong(resultSet.getLong("ID")));
                 
                tempTrafficFileVO.setTrafficNo(GlobalUtilities.getLong(resultSet.getLong("TRAFFIC_NO")));
                tempTrafficFileVO.setTrafficOwnerEn(new String(resultSet.getString("OWNER_NAME_E")));
                tempTrafficFileVO.setTrafficOwnerAr(new String(resultSet.getString("OWNER_NAME_A")));
                tempTrafficFileVO.setEmiCode(getString(resultSet,"EMI_CODE"));  
                tempTrafficFileVO.setPreferredLanCode(getString(resultSet,"PREFERRED_LAN_CODE"));  
 
                tempTrafficFileVO.setTrafficFileTrsId(getLong(resultSet,"TRAFFIC_FILE_TRS_ID"));
                
            }
            return tempTrafficFileVO;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
            
        }
    }
    
  
}
