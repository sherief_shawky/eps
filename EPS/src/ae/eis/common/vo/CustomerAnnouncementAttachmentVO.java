/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng.Mohammed Kabeel  08/06/2013  - File created.
 */

package ae.eis.common.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Customer Announcement Attachment value object.
 *
 * @author Eng.Mohammed Kabeel
 * @version 1.00
 */
public class CustomerAnnouncementAttachmentVO extends ValueObject {
    
    /*
     * Instance variables
     */
    
    /** Customer Announcement VO */
    private CustomerAnnouncementVO customerAnnouncementVO;
    
    /** Attachment Name  */
    private String attachmentName;
    
    /** Attachment File */
    private byte[] attachment;
    
    /** File Type */
    private String fileType;
    
    /*
     * Constructors
     */

    /**
     * Default constructor.
     */
    public CustomerAnnouncementAttachmentVO() {
        // Empty body
    }

    /**
     * Setter method for customerAnnouncementVO : Customer Announcement VO
     * 
     * @param customerAnnouncementVO : Customer Announcement VO
     */
    public void setCustomerAnnouncementVO(CustomerAnnouncementVO customerAnnouncementVO) {
        this.customerAnnouncementVO = customerAnnouncementVO;
    }

    /**
     * Getter method for customerAnnouncementVO : Customer Announcement VO
     * 
     * @return customerAnnouncementVO : Customer Announcement VO
     */
    public CustomerAnnouncementVO getCustomerAnnouncementVO() {
        return customerAnnouncementVO;
    }

    /**
     * Setter method for attachmentName : Attachment Name
     * 
     * @param attachmentName : Attachment Name
     */
    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    /**
     * Getter method for attachmentName : Attachment Name
     * 
     * @return attachmentName : Attachment Name
     */
    public String getAttachmentName() {
        return attachmentName;
    }

    /**
     * Setter method for attachment : Attachment File
     * 
     * @param attachment : Attachment File
     */
    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    /**
     * Getter method for attachment : Attachment File
     * 
     * @return attachment : Attachment File
     */
    public byte[] getAttachment() {
        return attachment;
    }

    /**
     * Setter method for fileType : File Type
     * 
     * @param fileType : File Type
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * Getter method for fileType : File Type
     * 
     * @return fileType : File Type
     */
    public String getFileType() {
        return fileType;
    }
}