/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- ------------------  ----------  ----------------------------------------
 * 1.00   Moh'd Abdul Jawad  25/04/2013  - File created.
 */
 
package ae.eis.common.vo;

import java.io.Serializable;

/**
 * Column MetaData Info.
 *
 * @author Moh'd Abdul Jawad
 * @version 1.00
 */
public class ColumnMetaInfo implements Serializable {
    /*
     * Constanst
     */
    /** Object String */
    public static final String OBJECT_STRING = "1";

    /** Object Long */
    public static final String OBJECT_LONG = "2";
    
    /** Object Integer */
    public static final String OBJECT_INTEGER = "3";
    
    /** Object Date */
    public static final String OBJECT_DATE = "4";
  
    /** Object Choice */
    public static final String OBJECT_CHOICE = "5";
    
    
    /** Name */
    private String name;
    
    /** Data Type */
    private int dataType;
    
    /** Size */
    private int size;
    
    /**
     * Column Meta Info.
     * 
     * @param size : Size.
     * @param dataType : Data Type
     * @param name : Name.
     */
    public ColumnMetaInfo(String name, int dataType, int size) {
        super();
        this.name = name;
        this.dataType = dataType;
        this.size = size;
    }
    
    /**
     * Setter For Name.
     * 
     * @param name : Name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Getter For Name.
     * 
     * @return name : Name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Setter For Size.
     * 
     * @param size : Size.
     */
    public void setSize(int size) {
        this.size = size;
    }
    
    /**
     * Getter For Size
     * 
     * @return Size : Size.
     */
    public int getSize() {
        return size;
    }
    
    /**
     * Is Varchar.
     * 
     * @return dataType : Data Type.
     */
    public boolean isVarchar() {
        return dataType == 12;
    }
    
    /**
     * Is Numeric.
     * 
     * @return dataType : Data Type.
     */
    public boolean isNumeric() {
        return dataType == 3;
    }
    
    /**
     * Is Date or Date/Time.
     * 
     * @return dataType : Data Type.
     */
    public boolean isDate() {
        return dataType == 93 || dataType == 91;
    }
    
    /**
     * Is Blob.
     * 
     * @return dataType : Data Type.
     */
    public boolean isBlob() {
        return dataType == 2004;
    }
    
    /**
     * Setter For Data Type.
     * 
     * @param dataType : Data Type.
     */
    public void setDataType(int dataType) {
        this.dataType = dataType;
    }
    
    /**
     * Getter For Data Type.
     * 
     * @return dataType : Data Type.
     */
    public int getDataType() {
        return dataType;
    }
}