/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Khalil Qadous      19/11/2008  - First version
 * 
 * 1.01  Eng. Ayman Atiyeh   23/03/2009  - Adding user fields, version 1.00 
 *                                         defines only name field
 *                                         
 * 1.10  Alaa Salem         28/10/2009  - Adding statusDesc field & accessors.
 *                                      - Adding password field & accessors.
 *                                      - Adding isStatusNotActive() method.
 *                                      - Adding isStatusActive() method.
 *                                      - Adding isStatusLocked() method.
 *                                      - Adding USER_LEVEL_DP_USER constant.
 *                                      - Adding USER_LEVEL_RTA_PARTNAR_USER
 *                                        constant.
 *                                      - Adding isUserLevelRTAUser() method.
 *                                      - Adding isUserLevelTrafficAdmin()
 *                                        method.
 *                                      - Adding isUserLevelRTAAdmin()
 *                                        method.
 *                                      - Adding isUserLevelDubaiPoilceAdmin()
 *                                        method.
 *                                      - Adding isUserLevelRemoteSystem()
 *                                        method.
 *                                      - Adding isUserLevelSystemService()
 *                                        method.
 *                                      - Adding isUserLevelDubaiPoliceUser()
 *                                        method.
 *                                      - Adding isUserLevelRTAPartnarUser()
 *                                        method.
 *                                      - Rename USER_LEVEL_NORMAL_USER To
 *                                        USER_LEVEL_RTA_USER.
 *                                         
 */

package ae.eis.common.vo;

import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;
import java.util.Date;
import java.util.Set;

/**
 * User Value Object
 *
 * @version 1.00
 * @auther Khalil Qadous
 */

public class UserVO extends ValueObject  {

    /*
     * Class Varaibles
     */
    /** Sub Status Values */
    public static final Integer SUB_STATUS_OTP=Integer.valueOf("1");
    public static final Integer SUB_STATUS_RESET_BY_BACKEND=Integer.valueOf("2");
    /** Column ( SF_INF_USERS ) */
    public static final String NAME = "NAME";

    /** Table Name */
    public static final String TABLE_NAME = "SF_INF_USERS";
    
    /** Status Values */
    public static final int STATUS_NOT_ACTIVE = 1;
    public static final int STATUS_ACTIVE = 2;
    public static final int STATUS_LOCKED = 3;
    
    /** User Level Values */
    public static final int USER_LEVEL_RTA_USER = 0;
    public static final int USER_LEVEL_TRF_ADMIN = 1;
    public static final int USER_LEVEL_RTA_ADMIN = 2;
    public static final int USER_LEVEL_DP_ADMIN = 3;
    public static final int USER_LEVEL_REMOTE_USER = 4;
    public static final int USER_LEVEL_SYSTEM_USER = 5;
    public static final int USER_LEVEL_DP_USER = 6;
    public static final int USER_LEVEL_RTA_PARTNAR_USER = 7;


    /** Traffic User Name */
    public static final String TRAFFIC_USER_NAME = "TRAFFIC_USER";
    
    public static final Long TRAFFIC_USER_ID = 15920L;
    
    /** Portal User Name */
    public static final String PORTAL_USER_NAME = "PORTAL_USER";

    /** Portal User Id */
    public static final Long PORTAL_USER_ID = 14800L;
   
    /*
     * Instance Variables
     */
    
    /** User Name */
    private String name;
    
    /** User Password. */
    private String password;
    
    /** User status. */
    private Integer status;
    
    /** Old User status. */
    private Integer oldStatus;
    
    /** User status description. */
    private String statusDesc;
    
    /** Status last update date. */
    private Date statusDate;
    
    /** User machine IP address. */
    private String ipAddress;
    
    /** Last application access date. */
    private Date lasAccessDate;
    
    /** Account remarks. */
    private String remarks;
    
    /** User level. */
    private Integer userLevel;
    
    /** User machine host name. */
    private String domainUserHost;
    
    /** First user login flag. */
    private Integer isFirstLogin;
    
    /** User login flag. */
    private Integer isLogedIn;

    /** User login description. */
    private String isLogedInDesc;
    
    /** User failed login count. */
    private Integer failedTrialsCount;
    
    /** Password expired flag. */
    private Integer isPasswordExpired;
    
    /** Last active user session ID. */
    private String sessionId;
    
    /** Employee Value Object */
    private EmployeeVO employee;
    /** sub Status **/
    private Integer subStatus;
    
    /** Active user announcements count. */
    private Integer activeAnnouncementsCount;
    private Integer expiredAnnouncementsCount;
    
    /** WS Log Enabled */
    private Choice wsLogEnabled;
    
    /** WS Log Enabled Desc*/
    private String wsLogEnabledDesc;

    /** External User Mapping*/
    private String externalUserMapping;

    /** Session Time Out */
    private Long sessionTimeOut;

    /** Max Wrong Trails */
    private Long maxLoginTrails;

    /** Password Age */
    private Long passwordAge;
    
    /*
     * Methods
     */

    /**
     * Set User name
     * 
     * @param name User name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get User name
     * 
     * @return User name
     */
    public String getName() {
        return name;
    }

    /**
     * Set User status.
     * 
     * @param status User status.
     */
    public void setStatus(Integer status) {
        if(status == null) {
            throw new IllegalArgumentException("Status cannot be null");
        }
        switch (status.intValue())  {
            case STATUS_NOT_ACTIVE:
            case STATUS_ACTIVE:
            case STATUS_LOCKED:
                this.status = status;    
                break;        
            default: 
                throw new IllegalArgumentException("Invalid Status value, status="+status);
        }
    }

    /**
     * Check if user status is `not active`.
     * 
     * @return true if user status is `not active`.
     */
    public boolean isStatusNotActive() {
        return status != null && status.intValue() == STATUS_NOT_ACTIVE;
    }

    /**
     * Check if user status is `active`.
     * 
     * @return true if user status is `active`.
     */
    public boolean isStatusActive() {
        return status != null && status.intValue() == STATUS_ACTIVE;
    }

    /**
     * Check if user status is `locked`.
     * 
     * @return true if user status is `locked`.
     */
    public boolean isStatusLocked() {
        return status != null && status.intValue() == STATUS_LOCKED;
    }
    
    /**
     * Check if user status is `not active`.
     * 
     * @return true if user status is `not active`.
     */
    public boolean isWSLogEnabled() {
        return wsLogEnabled != null && wsLogEnabled.getBoolean();
    }

    /**
     * Get User status.
     * 
     * @return User status.
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Set Old User status.
     * 
     * @param oldStatus User status.
     */
    public void setOldStatus(Integer oldStatus) {
        if(oldStatus == null) {
            throw new IllegalArgumentException("Status cannot be null");
        }
        switch (status.intValue())  {
            case STATUS_NOT_ACTIVE:
            case STATUS_ACTIVE:
            case STATUS_LOCKED:
                this.oldStatus = oldStatus;    
                break;        
            default: 
                throw new IllegalArgumentException("Invalid Status value, status="+status);
        }
    }

    /**
     * Check if old user status is `not active`.
     * 
     * @return true if old user status is `not active`.
     */
    public boolean isOldStatusNotActive() {
        return oldStatus != null && oldStatus.intValue() == STATUS_NOT_ACTIVE;
    }

    /**
     * Check if old user status is `active`.
     * 
     * @return true if old user status is `active`.
     */
    public boolean isOldStatusActive() {
        return oldStatus != null && oldStatus.intValue() == STATUS_ACTIVE;
    }

    /**
     * Check if old user status is `locked`.
     * 
     * @return true if old user status is `locked`.
     */
    public boolean isOldStatusLocked() {
        return oldStatus != null && oldStatus.intValue() == STATUS_LOCKED;
    }

    /**
     * Get Old User status.
     * 
     * @return Old User status.
     */
    public Integer getOldStatus() {
        return oldStatus;
    }

    /**
     * Set User status description.
     * 
     * @param statusDesc User status description.
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * Get User status description.
     * 
     * @return User status description.
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * Set Status last update date.
     * 
     * @param statusDate Status last update date.
     */
    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * Get Status last update date.
     * 
     * @return Status last update date.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * Set User machine IP address.
     * 
     * @param ipAddress User machine IP address.
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Get User machine IP address.
     * 
     * @return User machine IP address.
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Set Last application access date.
     * 
     * @param lasAccessDate Last application access date.
     */
    public void setLasAccessDate(Date lasAccessDate) {
        this.lasAccessDate = lasAccessDate;
    }

    /**
     * Get Last application access date.
     * 
     * @return Last application access date.
     */
    public Date getLasAccessDate() {
        return lasAccessDate;
    }

    /**
     * Set Account remarks.
     * 
     * @param remarks Account remarks.
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * Get Account remarks.
     * 
     * @return Account remarks.
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Set User level.
     * 
     * @param userLevel User level.
     */
    public void setUserLevel(Integer userLevel) {
        if(userLevel == null) {
            throw new IllegalArgumentException("User level cannot be null");
        }
        
//        switch (userLevel.intValue())  {
//            case USER_LEVEL_RTA_USER:
//            case USER_LEVEL_TRF_ADMIN:
//            case USER_LEVEL_RTA_ADMIN:
//            case USER_LEVEL_DP_ADMIN:
//            case USER_LEVEL_REMOTE_USER:
//            case USER_LEVEL_SYSTEM_USER:
//            case USER_LEVEL_DP_USER:                // Alaa Salem 13-12-2009
//            case USER_LEVEL_RTA_PARTNAR_USER:       // Alaa Salem 13-12-2009
//                this.userLevel = userLevel;
//                break;
//        
//            default: 
//                throw new IllegalArgumentException("Invalid user level value, userLevel:"+userLevel);
//        }

        this.userLevel = userLevel;
        
        
    }

    /**
     * Check if user level is an RTA user.
     * 
     * @return true if user level is an RTA user, otherwise false.
     */
    public boolean isUserLevelRTAUser() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_RTA_USER);
    }

    /**
     * Check if user level is a traffic admin.
     * 
     * @return true if user level is a traffic admin, otherwise false.
     */
    public boolean isUserLevelTrafficAdmin() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_TRF_ADMIN);
    }

    /**
     * Check if user level is an RTA admin.
     * 
     * @return true if user level is an RTA admin, otherwise false.
     */
    public boolean isUserLevelRTAAdmin() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_RTA_ADMIN);
    }

    /**
     * Check if user level is an dubai police admin.
     * 
     * @return true if user level is an Dubai Police admin, otherwise false.
     */
    public boolean isUserLevelDubaiPoilceAdmin() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_DP_ADMIN);
    }

    /**
     * Check if user level represents a remote system.
     * 
     * @return true if user level represents a remote system, otherwise false.
     */
    public boolean isUserLevelRemoteSystem() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_REMOTE_USER);
    }

    /**
     * Check if user level represents a system service.
     * 
     * @return true if user level represents a system service, otherwise false.
     */
    public boolean isUserLevelSystemService() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_SYSTEM_USER);
    }

    /**
     * Check if user level is an dubai police user.
     * 
     * @return true if user level is an dubai police user, otherwise false.
     */
    public boolean isUserLevelDubaiPoliceUser() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_DP_USER);
    }

    /**
     * Check if user level is an RTA partnar user.
     * 
     * @return true if user level is an RTA partnar user, otherwise false.
     */
    public boolean isUserLevelRTAPartnarUser() {
        Integer userLevel = getUserLevel();
        return (userLevel != null && userLevel.intValue() == USER_LEVEL_RTA_PARTNAR_USER);
    }

    /**
     * Get User level.
     * 
     * @return User level.
     */
    public Integer getUserLevel() {
        return userLevel;
    }

    /**
     * Set User machine host name.
     * 
     * @param domainUserHost User machine host name.
     */
    public void setDomainUserHost(String domainUserHost) {
        this.domainUserHost = domainUserHost;
    }

    /**
     * Get User machine host name.
     * 
     * @return User machine host name.
     */
    public String getDomainUserHost() {
        return domainUserHost;
    }

    /**
     * Set First user login flag.
     * 
     * @param isFirstLogin First user login flag.
     */
    public void setIsFirstLogin(Integer isFirstLogin) {
        this.isFirstLogin = isFirstLogin;
    }

    /**
     * Get First user login flag.
     * 
     * @return First user login flag.
     */
    public Integer getIsFirstLogin() {
        return isFirstLogin;
    }

    /**
     * Get First user login flag.
     * 
     * @return First user login flag.
     */
    public boolean isFirstLogin() {
        return isFirstLogin != null && isFirstLogin.equals(TRUE);
    }

    /**
     * Set User login flag.
     * 
     * @param isLogedIn User login flag.
     */
    public void setIsLogedIn(Integer isLogedIn) {
        this.isLogedIn = isLogedIn;
    }

    /**
     * Get User login flag.
     * 
     * @return User login flag.
     */
    public Integer getIsLogedIn() {
        return isLogedIn;
    }

    /**
     * Get User login flag.
     * 
     * @return User login flag.
     */
    public boolean isLogedIn() {
        return isLogedIn != null && isLogedIn.equals(TRUE);
    }

    /**
     * Set User failed login count.
     * 
     * @param failedTrialsCount User failed login count.
     */
    public void setFailedTrialsCount(Integer failedTrialsCount) {
        this.failedTrialsCount = failedTrialsCount;
    }


    public Integer getFailedTrialsCount() {
        return failedTrialsCount;
    }

    /**
     * Set Password expired flag.
     * 
     * @param isPasswordExpired Password expired flag.
     */
    public void setIsPasswordExpired(Integer isPasswordExpired) {
        this.isPasswordExpired = isPasswordExpired;
    }

    /**
     * Get Password expired flag.
     * 
     * @return Password expired flag.
     */
    public Integer getIsPasswordExpired() {
        return isPasswordExpired;
    }
    
    /**
     * Get Password expired flag.
     * 
     * @return Password expired flag.
     */
    public boolean isPasswordExpired() {
        return isPasswordExpired != null && isPasswordExpired.equals(TRUE);
    }

    /**
     * Set Last active user session ID.
     * 
     * @param sessionId Last active user session ID.
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Get Last active user session ID.
     * 
     * @return Last active user session ID.
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * set Employee value object
     * 
     * @param employee
     */
    public void setEmployee(EmployeeVO employee) {
        this.employee = employee;
    }

    /**
     * get Employee value object
     * 
     * @return employee
     */
    public EmployeeVO getEmployee() {
        return employee;
    }

    /**
     * set user Loged In Description
     * 
     * @param isLogedInDesc
     */
    public void setIsLogedInDesc(String isLogedInDesc) {
        this.isLogedInDesc = isLogedInDesc;
    }

    /**
     * get user Loged In Description
     * 
     * @return isLogedInDesc
     */
    public String getIsLogedInDesc() {
        return isLogedInDesc;
    }

    /**
     * Set Active user announcements count.
     * 
     * @param count Active user announcements count.
     */
    public void setActiveAnnouncementsCount(Integer count) {
        this.activeAnnouncementsCount = count;
    }

    /**
     * Get Active user announcements count.
     * 
     * @return Active user announcements count.
     */
    public Integer getActiveAnnouncementsCount() {
        return activeAnnouncementsCount;
    }

    /**
     * Set Expired user announcements count.
     * 
     * @param count Expired user announcements count.
     */
    public void setExpiredAnnouncementsCount(Integer count) {
        this.expiredAnnouncementsCount = count;
    }

    /**
     * Get Expired user announcements count.
     * 
     * @return Expired user announcements count.
     */
    public Integer getExpiredAnnouncementsCount() {
        return expiredAnnouncementsCount;
    }

    /**
     * Set User Password.
     * 
     * @param password User Password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get User Password.
     * 
     * @return User Password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set Sub Status.
     * 
     * @param subStatus Sub Status.
     */
    public void setSubStatus(Integer subStatus) {
        this.subStatus = subStatus;
    }

    /**
     * Get Sub Status.
     * 
     * @return Sub Status.
     */
    public Integer getSubStatus() {
        return subStatus;
    }

    /**
     * Set WS Log Enabled
     * @param wsLogEnabled WS Log Enabled
     */
    public void setWsLogEnabled(Choice wsLogEnabled) {
        this.wsLogEnabled = wsLogEnabled;
    }
    
    /**
     * Get WS Log Enabled
     * @return WS Log Enabled
     */
    public Choice getWsLogEnabled() {
        return wsLogEnabled;
    }

    /**
     * Set WS Log Enabled Desc
     * @param wsLogEnabledDesc WS Log Enabled Desc
     */
    public void setWsLogEnabledDesc(String wsLogEnabledDesc) {
        this.wsLogEnabledDesc = wsLogEnabledDesc;
    }
    
    /**
     * Get WS Log Enabled Desc
     * @return WS Log Enabled Desc
     */
    public String getWsLogEnabledDesc() {
        return wsLogEnabledDesc;
    }
    
    /**
     * Set External User Mapping
     * @param externalUserMapping External User Mapping
     */
    public void setExternalUserMapping(String externalUserMapping) {
        this.externalUserMapping = externalUserMapping;
    }
    
    /**
     * Get External User Mapping
     * @return External User Mapping
     */
    public String getExternalUserMapping() {
        return externalUserMapping;
    }

    /**
     *  Getter of Session Timeout
     *
     * @return sessionTimeOut
     */
    public Long getSessionTimeOut() {
        return sessionTimeOut;
    }

    /**
     *  Setter of Session Timeout
     *
     * @param  sessionTimeOut
     */
    public void setSessionTimeOut(Long sessionTimeOut) {
        this.sessionTimeOut = sessionTimeOut;
    }

    /**
     * Getter of max login trails
     *
     * @return maxLoginTrails
     */
    public Long getMaxLoginTrails() {
        return maxLoginTrails;
    }

    /**
     * Setter of max login trails
     *
     * @param  maxLoginTrails
     */
    public void setMaxLoginTrails(Long maxLoginTrails) {
        this.maxLoginTrails = maxLoginTrails;
    }

    /**
     * Get Password Age
     *
     * @return passwordAge
     */
    public Long getPasswordAge() {
        return passwordAge;
    }

    /**
     * Set Password Age
     *
     * @param  passwordAge
     */
    public void setPasswordAge(Long passwordAge) {
        this.passwordAge = passwordAge;
    }
}
