/*
 * Copyright (c) i-Soft 2008.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Bassem R. Zohdy    05/02/2008  - first version.
 *
 * 1.01  Hamzeh Abu Lawi    03/03/2008  - add isTransferableTrafficFile
 *
 * 1.02  Waseem N.Zawaideh  17/03/2008  - Add two attributes orgId and prsId
 *                                          with their accessors and mutators
 *
 * 1.03  Mahmoud Atiyeh     21/04/2013  - added vehicleVO property.
 *
 */

package ae.eis.common.vo;
  
import ae.eis.eps.vo.ProcedureVO;
 
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

public class TrafficFileVO extends ValueObject {

    /*
     * Constance
     */

    /** Column ( TF_STP_TRAFFIC_FILES ) */
    public static final String TRAFFIC_NO = "TRAFFIC_NO";

    /** Table Name */
    public static final String TABLE_NAME = "TF_STP_TRAFFIC_FILES";

    /** Open File Reason */
    public static final Integer OPEN_FILE_REASON_OPEN_TRAINING_FILE = new Integer(1);
    public static final Integer OPEN_FILE_ISSUE_LICENSE_FROM_REPLACMENT = new Integer(2);
    public static final Integer OPEN_FILE_REGISTER_VEHICLE = new Integer(3);
    public static final Integer OPEN_FILE_BUY_PLATE = new Integer(4);
    public static final Integer OPEN_FILE_EXPORT_VEHICLE = new Integer(5);
    public static final Integer OPEN_FILE_SUBSCRIBE_TO_AUCTION = new Integer(6);
    public static final Integer OPEN_FILE_ALLOCATING_SPECIAL_PLATE = new Integer(7);
    public static final Integer OPEN_FILE_EXPORT_VEHICLE_TO_BUYER_TRF_FILE = new Integer(8);
    public static final Integer OPEN_FILE_CHANGE_OWNERSHIP = new Integer(9);
    public static final Integer OPEN_FILE_TRANSFER_VEHICLES = new Integer(10);
    public static final Integer OPEN_FILE_POSSESSION_VEHICLE = new Integer(11);
    public static final Integer OPEN_FILE_CHANGE_POSSESSION_OWNERSHIP = new Integer(12);
    public static final Integer OPEN_FILE_CHANGE_OWNERSHIP_YEAR = new Integer(13);
    
    public static final Integer OPEN_FILE_ALLOCATING_PLATE = new Integer(16);
    public static final Integer OPEN_FILE_ALLOCATING_AUCTION_SPECIAL_PLATE = new Integer(17);
    
    /** Open File Reason Update Person Info */ 
    public static final Integer  OPEN_FILE_REASON_UPDATE_PERSON_INFO = new Integer(14);

    /**Fee Collection Status*/
    public static final Integer FEE_COLLECTION_STATUS_NOT_COLLECTED = new Integer(1);
    public static final Integer FEE_COLLECTION_STATUS_COLLECTED = new Integer(2);
    public static final Integer FEE_COLLECTION_STATUS_OLD_FILE = new Integer(3);

    /** TF_OFFENDER_CLASS */
    public static final Integer TRAFFIC_FILE_TYPE_PRIVATE_FACILITIES = new Integer(4);
    public static final Integer TRAFFIC_FILE_TYPE_CAR_RENTAL_COMPANIES = new Integer(5);


    /** Open File Reason Set */
    public static final Set OPEN_FILE_REASON_SET = new HashSet();

    /** Subscriber Type Person */
    public static final Integer SUBSCRIBER_TYPE_PERSON = new Integer(1);

    /** Subscriber Type Organizations */
    public static final Integer SUBSCRIBER_TYPE_ORGANIZATIONS = new Integer(2);


    /** Person ID. */
    private Long prsId;

    /** Organization ID. */
    private Long orgId;

    /** Traffic file emicode */
    private String emiCode;

    /** Traffic file No. */
    @XmlElement(name = "trafficNo")
    private Long trafficNo;

    /** Traffic file Class. */
    private String trafficFileClass;

    /** Traffic file Class En Desc. */
    private String trafficFileClassDescEn;

    /** Traffic file Class Ar Desc. */
    private String trafficFileClassDescAr;

     
    /** Arabic Owner Name */
    @XmlElement(name = "ownerNameAr")
    private String ownerNameAr;

    /** Prefered Shipment Email */
    private String preferedShipmentEmail;

    /** Prefered Shipment Phone */
    private String preferedShipmentPhone;

    /** Prefered Shipment Mobile */
    private String preferedShipmentMobile;

    /** Prefered First Address */
    private String preferedFirstAddress;

    /** Prefered Second Address */
    private String preferedSecondAddress;

    /** Prefered Shipment Name */
    private String preferedShipmentName;

    /** English Prefered Shipment Name */
    private String preferedShipmentNameEn;

    /** Prefered Emiarte */
    private String preferedEmirate;

    /** Prefered Fedex Location */
    private String preferedFedexLocation;

    /**Transferable traffic file number**/
    public static final String TRANSFERABLE_TRAFFIC_NO = "19999999";

    /**Transferable traffic file ID**/
    public static final long TRANSFERABLE_TRAFFIC_FILE_ID = 19999999;

    /** desc Of Transferable Traffic File Id **/
    public static final String DESC_OF_TRANSFERABLE_TRAFFIC_FILE_ID = "Personal Export";

    
    /** Fee Collection Status */
    private Integer feeCollectionStatus;

    /** Traffic  Owner Ar */
    private String trafficOwnerAr;

    /** Open File Reason */
    private Integer openFileReason;

    /** UTS Traffic File */
    private Long utsTrafficFile;

    /** Needs Insurance */
    private boolean needsInsurance;

     /** preferred Delivery Center */
    private Choice sendSms;

    /** preferred Delivery Method */
    private String preferredDeliveryMethod;

    /** preferred Language Code */
    private String preferredLanCode;

    /** is Courier Available */
    private Choice isCourierAvailable;

    /** is Collection Role */
    private Choice isCollectionRole;

    /** is Courier Role */
    private Choice isCourierRole;

    /** is Mail Shared */
    private Choice isMailShared;


    /** Traffic Owner En */
    private String trafficOwnerEn;

    
    /** preferred Phone */
    private String preferredPhone;

    /** preferred PO Box */
    private String preferredPOBox;

    /** po Box City Code */
    private String poBoxCityCode;

    /** is Mail delivery */
    private Choice isMailDelivery;

    /** is Kiosk delivery */
    private Choice isKioskDelivery;

    
    /** Owner Type */
    private String ownerType;
 
    /** Send Email Notification */
    private Choice sendEMail;

    /** Owner Email */
    private String ownerEmail;

    /** Owned Vehicles Count*/
    private Integer ownedVehiclesCount;

    /** traffic File Type*/
    private Integer trafficFileType;

    /** owner Name En */
    @XmlElement(name = "ownerNameEn")
    private String ownerNameEn;

    /** Owner Id */
    private Long ownerId;

    /** User Language */
    private String userLanguage;

    /** Has VSD Block */
    private Choice hasVSDBlock;

    /** Required ENOC */
    private Choice requiredENOC;

    /** Required ENOC Int*/
    private Integer requiredENOCInt;

    /** Traffic Owner Fax */
    private String trafficOwnerFax;

    /** Traffic Owner Mobile */
    private String trafficOwnerMobile;

    /** Traffic Owner Email */
    private String trafficOwnerEmail;

    /** Traffic Owner Po Box City Code */
    private String trafficOwnerPoBoxCityCode;

    /** Traffic Owner Po Box No */
    private String trafficOwnerPoBoxNo;

    /** Traffic profile needs update */
    private Choice trafficProfileNeedsUpdate;

    /** Traffic profile update date */
    private Date trafficProfileUpdateDate;

    /** file Audit Status*/
    private Integer fileAuditStatus;

    /** file Search Method*/
    private Integer fileSearchMethod;

    /** needs Open Reason Validation*/
    private Integer needsOpenReasonValidation;

     
    /**traffic File Trs Id*/
    private Long trafficFileTrsId;
    /**ProcedureVO*/
    private ProcedureVO procedureVO;

    /** prefered Shipment Company Name.  */
    private String preferedShipmentCompanyName;    

    /** is Immediate delivery */
    private Choice isImmediateDeliveryAvailable;
    
    
    /*
     * Static Initializer
     */
    static {
        OPEN_FILE_REASON_SET.add(OPEN_FILE_REGISTER_VEHICLE);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_BUY_PLATE);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_REASON_OPEN_TRAINING_FILE);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_ISSUE_LICENSE_FROM_REPLACMENT);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_EXPORT_VEHICLE);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_SUBSCRIBE_TO_AUCTION);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_ALLOCATING_SPECIAL_PLATE);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_CHANGE_OWNERSHIP);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_CHANGE_OWNERSHIP_YEAR);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_TRANSFER_VEHICLES);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_POSSESSION_VEHICLE);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_CHANGE_POSSESSION_OWNERSHIP);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_EXPORT_VEHICLE_TO_BUYER_TRF_FILE);        
        OPEN_FILE_REASON_SET.add(OPEN_FILE_REASON_UPDATE_PERSON_INFO);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_ALLOCATING_PLATE);
        OPEN_FILE_REASON_SET.add(OPEN_FILE_ALLOCATING_AUCTION_SPECIAL_PLATE);
    }

    /*
     * Constructors
     * */

    /**
     * Create traffic file value object.
     * @since 1.0
     */
    public TrafficFileVO() {
    }

    /**
     * Create tarrfic file value object contains traffic file id
     * and traffic file number.
     * @since 1.0
     * @param trafficNo traffic file number.
     * @param id traffic file id.
     */
    public TrafficFileVO(Long id, Long trafficNo) {
        setId(id);
        setTrafficNo(trafficNo);
    }

    /**
     * Create tarrfic file value object contains traffic file number.
     * @since 1.0
     * @param trafficNo traffic file number.
     */
    public TrafficFileVO(Long trafficNo) {
        setTrafficNo(trafficNo);
    }

    public Boolean isPersonFile() {
        if (getTrafficNo().toString().charAt(0) == '1') {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public Boolean isOrganizationFile() {
        if (getTrafficNo().toString().charAt(0) == '5') {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }


    public void setTrafficNo(Long trafficNo) {
        this.trafficNo = trafficNo;
    }


    public Long getTrafficNo() {
        return trafficNo;
    }

 
 


    public void setOwnerNameAr(String ownerNameAr) {
        this.ownerNameAr = ownerNameAr;
    }


    public String getOwnerNameAr() {
        return ownerNameAr;
    }

    /**
     * Returns the String representation of this object.
     * @since 1.1
     * @return String representation of this object.
     */
    public String toString() {
        StringBuffer buf = new StringBuffer(super.toString());
        buf.append("trafficNo=").append(getTrafficNo()).append(",transferableTrafficFile=").append(isTransferableTrafficFile());
        return buf.toString();
    }

    /**
     * Check ifthe traffic file id for current insatnce is equal to 19999999
     * @return True if the traffic file id is equal to 19999999 otherwise
     *                return false;
     */
    public boolean isTransferableTrafficFile() {
        if (getId() != null && TRANSFERABLE_TRAFFIC_FILE_ID == getId().longValue())
            return true;
        else if (getTrafficNo() != null && TRANSFERABLE_TRAFFIC_NO.equals(getTrafficNo().toString())) {
            return true;
        }
        return false;
    }


    /**
     * Set person ID
     *
     * @param prsId
     */
    public void setPrsId(Long prsId) {
        this.prsId = prsId;
    }

    /**
     * Get person ID
     *
     * @return person ID
     */
    public Long getPrsId() {
        return prsId;
    }

    /**
     * Set organization ID
     *
     * @param orgId
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * Get organization ID
     *
     * @return organization ID
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * Set emirate code
     *
     * @param emiCode
     */
    public void setEmiCode(String emiCode) {
        this.emiCode = emiCode;
    }

    /**
     * Get emirate code
     *
     * @return emicode
     */
    public String getEmiCode() {
        return emiCode;
    }

    
     

    /**
     * Set Prefered Shipment Email
     *
     * @param preferedShipmentEmail Prefered Shipment Email
     */
    public void setPreferedShipmentEmail(String preferedShipmentEmail) {
        this.preferedShipmentEmail = preferedShipmentEmail;
    }

    /**
     * Get Prefered Shipment Email
     *
     * @return preferedShipmentEmail
     */
    public String getPreferedShipmentEmail() {
        return preferedShipmentEmail;
    }

    /**
     * Set Prefered Shipment Mobile
     *
     * @param preferedShipmentMobile Prefered Shipment Mobile
     */
    public void setPreferedShipmentMobile(String preferedShipmentMobile) {
        if (preferedShipmentMobile != null) {
            this.preferedShipmentMobile = preferedShipmentMobile.trim();
        } else {
            this.preferedShipmentMobile = preferedShipmentMobile;
        }
    }

    /**
     * Get Prefered Shipment Mobile
     *
     * @return preferedShipmentMobile Prefered Shipment Mobile
     */
    public String getPreferedShipmentMobile() {
        return preferedShipmentMobile;
    }



    /**
     * Setter For Prefered Shipment Phone
     *
     * @param preferedShipmentPhone Prefered Shipment Phone
     */
    public void setPreferedShipmentPhone(String preferedShipmentPhone) {
        this.preferedShipmentPhone = preferedShipmentPhone;
    }

    /**
     * Getter Fior Prefered Shipment Phone
     *
     * @return Prefered Shipment Phone
     */
    public String getPreferedShipmentPhone() {
        return preferedShipmentPhone;
    }

    /**
     * Setter For Prefered First Address
     *
     * @param preferedFirstAddress Prefered First Address
     */
    public void setPreferedFirstAddress(String preferedFirstAddress) {
        this.preferedFirstAddress = preferedFirstAddress;
    }

    /**
     * Getter For Prefered First Address
     *
     * @return Prefered First Address
     */
    public String getPreferedFirstAddress() {
        return preferedFirstAddress;
    }

    /**
     * Sette For Prefered Second Address
     *
     * @param preferedSecondAddress Prefered Second Address
     */
    public void setPreferedSecondAddress(String preferedSecondAddress) {
        this.preferedSecondAddress = preferedSecondAddress;
    }

    /**
     * Getter For Prefered Second Address
     *
     * @return Prefered Second Address
     */
    public String getPreferedSecondAddress() {
        return preferedSecondAddress;
    }

    /**
     * Setter For Prefered Emirate
     *
     * @param preferedEmirate Prefered Emirate
     */
    public void setPreferedEmirate(String preferedEmirate) {
        this.preferedEmirate = preferedEmirate;
    }

    /**
     * Getter For Prefered Emirate
     *
     * @return Prefered Emirate
     */
    public String getPreferedEmirate() {
        return preferedEmirate;
    }

    /**
     * Setter For Prefered Fedex Location
     *
     * @param preferedFedexLocation Prefered Fedex Location
     */
    public void setPreferedFedexLocation(String preferedFedexLocation) {
        this.preferedFedexLocation = preferedFedexLocation;
    }

    /**
     * Getter For Prefered Fedex Location
     *
     * @return Prefered Fedex Location
     */
    public String getPreferedFedexLocation() {
        return preferedFedexLocation;
    }

    /**
     * Setter For Prefered Shipment Name
     *
     * @param preferedShipmentName Prefered Shipment Name
     */
    public void setPreferedShipmentName(String preferedShipmentName) {
        this.preferedShipmentName = preferedShipmentName;
    }

    /**
     * Getter For Prefered Shipment Name
     *
     * @return Prefered Shipment Name
     */
    public String getPreferedShipmentName() {
        return preferedShipmentName;
    }

    /**
     * Set Fee Collection Status
     *
     * @param feeCollectionStatus Fee Collection Status
     */
    public void setFeeCollectionStatus(Integer feeCollectionStatus) {
        this.feeCollectionStatus = feeCollectionStatus;
    }

    /**
     * Get Fee Collection Status
     *
     * @return feeCollectionStatus Fee Collection Status
     */
    public Integer getFeeCollectionStatus() {
        return feeCollectionStatus;
    }

    /**
     * Setter for arabic traffic owner
     *
     * @param trafficOwnerAr arabic traffic owner
     */
    public void setTrafficOwnerAr(String trafficOwnerAr) {
        this.trafficOwnerAr = trafficOwnerAr;
    }

    /**
     * Getter for arabic traffic owner
     *
     * @return arabic traffic owner
     */
    public String getTrafficOwnerAr() {
        return trafficOwnerAr;
    }

    /**
     * Setter for Open File Reason
     *
     * @param openFileReason : Open File Reason
     */
    public void setOpenFileReason(Integer openFileReason) {

        if (openFileReason != null) {
            if (!OPEN_FILE_REASON_SET.contains(openFileReason)) {
                throw new IllegalArgumentException("Invalid value for Open File Reason.. " + openFileReason);
            }
        }
        this.openFileReason = openFileReason;
    }

    /**
     * Getter for Open File Reason
     *
     * @return Open File Reason
     */
    public Integer getOpenFileReason() {
        return openFileReason;
    }

    /**
     * Setter for UTS Traffic File
     *
     * @param utsTrafficFile : UTS Traffic File
     */
    public void setUtsTrafficFile(Long utsTrafficFile) {
        this.utsTrafficFile = utsTrafficFile;
    }

    /**
     * Getter for UTS Traffic File
     *
     * @return UTS Traffic File
     */
    public Long getUtsTrafficFile() {
        return utsTrafficFile;
    }

    /**
     * Check if Open File Reason is Open Training File
     *
     * @return true if Open File Reason is Open Training File
     */
    public boolean isOpenFileReasonOpenTrainingFile() {
        return openFileReason != null && openFileReason.equals(OPEN_FILE_REASON_OPEN_TRAINING_FILE);
    }

    /**
     * Check if Open File Reason is Issue License From Replacment
     *
     * @return true if Open File Reason is Issue License From Replacment
     */
    public boolean isOpenFileReasonIssueLicenseFromReplacment() {
        return openFileReason != null && openFileReason.equals(OPEN_FILE_ISSUE_LICENSE_FROM_REPLACMENT);
    }

    /**
     * Check if Open File Reason is Register Vehicle
     *
     * @return true if Open File Reason is Register Vehicle
     */
    public boolean isOpenFileReasonRegisterVehicle() {
        return openFileReason != null && openFileReason.equals(OPEN_FILE_REGISTER_VEHICLE);
    }

    /**
     * Check if Open File Reason is Buy Plate
     *
     * @return true if Open File Reason is Buy Plate
     */
    public boolean isOpenFileReasonBuyPlate() {
        return openFileReason != null && openFileReason.equals(OPEN_FILE_BUY_PLATE);
    }

    /**
     * Check if Open File Reason is Export Vehicle
     *
     * @return true if Open File Export Vehicle
     */
    public boolean isOpenFileReasonExportVehicle() {
        return openFileReason != null && openFileReason.equals(OPEN_FILE_EXPORT_VEHICLE);
    }



    /**
     * Setter for Needs Insurance
     * @param needsInsurance Needs Insurance
     */
    public void setNeedsInsurance(boolean needsInsurance) {
        this.needsInsurance = needsInsurance;
    }

    /**
     * Getter for Needs Insurance
     * @return needsInsurance Needs Insurance
     */
    public boolean isNeedsInsurance() {
        return needsInsurance;
    }

     
    /**
     * Setter for send Sms
     * @param sendSms
     */
    public void setSendSms(Choice sendSms) {
        this.sendSms = sendSms;
    }

    /**
     * Getter for send Sms
     * @return sendSms
     */
    public Choice getSendSms() {
        return sendSms;
    }

    /**
     * Setter for preferred Delivery Method
     * @param preferredDeliveryMethod
     */
    public void setPreferredDeliveryMethod(String preferredDeliveryMethod) {
        this.preferredDeliveryMethod = preferredDeliveryMethod;
    }

    /**
     * Getter for preferred Delivery Method
     * @return preferredDeliveryMethod
     */
    public String getPreferredDeliveryMethod() {
        return preferredDeliveryMethod;
    }

    /**
     * Setter for preferred Lan Code
     * @param preferredLanCode
     */
    public void setPreferredLanCode(String preferredLanCode) {
        this.preferredLanCode = preferredLanCode;
    }

    /**
     * Getter for preferred Lan Code
     * @return preferredLanCode
     */
    public String getPreferredLanCode() {
        return preferredLanCode;
    }

    /**
     * Setter for is Courier Available
     * @param isCourierAvailable
     */
    public void setIsCourierAvailable(Choice isCourierAvailable) {
        this.isCourierAvailable = isCourierAvailable;
    }

    /**
     * Getter for is Courier Available
     * @return isCourierAvailable
     */
    public Choice getIsCourierAvailable() {
        return isCourierAvailable;
    }

    /**
     * Setter for is Collection Role
     * @param isCollectionRole
     */
    public void setIsCollectionRole(Choice isCollectionRole) {
        this.isCollectionRole = isCollectionRole;
    }

    /**
     * Getter for is Collection Role
     * @return isCollectionRole
     */
    public Choice getIsCollectionRole() {
        return isCollectionRole;
    }

    /**
     * Setter for isCourierRole
     * @param isCourierRole
     */
    public void setIsCourierRole(Choice isCourierRole) {
        this.isCourierRole = isCourierRole;
    }

    /**
     * Getter for is Courier Role
     * @return isCourierRole
     */
    public Choice getIsCourierRole() {
        return isCourierRole;
    }

    /**
     * Setter for is Mail Shared
     * @param isMailShared
     */
    public void setIsMailShared(Choice isMailShared) {
        this.isMailShared = isMailShared;
    }

    /**
     * Getter for is Mail Shared
     * @return isMailShared
     */
    public Choice getIsMailShared() {
        return isMailShared;
    }

    /**
     * Set English traffic Owner
     *
     * @param trafficOwnerEn English traffic Owner
     */
    public void setTrafficOwnerEn(String trafficOwnerEn) {
        this.trafficOwnerEn = trafficOwnerEn;
    }

    /**
     * Get English traffic Owner
     *
     * @return English traffic Owner
     */
    public String getTrafficOwnerEn() {
        return trafficOwnerEn;
    }
 

    /**
     * Set English Prefered Shipment Name.
     *
     * @param preferedShipmentNameEn English Prefered Shipment Name
     */
    public void setPreferedShipmentNameEn(String preferedShipmentNameEn) {
        this.preferedShipmentNameEn = preferedShipmentNameEn;
    }

    /**
     * Get English Prefered Shipment Name.
     *
     * @return English Prefered Shipment Name.
     */
    public String getPreferedShipmentNameEn() {
        return preferedShipmentNameEn;
    }

    /**
     * Set preferredPhone.
     *
     * @param preferredPhone :  preferred Phone
     */
    public void setPreferredPhone(String preferredPhone) {
        this.preferredPhone = preferredPhone;
    }

    /**
     * Get preferredPhone.
     *
     * @return preferredPhone : preferred Phone.
     */
    public String getPreferredPhone() {
        return preferredPhone;
    }

    /**
     * Set preferredPOBox.
     *
     * @param preferredPOBox :  preferred PO Box
     */
    public void setPreferredPOBox(String preferredPOBox) {
        this.preferredPOBox = preferredPOBox;
    }

    /**
     * Get preferredPOBox.
     *
     * @return preferredPOBox : preferred PO Box.
     */
    public String getPreferredPOBox() {
        return preferredPOBox;
    }

    /**
     * Set poBoxCityCode.
     *
     * @param poBoxCityCode :  po Box City Code
     */
    public void setPoBoxCityCode(String poBoxCityCode) {
        this.poBoxCityCode = poBoxCityCode;
    }

    /**
     * Get poBoxCityCode.
     *
     * @return poBoxCityCode : po Box City Code.
     */
    public String getPoBoxCityCode() {
        return poBoxCityCode;
    }

    /**
     * Set Maildelivery.
     *
     * @param Maildelivery :  Mail delivery.
     */
    public void setIsMailDelivery(Choice isMailDelivery) {
        this.isMailDelivery = isMailDelivery;
    }

    /**
     * Get isMaildelivery.
     *
     * @return isMaildelivery :isMaildelivery.
     */
    public Choice getIsMailDelivery() {
        return isMailDelivery;
    }

    /**
     * Set is Kiosk Delivery flag.
     *
     * @param isKioskDelivery :  Kiosk Delivery flag.
     */
    public void setIsKioskDelivery(Choice isKioskDelivery) {
        this.isKioskDelivery = isKioskDelivery;
    }

    /**
     * Get is Kiosk Delivery.
     *
     * @return isKioskDelivery :isKioskDelivery.
     */
    public Choice getIsKioskDelivery() {
        return isKioskDelivery;
    }

   

    /**
     * Set Owner Tpye
     *
     * @param ownerType Owner Tpye
     */
    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    /**
     * Get Owner Tpye
     *
     * @return Owner Tpye
     */
    public String getOwnerType() {
        return ownerType;
    }

     

    /**
     * Set Send E-Mail
     *
     * @param sendEMail Send E-Mail
     */
    public void setSendEMail(Choice sendEMail) {
        this.sendEMail = sendEMail;
    }

    /**
     * Get Send E-Mail
     *
     * @return Send E-Mail
     */
    public Choice getSendEMail() {
        return sendEMail;
    }

    /**
     * True if Email notification is true
     *
     * @return if Email notification is true
     */
    public boolean isNotificationTypeSendEMail() {

        return (sendEMail != null && sendEMail.equals(Choice.YES));
    }

    /**
     * True if SMS notification is true
     *
     * @return if SMS notification is true
     */
    public boolean isNotificationTypeSendSMS() {

        return (sendSms != null && sendSms.equals(Choice.YES));
    }

    /**
     * Set Owned Vehicles Count
     *
     * @param ownedVehiclesCount : Owned Vehicles Count
     */
    public void setOwnedVehiclesCount(Integer ownedVehiclesCount) {
        this.ownedVehiclesCount = ownedVehiclesCount;
    }

    /**
     * Get Owned Vehicles Count
     *
     * @return ownedVehiclesCount : Owned Vehicles Count
     */
    public Integer getOwnedVehiclesCount() {
        return ownedVehiclesCount;
    }

    /**
     * Set trafficFileType
     *
     * @param trafficFileType : traffic File Type
     */
    public void setTrafficFileType(Integer trafficFileType) {
        this.trafficFileType = trafficFileType;
    }

    /**
     * Get trafficFileType
     *
     * @return trafficFileType : traffic File Type.
     */
    public Integer getTrafficFileType() {
        return trafficFileType;
    }

    /**
     * Set ownerNameEn
     *
     * @param ownerNameEn : owner Name En.
     */
    public void setOwnerNameEn(String ownerNameEn) {
        this.ownerNameEn = ownerNameEn;
    }

    /**
     * Get ownerNameEn
     *
     * @return ownerNameEn : owner Name En.
     */
    public String getOwnerNameEn() {
        return ownerNameEn;
    }

    /**
     * Set Owner Id
     *
     * @param ownerId
     */
    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * Get Owner Id
     *
     * @return
     */
    public Long getOwnerId() {
        return ownerId;
    }

    /**
     * Setter for Owner Email
     *
     * @param ownerEmail : Owner Email
     */
    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    /**
     * Getter for Owner Email
     *
     * @return Owner Email
     */
    public String getOwnerEmail() {
        return ownerEmail;
    }

    /**
     * Setter for User Language
     *
     * @param userLanguage : User Language
     */
    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    /**
     * Getter for User Language
     *
     * @return User Language
     */
    public String getUserLanguage() {
        return userLanguage;
    }

    /**
     * Setter for Traffic File Class
     *
     * @param trafficFileClass : traffic File Class
     */
    public void setTrafficFileClass(String trafficFileClass) {
        this.trafficFileClass = trafficFileClass;
    }

    /**
     * Getter Traffic File Class
     *
     * @return trafficFileClass
     */
    public String getTrafficFileClass() {
        return trafficFileClass;
    }

    /**
     * Setter for Traffic File Class En
     *
     * @param trafficFileClassDescEn : traffic File Class En
     */
    public void setTrafficFileClassDescEn(String trafficFileClassDescEn) {
        this.trafficFileClassDescEn = trafficFileClassDescEn;
    }

    /**
     * Getter Traffic File Class En
     *
     * @return trafficFileClassDescEn
     */
    public String getTrafficFileClassDescEn() {
        return trafficFileClassDescEn;
    }

    /**
     * Setter for Traffic File Class Ar
     *
     * @param trafficFileClassDescAr : traffic File Class Ar
     */
    public void setTrafficFileClassDescAr(String trafficFileClassDescAr) {
        this.trafficFileClassDescAr = trafficFileClassDescAr;
    }

    /**
     * Getter Traffic File Class Ar
     *
     * @return trafficFileClassDescAr
     */
    public String getTrafficFileClassDescAr() {
        return trafficFileClassDescAr;
    }

    /**
     * Setter for Has VSD Block
     *
     * @param hasVSDBlock : Has VSD Block
     */
    public void setHasVSDBlock(Choice hasVSDBlock) {
        this.hasVSDBlock = hasVSDBlock;
    }

    /**
     * Getter Has VSD Block
     *
     * @return Has VSD Block
     */
    public Choice getHasVSDBlock() {
        return hasVSDBlock;
    }












    /**
     * Set Required ENOC
     * @param requiredENOC Required ENOC
     */
    public void setRequiredENOC(Choice requiredENOC) {
        this.requiredENOC = requiredENOC;
    }

    /**
     * Get Required ENOC
     * @return Required ENOC
     */
    public Choice getRequiredENOC() {
        return requiredENOC;
    }

    /**
     * Set Required ENOC Int
     * @param requiredENOCInt Required ENOC Int
     */
    public void setRequiredENOCInt(Integer requiredENOCInt) {
        this.requiredENOCInt = requiredENOCInt;
    }

    /**
     * Get Required ENOC Int
     * @return Required ENOC Int
     */
    public Integer getRequiredENOCInt() {
        return requiredENOCInt;
    }


    /**
     * Set traffic profile needs update
     *
     * @param trafficProfileNeedsUpdate Traffic profile needs update
     */
    public void setTrafficProfileNeedsUpdate(Choice trafficProfileNeedsUpdate) {
        this.trafficProfileNeedsUpdate = trafficProfileNeedsUpdate;
    }

    /**
     * Get traffic profile needs update
     *
     * @return Identify if the traffic file profile needs update or not
     */
    public Choice getTrafficProfileNeedsUpdate() {
        return trafficProfileNeedsUpdate;
    }

    /**
     * Set traffic profile update date
     *
     * @param trafficProfileUpdateDate Traffic profile update date
     */
    public void setTrafficProfileUpdateDate(Date trafficProfileUpdateDate) {
        this.trafficProfileUpdateDate = trafficProfileUpdateDate;
    }

    /**
     * Set traffic profile update date
     *
     * @return Traffic profile update date
     */
    public Date getTrafficProfileUpdateDate() {
        return trafficProfileUpdateDate;
    }


    /**
     * Setter for fileAuditStatus
     * @param fileAuditStatus
     */
    public void setFileAuditStatus(Integer fileAuditStatus) {
        this.fileAuditStatus = fileAuditStatus;
    }

    /**Getter for fileAuditStatus
     * @return fileAuditStatus
     */
    public Integer getFileAuditStatus() {
        return fileAuditStatus;
    }

    /**
     * Setter for fileSearchMethod
     * @param fileSearchMethod
     */
    public void setFileSearchMethod(Integer fileSearchMethod) {
        this.fileSearchMethod = fileSearchMethod;
    }

    /**
     * Getter for fileSearchMethod
     * @return fileSearchMethod
     */
    public Integer getFileSearchMethod() {
        return fileSearchMethod;
    }

    /**
     * Setter for needsOpenReasonValidation
     * @param needsOpenReasonValidation
     */
    public void setNeedsOpenReasonValidation(Integer needsOpenReasonValidation) {
        this.needsOpenReasonValidation = needsOpenReasonValidation;
    }

    /**
     * Getter for needsOpenReasonValidation
     * @return needsOpenReasonValidation
     */
    public Integer getNeedsOpenReasonValidation() {
        return needsOpenReasonValidation;
    }

     

    /**
     * Setter for trafficFileTrsId
     * @param trafficFileTrsId
     */
    public void setTrafficFileTrsId(Long trafficFileTrsId) {
        this.trafficFileTrsId = trafficFileTrsId;
    }

    /**
     * Getter for trafficFileTrsId
     * @return trafficFileTrsId
     */
    public Long getTrafficFileTrsId() {
        return trafficFileTrsId;
    }


      

    /**
     * Setter for procedureVO
     * @param procedureVO
     */
    public void setProcedureVO(ProcedureVO procedureVO) {
        this.procedureVO = procedureVO;
    }

    /**
     * Getter for procedureVO
     * @return procedureVO
     */
    public ProcedureVO getProcedureVO() {
        return procedureVO;
    }
    
    
    /**
     * Setter for preferedShipmentCompanyName
     * @param preferedShipmentCompanyName
     */
    public void setPreferedShipmentCompanyName(String preferedShipmentCompanyName) {
        this.preferedShipmentCompanyName = preferedShipmentCompanyName;
    }
    
    /**
     * Getter for preferedShipmentCompanyName
     * @return String
     */
    public String getPreferedShipmentCompanyName() {
        return preferedShipmentCompanyName;
    }

    /**
     *  Set Is Immediate Delivery
     * @param isImmediateDelivery
     */
    public void setIsImmediateDeliveryAvailable(Choice isImmediateDelivery) {
        this.isImmediateDeliveryAvailable = isImmediateDelivery;
    }
    
    /**
     * Get Is Immediate Delivery
     * @return isImmediateDeliveryAvailable
     */
    public Choice getIsImmediateDeliveryAvailable() {
        return isImmediateDeliveryAvailable;
    }

   
    


    /**
     *  Check if the traffic no valid or not
     * @return
     */
    public boolean hasValidTrafficNo() {
        if(trafficNo == null){
            return false;
        }
        
        if(trafficNo.toString().length() != 8 || (!isPersonFile() && !isOrganizationFile())) {
            return false;
        }
        
        return true;
    }
}
