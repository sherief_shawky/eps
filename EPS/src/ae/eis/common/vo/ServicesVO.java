/* 
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh    07/07/2008  - First version
 */

package ae.eis.common.vo;

import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version
 * @author Tariq Abu Amireh
 */
public class ServicesVO extends ValueObject  {
    
    /*
     * Service Code Values
     */
    /** Column ( TF_STP_SERVICES ) */
    public static final String CODE = "CODE";

    /** Table Name */
    public static final String TABLE_NAME = "TF_STP_SERVICES";    
    
    /** Issue New Company. */
    public static final Integer ISSUE_NEW_COMPANY = new Integer(801);
    
    /** Issue Learning Permit. */
    public static final Integer ISSUE_LEARNING_PERMIT = new Integer(2);
    
    /** Renew Permit Application. */
    public static final Integer RENEW_CML_PERMIT_APPLICATION = new Integer(804);
    
    /** Issue New Permit Application. */
    public static final Integer ISSUE_NEW_PERMIT_APPLICATION = new Integer(805);
    
    /** Issue Desert Learning Permit. */
    public static final Integer ISSUE_DESERT_LEARNING_PERMIT = new Integer(13);
    
    /** Issue Desert Driving License. */
    public static final Integer ISSUE_DESERT_DRIVING_LICENSE = new Integer(117);
    /**vehicle registration */
    public static final Integer VEHICLE_REGISTRATION=new Integer(201);
    /** renewal with change number  */
    public static final Integer RENEWAL_WITH_CHANGE_NUMBER=new  Integer(224);
    /** lost damaged  number  */
    public static final Integer LOST_DAMAGED_NUMBER=new Integer(218);
    /**tourism certificates   */
    public static final Integer TOURISM_CERTIFICATES=new Integer(212);
    /** lost damaged registration card  */
    public static final Integer LOST_DAMAGED_REGISTRATION_CARD=new  Integer(211);
    /** change ownership  */
    public static final Integer CHANGE_OWNERSHIP =new  Integer(202);
    /** change ownership for period  */
    public static final Integer CHANGE_OWNERSHIP_FOR_PERIOD=new  Integer(203);
    /** lost damaged export   */
    public static final Integer LOST_DAMAGED_EXPORT=new Integer(219);
    /** lost damaged possession   */
    public static final Integer LOST_DAMAGED_POSSESSION=new  Integer(220);
    /** export to country   */
    public static final Integer EXPORT_TO_COUNTRY=new Integer(206);
    /** export to emirate   */
    public static final Integer EXPORT_TO_EMIRATE=new Integer(207);
    /** possession  certificate  */
    public static final Integer POSSESSION_CERTIFICATE=new Integer(205);

    /** Experience Certificate Service Code */
    public static final Integer EXPERIENCE_CERTIFICATE = new Integer(105);
        
    /** Reprint Desert Learning Permit Service Code */
    public static final Integer REPRINT_DESERT_LEARNING_PERMIT = new Integer(15);

    /** Renew Desert Learning Permit Service Code. */
    public static final Integer RENEW_DESERT_LEARNING_PERMIT = new Integer(16);
    
    /** Refund Insurance Premium Certificate Service Code. */
    public static final Integer REFUND_INSURANCE_PREMIUM_CERTIFICATE = new Integer(216);
    
    /** Purchase Image Plate Service Code. */
    public static final Integer SVC_CODE_PURCHASE_IMAGE_PLATE = new Integer(231);

    /** issue housing learning permit */
    public static final Integer ISSUE_HOUSING_LEARNING_PERMIT = new Integer(18);

    /** renew housing learning permit */
    public static final Integer RENEW_HOUSING_LEARNING_PERMIT = new Integer(19);

    /** reprint housing learning permit */
    public static final Integer REPRINT_HOUSING_LEARNING_PERMIT = new Integer(21);
    
    /** renewal housing permit. */
    public static final Integer  RENEWAL_HOUSING_PERMIT = new Integer(121); 

    /** Reprint Hous Permit. */
    public static final Integer REPRINT_HOUS_PERMIT = new Integer(122);

    /** Renewal Desert Permit. */
    public static final Integer RENEWAL_DESERT_PERMIT = new Integer(123);
  
    /** Reprint Desert Driving Permit Service Code. */
    public static final Integer REPRINT_DESERT_DRIVING_PERMIT = new Integer(119);

    /** issue housing permit */
    public static final Integer SVC_ISSUE_HOUSING_PERMIT = new Integer(120);
    
    /** Issue New General Permit */
    public static final Integer SVC_ISSUE_GENERAL_PERMIT = new Integer(124);
    
    /** Change Possession Ownership */
    public static final Integer SVC_CHANGE_POSSESSION_OWNERSHIP = new Integer(240);
    
    /** Print Registered Vehicle Report */
    public static final Integer SVC_PRINT_REG_VHL_REPORT = new Integer(241);

    /** Modify License Class */
    public static final Integer SVC_MODIFY_LICENSE_CLASS = new Integer(4);
    
    /** Issue Exam Appointment */
    public static final Integer SVC_ISSUE_EXAM_APPOINTMENT = new Integer(7);
    
    /** Issue Exam Appointment */
    public static final Integer SVC_CHANGE_EXAM_APPOINTMENT = new Integer(8);
    
    /** svc add mortgage request */
    public static final Integer SVC_ADD_MORTGAGE_REQUEST = new Integer(245);

    /** svc add mortgage request */
    public static final Integer SVC_SALES_TRANSACTION = new Integer(228);
    
    /** svc Possess Unregistered Vehicle */
    public static final Integer SVC_CODE_POSSESS_UNREGISTERED_VEHICLE = new Integer(247);

    /** Service Approve mortgage request */
    public static final Integer SVC_APPROVE_MORTGAGE_REQUEST = new Integer(248);
    
    /** Service Approve Mortgage Release */
    public static final Integer SVC_APPROVE_MORTGAGE_RELEASE = new Integer(251);
    
    /** Service Approve Mortgage NOC */
    public static final Integer SVC_APPROVE_MORTGAGE_NOC = new Integer(252);
    
    public static final Integer SVC_ISSUE_NOC  = new Integer(253);
    
    /** Service Install Telematic Device */
    public static final Integer SVC_INSTALL_TELEMATIC_DEVICE = new Integer(254);
    
    /** Service issue NFC Cards */
    public static final Integer SVC_ISSUE_NFC_CARDS = new Integer(255);
    
    /** Service add roaming service */
    public static final Integer SVC_ADD_ROAMING_SERVICE = new Integer(259);
    
    /** SVC ISSUE DTT NOC*/
    public static final Integer SVC_ISSUE_DTT_NOC  = new Integer(24);
    
    /** SVC NO OBLIGATION TO SEND FILE TO EMIRATE */
    public static final Integer NO_OBLIGATION_TO_SEND_FILE_TO_EMIRATE  = new Integer(11);
    
    /** SVC NO OBLIGATION TO SEND FILE FROM EMIRATE */
    public static final Integer NO_OBLIGATION_TO_SEND_FILE_FROM_EMIRATE  = new Integer(12);
    
    /** SVC RENEW FILE */
    public static final Integer RENEW_FILE_SVC  = new Integer(23);    
    
    /** Open Traffic File Service Code. */
    public static final Integer SVC_OPEN_TRAFFIC_FILE = new Integer(931);
    
    /** Vehicle Renewl Service. */
    public static final Integer SVC_VEHICLE_RENEWAL = new Integer(204);
    
    /** Service Code Uninstall Telematic Service */
    public static final Integer SVC_CODE_UNINSTALL_TELEMATIC_SERVICE = new  Integer(260);
    
    /** Service Code Reinstall Telematic Service */
    public static final Integer SVC_CODE_REINSTALL_TELEMATIC_SERVICE = new  Integer(261);
    
    /** Eligibility Request Service Code */
    public static final Integer SVC_CODE_ELIGIBILITY_REQUEST = new Integer(14); 
    
    public static final int SERVICE_CODE_CHANGE_PLATE_OWNERSHIP = 403;

    /*
     * Instance Variables
     */
    /** Service Name */
    private String serviceName;
    
    /** Service English Name */
    private String serviceNameEn;
    
    /** Service Code */
    private String serviceCode;
    
    /** system Code */
    private String systemCode;
    
    /** is Needs Review */
    private Choice isNeedsReview;
    
    /** is Mail Delivery */
    private Choice isMailDelivery;
    
    /** Is Visible*/
    private Choice isVisible ;
    
    /** Is Courier And Collection */
    private Choice isCourierAndCollection;
    
    /** Service Usage **/
    private Integer usage;
    
    private String urlTransaction;
    
    /*
     * Constructors
     */
    /** Default Constructor */ 
    public ServicesVO() {
    }
    
    
    
    /*
     * Accessors
     */
    /**
     * set Service Name
     * 
     * @param serviceName ( Service Name )
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * get Service Name
     * 
     * @return serviceName ( Service Name )
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * set Service Code
     * 
     * @param serviceCode ( Service Code )
     */
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    /**
     * get Service Code
     * 
     * @return serviceCode ( Service Code )
     */
    public String getServiceCode() {
        return serviceCode;
    }    
    
    /**
     * Is Service Issue Desert Learning Permit
     *
     * @return True if Service Is Issue Desert Learning Permit
     */
    public boolean isServiceIssueDesertPermit() {
        return serviceCode != null && 
               serviceCode.equals(ISSUE_DESERT_LEARNING_PERMIT);
    }
    
    /**
     * Is Service Issue Desert Driving License
     *
     * @return True if Service Is Issue Driving License
     */
    public boolean isServiceIssueDesertDrivingLicense() {
        return serviceCode != null && 
               serviceCode.equals(ISSUE_DESERT_DRIVING_LICENSE); 
    }

    /**
     * Set Service English Name
     * 
     * @param serviceNameEn Service English Name
     */
    public void setServiceNameEn(String serviceNameEn) {
        this.serviceNameEn = serviceNameEn;
    }

    /**
     * Get Service English Name
     * 
     * @return Service English Name
     */
    public String getServiceNameEn() {
        return serviceNameEn;
    }
    
    
    /**
     * Is Service Vehicle Registration
     *
     * @return True if Service Is Vehicle Registration
     */
    public boolean isServiceVehicleRegistration() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString( VEHICLE_REGISTRATION));
    }
    
    /**
     * Is Service renewal with change number
     *
     * @return True if Service Is renewal with change number
     */
    public boolean isServiceRenewalWithChangeNumber() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString( RENEWAL_WITH_CHANGE_NUMBER));
    }
    
    /**
     * Is Service lost damaged number
     *
     * @return True if Service Is lost damaged number
     */
    public boolean isServiceLostDamagedNumber() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString( LOST_DAMAGED_NUMBER));
    }
    
    /**
     * Is Service tourism certificates
     *
     * @return True if Service Is  tourism certificates
     */
    public boolean isServiceTourismCertificates() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString( TOURISM_CERTIFICATES));
    }
    
    /**
     * Is Service lost damaged registration card
     *
     * @return True if Service Is  lost damaged registration card
     */
    public boolean isServiceLostDamagedRegistrationCard() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString( LOST_DAMAGED_REGISTRATION_CARD));
    }
    
    /**
     * Is Service change ownership
     *
     * @return True if Service Is  change ownership
     */
    public boolean isServiceChangeOwnership() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString( CHANGE_OWNERSHIP));
    }
    
    /**
     * Is Service change ownership for period
     *
     * @return True if Service Is  change ownership for period
     */
    public boolean isServiceChangeOwnershipForPeriod() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString( CHANGE_OWNERSHIP_FOR_PERIOD));
    }
    
    /**
     * Is Service lost damaged export
     *
     * @return True if Service Is  lost damaged export
     */
    public boolean isServiceLostDamagedExport() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(LOST_DAMAGED_EXPORT));
    }
    
    /**
     * Is Service lost damaged possession
     *
     * @return True if Service Is  lost damaged possession
     */
    public boolean isServiceLostDamagedPossession() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(LOST_DAMAGED_POSSESSION));
    } 
    
    /**
     * Is Service export to country
     *
     * @return True if Service Is  export to country
     */
    public boolean isServiceExportToCountry() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(EXPORT_TO_COUNTRY));
    } 
    
     /**
     * Is Service export to emirate
     *
     * @return True if Service Is  export to emirate
     */
    public boolean isServiceExportToEmirate() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(EXPORT_TO_EMIRATE));
    } 
    
    /**
     * Is Service possession certificate
     *
     * @return True if Service Is  possession certificate
     */
    public boolean isServicePossessionCertificate() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(POSSESSION_CERTIFICATE));
    } 
   
    /**
     * Is Service Experience certificate
     *
     * @return True if Service Is Experience certificate
     */
    public boolean isServiceExperienceCertificate() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(EXPERIENCE_CERTIFICATE));
    } 
    
    /**
     * Is Service Reprint Desert Learning Permit
     *
     * @return True if Service Is Reprint Desert Learning Permit
     */
    public boolean isServiceReprintDesertLearningPermit() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(REPRINT_DESERT_LEARNING_PERMIT));
    }     
    
    /**
     * Check If Service Is Renew Desert Learning Permit Service.
     *
     * @return true If Service Is Renew Desert Learning Permit Service.
     */
    public boolean isServiceRenewDesertLearningPermit() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(RENEW_DESERT_LEARNING_PERMIT));
    }

    /**
     * Check If Service Is Refund Insurance Premium Certificate Service.
     *
     * @return true If Service Is Refund Insurance Premium Certificate Service.
     */
    public boolean isRefundInsurancePremiumCertificate() {
        return serviceCode != null && 
               serviceCode.equals(GlobalUtilities.getString(REFUND_INSURANCE_PREMIUM_CERTIFICATE));
    }

    /**
     * Setter System Code.
     * 
     * @param systemCode: System Code.
     */
    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    /**
     * Getter System Code.
     * 
     * @return System Code
     */
    public String getSystemCode() {
        return systemCode;
    }

     /**
     * Setter Needs Review
     * 
     * @param isNeedsReview : is Needs Review.
     */
    public void setIsNeedsReview(Choice isNeedsReview) {
        this.isNeedsReview = isNeedsReview;
    }

    /**
     * isNeedsReview
     * 
     * @return isNeedsReview : is Needs Review
     */
    public Choice getIsNeedsReview() {
        return isNeedsReview;
    }

     /**
     * Setter is Mail Delivery
     * 
     * @param isMailDelivery: is Mail Delivery.
     */
    public void setIsMailDelivery(Choice isMailDelivery) {
        this.isMailDelivery = isMailDelivery;
    }

    /**
     * Getter isMailDelivery
     * 
     * @return isMailDelivery : is Mail Delivery.
     */
    public Choice getIsMailDelivery() {
        return isMailDelivery;
    }
    /**
     * Set Is Courier And Collection 
     * 
     * @param isCourierAndCollection Is Courier And Collection 
     */
    public void setIsCourierAndCollection(Choice isCourierAndCollection) {
        this.isCourierAndCollection = isCourierAndCollection;
    }

    /**
     * Get Is Courier And Collection 
     * 
     * @return Is Courier And Collection 
     */
    public Choice getIsCourierAndCollection() {
        return isCourierAndCollection;
    }
    

    /**
     * Set usage
     * 
     * @param usage
     */
    public void setUsage(Integer usage) {
        this.usage = usage;
    }

    /**
     * Get service usage. Represents transactions count in statuses 3 and 6 for this service.
     * 
     * @return usage
     */
    public Integer getUsage() {
        return usage;
    }

    public void setIsVisible(Choice isVisible) {
        this.isVisible = isVisible;
    }

    public Choice getIsVisible() {
        return isVisible;
    }


    public void setUrlTransaction(String urlTransaction) {
        this.urlTransaction = urlTransaction;
    }

    public String getUrlTransaction() {
        return urlTransaction;
    }

    public static boolean isSkippingTrafficNoValidation(Integer serviceCode) {
        
        if(serviceCode == null){
            return false;
        }
        
        List<Integer> skippingServices = new ArrayList<Integer>();
        skippingServices.add(SVC_ADD_MORTGAGE_REQUEST);
        skippingServices.add(SVC_APPROVE_MORTGAGE_REQUEST);
        skippingServices.add(SVC_APPROVE_MORTGAGE_RELEASE);
        skippingServices.add(SVC_APPROVE_MORTGAGE_NOC);
        skippingServices.add(SVC_SALES_TRANSACTION);
        skippingServices.add(SVC_ISSUE_NOC);
        skippingServices.add(SVC_VEHICLE_RENEWAL);
        return skippingServices.contains(serviceCode);
    }
}
