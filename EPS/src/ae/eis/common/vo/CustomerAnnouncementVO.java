/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Mohammed Kabeel  05/06/2013  - File created.
 */

package ae.eis.common.vo;

import ae.eis.util.vo.ValueObject;
import java.util.Date;

/**
 * Customer Announcement value object.
 *
 * @author Eng. Mohammed Kabeel
 * @version 1.00
 */
public class CustomerAnnouncementVO extends ValueObject {

    /*
     * Constants
     */
    
    /** Status Domain */ 
    public static final String STATUS_DOMAIN = "TF_ENS_STATUS";
    
    /** Status Values */
    public static final Integer STATUS_NEW = new Integer(1);
    public static final Integer STATUS_PUBLISHED = new Integer(2);
    public static final Integer STATUS_CANCELLED = new Integer(3);
    
    /*
     * Instance Variables
     */
    
    /** Announcement Language Code */
    private String languageCode;
    
    /** Announcement Title */
    private String title;
    
    /** Announcement Message */
    private String message;
    
    /** Announcement Expiry Date */
    private Date expiryDate; 
    
    /** Announcement Status */
    private Integer status;
    
    /** Arabic Status Description */
    private String statusDescriptionAr;
    
    /** English Status Description */
    private String statusDescriptionEn;
    
    /** Announcement Status Date */
    private Date statusDate;
    
    /** Remarks */
    private String remarks;
    
    /*
     * Constructors
     */

    /**
     * Default constructor.
     */
    public CustomerAnnouncementVO() {
        // Empty body
    }

    
    
    /**
     * Setter method for languageCode : Language Code
     * 
     * @param languageCode : Language Code
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Getter method for languageCode : Language Code
     * 
     * @return languageCode : Language Code
     */
    public String getLanguageCode() {
        return languageCode;
    }
    
    /**
     * Setter method for title : Announcement Title
     * 
     * @param title : Announcement Title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter method for title : Announcement Title
     * 
     * @return title : Announcement Title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter method for message : Announcement Message Body
     * 
     * @param message : Announcement Message Body
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Getter method for message : Announcement Message Body
     * 
     * @return message : Announcement Message Body
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter method for expiryDate : Announcement Expiry Date
     * 
     * @param expiryDate : Announcement Expiry Date
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * Getter method for expiryDate : Announcement Expiry Date
     * 
     * @return expiryDate : Announcement Expiry Date
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * Setter method for status : Announcement Status
     * 
     * @param status : Announcement Status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Getter method for status : Announcement Status
     * 
     * @return status : Announcement Status
     */
    public Integer getStatus() {
        return status;
    }
    
    /**
     * Setter method for statusDate : Announcement Status Date
     * 
     * @param statusDate : Announcement Status Date
     */
    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * Getter method for statusDate : Announcement Status Date
     * 
     * @return statusDate : Announcement Status Date
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * Setter method for remarks
     * 
     * @param remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * Getter method for remarks
     * 
     * @return remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Setter method for statusDescriptionAr : Arabic Status Description
     * 
     * @param statusDescriptionAr : Arabic Status Description
     */
    public void setStatusDescriptionAr(String statusDescriptionAr) {
        this.statusDescriptionAr = statusDescriptionAr;
    }

    /**
     * Getter method for statusDescriptionAr : Arabic Status Description
     * 
     * @return statusDescriptionAr : Arabic Status Description
     */
    public String getStatusDescriptionAr() {
        return statusDescriptionAr;
    }

    /**
     * Setter method for statusDescriptionEn : English Status Description
     * 
     * @param statusDescriptionEn : English Status Description
     */
    public void setStatusDescriptionEn(String statusDescriptionEn) {
        this.statusDescriptionEn = statusDescriptionEn;
    }
    
    /**
     * Getter method for statusDescriptionEn : English Status Description
     * 
     * @return statusDescriptionEn : English Status Description
     */
    public String getStatusDescriptionEn() {
        return statusDescriptionEn;
    }   
}