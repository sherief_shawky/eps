/*
 * Copyright (c) i-Soft 2007.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   10/05/2008  - File created.
 */

package ae.eis.common.vo;

import ae.eis.util.vo.ValueObject;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import javax.xml.bind.annotation.XmlElement;

/**
 * Messages Value Object
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
@XmlAccessorType(XmlAccessType.NONE)
public class MessageVO extends ValueObject {
    
    /** Language Codes */
    public static final String LANGUAGE_AR = "AR";
    public static final String LANGUAGE_EN = "EN";
    public static final String SDDI_MANUAL_MSG = "SDDI_MANUAL_MSG";
    

    /** MESSAGE HEADER */
    public static final String MESSAGE_HEADER = "TF_MSG_HEADER";
    
    /** MESSAGE HEADER ADMIN */
    public static final String MESSAGE_HEADER_ADMIN = "TF_MSG_HEADER_ADMIN";
    
    /** MESSAGE HEADER ADMIN */
    public static final String MESSAGE_HEADER_AUTH_SELLER = "TF_MSG_HEADER_AUTH_SELLER";
    
    /** MESSAGE GENERAL EMAIL */
    public static final String MESSAGE_GENERAL_EMAIL = "TF_GENERAL_EMAIL";
    
    /** MESSAGE NO TRANSLATION LAN CHOOSEN */
    public static final String MESSAGE_NO_TRANSLATION_LAN_CHOOSEN = "NO_TRANSLATION_LAN_CHOOSEN";
    
    /** Message Type */
    public static final Integer TYPE_INFO    = new Integer(1);
    public static final Integer TYPE_WARNING = new Integer(2);
    public static final Integer TYPE_ERROR   = new Integer(3);
    public static final Integer TYPE_FATAL   = new Integer(4);
    public static final Integer TYPE_TITLE   = new Integer(5);
    public static final Integer TYPE_MESSAGE = new Integer(6);
    public static final Integer TYPE_EMAIL   = new Integer(7);
    public static final Integer TYPE_FAX     = new Integer(8);
    private static final Set messageTypeSet = new HashSet();
    static {
        messageTypeSet.add(TYPE_INFO);
        messageTypeSet.add(TYPE_WARNING);
        messageTypeSet.add(TYPE_ERROR);
        messageTypeSet.add(TYPE_FATAL);
        messageTypeSet.add(TYPE_TITLE);
        messageTypeSet.add(TYPE_MESSAGE);
        messageTypeSet.add(TYPE_EMAIL);
        messageTypeSet.add(TYPE_FAX);
    }    
    
    
    /*
     * Instance Variables
     */
    /** Message Code */
    @XmlElement(name = "code")
    private String code;
    
    /** Message */
    @XmlElement(name = "message")
    private String message;
    
    /** language Code */
    @XmlElement(name = "languageCode")
    private String languageCode;
    
    /** Message Type */
    @XmlElement(name = "messageType")
    private Integer messageType;
    
    /** Message En */
    private String messageEn;
    
    
    public MessageVO(){}
    
    public MessageVO(String code){
        this.code = code;
    }
    
    
    /*
     * Accessors
     */
    /**
     * set Code
     * 
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     * get Code
     * 
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * set Message
     * 
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * get Message
     * 
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * set Language Code
     * 
     * @param languageCode
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * get Language Code
     * 
     * @return languageCode
     */
    public String getLanguageCode() {
        return languageCode;
    }
    
    /**
     * is Valid Language
     * return true if language is not equal null and equal to 'AR' ro 'EN'
     *                                                  otherwise return false
     * 
     * @return true or false
     * @param lang
     */
    public boolean isValidLanguage(String lang) {
        return (lang == null || 
                lang.equalsIgnoreCase(LANGUAGE_AR) || 
                lang.equalsIgnoreCase(LANGUAGE_EN));
    }
    
    /**
     * isArabic
     * return true if language is valid and equal Arabic otherwise return false
     * 
     * @return true or false
     */
    public boolean isArabic(){
        return languageCode != null && languageCode.equals(LANGUAGE_AR);
    }
    
    /**
     * is English
     * return true if language is valid and equal English otherwise return false
     * 
     * @return true or false
     */
    public boolean isEnglish(){
        return languageCode != null && languageCode.equals(LANGUAGE_EN);
    }

    /**
     * Set Message Type
     * 
     * @param messageType Message Type
     */
    public void setMessageType(Integer messageType) {
    
        if (messageType != null && ! messageTypeSet.contains(messageType)) {
            throw new IllegalArgumentException(new StringBuffer(
              "Invalid Message Type : ").append(messageType).toString());
        }
        
        this.messageType = messageType;
    }

    /**
     * Get Message Type
     * 
     * @return messageType Message Type
     */
    public Integer getMessageType() {
        return messageType;
    }
    
    /**
     * Is Info Message Type
     * return true if message type is not null and equal info, otherwise return false
     * 
     * @return boolean
     */
    public boolean isInfoMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_INFO);
    }
      
    /**
     * Is Warning Message Type
     * return true if message type is not null and equal warning, otherwise return false
     * 
     * @return boolean
     */
    public boolean isWarningMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_WARNING);
    }
    
    /**
     * Is Error Message Type
     * return true if message type is not null and equal error, otherwise return false
     * 
     * @return boolean
     */
    public boolean isErrorMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_ERROR);
    }
    
    /**
     * Is Fatal Message Type
     * return true if message type is not null and equal fatal, otherwise return false
     * 
     * @return boolean
     */
    public boolean isFatalMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_FATAL);
    }    

    /**
     * Is Title Message Type
     * return true if message type is not null and equal title, otherwise return false
     * 
     * @return boolean
     */
    public boolean isTitleMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_TITLE);
    }    
    
    /**
     * Is SMS Message Type
     * return true if message type is not null and equal SMS, otherwise return false
     * 
     * @return boolean
     */
    public boolean isSMSMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_MESSAGE);
    }     

    /**
     * Is Email Message Type
     * return true if message type is not null and equal email, otherwise return false
     * 
     * @return boolean
     */
    public boolean isEmailMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_EMAIL);
    } 

    /**
     * Is Fax Message Type
     * return true if message type is not null and equal fax, otherwise return false
     * 
     * @return boolean
     */
    public boolean isFaxMessageType(){
        return getMessageType() != null &&
               getMessageType().equals(TYPE_FAX);
    }

    /**
     *  Set Message En
     * @param messageEn
     */
    public void setMessageEn(String messageEn) {
        this.messageEn = messageEn;
    }

    /**
     *  Get Message En
     * @return messageEn
     */
    public String getMessageEn() {
        return messageEn;
    }

}
