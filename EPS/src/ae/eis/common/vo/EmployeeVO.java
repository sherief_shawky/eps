/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh    14/04/2008  - First version
 */

package ae.eis.common.vo;

import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

/**
 * Employee Value Object
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public class EmployeeVO extends ValueObject  {
    public enum STATUS{
        VACATION(1),
        ONDUTY(2);
        
        STATUS(int value) {
            this.value = value;
        }
        
        private int value;
        
        public int getValue() {
            return value;
        }
    }
    public enum EXMIANER_TYPE{
        BACKUP(1),PRIMARY(2),SUPERVISOR(3),ZONE_SUPERVISOR(4);
        
        EXMIANER_TYPE(int value) {
            this.value = value;
        }
        
        private int value;
        
        public int getValue() {
            return value;
        }
    }
    
    /** Period Type Enum */
    public static enum PeriodType {
        
        /** Enum. Constants. */
        MORNING(1),
        EVENING(2),
        MORNING_EVENNG(3);
        /** DOMIAN STATUS TYPES */
        public static final String MODES_DOMAIN = "TF_EMP_PERIOD_TYPE";
        
        /*
         * Fields.
         */
        
        /** Status */
        private final Integer periodType;
        
        /**
         * Enum. Constructor.
         * 
         * @param value Enum. Value.
         */
        PeriodType(Integer periodType) {
            this.periodType = periodType;
        }
        
        /**
         * Get Enum. Value.
         * 
         * @return Enum. Value.
         */
        public Integer getValue() {
            return periodType;
        } 
    }
    /*
     * Class Variables
     */
    
    /** Gender Values */
    public static final Integer ALL_GENDER = new Integer(3);
    public static final Integer GENDER_MALE = new Integer(2);
    public static final Integer GENDER_FEMALE = new Integer(1);

    /** Examiner Attendance Commitment */
    public static final Integer EXAMINER_ATTENDANCE_COMMITMENT_ALL = new Integer(3);
    
    public static final String EMPLOYEE_ID_PARAM = "empId";


    
    /** Language Values */
    public static final int LANGUAGE_ENGLISH = 1;
    public static final int LANGUAGE_ARABIC = 2;     
    
    /** Ex Types */
    public static final int PRIMARY= 1;  
    /*
     * Instance variables.
     */   
    /** Employee Name */ 
    private String employeeNameAr; 
    
    /** employee Number */
    private String employeeNo;
    
    /** Mobile Number */
    private Long mobileNo;
    
    /** Mobile Number */
    private String mobileNoAsString;
    
    /** Email */
    private String email;
    
    /** Employee Language */
    private Integer employeeLanguage;
    
    
    /** Is Examiner */
    private Choice isExaminer;
    
    
    /** Gender */
    private Integer gender;
    
    /** Gender Desc Arabic */
    private String genderDescAr;
    
    private Integer examinerStatus;
    
    private Integer examinerType;
    
    /** Examiner Type Desc */
    private String examinerTypeDesc;
    /** Examiner Type Desc En*/
    private String examinerTypeDescEn;
    
    /** Gender Desc English */
    private String genderDescEn;
    
    /** Employee Name English */ 
    private String employeeNameEn;
    
    /** Employee Picture */
    private byte[] employeePicture;

    /** Is Available Flag */    
    private Choice isAvailable;
    
    /** On Duty Flag */
    private Choice onDuty;
    
    /** emirates Id*/
    private String emiratesId;
    
    /** Employee Picture */
    private byte[] picture;
    
    /** Mime Type Extension */
    private String mtpExtension;
    
    /** Excluded Employees Ids separeted by comma */
    private String excludedEmployeesIds;
    /** Exam Fail Canidiate Count */
    private Double examFailCanidiatePer;
    
    /** Exam Success Canidiate Count */
    private Double examSuccessCanidiatePer;
    
    /** Candidate Count */ 
    private Integer candidateCount;
    
    /** Productivity */
    private Double productivity;
    
    /** Productivity With Center */
    private Double productivityWithCenter;
    
    /** Total Exam Period Per */
    private Double totalExamPeriodPer;
    
    /** Total Female Number */
    private Integer totalFemaleNumber;
    
    /** Total Male Number */
    private Integer totalMaleNumber;
    
    /** Picture Exist */
    private boolean pictureExist;
    
    /** Status */
    private Integer status;
    
    /** Status Description En */
    private String statusDescriptionEn;
    
    /** Status Description Ar */
    private String statusDescriptionAr;
    
    /** Type Description En */
    private String typeDescriptionEn;
    
    /** Type Description Ar */
    private String typeDescriptionAr;
    
    /** Show Red Light */
    private boolean showRedLight;   
    
    /** Show Green Light */
    private boolean showGreenLight;   
    
    /** Show Orange Light */
    private boolean showOrangeLight; 
    
    private Integer noOfRotation;
    
    private TrafficFileVO trafficFile;
    
    private Long userId;
    
    private String userName;
    
    /** Examination Period Type */
    private Integer examinationPeriodType;
    
    /** Examination Period Type Arabic */
    private String examinationPeriodTypeDescAr;
    
    /** Examination Period Type english */
    private String examinationPeriodTypeDescEn;
    
    
    
    /** Employee Leavs Dates List */
    private List<Date> employeeLeavsDatesList;
    
    /** dtt Role Group Description Ar */
    private String dttRoleGroupDescriptionAr;
    
    /** Dtt Role Group Description En */
    private String dttRoleGroupDescriptionEn;

    
    /** Employee Type */
    private Integer employeeType;
    
    /** Can Collect Money */
    private Choice canCollectMoney;
    
    /** Is Active MMS */
    private Choice isActiveMms;
    
    /** Is Internet */
    private Choice isInternet;
     
    /** Is KIOSK */
    private Choice isKiosk;
    
    /** Is IVR */
    private Choice isIvr;
    
    /** Grade */
    private Integer grade;
    
    
    /** Exam Allowed Category Desc Ar.*/
    private String examAllowedCategoryDescAr;
    
    /** Exam Allowed Category. Desc En*/
    private String examAllowedCategoryDescEn;
    
    /** Exam Allowed Category.*/
    private Integer examAllowedCategory;
    
    /*
     * Default Constructor
     */
    public EmployeeVO() {
    }

    /**
     * Constructor with Id
     * @param id
     */
    public EmployeeVO(Long id){
      setId(id);
    }
    /**
     * Returns String represents the values of properties of the current object. 
     * 
     * @return String represents the values of properties of the current object. 
     */
    public String toString() {
        StringBuffer buf = new StringBuffer(getClass().getName());
        buf.append("{\n").append(super.toString())
           .append(",employee name=").append(getEmployeeNameAr())
           .append(",employee number=").append(getEmployeeNo())
           .append(",mobile number=").append(getMobileNo())
           .append(",email=").append(getEmail())
           .append(",employeeLanguage=").append(getEmployeeLanguage())
           .append(",excludedEmployeesIds=").append(getExcludedEmployeesIds());
        return buf.toString();
    }    

    /*
     * Accessors
     */

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof EmployeeVO)) {
            return false;
        }
        final EmployeeVO other = (EmployeeVO) object;
        if (!(getId() == null ? other.getId() == null : getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int PRIME = 37;
        int result = super.hashCode();
        result = PRIME * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    /**
     * set Employee Name Arabic
     *
     * @param employeeNameAr ( Employee Name Arabic )
     */
    public void setEmployeeNameAr(String employeeNameAr) {
        this.employeeNameAr = employeeNameAr;
    }

    /**
     * get Employee Name Arabic
     * 
     * @return employeeNameAr ( Employee Name Arabic )
     */
    public String getEmployeeNameAr() {
        return employeeNameAr;
    }

    /**
     * set Employee Number
     * 
     * @param employeeNo ( Employee Number )
     */
    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    /**
     * get Employee Number
     * 
     * @return employeeNo ( Employee Number )
     */
    public String getEmployeeNo() {
        return employeeNo;
    }

    /**
     * Set Mobile number
     * 
     * @param mobileNo Mobile number
     */
    public void setMobileNo(Long mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * Get Mobile number
     * 
     * @return Mobile number
     */
    public Long getMobileNo() {
        return mobileNo;
    }

    /**
     * Set Email
     * 
     * @param email Email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get Email
     * 
     * @return Email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set Employee language
     * 
     * @param employeeLanguage Employee language
     */
    public void setEmployeeLanguage(Integer employeeLanguage) {
        if(employeeLanguage == null) {
            
            throw new IllegalArgumentException("Employee Language cannot be null");
        }
        
        switch (employeeLanguage.intValue())  {
            case LANGUAGE_ENGLISH:
            case LANGUAGE_ARABIC:
                this.employeeLanguage = employeeLanguage;
                break;
        
            default:
                throw new IllegalArgumentException("Invalid language value, language = "+employeeLanguage);
        }
    }

    /**
     * Get Employee language
     * 
     * @return Employee language
     */
    public Integer getEmployeeLanguage() {
        return employeeLanguage;
    }
    
    /**
     * Check if the language is arabic or not
     * 
     * @return true if yes
     */
    public boolean isArabicLanguage() {
        return getEmployeeLanguage()!=null && getEmployeeLanguage().intValue() == LANGUAGE_ARABIC;
    }

    /**
     * Check if the language is english or not
     * 
     * @return true if yes
     */
    public boolean isEnglishLanguage() {
        return getEmployeeLanguage()!=null && getEmployeeLanguage().intValue() == LANGUAGE_ENGLISH;
    }    



    /**
     * Setter for Is Examiner
     * 
     * @param isExaminer Is Examiner
     */
    public void setIsExaminer(Choice isExaminer) {
        this.isExaminer = isExaminer;
    }

    /**
     * Getter for Is Examiner
     * 
     * @return True if the employee is examiner
     *         False Otherwise
     */
    public Choice getIsExaminer() {
        return isExaminer;
    }



    /**
     * Setter For Gender 
     * 
     * @param gender Gender 
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * Getter For Gender 
     * 
     * @return gender Gender 
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * Setter For Gender Desc Arabic
     * 
     * @param genderDescAr Gender Desc Arabic
     */
    public void setGenderDescAr(String genderDescAr) {
        this.genderDescAr = genderDescAr;
    }

    /**
     * Getter For Gender Desc Arabic
     * 
     * @return genderDescAr Gender Desc Arabic
     */
    public String getGenderDescAr() {
        return genderDescAr;
    }

    /**
     * Setter For Gender Desc English
     * 
     * @param genderDescEn Gender Desc English
     */
    public void setGenderDescEn(String genderDescEn) {
        this.genderDescEn = genderDescEn;
    }

    /**
     * Getter For Gender Desc English
     * 
     * @return genderDescEn Gender Desc English
     */
    public String getGenderDescEn() {
        return genderDescEn;
    }

    /**
     * Setter For Employee Name English
     * 
     * @param employeeNameEn Employee Name English
     */
    public void setEmployeeNameEn(String employeeNameEn) {
        this.employeeNameEn = employeeNameEn;
    }

    /**
     * Getter For Employee Name English
     * 
     * @return employeeNameEn Employee Name English
     */
    public String getEmployeeNameEn() {
        return employeeNameEn;
    }

    /**
     * Setter For Employee Picture
     * 
     * @param employeePicture Employee Picture
     */
    public void setEmployeePicture(byte[] employeePicture) {
        this.employeePicture = employeePicture;
    }

    /**
     * Getter For Employee Picture
     * 
     * @return employeePicture Employee Picture
     */
    public byte[] getEmployeePicture() {
        return employeePicture;
    }


    /**
     * Set is available
     * 
     * @param isAvailable
     */
    public void setIsAvailable(Choice isAvailable) {
        this.isAvailable = isAvailable;
    }


    /**
     * Get is available
     * 
     * @return 
     */
    public Choice getIsAvailable() {
        return isAvailable;
    }


    /**
     * Set on duty
     * 
     * @param onDuty
     */
    public void setOnDuty(Choice onDuty) {
        this.onDuty = onDuty;
    }


    /**
     * Get on duty
     * 
     * @return 
     */
    public Choice getOnDuty() {
        return onDuty;
    }

    /**
     * Setter For emiratesId
     * 
     * @param emiratesId : emirates Id.
     */
    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }

    /**
     * Get emiratesId
     * 
     * @return emirates Id.
     */
    public String getEmiratesId() {
        return emiratesId;
    }


    /**
     * Set employee picture.
     * 
     * @param picture
     */
    public void setPicture(byte[] picture) {
        this.picture = picture;
    }
  
  
    /**
     * Get employee picture.
     * 
     * @return 
     */
    public byte[] getPicture() {
        return picture;
    }


    /**
     * Set mime type extension value.
     * 
     * @param mtpExtension
     */
    public void setMtpExtension(String mtpExtension) {
        this.mtpExtension = mtpExtension;
    }


    /**
     * Get mime type extension value.
     * 
     * @return String
     */
    public String getMtpExtension() {
        return mtpExtension;
    }

    /**
     * Set Excluded Employees Ids separeted by comma
     * @param excludedEmployeesIds
     */
    public void setExcludedEmployeesIds(String excludedEmployeesIds)
    {
        this.excludedEmployeesIds = excludedEmployeesIds;
    }

    /**
     * Get Excluded Employees Ids separeted by comma
     * @return 
     */
    public String getExcludedEmployeesIds()
    {
        return excludedEmployeesIds;
    }
    

    /**
     * Setter for Exam Fail Canidiate Per
     * 
     * @param examFailCanidiatePer Exam Fail Canidiate Per
     */
    public void setExamFailCanidiatePer(Double examFailCanidiatePer) {
        this.examFailCanidiatePer = examFailCanidiatePer;
    }

    /**
     * Getter for Exam Fail Canidiate Per
     * 
     * @return examFailCanidiatePer Exam Fail Canidiate Per
     */
    public Double getExamFailCanidiatePer() {
        return examFailCanidiatePer;
    }

    /**
     * Setter for Exam Success Canidiate Per
     * 
     * @param examSuccessCanidiatePer Exam Success Canidiate Per
     */
    public void setExamSuccessCanidiatePer(Double examSuccessCanidiatePer) {
        this.examSuccessCanidiatePer = examSuccessCanidiatePer;
    }

    /**
     * Getter for Exam Success Canidiate Per
     * 
     * @return examSuccessCanidiatePer Exam Success Canidiate Per
     */
    public Double getExamSuccessCanidiatePer() {
        return examSuccessCanidiatePer;
    }

    /**
     * Setter for Candidate Count
     * 
     * @param candidateCount Candidate Count
     */
    public void setCandidateCount(Integer candidateCount) {
        this.candidateCount = candidateCount;
    }

    /**
     * Getter for Candidate Count
     * 
     * @return candidateCount Candidate Count
     */
    public Integer getCandidateCount() {
        return candidateCount;
    }

    /**
     * Setter for Productivity
     * 
     * @param productivity Productivity
     */
    public void setProductivity(Double productivity) {
        this.productivity = productivity;
    }

    /**
     * Getter for Productivity
     * 
     * @return productivity Productivity
     */
    public Double getProductivity() {
        return productivity;
    }

    /**
     * Setter for Productivity With Center
     * 
     * @param productivityWithCenter Productivity With Center
     */
    public void setProductivityWithCenter(Double productivityWithCenter) {
        this.productivityWithCenter = productivityWithCenter;
    }

    /**
     * Getter for Productivity With Center
     * 
     * @return productivityWithCenter Productivity With Center
     */
    public Double getProductivityWithCenter() {
        return productivityWithCenter;
    }

    /**
     * Setter for Total Exam Period Per
     * 
     * @param totalExamPeriodPer Total Exam Period Per
     */
    public void setTotalExamPeriodPer(Double totalExamPeriodPer) {
        this.totalExamPeriodPer = totalExamPeriodPer;
    }

    /**
     * Getter for Total Exam Period Per
     * 
     * @return totalExamPeriodPer Total Exam Period Per
     */
    public Double getTotalExamPeriodPer() {
        return totalExamPeriodPer;
    }

    public void setTotalFemaleNumber(Integer totalFemaleNumber) {
        this.totalFemaleNumber = totalFemaleNumber;
    }

    public Integer getTotalFemaleNumber() {
        return totalFemaleNumber;
    }

    public void setTotalMaleNumber(Integer totalMaleNumber) {
        this.totalMaleNumber = totalMaleNumber;
    }

    public Integer getTotalMaleNumber() {
        return totalMaleNumber;
    }

    public void setPictureExist(boolean pictureExist) {
        this.pictureExist = pictureExist;
    }

    public boolean isPictureExist() {
        return pictureExist;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setShowRedLight(boolean showRedLight) {
        this.showRedLight = showRedLight;
    }

    public boolean isShowRedLight() {
        return showRedLight;
    }

    public void setShowGreenLight(boolean showGreenLight) { 
        this.showGreenLight = showGreenLight;
    }

    public boolean isShowGreenLight() {
        return showGreenLight;
    }


    public void setStatusDescriptionEn(String statusDescriptionEn) {
        this.statusDescriptionEn = statusDescriptionEn;
    }

    public String getStatusDescriptionEn() {
        return statusDescriptionEn;
    }

    public void setStatusDescriptionAr(String statusDescriptionAr) {
        this.statusDescriptionAr = statusDescriptionAr;
    }

    public String getStatusDescriptionAr() {
        return statusDescriptionAr;
    }

    public void setTypeDescriptionEn(String typeDescriptionEn) {
        this.typeDescriptionEn = typeDescriptionEn;
    }

    public String getTypeDescriptionEn() {
        return typeDescriptionEn;
    }

    public void setTypeDescriptionAr(String typeDescriptionAr) {
        this.typeDescriptionAr = typeDescriptionAr;
    }

    public String getTypeDescriptionAr() {
        return typeDescriptionAr;
    }

    public void setExaminerStatus(Integer examinerStatus) {
        this.examinerStatus = examinerStatus;
    }

    public Integer getExaminerStatus() {
        return examinerStatus;
    }

    public void setExaminerType(Integer examinerType) {
        this.examinerType = examinerType;
    }

    public Integer getExaminerType() {
        return examinerType;
    }


    public void setMobileNoAsString(String mobileNoAsString) {
        this.mobileNoAsString = mobileNoAsString;
    }

    public String getMobileNoAsString() {
        return mobileNoAsString;
    }
    public boolean isExaminerTypePrimry() {
        return getExaminerType() != null &&
               getExaminerType().intValue() == (PRIMARY);
    }
    public boolean isExminerInOnDuty() { 
        return getStatus() != null && 
                getStatus().intValue() == STATUS.ONDUTY.getValue();
    }


    public void setNoOfRotation(Integer noOfRotation) {
        this.noOfRotation = noOfRotation;
    }

    public Integer getNoOfRotation() {
        return noOfRotation;
    }

    public void setShowOrangeLight(boolean showOrangeLight) {
        this.showOrangeLight = showOrangeLight;
    }

    public boolean isShowOrangeLight() {
        return showOrangeLight;
    }

    public void setTrafficFile(TrafficFileVO trafficFile) {
        this.trafficFile = trafficFile;
    }

    public TrafficFileVO getTrafficFile() {
        return trafficFile;
    }
    public boolean isExaminerTypeSupervisor() {
        return examinerType != null && 
               examinerType.intValue() == EXMIANER_TYPE.SUPERVISOR.getValue(); 
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
    
    /**
     * Sets Examination Period Type.
     * 
     * @param examinationPeriodType : examination Period Type.
     */
    public void setExaminationPeriodType(Integer examinationPeriodType) {
        this.examinationPeriodType = examinationPeriodType;
    }
    
    /**
     * Gets Examination Period Type.
     * 
     * @return examination Period Type.
     */
    public Integer getExaminationPeriodType() {
        return examinationPeriodType;
    }

    
    
    /**
     * Sets examination Period Type Description Arabic.
     * 
     * @param examinationPeriodTypeDescAr : examination Period Type Description Arabic.
     */
    public void setExaminationPeriodTypeDescAr(String examinationPeriodTypeDescAr) {
        this.examinationPeriodTypeDescAr = examinationPeriodTypeDescAr;
    }
    
    /**
     * Gets examination Period Type Description Arabic.
     * 
     * @return String.
     */
    public String getExaminationPeriodTypeDescAr() {
        return examinationPeriodTypeDescAr;
    }
    
    /**
     * Sets examination Period Type Description english.
     * 
     * @param examinationPeriodTypeDescEn : examination Period Type Description english.
     */
    public void setExaminationPeriodTypeDescEn(String examinationPeriodTypeDescEn) {
        this.examinationPeriodTypeDescEn = examinationPeriodTypeDescEn;
    }
    
    /**
     * Gets examination Period Type Description english.
     * 
     * @return String.
     */
    public String getExaminationPeriodTypeDescEn() {
        return examinationPeriodTypeDescEn;
    }
    
    
    
    /**
     * Sets employee Leavs Dates List.
     * 
     * @param employeeLeavsDatesList : List of Date.
     */
    public void setEmployeeLeavsDatesList(List<Date> employeeLeavsDatesList) {
        this.employeeLeavsDatesList = employeeLeavsDatesList;
    }
    
    /**
     * Gets employee Leavs Dates List.
     * 
     * @return List of Date.
     */
    public List<Date> getEmployeeLeavsDatesList() {
        return employeeLeavsDatesList;
    }

    /**
     * Sets dtt Role Group Description arabic.
     * 
     * @param dttRoleGroupDescriptionAr : String.
     */
    public void setDttRoleGroupDescriptionAr(String dttRoleGroupDescriptionAr) {
        this.dttRoleGroupDescriptionAr = dttRoleGroupDescriptionAr;
    }
    
    /**
     * Gets dtt Role Group Description arabic.
     * 
     * @return String.
     */
    public String getDttRoleGroupDescriptionAr() {
        return dttRoleGroupDescriptionAr;
    }
    
    /**
     * Sets dtt Role Group Description English.
     * 
     * @param dttRoleGroupDescriptionEn : String.
     */
    public void setDttRoleGroupDescriptionEn(String dttRoleGroupDescriptionEn) {
        this.dttRoleGroupDescriptionEn = dttRoleGroupDescriptionEn;
    }
    
    

    /**
     * Set employee type
     * 
     * @param employeeType Integer
     */
    public void setEmployeeType(Integer employeeType) {
        this.employeeType = employeeType;
    }

    /**
     * Get employee type
     * 
     * @return employeeType Integer
     */
    public Integer getEmployeeType() {
        return employeeType;
    }

    /**
     * Set can collect money
     * 
     * @param canCollectMoney Choice
     */
    public void setCanCollectMoney(Choice canCollectMoney) {
        this.canCollectMoney = canCollectMoney;
    }

    /**
     * Get can collect money
     * 
     * @return canCollectMoney Choice
     */
    public Choice getCanCollectMoney() {
        return canCollectMoney;
    }

    /**
     * Set is active mms
     * 
     * @param isActiveMms Choice
     */
    public void setIsActiveMms(Choice isActiveMms) {
        this.isActiveMms = isActiveMms;
    }

    /**
     * Get is active mms
     * 
     * @return isActiveMms Choice
     */
    public Choice getIsActiveMms() {
        return isActiveMms;
    }

    /**
     * Set is internet
     * 
     * @param isInternet Choice
     */
    public void setIsInternet(Choice isInternet) {
        this.isInternet = isInternet;
    }

    /**
     * Get is internet
     * 
     * @return isInternet Choice
     */
    public Choice getIsInternet() {
        return isInternet;
    }

    /**
     * Set is kiosk
     * 
     * @param isKiosk Choice
     */
    public void setIsKiosk(Choice isKiosk) {
        this.isKiosk = isKiosk;
    }

    /**
     * Get is kiosk
     * 
     * @return isKiosk Choice
     */
    public Choice getIsKiosk() {
        return isKiosk;
    }

    /**
     * Set is ivr
     * 
     * @param isIvr Choice
     */
    public void setIsIvr(Choice isIvr) {
        this.isIvr = isIvr;
    }

    /**
     * Get is ivr
     * 
     * @return isIvr Choice
     */
    public Choice getIsIvr() {
        return isIvr;
    }

    /** 
     * Setter for Grade
     *
     * @param grade Grade
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * Getter for Grade
     *
     * @return grade Grade
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * Setter Examiner Type Desc 
     * 
     * @param ExaminerTypeDesc
     */
    public void setExaminerTypeDesc(String examinerTypeDesc) {
        this.examinerTypeDesc = examinerTypeDesc;
    }
    /**
     * Get Examiner Type Desc 
     * 
     * @return ExaminerTypeDesc
     */
    public String getExaminerTypeDesc() {
        return examinerTypeDesc;
    }
    /**
     * Setter Examiner Type Desc En
     * 
     * @param ExaminerTypeDescEn
     */
    public void setExaminerTypeDescEn(String examinerTypeDescEn) {
        this.examinerTypeDescEn = examinerTypeDescEn;
    }
    /**
     * Get Examiner Type Desc En
     * 
     * @return ExaminerTypeDescEn
     */
    public String getExaminerTypeDescEn() {
        return examinerTypeDescEn;
    }



    /**
     * Setter for ExamAllowedCategoryDescAr
     *
     * @param ExamAllowedCategoryDescAr
     */
    public void setExamAllowedCategoryDescAr(String examAllowedCategoryDescAr) {
        this.examAllowedCategoryDescAr = examAllowedCategoryDescAr;
    }
    /**
     * Getter for ExamAllowedCategoryDescAr
     *
     * @return ExamAllowedCategoryDescAr
     */
    public String getExamAllowedCategoryDescAr() {
        return examAllowedCategoryDescAr;
    }
    /**
     * Setter for ExamAllowedCategoryDescEn
     *
     * @param ExamAllowedCategoryDescEn
     */
    public void setExamAllowedCategoryDescEn(String examAllowedCategoryDescEn) {
        this.examAllowedCategoryDescEn = examAllowedCategoryDescEn;
    }
    /**
     * Getter for ExamAllowedCategoryDescEn
     *
     * @return ExamAllowedCategoryDescEn
     */
    public String getExamAllowedCategoryDescEn() {
        return examAllowedCategoryDescEn;
    }

    /**
     * Setter for ExamAllowedCategory
     *
     * @param ExamAllowedCategory
     */
    public void setExamAllowedCategory(Integer examAllowedCategory) {
        this.examAllowedCategory = examAllowedCategory;
    }
    /**
     * Getter for ExamAllowedCategory
     *
     * @return ExamAllowedCategory
     */
    public Integer getExamAllowedCategory() {
        return examAllowedCategory;
    }
}
