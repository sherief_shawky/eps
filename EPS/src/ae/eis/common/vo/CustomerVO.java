/*
 * Copyright (c) i-Soft 2008.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Bassem R. Zohdy    05/02/2008  - first version.
 *
 * 1.01  Hamzeh Abu Lawi    03/03/2008  - add isTransferableTrafficFile
 *
 * 1.02  Waseem N.Zawaideh  17/03/2008  - Add two attributes orgId and prsId
 *                                          with their accessors and mutators
 *
 * 1.03  Mahmoud Atiyeh     21/04/2013  - added vehicleVO property.
 *
 */

package ae.eis.common.vo;


import ae.eis.eps.vo.ProcedureVO;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

public class CustomerVO extends ValueObject {

   

    /** Person ID. */
    private Long prsId;

    /** Organization ID. */
    private Long orgId;

    /** Traffic file emicode */
    private String emiCode;

 /** Arabic Owner Name */
    @XmlElement(name = "ownerNameAr")
    private String ownerNameAr;

    /** Prefered Shipment Email */
    private String preferedShipmentEmail;

    /** Prefered Shipment Phone */
    private String preferedShipmentPhone;

    /** Prefered Shipment Mobile */
    private String preferedShipmentMobile;

    /** Prefered First Address */
    private String preferedFirstAddress;

    /** Prefered Second Address */
    private String preferedSecondAddress;

    /** Prefered Shipment Name */
    private String preferedShipmentName;

    /** English Prefered Shipment Name */
    private String preferedShipmentNameEn;

    /** Prefered Emiarte */
    private String preferedEmirate;

    /** Prefered Fedex Location */
    private String preferedFedexLocation;



    /** preferred Phone */
    private String preferredPhone;

    /** preferred PO Box */
    private String preferredPOBox;

    /** po Box City Code */
    private String poBoxCityCode;

    /** is Mail delivery */
    private Choice isMailDelivery;


    /** Owner Type */
    private String ownerType;


    /** Send Email Notification */
    private Choice sendEMail;

    /** Owner Email */
    private String ownerEmail;

    /** Owned Vehicles Count*/
    private Integer ownedVehiclesCount;

    /** traffic File Type*/
    private Integer trafficFileType;

    /** owner Name En */
    @XmlElement(name = "ownerNameEn")
    private String ownerNameEn;

    /** Owner Id */
    private Long ownerId;

    /** User Language */
    private String userLanguage;

    /*
     * Constructors
     * */

    /**
     * Create traffic file value object.
     * @since 1.0
     */
    public CustomerVO() {
    }

  

    public void setOwnerNameAr(String ownerNameAr) {
        this.ownerNameAr = ownerNameAr;
    }


    public String getOwnerNameAr() {
        return ownerNameAr;
    }

  

    

    /**
     * Set person ID
     *
     * @param prsId
     */
    public void setPrsId(Long prsId) {
        this.prsId = prsId;
    }

    /**
     * Get person ID
     *
     * @return person ID
     */
    public Long getPrsId() {
        return prsId;
    }

    /**
     * Set organization ID
     *
     * @param orgId
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * Get organization ID
     *
     * @return organization ID
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * Set emirate code
     *
     * @param emiCode
     */
    public void setEmiCode(String emiCode) {
        this.emiCode = emiCode;
    }

    /**
     * Get emirate code
     *
     * @return emicode
     */
    public String getEmiCode() {
        return emiCode;
    }

    /**
     * Set Prefered Shipment Email
     *
     * @param preferedShipmentEmail Prefered Shipment Email
     */
    public void setPreferedShipmentEmail(String preferedShipmentEmail) {
        this.preferedShipmentEmail = preferedShipmentEmail;
    }

    /**
     * Get Prefered Shipment Email
     *
     * @return preferedShipmentEmail
     */
    public String getPreferedShipmentEmail() {
        return preferedShipmentEmail;
    }

    /**
     * Set Prefered Shipment Mobile
     *
     * @param preferedShipmentMobile Prefered Shipment Mobile
     */
    public void setPreferedShipmentMobile(String preferedShipmentMobile) {
        if (preferedShipmentMobile != null) {
            this.preferedShipmentMobile = preferedShipmentMobile.trim();
        } else {
            this.preferedShipmentMobile = preferedShipmentMobile;
        }
    }

    /**
     * Get Prefered Shipment Mobile
     *
     * @return preferedShipmentMobile Prefered Shipment Mobile
     */
    public String getPreferedShipmentMobile() {
        return preferedShipmentMobile;
    }

  

    /**
     * Setter For Prefered Shipment Phone
     *
     * @param preferedShipmentPhone Prefered Shipment Phone
     */
    public void setPreferedShipmentPhone(String preferedShipmentPhone) {
        this.preferedShipmentPhone = preferedShipmentPhone;
    }

    /**
     * Getter Fior Prefered Shipment Phone
     *
     * @return Prefered Shipment Phone
     */
    public String getPreferedShipmentPhone() {
        return preferedShipmentPhone;
    }

    /**
     * Setter For Prefered First Address
     *
     * @param preferedFirstAddress Prefered First Address
     */
    public void setPreferedFirstAddress(String preferedFirstAddress) {
        this.preferedFirstAddress = preferedFirstAddress;
    }

    /**
     * Getter For Prefered First Address
     *
     * @return Prefered First Address
     */
    public String getPreferedFirstAddress() {
        return preferedFirstAddress;
    }

    /**
     * Sette For Prefered Second Address
     *
     * @param preferedSecondAddress Prefered Second Address
     */
    public void setPreferedSecondAddress(String preferedSecondAddress) {
        this.preferedSecondAddress = preferedSecondAddress;
    }

    /**
     * Getter For Prefered Second Address
     *
     * @return Prefered Second Address
     */
    public String getPreferedSecondAddress() {
        return preferedSecondAddress;
    }

    /**
     * Setter For Prefered Emirate
     *
     * @param preferedEmirate Prefered Emirate
     */
    public void setPreferedEmirate(String preferedEmirate) {
        this.preferedEmirate = preferedEmirate;
    }

    /**
     * Getter For Prefered Emirate
     *
     * @return Prefered Emirate
     */
    public String getPreferedEmirate() {
        return preferedEmirate;
    }

    /**
     * Setter For Prefered Fedex Location
     *
     * @param preferedFedexLocation Prefered Fedex Location
     */
    public void setPreferedFedexLocation(String preferedFedexLocation) {
        this.preferedFedexLocation = preferedFedexLocation;
    }

    /**
     * Getter For Prefered Fedex Location
     *
     * @return Prefered Fedex Location
     */
    public String getPreferedFedexLocation() {
        return preferedFedexLocation;
    }


   
                                 
     

    /**
     * Set English Prefered Shipment Name.
     *
     * @param preferedShipmentNameEn English Prefered Shipment Name
     */
    public void setPreferedShipmentNameEn(String preferedShipmentNameEn) {
        this.preferedShipmentNameEn = preferedShipmentNameEn;
    }

    /**
     * Get English Prefered Shipment Name.
     *
     * @return English Prefered Shipment Name.
     */
    public String getPreferedShipmentNameEn() {
        return preferedShipmentNameEn;
    }

    /**
     * Set preferredPhone.
     *
     * @param preferredPhone :  preferred Phone
     */
    public void setPreferredPhone(String preferredPhone) {
        this.preferredPhone = preferredPhone;
    }

    /**
     * Get preferredPhone.
     *
     * @return preferredPhone : preferred Phone.
     */
    public String getPreferredPhone() {
        return preferredPhone;
    }

    /**
     * Set preferredPOBox.
     *
     * @param preferredPOBox :  preferred PO Box
     */
    public void setPreferredPOBox(String preferredPOBox) {
        this.preferredPOBox = preferredPOBox;
    }

    /**
     * Get preferredPOBox.
     *
     * @return preferredPOBox : preferred PO Box.
     */
    public String getPreferredPOBox() {
        return preferredPOBox;
    }

    /**
     * Set poBoxCityCode.
     *
     * @param poBoxCityCode :  po Box City Code
     */
    public void setPoBoxCityCode(String poBoxCityCode) {
        this.poBoxCityCode = poBoxCityCode;
    }

    /**
     * Get poBoxCityCode.
     *
     * @return poBoxCityCode : po Box City Code.
     */
    public String getPoBoxCityCode() {
        return poBoxCityCode;
    }

    /**
     * Set Maildelivery.
     *
     * @param Maildelivery :  Mail delivery.
     */
    public void setIsMailDelivery(Choice isMailDelivery) {
        this.isMailDelivery = isMailDelivery;
    }

    /**
     * Get isMaildelivery.
     *
     * @return isMaildelivery :isMaildelivery.
     */
    public Choice getIsMailDelivery() {
        return isMailDelivery;
    }

    
   

    /**
     * Set Owner Tpye
     *
     * @param ownerType Owner Tpye
     */
    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    /**
     * Get Owner Tpye
     *
     * @return Owner Tpye
     */
    public String getOwnerType() {
        return ownerType;
    }

    /**
     * Set Send E-Mail
     *
     * @param sendEMail Send E-Mail
     */
    public void setSendEMail(Choice sendEMail) {
        this.sendEMail = sendEMail;
    }

    /**
     * Get Send E-Mail
     *
     * @return Send E-Mail
     */
    public Choice getSendEMail() {
        return sendEMail;
    }

    /**
     * True if Email notification is true
     *
     * @return if Email notification is true
     */
    public boolean isNotificationTypeSendEMail() {

        return (sendEMail != null && sendEMail.equals(Choice.YES));
    }

   

    /**
     * Set Owned Vehicles Count
     *
     * @param ownedVehiclesCount : Owned Vehicles Count
     */
    public void setOwnedVehiclesCount(Integer ownedVehiclesCount) {
        this.ownedVehiclesCount = ownedVehiclesCount;
    }

    /**
     * Get Owned Vehicles Count
     *
     * @return ownedVehiclesCount : Owned Vehicles Count
     */
    public Integer getOwnedVehiclesCount() {
        return ownedVehiclesCount;
    }

    /**
     * Set trafficFileType
     *
     * @param trafficFileType : traffic File Type
     */
    public void setTrafficFileType(Integer trafficFileType) {
        this.trafficFileType = trafficFileType;
    }

    /**
     * Get trafficFileType
     *
     * @return trafficFileType : traffic File Type.
     */
    public Integer getTrafficFileType() {
        return trafficFileType;
    }

    /**
     * Set ownerNameEn
     *
     * @param ownerNameEn : owner Name En.
     */
    public void setOwnerNameEn(String ownerNameEn) {
        this.ownerNameEn = ownerNameEn;
    }

    /**
     * Get ownerNameEn
     *
     * @return ownerNameEn : owner Name En.
     */
    public String getOwnerNameEn() {
        return ownerNameEn;
    }

    /**
     * Set Owner Id
     *
     * @param ownerId
     */
    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * Get Owner Id
     *
     * @return
     */
    public Long getOwnerId() {
        return ownerId;
    }

    /**
     * Setter for Owner Email
     *
     * @param ownerEmail : Owner Email
     */
    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    /**
     * Getter for Owner Email
     *
     * @return Owner Email
     */
    public String getOwnerEmail() {
        return ownerEmail;
    }

    /**
     * Setter for User Language
     *
     * @param userLanguage : User Language
     */
    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    /**
     * Getter for User Language
     *
     * @return User Language
     */
    public String getUserLanguage() {
        return userLanguage;
    }

    

    public void setPreferedShipmentMobile1(String preferedShipmentMobile) {
        this.preferedShipmentMobile = preferedShipmentMobile;
    }
}
