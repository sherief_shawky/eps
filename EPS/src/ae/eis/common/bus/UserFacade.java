package ae.eis.common.bus;

import ae.eis.common.vo.UserVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import isoft.com.util.UserProfile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface UserFacade {


    /**
     * Get account info by username.
     *
     * @param username Username.
     * @return User value object.
     */
    UserVO getByUserId(Long userId);

  
}
