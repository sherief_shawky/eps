/*
 * Copyright (c) i-Soft 2007.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00 Bassem R. Zohdy     05/02/2008  - Add isBlackListed(Long trfID).
 * 
 * 1.01 Hamzeh Abu Lawi     27/02/2008  - Add getPersonTrafficFile
 * 
 * 1.02 Waseem N.Zawaideh   22/03/2008  - Add generateTrafficNoForOrg
 * 
 * 1.03 Rami Nassar         18/07/2012  - Add method getByClassicPlateNo.
 * 
 * 1.04 Sami Abudayeh       28/09/2012  - Adding getOwnerInfoByTrfNo(Long trafficFileNumber) 
 *                                      - Adding getOwnerInfoByLicenseInfo(Long licenseNumber)
 *                                      - Adding getOwnerInfoByTicketNo(Long ticketNo)
 *                                      - Adding getOwnerInfoByPlateInfo(ae.rta.vhl.vo.PlateVO plateVO)
 *                                      - Adding getOwnerInfoByCircularTicketInfo(FineVO fine)
 * 
 * 1.05  Rami Nassar        15/10/2012  - TRF-8578
 * 
 * 1.06 Mahmoud Atiyeh      20/04/2013  - Added isRelatedToTransaction(Long trafficNo, Long transactionId)
 */
package ae.eis.common.bus;
 
import ae.eis.common.dao.TrafficFileDAO;
import ae.eis.common.vo.TrafficFileVO; 

import ae.eis.eps.vo.ProcedureVO;

import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.PreferencesFacade;
import ae.eis.util.bus.PreferencesHandler;
import ae.eis.util.bus.RuleException;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.BusinessRuleVO;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile;

import isoft.com.util.Profile;

import java.sql.Connection;


import java.util.HashMap;
import java.util.List;
 
/**
 * Traffic File Business Handler
 *
 * @auther Bassem R. Zohdy
 * @vesion 1.04
 */
public class TrafficFileHandler extends BusinessObject {

    /*
     * Business Objects
     */
    

    
    
    /*
     * Methods
     */

    /**
     * Get Traffic File By Id
     * 
     * @param id : Traffic file Id
     * @return Traffic file Value Object
     */
    public TrafficFileVO getTrfFileById(Long id){
        TrafficFileDAO dao = null;
        TrafficFileVO trafficFileVO=new TrafficFileVO();
        try {
            dao =(TrafficFileDAO) getDAO(TrafficFileDAO.class);
            trafficFileVO=dao.getTrfFileById(id);
            return trafficFileVO;
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }	
    }
    

    /**
    * Get traffic file value object using traffic file number for database searching.
    * @since 1.0
    * @return traffic file value object.
    * @param trafficFileVO
    */
    public TrafficFileVO getTrfFileByTrfNo(TrafficFileVO trafficFileVO){
                TrafficFileDAO dao = null;
            if(trafficFileVO.getTrafficNo()==null)
                throw new BusinessException("You should insert traffic file number.");
        try {            
            dao =(TrafficFileDAO) getDAO(TrafficFileDAO.class);
            trafficFileVO=dao.getTrfFileByTrfNo(trafficFileVO);
            return trafficFileVO;
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }	
    }
    
  
}
