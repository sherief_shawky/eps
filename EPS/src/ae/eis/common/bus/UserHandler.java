/*
 * Copyright (c) i-Soft 2008.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Bassem R. Zohdy    26/03/2008  - Creation.
 *
 * 1.01  Eng. Ayman Atiyeh  23/03/2009  - Adding doLogin()
 *                                      - Adding doLogout()
 *                                      - Adding addFailedLoginTrial()
 *                                      - Adding isLocked()
 *                                      - Adding getByUsername()
 *                                      - Adding changePassword()
 *                                      - Adding isCurrentPassword()
 *                                      - Adding resetPassword()
 *
 * 1.10  Alaa Salem         28/10/2009  - Adding find()
 *                                      - Adding add()
 *                                      - Adding 'userCenterHandler' Handler.
 *                                      - Adding isUserNameExists()
 *                                      - Adding isEmployeeHasActiveUser()
 *                                      - Adding isUserLogIn()
 *                                      - Adding update()
 *                                      - Adding sendPasswordByEmail()
 *                                        Overloaded Method.
 *                                      - Adding sendPasswordBySMS()
 *                                        Overloaded Method.
 *                                      - Adding isProfileAdminLevelCanUpdateUserInfo()
 *                                        Method.
 *                                      - Adding isProfileAdminLevelCanSetUserLevel()
 *                                        Method.
 *
 * 1.11  Alaa Salem         26/01/2010  - Adding isUserSupervisor() Method.
 *
 */

package ae.eis.common.bus;


import ae.eis.common.vo.UserVO;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import isoft.com.util.UserProfile;

import java.util.Collections;
import java.util.List;

/**
 *  User DAO Implementation
 *
 * @author Bassem R. Zohdy
 * @version 1.00
 */
public class UserHandler extends BusinessObject implements UserFacade {

    @Override
    public UserVO getByUserId(Long userId) {
        // TODO Implement this method
        return null;
    }

 
}
