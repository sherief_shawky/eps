/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  17/01/2008  - File created.
 * 
 * 1.01  Eng. Ayman Atiyeh  03/03/2008  - Adding business rule parameter methods.
 * 
 * 1.10  Alaa Salem         12/11/2009  - Adding findDomainKeys() method.
 * 
 * 1.11  Alaa Salem         21/01/2010  - Adding findBusinessRules() Method.
 * 
 * 1.02  Ahmad M.Adawi      11/01/2011  - Adding getDomainValues(String key,UserProfile profile,String [] rolesCodes) Method
 * 
 * 1.03  Ahmad M.Adawi      14/08/2012  - Adding getReportTagDomainValues
 * 
 * 1.04  Bashar Alnemrawi   19/02/2013  - Adding getBusinessRuleMessageEn() method.
 * 
 */
package ae.eis.util.bus;


import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.BusinessRuleVO;
import ae.eis.util.vo.DomainVO;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * System preferences business object. Used to retieve system preferences
 * and setup values.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.03
 */
public class PreferencesHandler extends BusinessObject implements PreferencesFacade {

    @Override
    public DomainVO[] getDomainValues(String key) {
        // TODO Implement this method
        return new DomainVO[0];
    }

    @Override
    public DomainVO[] getDomainWithoutValues(String name, Integer[] values) {
        // TODO Implement this method
        return new DomainVO[0];
    }

    @Override
    public DomainVO getDomainValue(String name, Integer value) {
        // TODO Implement this method
        return null;
    }

    @Override
    public BusinessRuleVO getBusinessRule(String key, String[] parameters) {
        // TODO Implement this method
        return null;
    }

    @Override
    public BusinessRuleVO getBusinessRule(String key, String[] parameters, String[] parametersEn) {
        // TODO Implement this method
        return null;
    }

    @Override
    public BusinessRuleVO getBusinessRule(String key) {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getBusinessRuleMessage(String key) {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getBusinessRuleMessage(String key, String lang) {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getBusinessRuleMessage(RuleException ex, String lang) {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getBusinessRuleMessage(RuleException ex) {
        // TODO Implement this method
        return null;
    }

    @Override
    public BusinessRuleVO[] getBusinessRules(Set keys) {
        // TODO Implement this method
        return new BusinessRuleVO[0];
    }

    @Override
    public DomainVO[] getDomainValues(String name, Integer[] values) {
        // TODO Implement this method
        return new DomainVO[0];
    }

    @Override
    public boolean isDomainCacheEnabled() {
        // TODO Implement this method
        return false;
    }

    @Override
    public SearchPageVO findDomainKeys(int pageNo, DomainVO vo) {
        // TODO Implement this method
        return null;
    }

    @Override
    public SearchPageVO findBusinessRules(int pageNo, BusinessRuleVO vo) {
        // TODO Implement this method
        return null;
    }

    @Override
    public List<BusinessRuleVO> getBusinessRules(BusinessRuleVO vo) {
        // TODO Implement this method
        return Collections.emptyList();
    }

    @Override
    public DomainVO[] getDomainValues(String key, UserProfile profile, String[] rolesCodes) {
        // TODO Implement this method
        return new DomainVO[0];
    }

    @Override
    public boolean isServiceAllowed(String key, UserProfile profile, String[] rolesCodes, Integer serviceCode) {
        // TODO Implement this method
        return false;
    }

    @Override
    public DomainVO[] getDomainValuesByParentRoleId(String key, UserProfile profile, String[] rolesCodes,
                                                    String parentRoleCode) {
        // TODO Implement this method
        return new DomainVO[0];
    }

    @Override
    public String getDescriptionByValue(String domainName, String value) {
        // TODO Implement this method
        return null;
    }

    @Override
    public DomainVO getDomainByDescription(String key, String descriptionAr, String descriptionEn) {
        // TODO Implement this method
        return null;
    }

    @Override
    public DomainVO[] getReportTagDomainValues(String tableName, String cloumnName, String whereClouse) {
        // TODO Implement this method
        return new DomainVO[0];
    }

    @Override
    public String getBusinessRuleMessageEn(String key) {
        // TODO Implement this method
        return null;
    }

    @Override
    public DomainVO[] getDomainWithoutValues(String key, UserProfile profile, String[] rolesCodes,
                                             Integer[] excludedValues) {
        // TODO Implement this method
        return new DomainVO[0];
    }

    @Override
    public DomainVO[] getOrderDomainValues(String key, String lang) {
        // TODO Implement this method
        return new DomainVO[0];
    }
}
