package ae.eis.util.bus;

import ae.eis.util.vo.BusinessRuleVO;
import ae.eis.util.vo.DomainVO;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile;

import java.util.List;
import java.util.Set;

public interface PreferencesFacade {
    /**
     * Get domain values.
     *
     * @param key Domain value key.
     * @return Domain values info.
     */
    DomainVO[] getDomainValues(String key);

    /**
     * Get domain for Without values.
     */
    DomainVO[] getDomainWithoutValues(String name, Integer values[]);

    /**
     * Get domain value.
     *
     * @param name Domain value name.
     * @param value Domain value.
     * @return Domain values info.
     */
    DomainVO getDomainValue(String name, Integer value);

    /**
     * Get business rule info.
     *
     * @param key business rule key.
     * @param parameters Business rule parameters.
     * @return business rule value object.
     */
    BusinessRuleVO getBusinessRule(String key, String[] parameters);

    /**
     * Get business rule info.
     *
     * @param key business rule key.
     * @param parameters Business rule parameters.
     * @param parametersEn Business rule english parameters.
     * @return business rule value object.
     */
    BusinessRuleVO getBusinessRule(String key, String[] parameters, String[] parametersEn);

    /**
     * Get business rule info.
     *
     * @param key business rule key.
     * @return business rule value object.
     */
    BusinessRuleVO getBusinessRule(String key);

    /**
     * Get business rule message.
     *
     * @param key business rule key value.
     * @return business rule message.
     */
    String getBusinessRuleMessage(String key);

    /**
     * Get business rule message.
     *
     * @param key business rule key value.
     * @param lang user language.
     * @return business rule message localized.
     */
    String getBusinessRuleMessage(String key, String lang);

    /**
     * Get business rule message.
     *
     * @param exception business rule exception.
     * @param lang user language.
     * @return business rule message localized.
     */
    String getBusinessRuleMessage(RuleException ex, String lang);

    /**
     * Get business rule message.
     *
     * @param exception business rule exception.
     * @return business rule message.
     */
    String getBusinessRuleMessage(RuleException ex);

    /**
     * Get business rules info.
     *
     * @param keys List of business rules keys.
     * @return business rules value objects.
     */
    BusinessRuleVO[] getBusinessRules(Set keys);

    /**
     * Get domain values.
     *
     * @return Domain values info.
     * @param name Domain value name.
     * @param values
     */
    DomainVO[] getDomainValues(String name, Integer values[]);

    /**
     * Check if domain-values cache is enabled.
     *
     * @return true if if domain-values cache is enabled.
     */
    boolean isDomainCacheEnabled();

    /**
     * Find Domain Keys.
     *
     * @return Search Page Value Object
     * @param pageNo pagination page Number
     * @param vo Domain VO
     */
    SearchPageVO findDomainKeys(int pageNo, DomainVO vo);

    /**
     * Find Business Rules.
     *
     * @param pageNo Pagination Page Number.
     * @param vo Business Rule VO.
     * @return Search Page Value Object.
     */
    SearchPageVO findBusinessRules(int pageNo, BusinessRuleVO vo);

    /**
     * Get Business Rules.
     *
     * @param vo Business Rule VO.
     * @return List Of Business Rule VO
     */
    List<BusinessRuleVO> getBusinessRules(BusinessRuleVO vo);

    /**
     * Get Domain Values
     *
     * @param key
     * @param profile
     * @param rolesCodes
     *
     * @return Domain Values Info. That User Has Privilege In It
     */
    DomainVO[] getDomainValues(String key, UserProfile profile, String[] rolesCodes);

    /**
     * Get Domain Values
     *
     * @param key : System Privilige key
     * @param profile : Profile
     * @param rolesCodes : Roles Codes
     * @param serviceCode : Service Code
     * @return true if selected Service Code is allowed for user
     */
    boolean isServiceAllowed(String key, UserProfile profile, String[] rolesCodes, Integer serviceCode);

    /**
     * Get Domain Values By Parent Role Id
     *
     * @param key
     * @param profile
     * @param rolesCodes
     * @param parentRoleCode
     *
     * @return Domain Values Info. That User Has Privilege In It
     */
    DomainVO[] getDomainValuesByParentRoleId(String key, UserProfile profile, String[] rolesCodes,
                                             String parentRoleCode);

    /**
     * Get Description By Value
     *
     * @param domainName Domain Name
     * @param value alue
     *
     * @return Domain Description
     */
    String getDescriptionByValue(String domainName, String value);

    /**
     * Get Domain By Description.
     *
     * @param key Domain Key.
     * @param Domain DescriptionAr.
     * @param Domain DescriptionEn.
     * @return Domain By Description.
     */
    DomainVO getDomainByDescription(String key, String descriptionAr, String descriptionEn);

    /**
     * Get Report Tag Domain Values
     *
     * @param whereClouse
     * @param cloumnName
     * @param tableName
     *
     * @return Array of Domain Value Object
     */
    DomainVO[] getReportTagDomainValues(String tableName, String cloumnName, String whereClouse);

    /**
     * Get business rule message english.
     *
     * @param key business rule key value.
     * @return business rule message english.
     */
    String getBusinessRuleMessageEn(String key);

    /**
     * Get Domain Values
     *
     * @param key
     * @param profile
     * @param rolesCodes
     * @param excludedValues
     *
     * @return Domain Values Info. That User Has Privilege In It
     */
    DomainVO[] getDomainWithoutValues(String key, UserProfile profile, String[] rolesCodes, Integer[] excludedValues);

    /**
     * Get Order Domain Values
     *
     * @param key
     * @param lang
     *
     * @return Domain Values Info.
     */
    DomainVO[] getOrderDomainValues(String key, String lang);
}
