/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  14/01/2008  - File created.
 * 
 * 1.01  Rami Nassar        15/10/2012  - Add isEnglishText method TRF-8578
 */

package ae.eis.util.bus;


import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DAOFactory;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.ValueObject;

import java.io.InputStream;
import java.io.OutputStream;

import java.net.Socket;

import java.sql.Connection;
import java.sql.Statement;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;


/**
 * Defines the common functionality for business objects. All BO classes must
 * extend this class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public abstract class BusinessObject {


    /** Preferences Business Object. */
    private static PreferencesFacade preferencesHandler = new PreferencesHandler();
    
    /*
     * Methods
     */
    
    /**
     * Clear time from date object.
     * 
     * @param date Date object to be processed.
     * @return Date without time or null if the date parameter was null.
     */
    protected static Date clearTime(Date date) {
        return GlobalUtilities.clearTime(date);
    }
    
    
    /**
     * Check if the field is not null or contains only blank spaces.
     * 
     * @param value String value to be checked.
     * @return true if the field is not null or blank.
     */
    protected static boolean isBlankOrNull(String value) {
        return GlobalUtilities.isBlankOrNull(value);
    }
    
    /**
     * Is English Text
     * return true if all charecters are english, otherwise return false
     * 
     * @return boolean ( true / false )
     * @param text
     */
    public static boolean isEnglishText(String text) {
        return GlobalUtilities.isEnglishText(text);
    }
    
    /**
     * Check if the field contains a long value.
     * 
     * @param value String value to be checked.
     * @return true if the field contains a long value.
     */
    protected static boolean isLong(String value) {
        return GlobalUtilities.isLong(value);
    }
    
    /**
     * Check if the field contains a integer value.
     * 
     * @param value : Integer value to be checked.
     * @return true if the field contains a integer value.
     */
    protected static boolean isInteger(String value) {
        return GlobalUtilities.isInteger(value);
    }
	
    /**
     * Close the JDBC Statement.
     * 
     * @param stm CallableStatement to be closed.
     */
    private static boolean close(Statement stm) {
        if (stm == null) {
            return false;
        }
        try {
            stm.close();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
	
    /**
     * Close DAO object and rlease its resources. This method used to enhance 
     * code clarity and readability.
     * 
     * @param dao DataAccessObject to be closed.
     */
    protected void close(DataAccessObject dao) {
        if (dao == null) {
            return;
        }

        dao.close();
    }

    /**
     * Close DAO object and rlease its resources. This method used to enhance 
     * code clarity and readability.
     * 
     * @param dao DataAccessObject to be closed.
     */
    protected static void closeDao(DataAccessObject dao) {
        if (dao == null) {
            return;
        }

        dao.close();
    }
    
    /**
     * Close DAO object and rlease its resources. This method used to enhance 
     * code clarity and readability.
     * 
     * @param dao DataAccessObject to be closed.
     */
    protected void close(isoft.com.util.dao.DataAccessObject dao) {
        if (dao == null) {
            return;
        }

        dao.close();
    }

    /**
     * This method used to enhance code clarity and readability.
     * 
     * @param dao DataAccessObject to be rolled back.
     */
    protected void rollback(DataAccessObject dao) {
        if (dao == null) {
            return;
        }

        dao.rollback();
    }
    
    
    
    
    /**
     * This method used to enhance code clarity and readability.
     * 
     * @param dao DataAccessObject to be rolled back.
     */
    protected static void rollbackDao(DataAccessObject dao) {
        if (dao == null) {
            return;
        }

        dao.rollback();
    }
    
    /**
     * This method used to enhance code clarity and readability.
     * 
     * @param dao DataAccessObject to be rolled back.
     */
    protected void rollback(isoft.com.util.dao.DataAccessObject dao) {
        if (dao == null) {
            return;
        }

        dao.rollback();
    }

    /**
     * Get class logger object.
     * 
     * @return class logger object.
     */
    protected Logger getLogger() {
        return Logger.getLogger(this.getClass().getName());
    }    

    /**
     * Get string representation of this object
     * 
     * @param obj Object to be parsed,
     * @return string representation of this object
     */
    public String getString(Object obj) {
        return GlobalUtilities.getString(obj);
    }

    /**
     * Get Long object or null if the value is null or empty string.
     * 
     * @param value String value to be tested and parsed.
     * @return Long object or null if the value is null or empty string.
     */
    public static Long getLong(Object value) {
        return GlobalUtilities.getLong(value);
    }
    
    /**
     * Get Integer object or null if the value is null or empty string.
     * 
     * @param value String value to be tested and parsed.
     * @return Integer object or null if the value is null or empty string.
     */
    public static Integer getInteger(Object value) {
        return GlobalUtilities.getInteger(value);
    }
    
    /**
     * Get Date object or null if the value is null or empty string.
     * 
     * @param value Object to be tested and parsed.
     * @return date object or null if the value is null or empty string.
     */
    public static Date getDate(Object value) {
        try  {
            return GlobalUtilities.getDate(value);
        } catch (Exception ex)  {
            throw new BusinessException(ex);
        }
    }
    
    /**
     * Get Data Access Object implementation class.
     * 
     * @param daoType Data Access Object interface.
     * @return Data Access Object implementation class.
     */
    protected DataAccessObject getDAO(Class daoType) {
        return DAOFactory.getInstance().getDAO(daoType);
    }
    
    /**
     * Get Data Access Object implementation class.
     * 
     * @param daoType Data Access Object interface.
     * @return Data Access Object implementation class.
     */
    public <T extends DataAccessObject> T getDataAccessObject(Class<T> daoType){
        return DAOFactory.getInstance().getDataAccessObject(daoType);
    }
    
    /**
     * Get Data Access Object implementation class.
     * 
     * @param daoType Data Access Object interface.
     * @param trsDAO Transactional DAO used to initialize the new created DAO.
     * @return Data Access Object implementation class.
     */
    protected DataAccessObject getDAO(Class daoType, DataAccessObject trsDAO) {
        return DAOFactory.getInstance().getDAO(daoType, trsDAO);
    }

    /**
     * Get Data Access Object implementation class.
     * 
     * @param daoType Data Access Object interface.
     * @param connection The new DAO will use this connection.
     * @return Data Access Object implementation class.
     */
    protected DataAccessObject getDAO(Class daoType, Connection connection) {
        return DAOFactory.getInstance().getDAO(daoType, connection);
    }


    /**
     * Close network socket.
     * 
     * @param socket network socket.
     */
    protected void close(Socket socket) {
        if (socket == null) {
            return;
        }

        try {socket.close();} 
        catch (Exception ex) {
            // Ignore exception
        }
    }

    /**
     * Close output stream.
     * 
     * @param out output stream.
     */
    protected void close(OutputStream out) {
        if (out == null) {
            return;
        }

        try {out.close();} 
        catch (Exception ex) {
            // Ignore exception
        }
    }

    /**
     * Close input stream.
     * 
     * @param in input stream.
     */
    protected void close(InputStream in) {
        if (in == null) {
            return;
        }

        try {in.close();} 
        catch (Exception ex) {
            // Ignore exception
        }
    }
    
     
    /**
     * Lookup for entities based on query search
     * 
     * @param query : text to search
     * @param lang : language for logged-in user
     * @param paramsMap : extra where condetion params Map
     * @return list of entities matches this query
     */
    public List<? extends ValueObject> lookup(String query, String lang, int pageSize,HashMap<String,String> paramsMap){
        throw new BusinessException("this method not implemented yet!!");
    }
    
    /**
     * Validate (id, text) for entity recived from lookup componant
     * 
     * @param id : id of the entity
     * @param text : description based on language
     * @param lang : language for loogged-in user
     * @return true if data is valid
     */
    public boolean validateLookupValue(String id, String text, String lang){
        throw new BusinessException("this method not implemented yet!!");
    }
    
    
	
    
    

    
	
	 
	
	
	
	
	

   
	
    
    /**
     * Check if the field contains a valid date value.
     * 
     * @param value : date value to be checked.
     * @return true if the field contains a vaild date value.
     */
    
    public boolean isValidDate(Date dateValue)
    {
        if (dateValue == null || dateValue.toString().trim().isEmpty()) {
            return false;
        }
        
        return true;
    }
    
    
    
    
    
    /**
     * Is English Text With Special Char
     * return true if all charecters are english or Special Char, otherwise return false
     * 
     * @return boolean ( true / false )
     * @param text, specialCharacters
     */
    public static boolean isEnglishTextWithSpecialChar(String text, String[] specialCharacters ) {
        return GlobalUtilities.isEnglishTextWithSpecialChar(text , specialCharacters);
    }
    
    /**
     * is Null Or Empty Collection
     * 
     * @param collection : Collection
     * @return if passed collection is null or Empty
     */
    public static boolean isNullOrEmpty(Collection collection){
        return GlobalUtilities.isBlankOrNull( collection );
    }
}
