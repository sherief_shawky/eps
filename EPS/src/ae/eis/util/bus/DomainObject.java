/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  14/01/2008  - File created.
 * 
 * 1.01  Eng. Ayman Atiyeh  10/03/2008  - Adding getDomainObjectDAO().
 */

package ae.eis.util.bus;

import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.dao.DataAccessObjectFactory;
import ae.eis.util.vo.ValueObject;
import java.util.logging.Logger;

/**
 * Defines the common functionality for domain objects. All domain classes must 
 * extend this class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public abstract class DomainObject extends BusinessObject {
    /*
     * Instance variables
     */

    /** Value object used to encapsulate domain data. */
    private ValueObject valueObject;

    /*
     * Constructors
     */
     
    /**
     * Construct new domain object.
     * 
     * @param vo Value object used to encapsulate domain data.
     */
    public DomainObject(ValueObject vo) {
        setValueObject(vo);
    }
    
    /*
     * Methods
     */

    /**
     * Value object used to encapsulate domain data.
     * 
     * @param vo Value object used to encapsulate domain data.
     */
    protected void setValueObject(ValueObject vo) {
        if (vo == null) {
            throw new BusinessException("NULL value object parameter");
        }

        this.valueObject = vo;
    }

    /**
     * Get Value object used to encapsulate domain data.
     * 
     * @return Value object used to encapsulate domain data.
     */
    public ValueObject getValueObject() {
        return valueObject;
    }
    
    /**
     * Get domain data access object implementation class.
     *
     * @return domain data access object implementation class.
     */
    public DataAccessObject getDomainObjectDAO() {
        return DataAccessObjectFactory.getDomainObjectDAO();
    }
}