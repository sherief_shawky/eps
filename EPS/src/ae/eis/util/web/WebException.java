/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  27/12/2007  - File created.
 */

package ae.eis.util.web;


import isoft.com.util.exceptions.NestedRuntimeException;

/**
 * Web layer exception.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class WebException extends NestedRuntimeException {
    /**
     * Construct new WebException object.
     */
    public WebException() {
    	super();
    }

    /**
     * Construct new WebException object.
     *
     * @param msg Exception error message.
     */
    public WebException(String msg) {
        super(msg);
    }

    /**
     * Nest the geneterated exception inside WebException object.
     *
     * @param nestedException Generated exception.
     */
    public WebException(Throwable nestedException) {
        super(nestedException);
    }

    /**
     * Nest the geneterated exception inside WebException object.
     *
     * @param msg Exception error message.
     * @param nestedException Generated exception.
     */
    public WebException(String msg, Throwable nestedException) {
        super(msg, nestedException);
    }
}