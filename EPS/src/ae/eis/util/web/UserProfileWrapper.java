/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Zaid Akel           25/01/2015  - File created.
 */
 
package ae.eis.util.web;

/**
 * 
 * @auther Zaid Akel
 */
public class UserProfileWrapper extends UserProfile{

    /** Username **/
    private String username;
    
    /** Center ID **/
    private Long centerId;
    
    private Long userId;
    
    /**
     * Setter for username
     * @param username Username
     */
    public void setUsername(String username){
        this.username = username;
    }
    
    /**
     * Get username.
     * 
     * @return username.
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * Setter for centerId
     * 
     * @param centerId Center ID
     */
    public void setCenterId(Long centerId){
        this.centerId = centerId;
    }
    
    /**
     * Get center ID.
     * 
     * @return center ID.
     */
    public Long getCenterId() {
        return centerId;
    }

    /**
     * Setter for userId
     * 
     * @param userId User ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    /**
     * Get user ID.
     * 
     * @return user ID.
     */
    public Long getUserId() {
        return userId;
    }

}
