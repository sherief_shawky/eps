/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  15/01/2008  - File created.
 */

package ae.eis.util.web;


import ae.eis.util.common.GlobalUtilities;

import isoft.com.util.Profile;

import java.io.Serializable;

import java.util.List;

/**
 * User profile bean value object. Used to encapsulated authenticated user
 * profile.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class UserProfile implements Serializable {
    /*
     * Constants and class variables.
     */

    /** HTTP session attribute used to save user profile. */
    public static final String USER_PROFILE_BEAN = "user_profile";
    
 
    /*
     * Instance variables
     */
    
    /** Traffic profile value object. */
    private Profile profile;
    


    /*
     * Constructors
     */

    /**
     * Default constructor.
     */
    public UserProfile() {
        // Empty body.
    }

    /**
     * Construct and initialize new UserProfile object.
     * 
     * @param prof Traffic profile value object.
     */
    public UserProfile(Profile prof) {
        setProfile(prof);
    }

    /*
     * Methods
     */
    
    /**
     * Set Traffic profile value object.
     * 
     * @param prof Traffic profile value object.
     */
    public void setProfile(Profile prof) {
        if (prof == null) {
            throw new IllegalStateException("NULL profile object");
        }

        this.profile = prof;
    }

    /**
     * Get Traffic profile value object.
     * 
     * @return Traffic profile value object.
     */
    public Profile getProfile() {
        return profile;
    }
    
    /**
     * Get profile attribute
     * 
     * @param attrId attribute ID.
     * @return profile attribute.
     */
    private Object getAttribute(short attrId) {
        if (getProfile() == null) {
            throw new IllegalStateException("NULL profile");
        }

        return getProfile().getAttribute(attrId);
    }
    
    /**
     * Get String attribute.
     * 
     * @param attrId attribute ID.
     * @return String attribute.
     */
    private String getString(short attrId) {
        Object obj = getAttribute(attrId);
        return (obj != null) ? obj.toString() : null;
    }
    
    /**
     * Get long attribute.
     * 
     * @param attrId attribute ID.
     * @return Long attribute.
     */
    private Long getLong(short attrId) {
        Object obj = getAttribute(attrId);
        if (obj == null) {
            return null;
        }

        if (obj instanceof Integer) {
            return new Long(((Integer) obj).intValue());
        }

        throw new IllegalStateException(new StringBuffer(
          "Invalid Integer attribute type")
            .append(", class=").append(obj.getClass().getName())
              .append(", value=").append(obj).toString());
    }

    /**
     * Get integer attribute.
     * 
     * @param attrId attribute ID.
     * @return integer attribute.
     */
    private Integer getInteger(short attrId) {
        Object obj = getAttribute(attrId);
        if (obj == null) {
            return null;
        }

        if (obj instanceof Integer) {
            return (Integer) obj;
        }

        throw new IllegalStateException(new StringBuffer(
          "Invalid Integer attribute type")
            .append(", class=").append(obj.getClass().getName())
              .append(", value=").append(obj).toString());
    }

    /**
     * Get boolean attribute.
     * 
     * @param attrId attribute ID.
     * @return boolean attribute.
     */
    private Boolean getBoolean(short attrId) {
        Object obj = getAttribute(attrId);
        if (obj == null) {
            return null;
        }

        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }

        throw new IllegalStateException(new StringBuffer(
          "Invalid Boolean attribute type")
            .append(", class=").append(obj.getClass().getName())
              .append(", value=").append(obj).toString());
    }

    /**
     * Get username.
     * 
     * @return username.
     */
    public String getUsername() {
        return getString(Profile.USER_NAME);
    }
    
    /**
     * Get user ID.
     * 
     * @return user ID.
     */
    public Long getUserId() {
        return getLong(Profile.USER_ID);
    }
    


    /**
     * Get employee name.
     * 
     * @return employee name.
     */
    public String getEmployeeName() {
        return getString(Profile.EMPLOYEE_NAME);
    }
    
    /**
     * Get employee name E.
     * 
     * @return employee name E.
     */
    public String getEmployeeNameE() {
        return getString(Profile.EMPLOYEE_NAME_E);
    }


    /**
     * Get employee number.
     * 
     * @return employee number.
     */
    public String getEmployeeNo() {
        return getString(Profile.EMPLOYEE_NUMBER);
    }

    /**
     * Get employee ID.
     * 
     * @return employee ID.
     */
    public Long getEmployeeId() {
        return getLong(Profile.EMPLOYEE_ID);
    }

    /**
     * Get user groups.
     * 
     * @return user groups.
     */
    public List getUserGroups() {
        return (List) getProfile().getAttribute(Profile.USER_GROUPS);
    }

    /**
     * Get center name in arabic.
     * 
     * @return center name in arabic.
     */
    public String getCenterNameAr() {
        return getString(Profile.USER_CENTER_NAME);
    }

  

    /**
     * Returns the String representation of this object.
     * @return String representation of this object.
     */
    public String toString() {
        StringBuffer buf = new StringBuffer(getClass().getName());
        buf.append("{\n")
           .append(  "  userId=").append(getUserId())
           .append("\n, username=").append(getUsername())
           .append("\n, employeeId=").append(getEmployeeId())
           .append("\n, employeeName=").append(getEmployeeName())
           .append("\n, employeeNo=").append(getEmployeeNo())
           .append("\n, centerNameAr=").append(getCenterNameAr())
           .append("\n, userGroups=").append(getUserGroups())
           .append("\n, lanCode=").append(getLanCode())
           .append( "\n}");

        return buf.toString();
    }


    /**
     * Check If The User Is Granted To A Role.
     * 
     * @param roleCode Role Code.
     * @return true If The User Is Granted To A Role.
     */
    public boolean isUserInRole(String roleCode) {
       //TODO replace with proper implemention
       return true;
    }
    
    /**
     * Check If The User Is Granted To A Role.
     * 
     * @param roleCode Role Code.
     * @return true If The User Is Granted To A Role.
     */
    public boolean isUserInRoleReport(String roleCode) {
       //TODO replace with proper implemention
        return true;
    }

    /**
     * Get lan code.
     * 
     * @return lan.
     */
    public String getLanCode() {
        return getString(Profile.LANG_CODE);
    }
    
    public Long getCenterId(){
        return 0L;
    }



}
