/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  15/05/2009  - File created.
 */

package ae.eis.util.vo;


/**
 * Attachment value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class AttachmentVO extends MimeTypeVO {
    /*
     * Instants variables.
     */

    /** Attachment arabic name. */
    private String nameAr;
    
    /** Attachment english name. */
    private String nameEn;

    /** Attachment binary content. */
    private byte[] attachment;
    /** base64. */    
    private String base64;
    
    /** File Size */
    private Double fileSize;

    /*
     * Constructors
     */
    
    /**
     * Default constructor
     */
    public AttachmentVO() {
        // Empty body
    }

    /**
     * Construct and initialize new value object.
     * 
     * @param mimeVO Attachment MIME info.
     */
    public AttachmentVO(MimeTypeVO mimeVO) {
        setMimeInfo(mimeVO);
    }

    /*
     * Methods
     */
    
    /**
     * Set file attachment info
     * 
     * @param vo Attachment file info.
     */
    public void setAttachmentInfo(AttachmentVO vo) {
        setMimeInfo(vo);

        if (vo == null) {
            this.setAttachment(null);
            this.setNameAr(null);
            this.setNameEn(null);
        } else {
            this.setAttachment(vo.getAttachment());
            this.setNameAr(vo.getNameAr());
            this.setNameEn(vo.getNameEn());
        }
    }

    /**
     * Set Attachment binary content.
     * 
     * @param attachment Attachment binary content.
     */
    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    /**
     * Get Attachment binary content.
     * 
     * @return Attachment binary content.
     */
    public byte[] getAttachment() {
        return attachment;
    }
    
    /**
     * Check if this is an empty body attachment.
     * 
     * @return true if this is an empty body attachment.
     */
    public boolean isEmptyBody() {
        return attachment == null || attachment.length == 0;
    }
    
    /**
     * Get attachment size in bytes.
     * 
     * @return attachment size in bytes.
     */
    public long getSize() {
        return (attachment == null) ? 0 : attachment.length;
    }

    /**
     * setter for nameAr
     * @param nameAr
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * getter for arabicName
     * @return arabicName
     */
    public String getNameAr() {
        return this.nameAr;
    }

    /**
     * setter for nameEn
     * @param nameEn
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * getter for nameEn
     * @return nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
    * setter for base64
    * @param base64
    */
    public void setBase64(String base64) {
        this.base64 = base64;
    }
    /**
    * getter for base64
    * @return base64
    */

    public String getBase64() {
        return base64;
    }

    /**
     * Setter for File Size
     *
     * @param fileSize File Size
     */
    public void setFileSize(Double fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * Getter for File Size
     *
     * @return fileSize File Size
     */
    public Double getFileSize() {
        return fileSize;
    }
}
