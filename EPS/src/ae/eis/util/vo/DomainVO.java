/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  17/01/2008  - File created.
 */

package ae.eis.util.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Encapsulate domain values. Domain values used to describe business entities
 * statuses and types such as template-category, plate-status ...
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */

@XmlRootElement
public class DomainVO implements Serializable {

    /** False Boolean **/
    public static final int FALSE_BOOLEAN = 1;    
    /** True Boolean **/
    public static final int TRUE_BOOLEAN = 2;

    /** Status values inactive */
    public static Integer STATUS_INACTIVE = new Integer(1);
    
    /** Status values active */
    public static Integer STATUS_ACTIVE = new Integer(2);

    
    /** Domain table simple **/
    public static final Integer TF_DOMAIN_TABLE_SIMPLE = 1;

    /** Domain table lookup **/
    public static final Integer TF_DOMAIN_TABLE_LOOKUP = 2;
    
    /*
     * Instance variables
     */

    /** Domain arabic description. */
    private String descriptionAr;

    /** Domain english description. */
    private String descriptionEn;

    /** Domain value. */
    private Integer value;
    
    /** Domain key. */
    private String key;
    
    /*
     * Constructors
     */
    
    /**
     * Construct new DomainVO object.
     */
    public DomainVO() {
        // Empty body
    }
    
    /**
     * Construct and initialize new DomainVO object.
     * 
     * @param newKey Domain object key.
     */
    public DomainVO(String newKey) {
        setKey(newKey);
    }
    
    /**
     * Construct and initialize new DomainVO object.
     * 
     * @param newKey Domain object key.
     * @param val Domain value.
     */
    public DomainVO(String newKey, Integer val) {
        this(newKey);
        setValue(val);
    }

    /**
     * Construct and initialize new DomainVO object.
     * 
     * @param newKey Domain object key.
     * @param val Domain value.
     * @param descAr Domain arabic description.
     */
    public DomainVO(String newKey, Integer val, String descAr) {
        this(newKey, val);
        setDescriptionAr(descAr);
    }

    /**
     * Construct and initialize new DomainVO object.
     * 
     * @param newKey Domain object key.
     * @param val Domain value.
     * @param descAr Domain arabic description.
     * @param descAe Domain english description.
     */
    public DomainVO(String newKey, Integer val, String descAr, String descAe) {
        this(newKey, val, descAr);
        setDescriptionEn(descAe);
    }

    /*
     * Methods
     */

    /**
     * Returns the String representation of this object.
     * 
     * @return String representation of this object.
     */
    public String toString() {
        StringBuffer buf = new StringBuffer(getClass().getName());
        buf.append( "{key=").append(getKey())
           .append(", value=").append(getValue())
           .append(", descriptionAr=").append(getDescriptionAr())
           .append(", descriptionEn=").append(getDescriptionEn())
           .append("}");

        return buf.toString();
    }

    /**
     * Set Domain arabic description.
     * 
     * @param descAr Domain arabic description.
     */
    public void setDescriptionAr(String descAr) {
        this.descriptionAr = descAr;
    }

    /**
     * Get Domain arabic description.
     * 
     * @return Domain arabic description.
     */
    public String getDescriptionAr() {
        return descriptionAr;
    }

    /**
     * Set Domain english description.
     * 
     * @param descEn Domain english description.
     */
    public void setDescriptionEn(String descEn) {
        this.descriptionEn = descEn;
    }

    /**
     * Get Domain english description.
     * 
     * @return Domain english description.
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * Set Domain value.
     * 
     * @param val Domain value.
     */
    public void setValue(Integer val) {
        this.value = val;
    }

    /**
     * Get Domain value.
     * 
     * @return Domain value.
     */
    public Integer getValue() {
        return value;
    }

    /**
     * Set Domain key.
     * 
     * @param newkey Domain key.
     */
    public void setKey(String newkey) {
        this.key = newkey;
    }

    /**
     * Get Domain key.
     * 
     * @return Domain key.
     */
    public String getKey() {
        return key;
    }
}