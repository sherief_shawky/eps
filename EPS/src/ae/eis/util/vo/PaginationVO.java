/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  15/01/2008  - File created.
 */

package ae.eis.util.vo;

import ae.eis.util.common.GlobalUtilities;

import java.io.Serializable;

/**
 * Pagination controller. Used to control pagination actions.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class PaginationVO implements Serializable {
    /*
     * Constants.
     */
    
    /** Request attribute name used to store this bean object. */
    public static final String BEAN = PaginationVO.class.getName();

    /*
     * Instance variables.
     */
    
    /** Current search page result info. */
    private SearchPageVO searchPage;
    
    /** Page number hidden field name. If not exists pageNo will be used. */
    private String pageNoField;
    
    /** 
     * When the user clicks a pagination control the pageNo hidden field will 
     * be updated and this Java-Script function will be called to submit the 
     * HTML search form. If this function was not provided by the user the tag
     * will submit the page form.
     */
    private String submitFunc;

    /** HTML form name. If not exists forms[0] will be used. */
    private String form;
    
    /** Support HTML5.*/
    private boolean supportHtml5;
    
    /*
     * Constructors.
     */

    /**
     * Default constructor.
     */
    public PaginationVO() {
        // Empty body
    }

    /**
     * Construct new Pagination object.
     * 
     * @param pageNo Page number hidden field name.
     * @param func JS function to be called after updating the pagination control.
     * @param form HTML form name. If not exists forms[0] will be used.
     * @param page Current page info.
     */
    public PaginationVO(String pageNo, String func, String form, SearchPageVO page,boolean isSupportHtml5) {
        setPageNoField(pageNo);
        setSubmitFunc(func);
        setForm(form);
        setSearchPage(page);
        setSupportHtml5(isSupportHtml5);
    }

    /**
     * Construct new Pagination object.
     * 
     * @param pageNo Page number hidden field name.
     * @param func JS function to be called after updating the pagination control.
     */
    public PaginationVO(String pageNo, String form, String func,boolean isSupportHtml5) {
        this(pageNo, func, form, new SearchPageVO(),isSupportHtml5);
    }

    /*
     * Navigation methods
     */

    /**
     * Go to the first page.
     */
    public void goFirst() {
        if (isGoFirstEnabled() == false) {
            return;
        }
        
        this.searchPage.setPageNo(0);
    }

    /**
     * Go to the last page.
     */
    public void goLast() {
        if (isGoLastEnabled() == false) {
            return;
        }

        this.searchPage.setPageNo(getLastPageNo());
    }

    /**
     * Go to the next page.
     */
    public void goNext() {
        if (isGoNextEnabled() == false) {
            return;
        }
        
        this.searchPage.setPageNo(this.searchPage.getPageNo() + 1);
    }

    /**
     * Go to the previous page.
     */
    public void goBack() {
        if (isGoBackEnabled() == false) {
            return;
        }

        this.searchPage.setPageNo(this.searchPage.getPageNo() - 1);
    }

    /*
     * Methods.
     */
    
    /**
     * Get last page number.
     * 
     * @return last page number.
     */
    public int getLastPageNo() {
        int totalRecords = getTotalRecords();
        int searchPageSize = getSearchPageSize();
        int lastPageNo = (int) Math.floor((double) totalRecords / searchPageSize);

        if ((totalRecords % searchPageSize) == 0) {
            lastPageNo--;
        }

        return lastPageNo;
    }

    /**
     * Returns the String representation of this object.
     * @return String representation of this object.
     */
    public String toString() {
        StringBuffer buf = new StringBuffer(getClass().getName());
        buf.append("{\n page=").append(this.searchPage)
           .append(",\n offset=").append(getOffset())
           .append(",\n jsFunc=").append(submitFunc)
           .append(",\n form=").append(form)
           .append(",\n pageNoField=").append(pageNoField)
           .append(",\n firstRecoredIndex=").append(getFirstRecoredIndex())
           .append(",\n lastRecoredIndex=").append(getLastRecoredIndex())
           .append(",\n isGoNextEnabled=").append(isGoNextEnabled())
           .append(",\n isGoBackEnabled=").append(isGoBackEnabled())
           .append(",\n isGoLastEnabled=").append(isGoLastEnabled())
           .append(",\n isGoFirstEnabled=").append(isGoFirstEnabled())
           .append( "\n}");

        return buf.toString();
    }

    /**
     * Returns total number of recordes to navigate throw.
     * 
     * @Return The new total number of recordes to navigate throw.
     */
    public int getTotalRecords() {
        return this.searchPage.getTotalRecords();
    }

    /**
     * Returns search page size.
     * 
     * @return search page size.
     */
    public int getSearchPageSize() {
        return this.searchPage.getSearchPageSize();
    }

    /**
     * Returns page number.
     * 
     * @return page number.
     */
    public int getPageNo() {
        return this.searchPage.getPageNo();
    }

    /**
     * Returns first record offset.
     * 
     * @return first record offset.
     */
    public int getOffset() {
        return getPageNo() * getSearchPageSize();
    }

    /**
     * Returns current page size.
     * 
     * @retutrn Current page size.
     */
    public int getActivePageSize() {
        return this.searchPage.getCurrentPageSize();
    }

    /**
     * Check if GO_FIRST action is enabled or not.
     * 
     * @return true if GO_FIRST was enabled, false other wise.
     */
    public boolean isGoFirstEnabled() {
        return (getOffset() > 0);
    }

    /**
     * Returns Index of the first recored in the current page.
     * 
     * @return Index of the first recored in the current page.
     */
    public int getFirstRecoredIndex() {
        return (getTotalRecords() == 0) ? 0 : getOffset() + 1;
    }

    /**
     * Returns Index of the last recored in the current page.
     * 
     * @return Index of the last recored in the current page.
     */
    public int getLastRecoredIndex() {
        return (getOffset() + getSearchPageSize()) <= getTotalRecords()
            && (getOffset() + getSearchPageSize()) > 0
              ? getOffset() + getSearchPageSize()
              : getTotalRecords();
    }

    /**
     * Check if GO_LAST action is enabled or not.
     * 
     * @return true if GO_LAST was enabled, false other wise.
     */
    public boolean isGoLastEnabled() {
        return isGoNextEnabled();
    }

    /**
     * Check if GO_NEXT action is enabled or not.
     * 
     * @return true if GO_NEXT was enabled, false other wise.
     */
    public boolean isGoNextEnabled() {
        if (getSearchPageSize() <= 0) {
            return false;
        }

        return (getOffset() + getSearchPageSize()) < getTotalRecords();
    }

    /**
     * Check if GO_BACK action is enabled or not.
     * 
     * @return true if GO_BACK was enabled, false other wise.
     */
    public boolean isGoBackEnabled() {
        return isGoFirstEnabled();
    }

    /**
     * Get the JS function to be called after updating the pagination control.
     * <br> When the user clicks a pagination control the pageNo hidden field 
     * will  be updated and this Java-Script function will be called to submit 
     * the HTML search form.
     * 
     * @return jsFunc JS function to be called after updating the pagination control.
     */
    public String getJsFunc() {
        if (!GlobalUtilities.isBlankOrNull(submitFunc)) {
            return submitFunc;
        }

        return new StringBuffer("document.")
               .append(getForm())
               .append(".submit")
               .toString();
    }
    
    /**
     * Set the JS function to be called after updating the pagination control.
     * 
     * @param func the JS function to be called after updating the pagination 
     *        control.
     */
    private void setSubmitFunc(String func) {
        this.submitFunc = func;
    }

    /**
     * Get Page number hidden field name.
     * 
     * @return pageNoField Page number hidden field name.
     */
    public String getPageNoField() {
        return GlobalUtilities.isBlankOrNull(pageNoField) ? "pageNo" : pageNoField.trim();
    }

    /**
     * Set Page number hidden field name.
     * 
     * @param name Page number hidden field name.
     */
    private void setPageNoField(String name) {
        this.pageNoField = name;
    }

    /**
     * Set Current search page result info.
     * 
     * @param page Current search page result info.
     */
    private void setSearchPage(SearchPageVO page) {
        if (page == null) {
            throw new IllegalArgumentException("Null SearchPage parameter");
        }

        this.searchPage = page;
    }
    
    /**
     * Get search page object.
     * 
     * @return search page object.
     */
    public SearchPageVO getSearchPage() {
        return this.searchPage;
    }

    /**
     * Set HTML form name. If not exists forms[0] will be used.
     * 
     * @param form HTML form name. If not exists forms[0] will be used.
     */
    private void setForm(String form) {
        this.form = form;
    }

    /**
     * Get HTML form name. If not exists forms[0] will be used.
     * 
     * @return HTML form name. If not exists forms[0] will be used.
     */
    public String getForm() {
        return GlobalUtilities.isBlankOrNull(form) ? "forms[0]" : form.trim();
    }

    /**
     * Setter For Support HTML5.
     * 
     * @param supportHtml5
     */
    public void setSupportHtml5(boolean supportHtml5) {
        this.supportHtml5 = supportHtml5;
    }
    
    /**
     * Getter For Support HTML5.
     * 
     * @return supportHtml5
     */
    public boolean isSupportHtml5() {
        return supportHtml5;
    }
}
