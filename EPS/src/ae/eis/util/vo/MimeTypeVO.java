/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  19/05/2009  - File created.
 */

package ae.eis.util.vo;

/**
 * Attachment MIME type value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class MimeTypeVO extends ValueObject {
    
    /** Mime Exte Categories */
    public static final String MIME_TYPE_IMAGE_GIF = "gif";
    public static final String MIME_TYPE_IMAGE_JPEG = "jpeg";
    public static final String MIME_TYPE_IMAGE_JPG = "jpg";
    public static final String MIME_TYPE_IMAGE_JPE = "jpe";
    public static final String MIME_TYPE_IMAGE_BMP = "bmp";
    public static final String MIME_TYPE_IMAGE_TIF = "tif";
    public static final String MIME_TYPE_IMAGE_PNG = "png";
    public static final String MIME_TYPE_APPLICATOIN_PDF = "pdf";
    
    /*
     * Instants variables.
     */

    /** Attachment MIME type. */
    private String mimeType;
    
    /** Attachment file extension. */    
    private String fileExtension;
    
    /** Attachment Arabic description. */
    private String descriptionAr;
    
    /** Attachment English description. */
    private String descriptionEn;
    
    /** Browser support flag. */
    private Integer viewableInsideBrowser;

    /** Attachment file Hexadecimal Format. */
    private String fileHexFormat;

    /*
     * Methods
     */

    /**
     * Set MIME type info
     * 
     * @param mimeVO Attachment MIME info.
     */
    public void setMimeInfo(MimeTypeVO mimeVO) {
        if (mimeVO == null) {
            setFileExtension(null);
            setMimeType(null);
            setDescriptionAr(null);
            setDescriptionEn(null);
        } else {
            setFileExtension(mimeVO.getFileExtension());
            setMimeType(mimeVO.getMimeType());
            setDescriptionAr(mimeVO.getDescriptionAr());
            setDescriptionEn(mimeVO.getDescriptionEn());
        }
    }

    /**
     * Check if the mime type is Image
     * 
     */
    public boolean isImageType() {
        
        if (isBlankOrNull(fileExtension)) {
            return false;
        }
        
        if (fileExtension.equals(MIME_TYPE_IMAGE_GIF) ||
            fileExtension.equals(MIME_TYPE_IMAGE_JPEG) ||
            fileExtension.equals(MIME_TYPE_IMAGE_JPE) ||
            fileExtension.equals(MIME_TYPE_IMAGE_BMP) ||
            fileExtension.equals(MIME_TYPE_IMAGE_JPG) ||
            fileExtension.equals(MIME_TYPE_IMAGE_TIF) ||
            fileExtension.equals(MIME_TYPE_IMAGE_PNG)) {
            
            return true;    
        }
        
        return false;
    }

    /**
     * Check if the mime type from allowed type for pemit attachemets
     * 
     * @return Boolean true if valid
     */
    public boolean isValidPermitAttachementType() {
        if (isBlankOrNull(fileExtension)) {
            return false;
        }
        if (fileExtension.equals(MIME_TYPE_IMAGE_GIF) ||
            fileExtension.equals(MIME_TYPE_IMAGE_JPEG) ||
            fileExtension.equals(MIME_TYPE_IMAGE_JPE) ||
            fileExtension.equals(MIME_TYPE_IMAGE_BMP) ||
            fileExtension.equals(MIME_TYPE_IMAGE_JPG) ||
            fileExtension.equals(MIME_TYPE_IMAGE_TIF) ||
            fileExtension.equals(MIME_TYPE_APPLICATOIN_PDF)) {
            
            return true;    
        }
        
        return false;
    }
    
    /**
     * Set Attachment MIME type.
     * 
     * @param mimeType Attachment MIME type.
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * Get Attachment MIME type.
     * 
     * @return Attachment MIME type.
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Set Attachment file extension.
     * 
     * @param fileExtension Attachment file extension.
     */
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    /**
     * Get Attachment file extension.
     * 
     * @return Attachment file extension.
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * Set Attachment Arabic description.
     * 
     * @param descriptionAr Attachment Arabic description.
     */
    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    /**
     * Get Attachment Arabic description.
     * 
     * @return Attachment Arabic description.
     */
    public String getDescriptionAr() {
        return descriptionAr;
    }

    /**
     * Set Attachment English description.
     * 
     * @param descriptionEn Attachment English description.
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * Get Attachment English description.
     * 
     * @return Attachment English description.
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * Set Browser support flag.
     * 
     * @param viewableInsideBrowser Browser support flag.
     */
    public void setViewableInsideBrowser(Integer viewableInsideBrowser) {
        this.viewableInsideBrowser = viewableInsideBrowser;
    }

    /**
     * Get Browser support flag.
     * 
     * @return Browser support flag.
     */
    public Integer getViewableInsideBrowser() {
        return viewableInsideBrowser;
    }

    /**
     * Get Browser support flag.
     * 
     * @return Browser support flag.
     */
    public boolean isViewableInsideBrowser() {
        return viewableInsideBrowser != null && 
               viewableInsideBrowser.equals(TRUE);
    }
    
    /**
     * Set File Hexadecimal Format.
     * 
     * @param fileHexFormat.
     */
    public void setFileHexFormat(String fileHexFormat) {
        this.fileHexFormat = fileHexFormat;
    }
    
    /**
     * Get FileHexFormat.
     * 
     * @return fileHexFormat.
     */
    public String getFileHexFormat() {
        return fileHexFormat;
    }
}
