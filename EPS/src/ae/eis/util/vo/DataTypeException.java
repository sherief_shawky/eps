/*
 * Copyright (c) i-Soft 2006.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  25/01/2010  - File created.
 */

package ae.eis.util.vo;

/**
 * Invalid domain data-type exception.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class DataTypeException extends RuntimeException {
    /*
     * Constructors
     */

    /**
     * Construct new DataTypeException object.
     */
    public DataTypeException() {
    	super();
    }

    /**
     * Construct new DataTypeException object.
     * 
     * @param msg Exception message.
     */
    public DataTypeException(String msg) {
        super(msg);
    }

    /**
     * Construct new DataTypeException object.
     * 
     * @param msg Exception message.
     */
    public DataTypeException(Class domainClass, Object value) {
        super(new StringBuffer("Invalid domain value for ")
                  .append(domainClass.getName())
                  .append(": [")
                  .append(value).append("]").toString());
    }
}