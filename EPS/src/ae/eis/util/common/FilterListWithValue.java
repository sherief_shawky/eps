/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mohamed Fayek      05/05/2015  - File created.
 */
package ae.eis.util.common;

import java.util.List;
import java.util.ArrayList;


/**
 * Filter List With Value.
 *
 * @author Mohamed Fayek
 * @version 1.00
 */
public class FilterListWithValue<E> {
    
    /**
     * Filter List.
     * 
     * @param originalList : original List.
     * @param filter : filter.
     * @param text : text.
     * 
     * @return Filterd List.
     */
    public  <T> List filterList(List<T> originalList, FilterList filter, E text) {
        List<T> filterList = new ArrayList<T>();
        for (T object : originalList) {
            if (filter.isMatched(object, text)) {
                filterList.add(object);
            } else {
                continue;
            }
        }
        return filterList;
    }
} 
