/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  12/02/2008  - File created.
 * 
 * 1.01  Hamzeh abu lawi    28/02/2008  - Add getDouble,getFloat
 * 1.02  Mohammad Abulawi   05/07/2012  -TRF-6750, Add getDouble(Object)
 * 
 * 1.03  Ahmad M.Adawi      14/08/2012  - Add getReportTagDomainValues
 * 1.04  Mohammad Kabeel    06/08/2012  -TRF-7239, Add getStackTrace(Exception) Method.
 * 1.05  Bashar Alnemrawi   12/11/2012  - Add isValidPOBox(pobox) Method.
 * 
 * 1.06  Moh'd Abdul Jawad  22/09/2013  - Added (isValidMacAddress) Method.
 *                                      - Added (isValidIpAddress) Method.
 * 
 * 1.07  Ibrahim Hamarneh   31/05/2018  - Added (toDate) Method, Returns java.util.date from XMLGregorianCalendar
 * 
 */
 
package ae.eis.util.common;

import isoft.com.util.common.ServiceLocator;



import ae.eis.common.vo.ColumnMetaInfo;
import ae.eis.util.bus.PreferencesFacade;
import ae.eis.util.bus.PreferencesHandler;
import ae.eis.util.bus.RuleException;
import ae.eis.util.vo.BusinessRuleVO;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.DomainVO;
import ae.eis.util.vo.ValueObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.math.BigInteger;

import java.net.URL;
import java.net.URLConnection;

import java.security.SecureRandom;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

//import org.json.JSONException;
//import org.json.JSONObject;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * This class Contains common utility methods used by all tires.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.03
 */
public abstract class GlobalUtilities {
    /*
     * Constants and class variables.
     */

    /** UAE mobile number patteren. */
    private static String MOBILE_NUMBER_PATTEREN = "971(50|55|56|52|54|58)[0-9]{7}$";
    
    /** UAE phone number patteren. */
    private static String PHONE_NUMBER_PATTEREN = "0[2-9][0-9]{7}$";
    
    /** UAE mail number patteren. */
    private static String EMAIL_NUMBER_PATTEREN = "^[a-zA-Z0-9\\_\\-\\']+(\\.[a-zA-Z0-9\\_\\-\\']+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,4})$";
    
    /** English character pattern */
    private static String ENGLISH_CHARACTER_PATTEREN = "[A-Za-z0-9\\_\\.\\(\\)]";
    
    /** EID English character pattern */
    private static String EID_ENGLISH_CHARACTER_PATTEREN = "[A-Za-z0-9\\_\\.\\-\\(\\)]";
    
    /** Arabic character pattern */
    private static Pattern ARABIC_FIELD_PATTEREN = Pattern.compile("^[0-9\\\\\\ \\.\\-\\(\\)\\u0600-\\u06FF\\/]+$");
    
    /** Special character. */
    // "\u061f" refers to arabic question mark unicode
    private static String[] specialCharacters = new String[]{"?","\u061f"};
    
    /** English character without special character pattern. */
    private static String ENGLISH_CHARACTER_WITHOUT_SPEC_CHAR_PATTEREN = "[A-Za-z0-9\\-]";
    
    /** Eid number pattern.*/
    private static String EID_NUMBER_PATTERN = "^\\d{3}\\-\\d{4}\\-\\d{7}\\-\\d{1}$";

    /** Used to pare/format date web layer dates. */
    public static final SimpleDateFormat DATE_FORMAT = 
      new SimpleDateFormat("dd-MM-yyyy", Locale.US);

    /** Used to pare/format date web layer dates. */
    public static final SimpleDateFormat SHORT_DATE_FORMAT = 
      new SimpleDateFormat("dd/MM", Locale.US);
    public static final SimpleDateFormat HOURS24_DAY_MONTH_FORMAT
               = new SimpleDateFormat("HH dd/MM", Locale.US);
    
    public static final SimpleDateFormat HOURS12_DAY_MONTH_FORMAT
               = new SimpleDateFormat("hh:mm aa dd/MM", Locale.US);
    
    /** Used to pare/format date web layer dates. */
    public static final SimpleDateFormat YEAR_MONTH_DAY_DATE_FORMAT = 
      new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    /** Used to pare/format date database layer dates. */
    public static final SimpleDateFormat ORACLE_DATE_FORMAT =
      new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    /** Used to pare/format date database layer dates. */
    public static final SimpleDateFormat NO_SEPARATOR_DATE_FORMATTER =
      new SimpleDateFormat("yyyyMMddHHmmssmmm", Locale.US);
    
    /** Used to pare/format time web layer dates. */
    public static final SimpleDateFormat TIME_FORMAT = 
      new SimpleDateFormat("H:mm", Locale.US);
    
    /** Used to pare/format time web layer dates. */
    public static final SimpleDateFormat TIME_IN_SECONDS_FORMAT = 
      new SimpleDateFormat("HH:mm:ss", Locale.US);
    
    /** Date Time Format */ 
    public static final SimpleDateFormat DATE_TIME_FORMAT = 
      new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US);    

    /** Default date format: EEE, dd MMMM yyyy hh:mm:ss */
    public static final SimpleDateFormat DATE_TIME_SECONDS_FORMAT = 
                    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                    
    /** Default date format: EEE, dd MMMM yyyy hh:mm:ss.SSS */
    public static final SimpleDateFormat DATE_TIME_MILLISECONDS_FORMAT = 
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
                    
    /** ORACLE date format: EEE, dd MMMM yyyy hh:mm:ss */
    public static final SimpleDateFormat DATE_TIME_SECONDS_ORACLE_FORMAT = 
                    new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    
    /** HH:mm:ss*/
    public static final SimpleDateFormat TIME_FORMAT_SIMPLE_FORMAT = 
                    new SimpleDateFormat("HH:mm:ss");           
    /** IPAddress pattern : 111.111.111.111 */
    private static String IPADDRESS_PATTERN = "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$";
    
    /** Mac Address pattern : aa-bb-cc-aa-bb-cc */
    private static String MACADDRESS_PATTERN = "^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$";
    
    /** Device Key Pattern : dddd-dddd-dddd-dddd */
    private static String DEVICE_KEY_PATTERN = "^(\\d{1,4})\\-(\\d{1,4})\\-(\\d{1,4})$";
    
    /** Service Key Pattern : dddd-dddd-dddd-dddddddd-dddd */
    private static String SERVICE_KEY_PATTERN = "^(\\d{1,4})\\-(\\d{1,4})\\-(\\d{1,4})\\-(\\d{1,8})\\-(\\d{1,4})$";
    
    /** Used to pare/format time web layer dates. */
    public static final SimpleDateFormat TIME_24_FORMAT = new SimpleDateFormat("HH:mm", Locale.US);    
      
    /** Used to pare/format time web layer dates. */
    public static final SimpleDateFormat FULL_TIME_24_FORMAT = new SimpleDateFormat("HH:mm:ss", Locale.US);  
      
    /** EID first three number. */  
    public static final String EID_FIRST_NUM = "784";

    public static final String CONTENT_TYPE = "application/pdf"; 
    public static final String CONTENT_TYPE_XLS = "application/octet-stream";

    /** Used to pare/format time using the default application time format. */
    private static final SimpleDateFormat TIME_12_FORMAT =
        new SimpleDateFormat("hh:mm", Locale.US);    
    
    /** Used to pare/format time using the default application time format. */
    private static final SimpleDateFormat TIME_12_FORMAT_AM_PM = new SimpleDateFormat("hh:mm aa", Locale.US);    
    
     
    /** Used to parse/format dates using the new SDDI date format. */
    private static final SimpleDateFormat DATE_FORMAT_SDDI_FORMAT =
        new SimpleDateFormat("MM/dd/yyyy", Locale.US);
    
    /** ORACLE date format:  dd MMMM yyyy */
    public static final SimpleDateFormat DATE_TIME_ORACLE_FORMAT_AR = 
                    new SimpleDateFormat("yyyy/MM/dd");

     
    /*
     * Business objects
     */
    /** System preferences business object. */
    private static PreferencesFacade preferencesHandler = new PreferencesHandler();
    
    
    
    public static final String GEN_INQ_SRV_GMTPLUS4_FORMAT_REQUESTED = "GeneralInquiryService.GMTPlus4FormatRequested";
    public static final String GMTPLUS4_FORMAT_REQUESTED = "default.GMTPlus4FormatRequested";
    
    public static final String GMTPLUS4_FORMAT = "EEE MMM dd HH:mm:ss z yyyy";
    public static final String GMTPLUS4_TIMEZONE = "GMT+04:00";
    
    public static final int MODE_INCLUDE_ANY = 1;
    public static final int MODE_EXCLUDE_ALL = 2;

    /** Arabic character pattern  */
    private static final String ARABIC_CHARACTER_PATTEREN = "[0-9\\p{InArabic}\\ \\_\\.\\-\\(\\)]+";
    
    /** Display Date Format **/
    public static final String DISPLAY_DATE_FORMAT = "dd/MM/yyyy";
    
    /*
     * Methods
     */
    
    /**
     * Clear time from date object.
     * 
     * @param date Date object to be processed.
     * @return Date without time or null if the date parameter was null.
     */
    public static Date clearTime(Date date) {
        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);        
        
        return cal.getTime();
    }
    
    /**
     * Clear time from Calendar object.
     * 
     * @param cal Calendar object to be processed.
     * @return Calendar without time or null if the cal parameter was null.
     */
    public static Calendar clearTime(Calendar cal) {
        
        if (cal == null) {
            return null;
        }
        
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);        
        
        return cal;
    }
     
    /**
     * Clear time from date object.
     * 
     * @param date Date object to be processed.
     * @return Calendar without time or null if the date parameter was null.
     */
    public static Calendar clearTimeFromDate(Date date) {
        if (date == null) {
            return null;
        }
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);        
        
        return cal;
    }
    
    /**
     * Get business rule arabic description.
     * 
     * @param ruleKey business rule key.
     * @return business rule arabic description.
     */
    public static String getBusinessRuleDesc(String ruleKey) {
        BusinessRuleVO ruleVO = preferencesHandler.getBusinessRule(ruleKey);
        return (ruleVO != null) 
              ? ruleVO.getDescriptionAr() 
              : new StringBuffer("Business rule not found: ")
                   .append(ruleKey).toString();
    }

    /**
     * Get business rule arabic description.
     * 
     * @param ruleKey business rule key.
     * @param parameters Business rule parameters.
     * @return business rule arabic description.
     */
    public static String getBusinessRuleDesc(String ruleKey, String[] parameters) {
        BusinessRuleVO ruleVO = preferencesHandler.getBusinessRule(ruleKey, parameters);
        return (ruleVO != null) 
              ? ruleVO.getDescriptionAr() 
              : new StringBuffer("Business rule not found: ")
                   .append(ruleKey).toString();
    }

     
    /**
     * Get domain values.
     * 
     * @param key Domain value key.
     * @return Domain values info.
     */
    public static DomainVO[] getDomainValues(String key) {
        return preferencesHandler.getDomainValues(key);
    }

    /**
     * Get domain value.
     * 
     * @param key Domain value key.
     * @param value Domain value.
     * @return Domain values info.
     */
    public static DomainVO getDomainValue(String key, Integer value) {
        return preferencesHandler.getDomainValue(key, value);
    }

    /**
     * Get Description By Value
     * 
     * @param key Domain value key.
     * @param value Domain value.
     * 
     * @return Domain values description.
     */
    public static String getDescriptionByValue(String key, String value) {
        return preferencesHandler.getDescriptionByValue(key, value);
    }
    
    /**
     * Get domain for Without values.
     * @return  domain for specfic input values.
     * @param values
     * @param key
     */
    public static DomainVO[] getDomainWithoutValues(String key, Integer values[]) {
        return preferencesHandler.getDomainWithoutValues(key, values);
    }
    
    /**
     * Used to generate date representation
     */
    public static String formatDate(Date date) {
        if (date == null) {
            return null;
        } 

        return DATE_FORMAT.format(date);
    }
    
    
    /**
     * Used to generate date representation
     */
    public static String shortFormatDate(Date date) {
        if (date == null) {
            return null;
        } 

        return SHORT_DATE_FORMAT.format(date);
    }
    
    /**
     * Used to generate date representation
     */
    public static String formatHours24DayMonthDate(Date date) {
        if (date == null) {
            return null;
        } 

        return HOURS24_DAY_MONTH_FORMAT.format(date);
    }
    
    /**
     * Used to generate date representation
     */
    public static String formatHours12DayMonthDate(Date date) {
        if (date == null) {
            return null;
        } 

        return HOURS12_DAY_MONTH_FORMAT.format(date);
    }
    
    /**
     * Used to generate date representation
     */
    public static String formatYearDate(Date date) {
        if (date == null) {
            return null;
        } 

        return YEAR_MONTH_DAY_DATE_FORMAT.format(date);
    }
    
    /**
     * Used to generate time representation
     */
    public static String formatTime(Date time) {
        if (time == null) {
            return null;
        }

        return TIME_FORMAT.format(time);
    }

    /**
     * Used to generate date representation
     */
    public static String formatDate(long timeMillis) {
        return formatDate(new Date(timeMillis));
    }
    
    /**
     * Used to generate date representation
     * 
     * @param date : Date.
     * @return string representation of Date.
     */
    public static String formatOracleDate(Date date) throws ParseException {
        if (date == null) {
            return null;
        }

        return ORACLE_DATE_FORMAT.format(date);
    }
    
    /**
     * Used to generate No Separator date representation
     * 
     * @param date : Date.
     * @return No Separator string representation of Date.
     */
    public static String formatNoSeparatorDate(Date date) throws ParseException {
        if (date == null) {
            return null;
        }
        
        return NO_SEPARATOR_DATE_FORMATTER.format(date);
    }
    
    /**
     * Used to generate date representation
     * 
     * @param date : Date.
     * @return string representation of Date.
     */
    public static String formatFullTimeIn24(Date date) throws ParseException {
        if (date == null) {
            return null;
        }

        return TIME_IN_SECONDS_FORMAT.format(date);
    }

    /**
     * Checks if the passed value object is null based on its type
     * Note: Supported objects type are only : String , Collection , Map , Number
     * Other wise this method will throw runtime exception
     *
     * @param value Object to be checked.
     * @return true if the value is not null nor empty collection nor emprty or blank string
     */
    public static boolean isBlankOrNull( Object value ) {
        
        if ( value == null ) {
            return true;
        } else if ( value instanceof Collection ) {
            return isEmpty( (Collection) value );
        } else if ( value instanceof Map ) {
            return ( (Map) value ).keySet().size() == 0;
        } else if ( value instanceof String ) {
            return isBlankOrNull( value.toString() );
        } else if ( value instanceof Number ) {
             return false;
        } else {
            return isBlankOrNull( value.toString() );
//            throw new RuntimeException ("Invalid type ( " + value.getClass().getName() + "). isBlankOrNull cannot check object types other than String , Collection , Map , Number");
        }       
    }
    
    /**
     * Checks if the field is not null or contains only blank spaces.
     * 
     * @param value String value to be checked.
     * @return true if the field is not null or blank.
     */
    public static boolean isBlankOrNull(String value) {
       return ((value == null) || (value.trim().length() == 0) || (value.trim().equals("null")));
    }

    /**
     * Parse date string
     * 
     * @param date Date string.
     * @return Date object.
     */
    public static Date parseDate(String date) throws ParseException {
        if (isBlankOrNull(date)) {
            return null;
        }

        return DATE_FORMAT.parse(date);
    }
    
    /**
     * Parse Oracle date string
     * 
     * @param date Date string.
     * @return Date object.
     */
    public static Date parseOracleDate(String date) throws ParseException {
        if (isBlankOrNull(date)) {
            return null;
        }

        return ORACLE_DATE_FORMAT.parse(date);
    }    
    
    /**
     * Parse date time string
     * 
     * @param date Date string.
     * @return Date object.
     */
    public static Date parseDateTime(String date) throws ParseException {
        if (isBlankOrNull(date)) {
            return null;
        }
    
        return DATE_TIME_FORMAT.parse(date);
    }  
    
    /**
     * Used to generate time representation
     */
    public static String formatDateTimeSeconds(Date time) {
        if (time == null) {
            return null;
        }

        return DATE_TIME_SECONDS_FORMAT.format(time);
    }
    
    /**
     * Used to generate time with millisecond representation
     */
    public static Date formatDateTimeMilliseconds(String dateTimeMillisecond) throws ParseException {
        if (dateTimeMillisecond == null) {
            return null;
        }

        return DATE_TIME_MILLISECONDS_FORMAT.parse(dateTimeMillisecond);
    }
    
    
    /**
     * Used to generate time Oracle representation
     */
    public static String formatDateTimeSecondsOracle(Date time) {
        if (time == null) {
            return null;
        }

        return DATE_TIME_SECONDS_ORACLE_FORMAT.format(time);
    }    
    
    /**
     * Used to generate time representation
     */
    public static String formatFullTime(Date time) {
        if (time == null) {
            return null;
        }

        return TIME_FORMAT_SIMPLE_FORMAT.format(time);
    } 
    
    
    /**
     * Check if this is a valid date value.
     * 
     * @param date Date string.
     * @return true if this is a valid date value.
     */
    public static boolean isValidDate(String date) {
        if (date == null || date.length() == 0) {
            return true;
        }
        
        try {
            DATE_FORMAT.parse(date);
            return true;
            
        } catch (Exception ex)  {
            return false;
        }
    }
    
    /**
     * Check if this is a valid Oracle date value.
     * 
     * @param date Date string.
     * @return true if this is a valid date value.
     */
    public static boolean isValidOracleDate(String date) {
        if (date == null || date.length() == 0) {
            return true;
        }
        
        try {
            ORACLE_DATE_FORMAT.parse(date);
            return true;
            
        } catch (Exception ex)  {
            return false;
        }
    }
    
    /**
     * Parse time string
     * 
     * @param time Date string.
     * @return Date object.
     */
    public static Date parseTime(String time) throws ParseException {
        if (isBlankOrNull(time)) {
            return null;
        }

        return TIME_FORMAT.parse(time);
    }

    /**
     * Parse time string
     * 
     * @param time Date string.
     * @return Date object.
     */
    public static Date parse24Time(String time) throws ParseException {
        if (isBlankOrNull(time)) {
            return null;
        }

        return TIME_24_FORMAT.parse(time);
    }

    /**
     * Checks if the field contains a long value.
     * 
     * @param value String value to be checked.
     * @return true if the field contains a long value.
     */
    public static boolean isLong(String value) {
       if (isBlankOrNull(value)) {
           return false;
       }

       try {
           Long.parseLong(value);
           return true;
       } catch (Exception ex) {
           return false;
       }
    }

    /**
     * Checks if the field contains a int value.
     * 
     * @param value String value to be checked.
     * @return true if the field contains a long value.
     */
    public static boolean isInteger(String value) {
       if (isBlankOrNull(value)) {
           return false;
       }

       try {
           Integer.parseInt(value);
           return true;
       } catch (Exception ex) {
           return false;
       }
    }
    /**
     * Get Long object or null if the value is null or empty string.
     * 
     * @param value String value to be tested and parsed.
     * @return Long object or null if the value is null or empty string.
     */
    public static Long getLong(Object value) {
        if (value == null) {
            return null;
        }
        
        if (value instanceof Long) {
            return (Long) value;
        }
        
     
        
        if (value instanceof Number) {
            return new Long(((Number)value).longValue());
        }
        
        String str = getString(value);
        if (isBlankOrNull(str)) {
            return null;
        }
        
        return new Long(str.trim());
    }

    /**
     * Get Double object or null if the value is null or empty string.
     * 
     * @param value Object value to be tested and parsed.
     * @return Double object or null if the value is null or empty string.
     */
    public static Double getDouble(Object value) {
        if (value == null) {
            return null;
        }
        
        if (value instanceof Double) {
            return (Double) value;
        }
        
        
        String str = getString(value);
        if (isBlankOrNull(str)) {
            return null;
        }
        
        return new Double(str.trim());
    }
    
    /**
     * Get Date object or null if the value is null or empty string.
     * 
     * @param value Object to be tested and parsed.
     * @return date object or null if the value is null or empty string.
     */
    public static Date getDate(Object value) throws ParseException {
        
        if (value == null) {
            return null;
        }
        
        if (value instanceof Date) {
            return (Date) value;
        }

        String str = getString(value);
        if (isBlankOrNull(str)) {
            return null;
        }
        
        return parseDate(str);
    }
    
    /**
     * Get Date object or null if the value is null or empty time.
     * 
     * @param time  :   Long values to be converted
     * @return date :   Date representaion of the passed time
     */
    public static Date getDateByTime(Long time) {
        
        if (time == null) {
            return null;
        }
        
        return new Date(time);
    }
    
    /**
     * Get Date object or null if the value is null or empty string.
     * 
     * @param value Object to be tested and parsed.
     * @return date object or null if the value is null or empty string.
     */
    public static Date getDateTime(Object value) throws ParseException {
        if (value == null) {
            return null;
        }
        
        if (value instanceof Date) {
            return (Date) value;
        }

        String str = getString(value);
        if (isBlankOrNull(str)) {
            return null;
        }
        
        return parseDateTime(str);
    }
    
    /**
     * Get Double object or null if the value is null or empty string.
     * 
     * @param value String value to be tested and parsed.
     * @return Double object or null if the value is null or empty string.
     */
    public static Double getDouble(String value) {
        if (isBlankOrNull(value)) {
            return null;
        }
        
        return new Double(value);
    }
    
    /**
     * Get Float object or null if the value is null or empty string.
     * 
     * @param value Object value to be tested and parsed.
     * @return Float object or null if the value is null.
     */
    public static Float getFloat(Object value) {
        if(value == null) {
            return null;
        }
        String valueStr = getString(value);
        if (isBlankOrNull(valueStr)) {
            return null;
        }
        
        return new Float(valueStr);
    }
    
    /**
     * Get Integer object or null if the value is null or empty string.
     * 
     * @param value String value to be tested and parsed.
     * @return Integer object or null if the value is null or empty string.
     */
    public static Integer getInteger(String value) {
        if (isBlankOrNull(value)) {
            return null;
        }
        
        return new Integer(value.trim());
    }
    
    /**
     * Get Boolean value if the value is null or not true value.
     * 
     * @param value Object value to be tested and parsed.
     * @return Boolean value (true/false).
     */
    public static boolean getBoolean(Object value) {
        
        if (value == null) {
            return false;
        }
        
        if (value instanceof Integer) {
            return getInteger(value).intValue() == Choice.YES.getInteger().intValue();
        }
        
       
        
        if(value instanceof Choice) {
            return getChoice(value).getBoolean();
        }
        
        return value.toString().equalsIgnoreCase("true");
    }
    
    /**
     * Get Integer object or null if the value is null or empty string.
     * 
     * @param value String value to be tested and parsed.
     * @return Integer object or null if the value is null or empty string.
     */
    public static Integer getInteger(Object value) {
        
        if (value == null) {
            return null;
        }
        
        if (value instanceof Integer) {
            return (Integer) value;
        }
        
        
        
        if (value instanceof Number) {
            return new Integer(((Number)value).intValue());
        }
        
        String strValue = getString(value);
        
        if (isBlankOrNull(strValue)) {
            return null;
        }
        
        return new Integer(strValue.trim());
    }
    
    /**
     * Get Choice object or null if the value is null or empty string.
     * 
     * @param value String value to be tested and parsed.
     * 
     * @return Choice object or null if the value is null or empty string.
     */
    public static Choice getChoice(Object value) {
        
        if(value == null) {
            return null;
        }
        
        if (value instanceof Boolean) {
            return new Choice(getBoolean(value));
        }
        
        Integer intValue = getInteger(value);

        if (intValue == null) {
            return null;
        }
        
        return new Choice(intValue);
    }
    
    /**
     * check if input email is valied or not
     * 
     * @return if input email is valied or not 
     * @param email
     */
    public static boolean isValidUAEEmail(String email) {
        Pattern pattern = null;
        pattern = Pattern.compile(EMAIL_NUMBER_PATTEREN);
        Matcher m = pattern.matcher(email);
        return m.matches();
    }

    /**
     * check if input phone is valied or not
     * 
     * @return  if input phone is valied or not
     * @param phone
     */
    public static boolean isValidUAEPhone(String phone) {
        Pattern pattern = null;
        pattern = Pattern.compile(PHONE_NUMBER_PATTEREN);
        Matcher m = pattern.matcher(phone);
        boolean isValid = m.matches();
        if(!isValid){
            return false;
        }
        
        String subPhone = phone.substring(2,phone.length());
        if(subPhone.equals("0000000")){
            return false;
        }          
        return true;
    }
    
    /**
     *  check if input mobile is valied or not
     *  
     * @return 
     * @param mobile
     */
    public static boolean isValidUAEMobile(String mobile) {
        Pattern pattern = null;
        pattern = Pattern.compile(MOBILE_NUMBER_PATTEREN);
        Matcher m = pattern.matcher(mobile);
        boolean isValid = m.matches();
        if(!isValid){
            return false;
        }        
        String subMobile = mobile.substring(5,mobile.length());
        
        if(subMobile.equals("0000000")){
            return false;
        }
        return true;
    }
    
    /**
     * Is English Text
     * return true if all charecters are english, otherwise return false
     * 
     * @return boolean ( true / false )
     * @param text
     */
    public static boolean isEnglishText(String text) {
        if( isBlankOrNull(text) ){
            return true;
        }
        
        Pattern pattern = null;
        pattern = Pattern.compile(ENGLISH_CHARACTER_PATTEREN);
        Matcher m = null;
        for (int i = 0 ; i < text.length(); i++)  {
            CharSequence index = text.subSequence(i,i+1);
            if ( !isBlankOrNull(index.toString()) ) {
                m = pattern.matcher(index);
                if (m.find() == false) {
                    return false;
                }                
            }

        }
         
        return true;
    }
    
    /**
     * Is Arabic Text
     * return true if all charecters are Arabic, otherwise return false
     * 
     * @return boolean ( true / false )
     * @param text
     */
    public static boolean isValidArabicText(String value) {
        if (value == null) {
            return true;
        }
        String text = value.toString();
        if(text.trim().length() == 0 ){
          return true;
        }
        for(String charcter : specialCharacters){
          text = new String(text.toLowerCase().replace(charcter,""));
        }
        
        if(ARABIC_FIELD_PATTEREN.matcher(text).matches()){
            return true;
        }
        return false;   
    }
    
    /**
     * Is EID English Name
     * return true if all charecters are allowed for EID english Names, otherwise return false
     * 
     * @return boolean ( true / false )
     * @param text
     */
    public static boolean isValidEidEnglishName(String text) {
        if( isBlankOrNull(text) ){
            return true;
        }
        
        Pattern pattern = null;
        pattern = Pattern.compile(EID_ENGLISH_CHARACTER_PATTEREN);
        Matcher m = null;
        for (int i = 0 ; i < text.length(); i++)  {
            CharSequence index = text.subSequence(i,i+1);
            if ( !isBlankOrNull(index.toString()) ) {
                m = pattern.matcher(index);
                if (m.find() == false) {
                    return false;
                }                
            }

        }
         
        return true;
    }
    /**
     * Get string representation of this object
     * 
     * @param obj Object to be parsed,
     * @return string representation of this object
     */
    public static String getString(Object obj) { 
        
        if (obj == null) {
            return null;
        }
        
        if(obj instanceof Date){
            boolean isGMTFormatRequested = false;
            if(isGMTFormatRequested){
                DateFormat formatter = new SimpleDateFormat(GMTPLUS4_FORMAT,Locale.ENGLISH);
                formatter.setTimeZone(TimeZone.getTimeZone(GMTPLUS4_TIMEZONE));
                
                return formatter.format(obj);
            }else{
                return obj.toString();
            }
        }
        
        return obj.toString();
    }
    
    
    public static String DateToGMTplus4Format(Date dateObj) {
        boolean isGMTFormatRequested = true;
        
        if(dateObj != null){
            if(isGMTFormatRequested){
                DateFormat formatter = new SimpleDateFormat(GMTPLUS4_FORMAT,Locale.ENGLISH);
                formatter.setTimeZone(TimeZone.getTimeZone(GMTPLUS4_TIMEZONE));
                
                return formatter.format(dateObj);
            }else{
                return dateObj.toString();
            }
        }
        return null;
    }
    
    /**
     * Get domain values.
     * 
     * @return Domain values info.
     * @param key Domain value key.
     * @param values 
     */
    public static DomainVO[] getDomainValues(String key, Integer values[]) {
        return preferencesHandler.getDomainValues(key,values);
    }
    
    /**
     * Get Report Tag Domain Values
     * 
     * @param whereClouse
     * @param cloumnName
     * @param tableName
     * 
     * @return Array of Domain Value Object
     */
    public static DomainVO[] getReportTagDomainValues (String tableName,
                                                String cloumnName, 
                                                String whereClouse){
        return preferencesHandler.getReportTagDomainValues(tableName,cloumnName,whereClouse);
    }    
    
    /**
     * Convert the passed string to ascii Code
     * 
     * @param text passed text
     * @return Converted string as ascii Code
     */
    public static String convertToAsciiCode(String text) {
    
        String asciiCodeString = "";
        if(text!=null && text.length() > 0) {
            
            char [] toCharArray = text.toCharArray();
            
            for(int i=0;i<toCharArray.length;i++) {
                
                int toInteger = (int)toCharArray[i];
                
                asciiCodeString+="&#"+toInteger+";";
            }
        }
        
        return asciiCodeString;
    }
    
    /**
     * Used to generate date representation
     */
    public static String formatDateTime(Date date) {
        if (date == null) {
            return null;
        }

        return DATE_TIME_FORMAT.format(date);
    }
    
    /**
     * Replace all oldValue occurences on the src text with newValue.
     * 
     * @param src Source text to be updated,
     * @param oldValue Old token value.
     * @param newValue New token value.
     * @return Updated text.
     */
    public static StringBuffer replaceAll(StringBuffer src, 
                                       String oldValue, 
                                       Object newValue) {
        // Validate parameters
        if (oldValue == null || oldValue.length() == 0) {
            throw new IllegalArgumentException(
                "Old value must not be null or empty String");
        }

        if (newValue == null) {
            throw new IllegalArgumentException("New value must not be null");
        }

        if (src == null || isBlankOrNull(src.toString())) {
            return src;
        }

        while (src.indexOf(oldValue) >= 0) {
            int start = src.indexOf(oldValue);
            int end = start + oldValue.length();
            src.replace(start, end, newValue.toString());
        }
        
        // return updated text
        return src;
    }

    /**
     * Get number of Months difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Number of Months
     */
    public static int getMonthsDifference(Date firstDate, Date secondDate) {
        
        if(firstDate == null) {
            
            throw new IllegalArgumentException("First date cannot be null");
        }
        
        if(secondDate == null) {
            
            throw new IllegalArgumentException("Second date cannot be null");
        }
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(firstDate);
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(secondDate);

        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
        
        return diffMonth;
    }
    
    /**
     * Get number of days difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Number of days
     */
    public static long getDaysDifference(Date firstDate, Date secondDate) {
        
        if(firstDate == null) {
            
            throw new IllegalArgumentException("First date cannot be null");
        }
        
        if(secondDate == null) {
            
            throw new IllegalArgumentException("Second date cannot be null");
        }        
        
        Calendar firstDateCalendar = Calendar.getInstance();
        firstDateCalendar.setTime(firstDate);

        firstDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        firstDateCalendar.set(Calendar.MINUTE, 0);
        firstDateCalendar.set(Calendar.SECOND, 0);
        firstDateCalendar.set(Calendar.MILLISECOND, 0);
        
        Calendar secondDateCalendar = Calendar.getInstance();
        secondDateCalendar.setTime(secondDate);          

        secondDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        secondDateCalendar.set(Calendar.MINUTE, 0);
        secondDateCalendar.set(Calendar.SECOND, 0);  
        secondDateCalendar.set(Calendar.MILLISECOND, 0);
        
        return (Math.abs(
                        secondDateCalendar.getTimeInMillis() 
                        - firstDateCalendar.getTimeInMillis()) /(1000*60*60*24)
                );
    }
        
    /**
     * Clear Day/Month/Year Part From Date Object.
     * 
     * @param date Date Object To Be processed.
     * @return Date Without Day/Month/Year Or null If The Date Parameter Was null.
     */
	public static Date clearAllExceptTime(Date date) {

        if(date == null) {
            return null;
        }

		Calendar cal = Calendar.getInstance();
        cal.clear();

        cal.set(Calendar.HOUR_OF_DAY, date.getHours());
        cal.set(Calendar.MINUTE, date.getMinutes());
        cal.set(Calendar.SECOND, date.getSeconds());
        
		return cal.getTime();
	}
    
    /**
     * Clears Date and Seconds from the passed data object.
     *
     * @param date Date Object To Be processed.
     * @return Date Without Day/Month/Year Or null If The Date Parameter Was null.
     */
    public static Date clearDateAndSeconds(Date date) {

        if (date == null) {
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.clear();

        cal.set(Calendar.HOUR_OF_DAY, date.getHours());
        cal.set(Calendar.MINUTE, date.getMinutes());
        cal.set(Calendar.SECOND, 0);

        return cal.getTime();
    }

    /**
     * Get Minutes difference between two dates
     *
     * @param firstDate First Date
     * @param secondDate Second Date
     *
     * @return Minutes
     */
    public static long getMinutesDifference(Date firstDate, Date secondDate) {
        
        if(firstDate == null) {
            
            throw new IllegalArgumentException("First date cannot be null");
        }
        
        if(secondDate == null) {
            
            throw new IllegalArgumentException("Second date cannot be null");
        }        
        
        Calendar firstDateCalendar = Calendar.getInstance();
        firstDateCalendar.setTime(firstDate);

        firstDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        firstDateCalendar.set(Calendar.MINUTE, 0);
        firstDateCalendar.set(Calendar.SECOND, 0);
        firstDateCalendar.set(Calendar.MILLISECOND, 0);
        
        Calendar secondDateCalendar = Calendar.getInstance();
        secondDateCalendar.setTime(secondDate);          

        secondDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        secondDateCalendar.set(Calendar.MINUTE, 0);
        secondDateCalendar.set(Calendar.SECOND, 0);  
        secondDateCalendar.set(Calendar.MILLISECOND, 0);
        
        return (Math.abs(
                        secondDateCalendar.getTimeInMillis() 
                        - firstDateCalendar.getTimeInMillis()) /(1000*60)
                );
    }
     
    /**
     * Get Minutes difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Minutes
     */
    public static long getMinutesDifferenceAfterIgnoreDates(Date firstDate, Date secondDate) {
        
        if(firstDate == null) {
            
            throw new IllegalArgumentException("First date cannot be null");
        }
        
        if(secondDate == null) {
            
            throw new IllegalArgumentException("Second date cannot be null");
        }        
        
        Calendar firstDateCalendar = Calendar.getInstance();
        firstDateCalendar.setTime(clearAllExceptTime(firstDate));
 
        Calendar secondDateCalendar = Calendar.getInstance();
        secondDateCalendar.setTime(clearAllExceptTime((secondDate)));          
 
        return (Math.abs(
                        secondDateCalendar.getTimeInMillis() 
                        - firstDateCalendar.getTimeInMillis()) /(1000*60)
                ); 
    }
    
    /**
     * Used to generate date representation
     * @param date Date
     * @param pattern Date Pattern
     */
    public static String formatDate(Date date, String pattern) {
        
        if (date == null || isBlankOrNull(pattern)) {
            return null;
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat(pattern,Locale.US);

        return sdf.format(date);
    }

    /**
     * Reverse String
     * 
     * @param source Source
     * 
     * @return reverted string
     */
    public static String reverseString(String source) {
        int i, len = source.length();
        StringBuffer dest = new StringBuffer(len);

        for (i = (len - 1); i >= 0; i--) {
            dest.append(source.charAt(i));
        }
        return dest.toString();
    }
    
    /**
     * Get Date Format
     * 
     * @return Date Format
     */    
    public static synchronized SimpleDateFormat getDateFormat(String dateFormat) {
        return new SimpleDateFormat(dateFormat, Locale.US);
    }
    
    /**
     * Parse date string
     * 
     * @return Date.
     */    
    public static synchronized Date parseSoapDateTime(String date)  throws ParseException {
        return (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)).parse(date);
    }
    
    /**
     * Get Date Format
     * 
     * @return String Date Formated
     */    
    public static synchronized String formatSoapDateTime(Date date) {
        return (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)).format(date);
    }

    /**
     * Check Special Characters
     * 
     * @param msg massage
     * @param excecludedCharacters excecluded characters
     * 
     * @return true if the massage contain Special Characters
     */
    public static synchronized  boolean checkSpecialCharacters(String msg,String excecludedCharacters) {
        String iChars = "!@#$%^&*+=-[]\';,/{}|:\"<>?~_";
        for(int i=0; i<msg.length(); i++){
            if(iChars.indexOf(msg.charAt(i)) !=-1){
                if(excecludedCharacters == null || 
                   excecludedCharacters.length() == 0 ||
                   excecludedCharacters.indexOf(msg.charAt(i))==-1 ){
                     return true;
                 }
            }
        }
        return false;
    }
    
    /**
     * Get Exception Stack Trace as String Format
     * 
     * @param ex : Global Exception
     * @return Stack Trace as String Format
     */
    public static String getStackTrace(Exception ex) {


        StringWriter str = new StringWriter();
        PrintWriter writer = new PrintWriter(str);
        ex.printStackTrace(writer);
        return str.getBuffer().toString();
    }
    
    /**
     * Get Exception Stack Trace as String Format
     * 
     * @param ex : Global Exception
     * @return Stack Trace as String Format
     */
    public static String getStackTrace(Throwable ex) {

        

        StringWriter str = new StringWriter();
        PrintWriter writer = new PrintWriter(str);
        ex.printStackTrace(writer);
        return str.getBuffer().toString();
    }
    
        /**
     * Get Exception Stack Trace as String Format
     * 
     * @param ex : Global Exception
     * @param maxLength : Maximum Stack Trace Length.
     * 
     * @return Stack Trace as String Format
     */
    public static String getStackTrace(Throwable ex, int maxLength) {
    
        StringWriter str = new StringWriter();
        PrintWriter writer = new PrintWriter(str);
        ex.printStackTrace(writer);
        
        String stackTrace = str.getBuffer().toString();
        
        if (!isBlankOrNull(stackTrace) && stackTrace.length() > maxLength) {
            stackTrace = stackTrace.substring(0, maxLength);
        }
        
        return stackTrace;
    }
    
    /**
     * Used to generate time representation
     */
    public static synchronized Date formatDateTimeSeconds(String date) throws ParseException {
        if (isBlankOrNull(date))  {
            return null;
        }
        
        return (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)).parse(date);
    }

    /**
     * Split source list into smaller lists with a specific size. This method
     * is commonly used by presentation layer to view search pages result where
     * earch row contains more than one search result.
     * 
     * @param srcList Source list to be splited.
     * @param size Max list size.
     * @param emptyElement Object to add if the number of elements per row is 
     *                     less than row size.
     * @return New array od lists where each list has a max fixed size.
     */
    public static List[] split(List srcList, int size, Object emptyElement) {
        return split(srcList, size, true, emptyElement);
    }

    /**
     * Split source list into smaller lists with a specific size. This method
     * is commonly used by presentation layer to view search pages result where
     * earch row contains more than one search result.
     * 
     * @param srcList Source list to be splited.
     * @param size Max list size.
     * @param emptyElement Object to add if the number of elements per row is 
     *                     less than row size.
     * @return New array od lists where each list has a max fixed size.
     */
    public static List[] split(List srcList, int size, 
                   boolean appendEmptyElements, Object emptyElement) {
        // Validate source list
        if (srcList == null || srcList.size() == 0) {
            return new List[0];
        }
        
        // Validate requested list size
        if (size <= 0) {
            throw new IllegalArgumentException("Invalid split size: "+new Integer(size));
        }
        
        // Check if source-list size is less or equal requested list size
        if (srcList.size() <= size) {
            // Append empty elements
            if (appendEmptyElements) {
                List newList = new ArrayList(srcList);
                int missingElemntsCount = size - newList.size();

                for (int i = 0; i < missingElemntsCount; i++) {
                    newList.add(emptyElement);
                }
                
                return new List[]{newList};
            } else {
                return new List[]{srcList};
            }
        }
        
        // Split source list to sub-lists
        List tempList = new ArrayList();
        List newList = new ArrayList();
        newList.add(tempList);
        for (int i = 0; i < srcList.size(); i++)  {
            if (tempList.size() >= size) {
                tempList = new ArrayList();
                newList.add(tempList);
            }
            
            tempList.add(srcList.get(i));
        }

        // Append empty elements
        if (appendEmptyElements) {
            List lastList = (List) newList.get(newList.size() - 1);
            int missingElemntsCount = size - lastList.size();
            if (lastList.size() < size) {
                for (int i = 0; i < missingElemntsCount; i++) {
                    lastList.add(emptyElement);
                }
            }
        }
        
        return (List[]) newList.toArray(new List[newList.size()]);
    }
    
   /**
    * Check P.O.Box Is Number Or Not Have Zero Or Spical Characters
    *
    * @param poBox: P.O.Box Person And Organization Box
    * @return ture if P.O.Box is Number Or Not Have Zero
    *         Or Not Have Spical Characters else return false.
    */
    public static boolean isValidPOBox(String pobox) {
        
        if (isBlankOrNull(pobox))  {
            return false;
        }
        
        if (!isLong(pobox))  {
            return false;
        }
        
        boolean isPoBoxValid = false;
        for (int i = 0 ; i < pobox.length() ; i++ )  {
            if (pobox.charAt(i) != '0')  {
                isPoBoxValid = true;
                break;
            }
            
        }
        return isPoBoxValid;
    }

    /**
     * Tokenize Sting To List
     * 
     * @param string : Original String
     * @param delimeter : string delimeter
     * @return String tokens added in List
     */
    public static List<String> tokenizeStingToList(String string,String delimeter) {
        
        if(isBlankOrNull(string) || isBlankOrNull(delimeter)) {
            return null;
        }
        
        List<String> tokenList = new ArrayList<String>();
        StringTokenizer servicesTokenizer = new StringTokenizer(string,delimeter);
        while (servicesTokenizer.hasMoreTokens()) {
            String token = servicesTokenizer.nextToken();
            tokenList.add(token);
        }
        
        return tokenList;
    }
    
    /**
     * Tokenize Ids To List
     * 
     * @param string : Original String
     * @param delimeter : string delimeter
     * @return Long tokens added in List
     */
    public static List<Long> tokenizeIdsToList(String stringVal,String delimeter) {
        
        if(isBlankOrNull(stringVal) || isBlankOrNull(delimeter)) {
            return null;
        }
        
        List<Long> tokenList = new ArrayList<Long>();
        StringTokenizer servicesTokenizer = new StringTokenizer(stringVal,delimeter);
        while (servicesTokenizer.hasMoreTokens()) {
            tokenList.add(getLong(servicesTokenizer.nextToken()));
        }
        
        return tokenList;
    }

    /**
     * Format The Date in +4 Time Zone
     * 
     * @param responseDate Response Date
     * @return Formated Date by +4 Time Zone
     */
    public static synchronized Date formatDateTimeZone(String responseDate) throws Exception{
        if(isBlankOrNull(responseDate)) {
            return null;
        }
        Date date = GlobalUtilities.DATE_TIME_SECONDS_FORMAT.parse(responseDate);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getTime(); 
    }
    
    /**
     * Check ORA Exceptions
     * 
     * @param ex Exception
     * @return Business Message if it is exist in standard format
     */
    public static synchronized String checkOraExceptions(Exception ex) {
        
        String exceptionStackTrace = getStackTrace(ex);
        String exceptionMessage = "";
        if(!GlobalUtilities.isBlankOrNull(exceptionStackTrace) && 
           ((exceptionStackTrace.indexOf("::") != -1 &&
             exceptionStackTrace.lastIndexOf("::") != -1) ||
            (exceptionStackTrace.indexOf("[[") != -1 &&
             exceptionStackTrace.indexOf("]]") != -1))) {
            
            if(exceptionStackTrace.indexOf("::") != -1 &&
               exceptionStackTrace.lastIndexOf("::") != -1) {  

                exceptionMessage = exceptionStackTrace.substring(exceptionStackTrace.
                                indexOf("::") + "::".length(), 
                                exceptionStackTrace.lastIndexOf("::"));
            }
            
            if(exceptionStackTrace.indexOf("[[") != -1 &&
               exceptionStackTrace.indexOf("]]") != -1) {

                exceptionMessage = exceptionStackTrace.substring(exceptionStackTrace.
                                indexOf("[[") + "[[".length(), 
                                exceptionStackTrace.indexOf("]]"));
            }                
                                                                    
        }
        
        return exceptionMessage;
    }


    /**
     * Get Year Month Day Date Format object or null if the value is null or empty string.
     * 
     * @param value Object to be tested and parsed.
     * @return date object or null if the value is null or empty string.
     */
    public static Date getYearMonthDayDateFormat(Object value) throws ParseException {
        if (value == null) {
            return null;
        }
        
        if (value instanceof Date) {
            return (Date) value;
        }

        String str = getString(value);
        if (isBlankOrNull(str)) {
            return null;
        }
        return parseDateFormat(str);
    }

    /**
     * Parse date Format
     * 
     * @param date Date string.
     * @return Date object.
     */
    public static Date parseDateFormat(String date) throws ParseException {
        if (isBlankOrNull(date)) {
            return null;
        }

        return YEAR_MONTH_DAY_DATE_FORMAT.parse(date);
    }
    
    /**
     * Get number of years difference between two dates
     * 
     * @param date Date
     * 
     * @return Number of years difference
     */
    public static int getYearsDifference(Date date) {
        
        if(date == null) {
            throw new IllegalArgumentException("Date cannot be null");
        }
        Calendar firstDateCalendar = Calendar.getInstance(Locale.ENGLISH);
        firstDateCalendar.setTime(date);
        Calendar sysDate = Calendar.getInstance(Locale.ENGLISH);
        
        return sysDate.get(sysDate.YEAR) - firstDateCalendar.get(firstDateCalendar.YEAR);
    }
    
    /**
     * Get number of years difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Number of days
     */
    public static Date addDaysToDate(Date date, int days) {
        if(date == null) {
            
            throw new IllegalArgumentException("Date cannot be null");
        }
        if(days == 0) {
            
            throw new IllegalArgumentException("Days cannot be Zero");
        }
        
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
       
        dateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        dateCalendar.set(Calendar.MINUTE, 0);
        dateCalendar.set(Calendar.SECOND, 0);
        dateCalendar.set(Calendar.MILLISECOND, 0);
        
        dateCalendar.add(Calendar.DATE, days);
        
        return dateCalendar.getTime();
    }
    /**
     * Get number of years difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Number of days
     */
    public static Date addMonthsToDate(Date date, int months) {
        if(date == null) {
            
            throw new IllegalArgumentException("Date cannot be null");
        }
        if(months == 0) {
            
            throw new IllegalArgumentException("Days cannot be Zero");
        }
        
        Calendar dateCalendar = Calendar.getInstance(Locale.ENGLISH);
        dateCalendar.setTime(date);
       
        dateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        dateCalendar.set(Calendar.MINUTE, 0);
        dateCalendar.set(Calendar.SECOND, 0);
        dateCalendar.set(Calendar.MILLISECOND, 0);
        
        dateCalendar.add(Calendar.MONTH, months);
        
        return dateCalendar.getTime();
    }
    /**
     * Get number of years difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Number of days
     */
    public static Date resetDateToFirstDayInMonth(Date date) {
        if(date == null) {
            
            throw new IllegalArgumentException("Date cannot be null");
        } 
        
        Calendar dateCalendar = Calendar.getInstance(Locale.ENGLISH);
        dateCalendar.setTime(date);
       
        dateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        dateCalendar.set(Calendar.MINUTE, 0);
        dateCalendar.set(Calendar.SECOND, 0);
        dateCalendar.set(Calendar.MILLISECOND, 0);
        
        dateCalendar.set(Calendar.DATE, 1);
        if (dateCalendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            dateCalendar.set(Calendar.DATE, dateCalendar.get(Calendar.DATE) + 2);    
        }
        return dateCalendar.getTime();
    }
    /**
     * Get number of years difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Number of days
     */
    public static Date resetDateToFirstDayInWeek(Date date) {
        if(date == null) {
            
            throw new IllegalArgumentException("Date cannot be null");
        } 
        
        Calendar dateCalendar = Calendar.getInstance(Locale.ENGLISH);
        dateCalendar.setTime(date);
       
        dateCalendar.set(Calendar.HOUR_OF_DAY, 0);
        dateCalendar.set(Calendar.MINUTE, 0);
        dateCalendar.set(Calendar.SECOND, 0);
        dateCalendar.set(Calendar.MILLISECOND, 0);
        
        dateCalendar.set(Calendar.DAY_OF_WEEK, dateCalendar.getFirstDayOfWeek());
        dateCalendar.set(Calendar.DATE, dateCalendar.get(Calendar.DATE) - 1);
        
        return dateCalendar.getTime();
    }
    /**
     * is Date Between Two Dates.
     * 
     * @param date : date.
     * @param dateStart : date Start.
     * @param dateEnd : date End.
     * 
     * @return true if date in between two dates.
     */
    public static boolean isDateBetweenTwoDates(Date date, Date dateStart, Date dateEnd) {
        if (date != null && dateStart != null && dateEnd != null) {
            if (!date.before(dateStart) && !date.after(dateEnd)) {
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }
    
    /**
     * Sub String To Max Length
     * 
     * @param value String Value 
     * @param maxLenght Max Length
     * 
     * @return String Value after trim
     */
    public static String subStringToMaxLength(String value , int maxLenght){
        
        if (!isBlankOrNull(value) && value.length() > maxLenght){
            value = value.substring(0,maxLenght);
            value = value.substring(0,value.lastIndexOf(" "));
        }
        return value;
    }
    
    /**
     * Check if Eid Number matches follwoing format (NNN-NNNN-NNNNNNN-N).
     * 
     * @param eidNumber : Eid Number.
     * @return true if valid eid number format.
     */
    public static boolean isValidEid(String eidNumber) {
        
        if (isBlankOrNull(eidNumber))  {
            return false;
        }
        
        Pattern pattern = Pattern.compile(EID_NUMBER_PATTERN);
        Matcher m = pattern.matcher(eidNumber);
        return m.matches();
    }
    
    /**
     * isEid Start With Valid Num.
     * 
     * Description:
     * Emirate ID number should start with 784.
     * 
     * @param eidNumber : Eid Number.
     * @return true if is Eid Start Wit hValid Num (784).
     */
    public static boolean isEidStartWithValidNum(String eidNumber) {
        
        if (isBlankOrNull(eidNumber) && eidNumber.length() != 18) {
            return false;
        }
        
        String firstBlockNum = eidNumber.substring(0, 3);
        
        if (firstBlockNum.equals(EID_FIRST_NUM)) {
            return true;    
        }
        
        return false;
    }
    
    /**
     * is Second Four Digits Match Customer Birth Year.
     * Description:
     * First four digits after 784 should match customer birth year.
     * 
     * @param eidNumber : Eid Number.
     * @return true if Second FourDigits Match Customer Birth Year.
     */
    public static boolean isSecondFourDigitsMatchCustomerBirthYear(String eidNumber,Calendar personDate) {
        
        if (isBlankOrNull(eidNumber) || eidNumber.length() != 18) {
            return false;
        }
        
        if (personDate == null) {
            return false;    
        }
        
        int year = personDate.get(Calendar.YEAR);
        
        String secondBlockNum = eidNumber.substring(4, 8);
        
        if (getInteger(secondBlockNum).intValue() == year) {
            return true;    
        }
        
        return false;
    }
    
   
    
    /**
     * Check if IP Address matches follwoing format (111.111.111.111).
     * 
     * @param ipAddress : IP Address.
     * @return true if valid IP Address format.
     */
    public static boolean isValidIpAddress(String ipAddress) {
        
        if (isBlankOrNull(ipAddress))  {
            return false;
        }
        
        Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
        Matcher m = pattern.matcher(ipAddress);
        return m.matches();
    }
    
    /**
     * Check if MAC Address matches follwoing format (AA-BB-CC-AA-BB-CC).
     * 
     * @param macAddress : MAC Address.
     * @return true if valid MAC Address format.
     */
    public static boolean isValidMacAddress(String macAddress) {
        
        if (isBlankOrNull(macAddress))  {
            return false;
        }
        
        Pattern pattern = Pattern.compile(MACADDRESS_PATTERN);
        Matcher m = pattern.matcher(macAddress);
        return m.matches();
    }
    

    /**
     * Get Age From Birth Date
     * 
     * @param birthDate Birth Date
     * @return Age in years
     */
    public static int getAgeFromBirthDate(Date birthDate){
        
        if (birthDate == null)  {
             throw new IllegalArgumentException("Date cannot be null");
        }
        
        Calendar calBirthDate = Calendar.getInstance();  
        calBirthDate.setTime(clearTime(birthDate));
    
        Calendar sysDate = Calendar.getInstance();  
        sysDate.setTime(GlobalUtilities.clearTime(new java.util.Date()));
        return new Long((((sysDate.getTimeInMillis() - calBirthDate.getTimeInMillis())/31556926)/1000)).intValue();      
        
    }
    
     /**
      * calculate Age of person in years.
      * 
      * @param Date birthDate
      * @return Age of person in years 
      * @throws BusinessException
      */
     public static Integer calculatePersonAge(Date birthDate){
         
         if (birthDate == null)  {
              throw new IllegalArgumentException("Date cannot be null");
         }
         
         Integer currentYear;
         Integer birthDateYear;
         Integer age;
         
         java.util.Date currentDate = new Date();
         Calendar currentCalendar = Calendar.getInstance();
         currentCalendar.setTime(currentDate);
         currentYear  = currentCalendar.get(Calendar.YEAR);

         Calendar birthDateCalendar = Calendar.getInstance();
         birthDateCalendar.setTime(birthDate);
         birthDateYear  = birthDateCalendar.get(Calendar.YEAR);
         
         age = currentYear - birthDateYear;
         
         if (currentCalendar.get(Calendar.DAY_OF_YEAR) < birthDateCalendar.get(Calendar.DAY_OF_YEAR)){
             age--;  
         }
    
         return age;  
     }
    
    
    /**
     * Merge distinct date and time in one Date object
     * 
     * @return Date
     * @param time Date
     * @param date Date
     */
    public static Date mergeDateWithTime(Date date, Date time) {
        Calendar dateCld = Calendar.getInstance();
        dateCld.setTime(date);
        
        Calendar timeCld = Calendar.getInstance();
        timeCld.setTime(time);
        
        dateCld.set(Calendar.HOUR_OF_DAY, timeCld.get(Calendar.HOUR_OF_DAY));
        dateCld.set(Calendar.MINUTE, timeCld.get(Calendar.MINUTE));
        dateCld.set(Calendar.SECOND, timeCld.get(Calendar.SECOND));
        dateCld.set(Calendar.MILLISECOND, timeCld.get(Calendar.MILLISECOND));

        return dateCld.getTime();
    }
    
    /**
     * Check If Device Key Matches The Follwoing Format (dddd-dddd-dddd-dddd).
     * 
     * @info d : Digit.
     * @param deviceKey : Device Key.
     * @return True If Valid Device Key Format.
     */
    public static boolean isValidDeviceKey(String deviceKey) {
        
        if (isBlankOrNull(deviceKey)) {
        
            return false;
        }
        
        Pattern pattern = Pattern.compile(DEVICE_KEY_PATTERN);
        Matcher m = pattern.matcher(deviceKey);
        return m.matches();
    }
    
    /**
     * Check If Service Key Matches The Follwoing Format (dddd-dddd-dddddddd-dddd).
     * 
     * @info d : Digit.
     * @param serviceKey : Service Key.
     * @return True If Valid Service Key Format.
     */
    public static boolean isValidServiceKey(String serviceKey) {
        
        if (isBlankOrNull(serviceKey)) {
        
            return false;
        }
        
        Pattern pattern = Pattern.compile(SERVICE_KEY_PATTERN);
        Matcher m = pattern.matcher(serviceKey);
        return m.matches();
    }

    /**
     * is Numeric
     * 
     * @param text
     * @return true /false
     */
    public static boolean isNumeric(String text) {
        try {
           Double.parseDouble(text);
           return true; 
        } catch (Exception e) {
           return false;
        }
    }
    
    /**
     * Used to generate time representation
     */
    public static String formatTimeIn24Format(Date time) {
        if (time == null) {
            return null;
        }

        return TIME_24_FORMAT.format(time);
    }
    
    /**
     * Convert String To List
     * 
     * @param delimeter : Delimeter
     * @param strList : String List
     * @return List Of Long
     */
    public static List stringToLongList(String strList,String delimeter) {
    
        if(isBlankOrNull(strList)) {
           return null; 
        }
        if((isBlankOrNull(delimeter) || strList.indexOf(delimeter) < 0) && isLong(strList.trim())) {
           ArrayList list = new ArrayList();
           list.add(new Long(strList.trim()));
           return list;
        }

        List tokenList = new ArrayList();
        StringTokenizer servicesTokenizer = new StringTokenizer(strList,delimeter);
        
        while (servicesTokenizer.hasMoreTokens()) {
            String token = servicesTokenizer.nextToken();
            if(!isBlankOrNull(token)) {
                tokenList.add(new Long(token.trim()));
            }
        }
        return tokenList;
    }
    
/**
 * Report Handler for RDF files
 * 
 * @param  type: Report Type
 * @param  parameters: Map of Report Parameters
 * @return file contents as Array of Bytes
 */
    public static byte[] retrieveReport(String type, Map parameters) {

        if (isBlankOrNull(type)) {
            throw new RuntimeException("Report Type is null");
        }
        if (parameters == null || parameters.isEmpty()) {
            throw new RuntimeException(" Null Parameters Values");
        }
    
        String desFormat = "";
        if (type.equalsIgnoreCase("PDF")) {
            desFormat = "PDF";
        } else {
            desFormat = "ENHANCEDSPREADSHEET";
        }
        
        String credentials = "/reports/rwservlet?cmdkey="+"tester";

        StringBuffer url = new StringBuffer("")
            .append("10.10.10.10")
            .append(':').append("7101")
            .append(credentials)
            .append("&destype=cache&server=").append("tester")
            .append("&desformat=").append(desFormat)
            .append("&EMICODE=DXB");
    
    
        Set params = parameters.keySet();
        Iterator itt = params.iterator();
    
        String paramName = "";
        String paramValue = null;
    
        while (itt.hasNext()) {
            paramName = (String) itt.next();
            paramValue = (String) parameters.get(paramName);
            url.append("&" + paramName + "=" + paramValue);
        }
    
        ByteArrayOutputStream reportByts = new ByteArrayOutputStream();
        BufferedInputStream in = null;
        try {
            // Open URL connection
            URL connectionUrl = new URL(url.toString());
            URLConnection servletConnection = connectionUrl.openConnection();
    
            // Set connection attributes
            servletConnection.setDoInput(true);
            servletConnection.setDoOutput(true);
            servletConnection.setUseCaches(false);
    
            if (type.equalsIgnoreCase("PDF")) {
                servletConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
            } else {
                servletConnection.setRequestProperty("Content-Type", CONTENT_TYPE_XLS);
            }
    
            // Read request report
            in = new BufferedInputStream(servletConnection.getInputStream());
            int b;
            while ((b=in.read()) != -1) {
                reportByts.write(b);
            }
    
            return reportByts.toByteArray();
    
        } catch(Exception e ){
            throw new RuntimeException("download report fail : ?",e);
        } finally {
            try {
                reportByts.close(); 
                if (in!=null) {
                    in.close();
                }
            } catch(Exception e) {
                throw new RuntimeException("download report fail : ?",e); 
            }
        }
    }    


    /**
     * Send To Report Server
     * 
     * @param  type: Report Type
     * @param  parameters: Map of Report Parameters
     * @return file contents as Array of Bytes
     */
        public static void sendToReportServer(String type, Map parameters,
                                              HttpServletResponse response) {

            if (isBlankOrNull(type)) {
                throw new RuntimeException("Report Type is null");
            }
            if (parameters == null || parameters.isEmpty()) {
                throw new RuntimeException(" Null Parameters Values");
            }
            if (response == null) {
                throw new RuntimeException(" Null Http Response");
                
            }
        
            String desFormat = "";
            if (type.equalsIgnoreCase("PDF")) {
                desFormat = "PDF";
            } else {
                desFormat = "ENHANCEDSPREADSHEET";
            }
            

            StringBuffer url = new StringBuffer("")
                .append("10.10.10.10")
                .append(':')
                .append("7010")
                .append("/reports/rwservlet?cmdkey=").append("test")
                .append("&destype=cache&server=").append("test")
                .append("&desformat=").append(desFormat)
                .append("&EMICODE=DXB");  
        
        
            Set params = parameters.keySet();
            Iterator itt = params.iterator();
        
            String paramName = "";
            String paramValue = null;
        
            while (itt.hasNext()) {
                paramName = (String) itt.next();
                paramValue = (String) parameters.get(paramName);
                url.append("&" + paramName + "=" + paramValue);
            }
        
             
            try { 
                response.sendRedirect(url.toString());
        
            } catch(Exception e ){
                throw new RuntimeException("Erro In Sending TO report server : ?",e);
            } 
        } 
 
    /**
     * Format time using full time format.
     *
     * @param time Date to be formated.
     * @return time String representation or null if the date parameter was null.
     */
    public static String format12Time(Date time) {
        if (time == null) { 
            return null;
        }

        return TIME_12_FORMAT.format(time);
    }

    /**
     * Format time using full time format AM PM.
     *
     * @param time Date to be formated.
     * @return time String representation or null if the date parameter was null.
     */
    public static String format12TimeAMPM(Date time) {
        if (time == null) { 
            return null;
        }

        return TIME_12_FORMAT_AM_PM.format(time);
    }
  
    /**
     * Format time using full time format.
     *
     * @param time Date to be formated.
     * @return time String representation or null if the date parameter was null.
     */
    public static String format24Time(Date time) {
        if (time == null) { 
            return null;
        }

        return TIME_24_FORMAT.format(time);
    }
    
     /**
     * Formate Emirate Id Value
     * 
     * @param value : Old Emirate Id Value
     * @return Fromatted Value : ###-####-#######-#
     */
    public static String formatEmirateId(String value) {
        if (!isBlankOrNull(value) && isLong(value) &&
            value.length() == 15)  {
            value = value.substring(0,3)
                    + "-" + value.substring(3,7)
                    + "-" + value.substring(7,14)
                    + "-" + value.substring(14);
        }
        return value;
    }    
    
    /**
    * Format Residency Number
    * 
    * @param value : Old Residency Number
    * @return Residency Number Value : ###/####/#######
    */
    public static String formatResidencyNumber(String value) {
       if (!isBlankOrNull(value) && isLong(value))  {
           value = value.substring(0,3)
                   + "/" + value.substring(3,7)
                   + "/" + value.substring(7,value.length());
       }
       
       return value;
    }
    
    /**
     * Get number of days difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date
     * 
     * @return Number of days
     */
    public static boolean isInSameDay(Date firstDate, Date secondDate) {
        
        return getDaysDifference(firstDate, secondDate) == 0;
    }

    
   /**
     * Get number of letters in this text.
     * 
     * @param text
     * @return number of letters in this text.
     */
    public static int getLettersCount(String text) {
        //check parameter
        if (text == null) {
            return 0;
        }
        //initiate count 
        int count = 0;
        
        //loop over each character and update the count
        for (int i = 0; i < text.length(); i++)  {
            if (Character.isLetter(text.charAt(i))) {
                //increment the count
                count++;
            }
        }
        
        //return letters count.
        return count;
    }
    
    /**
     * Get number of small letters in this text.
     * 
     * @param text
     * @return number of small letters in this text.
     */
    public static int getSmallLettersCount(String text) {
    
        //check parameter
        if (text == null) {
            return 0;
        }
        //initiate count 
        int count = 0;
        
        //loop over each character and update the count
        for (int i = 0; i < text.length(); i++)  {
            if (Character.isLowerCase(text.charAt(i))) {
                //increment the count
                count++;
            }
        }
        
        //return small letters count.
        return count;
    }
    
    
    
    /**
     *  Get number of capital letters in this text.
     *  
     * @return number of capital letters in this text.
     * @param text
     */
    public static int getCapitalLettersCount(String text) {
    
         //check parameter
        if (text == null) {
            return 0;
        }
        //initiate count 
        int count = 0;
        
        //loop over each character and update the count
        for (int i = 0; i < text.length(); i++)  {
            if (Character.isUpperCase(text.charAt(i))) {
                //increment the count
                count++;
            }
        }
        
        //return capital letters count.
        return count;
    }
    
    /**
    *  Get number of Special Characters in this text.
    *  
    * @return number of special characters in this text.
    * @param text
    */
    public static int getSpecialCharacterCount(String text) {
        //check parameter
        if (text == null) {
            return 0;
        }
        //initiate count 
        int count = 0;
        
        //loop over each character and update the count
        for (int i = 0; i < text.length(); i++)  {
            if (isSpecialChar(text.charAt(i))) {
                //increment the count
                count++;
            }
        }
        
        //return special characters count.
        return count;
    }
    
    /**
     * Get number of digits in this text.
     * 
     * @param text.
     * @return number of digits in text.
     */
    public static int getDigitsCount(String text) {
        //check parameter
        if (text == null) {
            return 0;
        }
        //initiate count 
        int count = 0;
        
        //loop over each character and update the count
        for (int i = 0; i < text.length(); i++)  {
            if (Character.isDigit(text.charAt(i))) {
                //increment the count
                count++;
            }
        }
        
        //return digits count.
        return count;
    }
    
    /**
     * if the character is not digit, alpha, control characters or space/ white space and the assci code is less than 128
     *  then it is a special charachter.
     * @return 
     * @param ch
     */
    public static boolean isSpecialChar(char ch) {
        
        //check if its a letter, digit or space char.
        if(Character.isLetterOrDigit(ch) || Character.isSpaceChar(ch)){
            return false;
        }
        
        //check assci code, if its between 32(control characters) and 127, then its special char.
        int ascii = (int)ch;
        if(ascii > 31 && ascii < 128) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Generate random Small Letter.
     * 
     * @param random generator.
     * @return random small letter.
     */
    public static char getRandomSmallLetter() {
        // Generate small letter: <<ASCII>> 97 - 122 : a-z
        return (char) ((int) (new Random().nextInt(26) + 97));
    }
    
    /**
     * Generate random Capital Letter.
     * 
     * @param random generator.
     * @return random capital letter.
     */
    public static char getRandomCapitalLetter() {
        // Generate capital letter: <<ASCII>> 65 - 90: A-Z
        return (char) ((int) (new Random().nextInt(26) + 65));
    }
    
    
    /**
     * Return random char. in String
     * @return 
     * @param text
     */
    public static char getRandomCharInText(char[] text) {
    
        //generate random index
        int index = new Random().nextInt(text.length);
        
        //return char at that index
        return text[index];
    }
    
    /**
     * Get Minutes difference between two dates
     * 
     * @param firstDate First Date
     * @param secondDate Second Date 
     * 
     * @return Minutes
     */
    public static long getHoursDifference(Date firstDate, Date secondDate) {
        
        //get difference in min and divid it by 60
        return Math.abs(getMinutesDifference(firstDate, secondDate) / 60);
                      
    }

    /**
     * Used to generate time representation
     */
    public static String formatFullTimeIn24Format(Date time) {
        if (time == null) {
            return null;
        }

        return FULL_TIME_24_FORMAT.format(time);
    }
    
    /**
     * Parse string to Full Time In 24 Format
     * 
     * @param timeAsString : time as String
     * @return Date object.
     */
    public static Date parseFullTimeIn24Format(String timeAsString) throws ParseException {
         
        if (isBlankOrNull(timeAsString)) {
            return null;
        }

        return FULL_TIME_24_FORMAT.parse(timeAsString);
    }
    
    /**
     * Check Data Correctness
     * 
     * @param columnMetaInfo Column Meta Info
     * @param objectType Object Type
     * @param objectStr object Str ing
     */
    public static void checkDataCorrectness(Object columnMetaInfoObj,String objectType,
                                      String objectStr ){
        checkDataCorrectness(columnMetaInfoObj,objectType,objectStr,null);                            
    }

    /**
     * Check Data Correctness
     * 
     * @param columnMetaInfo Column Meta Info
     * @param objectType Object Type
     * @param objectStr object Str ing
     */
    public static void checkDataCorrectness(Object columnMetaInfoObj,String objectType,
                                      String objectStr,SimpleDateFormat dateFormate){
        if (columnMetaInfoObj == null || 
            isBlankOrNull(objectType) || 
            isBlankOrNull(objectStr)) {
            return;
            
        }
        Object obj = null;
        if (objectType.equals(ColumnMetaInfo.OBJECT_STRING)) {
            try {
                obj = new String(objectStr);
            }catch (Exception ex) {
                throw new RuleException("BR_MMS024_MER",objectStr);
            }
            
        }else if (objectType.equals(ColumnMetaInfo.OBJECT_LONG)) {
            try {
                obj = getLong(objectStr);
            }catch (Exception ex) {
                throw new RuleException("BR_MMS024_MER",objectStr);
            }
        }else if (objectType.equals(ColumnMetaInfo.OBJECT_INTEGER)) {
            try {
                obj = getInteger(objectStr);
            }catch (Exception ex) {
                throw new RuleException("BR_MMS024_MER",objectStr);
            }
        }else if (objectType.equals(ColumnMetaInfo.OBJECT_DATE)) {
            if (dateFormate == null) {
                return;
            }
            try {
                obj = dateFormate.parse(objectStr);
            }catch (Exception ex) {
                throw new RuleException("BR_MMS024_MER",objectStr);
            }
        }          
        if (obj == null) {
            return;
        }
        ColumnMetaInfo columnMetaInfo = (ColumnMetaInfo)columnMetaInfoObj;
        
        if (obj instanceof String) {
            String objString =  (String)obj;
        
            if (!GlobalUtilities.isBlankOrNull(objString) &&
                objString.length() > columnMetaInfo.getSize()) {
                throw new RuleException("BR_MMS024_MER",objString);
            }
         
            if (!columnMetaInfo.isVarchar() && !GlobalUtilities.isBlankOrNull(objString)) {
                throw new RuleException("BR_MMS024_MER",objString);
            }
            
        }else if (obj instanceof Integer) {
            Integer objInteger =  (Integer)obj;
            if (objInteger != null && GlobalUtilities.getString(objInteger).length() > columnMetaInfo.getSize()) {
                throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objInteger));
            }
         
            if (!columnMetaInfo.isNumeric() && objInteger != null) {
                throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objInteger));
            }  
        }else if (obj instanceof Long) {
            Long objLong =  (Long)obj;
            if (objLong != null && GlobalUtilities.getString(objLong).length() > columnMetaInfo.getSize()) {
                throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objLong));
            }
         
            if (!columnMetaInfo.isNumeric() && objLong != null) {
                throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objLong));
            }   
        }else if (obj instanceof Date) {
            Date objLong =  (Date)obj;
            if (!columnMetaInfo.isDate() && objLong != null) {
                throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objLong));
            }
        }else if (obj instanceof Choice) {
            Choice objChoice =  (Choice)obj;
            if (objChoice != null) {
                Integer objInteger =  objChoice.getInteger();
                if (objInteger != null && GlobalUtilities.getString(objInteger).length() > columnMetaInfo.getSize()) {
                    throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objInteger));
                }
             
                if (!columnMetaInfo.isNumeric() && objInteger != null) {
                    throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objInteger));
                }  
                if (objInteger != null && 
                    !(objInteger.equals(Choice.YES.getInteger()) ||
                     objInteger.equals(Choice.NO.getInteger())) ) {
                    throw new RuleException("BR_MMS024_MER", GlobalUtilities.getString(objInteger));
                }
            }
        }

    }    
    
    /**
     * Calculates the  age in years and months and days for the 
     * passed birth date
     * 
     * @param birthDate represetns the birth date
     * @return String represents the age in (years,months,days) format
     */
    public static String getAgeDetailes(java.util.Date birthDate) {
        
        int years = 0;
        int months = 0;
        int days = 0;
        
        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        
        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        
        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        
        //Get difference between months
        months = currMonth - birthMonth;
        
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if (months < 0){
           years--;
           months = 12 - birthMonth + currMonth;
           if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
              months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)){
           years--;
           months = 11;
        }
        
        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
           days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
           int today = now.get(Calendar.DAY_OF_MONTH);
           now.add(Calendar.MONTH, -1);
           days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
           days = 0;
           if (months == 12){
              years++;
              months = 0;
           }
       }
       return years + "," + months+ "," + days;      
   }

    /**
     * Is English Text Without Special Char
     * return true if all charecters are english, otherwise return false
     * 
     * @return boolean ( true / false )
     * @param text
     */
    public static boolean isEnglishTextWithoutSpecialChar(String text) {
        if( isBlankOrNull(text) ){
            return true;
        }
        
        Pattern pattern = null;
        pattern = Pattern.compile(ENGLISH_CHARACTER_WITHOUT_SPEC_CHAR_PATTEREN);
        Matcher m = null;
        for (int i = 0 ; i < text.length(); i++)  {
            CharSequence index = text.subSequence(i,i+1);
            if ( !isBlankOrNull(index.toString()) ) {
                m = pattern.matcher(index);
                if (m.find() == false) {
                    return false;
                }                
            }

        }
         
        return true;
    }
    
    /**
     * Convert Number List To String..
     * 
     * @param listValue Source list to be splited.
     * @param delimeter : Delimeter between result values
     * @return String representation for list values
     */
    public static String convertNumberListToString(List<? extends Number> listValue,String delimeter) {
        // Validate source list
        if (listValue == null || listValue.isEmpty()) {
            return null;
        }
        
        if(isBlankOrNull(delimeter)) {
            delimeter = ",";
        }
        StringBuffer strValue = new StringBuffer("");
        for(Number value : listValue) {
            strValue.append(value).append(delimeter);
        }            
        return strValue.substring(0,strValue.length()-1);
    }
	
	/*
     * Report Handler for RDF files
     * 
     * @param  type: Report Type
     * @param  parameters: Map of Report Parameters
     * @return file contents as Array of Bytes
     */
    public static byte[] retrieveDirectReport(String type, Map parameters) {

        if (isBlankOrNull(type)) {
            throw new RuntimeException("Report Type is null");
        }
        if (parameters == null || parameters.isEmpty()) {
            throw new RuntimeException(" Null Parameters Values");
        }
    
        String desFormat = "";
        if (type.equalsIgnoreCase("PDF")) {
            desFormat = "PDF";
        } else {
            desFormat = "ENHANCEDSPREADSHEET";
        }
        
        /** Configuration param: Report web-server. */
        String reportWebHost = "10.10.10.10";
        /** Configuration param: Report web-port. */
        String reportWebPort = "7101";
        /** Configuration param: Report server. */
        String reportServer = "testtt";        
        
        String credentials =  "/reports/rwservlet?cmdkey="+"testtter";
   
        StringBuffer url = new StringBuffer("")
            .append(reportWebHost)
            .append(':').append(reportWebPort)
            .append(credentials)
            .append("&destype=cache&server=").append(reportServer)
            .append("&desformat=").append(desFormat)
            .append("&EMICODE=DXB");
    
        Set params = parameters.keySet();
        Iterator itt = params.iterator();
    
        String paramName = "";
        String paramValue = null;
    
        while (itt.hasNext()) {
            paramName = (String) itt.next();
            paramValue = (String) parameters.get(paramName);
            url.append("&" + paramName + "=" + paramValue);
        }
    
        ByteArrayOutputStream reportByts = new ByteArrayOutputStream();
        BufferedInputStream in = null;
        try {
            // Open URL connection
            URL connectionUrl = new URL(url.toString());
            URLConnection servletConnection = connectionUrl.openConnection();
    
            // Set connection attributes
            servletConnection.setDoInput(true);
            servletConnection.setDoOutput(true);
            servletConnection.setUseCaches(false);
    
            if (type.equalsIgnoreCase("PDF")) {
                servletConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
            } else {
                servletConnection.setRequestProperty("Content-Type", CONTENT_TYPE_XLS);
            }
    
            // Read request report
            in = new BufferedInputStream(servletConnection.getInputStream());
            int b;
            while ((b=in.read()) != -1) {
                reportByts.write(b);
            }
    
            return reportByts.toByteArray();
    
        } catch(Exception e ){
            throw new RuntimeException("download report fail : ?",e);
        } finally {
            try {
                reportByts.close(); 
                if (in!=null) {
                    in.close();
                }
            } catch(Exception e) {
                throw new RuntimeException("download report fail : ?",e); 
            }
        }
    }    
 
    /**
     * Get the number of elements of a comma seperated string, or any other specified delimeter.
     * 
     * @param str : String
     * @param delimeter : String
     * @return number of elements : Integer
     */
    public static Integer stringElementsCount(String str, String delimeter){
        if( isBlankOrNull(str) || delimeter == null){
            return 0;
        }
        
        String[] ary = str.split(delimeter);
        
        if(ary != null) {
            return ary.length;
        }
        
        return 0;
    }
    
    /**
      * Get the driver age years based on the passed birth date
      * 
      * @param birthDate represnets birth date
      * @return represents the number of years
      */
    public static int getAgeYears(java.util.Date birthDate) {
        if (birthDate == null) {
            return 0;
        }
        String driverAge = GlobalUtilities.getAgeDetailes(birthDate);
        if (driverAge == null) {
            return 0;
        }
       
        String[] driverAgeDetails = driverAge.split(",");
        if (driverAgeDetails == null && driverAgeDetails.length !=3 ) {
            return 0;
        }
       
        return GlobalUtilities.getInteger(driverAgeDetails[0]).intValue();
    }

    /**
     * Get number of year from date
     * 
     * @param date Date
     * 
     * @return year
     */
    public static int getYearfromDate(Date date) {
         
        if(date == null) {
            throw new IllegalArgumentException("Date cannot be null");
        }
        Calendar firstDateCalendar = Calendar.getInstance();
        firstDateCalendar.setTime(date);
        
        return firstDateCalendar.get(firstDateCalendar.YEAR);
    }
    
    private static final String[] HEADERS_TO_TRY = { 
        "X-Forwarded-For",
        "Proxy-Client-IP",
        "WL-Proxy-Client-IP",
        "HTTP_X_FORWARDED_FOR",
        "HTTP_X_FORWARDED",
        "HTTP_X_CLUSTER_CLIENT_IP",
        "HTTP_CLIENT_IP",
        "HTTP_FORWARDED_FOR",
        "HTTP_FORWARDED",
        "HTTP_VIA",
        "REMOTE_ADDR" };

    public static String getClientIpAddress(HttpServletRequest request) {
        if (request == null) { 
            return null;
        }
        for (String header : HEADERS_TO_TRY) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }
    
    
    /**
     * Get Integer from choice
     * 
     * @param Choice
     * 
     * @return Integer
     */
    public static Integer getChoiceAsInteger(Choice choice) {
        if (choice != null) 
            return choice.getInteger();
        else
            return null;
    }
    
    

    /**
     * Check if the given data contains the object
     * @param object
     * @param data
     * @return
     */
    public static <T> boolean in (T object, T... data){
        if(object == null){
            return false;
        }
        
        if(data == null || data.length == 0){
            return false;
        }
        for(T each : data){
            if(object.equals(each)){
                return true;
            }
        }
        return false;
    }
    
    
     /**
      * Check if the given data does not contain the object
      * @param object
      * @param data
      * @return
      */
    public static boolean notIn (Object object, Object... data){
        if(object == null){
            return true;
        }
        
        if(data == null || data.length == 0){
            return true;
        }
        return !in(object, data);
    }
     
    /**
     * Get Order Domain Values
     * 
     * @param key
     * @param lang  
     *    
     * @return Domain Values Info.
     */
    public static DomainVO[] getOrderDomainValues(String key,String lang) {
        return preferencesHandler.getOrderDomainValues(key, lang);
    }
    
    /**
     *  Check if the passed colection object is empty
     * @param collection
     * @return true if the passed object is null or size == 0 
     * otherwise false
     */
    public static boolean isEmpty(Collection collection) {

        if (collection == null || collection.size() == 0) {
            return true;
        }
        return false;
    }
    
    /**
     * Get Browser Information.
     * 
     * @param request: HttpServletRequest
     * @return browserInfoList:  List of browser info and operating system .
     */
    public static Map getBrowserInfo(HttpServletRequest request) {
        // Get user agent 
        String userAgent = request.getHeader("user-agent"); 
            
        String browserName = null;
        String browserVersion = null;
        Map browserInfoMap = new HashMap();

        // validation.
        if (userAgent == null) {
            browserName = "unknown Browser";
            return browserInfoMap;
        }
        
        browserInfoMap.put("USERAGENT", userAgent);
        // Get Operating System 
        if (userAgent.toLowerCase().indexOf("windows") > -1) {
            
            String winVersion = "Windows ";
            if (userAgent.indexOf(" NT ") > -1) {
                if (userAgent.lastIndexOf("NT 5.1") > -1) {
                    winVersion += "XP";
                } else if (userAgent.lastIndexOf("NT 5.2") > -1 ){
                    winVersion += "Server 2003";
                } else if (userAgent.lastIndexOf("NT 6.0") > -1 ){
                    winVersion += "Vista";
                } else if (userAgent.lastIndexOf("NT 6.1") > -1 ){
                    winVersion += "7";
                } else if (userAgent.lastIndexOf("NT 6.2") > -1 ){
                    winVersion += "8";
                } else if (userAgent.lastIndexOf("NT 6.3") > -1 ){
                    winVersion += "8.1";
                } else if (userAgent.lastIndexOf("NT 10.0") > -1 ){
                    winVersion += "10";
                }
            }
            browserInfoMap.put("OS",winVersion);

        } else if (userAgent.toLowerCase().indexOf("mac") > -1) {
            browserInfoMap.put("OS","Mac");
        } else if (userAgent.toLowerCase().indexOf("x11") > -1) {
            browserInfoMap.put("OS","Unix");
        } else if (userAgent.toLowerCase().indexOf("android") > -1) {
            browserInfoMap.put("OS","Android");
        } else if (userAgent.toLowerCase().indexOf("iphone") > -1) {
            browserInfoMap.put("OS","IPhone");
        } else {
            browserInfoMap.put("OS","");
        }

        // check Internet Explorer.
        if (userAgent.indexOf("MSIE") > -1) {
            String browserWithVer = userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browserVersion = browserWithVer.split(" ")[1];
            browserInfoMap.put("BROWSER","Internet Explorer");
            
            if (userAgent.toLowerCase().indexOf("trident") > -1) {
                String trident = userAgent.substring(userAgent.toLowerCase().indexOf("trident")).split(";")[0];
                if(!isBlankOrNull(trident)){
                    if(trident.endsWith(")")){
                        trident = trident.substring(0, trident.length() - 1);
                    }
                }
                browserInfoMap.put("TRIDENT",trident);
                browserInfoMap.put("BROWSERVERSION",getBrowserVersionByTrident(trident)); 
            }else{
                browserInfoMap.put("BROWSERVERSION",browserVersion);    
            }
        // check Netscape Browser.
        } else if (userAgent.indexOf("Netscape") > -1) {
            String browserWithVer = userAgent.substring(userAgent.indexOf("Netscape")).split(";")[0];
            browserVersion = browserWithVer.split(" ")[1];
            browserInfoMap.put("BROWSER","Netscape");
            browserInfoMap.put("BROWSERVERSION",browserVersion);
        // check Mozilla Firefox Browser.
        } else if (userAgent.indexOf("Firefox/") > -1) {
            browserName = "Mozilla Firefox";
            String browserWithVer = userAgent.substring(userAgent.indexOf("Firefox")).split(";")[0];
            browserVersion = browserWithVer.split("/")[1];
            browserInfoMap.put("BROWSER","Mozilla Firefox");
            browserInfoMap.put("BROWSERVERSION",browserVersion);
        // check Opera Browser.
        } else if (userAgent.indexOf("Opera") > -1) {
            browserName = "Opera";
            String browserWithVer = userAgent.substring(userAgent.indexOf("Opera")).split(";")[0];
            browserVersion = browserWithVer.split(" ")[1];
            browserInfoMap.put("BROWSER","Opera");
            browserInfoMap.put("BROWSERVERSION",browserVersion);
        // check Google Chrome Browser.
        } else if (userAgent.indexOf("Chrome/") > -1) {
            browserName = "Google Chrome";
            String browserWithVer = userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0];
            browserVersion = browserWithVer.split("/")[1];
            browserInfoMap.put("BROWSER","Google Chrome");
            browserInfoMap.put("BROWSERVERSION",browserVersion);
        // check Safari Browser.
        } else if (userAgent.indexOf("Safari/") > -1) {
            browserName = "Safari"; 
            String browserWithVer = userAgent.substring(userAgent.indexOf("Safari")).split(";")[0];
            browserVersion = browserWithVer.split("/")[1];
            browserInfoMap.put("BROWSER","Safari");
            browserInfoMap.put("BROWSERVERSION",browserVersion);
        } else if (userAgent.toLowerCase().indexOf("trident") > -1) {
            
            browserInfoMap.put("BROWSER","Internet Explorer");
            if (userAgent.indexOf("rv:") > -1) {
                browserVersion = userAgent.substring(userAgent.indexOf("rv:")+3,userAgent.indexOf(")"));
                browserInfoMap.put("BROWSERVERSION",browserVersion);
            }
            if (userAgent.toLowerCase().indexOf("trident") > -1) {
                String trident = userAgent.substring(userAgent.toLowerCase().indexOf("trident")).split(";")[0];
                browserInfoMap.put("TRIDENT",trident);
                browserInfoMap.put("BROWSERVERSION",getBrowserVersionByTrident(trident));
            }            
        }

        return browserInfoMap;
    }  
    
    private static String getBrowserVersionByTrident(String trident){
        String[] tridentArr = trident.split("/");
        Double tridentValue = Double.parseDouble(tridentArr[1].replaceAll("\\)", "").replaceAll("\\(", ""));
        Integer browserVersion = tridentValue.intValue() + 4; 
        return browserVersion.toString();
        
    }

  public static java.sql.Date convertStringToSQLDate(String date , String pattern){
    if(date == null){
      return null;
    }
    SimpleDateFormat format = new SimpleDateFormat(pattern);
    java.util.Date parsed = null;
    try
    {
      parsed = format.parse(date);
      java.sql.Date sql = new java.sql.Date(parsed.getTime()); 
      return sql;
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }
    return null;
  }
  
  public static String convertJavaUtilDateToString(java.util.Date date,String datePattern){
    if(date == null){
      return null;
    }
    // Create an instance of SimpleDateFormat used for formatting 
    // the string representation of date (month/day/year)
    DateFormat df = new SimpleDateFormat(datePattern);
      
    // Using DateFormat format method we can create a string 
    // representation of a date with the defined format.
    String convertedDate = df.format(date);
    return convertedDate;
  }

    /**
     *  convert Date To XML Gregorian Calendar
     * @param date  Date Object
     * @return XMLGregorianCalendar Date Object
     * @throws DatatypeConfigurationException
     */
    public static javax.xml.datatype.XMLGregorianCalendar convertDateToXMLGregorianCalendar(java.util.Date date) throws DatatypeConfigurationException {

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        javax.xml.datatype.XMLGregorianCalendar xmlGregorian = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        return xmlGregorian;
    }

    /**
     * convert XML Gregorian Calendar To Date
     * @param calendar XMLGregorianCalendar Object
     * @return Util Date Object
     * @throws DatatypeConfigurationException
     */
    public static java.util.Date convertXMLGregorianCalendarToDate(javax.xml.datatype.XMLGregorianCalendar calendar) throws DatatypeConfigurationException {
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
    
    /**
     * Checks if the field contains a boolean value.
     * 
     * @param value String value to be checked.
     * @return true if the field contains a boolean value.
     */
    public static boolean isBoolean(String value) {
       if (isBlankOrNull(value)) {
           return false;
       }

       try {
           Boolean.parseBoolean(value);
           return true;
       } catch (Exception ex) {
           return false;
       }
    }
    
    /**
     * Get Letter Date
     * 
     * @param date Date
     * @return Letter Date 
     */
    public static String getLetterDate(Date date) {
        if (date == null) {
            return "";
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        
        String monthString;
        switch (month) {
            case 1:  monthString = "January";       break;
            case 2:  monthString = "February";      break;
            case 3:  monthString = "March";         break;
            case 4:  monthString = "April";         break;
            case 5:  monthString = "May";           break;
            case 6:  monthString = "June";          break;
            case 7:  monthString = "July";          break;
            case 8:  monthString = "August";        break;
            case 9:  monthString = "September";     break;
            case 10: monthString = "October";       break;
            case 11: monthString = "November";      break;
            case 12: monthString = "December";      break;
            default: monthString = "Invalid month"; break;
        }
        
        return day + " " + monthString + " " + year;
    }
    
    public static List validateText(String tradeName, String[] wordsList, int mode)
    {
      
      boolean isMatched = true;
      if(mode == MODE_INCLUDE_ANY) {
          isMatched = false;
      }
      List matchedWords = new ArrayList();
      
      if(tradeName != null && wordsList !=null) {
              for(String word : wordsList){
                if(word != null){
                  if(tradeName.toUpperCase().contains(word.trim().toUpperCase())){
                      if(mode == MODE_INCLUDE_ANY) {
                            isMatched = true;
                            break;
                      }
                      else
                      {    
                        matchedWords.add(word.trim());
                          isMatched = true;
                      }
                  }                   
                }
              }        
              if(!isMatched && mode == MODE_INCLUDE_ANY) {
                  for(String word : wordsList){
                    matchedWords.add(word.trim());
                  }
              }

              
      } else if (tradeName == null) {
          if(mode == MODE_INCLUDE_ANY) {
              for(String word : wordsList){
                matchedWords.add(word.trim());
              }

          }
      }
        return matchedWords;
    
    }
    
    /**
     *  Formating string date to any pattern string date
     *  ex. 2016-05-02 to 02-05-2016 (both strings)
     *  
     * @param strDate String the source string to reformat 
     * @param srcPattern String the string date argument pattern
     * @param destPattern String the wanted pattern of the returned string date
     * @return reformattedDate String the reformatted string date
     */
    public static String formatStringDate(String srcStrDate, String srcPattern, String destPattern) {
         SimpleDateFormat sourcePattern = new SimpleDateFormat(srcPattern);
         SimpleDateFormat destinationPatthern = new SimpleDateFormat(destPattern);
                
        String reformattedDate = "";
        if (!GlobalUtilities.isBlankOrNull(srcStrDate)) {
            try {
                reformattedDate = destinationPatthern.format(sourcePattern.parse(srcStrDate));
            } catch (ParseException e) {
                return "";
            }
        }
        return reformattedDate;
    }
    
    /**
     * Returns the result of calling {@code toString} on the first
     * argument if the first argument is not {@code null} and returns
     * the second argument otherwise.
     *
     * @param o an object
     * @param nullDefault string to return if the first argument is
     *        {@code null}
     * @return the result of calling {@code toString} on the first
     * argument if it is not {@code null} and the second argument
     * otherwise.
     * @see Objects#toString(Object)
     */
    public static String toString(Object o, String nullDefault) {
        return (o != null) ? o.toString() : nullDefault;
    }

    /**
     * Convert File object to Base64 code string
     * 
     * @param file File
     * @return base64Str String 
     */
    public static String toBase64(File file) {
        if (file != null && file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                return toBase64(fis);
            } catch (FileNotFoundException e) {
                e.getMessage();
            }
        }
        return "";
    }

    /**
     * Convert byte array to Base64 code string
     * 
     * @param byteArray byte[]
     * @return base64Str String 
     */
    public static String toBase64(byte[] byteArray) {
        String base64Str = "";
        if (byteArray != null && byteArray.length > 0) {
            base64Str = new BASE64Encoder().encode(byteArray);
            
        }
        return base64Str;
    }

    /**
     * Convert Input Stream object to Base64 code string
     * 
     * @param inStream InputStream
     * @return base64Str String 
     */
    public static String toBase64(InputStream inStream) {
        if (inStream != null) {
            return toBase64(toByteArray(inStream));
        }
        return "";
    }
    
    /**
     * Convert Input Stream object to byte array
     * 
     * @param inStream InputStream
     * @return byteArray byte[] 
     */
    public static byte[] toByteArray(InputStream inStream) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] byteArray = null;
        if (inStream != null) {
            BufferedInputStream in = new BufferedInputStream(inStream);
            if (in != null) {
                int b;
                try {
                    while ((b = in.read()) != -1) {
                        out.write(b);
                    }
                } catch (IOException e) {
                    e.getMessage();
                }
            }
            return out.toByteArray();
        }
        return byteArray;
    }
    
    /**
     *  Check if the passed array object is empty
     * @param collection
     * @return true if the passed array is null or length == 0
     * otherwise false
     */
    public static <T> boolean isEmpty(T[] array) {

        if (array == null || array.length == 0) {
            return true;
        }
        return false;
    }
    
    /**
     * Get EJB JNDI name related to this class.
     * 
     * @param ejbClass EJB class.
     * @return EJB JNDI name.
     */
    public static String getEjbJndiName(Class ejbClass) {
        // Validate parameters
        if (ejbClass == null) {
            throw new RuntimeException("NULL ejbClass parameter");
        }
        
        return ejbClass.getName().replace(".", "-");
    }
    
    /**
     * Convert Base64 code string to byte array
     *
     * @param base64Str String
     * @return byteArray byte[]
     */
    public static byte[] convertBase64ToByteArray(String base64Str) {

        byte[] byteArray = null;

        if (isBlankOrNull(base64Str)) {
            return byteArray;
        }

        try {
            byteArray = new BASE64Decoder().decodeBuffer(base64Str);
        } catch (IOException e) {
            return byteArray;
        }

        return byteArray;
    }
    
    /**
     * Get the name of date from the passed date with based on the passed locale
     * 
     * @param date      : Date to read the day name from 
     * @param langCode  : Language of the day name, if the langCode is not specified arabic 
     *                    language will be used as default
     *                    
     * @return day name in the passed locale language
     */
    public static String getDayNameFromDate(java.util.Date date, String langCode ) {
        
        if ( date == null ) {
            return null;
        }
        
        if ( isBlankOrNull( langCode ) ) {
            langCode = "ar";
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat( "EEEE", new Locale( langCode ) );
        
        String dayName = sdf.format( date );

        return dayName;
    }
    
    /**
     * Is Arabic Text
     * return true if all charecters are Arabic, otherwise return false
     *
     * @return boolean ( true / false )
     * @param text
     */
    public static boolean isArabicText(String text) {
        if (isBlankOrNull(text)) {
            return true;
        }
        if (!text.matches(ARABIC_CHARACTER_PATTEREN)) {
            return false;
        }
        return true;
    }
    
    /**
     * Is English Text With Special Char
     * return true if all charecters are english or Special Char, otherwise return false
     * 
     * @return boolean ( true / false )
     * @param text, specialCharacters
     */
    public static boolean isEnglishTextWithSpecialChar(String text, String[] specialCharacters ) {
        if( isBlankOrNull(text) ){
            return false;
        }
        Pattern pattern = null;
        pattern = Pattern.compile(ENGLISH_CHARACTER_WITHOUT_SPEC_CHAR_PATTEREN);
        Matcher m = null;
        for(String charcter : specialCharacters){
          text = new String(text.toLowerCase().replace(charcter,""));
        }
        for (int i = 0 ; i < text.length(); i++)  {
            CharSequence index = text.subSequence(i,i+1);
            if ( !isBlankOrNull(index.toString()) ) {
                m = pattern.matcher(index);
                if (m.find() == false) {
                    return false;
                }                
            }

        }
        return true;
    }

    /**
     * Check if the passed date is friday.
     * 
     * @param dateToCheck : date To Check
     * @return boolean ( true / false )
     */
    public static boolean isFridayDay(Date dateToCheck) {
        
        if (dateToCheck == null) {
            return false;    
        }
        
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTime(dateToCheck);
        
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY;
    }
     
    /**
     * Get Residency Emirates
     * 
     * @param residencyNumber Residency Number
     * @return residencyEmirate Residency Emirate
     */
    public static String getResidencyCityCode(String residencyNumber) {
        String residencyEmirate = "";
//        if (!BeanUtils.isBlankOrNull(residencyNumber)) {
            String residencyDelem = residencyNumber.substring(0, 3);
            if ("101".equalsIgnoreCase(residencyDelem)) {
                residencyEmirate = "01";
            } else if ("201".equalsIgnoreCase(residencyDelem)) {
                residencyEmirate = "03";
            } else if ("301".equalsIgnoreCase(residencyDelem)) {
                residencyEmirate = "04";
            } else if ("401".equalsIgnoreCase(residencyDelem)) {
                residencyEmirate = "05";
            } else if ("501".equalsIgnoreCase(residencyDelem)) {
                residencyEmirate = "06";
            } else if ("601".equalsIgnoreCase(residencyDelem)) {
                residencyEmirate = "07";
            } else if ("701".equalsIgnoreCase(residencyDelem)) {
                residencyEmirate = "08";
            }
       // }
        
        return residencyEmirate;
    }
    /**
     * Checks if the field contains a double value.
     * 
     * @param value String value to be checked.
     * @return true if the field contains a double value.
     */
    public static boolean isDouble(String value) {
       if (isBlankOrNull(value)) {
           return false;
       }

       try {
           Double.parseDouble(value);
           return true;
       } catch (Exception ex) {
           return false;
       }
    }

    /**
     * Parse date string using new Sddi date format.
     *
     * @param date Date string.
     * @return Date object.
     */
    public static Date parseDateSddiFormat(String date) throws ParseException {
        if (isBlankOrNull(date)) {
            return null;
        }

        return new Date(DATE_FORMAT_SDDI_FORMAT.parse(date.trim()).getTime());
    }
    
    /**
     * Convert string value to the type proper for the value;
     * 
     * This method is typically usefull when converting string json data to proper types
     * For example a number can be as "1" this method will return it as Object but as
     * instance of class Integer
     * 
     * @param value
     * @return Object of proper internal type
     */
    public static Object getJsonValue( String jsonValue ) {
          
        Object val = jsonValue;
        if ( "true".equalsIgnoreCase( jsonValue ) ) {
            
            val = true;                
        } else if ("false".equalsIgnoreCase( jsonValue ) ) {
            
            val = false;
        } else if (GlobalUtilities.isNumeric( jsonValue )) {
            
            val = GlobalUtilities.getDouble( jsonValue ) ;        
        }
        return val;
     }
    
    /**
     * Get ServiceLocator reference.
     *
     * @return ServiceLocator reference.
     */
    public static ServiceLocator getServiceLocator() {
        return ServiceLocator.getInstance();
    }
    
    
   
    /*
     * Remove Special Characters
     *
     * @param name
     *
     * @return name without Special Characters
     */
    public static synchronized String removeSpecialCharacters(String name) {
        return removeSpecialCharacters(name, null);
    }

    /**
     * Remove Special Characters
     *
     * @param name
     * @param excecludedCharacters excecluded characters
     *
     * @return name without Special Characters
     */
    public static synchronized String removeSpecialCharacters(String name, String excecludedCharacters) {
        if (isBlankOrNull(name)) {
            return name;
        }
        String iChars = "!@#$%^&*+=-[]\';,/{}|:\"<>?~_";
        if (isBlankOrNull(iChars)) {
            return name;
        }
        StringBuffer src = new StringBuffer(name);

        for (int i = 0; i < iChars.length(); i++) {
            String oldValue = iChars.substring(i, i + 1);
            if (!isBlankOrNull(excecludedCharacters) && excecludedCharacters.indexOf(oldValue) != -1) {
                continue;
            }
            src = replaceAll(src, oldValue, "");

        }

        return src.toString();
    }

    /**
     * Format date and time from right to left
     * @param date
     * @return
     */
    public static String formatFullDateTimeRTL( Date date) {
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return dateFormat.format( date );
    }

    /**
     *  Check if the two attributes don't have same value
     * @param oldAttribute
     * @param newAttribute
     * @return
     */
    public static boolean attributeChanged(Object oldAttribute, Object newAttribute) {
        return attributeChanged(oldAttribute, newAttribute, false);
    }

    /**
     *  Check if the two attributes don't have same value
     * @param oldAttribute
     * @param newAttribute
     * @param exactMatch to check the value as is
     * @return
     */
    public static boolean attributeChanged(Object oldAttribute, Object newAttribute, boolean exactMatch) {

        if (oldAttribute == null || newAttribute == null) {

            if (isBlankOrNull(oldAttribute) && isBlankOrNull(newAttribute)) {
                return false;
            }
            return true;
        }
        if (!oldAttribute.getClass().equals(newAttribute.getClass())) {
            throw new IllegalArgumentException("Attributes are not from same type");
        }

        if (oldAttribute instanceof Date) {
            Date oldCal;
            Date newCal;

            if (exactMatch) {
                oldCal = (Date) oldAttribute;
                newCal = (Date) newAttribute;
            } else {
                oldCal = clearTime((Date) oldAttribute);
                newCal = clearTime((Date) newAttribute);
            }
            return !oldCal.equals(newCal);
        }

        if (oldAttribute instanceof Number) {

            Number oldVal = (Number) oldAttribute;
            Number newVal = (Number) newAttribute;

            return !oldVal.equals(newVal);
        }

        if (exactMatch) {
            return !oldAttribute.toString().equals(newAttribute.toString());
        } else {
            return !oldAttribute.toString()
                                .trim()
                                .equalsIgnoreCase(newAttribute.toString().trim());
        }
    }
    
    /**
     *  Check if the content between separator are
     *  the same regardless location
     * @param value1
     * @param value2
     * @param separator
     * @return
     */
    public static boolean internalContentMatched(String value1, String value2, String separator) {

        if (isBlankOrNull(separator)) {
            throw new IllegalArgumentException("separator cannot be null");
        }
        if ((isBlankOrNull(value1) && !isBlankOrNull(value2)) || (!isBlankOrNull(value1) && isBlankOrNull(value2))) {
            return false;
        }

        String[] values1 = value1.split(separator);
        String[] values2 = value2.split(separator);

        if (values1.length != values2.length) {
            return false;
        }

        List val2List = Arrays.asList(values2);

        for (String val : values1) {

            if (!val2List.contains(val)) {
                return false;
            }
        }
        return true;
    }

    /**
     * is Date In Period
     * @param date : Date to check
     * @param startDate : start of date
     * @param period : period 
     * @return true if is in period
     */
    public static boolean isDateInPeriod(Date date, Date startDate, Integer period) {
        
        if(date == null || startDate == null || period == null){
            return false;
        }
        startDate = clearTime(startDate);
        
        date = clearTime(date);

        Date endDate = addDaysToDate(startDate, period);
        
        return isDateBetweenTwoDates(date, startDate, endDate);

    }
    
    /**
     *
     * @return String
     */
    public static String getOnlyArabicCharacters(String word) {
        StringBuilder arabicWord = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            int chars = word.codePointAt(i);
            if (chars >= 0x0600 && chars <= 0x06E0) {
                arabicWord.append(word.charAt(i));
            }
            if (chars == 0x0020 && arabicWord.length() != 0) {
                arabicWord.append(" ");
            }
        }
        return arabicWord.toString();
    }
    
    /**
     *  Has Special Characters Only
     *
     *  Short Description:
     *  The description shouldn't have s Spicial Characters
     *  @param description
     *  return description
     */
    public static String hasSpecialCharactersOnly(String description) {

        if (GlobalUtilities.isBlankOrNull(description)) {
            return null;
        }

        char[] descriptionArr = description.toCharArray();
        boolean isDescHasSpace = false;

        for (int j = 0; j < descriptionArr.length; j++) {
            if (Character.isSpaceChar(descriptionArr[j])) {
                isDescHasSpace = true;
                break;
            }
        }

        if (!isDescHasSpace && GlobalUtilities.isEnglishTextWithoutSpecialChar(description)) {
            return description;
        } else if (!isDescHasSpace && GlobalUtilities.isArabicText(description)) {
            return description;
        }
        return null;
    }

    /**
     * Return List of Ids of from List of ValueObject
     * @param List of <T>
     * @return List of Ids
     */
    public static <T extends ValueObject> List<Long> getListOfIds(List<T> list){
        if(list == null || list.isEmpty()){
            return null;
        }
        List<Long> idsList = new ArrayList<Long>();
        for(T t : list){
            if(t.getId() == null){
                continue;
            }
            idsList.add(t.getId());
        }
        return idsList;
        
    }
    
    /**
     * Get Byte Array Content Type.
     * 
     * @param content File Content.
     * @return Byte Array Content Type.
     */
    public static String getByteArrayContentType(byte[] content) {
        
        try {
            InputStream is = new BufferedInputStream(new ByteArrayInputStream(content));
            String mimeType = java.net.URLConnection.guessContentTypeFromStream(is);
            return mimeType;
        } catch (Exception ex) {
            return null;
        }
    }
    
    /**
     * Validate Escape Parameters to avoid SQL injection
     *
     * @param param
     * @return
     */
    public static boolean validateEscapeParam(String param){
        if(isBlankOrNull(param)){
            return true;
        }
        
        String[] specialCharacters = new String[]{"^","$","*","'","?","|","]","[",":","%","=",";","\\","\\\\","\\/"};
        
        for(String charcter : specialCharacters){
          if(param.toLowerCase().contains(charcter)){
            return false;
          }
        }
        return true;
    }
    
    /**
     * Format Oracle Date Ar.
     * 
     * @param date : Date.
     * @return formatOracleDateAr.
     */
    public static String formatOracleDateAr(Date date) throws ParseException {
        if (date == null) {
            return null;
        }
        
        return DATE_TIME_ORACLE_FORMAT_AR.format(date);
    }

    /**
     * Convert Byte To HexString.
     *
     * @param bytes , hexaLength
     * @return String
     */
    public static String bytesToHexString(byte[] bytes, int hexaLength) {
        if (hexaLength <= -1) {
            return DatatypeConverter.printHexBinary(bytes);
        }
        byte[] newb = new byte[hexaLength];
        for (int i = 0; i < hexaLength; ++i) {
            newb[i] = bytes[i];
        }
        return DatatypeConverter.printHexBinary(newb);
    }
    
    
   
   
    /**
     *  Generate random number with specific length
     * @param length
     * @return
     */
    public static int generateRandomNumber(int length) {

        StringBuilder minBuilder = new StringBuilder(length);
        StringBuilder maxBuilder = new StringBuilder(length);
        minBuilder.append('1');
        maxBuilder.append('9');
        for (int i = 0; i < length - 1; i++) {
            minBuilder.append('0');
            maxBuilder.append('9');
        }

        int max = getInteger(maxBuilder);
        int min = getInteger(minBuilder);
        int random = new SecureRandom().nextInt((max - min)) + min;
        return random;
    }
    
    /**
     * @param num to check
     * @return true if the num is even otherwise return false
     */
    public static boolean isEven(int num) {

        if ((num % 2) == 0) {
            return true;
        }
        return false;
    }

    /**
     * is Identical
     * @param <T>
     * @param collection1
     * @param collection2
     * @return
     */
    public static <T> boolean isIdentical(Collection<T> collection1, Collection<T> collection2){
        if(isBlankOrNull(collection1) && isBlankOrNull(collection2)){
            return true;
        }
        
        if(isBlankOrNull(collection1) || isBlankOrNull(collection2)){
            return false;
        }
        
        if(collection1.size() != collection2.size()){
            return false;
        }
        
        if(!collection1.containsAll(collection2) || !collection2.containsAll(collection1)){
            return false;
        }
        
        return true;
        
    }

    /**
     * Get Integer object or null if the value is null or empty string.
     *
     * @param value String value to be tested and parsed.
     * @return Integer object or null if the value is null or empty string.
     */
    public static BigInteger getBigInteger(Object value) {
        if (isBlankOrNull(value)) {
            return null;
        }

        return new BigInteger(value.toString().trim());
    }

    /**
     * Fix other countries Plate no if its contain characters and numbers
     * @param plateNo
     * @return Plate no
     */
    public static String fixCountryPlateNumber(String plateNo) {
        if (isBlankOrNull(plateNo)) {
            return plateNo;
        }
        String digitsSequence = "";
        String charSequence = "";

        for (int index = 0; index < plateNo.length(); index++) {
            String character = plateNo.charAt(index)+"";
            if(GlobalUtilities.isNumeric(character) || character.equals(" ")) {
                digitsSequence += character;
            } else if(character.trim() != "") {
                charSequence += character.trim() + " ";
            }
        }
        if (!isBlankOrNull(charSequence) && !isBlankOrNull(digitsSequence) ){
           return (charSequence.trim()+ " " +digitsSequence.trim() );
        } else if(!isBlankOrNull(charSequence) && isBlankOrNull(digitsSequence)) {
            return charSequence.trim();
        } else if(isBlankOrNull(charSequence) && !isBlankOrNull(digitsSequence)) {
            return digitsSequence.trim();
        } else {
            return plateNo;
        }
    }

    public static String formatDateWithoutTime(Date date) {
        if (date == null) {
            return null;
        }

        return ORACLE_DATE_FORMAT.format(date);
    }

    /**
     * Get business rule arabic description.
     * 
     * @param ruleKey business rule key.
     * @return business rule arabic description.
     */
    public static String getBusinessRuleDescEn(String ruleKey) {
        BusinessRuleVO ruleVO = preferencesHandler.getBusinessRule(ruleKey);
        return (ruleVO != null) 
              ? ruleVO.getDescriptionEn() 
              : new StringBuffer("Business rule not found: ")
                   .append(ruleKey).toString();
    }
    /**
     * Trim String
     *
     * @param value
     *
     * @return trimed value
     */
    public static String trimString(String value) {
        if(GlobalUtilities.isBlankOrNull(value)) {
            return "";
        }
        return value.trim();
    }

    /**
     * Return mobile No in 10 Digints format by remove 971 and replace it by 0
     *
     * @param mobileNo Mobile No
     * @return Mobile No
     */
    public static String getMobileNo10DigitsFormat(String mobileNo){
        if (isBlankOrNull(mobileNo)) {
            return mobileNo;
        }
        if (mobileNo.length() == 10) {
            return mobileNo;
        }
        if (!mobileNo.startsWith("971")) {
            return mobileNo;
        }
        return mobileNo.replaceFirst("971","0");
    }

}
