/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mohamed Fayek      05/05/2015  - File created.
 */
package ae.eis.util.common;

/**
 * Filter List
 *
 * @author Mohamed Fayek
 * @version 1.00
 */
public interface FilterList<T,E> {
     boolean isMatched(T object, E text);
}
