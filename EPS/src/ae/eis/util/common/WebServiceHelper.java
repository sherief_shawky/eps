/*
 * Copyright (c) i-Soft 2008.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mohamed fayek      11/10/2014  - File created and implemented.
 * 
 */
package ae.eis.util.common;
import ae.eis.common.vo.CustomerVO;
import ae.eis.trs.vo.TransactionVO;

/**  
 * Web Service Helper.
 *
 * @auther Mohamed Fayek
 * 
 * @version 1.00 
 */
public class WebServiceHelper extends GlobalUtilities  {

    
    
    public WebServiceHelper() {
    }
    
    
    /**
     * Insert Trace Log.
     * 
     * @param trafficFileId : Traffic File Id.
     * @param serviceChannelId : Service Channel Id.
     * @param userName : user Name.
     */
    protected Long insertTraceLog(Long trafficFileId, 
                                Long serviceChannelId, 
                                String userName,
                                Exception ex) {
    //TODO replace by new logging implementation
//        TraceLogVO vo = new TraceLogVO();
//        
//        vo.setTransactionVO(new TransactionVO());
//        vo.setTrafficFile(new CustomerVO());
//        vo.getTrafficFile().setId(trafficFileId);
//        vo.setServiceChannel(new ServiceChannelVO());
//        vo.getServiceChannel().setId(serviceChannelId);
//        vo.setLogType(new Long(vo.LOG_TYPE_WS_EXCEPTION.intValue()));
//        vo.setLogSummeryAr(GlobalUtilities.getStackTrace(ex).substring(0,1022));
//        vo.setLogSummeryEn(GlobalUtilities.getStackTrace(ex).substring(0,1022));
//        vo.setLogDetails(GlobalUtilities.getStackTrace(ex));
//        vo.setCreatedBy(userName);
//        
        ex.printStackTrace();
        return 0L;
    }
    
}