/*
 * Copyright (c) i-Soft 2007.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mohammad Mashagbeh 22/06/2017  - File created.
 */

package ae.eis.util.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * URL Shortener.
 *
 * @author Mohammad Mashagbeh
 * @version 1.00
 */
public class URLShortener {
    private static final long serialVersionUID = 1L;
    public static final String GOOGLE_URL_SHORTENER = "https://www.googleapis.com/urlshortener/v1/url?key=";
    
    // API Key
    public static final String API_KEY = "AIzaSyCFmoN3EVgytfPB5MUUKLedMfxGvg3mNMo";
    
    public static void main(String arg[]) {
        System.out.println("1 >> "+getShortenUrl("http://www.google.com"));    
        System.out.println("2 >> "+getShortenUrl("https://trafficstg.rta.ae:7781/trfesrv/public_resources/public-access.do?switchLanguage=en&noCache=1498107681742"));    
        System.out.println("4 >> "+getShortenUrl("http://192.168.0.230:7101/trfesrv/public_resources/revamp/common/verify-sms-service.do?sellPlateId=1511076&trafficFileNo=10325477&serviceCode=401&ownedPlateId=1836697&switchLanguage=en&noCache=1496559821879"));
        System.out.println("5 >> "+getShortenUrl("http://trafficstg.rta.ae:7781/trfesrv/public_resources/revamp/common/verify-sms-service.do?sellPlateId=1511076&trafficFileNo=10325477&serviceCode=401&ownedPlateId=1836697&switchLanguage=en&noCache=1496559821879"));
        System.out.println("6 >> "+getShortenUrl("https://stg12c:7779/trfesrv/public_resources/spl/public_special_plate_inquiry.do?serviceCode=409"));
    }
    
    /**
     * Get Shorten Url
     * @param longUrl Long Url
     * 
     * @return Shorten Url
     */
    public static String getShortenUrl(String longUrl) {
        String data = "{\"longUrl\": '" + longUrl + "'}";
        try {
            URL url = new URL(GOOGLE_URL_SHORTENER + API_KEY);
    
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "toolbar");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
    
            OutputStreamWriter output = new OutputStreamWriter(connection.getOutputStream());
            output.write(data);
            output.flush();
    
            BufferedReader response = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = "";
            String line;
            while ((line = response.readLine()) != null) {
                result += line;
            }
    
            ObjectMapper mapper = new ObjectMapper();
            Map map = mapper.readValue(result, Map.class);
    
            output.close();
            response.close();
    
            return (String) map.get("id");
    
        } catch (Exception e) {
            e.printStackTrace();
            return longUrl;
        }
    }
}