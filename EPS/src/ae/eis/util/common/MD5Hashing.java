/*
 * Copyright (c) i-Soft 2008.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Alaa Salem         09/05/2010  - File created.
 */
package ae.eis.util.common;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * MD5 Hashing Utility Class.
 *
 * @author Alaa Salem
 * @version 1.00
 */
public class MD5Hashing  {

    /*
     * Constants.
     */

    /** Message Digest Version. */
    private static final String MD_VER = "MD5";
    
    /** Message Encoding. */
    private static final String ENCODING = "UTF-8";
    
    /** HEX Digits Lookup Table. */
    private static final char[] HEX_TABLE = new char[] {
        '0', '1', '2', '3', '4', '5', '6', '7', 
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
        
    };
    
    /** MD5Hashing Logger. */
    private static Logger logger = null;    
    
    static {
        logger = Logger.getLogger(MD5Hashing.class.getName());
    }    
    
    /**
     * Converts Array Of Bytes To HEX String.
     * 
     * @param input Array Og Bytes.
     * @return HEX String.
     */
    private static String hex(byte[] input) {
    
        // Create a StringBuffer 2x the size of the hash array.
        StringBuffer sb = new StringBuffer(input.length * 2);
    
        // Retrieve the byte array data, convert it to hex
        // and add it to the StringBuffer.
        for(int index = 0; index < input.length; index++) {
            sb.append(HEX_TABLE[(input[index] >> 4) & 0xf]);
            sb.append(HEX_TABLE[input[index] & 0xf]);
        }
        return sb.toString();
    }    
    
    /**
     * Hash All Fields Using Given Secret Code Using MD5 Algorithm.
     * 
     * @param fields Map Of Fields To Be Encrypted.
     * @param secureSecret Secret Code Used To Encrypt Hash Data In The Map.
     * @return Hashed String.
     */
    private static String hashAllFields(Map fields, String secureSecret) {
    
        // Create a list and sort it.
        List fieldNames = new ArrayList(fields.keySet());
        Collections.sort(fieldNames);		       
    
        StringBuffer buf = new StringBuffer();
    
        // Iterate through the list and add the remaining field values
        Iterator itr = fieldNames.iterator();
    
        while(itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) fields.get(fieldName);
            if((fieldValue != null) && (fieldValue.length() > 0)) {
                buf.append(fieldValue);
            }
        } 
    
        // Create a buffer for the md5 input and add the secure secret last.
        buf.append(secureSecret);
        MessageDigest md5 = null;
        byte[] ba = null;
    
        // Create the md5 hash and UTF-8 encode it.
        try {
            md5 = MessageDigest.getInstance(MD_VER);
            ba = md5.digest(buf.toString().getBytes(ENCODING));
        } catch(Exception e) {
            e.printStackTrace();
        }
    
        return hex(ba);    
    }  
    
    /**
     * Check If The Result Of Hashing All Fields In The Map Is Equal To Field
     * ENCRYPTEDMSG Value In The Map.
     * 
     * @param hm Hash Map.
     * @param secureSecret Secret Code Used To Encrypt Hash Data In The Map.
     * @return true If The Result Of Hashing All Fields In The Map Is Equal To
     *              Field ENCRYPTEDMSG Value In The Map.
     */
    public boolean checksum(HashMap hm, String secureSecret){
    
        String hashedValue = (String)hm.get("ENCRYPTEDMSG");		
        hm.remove("ENCRYPTEDMSG");
        String convertedString = hashAllFields(hm, secureSecret);

        logger.info("Generated response encrypted message:: "+convertedString);
        
        return hashedValue.equals(convertedString);
    }
    
    /**
     * Hash All Fields Using Given Secret Code Using MD5 Algorithm.
     * 
     * @param hm Hash Map Of Fields To Be Encrypted.
     * @param secureSecret Secret Code Used To Encrypt Hash Data In The Map.
     * @return Hashed String.
     */
    public String md5Response(HashMap hm, String secureSecret){		
    
        return hashAllFields(hm, secureSecret);          
    }

}

