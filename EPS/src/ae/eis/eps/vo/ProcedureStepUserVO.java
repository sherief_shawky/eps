/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  07/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.common.vo.UserVO;

/**
 * Procedure Step group value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepUserVO extends TemplateStepUserVO {
    /*
     * Constructors
     */

    /**
     * Default constructor
     */
    public ProcedureStepUserVO() {
       // Default constructor
    }

    /**
     * Construct and initialize new ProcedureStepUserVO.
     *
     * @param vo Security group value object.
     */
    public ProcedureStepUserVO(UserVO vo) {
       super(vo);
    }
}