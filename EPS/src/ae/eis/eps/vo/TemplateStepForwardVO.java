/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Procedure template step forwards value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepForwardVO extends ValueObject {
    /*
     * Instance variables.
     */

    /** Parent template step. */
    private TemplateStepVO parentStep;

    /** Next template step. */
    private TemplateStepVO nextStep;

    /** Forward action type. */
    private Integer actionType;
    
    /** Forward action type Description */
    private String actionTypeDesc;

    /*
     * Constructors
     */

    /**
     * Defualt constructor
     */
    public TemplateStepForwardVO() {
        // Defualt constructor
    }

    /*
     * Methods
     */

    /**
     * Set Parent template step.
     *
     * @param parentStep Parent template step.
     */
    public void setParentStep(TemplateStepVO parentStep) {
        this.parentStep = parentStep;
    }

    /**
     * Get Parent template step.
     *
     * @return Parent template step.
     */
    public TemplateStepVO getParentStep() {
        return parentStep;
    }

    /**
     * Set Next template step.
     *
     * @param nextStep Next template step.
     */
    public void setNextStep(TemplateStepVO nextStep) {
        this.nextStep = nextStep;
    }

    /**
     * Get Next template step.
     *
     * @return Next template step.
     */
    public TemplateStepVO getNextStep() {
        return nextStep;
    }

    /**
     * Set Forward action type.
     *
     * @param actionType Forward action type.
     */
    public void setActionType(Integer actionType) {
        if (actionType != null &&
          ! actionType.equals(TemplateStepActionVO.TYPE_APPROVE) &&
          ! actionType.equals(TemplateStepActionVO.TYPE_REJECT)) {
            throw new IllegalArgumentException(
                "Invalid forward action type: " + actionType);
        }

        this.actionType = actionType;
    }

    /**
     * Check if this step forward is related to approve action.
     *
     * @return true if this step forward is related to approve action.
     */
    public boolean isApproveForward() {
        return actionType != null &&
               actionType.equals(TemplateStepActionVO.TYPE_APPROVE);
    }

    /**
     * Check if this step forward is related to reject action.
     *
     * @return true if this step forward is related to reject action.
     */
    public boolean isRejectForward() {
        return actionType != null &&
               actionType.equals(TemplateStepActionVO.TYPE_REJECT);
    }

    /**
     * Get Forward action type.
     *
     * @return Forward action type.
     */
    public Integer getActionType() {
        return actionType;
    }

    /**
     * set Action Type Description
     * 
     * @param actionTypeDesc
     */
    public void setActionTypeDesc(String actionTypeDesc) {
        this.actionTypeDesc = actionTypeDesc;
    }

    /**
     * get Action Type Description
     * 
     * @return actionTypeDesc
     */
    public String getActionTypeDesc() {
        return actionTypeDesc;
    }

}