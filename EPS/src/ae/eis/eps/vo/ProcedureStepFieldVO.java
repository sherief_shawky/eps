/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Procedure step-field value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepFieldVO extends TemplateStepFieldVO {
    /*
     * Instance variables.
     */
    
    /** Procedure step info. */
    private ProcedureStepVO procedureStep;

    /*
     * Methods
     */

    /**
     * Set Procedure step info.
     * 
     * @param procedureStep Procedure step info.
     */
    public void setProcedureStep(ProcedureStepVO procedureStep) {
        this.procedureStep = procedureStep;
    }

    /**
     * Get Procedure step info.
     * 
     * @return Procedure step info.
     */
    public ProcedureStepVO getProcedureStep() {
        return procedureStep;
    }
    
    /**
     * Get procedure field info.
     * 
     * @return procedure field info.
     */
    public ProcedureFieldVO getProcedureField() {
        return (ProcedureFieldVO) getField();
    }
}