/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 * 1.01  Ali Abdel-Aziz     11/01/2010  - Adding NotifyRequester
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Procedure template step value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepVO extends ValueObject {
    /*
     * Constants and class variables.
     */

    /** Step type values. */
    public static final Integer TYPE_HUMAN_TASK = new Integer(1);
    public static final Integer TYPE_NOTIFICATION = new Integer(2);
    public static final Integer TYPE_BUSINESS_ACTION = new Integer(3);
    public static final Integer TYPE_BEGIN = new Integer(4);
    public static final Integer TYPE_END = new Integer(5);

    private static final Set STEP_TYPES = new HashSet();
    static {
        STEP_TYPES.add(TYPE_HUMAN_TASK);
        STEP_TYPES.add(TYPE_NOTIFICATION);
        STEP_TYPES.add(TYPE_BUSINESS_ACTION);
        STEP_TYPES.add(TYPE_BEGIN);
        STEP_TYPES.add(TYPE_END);
    }
    
    /** Task assignment type values. */
    public static final Integer TASK_ASSIGNMENT_HIDDEN = new Integer(1);
    public static final Integer TASK_ASSIGNMENT_OPTIONAL = new Integer(2);
    public static final Integer TASK_ASSIGNMENT_MANDATORY = new Integer(3);
    public static final Integer TASK_ASSIGNMENT_REQUESTER = new Integer(4);
    
    public static final Integer VIEW_TYPE_STATIC = new Integer(1);
    public static final Integer VIEW_TYPE_DYNAMIC = new Integer(2);  

    /*
     * Instance variables
     */

    /** Step procedure template. */
    private ProcedureTemplateVO template;

    /** Procedure step arabic name. */
    private String nameAr;

    /** Procedure step english name. */
    private String nameEn;

    /** Procedure step type. */
    private Integer stepType;
    
    /** Procedure step type Description */
    private String stepTypeDesc;

    /** Step requester notification flag. */
    private Integer hasRequesterNotification;

    /** Step Step users notification flag. */
    private Integer hasStepUsersNotification;
    
    /** Step notifications list. */
    private List notificationsList;

    /** Step actions list. */
    private List actionsList;

    /** Step security groups list. */
    private List groupsList;

    /** Step forwards list. */
    private List forwardslist;
    
    /** Step forwards list with no security. */
    private List forwardslistNoSecurity;

    /** Step sequence number. */
    private Integer sequenceNo;
    
    /** Business action class full qualified name. */
    private String actionClass;
    
    /** Step fields. */
    private List fieldsList;
    
    /** Minimum number of attachments to be added to this step. */
    private Integer minAttachmentsCount;

    /** Maximum number of attachments to be added to this step. */
    private Integer maxAttachmentsCount;
    
    /** Task assignment type. */
    private Integer taskAssignmentType;
    
    /** View Type */
    private Integer viewType;
    
    /** View Link */
    private String viewLink;
    

    /*
     * Methods
     */

    /**
     * Set Procedure step arabic name.
     *
     * @param nameAr Procedure step arabic name.
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * Set Procedure step arabic name.
     *
     * @return Procedure step arabic name.
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * Get Procedure step english name.
     *
     * @param nameEn Procedure step english name.
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * Set Procedure step english name.
     *
     * @return Procedure step english name.
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * Set Procedure step type.
     *
     * @param type Procedure step type.
     */
    public void setStepType(Integer type) {
        if (! isValidStepType(type)) {
            throw new IllegalArgumentException("Invalid step type: " + type);
        }

        this.stepType = type;
    }

    /**
     * Get Procedure step type.
     *
     * @return Procedure step type.
     */
    public Integer getStepType() {
        return stepType;
    }
    
    /**
     * Set Step requester notification flag.
     *
     * @param hasRequesterNotification Step requester notification flag.
     */
    public void setHasRequesterNotification(Integer hasRequesterNotification) {
        this.hasRequesterNotification = hasRequesterNotification;
    }

    /**
     * Get Step requester notification flag.
     *
     * @return Step requester notification flag.
     */
    public Integer getHasRequesterNotification() {
        return hasRequesterNotification;
    }

    /**
     * Get Check if requester notification is enabled.
     *
     * @return true if requester notification is enabled.
     */
    public boolean hasRequesterNotification() {
        return hasRequesterNotification != null &&
               hasRequesterNotification.equals(TRUE);
    }

    /**
     * Set Step users notification flag.
     *
     * @param hasStepUsersNotification Step users notification flag.
     */
    public void setHasStepUsersNotification(Integer hasStepUsersNotification) {
        this.hasStepUsersNotification = hasStepUsersNotification;
    }

    /**
     * Get Step users notification flag.
     *
     * @return Step users notification flag.
     */
    public Integer getHasStepUsersNotification() {
        return hasStepUsersNotification;
    }

    /**
     * Get Check if step users notification is enabled.
     *
     * @return true if step users notification is enabled.
     */
    public boolean hasStepUsersNotification() {
        return hasStepUsersNotification != null &&
               hasStepUsersNotification.equals(TRUE);
    }

    /**
     * Check if this step-type value is valid.
     *
     * @param type step-Type value to be tested.
     * @return true if this STEP-type value is valid.
     */
    public static boolean isValidStepType(Integer type) {
        if (type == null) {
            return true;
        }

        return STEP_TYPES.contains(type);
    }

    /**
     * Set Step notifications list.
     *
     * @param notificationsList Step notifications list.
     */
    public void setNotificationsList(List notificationsList) {
        this.notificationsList = notificationsList;
    }

    /**
     * Get Step notifications list.
     *
     * @return Step notifications list.
     */
    public List getNotificationsList() {
        return notificationsList;
    }

    /**
     * Set Step actions list.
     *
     * @param actionsList Step actions list.
     */
    public void setActionsList(List actionsList) {
        this.actionsList = actionsList;
    }

    /**
     * Get Step actions list.
     *
     * @return Step actions list.
     */
    public List getActionsList() {
        return actionsList;
    }

    /**
     * Set Step security groups list.
     *
     * @param groupsList Step security groups list.
     */
    public void setGroupsList(List groupsList) {
        this.groupsList = groupsList;
    }

    /**
     * Get Step security groups list.
     *
     * @return Step security groups list.
     */
    public List getGroupsList() {
        return groupsList;
    }

    /**
     * Set Step forwards list.
     *
     * @param forwardslist Step forwards list.
     */
    public void setForwardslist(List forwardslist) {
        this.forwardslist = forwardslist;
    }

    /**
     * Get Step forwards list.
     *
     * @return Step forwards list.
     */
    public List getForwardslist() {
        return forwardslist;
    }

    /**
     * Set Step procedure template.
     *
     * @param template Step procedure template.
     */
    public void setTemplate(ProcedureTemplateVO template) {
        this.template = template;
    }

    /**
     * Get Step procedure template.
     *
     * @return Step procedure template.
     */
    public ProcedureTemplateVO getTemplate() {
        return template;
    }

    /**
     * Check if step type is Human-Task.
     *
     * @return true if step type is Human-Task.
     */
    public boolean isHumanTaskStep() {
        return stepType != null && stepType.equals(TYPE_HUMAN_TASK);
    }

    /**
     * Check if step type is Business-Action.
     *
     * @return true if step type is Business-Action.
     */
    public boolean isBusinessAction() {
        return stepType != null && stepType.equals(TYPE_BUSINESS_ACTION);
    }

    /**
     * Check if step type is Notification.
     *
     * @return true if step type is Notification.
     */
    public boolean isNotificationStep() {
        return stepType != null && stepType.equals(TYPE_NOTIFICATION);
    }

    /**
     * Check if step type is END.
     *
     * @return true if step type is END.
     */
    public boolean isProcedureEndStep() {
        return stepType != null && stepType.equals(TYPE_END);
    }

    /**
     * Check if step type is START.
     *
     * @return true if step type is START.
     */
    public boolean isProcedureStartStep() {
        return stepType != null && stepType.equals(TYPE_BEGIN);
    }

    /**
     * Set Step sequence number.
     *
     * @param sequenceNo Step sequence number.
     */
    public void setSequenceNo(Integer sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    /**
     * Get Step sequence number.
     *
     * @return Step sequence number.
     */
    public Integer getSequenceNo() {
        return sequenceNo;
    }

    /**
     * set Step Type Description
     * 
     * @param stepTypeDesc
     */
    public void setStepTypeDesc(String stepTypeDesc) {
        this.stepTypeDesc = stepTypeDesc;
    }

    /**
     * get Step Type Description
     * 
     * @return stepTypeDesc
     */
    public String getStepTypeDesc() {
        return stepTypeDesc;
    }

    /**
     * Set Business action class full qualified name.
     * 
     * @param actionClass Business action class full qualified name.
     */
    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }

    /**
     * Get Business action class full qualified name.
     * 
     * @return Business action class full qualified name.
     */
    public String getActionClass() {
        return actionClass;
    }

    /**
     * Set Step fields.
     * 
     * @param fieldsList Step fields.
     */
    public void setFieldsList(List fieldsList) {
        this.fieldsList = fieldsList;
    }

    /**
     * Get Step fields.
     * 
     * @return Step fields.
     */
    public List getFieldsList() {
        return fieldsList;
    }

    /**
     * Set Minimum number of attachments to be added to this step.
     * 
     * @param minAttachmentsCount Minimum number of attachments to be added 
     *        to this step.
     */
    public void setMinAttachmentsCount(Integer minAttachmentsCount) {
        this.minAttachmentsCount = minAttachmentsCount;
    }

    /**
     * Get Minimum number of attachments to be added to this step.
     * 
     * @return Minimum number of attachments to be added to this step.
     */
    public Integer getMinAttachmentsCount() {
        return minAttachmentsCount;
    }

    /**
     * Set Maximum number of attachments to be added to this step.
     * 
     * @param maxAttachmentsCount Maximum number of attachments to be added to 
     *        this step.
     */
    public void setMaxAttachmentsCount(Integer maxAttachmentsCount) {
        this.maxAttachmentsCount = maxAttachmentsCount;
    }

    /**
     * Get Maximum number of attachments to be added to this step.
     * 
     * @return Maximum number of attachments to be added to this step.
     */
    public Integer getMaxAttachmentsCount() {
        return maxAttachmentsCount;
    }

    /**
     * Check if this step has approve action.
     * 
     * @return true if this step has approve action.
     */
    public boolean hasApproveAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isApproveAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has reject action.
     * 
     * @return true if this step has reject action.
     */
    public boolean hasRejectAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isRejectAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has push-back action.
     * 
     * @return true if this step has push-back action.
     */
    public boolean hasPushBackAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isPushBackAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has "view step attachments" action.
     * 
     * @return true if this step has "view step attachments" action.
     */
    public boolean hasViewStepAttachmentsAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isViewStepAttachmentsAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has "view requester attachments" action.
     * 
     * @return true if this step has "view requester attachments" action.
     */
    public boolean hasViewRequesterAttachmentsAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isViewRequesterAttachmentsAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has "view users attachments" action.
     * 
     * @return true if this step has "view users attachments" action.
     */
    public boolean hasViewUsersAttachmentsAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isViewUsersAttachmentsAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has "delete requester attachments" action.
     * 
     * @return true if this step has "delete requester attachments" action.
     */
    public boolean hasDeleteRequesterAttachmentsAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isDeleteRequesterAttachmentsAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has "delete other steps attachments" action.
     * 
     * @return true if this step has "delete other steps attachments" action.
     */
    public boolean hasDeleteOtherStepsAttachmentsAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isDeleteOtherStepsAttachmentsAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if this step has "upload attachments" action.
     * 
     * @return true if this step has "upload attachments" action.
     */
    public boolean hasUploadAttachmentsAction() {
        if (getActionsList() == null) {
            return false;
        }
        
        for (int i = 0; i < getActionsList().size(); i++)  {
            TemplateStepActionVO action = (TemplateStepActionVO) actionsList.get(i);
            if (action.isUploadAttachmentsAction()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set Task assignment type.
     * 
     * @param type Task assignment type.
     */
    public void setTaskAssignmentType(Integer type) {
        if (type == null ||
            type.equals(TASK_ASSIGNMENT_HIDDEN) ||
            type.equals(TASK_ASSIGNMENT_OPTIONAL) ||
            type.equals(TASK_ASSIGNMENT_MANDATORY) ||
            type.equals(TASK_ASSIGNMENT_REQUESTER)) {
            this.taskAssignmentType = type;
            return;
        }
        
        throw new IllegalArgumentException("Invalid taskAssignmentType: " + type);
    }

    /**
     * Get Task assignment type.
     * 
     * @return Task assignment type.
     */
    public Integer getTaskAssignmentType() {
        return taskAssignmentType;
    }

    /**
     * Check if task assignment type is "Hidden".
     * 
     * @return true if task assignment type is "Hidden".
     */
    public boolean isTaskAssignmentHidden() {
        return taskAssignmentType != null && 
               taskAssignmentType.equals(TASK_ASSIGNMENT_HIDDEN);
    }

    /**
     * Check if task assignment type is "Optional".
     * 
     * @return true if task assignment type is "Optional".
     */
    public boolean isTaskAssignmentOptional() {
        return taskAssignmentType != null && 
               taskAssignmentType.equals(TASK_ASSIGNMENT_OPTIONAL);
    }

    /**
     * Check if task assignment type is "Mandatory".
     * 
     * @return true if task assignment type is "Mandatory".
     */
    public boolean isTaskAssignmentMandatory() {
        return taskAssignmentType != null && 
               taskAssignmentType.equals(TASK_ASSIGNMENT_MANDATORY);
    }

    /**
     * Check if task assignment type is "Requester".
     * 
     * @return true if task assignment type is "Requester".
     */
    public boolean isTaskAssignmentRequester() {
        return taskAssignmentType != null && 
               taskAssignmentType.equals(TASK_ASSIGNMENT_REQUESTER);
    }
    
    /**
     * Set View Type
     * 
     * @param viewType View Type
     */
    public void setViewType(Integer viewType) {
        if (viewType == null ||
            viewType.equals(VIEW_TYPE_STATIC) ||
            viewType.equals(VIEW_TYPE_DYNAMIC)) {
            this.viewType = viewType;
            return;
        }
        
        throw new IllegalArgumentException("Invalid viewType: " + viewType);    
    
    } 

    /**
     * Get View Type
     * 
     * @return viewType View Type
     */
    public Integer getViewType() {
        return viewType;
    }

    /**
     * Set View Link
     * 
     * @param viewLink View Link
     */
    public void setViewLink(String viewLink) {
        this.viewLink = viewLink;
    }

    /**
     * Get View Link
     * 
     * @return viewLink View Link
     */
    public String getViewLink() {
        return viewLink;
    }
    
    /**
     * Is Static View
     * 
     * @return 
     */
    public boolean isViewTypeStatic() {
        return viewType != null && 
               viewType.equals(VIEW_TYPE_STATIC);
    }

    /**
     * Is Dynamic View
     * 
     * @return 
     */
    public boolean isViewTypeDynamic() {
        return viewType != null && 
               viewType.equals(VIEW_TYPE_DYNAMIC);
    }
    
    /**
     * Set forwards list No Security
     * 
     * @param forwardslistNoSecurity forwards list No Security
     */
    public void setForwardslistNoSecurity(List forwardslistNoSecurity) {
        this.forwardslistNoSecurity = forwardslistNoSecurity;
    }
    
    /**
     * Get forwards list No Security
     * 
     * @return forwardslistNoSecurity forwards list No Security
     */
    public List getForwardslistNoSecurity() {
        return forwardslistNoSecurity;
    }
}