/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  15/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.AttachmentVO;

/**
 * Temporary procedure attachment value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemporaryAttachmentVO extends AttachmentVO {
    /*
     * Instants variables.
     */

    /** Attachment reference number. */
    private Long sequenceNo;
    
    /*
     * Constructors
     */
    
    /**
     * Default constructor
     */
    public TemporaryAttachmentVO() {
        // Empty body
    }

    /**
     * Construct and initialize new value object.
     * 
     * @param attachmentVO Attachment info.
     */
    public TemporaryAttachmentVO(AttachmentVO attachmentVO) {
        setAttachmentInfo(attachmentVO);
    }

    /*
     * Methods
     */

    /**
     * Set Attachment reference number.
     * 
     * @param sequenceNo Attachment reference number.
     */
    public void setSequenceNo(Long sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    /**
     * Get Attachment reference number.
     * 
     * @return Attachment reference number.
     */
    public Long getSequenceNo() {
        return sequenceNo;
    }
}