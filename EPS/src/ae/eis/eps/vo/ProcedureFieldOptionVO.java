/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 */

package ae.eis.eps.vo;

/**
 * Procedure field option value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureFieldOptionVO extends TemplateFieldOptionVO {
    /*
     * Instance variables
     */
     
    /** Procedure field info. */
    private ProcedureFieldVO procedureField;

    /**
     * Related field option. This option will be displayed only if related 
     * field option was selected.
     */
    private ProcedureFieldOptionVO relatedFieldOption;

    /*
     * Methods
     */
    
    /**
     * Set Procedure field info.
     * 
     * @param procedureField Procedure field info.
     */
    public void setProcedureField(ProcedureFieldVO procedureField) {
        this.procedureField = procedureField;
    }

    /**
     * Get Procedure field info.
     * 
     * @return Procedure field info.
     */
    public ProcedureFieldVO getProcedureField() {
        return procedureField;
    }

    /**
     * Set Related field option. This option will be displayed only if 
     * related field option was selected.
     * 
     * @param option Related field option.
     */
    public void setRelatedFieldOption(ProcedureFieldOptionVO option) {
        this.relatedFieldOption = option;
    }

    /**
     * Get Related field option. This option will be displayed only if 
     * related field option was selected.
     * 
     * @return Related field option.
     */
    public ProcedureFieldOptionVO getRelatedFieldOption() {
        return relatedFieldOption;
    }
    
    /**
     * Check if this field option has related field option. This option will be
     * displayed only if related field option was selected.
     * 
     * @return true if this field option has related field option.
     */
    public boolean hasRelatedFieldOption() {
        return relatedFieldOption != null &&
               relatedFieldOption.getOrderNo() != null &&
               relatedFieldOption.getProcedureField() != null &&
               relatedFieldOption.getProcedureField().getId() != null;
    }
}