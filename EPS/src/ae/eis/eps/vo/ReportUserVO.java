/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  04/10/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.common.vo.UserVO;
import ae.eis.util.vo.ValueObject;

/**
 * Procedure template report user value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ReportUserVO extends ValueObject {
    /*
     * Fields
     */
    
    /** Authorized user info. */
    private UserVO user;
    
    /** Related report info. */
    private TemplateReportVO report;
    
    /*
     * Methods
     */

    /**
     * Set Authorized user info.
     * 
     * @param user Authorized user info.
     */
    public void setUser(UserVO user) {
        this.user = user;
    }

    /**
     * Get Authorized user info.
     * 
     * @return Authorized user info.
     */
    public UserVO getUser() {
        return user;
    }

    /**
     * Set Related report info.
     * 
     * @param report Related report info.
     */
    public void setReport(TemplateReportVO report) {
        this.report = report;
    }

    /**
     * Get Related report info.
     * 
     * @return Related report info.
     */
    public TemplateReportVO getReport() {
        return report;
    }
}