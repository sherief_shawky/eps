/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;
import java.util.Date;
import java.util.List;

/**
 * Procedure template field value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateFieldVO extends ValueObject {
    /*
     * Constants and class variables.
     */

    /** Field type values. */
    public static final Integer TYPE_TEXT = new Integer(1);
    public static final Integer TYPE_NUMBER = new Integer(2);
    public static final Integer TYPE_DATE = new Integer(3);
    public static final Integer TYPE_COMBO_BOX = new Integer(4);
    public static final Integer TYPE_TEXT_AREA = new Integer(5);
    public static final Integer TYPE_CHECK_BOX = new Integer(6);
    public static final Integer TYPE_MOBILE = new Integer(7);
    public static final Integer TYPE_PHONE = new Integer(8);
    public static final Integer TYPE_EMAIL = new Integer(9);
    public static final Integer TYPE_TABLE = new Integer(10);
    public static final Integer TYPE_GOOGLE_MAP = new Integer(11);
    public static final Integer TYPE_TIME = new Integer(12); 
    public static final Integer TYPE_PLATE_LOOKUP = new Integer(13); 

    /*
     * Instance variables
     */
    
    /** Field procedure template. */
    private ProcedureTemplateVO procedureTemplate;
    
    /** Field type. */
    private Integer type;

    /** Field type description. */
    private String typeDesc;
    
    /** Field code. */
    private Integer code;
    
    /** Field label. */
    private String label;
    
    /** Field value. */
    private String value;
    
    /** Field value description */
    private String valueDesc;
    
    /** Field size. */
    private Integer size;
    
    /** Field options List. */
    private List optionsList;
    
    /**
     * Indicator that other list options are dependent (filtered) on the values 
     * of this list options.
     */
    private Integer hasRelatedOptions;
        
    /** Maximum Row Number */        
    private Integer maxRowNo;
        
    /*
     * Constructors
     */
     
    /**
     * Default constructor
     */
    public TemplateFieldVO() {
        // Empty body
    }
    
    /**
     * Construct and initialize new template field instance.
     * 
     * @param vo template field initialial values.
     */
    public TemplateFieldVO(TemplateFieldVO vo) {
        if (vo == null) {
            return;
        }
        
        setId(vo.getId());
        setCode(vo.getCode());
        setHasRelatedOptions(vo.getHasRelatedOptions());
        setLabel(vo.getLabel());
        setSize(vo.getSize());
        setType(vo.getType());
        setTypeDesc(vo.getTypeDesc());
        setValue(vo.getValue());
        setValueDesc(vo.getValueDesc());
        setMaxRowNo(vo.getMaxRowNo());
    }

    /*
     * Methods
     */

    /**
     * Set Field procedure template.
     * 
     * @param procedureTemplate Field procedure template.
     */
    public void setProcedureTemplate(ProcedureTemplateVO procedureTemplate) {
        this.procedureTemplate = procedureTemplate;
    }

    /**
     * Get Field procedure template.
     * 
     * @return Field procedure template.
     */
    public ProcedureTemplateVO getProcedureTemplate() {
        return procedureTemplate;
    }

    /**
     * Set Field type.
     * 
     * @param type Field type.
     */
    public void setType(Integer type) {
        if (type == null                ||
            type.equals(TYPE_TEXT)      ||
            type.equals(TYPE_NUMBER)    ||
            type.equals(TYPE_DATE)      ||
            type.equals(TYPE_COMBO_BOX) ||
            type.equals(TYPE_TEXT_AREA) ||
            type.equals(TYPE_CHECK_BOX) ||
            type.equals(TYPE_MOBILE)    ||
            type.equals(TYPE_PHONE)     ||
            type.equals(TYPE_EMAIL)     ||
            type.equals(TYPE_TABLE) ||
            type.equals(TYPE_GOOGLE_MAP) ||
            type.equals(TYPE_TIME) ||
            type.equals(TYPE_PLATE_LOOKUP)) {
            this.type = type;
            return;
        }
        
        throw new IllegalArgumentException("Invalid field type: " + type);
    }

    /**
     * Check if the field type is text.
     * 
     * @return true if the field type is text.
     */
    public boolean isTextField() {
        return type != null && type.equals(TYPE_TEXT);
    }

    /**
     * Check if the field type is number.
     * 
     * @return true if the field type is number.
     */
    public boolean isNumberField() {
        return type != null && type.equals(TYPE_NUMBER);
    }

    /**
     * Check if the field type is date.
     * 
     * @return true if the field type is date.
     */
    public boolean isDateField() {
        return type != null && type.equals(TYPE_DATE);
    }

    /**
     * Check if the field type is combo-box.
     * 
     * @return true if the field type is combo-box.
     */
    public boolean isComboBoxField() {
        return type != null && type.equals(TYPE_COMBO_BOX);
    }

    /**
     * Check if the field type is text area.
     * 
     * @return true if the field type is text area.
     */
    public boolean isTextAreaField() {
        return type != null && type.equals(TYPE_TEXT_AREA);
    }

    /**
     * Check if the field type is check box.
     * 
     * @return true if the field type is check box.
     */
    public boolean isCheckBoxField() {
        return type != null && type.equals(TYPE_CHECK_BOX);
    }
    
    /**
     * Check if the field type is mobile.
     * 
     * @return true if the field type is mobile.
     */
    public boolean isMobileField() {
        return type != null && type.equals(TYPE_MOBILE);
    }
    
    /**
     * Check if the field type is phone.
     * 
     * @return true if the field type is phone.
     */
    public boolean isPhoneField() {
        return type != null && type.equals(TYPE_PHONE);
    }
    
    /**
     * Check if the field type is email.
     * 
     * @return true if the field type is email.
     */
    public boolean isEMailField() {
        return type != null && type.equals(TYPE_EMAIL);
    }    
    
    /**
     * Is Table Field
     * 
     * @return true if the field type is table.
     */
    public boolean isTableField() {
        return type != null && type.equals(TYPE_TABLE);
    }   
    
    /**
     * Is Google Map Field
     * 
     * @return true if the field type is Google Map.
     */
    public boolean isGoogleMapField() {
        return type != null && type.equals(TYPE_GOOGLE_MAP);
    } 
    
    /**
     * Is Time Field
     * 
     * @return true if the field type is Time.
     */
    public boolean isTimeField() {
        return type != null && type.equals(TYPE_TIME);
    } 
    
    /**
     * Is Plate Lookup.
     * 
     * @return true if the field type is plate lookup.
     */
    public boolean isPlateLookup() {
        return type != null && type.equals(TYPE_PLATE_LOOKUP);
    }
    
    /**
     * Get Field type.
     * 
     * @return Field type.
     */
    public Integer getType() {
        return type;
    }

    /**
     * Set Field code.
     * 
     * @param code Field code.
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Get Field code.
     * 
     * @return Field code.
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Set Field label.
     * 
     * @param label Field label.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Get Field label.
     * 
     * @return Field label.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Set Field value.
     * 
     * @param value Field value.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Get Field value.
     * 
     * @return Field value.
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Get Field value as Long object.
     * 
     * @return Field value.
     */
    public Long getLongValue() {
        return GlobalUtilities.getLong(getValue());
    }

    /**
     * Get Field value as Integer object.
     * 
     * @return Field value.
     */
    public Integer getIntegerValue() {
        return GlobalUtilities.getInteger(getValue());
    }

    /**
     * Get Field value as Date object.
     * 
     * @return Field value.
     */
    public Date getDateValue() {
        try {
            return GlobalUtilities.getDate(getValue());

        } catch (Exception ex) {
            throw new IllegalArgumentException("Invalid date value: " + getValue());
        }
    }
    
    /**
     * Get Field value as Date object.
     * 
     * @return Field value.
     */
    public Date getDateTimeValue() {
        try {
            return GlobalUtilities.getDateTime(getValue());

        } catch (Exception ex) {
            throw new IllegalArgumentException("Invalid date value: " + getValue());
        }
    }
    
    /**
     * Get Field Value as boolean value.
     * 
     * @return Field Value.
     */
    public boolean getBooleanValue() {
        return Choice.getBoolean(new Choice(getIntegerValue()));
    }

    /**
     * Set Field size.
     * 
     * @param size Field size.
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * Get Field size.
     * 
     * @return Field size.
     */
    public Integer getSize() {
        return size;
    }


    /**
     * set Type Desc
     * 
     * @param typeDesc Type Desc
     */
    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    /**
     * get Type Desc
     * 
     * @return typeDesc Type Desc
     */
    public String getTypeDesc() {
        return typeDesc;
    }

    /**
     * Set Field options List.
     * 
     * @param optionsList Field options List.
     */
    public void setOptionsList(List optionsList) {
        this.optionsList = optionsList;
    }

    /**
     * Get Field options List.
     * 
     * @return Field options List.
     */
    public List getOptionsList() {
        return optionsList;
    }

    /**
     * Set Value Description
     * 
     * @param valueDesc Value Description
     */
    public void setValueDesc(String valueDesc) {
        this.valueDesc = valueDesc;
    }

    /**
     * Get Value Description
     * 
     * @return valueDesc Value Description
     */
    public String getValueDesc() {
        return valueDesc;
    }

    /**
     * Set Indicator that other list options are dependent (filtered) on the 
     * values of this list options.
     * 
     * @param flag Indicator that other list options are dependent (filtered) 
     *        on the values of this list options.
     */
    public void setHasRelatedOptions(Integer flag) {
        if (flag == null || flag.equals(TRUE) || flag.equals(FALSE)) {
            this.hasRelatedOptions = flag;
            return;
        }
        
        throw new IllegalArgumentException("Invalid hasRelatedOptions value: "
            + flag);
    }

    /**
     * Get Indicator that other list options are dependent (filtered) on 
     * the values of this list options.
     * 
     * @return Indicator that other list options are dependent (filtered) on 
     * the values of this list options.
     */
    public Integer getHasRelatedOptions() {
        return hasRelatedOptions;
    }

    /**
     * Check if other list options are dependent (filtered) on the values of 
     * this list options.
     * 
     * @return true if other list options are dependent (filtered) on the 
     *         values of this list options.
     */
    public boolean hasRelatedOptions() {
        return hasRelatedOptions != null && hasRelatedOptions.equals(TRUE);
    }

    /**
     * Set Maximum Row Number
     * 
     * @param maxRowNo Maximum Row Number
     */
    public void setMaxRowNo(Integer maxRowNo) {
        this.maxRowNo = maxRowNo;
    }

    /**
     * Get Maximum Row Number
     * 
     * @return maxRowNo Maximum Row Number
     */
    public Integer getMaxRowNo() {
        return maxRowNo;
    }
}