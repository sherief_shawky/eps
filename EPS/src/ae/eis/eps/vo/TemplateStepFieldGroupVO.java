/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  23/11/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Template step-fields group value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepFieldGroupVO extends ValueObject {
    /*
     * Fields
     */
    
    /** Arabic group name. */
    private String nameAr;
    
    /** English group name. */
    private String nameEn;
    
    /** Group order number. */
    private Integer orderNo;
    
    /** Label Width */
    private Integer labelWidth;
    
    /** Single Column Field Width */
    private Integer singleColumnFieldWidth;
    
    /** Multi Column Field Width */
    private Integer multiColumnFieldWidth;
    
    /** Related template step info. */
    private TemplateStepVO templateStep;
    
    /** Fields list. */
    protected List<TemplateStepFieldVO> fieldsList = new ArrayList();
    
    /** constants */
    public static final Integer LABEL_WIDTH = new Integer(160);
    public static final Integer SINGLE_COLUMN_FIELD_WIDTH = new Integer(154);
    public static final Integer MULTI_COLUMN_FIELD_WIDTH = new Integer(500);
    
    /*
     * Fields
     */

    /**
     * Set Arabic group name.
     * 
     * @param nameAr Arabic group name.
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * Get Arabic group name.
     * 
     * @return Arabic group name.
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * Set English group name.
     * 
     * @param nameEn English group name.
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * Get English group name.
     * 
     * @return English group name.
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * Set Group order number.
     * 
     * @param orderNo Group order number.
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * Get Group order number.
     * 
     * @return Group order number.
     */
    public Integer getOrderNo() {
        return orderNo;
    }

    /**
     * Set Related template step info.
     * 
     * @param templateStep Related template step info.
     */
    public void setTemplateStep(TemplateStepVO templateStep) {
        this.templateStep = templateStep;
    }

    /**
     * Get Related template step info.
     * 
     * @return Related template step info.
     */
    public TemplateStepVO getTemplateStep() {
        return templateStep;
    }
    
    /**
     * Add step field.
     * 
     * @param vo Template step field info.
     */
    public void addField(TemplateStepFieldVO vo) {
        if (vo == null) {
            throw new IllegalArgumentException("NULL TemplateStepFieldVO parameter");
        }
        
        this.fieldsList.add(vo);
    }

    /**
     * Add step field.
     * 
     * @param vo Template step field info.
     * @param index New field index.
     */
    public void addField(TemplateStepFieldVO vo, int index) {
        if (vo == null) {
            throw new IllegalArgumentException("NULL TemplateStepFieldVO parameter");
        }
        
        if (index < 0) {
            throw new IllegalArgumentException("Invalid field index: " + index);
        }
        
        if (index > fieldsList.size()) {
            throw new IllegalArgumentException(
                "Index must be less than fields count: index=" + index);
        }
        
        this.fieldsList.add(index, vo);
    }

    /**
     * Add step fields.
     * 
     * @param newFieldsList New template step fields.
     */
    public void addField(List newFieldsList) {
        if (newFieldsList == null || newFieldsList.size() == 0) {
            clearFields();
            return;
        }

        for (int i = 0; i < newFieldsList.size(); i++)  {
            TemplateStepFieldVO vo = (TemplateStepFieldVO) newFieldsList.get(i);
            addField(vo);
        }
    }
    
    /**
     * Clear fields list.
     */
    public void clearFields() {
        this.fieldsList.clear();
    }
    
    /**
     * Get group fields count.
     * 
     * @return group fields count.
     */
    public int getFieldsCount() {
        return this.fieldsList.size();
    }
    
    /**
     * Get template group fields.
     * 
     * @return template group fields.
     */
    public TemplateStepFieldVO[] getTemplateStepFields() {
        return (TemplateStepFieldVO[]) fieldsList.toArray(
            new TemplateStepFieldVO[fieldsList.size()]);
    }
    
    /**
     * Set Label Width
     * 
     * @param labelWidth labelWidth
     */
    public void setLabelWidth(Integer labelWidth) {
        this.labelWidth = labelWidth;
    }

    /**
     * Get Label Width
     * 
     * @return labelWidth
     */
    public Integer getLabelWidth() {
        return labelWidth;
    }

    /**
     * Set Single Column Field Width
     * 
     * @param singleColumnFieldWidth Single Column Field Width
     */
    public void setSingleColumnFieldWidth(Integer singleColumnFieldWidth) {
        this.singleColumnFieldWidth = singleColumnFieldWidth;
    }

    /**
     * Get Single Column Field Width
     * 
     * @return singleColumnFieldWidth Single Column Field Width
     */
    public Integer getSingleColumnFieldWidth() {
        return singleColumnFieldWidth;
    }

    /**
     * Set Multi Column Field Width
     * 
     * @param multiColumnFieldWidth Multi Column Field Width
     */
    public void setMultiColumnFieldWidth(Integer multiColumnFieldWidth) {
        this.multiColumnFieldWidth = multiColumnFieldWidth;
    }

    /**
     * Get Multi Column Field Width
     * 
     * @return multiColumnFieldWidth Multi Column Field Width
     */
    public Integer getMultiColumnFieldWidth() {
        return multiColumnFieldWidth;
    }


    public void setFieldsList(List<TemplateStepFieldVO> fieldsList) {
        this.fieldsList = fieldsList;
    }

    public List<TemplateStepFieldVO> getFieldsList() {
        return fieldsList;
    }

}
