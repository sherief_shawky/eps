/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00     Mena Emiel      24/02/2011  - File created and implemented.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;
import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Table column value value object.
 *
 * @author Mena Emiel
 * @version 1.00
 */
 
public class TableRowVO extends ValueObject {
    
    /*
     * Fields
     */
    
    /** columns map. */
    private List columnsList;
    
    /*
     * Constructors
     */
     
    /**
     * Default constructor.
     */
    public TableRowVO() {
        columnsList = new ArrayList();
    }
    
    /**
     * Add Column
     * 
     * @param tableColumnValueVO Table Column Value Info
     */
    public void addColumn(TableColumnValueVO tableColumnValueVO) {
        if (tableColumnValueVO == null) {
            throw new IllegalArgumentException("Null Table Column Value Info");
        }
        
        if (isBlankOrNull(tableColumnValueVO.getValue())) {
            throw new IllegalArgumentException("Null Table Column Value");
        }
        
        if (tableColumnValueVO.getTableHeaderVO() == null) {
            throw new IllegalArgumentException("Null Table Header Info");
        }
        
        if (tableColumnValueVO.getTableHeaderVO().getCode() == null) {
            throw new IllegalArgumentException("Null Table Header Code");
        }
        
        columnsList.add(tableColumnValueVO);
    }
    
    /**
     * Get Column
     * 
     * @return Added Column
     */
    public List getColumns() {
        return columnsList;
    }
    
}