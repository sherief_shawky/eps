/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 * 1.01  Ali Abdel-Aziz     05/01/2010  - Altering isValidSearch method to check 
 *                                        about the following properties:
 *                                        code, priority, statusDate, and id.
 * 1.02  Tariq Abu Amireh   06/01/2010  - remove isValidSearch method.                                         
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Procedure template value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureTemplateVO extends ValueObject {
    /*
     * Constants and class variables.
     */
     
    /** Procedure priority values. */
    public static final Integer PRIORITY_NORMAL = new Integer(1);
    public static final Integer PRIORITY_IMPORTANT = new Integer(2);
    public static final Integer PRIORITY_VERY_IMPORTANT = new Integer(3);

    /** Procedure template status values */
    public static final Integer TEMP_STATUS_NOT_ACTIVE = FALSE;
    public static final Integer TEMP_STATUS_ACTIVE = TRUE;
    
    /** Procedure template type values */
    public static final Integer TEMPLATE_TYPE_USER = new Integer(1);
    public static final Integer TEMPLATE_TYPE_SYSTEM = new Integer(2);

    /** Procedures Code */
    public static final Integer TEMPLATE_CODE_UPDATE_TRIAL_RESULT = new Integer(1996);
    public static final Integer TEMPLATE_CODE_APPLY_FOR_INSPECTION  = new Integer(9028);
    public static final Integer CODE_CONFISCATE_PLATE               = new Integer(35); 
    public static final Integer CODE_SELL_PLATE_PACKAGE             = new Integer(9041);
    public static final Integer TEMPLATE_CODE_NTA                   = new Integer(1990);
    public static final Integer TEMPLATE_CODE_PTA                   = new Integer(1991);
    public static final Integer TEMPLATE_CODE_TQM                   = new Integer(1992);
    public static final Integer TEMPLATE_CODE_DCD                   = new Integer(1993);
    public static final Integer CODE_ISSUE_TRADE_PLATE              = new Integer(9055);
    public static final Integer CODE_RENEW_TRADE_PLATE              = new Integer(9056);
    public static final Integer CODE_LOSS_DAMAGE_EXPORT             = new Integer(9060);
    public static final Integer CANCEL_CML_PERMIT                   = new Integer(9059);
    public static final Integer CODE_ISSUE_RNT_SYS_TRING_APPLICATION    = new Integer(9058); 
    public static final Integer CODE_CML_REPRINT    = new Integer(9057); 
    public static final Integer TEMPLATE_CODE_LIS = new Integer(2001); 
    public static final Integer TEMPLATE_CODE_CTA = new Integer(2005);
    public static final Integer TEMPLATE_CODE_PLANNING_AND_BUSINESS_ADMINISTRATION_SECTION = new Integer(2022);
    public static final Integer TEMPLATE_CODE_SYNC_NON_CTA_ORGANIZATION = new Integer(2024);
    public static final Integer TEMPLATE_CODE_SYNC_CTA_ORGANIZATION = new Integer(2028);
    public static final Integer TEMPLATE_CODE_SYNC_FRANCHISE_CTA_ORGANIZATION = new Integer(2029);
    public static final Integer TEMPLATE_CODE_SYNC_NOPERMIT_CTA_ORGANIZATION = new Integer(2030);

    public static final Integer CODE_VEHICLE_RENEWAL                = new Integer(9003);
    public static final Integer CODE_UODATE_REGISTRATION_INFO       = new Integer(9008);
    public static final Integer CODE_VEHICLE_OWNERSHIP_CERTIFICATE  = new Integer(9009);
    public static final Integer CODE_NON_OWNERSHIP_CERTIFICATE      = new Integer(9010);
    public static final Integer CODE_POSSESSION_CERTIFICATE         = new Integer(9011);
    public static final Integer CODE_EXPORT_CERTIFICATE             = new Integer(9012);
    public static final Integer CODE_TRANSFER_CERTIFICATE           = new Integer(9013);
    public static final Integer CODE_LOST_DAMAGED_POSSESSION_CERTIFICATE = new Integer(9015);
    public static final Integer CODE_INSURANCE_REFUND_CERTIFICATE   = new Integer(9016);
    public static final Integer CODE_LOST_DAMAGED_REGISTRATION_CARD = new Integer(9017);
    public static final Integer CODE_NEW_VEHICLE_REGISTRATION       = new Integer(9020);
    public static final Integer CODE_CHANGE_VEHICLE_PLATE_NUMBER    = new Integer(9023);
    public static final Integer CODE_VEHICLE_CLEARANCE_CERTIFICATE  = new Integer(9027);
    public static final Integer CODE_TOURISM_CERTIFICATE            = new Integer(9050);
    public static final Integer CODE_CHANGE_VEHICLE_OWNERSHIP       = new Integer(9066);
    public static final Integer CODE_ISSUE_SPECIAL_CERTIFICATES     = new Integer(9997);

    public static final Integer TEMPLATE_ID_NTA = new Integer(285);
    public static final Integer TEMPLATE_ID_PTA = new Integer(342);
    public static final Integer TEMPLATE_ID_TQM = new Integer(341);
    public static final Integer CODE_EXCEPTION_APPLICATION = new Integer(45);
    public static final Integer CODE_FRANSHISE_DEPARTMENT     = new Integer(1997);
    public static final Integer TEMPLATE_CODE_FINANCE = new Integer(2003);
    public static final Integer TEMPLATE_CODE_CONTRACTS_AND_PURCHASING_DEPARTMENT = new Integer(2004);
    public static final Integer TEMPLATE_CODE_SPL_SUPPORT = new Integer(2033);
    
    /*
     * Instance variables
     */
     
    /** Procedure template Arabic name. */
    private String templateNameAr;
    
    /** Procedure template English name. */
    private String templateNameEn;

    /** Procedure description. */
    private String description;
    
    /** Procedure status. */
    private Integer status;

    /** Procedure status Description. */
    private String statusDesc;
    
    /** Procedure status date. */
    private Date statusDate;

    /** Procedure priority. */
    private Integer priority;

    /** Procedure priority Description. */
    private String priorityDesc;

    /** Procedure version number. */
    private Integer versionNo;
    
    /** Procedure template type. */
    private Integer templateType;
    
    /** Procedure template code. */
    private Integer code;

    /** Procedure steps. */
    private List stepsList = new ArrayList();
    
    /** Procedure fields. */
    private List fieldsList = new ArrayList();
    

    /*
     * Accessors
     */
    /**
     * Set Procedure description.
     * 
     * @param description Procedure description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get Procedure description.
     * 
     * @return Procedure description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set Procedure status.
     * 
     * @param status Procedure status.
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Get Procedure status.
     * 
     * @return Procedure status.
     */
    public Integer getStatus() {
        return status;
    }
    
    /**
     * Check if procedure template is active.
     * 
     * @return true if procedure template is active.
     */
    public boolean isActiveTemplate() {
        return status != null && status.equals(TEMP_STATUS_ACTIVE);
    }

    /**
     * Set Procedure status date.
     * 
     * @param statusDate Procedure status date.
     */
    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * Get Procedure status date.
     * 
     * @return Procedure status date.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * Set Procedure priority.
     * 
     * @param priority Procedure priority.
     */
    public void setPriority(Integer priority) {
        if (priority == null || 
            priority.equals(PRIORITY_NORMAL) ||
            priority.equals(PRIORITY_IMPORTANT) || 
            priority.equals(PRIORITY_VERY_IMPORTANT)) {
            this.priority = priority;
            return;
        }
        
        throw new IllegalArgumentException("Invalid priority value: " + priority);
    }

    /**
     * Get Procedure priority.
     * 
     * @return Procedure priority.
     */
    public Integer getPriority() {
        return priority;
    }
    
    /**
     * Check if current procedure priority is normal.
     * 
     * @return true if current procedure priority is normal.
     */
    public boolean isPriorityNormal() {
        return priority != null && priority.equals(PRIORITY_NORMAL);
    }

    /**
     * Check if current procedure priority is important.
     * 
     * @return true if current procedure priority is important.
     */
    public boolean isPriorityImportant() {
        return priority != null && priority.equals(PRIORITY_IMPORTANT);
    }

    /**
     * Check if current procedure priority is very importamt.
     * 
     * @return true if current procedure priority is very importamt.
     */
    public boolean isPriorityVeryImportant() {
        return priority != null && priority.equals(PRIORITY_VERY_IMPORTANT);
    }

    /**
     * Set Procedure version number.
     * 
     * @param versionNo Procedure version number.
     */
    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }

    /**
     * Get Procedure version number.
     * 
     * @return Procedure version number.
     */
    public Integer getVersionNo() {
        return versionNo;
    }

    /**
     * Set Procedure steps.
     * 
     * @param stepsList Procedure steps.
     */
    public void setStepsList(List stepsList) {
        this.stepsList = stepsList;
    }

    /**
     * Get Procedure steps.
     * 
     * @return Procedure steps.
     */
    public List getStepsList() {
        return stepsList;
    }

    /**
     * Set Procedure template Arabic name.
     * 
     * @param templateNameAr Procedure template Arabic name.
     */
    public void setTemplateNameAr(String templateNameAr) {
        this.templateNameAr = templateNameAr;
    }

    /**
     * Get Procedure template Arabic name.
     * 
     * @return Procedure template Arabic name.
     */
    public String getTemplateNameAr() {
        return templateNameAr;
    }

    /**
     * Set Procedure template English name.
     * 
     * @param templateNameEn Procedure template English name.
     */
    public void setTemplateNameEn(String templateNameEn) {
        this.templateNameEn = templateNameEn;
    }

    /**
     * Get Procedure template English name.
     * 
     * @return Procedure template English name.
     */
    public String getTemplateNameEn() {
        return templateNameEn;
    }

    /**
     * set Status Desc
     * 
     * @param statusDesc
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * get Status Desc
     * 
     * @return statusDesc
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * Set priority Description
     * 
     * @param priorityDesc priority Description.
     */
    public void setPriorityDesc(String priorityDesc) {
        this.priorityDesc = priorityDesc;
    }

    /**
     * Get priority Description
     * 
     * @return priorityDesc priority Description
     */
    public String getPriorityDesc() {
        return priorityDesc;
    }

    /**
     * Set Procedure fields.
     * 
     * @param fieldsList Procedure fields.
     */
    public void setFieldsList(List fieldsList) {
        this.fieldsList = fieldsList;
    }

    /**
     * Get Procedure fields.
     * 
     * @return Procedure fields.
     */
    public List getFieldsList() {
        return fieldsList;
    }
    
    /**
     * Get begin template step.
     * 
     * @return begin template step.
     */
    public TemplateStepVO getBeginStep() {
        if (getStepsList() == null || getStepsList().size() == 0) {
            return null;
        }
        
        for (int i = 0; i < stepsList.size(); i++)  {
            TemplateStepVO stepVO = (TemplateStepVO) stepsList.get(i);
            if (stepVO.isProcedureStartStep()) {
                return stepVO;
            }
        }
        
        return null;
    }
    
    /**
     * Add new template step.
     * 
     * @param stepVO template step info.
     */
    public void addStep(TemplateStepVO stepVO) {
        if (stepVO == null) {
            throw new IllegalArgumentException("NULL step VO");
        }
        
        if (stepsList == null) {
            stepsList = new ArrayList();
        }
        
        stepsList.add(stepVO);
    }

    /**
     * Set Procedure template type.
     * 
     * @param templateType Procedure template type.
     */
    public void setTemplateType(Integer templateType) {
        if (templateType == null || 
            templateType.equals(TEMPLATE_TYPE_USER) ||
            templateType.equals(TEMPLATE_TYPE_SYSTEM)) {
            this.templateType = templateType;
            return;
        }
        
        throw new IllegalArgumentException("Invalid template type value: " + templateType);
    }

    /**
     * Get Procedure template type.
     * 
     * @return Procedure template type.
     */
    public Integer getTemplateType() {
        return templateType;
    }

    /**
     * Set template code.
     * 
     * @param code template code.
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Get template code.
     * 
     * @return template code.
     */
    public Integer getCode() {
        return code;
    }
    
    /**
     * Check if procedure template is user type.
     * 
     * @return true if procedure template is user type.
     */
    public boolean isUserTemplateType() {
        return templateType != null && templateType.equals(TEMPLATE_TYPE_USER);
    }
    
    /**
     * Check if procedure template is system type.
     * 
     * @return true if procedure template is system type.
     */
    public boolean isSystemTemplateType() {
        return templateType != null && templateType.equals(TEMPLATE_TYPE_SYSTEM);
    }
    
    /**
     * Check if procedure template Code is Apply For Inspection.
     * 
     * @return true if procedure template Code is Apply For Inspection.
     */
    public boolean isApplyForInspectionTemplateCode() {
        return getCode() != null && getCode().equals(TEMPLATE_CODE_APPLY_FOR_INSPECTION);
    }
    
    
    /**
     * Check if procedure template Code is Sell Plate Package
     * 
     * @return true if procedure template Code is Sell Plate Package.
     */
    public boolean isSellPlatePackageTemplateCode() {
        return getCode() != null && getCode().equals(CODE_SELL_PLATE_PACKAGE);
    }

}