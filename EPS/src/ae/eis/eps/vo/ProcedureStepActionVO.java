/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  07/05/2009  - File created.
 */

package ae.eis.eps.vo;

/**
 * Procedure step action value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepActionVO extends TemplateStepActionVO {
    /*
     * Constructors
     */

    /**
     * Default constructor
     */
    public ProcedureStepActionVO() {
       // Default constructor
    }

    /**
     * Construct and initialize new ProcedureStepActionVO.
     *
     * @param vo Security group value object.
     */
    public ProcedureStepActionVO(Integer type) {
       super(type);
    }
}