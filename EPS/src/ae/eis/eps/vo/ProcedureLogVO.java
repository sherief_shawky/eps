/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  07/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.util.vo.ValueObject;

/**
 * Procedure log value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureLogVO extends ValueObject {
    /*
     * Constants and class variables
     */
     
    /** Log action type values. */
    public static final Integer ACTION_APPROVE = new Integer(1);
    public static final Integer ACTION_REJECT = new Integer(2);
    public static final Integer ACTION_PUSH_BACK = new Integer(3);
    public static final Integer ACTION_CREATE_PROCEDURE = new Integer(4);
    public static final Integer ACTION_END_PROCEDURE = new Integer(5);
    public static final Integer ACTION_SEND_NOTIFICATION = new Integer(6);
    public static final Integer ACTION_CLAIM = new Integer(7);
    public static final Integer ACTION_UNCLAIM = new Integer(8);
    public static final Integer ACTION_RUN_BUSINESS_ACTION = new Integer(9);
    public static final Integer ACTION_UPDATE = new Integer(10);

    /*
     * Instance variables.
     */

    /** Log action type. */
    private Integer actionType;
    
    /** Log action type description. */
    private String actionTypeDesc;

    /** Employee info who takes the action. */
    private EmployeeVO employee;

    /** Step info. */
    private ProcedureStepVO step;

    /** User notes. */
    private String note;
    
    /*
     * Constructors
     */
    
    /**
     * Default constructor.
     */
    public ProcedureLogVO() {
        // Empty body
    }
    
    /*
     * Methods
     */

    /**
     * Set Employee info who takes the action.
     * 
     * @param employee Employee info who takes the action.
     */
    public void setEmployee(EmployeeVO employee) {
        this.employee = employee;
    }

    /**
     * Get Employee info who takes the action.
     * 
     * @return Employee info who takes the action.
     */
    public EmployeeVO getEmployee() {
        return employee;
    }

    /**
     * Set Step info.
     * 
     * @param step Step info.
     */
    public void setStep(ProcedureStepVO step) {
        this.step = step;
    }

    /**
     * Get Step info.
     * 
     * @return Step info.
     */
    public ProcedureStepVO getStep() {
        return step;
    }

    /**
     * Set User notes.
     * 
     * @param note User notes.
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * Get User notes.
     * 
     * @return User notes.
     */
    public String getNote() {
        return note;
    }

    /**
     * Set Log action type.
     * 
     * @param type Log action type.
     */
    public void setActionType(Integer type) {
        if (type == null ||
            type.equals(ACTION_APPROVE) ||
            type.equals(ACTION_CREATE_PROCEDURE) ||
            type.equals(ACTION_END_PROCEDURE) ||
            type.equals(ACTION_PUSH_BACK) ||
            type.equals(ACTION_REJECT) ||
            type.equals(ACTION_SEND_NOTIFICATION) ||
            type.equals(ACTION_CLAIM) ||
            type.equals(ACTION_UNCLAIM) ||
            type.equals(ACTION_RUN_BUSINESS_ACTION) ||
            type.equals(ACTION_UPDATE)) {
            this.actionType = type;
            return;
        }
        
        throw new IllegalArgumentException("Invalid action type: " + type);
    }

    /**
     * Get Log action type.
     * 
     * @return Log action type.
     */
    public Integer getActionType() {
        return actionType;
    }

    /**
     * Set Log action type description.
     * 
     * @param actionTypeDesc Log action type description.
     */
    public void setActionTypeDesc(String actionTypeDesc) {
        this.actionTypeDesc = actionTypeDesc;
    }

    /**
     * Get Log action type description.
     * 
     * @return actionTypeDesc Log action type description.
     */
    public String getActionTypeDesc() {
        return actionTypeDesc;
    }
}