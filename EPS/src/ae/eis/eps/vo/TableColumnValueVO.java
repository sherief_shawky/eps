/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00     Mena Emiel      23/02/2011  - File created and implemented.
 */

package ae.eis.eps.vo;

import ae.eis.common.vo.UserVO;
import ae.eis.util.vo.ValueObject;

/**
 * Table column value value object.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public class TableColumnValueVO extends ValueObject {
    
    /*
     * Fields
     */
     
    /** table header info. */
    private TableHeaderVO tableHeaderVO;
    
    /** Row Index. */
    private Integer rowIndex;
    
    /** Column Value. */
    private String value;
    
    /** User Which takes Action. */
    private UserVO userVO;
    
    /** Procedure Step. */
    private ProcedureStepVO procedureStepVO;
    
    
    /*
     * Constructors
     */
     
    /**
     * Default constructor.
     */
    public TableColumnValueVO() {
        // Empty body
    }

    /**
     * Setter for table header info
     * 
     * @param tableHeaderVO table header info
     */
    public void setTableHeaderVO(TableHeaderVO tableHeaderVO) {
        this.tableHeaderVO = tableHeaderVO;
    }

    /**
     * Getter for table header info
     * 
     * @return table header info
     */
    public TableHeaderVO getTableHeaderVO() {
        return tableHeaderVO;
    }

    /**
     * Setter for row index
     * 
     * @param rowIndex row index
     */
    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    /**
     * Setter for row index
     * 
     * @return row index
     */
    public Integer getRowIndex() {
        return rowIndex;
    }

    /**
     * Setter for column value
     * 
     * @param value column value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Getter for column value
     * 
     * @return column value
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for user info
     * 
     * @param userVO user info
     */
    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    /**
     * Getter for user info
     * 
     * @return user info
     */
    public UserVO getUserVO() {
        return userVO;
    }

    /**
     * Setter for procedure step info
     * 
     * @param procedureStepVO procedure step info
     */
    public void setProcedureStepVO(ProcedureStepVO procedureStepVO) {
        this.procedureStepVO = procedureStepVO;
    }

    /**
     * Getter for procedure step info
     * 
     * @return procedure step info
     */
    public ProcedureStepVO getProcedureStepVO() {
        return procedureStepVO;
    }
}