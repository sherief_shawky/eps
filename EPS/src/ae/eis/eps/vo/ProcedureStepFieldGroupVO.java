/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  23/11/2009  - File created.
 */

package ae.eis.eps.vo;

/**
 * Procedure step-fields group value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepFieldGroupVO extends TemplateStepFieldGroupVO {
    /*
     * Fields
     */
     
    /** Related procedure step info. */
    private ProcedureStepVO procedureStep;
    
    /*
     * Methods
     */

    /**
     * Set Related procedure step info.
     * 
     * @param procedureStep Related procedure step info.
     */
    public void setProcedureStep(ProcedureStepVO procedureStep) {
        this.procedureStep = procedureStep;
    }

    /**
     * Get Related procedure step info.
     * 
     * @return Related procedure step info.
     */
    public ProcedureStepVO getProcedureStep() {
        return procedureStep;
    }

    /**
     * Get procedure group fields.
     * 
     * @return procedure group fields.
     */
    public ProcedureStepFieldVO[] getProcedureStepFields() {
        return (ProcedureStepFieldVO[]) fieldsList.toArray(
            new ProcedureStepFieldVO[fieldsList.size()]);
    }
}