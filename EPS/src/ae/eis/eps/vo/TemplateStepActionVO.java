/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;
import java.util.HashSet;
import java.util.Set;

/**
 * Procedure template step action value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepActionVO extends ValueObject {
    /*
     * Constants and class variables
     */

    /** Action type values. */
    public static final Integer TYPE_APPROVE = new Integer(1);
    public static final Integer TYPE_REJECT = new Integer(2);
    public static final Integer TYPE_PUSH_BACK = new Integer(3);
    public static final Integer TYPE_VIEW_STEP_ATTACHMENTS = new Integer(4);
    public static final Integer TYPE_VIEW_REQUESTER_ATTACHMENTS = new Integer(5);
    public static final Integer TYPE_VIEW_USERS_ATTACHMENTS = new Integer(6);
    public static final Integer TYPE_DELETE_REQUESTER_ATTACHMENTS = new Integer(7);
    public static final Integer TYPE_DELETE_OTHER_STEPS_ATTACHMENTS = new Integer(8);
    public static final Integer TYPE_UPLOAD_ATTACHMENTS = new Integer(9);

    private static final Set ACTION_TYPES_SET = new HashSet();
    static {
        ACTION_TYPES_SET.add(TYPE_APPROVE);
        ACTION_TYPES_SET.add(TYPE_REJECT);
        ACTION_TYPES_SET.add(TYPE_PUSH_BACK);
        ACTION_TYPES_SET.add(TYPE_VIEW_STEP_ATTACHMENTS);
        ACTION_TYPES_SET.add(TYPE_VIEW_REQUESTER_ATTACHMENTS);
        ACTION_TYPES_SET.add(TYPE_VIEW_USERS_ATTACHMENTS);
        ACTION_TYPES_SET.add(TYPE_DELETE_REQUESTER_ATTACHMENTS);
        ACTION_TYPES_SET.add(TYPE_DELETE_OTHER_STEPS_ATTACHMENTS);
        ACTION_TYPES_SET.add(TYPE_UPLOAD_ATTACHMENTS);
    }

    /*
     * Instance variables.
     */

    /** Action type. */
    private Integer actionType;

    /** Related template step. */
    private TemplateStepVO templateStep;

    /*
     * Constructors
     */

    /**
     * Default constructor
     */
    public TemplateStepActionVO() {
       // Default constructor
    }

    /**
     * Construct and initialize new TemplateStepActionVO.
     *
     * @param vo Security group value object.
     */
    public TemplateStepActionVO(Integer type) {
       setActionType(type);
    }

    /*
     * Methods
     */

    /**
     * Set Action type.
     *
     * @param type Action type.
     */
    public void setActionType(Integer type) {
        if (! isValidActionType(type)) {
            throw new IllegalArgumentException("Invalid action type: " + type);
        }

        this.actionType = type;
    }

    /**
     * Get Action type.
     *
     * @return Action type.
     */
    public Integer getActionType() {
        return actionType;
    }

    /**
     * Check if this action-type value is valid.
     *
     * @param type Action-Type value to be tested.
     * @return true if this action-type value is valid.
     */
    public static boolean isValidActionType(Integer type) {
        if (type == null) {
            return true;
        }

        return ACTION_TYPES_SET.contains(type);
    }

    /**
     * Set Related template step.
     *
     * @param templateStep Related template step.
     */
    public void setTemplateStep(TemplateStepVO templateStep) {
        this.templateStep = templateStep;
    }

    /**
     * Get Related template step.
     *
     * @return Related template step.
     */
    public TemplateStepVO getTemplateStep() {
        return templateStep;
    }
    
    /**
     * Check if this action is an approve action.
     * 
     * @return true if this action is an approve action.
     */
    public boolean isApproveAction() {
        return actionType != null && actionType.equals(TYPE_APPROVE);
    }

    /**
     * Check if this action is an reject action.
     * 
     * @return true if this action is an reject action.
     */
    public boolean isRejectAction() {
        return actionType != null && actionType.equals(TYPE_REJECT);
    }

    /**
     * Check if this action is an push-back action.
     * 
     * @return true if this action is an push-back action.
     */
    public boolean isPushBackAction() {
        return actionType != null && actionType.equals(TYPE_PUSH_BACK);
    }

    /**
     * Check if this action is an "view step attachments" action.
     * 
     * @return true if this action is an "view step attachments" action.
     */
    public boolean isViewStepAttachmentsAction() {
        return actionType != null && actionType.equals(TYPE_VIEW_STEP_ATTACHMENTS);
    }

    /**
     * Check if this action is an "view requester attachments" action.
     * 
     * @return true if this action is an "view requester attachments" action.
     */
    public boolean isViewRequesterAttachmentsAction() {
        return actionType != null && actionType.equals(TYPE_VIEW_REQUESTER_ATTACHMENTS);
    }

    /**
     * Check if this action is an "view users attachments" action.
     * 
     * @return true if this action is an "view users attachments" action.
     */
    public boolean isViewUsersAttachmentsAction() {
        return actionType != null && actionType.equals(TYPE_VIEW_USERS_ATTACHMENTS);
    }

    /**
     * Check if this action is an "delete requester attachments" action.
     * 
     * @return true if this action is an "delete requester attachments" action.
     */
    public boolean isDeleteRequesterAttachmentsAction() {
        return actionType != null && 
                actionType.equals(TYPE_DELETE_REQUESTER_ATTACHMENTS);
    }

    /**
     * Check if this action is an "delete other steps attachments" action.
     * 
     * @return true if this action is an "delete other steps attachments" action.
     */
    public boolean isDeleteOtherStepsAttachmentsAction() {
        return actionType != null && 
               actionType.equals(TYPE_DELETE_OTHER_STEPS_ATTACHMENTS);
    }

    /**
     * Check if this action is an "upload attachments" action.
     * 
     * @return true if this action is an "upload attachments" action.
     */
    public boolean isUploadAttachmentsAction() {
        return actionType != null && actionType.equals(TYPE_UPLOAD_ATTACHMENTS);
    }
}