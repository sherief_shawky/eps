/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer                 Date        Comments
 * ----- -----------------         ----------  ----------------------------------------
 * 1.00  Sami Abudayeh             15/02/2017  - File created.
 */
package ae.eis.eps.vo;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.TrafficFileVO;
import ae.eis.common.vo.UserVO;
import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.vo.ValueObject;

import java.util.Date;

/**
 * @author: Sami Abudayeh
 * Esp Verify Search VO
 */ 
public class EpsVerifySearchVO extends ValueObject {
    
    /** Transaction VO */
    private TransactionVO trsVO;
    
    /** Employee VO */
    private EmployeeVO employeeVO;
    
    /** Procedure Template VO */
    private ProcedureTemplateVO procedureTemplateVO;
    
    /** From Date */
    private Date fromDate;
    
    /** To Date */
    private Date toDate;

    /** Request Name */
    private String requestName;
    
    
    /** Traffic File VO */
    private TrafficFileVO trafficFileVO;
    
    /** User VO */
    private UserVO userVO;
    
    
    /** With Out Eid */
    private boolean withoutEid;
    
    /*
     * Constructors
     */

    /**
     * Default constructor.
     */
    public EpsVerifySearchVO() {
        // Empty body
    }

    /**
     * Setter for Trs VO
     *
     * @param trsVO Transaction VO
     */
    public void setTrsVO(TransactionVO trsVO) {
        this.trsVO = trsVO;
    }

    /**
     * Getter for Trs VO
     *
     * @return trsVO Transaction VO
     */
    public TransactionVO getTrsVO() {
        return trsVO;
    }

    /**
     * Setter for Employee VO
     *
     * @param employeeVO Employee VO
     */
    public void setEmployeeVO(EmployeeVO employeeVO) {
        this.employeeVO = employeeVO;
    }
    
    /**
     * Getter for Employee VO
     *
     * @return employeeVO Employee VO
     */
    public EmployeeVO getEmployeeVO() {
        return employeeVO;
    }

    /**
     * Setter for Peocedure Template VO
     *
     * @param procedureTemplateVO Peocedure Template VO
     */
    public void setProcedureTemplateVO(ProcedureTemplateVO procedureTemplateVO) {
        this.procedureTemplateVO = procedureTemplateVO;
    }

    /**
     * Getter for Peocedure Template VO
     *
     * @return procedureTemplateVO Peocedure Template VO
     */
    public ProcedureTemplateVO getProcedureTemplateVO() {
        return procedureTemplateVO;
    }

    /**
     * Setter for From Date
     *
     * @param fromDate From Date
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * Getter for From Date
     *
     * @return fromDate From Date
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * Setter for To Date
     *
     * @param toDate To Date
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * Getter for To Date
     *
     * @return toDate To Date
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * Setter for Request Name
     *
     * @param requestName Request Name
     */
    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    /**
     * Getter for Request Name
     *
     * @return requestName Request Name
     */
    public String getRequestName() {
        return requestName;
    }

   
    /**
     * Setter for Traffic File VO
     *
     * @param trafficFileVO Traffic File VO
     */
    public void setTrafficFileVO(TrafficFileVO trafficFileVO) {
        this.trafficFileVO = trafficFileVO;
    }

    /**
     * Getter for Traffic File VO
     *
     * @return trafficFileVO Traffic File VO
     */
    public TrafficFileVO getTrafficFileVO() {
        return trafficFileVO;
    }

    /**
     * Setter for User VO
     *
     * @param userVO User VO
     */
    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    /**
     * Getter for User VO
     *
     * @return userVO User VO
     */
    public UserVO getUserVO() {
        return userVO;
    }

   
    /**
     * Setter for With Out Eid
     *
     * @param withoutEid With Out Eid
     */
    public void setWithoutEid(boolean withoutEid) {
        this.withoutEid = withoutEid;
    }

    /**
     * Getter for With Out Eid
     *
     * @return withoutEid With Out Eid
     */
    public boolean isWithoutEid() {
        return withoutEid;
    }
}
