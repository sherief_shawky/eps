/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 * 
 * 1.01  Tariq Abu Amireh   17/06/2009  - Re-Name File from Step Group
 *                                                       to Step User
 * 
 */

package ae.eis.eps.vo;


import ae.eis.common.vo.UserVO;
import ae.eis.util.vo.ValueObject;

/**
 * Procedure template step group value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepUserVO extends ValueObject {
    /*
     * Instance variables.
     */

    /** Security group. */
    private UserVO user;

    /** Related template step. */
    private TemplateStepVO step;

    /*
     * Constructors
     */

    /**
     * Default constructor
     */
    public TemplateStepUserVO() {
        // Default constructor
    }

    /**
     * Construct and initialize new TemplateStepGroupVO.
     *
     * @param vo Security group value object.
     */
    public TemplateStepUserVO(UserVO vo) {
        setUser(vo);
    }

    /*
     * Methods
     */

    /**
     * Set User
     *
     * @param securityGroup Security group
     */
    public void setUser(UserVO user) {
        this.user = user;
    }

    /**
     * Get User
     *
     * @return User
     */
    public UserVO getUser() {
        return user;
    }

    /**
     * Set Related template step.
     *
     * @param step Related template step.
     */
    public void setStep(TemplateStepVO step) {
        this.step = step;
    }

    /**
     * Get Related template step.
     *
     * @return Related template step.
     */
    public TemplateStepVO getStep() {
        return step;
    }
}