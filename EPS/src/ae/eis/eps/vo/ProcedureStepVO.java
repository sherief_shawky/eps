/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.common.vo.UserVO;
import java.util.Date;

/**
 * Procedure step value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepVO extends TemplateStepVO {
    /*
     * Instance variables.
     */

    /** Task assignment date. */
    private Date assignDate;

    /** Procedure value object. */
    private ProcedureVO procedure;
    
    /** User who claims the step to work on. */
    private UserVO claimedBy;
    
    /*
     * Methods
     */

    /**
     * Set Task assignment date.
     *
     * @param assignDate Task assignment date.
     */
    public void setAssignDate(Date assignDate) {
        this.assignDate = assignDate;
    }

    /**
     * Get Task assignment date.
     *
     * @return Task assignment date.
     */
    public Date getAssignDate() {
        return assignDate;
    }

    /**
     * Set Procedure value object.
     *
     * @param procedure Procedure value object.
     */
    public void setProcedure(ProcedureVO procedure) {
        this.procedure = procedure;
    }

    /**
     * Get Procedure value object.
     *
     * @return Procedure value object.
     */
    public ProcedureVO getProcedure() {
        return procedure;
    }

    /**
     * Set User who claims the step to work on.
     * 
     * @param claimedBy User who claims the step to work on.
     */
    public void setClaimedBy(UserVO claimedBy) {
        this.claimedBy = claimedBy;
    }

    /**
     * Get User who claims the step to work on.
     * 
     * @return User who claims the step to work on.
     */
    public UserVO getClaimedBy() {
        return claimedBy;
    }
    
    /**
     * Check if this step is claimed by a user.
     * 
     * @return if this step is claimed by a user.
     */
    public boolean isClaimed() {
        return getClaimedBy() != null && getClaimedBy().getId() != null;
    }

    /**
     * Check if this step is claimed by this user.
     * 
     * @return if this step is claimed by this user.
     */
    public boolean isClaimedBy(Long userId) {
        if (userId == null) {
            throw new IllegalArgumentException("NULL user ID parameter");
        }
    
        return getClaimedBy() != null && 
               getClaimedBy().getId() != null &&
               getClaimedBy().getId().equals(userId);
    }
}