/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   30/12/2009  - File created.
 */
 
package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Template Table Header Value Object
 * 
 * @version 1.0
 * @author Tariq Abu Amireh
 */
public class TemplateTableHeaderVO extends ValueObject {
    
    /*
     * constants
     */
    /** Column Types */
    public static final Long COLUMN_TYPE_TEXT     = new Long(1);
    public static final Long COLUMN_TYPE_NUMBER   = new Long(2);
    public static final Long COLUMN_TYPE_DATE     = new Long(3);
    
    /** Column Mandatory */
    public static final Integer COLUMN_MANDATORY     = new Integer(2);
    public static final Integer COLUMN_OPTIONAL   = new Integer(1);
    
    /*
     * Instance Variable
     */
    /** Arabic Name */
    private String nameAr;
    
    /** English Name */
    private String nameEn;
    
    /** Order Number */
    private Long orderNo;
    
    /** Column Type */
    private Long columnType;
    
    /** Column Type Description */
    private String columnTypeDesc;    
    
    /** Lable Width */
    private Long lableWidth;
    
    /** Max Size */
    private Long maxSize;
    
    /** Is Hidden */
    private Integer isHidden;
    
    /** Template Field Value Object */
    private TemplateFieldVO templateField;
    
    /** Header Code */
    private Integer code;
    
    /** Is Mandatory */
    private Integer isMandatory;
    
    
    /**
     * Default Constructor
     */
    public TemplateTableHeaderVO() {
    }
    
    /**
     * Template Table Header Value Object
     * 
     * @param vo Template Table Header initial value
     */
    public TemplateTableHeaderVO(TemplateTableHeaderVO vo) {
        if( vo == null ) {
            return ;
        }
        
        setId(vo.getId());
        setNameAr(vo.getNameAr());
        setNameEn(vo.getNameEn());
        setOrderNo(vo.getOrderNo());
        setColumnType(vo.getColumnType());
        setLableWidth(vo.getLableWidth());
        setMaxSize(vo.getMaxSize());
        setIsHidden(vo.getIsHidden());
        setColumnTypeDesc(null);
    }
    
    /**
     * Check if the column type is text.
     * 
     * @return true if the column type is text.
     */
    public boolean isTextColumn() {
        return columnType != null && columnType.equals(COLUMN_TYPE_TEXT);
    }

    /**
     * Check if the column type is number.
     * 
     * @return true if the column type is number.
     */
    public boolean isNumberColumn() {
        return columnType != null && columnType.equals(COLUMN_TYPE_NUMBER);
    }
    
    /**
     * Check if the column type is date.
     * 
     * @return true if the column type is date.
     */
    public boolean isDateColumn() {
        return columnType != null && columnType.equals(COLUMN_TYPE_DATE);
    }

    /*
     * Accessors
     */
    /**
     * Set Arabic Name
     * 
     * @param nameAr Arabic Name
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * Get Arabic Name
     * 
     * @return nameAr Arabic Name
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * Set English Name
     * 
     * @param nameEn English Name
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * Get English Name
     * 
     * @return nameEn English Name
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * Set Order Number
     * 
     * @param orderNo Order Number
     */
    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * Get Order Number
     * 
     * @return orderNo Order Number
     */
    public Long getOrderNo() {
        return orderNo;
    }

    /**
     * Set Column Type
     * 
     * @param columnType Column Type
     */
    public void setColumnType(Long columnType) {
        if (columnType == null                ||
            columnType.equals(COLUMN_TYPE_TEXT)      ||
            columnType.equals(COLUMN_TYPE_NUMBER)    ||
            columnType.equals(COLUMN_TYPE_DATE)) {
            this.columnType = columnType;
            return;
        }
        
        throw new IllegalArgumentException("Invalid column type: " + columnType);
        
    }

    /**
     * Get Column Type
     * 
     * @return columnType Column Type
     */
    public Long getColumnType() {
        return columnType;
    }

    /**
     * Set Lable Width
     * 
     * @param lableWidth Lable Width
     */
    public void setLableWidth(Long lableWidth) {
        this.lableWidth = lableWidth;
    }

    /**
     * Get Lable Width
     * 
     * @return lableWidth Lable Width
     */
    public Long getLableWidth() {
        return lableWidth;
    }

    /**
     * Set Max Size
     * 
     * @param maxSize Max Size
     */
    public void setMaxSize(Long maxSize) {
        this.maxSize = maxSize;
    }

    /**
     * Get Max Size
     * 
     * @return maxSize Max Size
     */
    public Long getMaxSize() {
        return maxSize;
    }

    /**
     * Set Is Hidden
     * 
     * @param isHidden Is Hidden
     */
    public void setIsHidden(Integer isHidden) {
        this.isHidden = isHidden;
    }

    /**
     * Get Is Hidden
     * 
     * @return isHidden Is Hidden
     */
    public Integer getIsHidden() {
        return isHidden;
    }
    
    /**
     * Get Is Hidden description
     * 
     * @return isHidden arabic description 
     */
    public String getIsHiddenDescAr() {
        return getBooleanDescAr(getIsHidden());
    }    
    
    /**
     * Get Is Mandatory description
     * 
     * @return isMandatory arabic description 
     */
    public String getIsMandatoryDescAr() {
        return getBooleanDescAr(getIsMandatory());
    }    

    /**
     * set Template Field
     * 
     * @param templateField Template Field
     */
    public void setTemplateField(TemplateFieldVO templateField) {
        this.templateField = templateField;
    }

    /**
     * Get Template Field
     * 
     * @return templateField Template Field
     */
    public TemplateFieldVO getTemplateField() {
        return templateField;
    }

    /**
     * Set Column Type Desc
     * 
     * @param columnTypeDesc Column Type Desc
     */
    public void setColumnTypeDesc(String columnTypeDesc) {
        this.columnTypeDesc = columnTypeDesc;
    }

    /**
     * Get Column Type Desc
     * 
     * @return columnTypeDesc Column Type Desc
     */
    public String getColumnTypeDesc() {
        return columnTypeDesc;
    }

    /**
     * Setter for code
     * 
     * @param code header code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Getter for code
     * 
     * @return return header code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Setter for is column mandatory
     * 
     * @param isMandatory is column mandatory
     */
    public void setIsMandatory(Integer isMandatory) {
        this.isMandatory = isMandatory;
    }

    /**
     * Getter for is column mandatory
     * 
     * @return True if is column mandatory
     */
    public Integer getIsMandatory() {
        return isMandatory;
    }
    
}
