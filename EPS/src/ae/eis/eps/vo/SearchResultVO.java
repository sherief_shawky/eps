/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Search result value object.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public class SearchResultVO extends ValueObject {
    
    /*
     * Fields
     */
    
    /** Related template field. */
    private TemplateFieldVO templateField;
    
    /** Search field order number. */
    private Integer orderNo;
    
    /** Column Width */
    private Integer columnWidth;
    
    /*
     * Methods
     */

    /**
     * Set Related template field.
     * 
     * @param templateField Related template field.
     */
    public void setTemplateField(TemplateFieldVO templateField) {
        this.templateField = templateField;
    }

    /**
     * Get Related template field.
     * 
     * @return Related template field.
     */
    public TemplateFieldVO getTemplateField() {
        return templateField;
    }

    /**
     * Set Search field order number.
     * 
     * @param orderNo Search field order number.
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * Get Search field order number.
     * 
     * @return Search field order number.
     */
    public Integer getOrderNo() {
        return orderNo;
    }

    /**
     * Set Column Width
     * 
     * @param columnWidth Column Width
     */
    public void setColumnWidth(Integer columnWidth) {
        this.columnWidth = columnWidth;
    }

    /**
     * Get Column Width
     * 
     * @return columnWidth Column Width
     */
    public Integer getColumnWidth() {
        return columnWidth;
    }
}