/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 * 
 * 1.10  Alaa Salem         07/01/2010  - Adding transaction Field & Accessors.
 * 
 */

package ae.eis.eps.vo;

import ae.eis.common.vo.TrafficFileVO;
import ae.eis.common.vo.UserVO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Electronic procedure value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
@XmlRootElement
public class ProcedureVO extends ProcedureTemplateVO {
    /*
     * Constants valriables.
     */
     
    /** Procedure status values. */
    public static final Integer STATUS_UNDER_PROCESSING = new Integer(1);
    public static final Integer STATUS_COMPLETED = new Integer(2);
    public static final Integer STATUS_REJECTED = new Integer(3);
    public static final Integer STATUS_CANCELED = new Integer(4);
    public static final Integer STATUS_PENDING = new Integer(5);
    
    // final eps step.
    public static final Integer FINAL_EPS_STEP = new Integer(999);

    /*
     * Instance variables
     */

    /** Requester email. */
    private String requesterEmail;
    
    /** Set Requester mobile. */
    private String requesterMobile;
    
    /** Last user action date. */
    private Date lastActionDate;
    
    /** Last user note. */
    private String lastNote;
    
    /** Last user action. */
    private Integer lastAction;
    
    /** Last user action description. */
    private String lastActionDesc;

    /** Procedure template. */
    private ProcedureTemplateVO template;
    
    /** Requester user info. */
    private UserVO requester;
    private Long trsId;
    /** Procedure attachments list. */
    private List attachmentsList = new ArrayList();

    /** Active step info. */
    private ProcedureStepVO activeStep;

    /** Last human task step info. */
    private ProcedureStepVO lastHumanTaskStep;
    
    /** Requester attachments count. */
    private Integer requesterAttachmentsCount;

    /** Users attachments count. */
    private Integer usersAttachmentsCount;
    
  
    /** Traffic file Value Object. */
    private TrafficFileVO trafficFile;
    
    
    /**  Language Code */
    private String languageCode;
    
    
    /**
     * Used to save user notes when user clicks update button without taking 
     * action.
     */
    private String activeNote;

    /** User assigned to the task. */
    private UserVO assignedTo;

    /**  code  */
    private Integer code;

    /**
     * hold company performance id if TF_STP_TRS_ATTACHMENTS.ATTACHMENT_TYPE: 7  
     */
    private String  performanceCertificateAttachmentId = null;
    
    /**
     * hold company contract id if TF_STP_TRS_ATTACHMENTS.ATTACHMENT_TYPE: 8  
     */
    private String companyContractAttachmentId = null;
    
    /** Requestor Arabic Name */
    private String requestorNameAr;
    
    /** Requestor English Name */
    private String requestorNameEn;
    
    /** status Desc Ar */
    private String statusDescAr;
    
    /** status Desc En */
    private String statusDescEn;
    
    private Long consumesTimeInMinutes;
    
    /** User Assigned To*/
    private String userAssignedTo;
    
    /** Step User*/
    private String stepUser;
    
   
    /** Current Date Time */
    private Date currDateTime;
    
    /*
     * Methods
     */

    /**
     * Set Requester email.
     * 
     * @param requesterEmail Requester email.
     */
    public void setRequesterEmail(String requesterEmail) {
        this.requesterEmail = requesterEmail;
    }

    /**
     * Get Requester email.
     * 
     * @return Requester email.
     */
    public String getRequesterEmail() {
        return requesterEmail;
    }

    /**
     * Set Set Requester mobile.
     * 
     * @param requesterMobile Set Requester mobile.
     */
    public void setRequesterMobile(String requesterMobile) {
        this.requesterMobile = requesterMobile;
    }

    /**
     * Get Set Requester mobile.
     * 
     * @return Set Requester mobile.
     */
    public String getRequesterMobile() {
        return requesterMobile;
    }

    /**
     * Set Last user action date.
     * 
     * @param lastActionDate Last user action date.
     */
    public void setLastActionDate(Date lastActionDate) {
        this.lastActionDate = lastActionDate;
    }

    /**
     * Get Last user action date.
     * 
     * @return Last user action date.
     */
    public Date getLastActionDate() {
        return lastActionDate;
    }

    /**
     * Set Last user note.
     * 
     * @param lastNote Last user note.
     */
    public void setLastNote(String lastNote) {
        this.lastNote = lastNote;
    }

    /**
     * Get Last user note.
     * 
     * @return Last user note.
     */
    public String getLastNote() {
        return lastNote;
    }

    /**
     * Set Procedure template.
     * 
     * @param template Procedure template.
     */
    public void setTemplate(ProcedureTemplateVO template) {
        this.template = template;
    }

    /**
     * Get Procedure template.
     * 
     * @return Procedure template.
     */
    public ProcedureTemplateVO getTemplate() {
        return template;
    }

    /**
     * Set Requester user info.
     * 
     * @param requester Requester user info.
     */
    public void setRequester(UserVO requester) {
        this.requester = requester;
    }

    /**
     * Get Requester user info.
     * 
     * @return Requester user info.
     */
    public UserVO getRequester() {
        return requester;
    }

    /**
     * Check if procedure status is under processing.
     * 
     * @return if procedure status is under processing.
     */
    public boolean isUnderProcessing() {
        return getStatus() != null && getStatus().equals(STATUS_UNDER_PROCESSING);
    }

    /**
     * Check if procedure status is completed.
     * 
     * @return if procedure status is completed.
     */
    public boolean isCompleted() {
        return getStatus() != null && getStatus().equals(STATUS_COMPLETED);
    }

    /**
     * Check if procedure status is rejected.
     * 
     * @return if procedure status is rejected.
     */
    public boolean isRejected() {
        return getStatus() != null && getStatus().equals(STATUS_REJECTED);
    }

    /**
     * Check if procedure status is Canceled.
     * 
     * @return if procedure status is Canceled.
     */
    public boolean isCanceled() {
        return getStatus() != null && getStatus().equals(STATUS_CANCELED);
    }

    /**
     * Check if procedure status is Pending.
     * 
     * @return if procedure status is Pending.
     */
    public boolean isPending() {
        return getStatus() != null && getStatus().equals(STATUS_PENDING);
    }

    /**
     * Set Procedure attachments list.
     * 
     * @param attachmentsList Procedure attachments list.
     */
    public void setAttachmentsList(List attachmentsList) {
        this.attachmentsList = attachmentsList;
    }

    /**
     * Get Procedure attachments list.
     * 
     * @return Procedure attachments list.
     */
    public List getAttachmentsList() {
        return attachmentsList;
    }

    /**
     * Set Last user action.
     * 
     * @param lastAction Last user action.
     */
    public void setLastAction(Integer lastAction) {
        this.lastAction = lastAction;
    }

    /**
     * Get Last user action.
     * 
     * @return Last user action.
     */
    public Integer getLastAction() {
        return lastAction;
    }

    /**
     * Set Active step info.
     * 
     * @param activeStep Active step info.
     */
    public void setActiveStep(ProcedureStepVO activeStep) {
        this.activeStep = activeStep;
    }

    /**
     * Get Active step info.
     * 
     * @return Active step info.
     */
    public ProcedureStepVO getActiveStep() {
        return activeStep;
    }

    /**
     * Set Last human task step info.
     * 
     * @param lastHumanTaskStep Last human task step info.
     */
    public void setLastHumanTaskStep(ProcedureStepVO lastHumanTaskStep) {
        this.lastHumanTaskStep = lastHumanTaskStep;
    }

    /**
     * Get Last human task step info.
     * 
     * @return Last human task step info.
     */
    public ProcedureStepVO getLastHumanTaskStep() {
        return lastHumanTaskStep;
    }

    /**
     * Set Last user action description.
     * 
     * @param lastActionDesc Last user action description.
     */
    public void setLastActionDesc(String lastActionDesc) {
        this.lastActionDesc = lastActionDesc;
    }

    /**
     * Get Last user action description.
     * 
     * @return Last user action description.
     */
    public String getLastActionDesc() {
        return lastActionDesc;
    }

    /**
     * Set Requester attachments count.
     * 
     * @param count Requester attachments count.
     */
    public void setRequesterAttachmentsCount(Integer count) {
        this.requesterAttachmentsCount = count;
    }

    /**
     * Get Requester attachments count.
     * 
     * @return Requester attachments count.
     */
    public Integer getRequesterAttachmentsCount() {
        return requesterAttachmentsCount;
    }
    
    /**
     * Check if this procedure has requester attachments.
     * 
     * @return true if this procedure has requester attachments.
     */
    public boolean hasRequesterAttachments() {
        return requesterAttachmentsCount != null &&
               requesterAttachmentsCount.intValue() > 0;
    }

    /**
     * Get Total attachments count.
     * 
     * @return Total attachments count.
     */
    public Integer getTotalAttachmentsCount() {
        int total = 0;
        
        if (requesterAttachmentsCount != null && 
            requesterAttachmentsCount.intValue() > 0) {
            total += requesterAttachmentsCount.intValue();
        }

        if (usersAttachmentsCount != null && 
            usersAttachmentsCount.intValue() > 0) {
            total += usersAttachmentsCount.intValue();
        }

        return new Integer(total);
    }

    /**
     * Check if this procedure has attachments.
     * 
     * @return true if this procedure has attachments.
     */
    public boolean hasAttachments() {
        return getTotalAttachmentsCount().intValue() > 0;
    }

    /**
     * Set Users attachments count.
     * 
     * @param usersAttachmentsCount Users attachments count.
     */
    public void setUsersAttachmentsCount(Integer usersAttachmentsCount) {
        this.usersAttachmentsCount = usersAttachmentsCount;
    }

    /**
     * Get Users attachments count.
     * 
     * @return Users attachments count.
     */
    public Integer getUsersAttachmentsCount() {
        return usersAttachmentsCount;
    }

    /**
     * Check if this procedure has users attachments.
     * 
     * @return true if this procedure has users attachments.
     */
    public boolean hasUsersAttachments() {
        return usersAttachmentsCount != null &&
               usersAttachmentsCount.intValue() > 0;
    }

    /**
     * Set active user notes.
     * 
     * @param activeNote Active user notes used to save user notes when user 
     *                   clicks update button  without taking action.
     */
    public void setActiveNote(String activeNote) {
        this.activeNote = activeNote;
    }

    /**
     * Get active user notes used to save user notes when user clicks update 
     * button without taking action.
     * 
     * @return active user notes..
     */
    public String getActiveNote() {
        return activeNote;
    }

    /**
     * Set User assigned to the task.
     * 
     * @param assignedTo User assigned to the task.
     */
    public void setAssignedTo(UserVO assignedTo) {
        this.assignedTo = assignedTo;
    }

    /**
     * Get User assigned to the task.
     * 
     * @return User assigned to the task.
     */
    public UserVO getAssignedTo() {
        return assignedTo;
    }

    /**
     * Get User ID assigned to the task.
     * 
     * @return User ID assigned to the task.
     */
    public Long getAssignedToUserId() {
        return assignedTo == null ? null : assignedTo.getId();
    }

    
    /**
     * Set Traffic file Value Object
     * 
     * @param trafficFile Traffic file Value Object
     */
    public void setTrafficFile(TrafficFileVO trafficFile) {
        this.trafficFile = trafficFile;
    }

    /**
     * Get Traffic file Value Object
     * 
     * @return Traffic file Value Object
     */
    public TrafficFileVO getTrafficFile() {
        return trafficFile;
    }

    
    /**
     * Set code.
     * 
     * @param code code.
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Get code.
     * 
     * @return code code.
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Setter for Language Code
     * 
     * @param languageCode : Language Code
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Getter for Language Code
     * 
     * @return Language Code
     */
    public String getLanguageCode() {
        return languageCode;
    }


    
     /**
      * setter for company performance id
      * 
      * @param performanceCertificateAttachmentId
     */
     public void setPerformanceCertificateAttachmentId(String  performanceCertificateAttachmentId) {
         this.performanceCertificateAttachmentId= performanceCertificateAttachmentId;
     }
    
    
    
     /**
      * getter for company performance id
      */
     public String getPerformanceCertificateAttachmentId() {
         return this.performanceCertificateAttachmentId;
     }
     
      /**
      * setter for company contract id
      * 
      * @param companyContractAttachmentId
     */
     public void setCompanyContractAttachmentId(String  companyContractAttachmentId) {
         this.companyContractAttachmentId= companyContractAttachmentId;
     }
    
     /**
      * getter for company contract id
     */
     public String getCompanyContractAttachmentId() {
         return this.companyContractAttachmentId;
     }

    /**
     * Set Requestor Arabic Name
     * 
     * @param requestorNameAr Requestor Arabic Name
     */
    public void setRequestorNameAr(String requestorNameAr) {
        this.requestorNameAr = requestorNameAr;
    }

    /**
     * Get Requestor Arabic Name
     * 
     * @return requestorNameAr Requestor Arabic Name
     */
    public String getRequestorNameAr() {
        return requestorNameAr;
    }

    /**
     * Set Requestor English Name
     * 
     * @param requestorNameEn Requestor English Name
     */
    public void setRequestorNameEn(String requestorNameEn) {
        this.requestorNameEn = requestorNameEn;
    }

    /**
     * Get Requestor English Name
     * 
     * @return requestorNameEn Requestor English Name
     */
    public String getRequestorNameEn() {
        return requestorNameEn;
    }
    
    /**
     * Set status Desc Ar
     * 
     * @param statusDescAr : status Desc Ar.
     */
    public void setStatusDescAr(String statusDescAr) {
        this.statusDescAr = statusDescAr;
    }

    /**
     * Get statusDescAr
     * 
     * @return statusDescAr : status Desc Ar.
     */
    public String getStatusDescAr() {
        return statusDescAr;
    }

    /**
     * Set statusDescEn
     * 
     * @param statusDescEn : status Desc En.
     */
    public void setStatusDescEn(String statusDescEn) {
        this.statusDescEn = statusDescEn;
    }

    /**
     * Get statusDescEn
     * 
     * @return statusDescEn : status Desc En.
     */
    public String getStatusDescEn() {
        return statusDescEn;
    }

    
    public void setConsumesTimeInMinutes(Long consumesTimeInMinutes) {
        this.consumesTimeInMinutes = consumesTimeInMinutes;
    }


    public Long getConsumesTimeInMinutes() {
        return consumesTimeInMinutes;
    }

    /**
     * Set User Assigned To
     * @param userAssignedTo User Assigned To
     */
    public void setUserAssignedTo(String userAssignedTo) {
        this.userAssignedTo = userAssignedTo;
    }
    
    /**
     * Get User Assigned To
     * @return User Assigned To
     */
    public String getUserAssignedTo() {
        return userAssignedTo;
    }
    
    /**
     * Set Step User
     * @param stepUser Step User
     */
    public void setStepUser(String stepUser) {
        this.stepUser = stepUser;
    }
    
    /**
     * Get Step User
     * @return Step User
     */
    public String getStepUser() {
        return stepUser;
    }

  
    /**
     * Setter for Current Date Time
     *
     * @param currDateTime Current Date Time
     */
    public void setCurrDateTime(Date currDateTime) {
        this.currDateTime = currDateTime;
    }

    /**
     * Getter for Current Date Time
     *
     * @return currDateTime Current Date Time
     */
    public Date getCurrDateTime() {
        return currDateTime;
    }

    public void setTrsId(Long trsId) {
        this.trsId = trsId;
    }

    public Long getTrsId() {
        return trsId;
    }
}
