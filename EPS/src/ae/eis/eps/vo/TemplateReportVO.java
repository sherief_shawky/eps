/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  04/10/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;
import java.util.List;

/**
 * Procedure template report value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateReportVO extends ValueObject {
    /*
     * Constants
     */
    
    /** Report type values. */
    public static final Integer TYPE_DYNAMIC = new Integer(1);
    public static final Integer TYPE_STATIC = new Integer(2);

    /*
     * Fields
     */
    
    /** Report Arabic name. */
    private String nameAr;
    
    /** Report link. */
    private String reportLink;
    
    /** Report type, dynamic or static. */
    private Integer reportType;
    
    /** Report Type Description */
    private String reportTypeDesc;
    
    /** Related report template. */
    private ProcedureTemplateVO template;
    
    /** Report parameters list. */
    private List parametersList;
    
    /*
     * Methods
     */

    /**
     * Set Report Arabic name.
     * 
     * @param nameAr Report Arabic name.
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * Get Report Arabic name.
     * 
     * @return Report Arabic name.
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * Set Report link.
     * 
     * @param reportLink Report link.
     */
    public void setReportLink(String reportLink) {
        this.reportLink = reportLink;
    }

    /**
     * Get Report link.
     * 
     * @return Report link.
     */
    public String getReportLink() {
        return reportLink;
    }

    /**
     * Set Report type, dynamic or static.
     * 
     * @param reportType Report type, dynamic or static.
     */
    public void setReportType(Integer reportType) {
        if (reportType == null ||
            reportType.equals(TYPE_DYNAMIC) ||
            reportType.equals(TYPE_STATIC)) {
            this.reportType = reportType;
            return;
        }

        throw new IllegalArgumentException("Invalid reportType:" + reportType);
    }

    /**
     * Get Report type, dynamic or static.
     * 
     * @return Report type, dynamic or static.
     */
    public Integer getReportType() {
        return reportType;
    }

    /**
     * Check if report type is "Static".
     * 
     * @return true if report type is "Static".
     */
    public boolean isReportTypeStatic() {
        return reportType != null && reportType.equals(TYPE_STATIC);
    }

    /**
     * Check if report type is "Dynamic".
     * 
     * @return true if report type is "Dynamic".
     */
    public boolean isReportTypeDynamic() {
        return reportType != null && reportType.equals(TYPE_DYNAMIC);
    }

    /**
     * Set Related report template.
     * 
     * @param template Related report template.
     */
    public void setTemplate(ProcedureTemplateVO template) {
        this.template = template;
    }

    /**
     * Get Related report template.
     * 
     * @return Related report template.
     */
    public ProcedureTemplateVO getTemplate() {
        return template;
    }

    /**
     * Set Report parameters list.
     * 
     * @param parametersList Report parameters list.
     */
    public void setParametersList(List parametersList) {
        this.parametersList = parametersList;
    }

    /**
     * Get Report parameters list.
     * 
     * @return Report parameters list.
     */
    public List getParametersList() {
        return parametersList;
    }

    /**
     * Check if this report has parameters.
     * 
     * @return true if this report has parameters.
     */
    public boolean hasParameters() {
        return parametersList != null && parametersList.size() > 0;
    }

    /**
     * Set Report Type Description
     * 
     * @param reportTypeDesc Report Type Description
     */
    public void setReportTypeDesc(String reportTypeDesc) {
        this.reportTypeDesc = reportTypeDesc;
    }

    /**
     * Get Report Type Description
     * 
     * @return reportTypeDesc Report Type Description
     */
    public String getReportTypeDesc() {
        return reportTypeDesc;
    }
    
}