/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Procedure field value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
@XmlRootElement
public class ProcedureFieldVO extends TemplateFieldVO {
    /*
     * Instance variables.
     */
     
    /** Field procedure. */
    private ProcedureVO procedure;
    
    /*
     * Methods
     */

    /**
     * Set Field procedure.
     * 
     * @param procedure Field procedure.
     */
    public void setProcedure(ProcedureVO procedure) {
        this.procedure = procedure;
    }

    /**
     * Get Field procedure.
     * 
     * @return Field procedure.
     */
    public ProcedureVO getProcedure() {
        return procedure;
    }
}