/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  04/06/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Procedure template field option value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateFieldOptionVO extends ValueObject {
    /*
     * Instance variables
     */
    
    /** Template field info. */
    private TemplateFieldVO templateField;
    
    /**
     * Related field option. This option will be displayed only if related 
     * field option was selected.
     */
    private TemplateFieldOptionVO relatedTemplateFieldOption;

    /** Option order number. */
    private Integer orderNo;
    
    /** Option value. */
    private String value;
    
    /** Option arabic description. */
    private String nameAr;
    
    /** Option english description. */
    private String nameEn;
    
    /*
     * Methods
     */
    
    /**
     * Set Template field info.
     * 
     * @param templateField Template field info.
     */
    public void setTemplateField(TemplateFieldVO templateField) {
        this.templateField = templateField;
    }

    /**
     * Get Template field info.
     * 
     * @return Template field info.
     */
    public TemplateFieldVO getTemplateField() {
        return templateField;
    }

    /**
     * Set Option order number.
     * 
     * @param orderNo Option order number.
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * Get Option order number.
     * 
     * @return Option order number.
     */
    public Integer getOrderNo() {
        return orderNo;
    }

    /**
     * Set Option value.
     * 
     * @param value Option value.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Get Option value.
     * 
     * @return Option value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Set Option arabic description.
     * 
     * @param nameAr Option arabic description.
     */
    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    /**
     * Get Option arabic description.
     * 
     * @return Option arabic description.
     */
    public String getNameAr() {
        return nameAr;
    }

    /**
     * Set Option english description.
     * 
     * @param nameEn Option english description.
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * Get Option english description.
     * 
     * @return Option english description.
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * Set Related field option. This option will be displayed only if 
     * related field option was selected.
     * 
     * @param option Related field option.
     */
    public void setRelatedTemplateFieldOption(TemplateFieldOptionVO option) {
        this.relatedTemplateFieldOption = option;
    }

    /**
     * Get Related field option. This option will be displayed only if 
     * related field option was selected.
     * 
     * @return Related field option.
     */
    public TemplateFieldOptionVO getRelatedTemplateFieldOption() {
        return relatedTemplateFieldOption;
    }
    
    /**
     * Check if this field option has related field option. This option will be
     * displayed only if related field option was selected.
     * 
     * @return true if this field option has related field option.
     */
    public boolean hasRelatedTemplateFieldOption() {
        if (relatedTemplateFieldOption == null ||
            relatedTemplateFieldOption.getTemplateField() == null ||
            relatedTemplateFieldOption.getTemplateField().getId() == null) {
            return false;
        }

        return relatedTemplateFieldOption.getOrderNo() != null ||
               relatedTemplateFieldOption.getValue() != null;
    }
}