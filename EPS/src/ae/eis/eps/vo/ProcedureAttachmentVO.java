/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  07/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.common.vo.UserVO;
import ae.eis.util.vo.AttachmentVO;

/**
 * Procedure attachment value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureAttachmentVO extends AttachmentVO {
    /*
     * Instants variables.
     */

    /** Attachment deletion flag. */
    private Integer isDeleted;

    /** Related attachment procedure. */
    private ProcedureVO procedure;

    /** User added this attachment. */
    private UserVO user;

    /** Procedure step used to add this attachment. */
    private ProcedureStepVO step;

    /*
     * Constructors
     */
    
    /**
     * Default constructor
     */
    public ProcedureAttachmentVO() {
        // Empty body
    }

    /**
     * Construct and initialize new value object.
     * 
     * @param attachmentVO Attachment info.
     */
    public ProcedureAttachmentVO(AttachmentVO attachmentVO) {
        setAttachmentInfo(attachmentVO);
    }

    /*
     * Methods
     */

    /**
     * Set Attachment deletion flag.
     * 
     * @param isDeleted Attachment deletion flag.
     */
    public void setIsDeleted(Integer isDeleted) {
        if (isDeleted == null || isDeleted.equals(TRUE) || isDeleted.equals(FALSE)) {
            this.isDeleted = isDeleted;
            return;
        }
        
        throw new IllegalArgumentException("Invalid isDeleted parameter: " + isDeleted);
    }

    /**
     * Get Attachment deletion flag.
     * 
     * @return Attachment deletion flag.
     */
    public Integer getIsDeleted() {
        return isDeleted;
    }

    /**
     * Get Attachment deletion flag.
     * 
     * @return Attachment deletion flag.
     */
    public boolean isDeleted() {
        return isDeleted != null && isDeleted.equals(TRUE);
    }

    /**
     * Set Related attachment procedure.
     * 
     * @param procedure Related attachment procedure.
     */
    public void setProcedure(ProcedureVO procedure) {
        this.procedure = procedure;
    }

    /**
     * Get Related attachment procedure.
     * 
     * @return Related attachment procedure.
     */
    public ProcedureVO getProcedure() {
        return procedure;
    }

    /**
     * Set User added this attachment.
     * 
     * @param user User added this attachment.
     */
    public void setUser(UserVO user) {
        this.user = user;
    }

    /**
     * Get User added this attachment.
     * 
     * @return User added this attachment.
     */
    public UserVO getUser() {
        return user;
    }

    /**
     * Set Procedure step used to add this attachment.
     * 
     * @param step Procedure step used to add this attachment.
     */
    public void setStep(ProcedureStepVO step) {
        this.step = step;
    }

    /**
     * Get Procedure step used to add this attachment.
     * 
     * @return Procedure step used to add this attachment.
     */
    public ProcedureStepVO getStep() {
        return step;
    }
}