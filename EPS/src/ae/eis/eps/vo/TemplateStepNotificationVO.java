/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  06/05/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Procedure template step notification value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepNotificationVO extends ValueObject {
    /*
     * Constants and class variables.
     */

    /** Notificaton type values. */
    public static final Integer TYPE_E_MAIL = new Integer(1);
    public static final Integer TYPE_FAX = new Integer(2);
    public static final Integer TYPE_SMS = new Integer(3);
    public static final Integer TYPE_MAIL = new Integer(4);

    /*
     * Instance variables.
     */

    /** Notifivation title. */
    private String title;

    /** Notifcation message. */
    private String messageBody;

    /** Notifcation type. */
    private Integer notificationType;

    /** Notifcation type description. */
    private String notificationTypeDesc;
    
    /** Template Step Value Object */
    private TemplateStepVO templateStep;
    /*
     * Methods
     */    
    
    /**
     * Set Notifivation title.
     *
     * @param title Notifivation title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get Notifivation title.
     *
     * @return Notifivation title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set Notifcation message.
     *
     * @param messageBody Notifcation message.
     */
    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    /**
     * Get Notifcation message.
     *
     * @return Notifcation message.
     */
    public String getMessageBody() {
        return messageBody;
    }

    /**
     * Set Notifcation type.
     *
     * @param type Notifcation type.
     */
    public void setNotificationType(Integer type) {
        if (type == null ||
            type.equals(TYPE_E_MAIL) ||
            type.equals(TYPE_FAX) ||
            type.equals(TYPE_MAIL) ||
            type.equals(TYPE_SMS)) {
            this.notificationType = type;
            return;
        }

        throw new IllegalArgumentException("Invalid notification type: " + type);
    }

    /**
     * Check if notification tyoe is E-Mail.
     *
     * @return true if notification tyoe is E-Mail.
     */
    public boolean isEmail() {
        return notificationType != null && notificationType.equals(TYPE_E_MAIL);
    }

    /**
     * Check if notification tyoe is Mail.
     *
     * @return true if notification tyoe is Mail.
     */
    public boolean isMail() {
        return notificationType != null && notificationType.equals(TYPE_MAIL);
    }

    /**
     * Check if notification tyoe is Fax.
     *
     * @return true if notification tyoe is Fax.
     */
    public boolean isFax() {
        return notificationType != null && notificationType.equals(TYPE_FAX);
    }

    /**
     * Check if notification tyoe is SMS.
     *
     * @return true if notification tyoe is SMS.
     */
    public boolean isSMS() {
        return notificationType != null && notificationType.equals(TYPE_SMS);
    }

    /**
     * Get Notifcation type.
     *
     * @return Notifcation type.
     */
    public Integer getNotificationType() {
        return notificationType;
    }

    /**
     * set Template Step
     * 
     * @param templateStep
     */
    public void setTemplateStep(TemplateStepVO templateStep) {
        this.templateStep = templateStep;
    }

    /**
     * get Template Step
     * 
     * @return templateStep
     */
    public TemplateStepVO getTemplateStep() {
        return templateStep;
    }

    /**
     * set Notification Type Description
     * 
     * @param notificationTypeDesc
     */
    public void setNotificationTypeDesc(String notificationTypeDesc) {
        this.notificationTypeDesc = notificationTypeDesc;
    }

    /**
     * get Notification Type Description
     * 
     * @return notificationTypeDesc
     */
    public String getNotificationTypeDesc() {
        return notificationTypeDesc;
    }
}