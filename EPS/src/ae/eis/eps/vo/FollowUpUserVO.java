/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 * 
 * 
 */

package ae.eis.eps.vo;


import ae.eis.common.vo.UserVO;
import ae.eis.util.vo.ValueObject;

/**
 * Follow up user value object.
 *
 * @author Oqba OWDA 
 * @version 1.00
 */
public class FollowUpUserVO extends ValueObject {
    /*
     * Instance variables.
     */

    /** Security group. */
    private UserVO user;

    /** Related procedure template. */
    private ProcedureTemplateVO procedureTemplate;

    /*
     * Constructors
     */

    /**
     * Default constructor
     */
    public FollowUpUserVO() {
        // Default constructor
    }

    /**
     * Construct and initialize new FollowUpUserVO.
     *
     * @param vo Security group value object.
     */
    public FollowUpUserVO(UserVO vo) {
        setUser(vo);
    }

    /*
     * Methods
     */

    /**
     * Set User
     *
     * @param securityGroup Security group
     */
    public void setUser(UserVO user) {
        this.user = user;
    }

    /**
     * Get User
     *
     * @return User
     */
    public UserVO getUser() {
        return user;
    }

    /**
     * Set  Related procedure template.
     * 
     * @param procedureTemplate  Related procedure template.
     */
    public void setProcedureTemplate(ProcedureTemplateVO procedureTemplate) {
        this.procedureTemplate = procedureTemplate;
    }

    /**
     * Get  Related procedure template.
     * 
     * @return  Related procedure template.
     */
    public ProcedureTemplateVO getProcedureTemplate() {
        return procedureTemplate;
    }

    
}