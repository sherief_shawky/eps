/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   19/05/2009  - File created.
 */
 
package ae.eis.eps.vo;

import ae.eis.util.vo.LogValueObject;

/**
 * Procedure template value object.
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public class ProcedureTemplateLogVO extends LogValueObject {
    
    /** Procedure Log Values */
    public static final Integer ACTION_CREATE_PROCEDURE = new Integer(1);
    public static final Integer ACTION_UPDATE_PROCEDURE = new Integer(2);
    public static final Integer ACTION_ACTIVATE_PROCEDURE = new Integer(3);
    public static final Integer ACTION_DEACTIVATE_PROCEDURE = new Integer(4);
    public static final Integer ACTION_ADD_STEP = new Integer(5);
    public static final Integer ACTION_UPDATE_STEP = new Integer(6);
    public static final Integer ACTION_DELETE_STEP = new Integer(7);
    public static final Integer ACTION_ADD_FIELD = new Integer(8);
    public static final Integer ACTION_UPDATE_FIELD = new Integer(9);
    public static final Integer ACTION_DELETE_FIELD = new Integer(10);
    
    public static final Integer ACTION_ADD_REPORT = new Integer(11);
    public static final Integer ACTION_UPDATE_REPORT = new Integer(12);
    public static final Integer ACTION_DELETE_REPORT = new Integer(13);     
    public static final Integer ACTION_ADD_PARAMETER = new Integer(14);
    public static final Integer ACTION_UPDATE_PARAMETER = new Integer(15);
    public static final Integer ACTION_DELETE_PARAMETER = new Integer(16);
    public static final Integer ACTION_DELETE_SEARCH_FIELD = new Integer(17);
    public static final Integer ACTION_ADD_SEARCH_FIELD = new Integer(18);
    public static final Integer ACTION_UPDATE_SEARCH_FIELD = new Integer(19);
    public static final Integer ACTION_DELETE_SEARCH_RESULT = new Integer(20);
    public static final Integer ACTION_ADD_SEARCH_RESULT = new Integer(21);
    public static final Integer ACTION_UPDATE_SEARCH_RESULT = new Integer(22);
    public static final Integer ACTION_ADD_SEARCH_SECURITY = new Integer(23);
    public static final Integer ACTION_DELETE_SEARCH_SECURITY = new Integer(24);

    /*
     * Instance Variable
     */
    /** Procedure Template Value Object */
    private ProcedureTemplateVO procedureTemplateVO;

    
    /*
     * Accessors
     */
    /**
     * set Procedure Template Value Object
     * 
     * @param procedureTemplateVO
     */
    public void setProcedureTemplateVO(ProcedureTemplateVO procedureTemplateVO) {
        this.procedureTemplateVO = procedureTemplateVO;
    }

    /**
     * get Procedure Template Value Object
     * 
     * @return procedureTemplateVO
     */
    public ProcedureTemplateVO getProcedureTemplateVO() {
        return procedureTemplateVO;
    }
    
}