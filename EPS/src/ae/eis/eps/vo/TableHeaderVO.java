/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00     Mena Emiel      23/02/2011  - File created and implemented.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;
import java.util.List;

/**
 * Table header value object.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public class TableHeaderVO extends TemplateTableHeaderVO {
    /*
     * Constructors
     */
     
    /** procedure field info. */
    private ProcedureFieldVO procedureFieldVO;
     
    /** List of table column value VO. */
    private List tableColumnValueVO;

    /**
     * Default constructor.
     */
    public TableHeaderVO() {
        // Empty body
    }

    /**
     * Setter for procedure field info
     * 
     * @param procedureFieldVO procedure field info
     */
    public void setProcedureFieldVO(ProcedureFieldVO procedureFieldVO) {
        this.procedureFieldVO = procedureFieldVO;
    }

    /**
     * Getter for procedure field info
     * 
     * @return procedure field info
     */
    public ProcedureFieldVO getProcedureFieldVO() {
        return procedureFieldVO;
    }

    /**
     * Setter for table column value list
     * 
     * @param tableColumnValueVO list of table column value
     */
    public void setTableColumnValueVO(List tableColumnValueVO) {
        this.tableColumnValueVO = tableColumnValueVO;
    }

    /**
     * Getter for table column value list
     * 
     * @return list of table column value
     */
    public List getTableColumnValueVO() {
        return tableColumnValueVO;
    }
}