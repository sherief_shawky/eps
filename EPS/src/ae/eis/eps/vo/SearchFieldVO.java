/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  03/12/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;
import java.util.Date;

/**
 * Procedure search field value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class SearchFieldVO extends ValueObject {
    /*
     * Constants
     */
    
    /** Search type values. */
    public static final Integer SEARCH_TYPE_BETWEEN = new Integer(1);
    public static final Integer SEARCH_TYPE_LIKE = new Integer(2);
    public static final Integer SEARCH_TYPE_EQUAL = new Integer(3);

    /*
     * Fields
     */

    /** Related template field. */
    private TemplateFieldVO templateField;
    
    /** Field search type. */
    private Integer searchType;
    
    /** Field search type arabic description. */
    private String searchTypeDescAr;
    
    /** Search field order number. */
    private Integer orderNo;

    /** Tabular column span, if 2 the field will be span the entire row. */
    private Integer columnSpan;
    
    /** Tabular column span description. */
    private String columnSpanDesc;    
    
    /** View in new line flag. */
    private Integer viewInNewLine;
    
    /** Strat search date. */
    private Date dateFrom;

    /** End search date. */
    private Date dateTo;
    
    /** Start search date flag. */
    private boolean dateSearchFrom;
    
    /** End search date flag. */
    private boolean dateSearchTo;
    
    /** Template Table Header VO. */
    private TemplateTableHeaderVO templateTableHeaderVO;
    
    /*
     * Constructors
     */
     
    /**
     * Default constructor
     */
    public SearchFieldVO() {
        // Empty body
    }
    
    /**
     * Construct and initialize new search field instance.
     * 
     * @param vo Search field initialial values.
     */
    public SearchFieldVO(SearchFieldVO vo) {
        if (vo == null) {
            return;
        }
        
        setId(vo.getId());
        setCenterName(vo.getCenterName());
        setEmployeeName(vo.getEmployeeName());
        setCreatedBy(vo.getCreatedBy());
        setCreationDate(vo.getCreationDate());
        setUpdatedBy(vo.getUpdatedBy());
        setUpdateDate(vo.getUpdateDate());
        setUserEmirateCode(vo.getUserEmirateCode());

        setColumnSpan(vo.getColumnSpan());
        setColumnSpanDesc(vo.getColumnSpanDesc());
        setOrderNo(vo.getOrderNo());
        setViewInNewLine(vo.getViewInNewLine());
        setSearchType(vo.getSearchType());
        setSearchTypeDescAr(vo.getSearchTypeDescAr());

        if (vo.getDateFrom() != null) {
            setDateFrom(new Date(vo.getDateFrom().getTime()));
        }
        
        if (vo.getDateTo() != null) {
            setDateTo(new Date(vo.getDateTo().getTime()));
        }
        
        if (vo.getTemplateField() != null) {
            setTemplateField(new TemplateFieldVO(vo.getTemplateField()));
        }
    }

    /*
     * Methods
     */

    /**
     * Set Related template field.
     * 
     * @param templateField Related template field.
     */
    public void setTemplateField(TemplateFieldVO templateField) {
        this.templateField = templateField;
    }

    /**
     * Get Related template field.
     * 
     * @return Related template field.
     */
    public TemplateFieldVO getTemplateField() {
        return templateField;
    }

    /**
     * Set Field search type.
     * 
     * @param type Field search type.
     */
    public void setSearchType(Integer type) {
        if (type == null ||
            type.equals(SEARCH_TYPE_BETWEEN) ||
            type.equals(SEARCH_TYPE_EQUAL) ||
            type.equals(SEARCH_TYPE_LIKE)) {
            // Valid search type
            this.searchType = type;
            return;
        }
        
        throw new IllegalArgumentException("Invalid search type: " + type);
    }

    /**
     * Get Field search type.
     * 
     * @return Field search type.
     */
    public Integer getSearchType() {
        return searchType;
    }
    
    /**
     * Check if search type is "Between".
     * 
     * @return true if search type is "Between".
     */
    public boolean isSearchTypeBetween() {
        return searchType != null && searchType.equals(SEARCH_TYPE_BETWEEN);
    }

    /**
     * Check if search type is "Equal".
     * 
     * @return true if search type is "Equal".
     */
    public boolean isSearchTypeEqual() {
        return searchType != null && searchType.equals(SEARCH_TYPE_EQUAL);
    }

    /**
     * Check if search type is "Like".
     * 
     * @return true if search type is "Like".
     */
    public boolean isSearchTypeLike() {
        return searchType != null && searchType.equals(SEARCH_TYPE_LIKE);
    }

    /**
     * Set Search field order number.
     * 
     * @param orderNo Search field order number.
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * Get Search field order number.
     * 
     * @return Search field order number.
     */
    public Integer getOrderNo() {
        return orderNo;
    }

    /**
     * Set Tabular column span, if 2 the field will be span the entire row.
     * 
     * @param span Tabular column span, if 2 the field will be span the entire row.
     */
    public void setColumnSpan(Integer span) {
        if (span == null || span.intValue() == 1 || span.intValue() == 2) {
            this.columnSpan = span;
            return;
        }
        
        throw new IllegalArgumentException("Invalid column span value: " + span);
    }

    /**
     * Get Tabular column span, if 2 the field will be span the entire row.
     * 
     * @return Tabular column span, if 2 the field will be span the entire row.
     */
    public Integer getColumnSpan() {
        return columnSpan;
    }

    /**
     * Set View in new line flag.
     * 
     * @param flag View in new line flag.
     */
    public void setViewInNewLine(Integer flag) {
        if (flag == null || flag.equals(TRUE) || flag.equals(FALSE)) {
            this.viewInNewLine = flag;
            return;
        }
        
        throw new IllegalArgumentException("Invalid viewInNewLine value: " + flag);
    }

    /**
     * Get View in new line flag.
     * 
     * @return View in new line flag.
     */
    public Integer getViewInNewLine() {
        return viewInNewLine;
    }

    /**
     * Get View in new line flag.
     * 
     * @return View in new line flag.
     */
    public boolean isViewInNewLine() {
        return viewInNewLine != null && viewInNewLine.equals(TRUE) ;
    }

    /**
     * Set Field search type arabic description.
     * 
     * @param searchTypeDescAr Field search type arabic description.
     */
    public void setSearchTypeDescAr(String searchTypeDescAr) {
        this.searchTypeDescAr = searchTypeDescAr;
    }

    /**
     * Get Field search type arabic description.
     * 
     * @return Field search type arabic description.
     */
    public String getSearchTypeDescAr() {
        return searchTypeDescAr;
    }

    /**
     * Set Strat search date.
     * 
     * @param dateFrom Strat search date.
     */
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    /**
     * Get Strat search date.
     * 
     * @return Strat search date.
     */
    public Date getDateFrom() {
        return dateFrom;
    }

    /**
     * Set End search date.
     * 
     * @param dateTo End search date.
     */
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    /**
     * Get End search date.
     * 
     * @return End search date.
     */
    public Date getDateTo() {
        return dateTo;
    }

    /**
     * Set Start search date flag.
     * 
     * @param dateSearchFrom Start search date flag.
     */
    public void setDateSearchFrom(boolean dateSearchFrom) {
        this.dateSearchFrom = dateSearchFrom;
    }

    /**
     * Get Start search date flag.
     * 
     * @return Start search date flag.
     */
    public boolean isDateSearchFrom() {
        return dateSearchFrom;
    }

    /**
     * Set End search date flag.
     * 
     * @param dateSearchTo End search date flag.
     */
    public void setDateSearchTo(boolean dateSearchTo) {
        this.dateSearchTo = dateSearchTo;
    }

    /**
     * Get End search date flag.
     * 
     * @return End search date flag.
     */
    public boolean isDateSearchTo() {
        return dateSearchTo;
    }

    /**
     * Set Column Span Description
     * 
     * @param columnSpanDesc Column Span Description
     */
    public void setColumnSpanDesc(String columnSpanDesc) {
        this.columnSpanDesc = columnSpanDesc;
    }

    /**
     * Get Column Span Description
     * 
     * @return columnSpanDesc Column Span Description
     */
    public String getColumnSpanDesc() {
        return columnSpanDesc;
    }

    /**
     * Setter for template table header info
     * 
     * @param templateTableHeaderVO template table header info
     */
    public void setTemplateTableHeaderVO(TemplateTableHeaderVO templateTableHeaderVO) {
        this.templateTableHeaderVO = templateTableHeaderVO;
    }

    /**
     * Getter for template table header info
     * 
     * @return template table header info
     */
    public TemplateTableHeaderVO getTemplateTableHeaderVO() {
        return templateTableHeaderVO;
    }
}