/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.vo;

import ae.eis.util.vo.ValueObject;

/**
 * Procedure template step-field value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepFieldVO extends ValueObject {
    /*
     * Instance variables.
     */
    
    /** Template field info. */
    private TemplateFieldVO field;
    
    /** Template step info. */
    private TemplateStepVO templateStep;
    
    /** Template step-fields group value object. */
    private TemplateStepFieldGroupVO templateStepFieldGroup;
    
    /** Field order number. */
    private Integer orderNo;
    
    /** Editable flag. */
    private Integer isEditable;

    /** Field mandatory flag. */
    private Integer isMandatory;
    
    /** Field printable flag, if false the field will not be printed. */
    private Integer isPrintable;
    
    /** Tabular column span, if 2 the field will be span the entire row. */
    private Integer columnSpan;
    
    /** column Span description */
    private String columnSpanDesc;    
    
    /** Number Of Rows */
    private Integer noOfRows;
    
    /** View In New Line */
    private Integer viewInNewLine;
    
    /** Minimum Number Of Rows */
    private Integer minNoOfRows;
    
    
    /*
     * Methods
     */

    /**
     * Set Template field info.
     * 
     * @param templateField Template field info.
     */
    public void setField(TemplateFieldVO templateField) {
        this.field = templateField;
    }

    /**
     * Get Template field info.
     * 
     * @return Template field info.
     */
    public TemplateFieldVO getField() {
        return field;
    }

    /**
     * Set Template step info.
     * 
     * @param templateStep Template step info.
     */
    public void setTemplateStep(TemplateStepVO templateStep) {
        this.templateStep = templateStep;
    }

    /**
     * Get Template step info.
     * 
     * @return Template step info.
     */
    public TemplateStepVO getTemplateStep() {
        return templateStep;
    }

    /**
     * Set Editable flag
     * 
     * @param isEditable Editable flag
     */
    public void setIsEditable(Integer isEditable) {
        if (isEditable == null ||
            isEditable.equals(TRUE) ||
            isEditable.equals(FALSE)) {
            this.isEditable = isEditable;
            return;
        }
        
        throw new IllegalArgumentException(
            "Invalid isEditable value: " + isEditable);
    }

    /**
     * Get Editable flag
     * 
     * @return Editable flag
     */
    public Integer getIsEditable() {
        return isEditable;
    }

    /**
     * Get Editable flag
     * 
     * @return Editable flag
     */
    public boolean isEditable() {
        return isEditable != null && isEditable.equals(TRUE);
    }

    /**
     * Set Field mandatory flag.
     * 
     * @param isMandatory Field mandatory flag.
     */
    public void setIsMandatory(Integer isMandatory) {
        if (isMandatory == null ||
            isMandatory.equals(TRUE) ||
            isMandatory.equals(FALSE)) {
            this.isMandatory = isMandatory;
            return;
        }

        throw new IllegalArgumentException(
            "Invalid isMandatory value: " + isMandatory);
    }

    /**
     * Get Field mandatory flag.
     * 
     * @return Field mandatory flag.
     */
    public Integer getIsMandatory() {
        return isMandatory;
    }

    /**
     * Get Field mandatory flag.
     * 
     * @return Field mandatory flag.
     */
    public boolean isMandatory() {
        return isMandatory != null && isMandatory.equals(TRUE);
    }

    /**
     * Set Field printable flag, if false the field will not be printed.
     * 
     * @param isPrintable Field printable flag.
     */
    public void setIsPrintable(Integer isPrintable) {
        if (isPrintable == null ||
            isPrintable.equals(TRUE) ||
            isPrintable.equals(FALSE)) {
            this.isPrintable = isPrintable;
            return;
        }

        throw new IllegalArgumentException(
            "Invalid isPrintable value: " + isPrintable);
    }

    /**
     * Get Field printable flag, if false the field will not be printed.
     * 
     * @return Field printable flag, if false the field will not be printed.
     */
    public Integer getIsPrintable() {
        return isPrintable;
    }

    /**
     * Get Field printable flag, if false the field will not be printed.
     * 
     * @return Field printable flag, if false the field will not be printed.
     */
    public boolean isPrintable() {
        return isPrintable != null && isPrintable.equals(TRUE);
    }
    
    /**
     * get Is Printable Description
     * 
     * @return Is Printable Description
     */
    public String getIsPrintableDesc(){
        return getBooleanDescAr(isPrintable);
    }
    
    /**
     * get Is Mandatory Description
     * 
     * @return Is Mandatory Description
     */
    public String getIsMandatoryDesc(){
        return getBooleanDescAr(isMandatory);
    }    
    
    /**
     * get Is Editable Description
     * 
     * @return Is Editable Description
     */
    public String getIsEditableDesc(){
        return getBooleanDescAr(isEditable);
    }        

    /**
     * Set Field order number.
     * 
     * @param orderNo Field order number.
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * Get Field order number.
     * 
     * @return Field order number.
     */
    public Integer getOrderNo() {
        return orderNo;
    }
    
    /**
     * Set Tabular column span, if 2 the field will be span the entire row.
     * 
     * @param columnSpan Tabular column span.
     */
    public void setColumnSpan(Integer columnSpan) {
        this.columnSpan = columnSpan;
    }

    /**
     * Get Tabular column span, if 2 the field will be span the entire row.
     * 
     * @return Tabular column span, if 2 the field will be span the entire row.
     */
    public Integer getColumnSpan() {
        return columnSpan;
    }
    
    /**
     * set Column Span Description
     * 
     * @param columnSpanDesc Column Span Description
     */
    public void setColumnSpanDesc(String columnSpanDesc) {
        this.columnSpanDesc = columnSpanDesc;
    }

    /**
     * get Column Span Description
     * 
     * @return columnSpanDesc Column Span Description
     */
    public String getColumnSpanDesc() {
        return columnSpanDesc;
    }

    /**
     * Set Number Of Rows
     * 
     * @param noOfRows Number Of Rows
     */
    public void setNoOfRows(Integer noOfRows) {
        this.noOfRows = noOfRows;
    }

    /**
     * Get Number Of Rows
     * 
     * @return noOfRows Number Of Rows
     */
    public Integer getNoOfRows() {
        return noOfRows;
    }    

    /**
     * Set View In New Line
     * 
     * @param viewInNewLine View In New Line
     */
    public void setViewInNewLine(Integer viewInNewLine) {
        if (viewInNewLine == null ||
            viewInNewLine.equals(TRUE) ||
            viewInNewLine.equals(FALSE)) {
            this.viewInNewLine = viewInNewLine;
            return;
        }
        
        throw new IllegalArgumentException(
            "Invalid viewInNewLine value: " + viewInNewLine);
    }

    /**
     * Get View In New Line
     * 
     * @return viewInNewLine View In New Line
     */
    public Integer getViewInNewLine() {
        return viewInNewLine;
    }
    
    /**
     * Is View In New Line
     * 
     * @return true if this field will view in new line
     */
    public boolean isViewInNewLine(){
        return viewInNewLine != null && viewInNewLine.equals(TRUE);
    }

    /**
     * Set Template step-fields group value object.
     * 
     * @param group Template step-fields group value object.
     */
    public void setTemplateStepFieldGroup(TemplateStepFieldGroupVO group) {
        this.templateStepFieldGroup = group;
    }

    /**
     * Get Template step-fields group value object.
     * 
     * @return Template step-fields group value object.
     */
    public TemplateStepFieldGroupVO getTemplateStepFieldGroup() {
        return templateStepFieldGroup;
    }

    /**
     * Set Minimum Number Of Rows
     * 
     * @param minNoOfRows Minimum Number Of Rows
     */
    public void setMinNoOfRows(Integer minNoOfRows) {
        this.minNoOfRows = minNoOfRows;
    }
 
    /**
     * Get Minimum Number Of Rows
     * 
     * @return minNoOfRows Minimum Number Of Rows
     */
    public Integer getMinNoOfRows() {
        return minNoOfRows;
    }
    
}