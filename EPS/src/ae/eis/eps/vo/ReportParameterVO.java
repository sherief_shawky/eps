/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  04/10/2009  - File created.
 */

package ae.eis.eps.vo;


import ae.eis.util.vo.ValueObject;

/**
 * Procedure template report field value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ReportParameterVO extends ValueObject {
    /*
     * Fields
     */
    
    /** Parameter mandatory-flag. */
    private Integer isMandatory;
    
    /** Report parameter name. */
    private String name;
    
    /** Parameter order number on report search page. */
    private Integer orderNo;
    
    /** Max search days allowed for date fields. */
    private Integer maxSearchDays;
    
    /** Related template field info. */
    private TemplateFieldVO templateField;
    
    /** Related report info. */
    private TemplateReportVO report;
    
    /*
     * Methods
     */

    /**
     * Set Parameter mandatory-flag.
     * 
     * @param flag Parameter mandatory-flag.
     */
    public void setIsMandatory(Integer flag) {
        if (flag == null || flag.equals(TRUE) || flag.equals(FALSE)) {
            this.isMandatory = flag;
            return;
        }

        throw new IllegalArgumentException("Invalid isMandatory parameter: " 
            + flag);
    }

    /**
     * Get Parameter mandatory-flag.
     * 
     * @return Parameter mandatory-flag.
     */
    public Integer getIsMandatory() {
        return isMandatory;
    }

    /**
     * Check if this parameter is mandatory.
     * 
     * @return if this parameter is mandatory.
     */
    public boolean isMandatory() {
        return isMandatory != null && isMandatory.equals(TRUE);
    }
    
    /**
     * Set Report parameter name.
     * 
     * @param name Report parameter name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get Report parameter name.
     * 
     * @return Report parameter name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set Parameter order number on report search page.
     * 
     * @param orderNo Parameter order number on report search page.
     */
    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * Get Parameter order number on report search page.
     * 
     * @return Parameter order number on report search page.
     */
    public Integer getOrderNo() {
        return orderNo;
    }

    /**
     * Set Max search days allowed for date fields.
     * 
     * @param maxSearchDays Max search days allowed for date fields.
     */
    public void setMaxSearchDays(Integer maxSearchDays) {
        this.maxSearchDays = maxSearchDays;
    }

    /**
     * Get Max search days allowed for date fields.
     * 
     * @return Max search days allowed for date fields.
     */
    public Integer getMaxSearchDays() {
        return maxSearchDays;
    }

    /**
     * Set Related template field info.
     * 
     * @param templateField Related template field info.
     */
    public void setTemplateField(TemplateFieldVO templateField) {
        this.templateField = templateField;
    }

    /**
     * Get Related template field info.
     * 
     * @return Related template field info.
     */
    public TemplateFieldVO getTemplateField() {
        return templateField;
    }

    /**
     * Set Related report info.
     * 
     * @param report Related report info.
     */
    public void setReport(TemplateReportVO report) {
        this.report = report;
    }

    /**
     * Get Related report info.
     * 
     * @return Related report info.
     */
    public TemplateReportVO getReport() {
        return report;
    }
}