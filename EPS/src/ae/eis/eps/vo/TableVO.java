/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00     Ahmad M.Adawi   27/10/2011  - File created and implemented.
 */
 
package ae.eis.eps.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Table value object.
 *
 * @author Ahmad M.Adawi
 * @version 1.00
 */
 
public class TableVO extends ProcedureFieldVO {

    /*
     * Fields
     */
    
    /** Table Headers */
    private List tableHeaders;
    
    /** Table Rows */
    private List tableRows;
    
    /*
     * Constructors
     */
    public TableVO() {
        tableHeaders = new ArrayList();
        tableRows = new ArrayList();
    }
    
    /*
     * Methods
     */
     
    /**
     * Setter Method For Table Rows
     * 
     * @param tableRows Table Rows
     */
    public void setTableRows(List tableRows) {
        this.tableRows = tableRows;
    }
    
    /**
     * Getter Method For Table Rows
     * 
     * @return Table Rows
     */
    public List getTableRows() {
        return tableRows;
    }
    
    /**
     * Setter Method For Table Headers
     * 
     * @param tableHeaders Table Headers
     */
    public void setTableHeaders(List tableHeaders) {
        this.tableHeaders = tableHeaders;
    }

    /**
     * Getter Method For Table Headers 
     * 
     * @return Table Headers 
     */
    public List getTableHeaders() {
        return tableHeaders;
    }
    
    /**
     * Add Table Header Value Object
     * 
     * @param header Table Header Value Object
     */
    public void add(TableHeaderVO header) {
        this.tableHeaders.add(header);
    }

    /**
     * Add Table Row Value Object
     * 
     * @param row Row Value Object
     */
    public void add(TableRowVO row) {
        this.tableRows.add(row);
    }
}