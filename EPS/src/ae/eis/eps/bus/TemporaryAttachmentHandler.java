/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemporaryAttachmentDAO;
import ae.eis.eps.vo.TemporaryAttachmentVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;

/**
 * Procedure temporary attachments business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemporaryAttachmentHandler extends BusinessObject {
    /**
     * Save attachment.
     * 
     * @param vo Attachment value object.
     */
    public void saveAttachment(TemporaryAttachmentVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL temporary attachment VO");
        }
        
        if (vo.isEmptyBody()) {
            throw new BusinessException("Empty attachment body");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new BusinessException("NULL attachment file name");
        }

        if (isBlankOrNull(vo.getFileExtension())) {
            throw new BusinessException("NULL attachment file extension");
        }
        
        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL username");
        }

        TemporaryAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemporaryAttachmentDAO) getDAO(TemporaryAttachmentDAO.class);
            dao.save(vo);

            // Commit changes
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Get attachment content by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    public TemporaryAttachmentVO getContentById(Long attachmentId) {
        // Validate parameters
        if (attachmentId == null) {
            throw new BusinessException("NULL temporary attachment ID");
        }

        TemporaryAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemporaryAttachmentDAO) getDAO(TemporaryAttachmentDAO.class);
            return dao.getContentById(attachmentId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * find attachment
     * 
     * @return list of attachment
     * @param sequenceNo
     */
    public SearchPageVO find(int pageNo, Long sequenceNo){
        TemporaryAttachmentDAO dao = null;
        try {
            dao = (TemporaryAttachmentDAO) getDAO(TemporaryAttachmentDAO.class);
            return dao.find(pageNo,sequenceNo);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Delete attachment.
     * 
     * @param attachmentId Attachment ID.
     * @return true if the attachment was deleted successfully.
     */
    public boolean delete(Long attachmentId) {
        // Validate parameters
        if (attachmentId == null) {
            throw new BusinessException("NULL attachment ID");
        }
        
        TemporaryAttachmentDAO dao = null;
        try {
            dao = (TemporaryAttachmentDAO) getDAO(TemporaryAttachmentDAO.class);
            boolean status = dao.delete(attachmentId);
            
            dao.commit();
            return status;
            
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Get total number of attachments.
     * 
     * @param sequenceNo Attachment sequence number.
     * @return total number of attachments.
     */
    public int getAttachmentsCount(Long sequenceNo) {
        if (sequenceNo == null) {
            throw new BusinessException("NULL attachments sequence no");
        }
    
        TemporaryAttachmentDAO dao = null;
        try {
            dao = (TemporaryAttachmentDAO) getDAO(TemporaryAttachmentDAO.class);
            return dao.getAttachmentsCount(sequenceNo);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
}