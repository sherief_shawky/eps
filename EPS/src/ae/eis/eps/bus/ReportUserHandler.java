/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   14/10/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ReportUserDAO;
import ae.eis.eps.vo.ReportUserVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;

/**
 * Procedure template report-users business object.
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public class ReportUserHandler extends BusinessObject {
    /*
     * Business Objects
     */

    /*
     * Business Methods
     */
    
    /**
     * Find report parameter
     * 
     * @return report parameters
     * @param pageNo pagination Page Number
     * @param reportId EPS report ID
     */
    public SearchPageVO find(Long reportId, int pageNo){
        // Validate parameters
        if (reportId == null) {
            throw new BusinessException("NULL report ID");
        }

        ReportUserDAO dao = null;
        try {
            dao = (ReportUserDAO) getDAO(ReportUserDAO.class);
            return dao.find(reportId,pageNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }
    
    /**
     * Find template step Security Groups
     * 
     * @return Search Page Value Object
     * @param vo Report User Value Object
     * @param pageNo page Number 
     */
    public SearchPageVO findUsers(ReportUserVO vo,int pageNo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL report ID");
        }

        ReportUserDAO dao = null;
        try {
            dao = (ReportUserDAO) getDAO(ReportUserDAO.class);
            return dao.findUsers(vo,pageNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Add Security User
     * 
     * @param vo Report User Value Object
     */
    public void addSecurityUser(ReportUserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL report user Value Object");
        }
        
        if(vo.getReport() == null ){
            throw new BusinessException("NULL report Value Object");
        }

        if(vo.getReport().getId() == null ){
            throw new BusinessException("NULL report ID");
        }

        if(vo.getUser() == null ){
            throw new BusinessException("NULL user value object");
        }
        
        if(vo.getUser().getId() == null ){
            throw new BusinessException("NULL user ID");
        }        
        
        if(isUserExist(vo.getUser().getId(),vo.getReport().getId())){
            throw new RuleException("EPS_ERU_001");
        }
        
        ReportUserDAO dao = null;
        try {
            dao = (ReportUserDAO) getDAO(ReportUserDAO.class);
            dao.addSecurityUser(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Delete Security User
     * 
     * @param reportId Report Id
     * @param userId User Id
     */
    public void deleteSecurityUser(Long userId,Long reportId){
        // Validate parameters
        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        if (reportId == null) {
            throw new BusinessException("NULL report user ID");
        }            
        
        ReportUserDAO dao = null;
        try {
            dao = (ReportUserDAO) getDAO(ReportUserDAO.class);
            dao.deleteSecurityUser(userId,reportId);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }

    /**
     * Is User Exist
     * 
     * @return true if the user is exist for current report, otherwise return false
     * @param reportId Report Id
     * @param userId User Id
     */
    public boolean isUserExist(Long userId,Long reportId){
        // Validate parameters
        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        if (reportId == null) {
            throw new BusinessException("NULL report user ID");
        }            
        
        ReportUserDAO dao = null;
        try {
            dao = (ReportUserDAO) getDAO(ReportUserDAO.class);
            return dao.isUserExist(userId,reportId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
}