/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mena Emiel  23/02/2011  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.BusinessException;
import ae.eis.eps.dao.TableHeaderDAO;
import ae.eis.eps.vo.TableHeaderVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;

/**
 * Table header business object.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public class TableHeaderHandler extends BusinessObject {
    /*
     * Business Methods
     */
     
     /**
     * Get Column Info
     * 
     * @param fieldId Field Id
     * @param columnCode Column Code
     * @param callerDao Data Access Object
     * 
     * @return TableHeaderVO Table Header Info
     */
    public TableHeaderVO getColumnInfo(Long fieldId, Integer columnCode, DataAccessObject callerDao) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL Filed Id");
        }
        
        if (columnCode == null) {
            throw new BusinessException("NULL Column Code");
        }
        
        TableHeaderDAO dao = (TableHeaderDAO) getDAO(TableHeaderDAO.class, callerDao);
        return dao.getColumnInfo(fieldId, columnCode);
       
    }
}