/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  23/11/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateStepFieldGroupDAO;
import ae.eis.eps.vo.TemplateStepFieldGroupVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

/**
 * Template step-fields group business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepFieldGroupHandler extends BusinessObject {
    /*
     * Methods
     */

    /**
     * Find step-fields groups.
     * 
     * @param stepId Template step ID.
     * @param pageNo Current search page number.
     * @return step-fields groups.
     */
    public SearchPageVO findByStepId(Long stepId, int pageNo) {
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL Step ID");
        }

        TemplateStepFieldGroupDAO dao = null;
        try {
            dao = (TemplateStepFieldGroupDAO) getDAO(TemplateStepFieldGroupDAO.class);
            return dao.findByStepId(stepId, pageNo);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Create new template step-fields group.
     * 
     * @param vo group info.
     * @return new group details.
     */
    public TemplateStepFieldGroupVO create(TemplateStepFieldGroupVO vo) {
        // Validate group info
        if (vo == null) {
            throw new BusinessException("NULL TemplateStepFieldGroupVO parameter");
        }

        if (vo.getTemplateStep() == null) {
            throw new BusinessException(
                "NULL TemplateStepFieldGroupVO.templateStep");
        }

        if (vo.getTemplateStep().getId() == null) {
            throw new RuleException("EPS_TFG_001");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new RuleException("EPS_TFG_002");
        }

        if (isBlankOrNull(vo.getNameEn())) {
            throw new RuleException("EPS_TFG_003");
        }

        if (vo.getOrderNo() == null) {
            throw new RuleException("EPS_TFG_004");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL TemplateStepFieldGroupVO.createdBy");
        }
        
        if (isGroupExist(vo.getTemplateStep().getId(), vo.getOrderNo())) {
            throw new RuleException("EPS_TFG_005");
        }

        if( vo.getLabelWidth() == null ) {
            throw new RuleException("EPS_TFG_009");
        }

        if( vo.getSingleColumnFieldWidth() == null ) {
            throw new RuleException("EPS_TFG_010");
        }
        
        if( vo.getMultiColumnFieldWidth() == null ) {
            throw new RuleException("EPS_TFG_011");
        }        

        TemplateStepFieldGroupDAO dao = null;
        try {
            dao = (TemplateStepFieldGroupDAO) getDAO(TemplateStepFieldGroupDAO.class);
            TemplateStepFieldGroupVO newVO = dao.create(vo);
            
            dao.commit();
            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Check if a step group already exist.
     * 
     * @param stepId Template step ID.
     * @param orderNo Group order number.
     * @return true if a step group already exist.
     */
    public boolean isGroupExist(Long stepId, Integer orderNo) {
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL template step ID");
        }
        
        if (orderNo == null) {
            throw new BusinessException("NULL orderNo");
        }
        
        TemplateStepFieldGroupDAO dao = null;
        try {
            dao = (TemplateStepFieldGroupDAO) getDAO(TemplateStepFieldGroupDAO.class);
            return dao.isGroupExist(stepId, orderNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }

    /**
     * Delete step-fields group.
     * 
     * @param groupId Group ID.
     */
    public void delete(Long groupId) {
        // Validate parameters
        if (groupId == null) {
            throw new BusinessException("Missing groupId parameter");
        }
        
        if(hasGroupFields(groupId)){
            throw new RuleException("EPS_TFG_008");
        }
        
        TemplateStepFieldGroupDAO dao = null;
        try {
            dao = (TemplateStepFieldGroupDAO) getDAO(TemplateStepFieldGroupDAO.class);
            dao.delete(groupId);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Get step-fields group by ID.
     * 
     * @param groupId Group ID.
     * @return Group info.
     */
    public TemplateStepFieldGroupVO getById(Long groupId) {
        // Validate parameters
        if (groupId == null) {
            throw new BusinessException("NULL group ID");
        }
        
        TemplateStepFieldGroupDAO dao = null;
        try {
            dao = (TemplateStepFieldGroupDAO) getDAO(TemplateStepFieldGroupDAO.class);
            return dao.getById(groupId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }

    /**
     * Update step-fields group.
     * 
     * @param vo group info.
     */
    public void update(TemplateStepFieldGroupVO newGroup) {
        // Validate group info
        if (newGroup == null) {
            throw new BusinessException("NULL TemplateStepFieldGroupVO parameter");
        }
        
        if (newGroup.getId() == null) {
            throw new BusinessException("NULL TemplateStepFieldGroupVO.id");
        }

        if (isBlankOrNull(newGroup.getUpdatedBy())) {
            throw new BusinessException("NULL TemplateStepFieldGroupVO.updatedBy");
        }

        if (isBlankOrNull(newGroup.getNameAr())) {
            throw new RuleException("EPS_TFG_002");
        }

        if (isBlankOrNull(newGroup.getNameEn())) {
            throw new RuleException("EPS_TFG_003");
        }

        if (newGroup.getOrderNo() == null) {
            throw new RuleException("EPS_TFG_004");
        }

        // Get old group info
        TemplateStepFieldGroupVO oldGroup = getById(newGroup.getId());
        if (oldGroup == null) {
            throw new BusinessException("Fields group not found, ID=" + 
                newGroup.getId());
        }
        
        // Disabled by Oqba according to Firas
//        if (oldGroup.getOrderNo().intValue() == 1 &&
//            newGroup.getOrderNo().intValue() != 1) {
//            throw new RuleException("EPS_TFG_007");
//        }

        if (oldGroup.getOrderNo().intValue() != newGroup.getOrderNo().intValue()) {
            if (isGroupExist(oldGroup.getTemplateStep().getId(), newGroup.getOrderNo())) {
                throw new RuleException("EPS_TFG_005");
            }
        }

        if( newGroup.getLabelWidth() == null ) {
            throw new RuleException("EPS_TFG_009");
        }

        if( newGroup.getSingleColumnFieldWidth() == null ) {
            throw new RuleException("EPS_TFG_010");
        }
        
        if( newGroup.getMultiColumnFieldWidth() == null ) {
            throw new RuleException("EPS_TFG_011");
        }     

        // Update group
        TemplateStepFieldGroupDAO dao = null;
        try {
            dao = (TemplateStepFieldGroupDAO) getDAO(TemplateStepFieldGroupDAO.class);
            dao.update(newGroup);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Check if group field has fields
     * 
     * @return true if group fields has fields
     * @param groupId group ID
     */
    public boolean hasGroupFields(Long groupId) {
        // Validate parameters
        if (groupId == null) {
            throw new BusinessException("NULL group ID");
        }
        
        TemplateStepFieldGroupDAO dao = null;
        try {
            dao = (TemplateStepFieldGroupDAO) getDAO(TemplateStepFieldGroupDAO.class);
            return dao.hasGroupFields(groupId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
}