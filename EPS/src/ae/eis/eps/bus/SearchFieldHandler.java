/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          02/12/2009  - File created.
 */
 
package ae.eis.eps.bus;

import ae.eis.eps.dao.SearchFieldDAO;
import ae.eis.eps.vo.SearchFieldVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Search field business Object.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public class SearchFieldHandler extends BusinessObject {
    /*
     * Business objects
     */
     
    /** Procedure field-options business object. */
    private TemplateFieldOptionHandler optionsHandler = 
        new TemplateFieldOptionHandler();

    /*
     * Methods
     */
    
    /**
     * Find search fields
     * 
     * @return searchPageVO search page value object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO find(int pageNo, Long templateId){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL templateId");
        }

        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            return dao.find(pageNo, templateId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get template search fields.
     * 
     * @param templateId EPS template ID.
     * @return List of search fields.
     */
    public List getSearchFields(Long templateId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL templateId");
        }

        SearchFieldDAO dao = null;
        try {
            // Get search fields
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            List fieldsList = dao.getSearchFields(templateId);
            
            // Load Combo-Box fields options
            for (int i = 0; fieldsList != null && i < fieldsList.size(); i++) {
                TemplateFieldVO fieldVO = ((SearchFieldVO) fieldsList.get(i))
                                            .getTemplateField();
                if (! fieldVO.isComboBoxField()) {
                    continue;
                }

                fieldVO.setOptionsList(optionsHandler.getFieldOptions(
                    fieldVO.getId()));
            }
            
            // Return search fields
            return fieldsList;
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Update search field 
     * 
     * @param vo search field value object
     */
    public void update(SearchFieldVO vo){
              
        if (vo.getTemplateField().getProcedureTemplate().getId() == null) {
            throw new BusinessException("NULL template ID");
        }
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL Search field ID");
        }
        
        validateUpdate(vo);
        
        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            dao.update(vo);          
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get by ID
     * 
     * @return  search field value object
     * @param searchFieldId search field ID
     */
    public SearchFieldVO getById(Long searchFieldId){
        
        // Validate parameters
        if (searchFieldId == null) {
            throw new BusinessException("NULL search field ID");
        }

        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            return dao.getById(searchFieldId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Creat search field 
     * 
     * @param vo search field value object
     */
    public void create(SearchFieldVO vo){
              
        if (vo.getTemplateField().getProcedureTemplate().getId() == null) {
            throw new BusinessException("NULL template ID");
        }
        
        validateAdd(vo);
        
        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            dao.create(vo);          
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Validate update
     * 
     * @param vo search field value object
     */
    private void validateUpdate(SearchFieldVO vo){
        
        if (vo.getOrderNo()== null) {
            throw new RuleException("EPS_SFI_003");
        }
        if (isOrderNoExists(vo.getTemplateField().getProcedureTemplate().getId(),
                            vo.getOrderNo(),vo.getId())) {
            throw new RuleException("EPS_SFI_012");
        }
        if (vo.getColumnSpan()== null) {
            throw new RuleException("EPS_SFI_005");
        }
        if (vo.getSearchType()== null) {
            throw new RuleException("EPS_SFI_006");
        }
        if (vo.getSearchType().intValue() == SearchFieldVO.SEARCH_TYPE_BETWEEN.intValue()) {
            if(vo.getColumnSpan().intValue() == 1){
                throw new RuleException("EPS_SFI_007");
            }
        }
        if(vo.getTemplateField().getType()!=null){
            if(vo.getTemplateField().getType().intValue() == TemplateFieldVO.TYPE_NUMBER.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_COMBO_BOX.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_CHECK_BOX.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_MOBILE.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_PHONE.intValue()){
               
               if(vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_EQUAL.intValue()){
                   throw new RuleException("EPS_SFI_009");
               }
            }
            if(vo.getTemplateField().getType().intValue() == TemplateFieldVO.TYPE_TEXT.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_TEXT_AREA.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_EMAIL.intValue()){
               
               if(vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_LIKE.intValue() &&
                  vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_EQUAL.intValue()){
                   throw new RuleException("EPS_SFI_010");
               }
            }
            if(vo.getTemplateField().getType().intValue() == TemplateFieldVO.TYPE_DATE.intValue()){
               if(vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_BETWEEN.intValue() &&
                  vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_EQUAL.intValue()){
                   throw new RuleException("EPS_SFI_011");
               }
            }
        }
    }
    
    /**
     * validate business rules before add
     * 
     * @param vo search field value object
     */
    private void validateAdd(SearchFieldVO vo){
        
        if (vo.getTemplateField().getId() == null) {
            throw new RuleException("EPS_SFI_001");
        }
        
        // Check unique search field
        if (isSearchFieldExists(vo.getTemplateField().getId())) {
            throw new RuleException("EPS_SFI_002");
        }
        
        if (vo.getOrderNo()== null) {
            throw new RuleException("EPS_SFI_003");
        }
        
        if (isOrderNoExists(vo.getTemplateField().getProcedureTemplate().getId(),
                            vo.getOrderNo(),vo.getId())) {
            throw new RuleException("EPS_SFI_012");
        }
        
        if (vo.getColumnSpan()== null) {
            throw new RuleException("EPS_SFI_005");
        }
        
        if (vo.getSearchType()== null) {
            throw new RuleException("EPS_SFI_006");
        }
        
        if (vo.getSearchType().intValue() == SearchFieldVO.SEARCH_TYPE_BETWEEN.intValue()) {
            if(vo.getColumnSpan().intValue() == 1){
                throw new RuleException("EPS_SFI_007");
            }
        }
        
        if(vo.getTemplateField().getType()!=null){
            if(vo.getTemplateField().getType().intValue() == TemplateFieldVO.TYPE_NUMBER.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_COMBO_BOX.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_CHECK_BOX.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_MOBILE.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_PHONE.intValue()){
               
               if(vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_EQUAL.intValue()){
                   throw new RuleException("EPS_SFI_009");
               }
            }
            
            if(vo.getTemplateField().getType().intValue() == TemplateFieldVO.TYPE_TEXT.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_TEXT_AREA.intValue()||
               vo.getTemplateField().getType().intValue() ==
                                  TemplateFieldVO.TYPE_EMAIL.intValue()){
               
               if(vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_LIKE.intValue()){
                   throw new RuleException("EPS_SFI_010");
               }
            }
            
            if(vo.getTemplateField().getType().intValue() == TemplateFieldVO.TYPE_DATE.intValue()){
               if(vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_BETWEEN.intValue() &&
                  vo.getSearchType().intValue() != SearchFieldVO.SEARCH_TYPE_EQUAL.intValue()){
                   throw new RuleException("EPS_SFI_011");
               }
            }
        }
    }
    
    /**
     * Delete search field 
     * 
     * @param templateId template ID
     * @param vo search field value object
     */
    public void delete(SearchFieldVO vo,Long templateId){
        
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL templateId");
        }
        if (vo.getId() == null) {
            throw new BusinessException("NULL search field ID");
        }

        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            dao.delete(vo,templateId);          
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is search field exists 
     * 
     * @return true if exist
     * @param fieldId
     */
    public boolean isSearchFieldExists(Long fieldId){
        
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field Id");
        }

        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            return dao.isSearchFieldExists(fieldId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is order number exists 
     * 
     * @return true if exist
     * @param templateId template ID
     * @param orderNo order number
     * @param searchFieldId search field ID
     */
    public boolean isOrderNoExists(Long templateId, Integer orderNo,Long searchFieldId){
        
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        if (orderNo == null) {
            throw new BusinessException("NULL order number");
        }

        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            return dao.isOrderNoExists(templateId,orderNo,searchFieldId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get Maximun Search Field Order Number
     * 
     * @return Maximun Search Field Order Number
     * @param templateId Template Id
     */
    public Integer getMaxSearchFieldOrderNo(Long templateId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        SearchFieldDAO dao = null;
        try {
            dao = (SearchFieldDAO) getDAO(SearchFieldDAO.class);
            return dao.getMaxSearchFieldOrderNo(templateId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
}