/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureAttachmentDAO;
import ae.eis.eps.vo.ProcedureAttachmentVO;
import ae.eis.eps.vo.ProcedureStepActionVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure attachments business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureAttachmentHandler extends BusinessObject {
    /*
     * Business Objects.
     */

    /** Procedure step business object. */
    private ProcedureStepHandler stepHandler = new ProcedureStepHandler();
    
    /** Procedure business object. */
    private ProcedureHandler procedureHandler = new ProcedureHandler();
    

    /*
     * Business Methods
     */

    /**
     * Save attachment.
     * 
     * @param vo Attachment value object.
     */
    public void saveAttachment(ProcedureAttachmentVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure attachment VO");
        }
        
        if (vo.isEmptyBody()) {
            throw new BusinessException("Empty attachment body");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new BusinessException("NULL attachment file name");
        }

        if (isBlankOrNull(vo.getMimeType())) {
            throw new BusinessException("NULL attachment MIME type");
        }
        
        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL username");
        }
        
        if (vo.getProcedure() == null) {
            throw new BusinessException("NULL attachment procedure VO");
        }

        if (vo.getProcedure().getId() == null) {
            throw new BusinessException("NULL attachment procedure ID");
        }

        if (vo.getUser() == null) {
            throw new BusinessException("NULL procedure attachment user VO");
        }

        if (vo.getUser().getId() == null) {
            throw new BusinessException("NULL procedure attachment user ID");
        }
        
        ProcedureStepVO step = stepHandler.getActiveStep(vo.getProcedure().getId());
        Long attachementCount = getAttachmentCount(vo.getProcedure().getId());
                
        if (attachementCount != null &&  
            step != null && step.getMaxAttachmentsCount()!=null &&
            attachementCount.longValue() > step.getMaxAttachmentsCount().intValue()) {
            
            throw new RuleException("BR_ATC_004");
        }     
        if (attachementCount != null && 
            step != null && step.getMinAttachmentsCount()!=null &&
            attachementCount.longValue() <
            step.getMinAttachmentsCount().intValue()) {
            
            throw new RuleException("BR_ATC_005");
        }
        
        ProcedureVO procedure = procedureHandler.getById(vo.getProcedure().getId());

        if (!procedure.isUnderProcessing()) {
            throw new RuleException("EPS_ATC_008");
        }
        
        if( procedure!=null && procedure.getActiveStep()!=null && 
            procedure.getActiveStep().getClaimedBy()!=null &&
            procedure.getActiveStep().getClaimedBy().getId()!=null &&
            ! procedure.getActiveStep().getClaimedBy().getId().equals(vo.getUser().getId()) ){
                
            throw new RuleException("EPS_ATC_009");
        }
            
        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            dao.save(vo);

            // Commit changes
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Get attachment content by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    public ProcedureAttachmentVO getContentById(Long attachmentId) {
        // Validate parameters
        if (attachmentId == null) {
            throw new BusinessException("NULL attachment ID");
        }

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            return dao.getContentById(attachmentId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get procedure requester attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findRequesterAttchments(int pageNo, Long procedureId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            return dao.findRequesterAttchments(pageNo, procedureId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Get end step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findEndStepAttchments(int pageNo, Long procedureId){
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            return dao.findEndStepAttchments(pageNo, procedureId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Get attachment info by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    public ProcedureAttachmentVO getInfoById(Long attachmentId) {
        // Validate parameters
        if (attachmentId == null) {
            throw new BusinessException("NULL attachment ID");
        }

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            return dao.getInfoById(attachmentId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get active step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     * @param userId : user Id.
     */
    public SearchPageVO findStepAttachments(int pageNo, 
                                            Long procedureId, 
                                            Long userId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        // Get procedure info
        ProcedureVO procedureVO = procedureHandler.getById(procedureId);
        if (procedureVO == null) {
            throw new BusinessException("procedure not found, ID: " + procedureId);
        }
        
        // Get allowed actions list
        Long stepId = procedureVO.getActiveStep().getId();
        List actionsList = stepHandler.getStepActions(stepId);
        procedureVO.getActiveStep().setActionsList(actionsList);
        
        ProcedureStepVO stepVO = procedureVO.getActiveStep();

        if (! stepVO.hasViewStepAttachmentsAction()) {
            throw new BusinessException("This step cannot view attachments");
        }

        // Get related attachments
        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            
            // Get all attachments
            if (stepVO.hasViewRequesterAttachmentsAction() &&
                stepVO.hasViewUsersAttachmentsAction()) {
                return dao.findStepAttachments(pageNo, procedureId);
            
            // Get requester attachments
            } else if (stepVO.hasViewRequesterAttachmentsAction()) {
                return dao.findActiveStepRequesterAttchments(pageNo, procedureId, userId);

            // Get users attachments
            } else if (stepVO.hasViewUsersAttachmentsAction()) {
                return dao.findActiveStepUsersAttchments(pageNo, procedureId, userId);

            } else {
                throw new BusinessException("Step cannot view attachments");
            }

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get active step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findActiveStepAttchments(int pageNo, 
                                                 Long procedureId, 
                                                 Long userId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        // Get procedure info
        ProcedureVO procedureVO = procedureHandler.getById(procedureId);
        if (procedureVO == null) {
            throw new BusinessException("procedure not found, ID: " + procedureId);
        }
        
        if (! procedureVO.isUnderProcessing()) {
            throw new BusinessException("procedure is not active, ID: " + procedureId);
        }
        
        // Get allowed actions list
        Long stepId = procedureVO.getActiveStep().getId();
        List actionsList = stepHandler.getStepActions(stepId);
        procedureVO.getActiveStep().setActionsList(actionsList);
        
        ProcedureStepVO stepVO = procedureVO.getActiveStep();

        if (! stepVO.hasViewStepAttachmentsAction()) {
            throw new BusinessException("This step cannot view attachments");
        }

        // Get related attachments
        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            
            // Get all attachments
            if (stepVO.hasViewRequesterAttachmentsAction() &&
                stepVO.hasViewUsersAttachmentsAction()) {
                return dao.findActiveStepAttchments(pageNo, procedureId);
            
            // Get requester attachments
            } else if (stepVO.hasViewRequesterAttachmentsAction()) {
                return dao.findActiveStepRequesterAttchments(pageNo, procedureId, userId);

            // Get users attachments
            } else if (stepVO.hasViewUsersAttachmentsAction()) {
                return dao.findActiveStepUsersAttchments(pageNo, procedureId, userId);

            } else {
                throw new BusinessException("Step cannot view attachments");
            }

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Delete attachment.
     * 
     * @param procedureId Procedure ID.
     * @param attachmentId Attachment ID.
     * @param userId Current user ID.
     */
    public void delete(Long procedureId, Long attachmentId, Long userId) {
        // Validate parameters
        if (attachmentId == null) {
            throw new BusinessException("NULL attachment ID");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        
        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }
        
        validateDeleteAction(procedureId, attachmentId, userId);

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            boolean status = dao.delete(procedureId, attachmentId, userId);
            
            if (status == false) {
                throw new RuleException("EPS_ATC_007");
            }

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Check if the user is allowed to delete this attachment.
     * 
     * @param procedureId Procedure ID.
     * @param attachmentId Attachment ID.
     * @param userId User ID
     */
    public void validateDeleteAction(Long procedureId, Long attachmentId, Long userId){
        // Validate parameters
        if (attachmentId == null) {
            throw new BusinessException("NULL attachment ID");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            int status = dao.isAllowedToDelete(procedureId, attachmentId, userId);
            
            // User is authorized to delete this attachment
            if (status == 0) {
                return;
            }
            
            if (status == 1) {
                throw new RuleException("EPS_ATC_003");

            } else if (status == 2) {
                throw new RuleException("EPS_ATC_006");

            } else {
                throw new RuleException("EPS_ATC_007");
            }

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get Attachment Count
     * 
     * @param procedureId Procedure ID.
     */
    public Long getAttachmentCount(Long procedureId){
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class);
            return dao.getAttachmentCount(procedureId);
        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Copy transaction attachments.
     * 
     * @param transactionVO Transaction Value Object.
     * @param procedureVO Procedure Value Object.
     * @param daoToConnect Data Access Object.
     */
    public void copyTrsAttachments(TransactionVO transactionVO, 
                                   ProcedureVO procedureVO, 
                                   DataAccessObject daoToConnect) {
        // Validate parameters
        if (procedureVO == null || procedureVO.getId() == null) {
            throw new BusinessException("NULL Procedure");
        }
        
        if (transactionVO == null) {
            throw new BusinessException("NULL Transaction");
        }
        
        if (transactionVO.getTransactionAttachments() == null || transactionVO.getTransactionAttachments().size() == 0) {
            throw new BusinessException("NULL Transaction Attachments");
        }
        
        if (transactionVO.getUserProfile() ==null || transactionVO.getUserProfile().getUserId() == null ) {
            throw new BusinessException("NULL Transaction User Id");
        }

        ProcedureAttachmentDAO dao = null;
        try {
            // Get DAO instance
            dao = (ProcedureAttachmentDAO) getDAO(ProcedureAttachmentDAO.class, daoToConnect);
            dao.copyTrsAttachments(transactionVO, procedureVO);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }
    }    
}