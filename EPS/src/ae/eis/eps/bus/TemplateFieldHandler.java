/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   04/06/2009  - File created.
 */

package ae.eis.eps.bus;
 
import ae.eis.eps.dao.TemplateFieldDAO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template step-field data business object.
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public class TemplateFieldHandler extends BusinessObject {
    
    /** Procedure Business Object */  
    private ProcedureHandler procedureHandler = new ProcedureHandler();
    
    /** Template Table Header Business Object */
    private TemplateTableHeaderHandler tableHeaderHandler = new TemplateTableHeaderHandler();
    
    /*
     * Business methods
     */
    
    /**
     * Find Step Fields
     * 
     * @return Search Page Value Object
     * @param stepId stepId
     * @param templateId template Id
     * @param pageNo pagination page Number
     */
    public SearchPageVO find(int pageNo,Long templateId,Long stepId){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL Template Id");
        }

        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.find(pageNo,templateId,stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * get Field By Template Id
     * 
     * @return Template Field Value Object
     * @param fieldId field Id
     */
    public TemplateFieldVO getById(Long fieldId){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field Id");
        }

        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.getById(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * create Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createText(TemplateFieldVO vo){
        // Validate parameters                
        validate(vo);
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }

        if(vo.getSize() == null && vo.isTextField()){
            throw new RuleException("EPS_TPE_003");
        }
        
        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isTextField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }           
      
        return create(vo);
      
    }
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateText(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }  
        
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }

        if(vo.getSize() == null && vo.isTextField()){
            throw new RuleException("EPS_TPE_003");
        }

        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isTextField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }          
        
        update(vo);
     
    }  
    
    /**
     * create Number Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createNumber(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getSize() == null && vo.isNumberField()){
            throw new RuleException("EPS_TPE_003");
        }
        
        // Validate field size
        if (vo.isNumberField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }     
        
        // Validate number fields values        
        if (vo.isNumberField() && vo.getValue() != null &&
            !GlobalUtilities.isLong(vo.getValue())) {
            throw new RuleException("EPS_TPE_013");
        }
        
        return create(vo);
    }
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateNumber(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
    
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }  
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getSize() == null && vo.isNumberField()){
            throw new RuleException("EPS_TPE_003");
        }
        
        // Validate field size
        if (vo.isNumberField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }     
        
        // Validate number fields values        
        if (vo.isNumberField() && vo.getValue() != null &&
            !GlobalUtilities.isLong(vo.getValue())) {
            throw new RuleException("EPS_TPE_013");
        }
        
        update(vo);        
    }
      
    /**
     * create Date Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createDate(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getSize() == null && vo.isNumberField()){
            throw new RuleException("EPS_TPE_003");
        }
                
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }      
            
        // Validate date fields values
        if (vo.isDateField() && vo.getValue() != null &&
            !GlobalUtilities.isValidDate(vo.getValue())) {
            throw new RuleException("EPS_TPE_014");
        }
        
        return create(vo);        
    }
        
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateDate(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
    
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }  
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getSize() == null && vo.isNumberField()){
            throw new RuleException("EPS_TPE_003");
        }
                
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }      
            
        // Validate date fields values
        if (vo.isDateField() && vo.getValue() != null &&
            !GlobalUtilities.isValidDate(vo.getValue())) {
            throw new RuleException("EPS_TPE_014");
        }
        
        update(vo);        
    } 
    
    /**
     * create List Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createList(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }
                                       
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        } 
        
        return create(vo);        
    }
    
    /**
     * create List Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public void updateList(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        } 
        
        update(vo);       
    }
    
    /**
     * create Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createTextArea(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
        
        if(vo.isTextAreaField() && vo.getSize() == null ){
            throw new RuleException("EPS_TPE_003");
        }
        
        if(!isBlankOrNull(vo.getValue()) &&
            vo.getValue().length() > vo.getSize().intValue()){
                throw new RuleException("EPS_TPE_006");
        }
        
        // Validate field size
        if (vo.isTextAreaField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }   
        
        return create(vo);       
    }    
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateTextArea(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
    
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }  
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
        
        if(vo.isTextAreaField() && vo.getSize() == null ){
            throw new RuleException("EPS_TPE_003");
        }
        
        if(!isBlankOrNull(vo.getValue()) &&
            vo.getValue().length() > vo.getSize().intValue()){
                throw new RuleException("EPS_TPE_006");
        }
        
        // Validate field size
        if (vo.isTextAreaField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }   
        
        update(vo);       
    }

    /**
     * create Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createCheckBox(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }

        if(vo.isCheckBoxField() && vo.getSize() == null){
            throw new RuleException("EPS_TPE_003");
        }
        
        if(vo.getSize().intValue() != 1){
            throw new RuleException("EPS_TPE_021");
        }
   
        if( vo.getValue() != null && 
           !vo.getValue().equalsIgnoreCase("1") &&
           !vo.getValue().equalsIgnoreCase("2") ){
            throw new RuleException("EPS_TPE_020"); 
        }
      
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        } 
        
        return create(vo);        
    }     
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateCheckBox(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
    
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        } 
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }

        if(vo.isCheckBoxField() && vo.getSize() == null){
            throw new RuleException("EPS_TPE_003");
        }
        
        if(vo.getSize().intValue() != 1){
            throw new RuleException("EPS_TPE_021");
        }
   
        if( vo.getValue() != null && 
           !vo.getValue().equalsIgnoreCase("1") &&
           !vo.getValue().equalsIgnoreCase("2") ){
            throw new RuleException("EPS_TPE_020"); 
        }
      
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
        
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        } 
        
        update(vo);       
    }    
        
    /**
     * delete Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void delete(Long fieldId){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field Id ");
        }
        
        if(isLinkedToStep(fieldId)) {
            throw new RuleException("EPS_TPE_010");
        }
            
        //  cannot delete field if a procedure instance exists  
        if(procedureHandler.fieldTemplateHasInstance(fieldId)) {
            throw new RuleException("EPS_TPE_025");
        }
        
        //  cannot delete field if there's related table header
        if( tableHeaderHandler.isFieldTemplateHasHeader(fieldId) ) {
            throw new RuleException("EPS_TPE_030");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            dao.delete(fieldId);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }   
    }
    
    /**
     * Is Exist Code
     *  - EPS_TPE_007 
     *  The code must be unique for the procedure
     *  
     * used to check if field code exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param templateId template Id
     * @param code code
     */
    public boolean isExistCode(Long fieldId,Long templateId,Integer code){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        if (code == null) {
            throw new BusinessException("NULL code");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.isExistCode(fieldId,templateId,code);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Exist Order
     *  - EPS_TPE_008 : The order must be unique for the procedure
     *  
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param templateId template Id
     * @param order order
     */
    public boolean isExistOrder(Long fieldId,Long templateId,Integer order){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        if (order == null) {
            throw new BusinessException("NULL order");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.isExistOrder(fieldId,templateId,order);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is Exist Label
     *  - EPS_TPE_009 : The label must be unique for the procedure
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param templateId template Id
     * @param label label
     */
    public boolean isExistLabel(Long fieldId,Long templateId,String label){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        if (label == null) {
            throw new BusinessException("NULL label");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.isExistLabel(fieldId,templateId,label);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is Linked To Step
     *  - EPS_PTE_010
     *  Cannot delete the field because it's linked with steps
     *  
     * @return boolean (true / false)
     * @param fieldId
     */
    public boolean isLinkedToStep(Long fieldId){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field Id");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.isLinkedToStep(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Valid ComboBox Field
     * Cannot add the field which it's type is drop down list because 
     *       it does not have option value
     * -  EPS_TSL_002      
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public List getValidComboBoxFieldSteps(Long templateId){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.getValidComboBoxFieldSteps(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Get By Report Id
     * 
     * @return Search Page Value Object
     * @param reportId Report Id
     */
    public SearchPageVO getByReportId(Long reportId,TemplateFieldVO vo,int pageNo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL report Id");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.getByReportId(reportId,vo,pageNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }              
    }
    
    /**
     * Find option dependent fields.
     * 
     * @param dependentFieldId Dependent field ID.
     * @param label Template field label.
     * @param code Template field code.
     * @param pageNo Search page number.
     * @return List of parent template fields.
     */
    public SearchPageVO findParentOptionFields(Long dependentFieldId, 
                                               String label, 
                                               Integer code, 
                                               int pageNo) {
        // Validate parameters
        if (dependentFieldId == null) {
            throw new BusinessException("NULL dependentFieldId");
        }
        
        if (pageNo < 0) {
            throw new BusinessException("Invalid pageNo: " + pageNo);
        }

        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.findParentOptionFields(dependentFieldId, 
                                             label, 
                                             code, 
                                             pageNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }              
    }

    /**
     * Update option-fields dependencies
     * 
     * @param templateId Procedure template ID.
     * @param username Active user name.
     */    
    public void updateOptionFieldsDependencies(Long templateId, String username) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template ID");
        }
        
        if (isBlankOrNull(username)) {
            throw new BusinessException("Username is mandatory");
        }

        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            dao.updateOptionFieldsDependencies(templateId, username);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }              
    }
    
    /**
     * create Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createMobile(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
                
        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isMobileField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        // validate if valid UAE Mobile
        if(!isBlankOrNull(vo.getValue()) && 
           !GlobalUtilities.isValidUAEMobile(vo.getValue())){
            throw new RuleException("EPS_TPE_022");
        }        
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }    
        
        return create(vo);        
    }
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateMobile(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
    
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }  
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
                
        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isMobileField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        // validate if valid UAE Mobile
        if(!isBlankOrNull(vo.getValue()) && 
           !GlobalUtilities.isValidUAEMobile(vo.getValue())){
            throw new RuleException("EPS_TPE_022");
        }        
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }    
        
        update(vo);       
    }   
    
    /**
     * create Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createPhone(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
                
        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isMobileField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        // validate if valid UAE Phone
        if(!isBlankOrNull(vo.getValue()) && 
           !GlobalUtilities.isValidUAEPhone(vo.getValue())){
            throw new RuleException("EPS_TPE_023");
         }        
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }  
        
        return create(vo);       
    }
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updatePhone(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
            
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }  
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
                
        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isMobileField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        // validate if valid UAE Phone
        if(!isBlankOrNull(vo.getValue()) && 
           !GlobalUtilities.isValidUAEPhone(vo.getValue())){
            throw new RuleException("EPS_TPE_023");
         }        
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }  
        
        update(vo);       
    }
    
    /**
     * create Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createMail(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
                
        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isMobileField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        // validate if valid UAE Phone
        if(!isBlankOrNull(vo.getValue()) && 
           !GlobalUtilities.isValidUAEEmail(vo.getValue())){ 
            throw new RuleException("EPS_TPE_024");
        }        
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }  
        
        return create(vo);       
    }
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateMail(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
    
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }  
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }
                
        if(vo.getValue() != null){
            if(isBlankOrNull(vo.getValue()) && vo.getValue().length() > 0){
                throw new RuleException("EPS_TPE_006");
            }            
        }
        
        // Validate field size
        if (vo.isMobileField() && vo.getValue() != null 
            && vo.getValue().length() > vo.getSize().intValue()) {
            throw new RuleException("EPS_TPE_016");
        }
        
        // validate if valid UAE Phone
        if(!isBlankOrNull(vo.getValue()) && 
           !GlobalUtilities.isValidUAEEmail(vo.getValue())){ 
            throw new RuleException("EPS_TPE_024");
        }        
        
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }  
        
        update(vo);      
    }
    
    /**
     * Find remaining search fields
     * 
     * @return Search Page Value Object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO findRemainingSearchFields(int pageNo,Long templateId){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL Template Id");
        }

        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.findRemainingSearchFields(pageNo,templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Find remaining search result fields
     * 
     * @return Search Page Value Object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO findRemainingSearchResultFields(int pageNo,Long templateId){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL Template Id");
        }

        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.findRemainingSearchResultFields(pageNo,templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Field Related To Active Procedure
     * 
     * @return boolean
     * @param code Field Code
     * @param templateId template Id
     */
    public boolean isFieldRelatedToActiveProcedure(Long templateId, Integer code){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL Field Id");
        }
 
        if (code == null) {
            throw new BusinessException("NULL code");
        }
        
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            return dao.isFieldRelatedToActiveProcedure(templateId,code);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }   
    } 
    
    
    
    /**
     * create Field
     * 
     * @param TemplateFieldVO Template Field Value Object
     */
    public TemplateFieldVO createTable(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);
                
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }    
                
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }     
        
        // in case of table field , maximum row number must be not null and greater than zero
        if( vo.getMaxRowNo() == null  || vo.getMaxRowNo().longValue() <= 0) {
            throw new RuleException("EPS_TPE_027");
        }
        
        // in case of table field , size must be null
        if( vo.getSize() != null) {
            throw new RuleException("EPS_TPE_028");
        }
        
        // in case of table field , value must be null
        if( vo.getValue() != null) {
            throw new RuleException("EPS_TPE_029");
        }
        
        return create(vo);       
    }
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void updateTable(TemplateFieldVO vo){
        // Validate parameters
        validate(vo);

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Field Id");
        }
            
        // procedure template status is active
        if(vo.getProcedureTemplate().getStatus() != null &&
           vo.getProcedureTemplate().getStatus().
           equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)){
           throw new RuleException("EPS_TPE_012");
        }
        
        if(isBlankOrNull(vo.getLabel())){
            throw new RuleException("EPS_TPE_001");
        }

        if(vo.getType() == null){
            throw new RuleException("EPS_TPE_002");
        }    
                
        if(isExistCode(vo.getId(),vo.getProcedureTemplate().getId(),vo.getCode())){
            throw new RuleException("EPS_TPE_007");
        }
                
        if(isExistLabel(vo.getId(),vo.getProcedureTemplate().getId(),vo.getLabel())){
            throw new RuleException("EPS_TPE_009");
        }     
        
        // in case of table field , maximum row number must be not null and greater than zero
        if( vo.getMaxRowNo() == null  || vo.getMaxRowNo().longValue() <= 0) {
            throw new RuleException("EPS_TPE_027");
        }
        
        // in case of table field , size must be null
        if( vo.getSize() != null) {
            throw new RuleException("EPS_TPE_028");
        }
        
        // in case of table field , value must be null
        if( vo.getValue() != null) {
            throw new RuleException("EPS_TPE_029");
        }
        
        update(vo);      
    }
    
    /**
     * Create
     * 
     * @return 
     * @param vo Template Field Value Object
     */
    private TemplateFieldVO create(TemplateFieldVO vo) {
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            TemplateFieldVO fieldVO = dao.create(vo);
            dao.commit();
            return fieldVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Update
     * 
     * @param vo Template Field Value Object
     */
    private void update(TemplateFieldVO vo) {
        TemplateFieldDAO dao = null;
        try {
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);
            dao.update(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Validate
     * 
     * @param vo Template Field Value Object
     */
    private void validate(TemplateFieldVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Field Value Object");
        }
        
        if(vo.getProcedureTemplate() == null){
            throw new BusinessException("NULL Procedure Template Value Object");
        }
        
        if(vo.getProcedureTemplate().getId() == null){
            throw new BusinessException("NULL Procedure Template Id");
        }      
        
        if(vo.getId() != null) {// escape this validation in case of add
            // check if field is related to active procedure
            TemplateFieldVO existFieldVO = getById(vo.getId());
            if( isFieldRelatedToActiveProcedure( vo.getProcedureTemplate().getId(),
                                                        existFieldVO.getCode()) ) {
                if(existFieldVO.getCode() != null && 
                   vo.getCode() != null &&
                   existFieldVO.getCode().intValue() != vo.getCode().intValue()) {
                    throw new RuleException("EPS_TPE_026");    
                }
            }
        }
    }
    
    /**
     * Get Field Id By Code
     * 
     * @param code
     * @param templateId
     * 
     * @return Field Id
     */
    public Long getFieldIdByCode(Integer code,Long templateId){
        if(code == null){
            throw new BusinessException("NULL Field Code");
        }
        
        if(templateId == null){
            throw new BusinessException("NULL Template Id");
        }
        TemplateFieldDAO dao = null;
        
        try {        
            dao = (TemplateFieldDAO) getDAO(TemplateFieldDAO.class);   
            
            return dao.getFieldIdByCode(code,templateId) ;
        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
}