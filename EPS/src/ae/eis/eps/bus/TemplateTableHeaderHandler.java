/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   30/12/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateTableHeaderDAO;
import ae.eis.eps.vo.TemplateTableHeaderVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template Table Header Business Object
 * 
 * @version 1.0
 * @author Tariq Abu Amireh
 */
public class TemplateTableHeaderHandler extends BusinessObject {

    /**
     * Find
     * 
     * @return list of table headers
     * @param fieldId Field Id
     */
    public SearchPageVO find(int pageNo, Long fieldId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field ID");
        }

        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.find(pageNo,fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Get Table Header Maximum Order Number
     * 
     * @return Table Header Maximum Order Number
     * @param fieldId Field Id
     */
    public Integer getTableHeaderMaxOrderNo(Long fieldId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field ID");
        }

        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.getTableHeaderMaxOrderNo(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }
    
    
    /**
     * Get Table Header Maximum Code
     * 
     * @param fieldId Field Id
     * @return Table Header Maximum Code
     */
    public Integer getTableHeaderMaxCode(Long fieldId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field ID");
        }

        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.getTableHeaderMaxCode(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }               
    }
    
    
    /**
     * Add Template Table Header
     * 
     * @return table header id
     * @param vo Template Table Header Value Object
     */
    public Long add(TemplateTableHeaderVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Table Header Value Object ");
        }
        
        if( vo.getTemplateField() == null ) {
            throw new BusinessException("NULL Template Field Value Object ");
        }

        if( vo.getTemplateField().getId() == null ) {
            throw new BusinessException("NULL Template Field ID ");
        }
        
        if( isBlankOrNull(vo.getNameAr()) ) {
            throw new RuleException("EPS_TTH_001");
        }

        if( isBlankOrNull(vo.getNameEn()) ) {
            throw new RuleException("EPS_TTH_002");
        }
        
        if( vo.getOrderNo() == null ) {
            throw new RuleException("EPS_TTH_003");
        }
        
        if( vo.getColumnType() == null ) {
            throw new RuleException("EPS_TTH_004");
        }

        if( vo.getLableWidth() == null ) {
            throw new RuleException("EPS_TTH_005");
        }
         
        if( (vo.getLableWidth().equals(TemplateTableHeaderVO.COLUMN_TYPE_TEXT) || 
             vo.getLableWidth().equals(TemplateTableHeaderVO.COLUMN_TYPE_NUMBER)) &&
             vo.getMaxSize() == null) {
                throw new RuleException("EPS_TTH_006");
        }
                
        if( isArabicNameExist(vo.getNameAr(),vo.getTemplateField().getId(),null) ) {
            throw new RuleException("EPS_TTH_007");
        }

        if( isEnglishNameExist(vo.getNameEn(),vo.getTemplateField().getId(),null) ) {
            throw new RuleException("EPS_TTH_008");
        }
        
        if( isOrderExist(vo.getOrderNo(),vo.getTemplateField().getId(),null) ) {
            throw new RuleException("EPS_TTH_009");
        }
        
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            Long id =  dao.add(vo);
            dao.commit();
            
            return id;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Get By Header Id
     * 
     * @return Template Table Header Value Object
     * @param headerId Header Id
     */
    public TemplateTableHeaderVO getById(Long headerId) {
        // Validate parameters
        if (headerId == null) {
            throw new BusinessException("NULL header ID");
        }

        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.getById(headerId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Update
     * 
     * @param vo Template Table Header Value Object
     */
    public void update(TemplateTableHeaderVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Table Header Value Object ");
        }
                
        if( isBlankOrNull(vo.getNameAr()) ) {
            throw new RuleException("EPS_TTH_001");
        }

        if( isBlankOrNull(vo.getNameEn()) ) {
            throw new RuleException("EPS_TTH_002");
        }
        
        if( vo.getOrderNo() == null ) {
            throw new RuleException("EPS_TTH_003");
        }
        
        if( vo.getColumnType() == null ) {
            throw new RuleException("EPS_TTH_004");
        }

        if( vo.getLableWidth() == null ) {
            throw new RuleException("EPS_TTH_005");
        }
        
        if( (vo.getLableWidth().equals(TemplateTableHeaderVO.COLUMN_TYPE_TEXT) || 
             vo.getLableWidth().equals(TemplateTableHeaderVO.COLUMN_TYPE_NUMBER)) &&
             vo.getMaxSize() == null) {
                throw new RuleException("EPS_TTH_006");
        }
                
        if( isArabicNameExist(vo.getNameAr(),vo.getTemplateField().getId(),vo.getId()) ) {
            throw new RuleException("EPS_TTH_007");
        }

        if( isEnglishNameExist(vo.getNameEn(),vo.getTemplateField().getId(),vo.getId()) ) {
            throw new RuleException("EPS_TTH_008");
        }
        
        if( isOrderExist(vo.getOrderNo(),vo.getTemplateField().getId(),vo.getId()) ) {
            throw new RuleException("EPS_TTH_009");
        }
        
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            dao.update(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Delete
     * 
     * @param headerId Header Id
     */
    public void delete(Long headerId) {
        // Validate parameters
        if (headerId == null) {
            throw new BusinessException("NULL Template Table Header ID ");
        }

        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            dao.delete(headerId);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }     
    }
    
    /**
     * Is Arabic Name Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param nameA Arabic Name
     */
    public boolean isArabicNameExist(String nameA,Long fieldId,Long headerId) {
        // Validate parameters
        if (nameA == null) {
            throw new BusinessException("NULL Search Criteria ");
        }

        if (fieldId == null) {
            throw new BusinessException("NULL Field ID ");
        } 
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.isArabicNameExist(nameA,fieldId,headerId); 

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is English Name Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param field Search Criteria
     */
    public boolean isEnglishNameExist(String nameE,Long fieldId,Long headerId) {
        // Validate parameters
        if (nameE == null) {
            throw new BusinessException("NULL English Name ");
        }

        if (fieldId == null) {
            throw new BusinessException("NULL Field ID ");
        } 
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.isEnglishNameExist(nameE,fieldId,headerId); 

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is Order Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param orderNo order Number
     */
    public boolean isOrderExist(Long orderNo,Long fieldId,Long headerId) {
        // Validate parameters
        if (orderNo == null) {
            throw new BusinessException("NULL Order Number ");
        }

        if (fieldId == null) {
            throw new BusinessException("NULL Field ID ");
        } 
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.isOrderExist(orderNo,fieldId,headerId); 

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }  
    
    /**
     * Field Template Has Header
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     */
    public boolean isFieldTemplateHasHeader(Long fieldId) {
        // Validate parameters

        if (fieldId == null) {
            throw new BusinessException("NULL Field ID ");
        } 
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance 
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.isFieldTemplateHasHeader(fieldId); 

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is Template Tables Has Header
     * 
     * @return boolean ( true / false )
     * @param templateId Template Id
     */
    public boolean isTemplateTablesHasHeader(Long templateId) {
        // Validate parameters

        if (templateId == null) {
            throw new BusinessException("NULL template ID ");
        } 
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance 
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.isTemplateTablesHasHeader(templateId); 

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is Column Mandatory
     * 
     * @param fieldId Field Id
     * @param columnCode Column Code
     * 
     * @return True if the column mandatory
     *         False if the column optional
     */
    public boolean isColumnMandatory(Long fieldId, Integer columnCode) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL Filed Id");
        }
        
        if (columnCode == null) {
            throw new BusinessException("NULL Column Code");
        }
        
        TemplateTableHeaderDAO dao = null;
        try {
            // Get DAO instance 
            dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class);
            return dao.isColumnMandatory(fieldId, columnCode);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }
    
    /**
     * Get Headers
     * 
     * @param fieldCode Field Code
     * @param dao Data Access Object Connection
     * 
     * @return List Of Template Table Header Info
     */
    public List getHeaders(Integer fieldCode, DataAccessObject trsDAO) {
        // Validate parameters
        if (fieldCode == null) {
            throw new BusinessException("NULL Filed Code");
        }
    
        TemplateTableHeaderDAO dao = (TemplateTableHeaderDAO) getDAO(TemplateTableHeaderDAO.class, trsDAO);
        return dao.getHeaders(fieldCode);
    }
}