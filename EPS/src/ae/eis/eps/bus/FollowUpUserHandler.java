/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.FollowUpUserDAO;
import ae.eis.eps.vo.FollowUpUserVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Follow up user business object.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public class FollowUpUserHandler extends BusinessObject {
    /*
     * Business methods
     */

    /**
     * Create new follow up user.
     * 
     * @param vo follow up user value object.
     */
    public void create(FollowUpUserVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL FollowUpUserVO value object");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL FollowUpUserVO CreatedBy username");
        }
        
        if (vo.getProcedureTemplate() == null) {
            throw new BusinessException("NULL procedure template VO");
        }

        if (vo.getProcedureTemplate().getId() == null) {
            throw new BusinessException("NULL ProcedureTemplate Id");
        }

        if (vo.getUser() == null) {
            throw new BusinessException("NULL User VO");
        }

        if (vo.getUser().getId() == null) {
            throw new RuleException("EPS_FUU_001");
        }
        if (isFollowUpUserExists(vo.getUser().getId(),
                            vo.getProcedureTemplate().getId())) {
            throw new RuleException("EPS_FUU_002");
        }
        if (vo.getUser().getStatus() == null) {
            throw new BusinessException("NULL User Status");
        }
        
        if (vo.getUser().getStatus().intValue() != UserVO.STATUS_ACTIVE) {
            throw new RuleException("EPS_FUU_003");
        }

        // Invoke related DAO object
        FollowUpUserDAO dao = null;
        
        try {
            dao = (FollowUpUserDAO) getDAO(FollowUpUserDAO.class);
            dao.create(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Delete Follow up user.
     * 
     * @param vo Follow up user value object.
     */
    public void delete(FollowUpUserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL FollowUpUserVO value object");
        }
        
        if (vo.getProcedureTemplate() == null ) {
            throw new BusinessException("NULL Procedure Template value object");
        }

        if (vo.getProcedureTemplate().getId() == null ) {
            throw new BusinessException("NULL procedure Template Id ");
        }
        if (vo.getUser()== null ) {
            throw new BusinessException("NULL User value object ");
        }
        if (vo.getUser().getId()== null ) {
            throw new BusinessException("NULL User ID ");
        }
        
        // Invoke related DAO object
        FollowUpUserDAO dao = null;
        try {
            dao = (FollowUpUserDAO) getDAO(FollowUpUserDAO.class);
            dao.delete(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Get Follow up user.
     * 
     * @param vo Follow up user value object.
     * @return Follow up user value object created.
     */
    public SearchPageVO find(int pageNo,FollowUpUserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL FollowUpUserVO value object");
        }
        
        if (vo.getProcedureTemplate().getId() == null) {
            throw new BusinessException("NULL Procedure template ID");
        }

        // Invoke related DAO object
        FollowUpUserDAO dao = null;
        try {
            dao = (FollowUpUserDAO) getDAO(FollowUpUserDAO.class);
            return dao.find(pageNo,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Find follow up user
     * 
     * @return Search Page Value Object
     * @param vo Follow Up User VO
     * @param page Number 
     */
    public SearchPageVO findUsers(int pageNo,FollowUpUserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Step Group value object");
        }        
        
        if (vo.getProcedureTemplate().getId() == null) {
            throw new BusinessException("NULL Procedure template ID");
        }
        // Invoke related DAO object
        FollowUpUserDAO dao = null;
        try {
            dao = (FollowUpUserDAO) getDAO(FollowUpUserDAO.class);
            return dao.findUsers(pageNo,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Is follow up user exists
     * 
     * @return true if foolow up user exist
     * @param templateId template ID
     * @param usrId user ID
     */
    public boolean isFollowUpUserExists(Long usrId, Long templateId){
        // Validate parameters
        if (usrId == null) {
            throw new BusinessException("NULL usrId ");
        }
        
        if (templateId == null) {
            throw new BusinessException("NULL template ID");
        }

        // Invoke related DAO object
        FollowUpUserDAO dao = null;
        try {
            dao = (FollowUpUserDAO) getDAO(FollowUpUserDAO.class);
            return dao.isFollowUpUserExists(usrId,templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
}