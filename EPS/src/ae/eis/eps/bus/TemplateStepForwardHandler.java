/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateStepForwardDAO;
import ae.eis.eps.vo.TemplateStepForwardVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure template step forward business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepForwardHandler extends BusinessObject {
    /*
     * Business methods
     */

    /**
     * Create new procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public TemplateStepForwardVO create(TemplateStepForwardVO vo) {
        
        // Validate parameters
        validateForCreate(vo);
                
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            TemplateStepForwardVO newVO = dao.create(vo);

            dao.commit();
            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Create new procedure template step forward.
     * overloaded version to use current transaction
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    protected TemplateStepForwardVO create(TemplateStepForwardVO vo,
                                           DataAccessObject trsDAO) {
        
        // Validate parameters
        validateForCreate(vo);
        
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;

        dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class,trsDAO);
        return dao.create(vo);
    }
    
    /**
     * validate For Create
     * 
     * @param Template Step Forward Value Object
     */
    private void validateForCreate(TemplateStepForwardVO vo){
        
        if (vo == null) {
            throw new BusinessException("NULL template step forward value object");
        }

        if (vo.getActionType() == null) {
            throw new RuleException("EPS_TSF_001");
        }
                
        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL Template step CreatedBy username");
        }
        
        if (vo.getParentStep() == null) {
            throw new BusinessException("NULL TemplateSetpForwardVO.ParentStep");
        }

        if (vo.getParentStep().getId() == null) {
            throw new BusinessException("NULL TemplateSetpForwardVO.ParentStep.Id");
        }

        if (vo.getNextStep() == null) {
            throw new BusinessException("NULL TemplateSetpForwardVO.NextStep");
        }

        if (vo.getNextStep().getId() == null) {
            throw new BusinessException("NULL TemplateSetpForwardVO.NextStep.Id");
        }
        
        if(hasRejectedForward(vo.getParentStep().getId()) &&
           vo.getActionType().equals(new Integer(2))){
                throw new RuleException("EPS_TSF_002",vo.getParentStep().getNameAr());
        } 
        
        if(!isValidForward(vo.getParentStep().getId(),vo.getNextStep().getId())){
            throw new RuleException("EPS_TSF_005");
        }
        
        if(isForwardExist(vo.getParentStep().getId(),vo.getNextStep().getId())){
            throw new RuleException("EPS_TSF_006");
        }
    }

    
    /**
     * Create new procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public void delete(TemplateStepForwardVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step forward value object");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL template step forward Id");
        }
        
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            dao.delete(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    
    /**
     * Delete procedure template step forward.
     * overloaded version to use current transaction
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    protected void delete(TemplateStepForwardVO vo, DataAccessObject trsDAO) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step forward value object");
        }

        if (vo.getParentStep() == null) {
            throw new BusinessException("NULL parent template step value object");
        }

        if (vo.getParentStep().getId() == null) {
            throw new BusinessException("NULL parent template step id ");
        }
        
        if (vo.getNextStep() == null) {
            throw new BusinessException("NULL next template step value object");
        }

        if (vo.getNextStep().getId() == null) {
            throw new BusinessException("NULL next template step id ");
        }
        
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class,trsDAO);
        dao.delete(vo);
    }
    
    /**
     * Delete procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public void deleteByStepId(Long stepId,DataAccessObject trsDAO){
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL Template Step Id  ");
        }
        
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class,trsDAO);
        dao.deleteByStepId(stepId);        
    }
    
    /**
     * Get procedure template step forward.
     * 
     * @param ptsId Procedure template step Id
     * @return Procedure template step forward value object created.
     */
    public SearchPageVO find(int pageNo,Long ptsId){
        // Validate parameters
        if (ptsId == null) {
            throw new BusinessException("NULL template step forward Id");
        }
        
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.find(pageNo,ptsId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Get Sequense Number
     * 
     * @return max of sequense number
     * @param Procedure Template Id
     */
    public Long getSequenseNo(Long ptlId){
        
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.getSequenseNo(ptlId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Has Rejected Forward
     * return TRUE if this step has rejected forward, otherwise return FALSE
     * 
     * @return boolean (true/false)
     * @param stepId
     */
    public boolean hasRejectedForward(Long stepId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.hasRejectedForward(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
   
    /**
     * Has Invalid Forwards
     * The step must have defined next allowed steps
     * -  EPS_PTT_007
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidForwards(Long templateId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.getInvalidForwards(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is Valid Forward
     * Cannot transfer from begin to end
     * -  EPS_TSF_005
     * 
     * @return boolean (true/false)
     * @param parentStepId
     * @param nextStepId
     */
    public boolean isValidForward(Long parentStepId,Long nextStepId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.isValidForward(parentStepId,nextStepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is Forward Exist
     * Cannot transfer to same step twice
     * -  EPS_TSF_006
     * 
     * @return boolean (true/false)
     * @param nextStepId
     * @param parentStepId
     */
    public boolean isForwardExist(Long parentStepId,Long nextStepId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.isForwardExist(parentStepId,nextStepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Find Procedure Forword Steps
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findForwardSteps(int pageNo,TemplateStepVO vo){
        if(vo == null){
            throw new BusinessException("NULL template step value object");
        }
        
        if(vo.getId() == null){
            throw new BusinessException("NULL Step Id");
        }
        
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class); 
            return dao.findForwardSteps(pageNo,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }    
    
    /**
     * Get Rejected Step Forwards
     * The step cannot have more than one allowed next step with action type reject
     *  - EPS_TSF_002
     * 
     * @return Number of Rejected forwards
     * @param templateId Template Id
     */
    public List getRejectedStepForwards(Long templateId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.getRejectedStepForwards(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        } 
    }
    
    /**
     * Is Valid Rejected Forward
     *  - EPS_TSF_003
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public List getInValidRejectedForwardSteps(Long templateId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.getInValidRejectedForwardSteps(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Valid End Step Forward
     * Cannot add allowed nextr step for step has step type end
     *  - EPS_TSF_004
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidEndStepForward(Long templateId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.isValidEndStepForward(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Valid Allowed Step
     * Must Define step forward with action type reject for step type human 
     *              action and has allowed action which is reject
     * -  EPS_TSF_007
     * 
     * @return boolean (true / false)
     * @param templateId
     */
    public List getInValidAllowedStep(Long templateId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.getInValidAllowedStep(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Get Non Approved Forward Steps
     * cannot activate procedure if there is forward step without approved forwards
     *  - EPS_TSF_009
     * 
     * @return list of non approved forward steps
     * @param templateId template Id
     */
    public List getNonApprovedForwardSteps(Long templateId){
        // Invoke related DAO object
        TemplateStepForwardDAO dao = null;
        try {
            dao = (TemplateStepForwardDAO) getDAO(TemplateStepForwardDAO.class);
            return dao.getNonApprovedForwardSteps(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
}