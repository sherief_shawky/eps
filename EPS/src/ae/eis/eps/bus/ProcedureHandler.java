/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 * 
 * 1.01  Moh  Fayek         12/06/2011  - Only one procedure can be created for the same transaction according to the same template.
                                          raise error if there is other procedure has the same transaction ID and has the same code.  
 * 1.02  Moh Fayek          17/07/2012  - TRF-5237                                           
 * 
 * 1.03  Sami Abudayeh      27/06/2012  - Adding find(Long transactionId,int pageNo)    
 * 
 * 1.04  Ahmed Abdelwahab   14/08/2012  - TRF-7413
 * 
 * 
 * 1.05  Ahmed Abdelwahab   21/08/2012  - TRF-7515.
 * 
 * 1.06  Mahmoud Atiyeh     15/05/2013  - Added getProceduresToBeMovedToDeliveryStep() method.
 * 
 * 1.07  Sami Abudayeh      19/09/2013   - Add assignCoordinatorTaskNote(ProcedureVO vo) method
 * 
 * 1.08  Bashar Alnemrawi   24/11/2013   - Add getAllApprovedNOCs() Method.
 */

package ae.eis.eps.bus;


import ae.eis.common.bus.TrafficFileHandler;


import ae.eis.eps.dao.ProcedureDAO;
import ae.eis.eps.vo.EpsVerifySearchVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.ntf.bus.NotificationFacade;
import ae.eis.ntf.bus.NotificationHandler;
import ae.eis.ntf.vo.NotificationVO;


import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile;



import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * Procedure business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.06
 */
public class ProcedureHandler extends BusinessObject {
    /*
     * Business objects 
     */
     
    /** Procedure field business object. */
    private ProcedureFieldHandler fieldsHandler = new ProcedureFieldHandler();
 		    
   
    /** traffic File Handler */
    private TrafficFileHandler trafficFileHandler = new TrafficFileHandler();

    /*
     * Business methods
     */
    
    /**
     * Create procedure DAO using transaction owner DAO instance.
     * 
     * @param trsDAO Transaction owner DAO instance.
     * @return Procedure DAO which uses the same trsDAO transaction.
     */
    private ProcedureDAO getProcedureDAO(DataAccessObject trsDAO) {
        return (ProcedureDAO) getDAO(ProcedureDAO.class, trsDAO);
    }

    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object.
     * @param tempAttachmentsRefNo Temporal attachments reference number.
     * @return Procedure value object created.
     */
    protected void create(ProcedureVO vo, 
                          Long tempAttachmentsRefNo, 
                          DataAccessObject dao) {
        
        // Validate parameters
        validateForCreate(vo);

        if (dao == null) {
            throw new BusinessException("NULL transactional DAO");
        }
        
        // Create new procedure entity
        Long procedureId = getProcedureDAO(dao).create(vo, tempAttachmentsRefNo);
         
         

        // Save new procedure Id
        vo.setId(procedureId);
    }
    
    /**
     * Process user push-back action.
     * 
     * @param vo Procedure step value object.
     */
    protected void pushBack(ProcedureVO vo, DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO");
        }

        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }

        // Save user action
        getProcedureDAO(dao).pushBack(vo);
    }
    
    
    /**
     * Clear Assigned to
     * 
     * @param vo Procedure step value object.
     */
    public void clearAssignTo(ProcedureVO vo, DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO");
        }

        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }

        // Save user action
        getProcedureDAO(dao).clearAssignTo(vo);
    }
     
    /**
     * Clear Assigned to
     * 
     * @param vo Procedure step value object.
     */
    public void clearAssignTo(ProcedureVO vo) {
    
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }
        
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            getProcedureDAO(dao).clearAssignTo(vo);
            dao.commit();
        }
        catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        } 
      
    }

    /**
     * Save user approve action.
     * 
     * @param vo Procedure value object.
     */
    protected void approve(ProcedureVO vo, DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL Transactional DAO instance");
        }

        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (vo.getActiveStep() == null) {
            throw new BusinessException("NULL procedure active step VO");
        }

        if (vo.getActiveStep().getSequenceNo() == null) {
            throw new BusinessException("NULL procedure active step sequence number");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }
        
        if(!isBlankOrNull(vo.getLastNote()) && vo.getLastNote().length() > 1024){ 
            throw new RuleException("EPS_PRD_028");
        }
        
        // Save user action
        getProcedureDAO(dao).approve(vo);
    }
    
    /**
     * Process user reject action.
     * 
     * @param vo Procedure step value object.
     */
    protected void reject(ProcedureVO vo, DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO");
        }

        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }

        if(isBlankOrNull(vo.getLastNote())){
            throw new RuleException("EPS_PRD_023");
        }
        
        if(vo.getLastNote().length() > 1024){ 
            throw new RuleException("EPS_PRD_028");
        }
        
        // Save user action
        getProcedureDAO(dao).reject(vo);
    }
    /**
     * Process user reject action with no security.
     * 
     * @param vo Procedure step value object.
     * @param DataAccessObject dao
     */
    protected void rejectNoSecurity(ProcedureVO vo, DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO");
        }

        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }

        if(isBlankOrNull(vo.getLastNote())){
            throw new RuleException("EPS_PRD_023");
        }
        
        if(vo.getLastNote().length() > 1024){ 
            throw new RuleException("EPS_PRD_028");
        }
        
        // Save user action
        getProcedureDAO(dao).rejectNoSecurity(vo);
    }

    /**
     * Process user approve Completed Procedure action.
     * 
     * @param vo Procedure step value object.
     * @param DataAccessObject dao
     */
    protected void approveCompletedProcedure(ProcedureVO vo, DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO");
        }

        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }
        // Save user action
        getProcedureDAO(dao).approveCompletedProcedure(vo);
    }

    /**
     * Activate next procedure step.
     * 
     * @param procedureId Procedure ID.
     */
    protected void activateNextStep(Long procedureId, 
                                    String username,
                                    DataAccessObject dao) {
        if (isBlankOrNull(username)) {
            throw new BusinessException("NULL username");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }

        if (dao == null) {
            throw new BusinessException("NULL DAO parameter");
        }

        // Invoke related DAO
        getProcedureDAO(dao).activateNextStep(procedureId, username);
    }

    /**
     * Activate next procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param stepSeqNo next step sequence number.
     */
    protected void activateStep(Long procedureId, 
                                Integer stepSeqNo, 
                                String username,
                                    DataAccessObject dao) {
        if (isBlankOrNull(username)) {
            throw new BusinessException("NULL username");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }

        if (stepSeqNo == null) {
            throw new BusinessException("NULL step sequence number");
        }

        if (dao == null) {
            throw new BusinessException("NULL DAO parameter");
        }

        // Invoke related DAO
        getProcedureDAO(dao).activateStep(procedureId, stepSeqNo, username);
    }

    /**
     * Close procedure.
     * 
     * @param procedureId Procedure ID.
     * @param username Current active employee username.
     * @param trsDAO Transaction data access object.
     * @return True if the procedure were closed successfully.
     */
    protected boolean closeProcedure(Long procedureId, 
                                  String username, 
                                  DataAccessObject dao) {
        // Validate parameters
        if (isBlankOrNull(username)) {
            throw new BusinessException("NULL username");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }

        if (dao == null) {
            throw new BusinessException("NULL DAO parameter");
        }

        // Invoke related DAO
        return getProcedureDAO(dao).closeProcedure(procedureId, username);
    } 

    /**
     * Validate procedure info before executing create business logic.
     * 
     * @param vo Procedure value object.
     */
    private void validateForCreate(ProcedureVO vo) {
        if (vo == null) {
            throw new BusinessException("NULL Procedure value object");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("Template CreatedBy username is mandatory");
        }

        if (vo.getTemplate() == null || vo.getTemplate().getId() == null) {
            throw new RuleException("EPS_PRD_002");
        }

        if (vo.getRequester() == null) {
            throw new BusinessException("NULL Procedure.Requester VO");
        }

        if (vo.getRequester().getId() == null) {
            throw new BusinessException("NULL Procedure.Requester.Id");
        }

        if (isBlankOrNull(vo.getRequesterEmail()) && 
            isBlankOrNull(vo.getRequesterMobile())) {
            
            //Throw Rule Exception if SPL Transaction.
//            if (vo.getTransaction() != null && 
//                vo.getTransaction().isSplTransaction()){
//                
//                throw new RuleException("EPS_PRD_001");
//            }
            
            //Check if transactionshipment exist in case of courier delivery mode.

        }

        if(!isBlankOrNull(vo.getRequesterMobile())){
            if(!GlobalUtilities.isValidUAEMobile(vo.getRequesterMobile())){
                throw new RuleException("EPS_PRD_018");            
            }            
        }

        if(!isBlankOrNull(vo.getRequesterEmail())){
            if(!GlobalUtilities.isValidUAEEmail(vo.getRequesterEmail())){
                throw new RuleException("EPS_PRD_019");
            }
        }
        
        //Only one procedure can be created for the same transaction according to the same template.
        //raise error if there is other procedure has the same transaction ID and has the same code.
        if( vo.getTrsId() != null  &&
            hasAnotherProcedure(vo.getTrsId() , vo.getTemplate().getCode())) {
             throw new RuleException("EPS_PRD_025");
         }
    }

    /**
     * Find work list procedures.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @param actionDateFrom Last action date start search.
     * @param actionDateTo Last action date end search.
     * @return Search results.
     */
    public SearchPageVO findWorklistProcedures(
                        int pageNo, ProcedureVO procedureVO, Long userId,
                        Date creationDateFrom, Date creationDateTo,
                        Date actionDateFrom, Date actionDateTo,
                        List searchFields) {
        // Validate parameters
        if (procedureVO == null) {
            throw new BusinessException("NULL Procedure value object");
        }

        if (userId == null) {
            throw new BusinessException("NULL active user ID");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            SearchPageVO searchPage = null;

            searchPage = dao.findWorklistProceduresNew(
                                      pageNo, procedureVO, userId,
                                      creationDateFrom, creationDateTo,
                                      actionDateFrom, actionDateTo,
                                      searchFields);

            // Get extra search results according to template followup settings
            if (procedureVO.getTemplate() != null && procedureVO.getTemplate().getId() != null) {
                Object[] procedures = searchPage.getRecords();
                for (int i = 0; i < procedures.length; i++) {
                    ProcedureStepVO record = (ProcedureStepVO) procedures[i];
                    record.getProcedure().setFieldsList(
                        fieldsHandler.getProcedureSearchResults(
                            record.getProcedure().getId()));
                }
            }

            // Return user requests
            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Search for user procedures requests.
     * 
     * @param vo Procedure value object.
     * @param pageNo Search page number.
     * @param fromDate Start search date.
     * @param toDate End search date.
     * @return Search results.
     */
    public SearchPageVO findUserRequests(int pageNo, 
                                         ProcedureVO vo, 
                                         Date fromDate, 
                                         Date toDate,
                                         List searchFields) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Procedure value object");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL Procedure createdBy");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            // Get user requests
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            SearchPageVO searchPage = dao.findUserRequests(pageNo, vo, 
                                                           fromDate, toDate, 
                                                           searchFields);

            // Get extra search results according to template followup settings
            if (vo.getTemplate().getId() != null ) {
                Object[] procedures = searchPage.getRecords();
                for (int i = 0; i < procedures.length; i++) {
                    ProcedureVO record = (ProcedureVO) procedures[i];
                    record.setFieldsList(
                        fieldsHandler.getProcedureSearchResults(record.getId()));
                }
            }

            // Return user requests
            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get procedure info.
     * 
     * @param procedureId Procedure ID
     * @return Procedure value object.
     */
    public ProcedureVO getById(Long procedureId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getById(procedureId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get procedure template count
     * @return template version number
     * @param templateId template Id
     * @param versionNo version number
     */
    public Integer getTemplateVersionCount(Long templateId,Integer versionNo) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL Template ID");
        }
        
        if (versionNo == null) {
            throw new BusinessException("NULL template version number");
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getTemplateVersionCount(templateId,versionNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Check if this user is allowed to take actions on this procedure.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     * @return true if this user is allowed to take actions on this procedure.
     */
    public boolean isUserActionAllowed(Long procedureId, Long userId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.isUserActionAllowed(procedureId, userId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    

    /**
     * Update procedure info.
     * 
     * @param procedureId Procedure ID.
     * @param notes User nores.
     * @param username Current username.
     */
    protected void update(Long procedureId, String notes, String username,
                          DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL Transactional DAO instance");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(username)) {
            throw new BusinessException("NULL updatedBy username");
        }

        // Save user action
        getProcedureDAO(dao).update(procedureId, notes, username);
    }
    
    /**
     * Update procedure notes.
     * 
     * @param procedureId Procedure ID.
     * @param notes User nores.
     * @param username Current username.
     */
    public void updateNotes(Long procedureId, String notes, String username,
                          DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL Transactional DAO instance");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(username)) {
            throw new BusinessException("NULL updatedBy username");
        }

        // Save user action
        getProcedureDAO(dao).updateNotes(procedureId, notes, username);
    }


    /**
     * Assign task to specific user.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID to be assigned to this task.
     */
    public void assignTask(Long procedureId, Long userId, DataAccessObject dao) {
        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL Transactional DAO instance");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        // Save user action
        getProcedureDAO(dao).assignTask(procedureId, userId);
    }
    
     /**
     * Update Procedure for confirm printing operation (ws) 
     * 
     * @param username external user
     * @param transactionId transaction id
     */
    public void finalizTrsProcedure(Long transactionId, String username,Long userId,DataAccessObject daoParent){
    
         // Validate parameters
        if (transactionId == null) {
            throw new BusinessException("NULL TransactionalId ");
            
        }
         if (isBlankOrNull(username)) {
            throw new BusinessException("NULL username of external user ");
            
        }
        
         if (userId == null) {
            throw new BusinessException("NULL user Id ");
            
        }
        
        
        ProcedureDAO dao = null;
        dao = (ProcedureDAO) getDAO(ProcedureDAO.class,daoParent);
        dao.finalizTrsProcedure(transactionId,username,userId);
    }

    /**
     * Assign task to specific user.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID to be assigned to this task.
     * @param remarks last notes.
     */
    public void assignTask(Long procedureId, Long userId, String remarks) {
        // Validate parameters
        
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }
        
//        if (isBlankOrNull(remarks)) {
//            throw new BusinessException("Remarks is mandatory");
//        }

        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            dao.assignTask(procedureId,userId,remarks);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao); 
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }      
    }
    
    /**
     * Assign task to specific user.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID to be assigned to this task.
     * @param remarks last notes.
     */
    public void assignTask(Long procedureId, Long userId, String remarks, DataAccessObject dao) {
        // Validate parameters
        
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        // Save user action
        getProcedureDAO(dao).assignTask(procedureId, userId, remarks);    
    }
    
    
    
    
    /**
     * Has Instance
     * 
     * @return true if has instance, otherwise return false
     * @param templateId Template Id
     * @param seqNo Sequence Number
     */
    public boolean hasInstance(Long templateId,Integer seqNo){
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.hasInstance(templateId,seqNo);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Field Template Has Instance
     * 
     * @return true if has instance, otherwise return false
     * @param fieldId Field Id
     */
    public boolean fieldTemplateHasInstance(Long fieldId){
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.fieldTemplateHasInstance(fieldId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Find follow-up procedures.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @return Search results.
     */
    public SearchPageVO findFollowupProcedures(int pageNo, 
                                              ProcedureVO procedureVO, 
                                              Long userId,
                                              Date creationDateFrom, 
                                              Date creationDateTo,
                                              List searchFields) {
        // Validate parameters
        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        if (procedureVO == null) {
            throw new BusinessException("NULL Procedure value object");
        }

        if (procedureVO.getTemplate() == null) {
            throw new BusinessException("NULL procedureVO.template");
        }

        if (procedureVO.getTemplate().getId() == null) {
            throw new BusinessException("NULL procedureVO.template.id");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            // Get follow-up procedures
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            SearchPageVO searchPage = dao.findFollowupProcedures(pageNo, 
                                          procedureVO, userId, 
                                          creationDateFrom, creationDateTo,
                                          searchFields);

            // Get extra search results according to template followup settings
            Object[] procedures = searchPage.getRecords();
            for (int i = 0; i < procedures.length; i++) {
                ProcedureVO record = (ProcedureVO) procedures[i];
                record.setFieldsList(
                    fieldsHandler.getProcedureSearchResults(record.getId()));
            }

            // Return user requests
            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get transactional procedure
     * 
     * @param procedureId procedure ID
     * @param callerDAO data access object
     * @return procedure value object
     */
    public ProcedureVO getTransactionalProcedure(Long procedureId,
                                                   DataAccessObject callerDAO) {
        if(procedureId == null) {
            throw new BusinessException("Null Procedure ID Value.");
        }
        ProcedureDAO dao = (ProcedureDAO) getDAO(ProcedureDAO.class, callerDAO);
        return dao.getTransactionalProcedure(procedureId);
    }
    
   
    /**
     * Get procedure info.
     * 
     * @param procedureId Procedure ID
     * @param DataAccessObject dao
     * 
     * @return Procedure value object.
     */
    public ProcedureVO getById(Long procedureId, DataAccessObject dao) {     
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }

        // Validate parameters
        if (dao == null) {
            throw new BusinessException("NULL Transactional DAO instance");
        }

        // Save user action
        return getProcedureDAO(dao).getById(procedureId);
    }    
    
    
    /**
     * Fetch Next Procedure
     * 
     * @param userId User ID
     * @param ctrId Center ID
     * 
     * @return Procedure Value Object
     */
    public ProcedureVO fetchNextProcedure(Long userId  ) {
        
        // Validate Parameter
        if(userId == null) {
            throw new BusinessException("NULL User ID");
        }
               
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.fetchNextProcedure(userId ); 
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Get By Transaction ID
     * 
     * @param transactionId Transaction ID
     * 
     * @return Procedure Value Object.
     */
    public ProcedureVO getByTransactionId(Long transactionId) {
        
        // Validate Parameter
        if(transactionId == null) {
            throw new BusinessException("NULL transaction ID");
        }
        
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getByTransactionId(transactionId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
     
    
    /**
     *getByAuditTransactionInfo  
     * 
     * @param trsId Transaction Id.
     * @param code Transaction code.
     * 
     * @return procedure value object , check if transaction is already audited
     */
    public ProcedureVO getByAuditTransactionInfo(Long trsId ,Long code ) {
        
        // validate parameter
        if(trsId == null) {
            throw new BusinessException("Null trsId ID Value.");
        }
        
        if(code == null) {
            throw new BusinessException("Null code  Value.");
        }
        
         ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getByAuditTransactionInfo(trsId , code);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
        
    }
    
    /**
     * update To Approve.
     * 
     * @param procedureId Procedure ID.
     * @param username Current active employee username.
     */
    public void updateToApprove(Long procedureId, String userName ,DataAccessObject callerDAO) {
        
        // validate parameter
        if(procedureId == null) {
            throw new BusinessException("Null Perocedure Value.");
        }
        
        if(isBlankOrNull(userName)) {
            throw new BusinessException("Null user Name.");
        }
        
         if(callerDAO == null) {
            throw new BusinessException("Null caller DAO.");
        }
        
         ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class ,callerDAO );
            dao.updateToApprove(procedureId ,userName);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }
    }
         
    /**
     * Get transactional procedure
     * 
     * @param procedureId procedure ID
     * @return procedure value object
     */
    public ProcedureVO getTransactionalProcedure(Long procedureId) {
    
        if(procedureId == null) {
            throw new BusinessException("Null Procedure ID Value.");
        }
        ProcedureDAO dao = null;
        try {
        
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getTransactionalProcedure(procedureId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }    
    
    /**
     * Has Active Procedure
     * 
     * @param transactionId  Transaction Id
     * @return true if Has Active Procedure
     */
    public boolean hasActiveEPSProcedure(Long transactionId) {

        // Validate parameters
        if (transactionId == null) {
            throw new BusinessException("NULL Transaction ID");
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.hasActiveEPSProcedure(transactionId);

        } catch (DataAccessException ex) { 
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    
    /**
     * has Another Procedure  
     * 
     * @param trsId Transaction Id.
     * @param code Transaction code.
     * 
     * @return (true) if there is other procedure has the same transaction ID and has the same code (belongs to the same template).
     */
    private  boolean hasAnotherProcedure(Long trsId ,Integer code  ) {
        
        // Validate parameters
        if (trsId == null) {
            throw new BusinessException("NULL Transaction ID");
        }
        
        if (code == null) {
            throw new BusinessException("NULL code");
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class );
            return dao.hasAnotherProcedure(trsId , code );

        } catch (DataAccessException ex) { 
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
        
    }
    
    
    /**
     * Find Procedure
     * 
     * @param transactionId transaction Id
     * @param pageNo page Number
     * 
     * @return SearchPageVO Object
     */
    public SearchPageVO find(Long transactionId,int pageNo){
           
        // Validate parameters
        if (transactionId == null) {
            throw new BusinessException("NULL Transaction ID");
        }
  
        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class );
            return dao.find(transactionId,pageNo);

        } catch (DataAccessException ex) { 
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    
    }
    
    



    /**
     * get List of templates by transaction id.
     *  
     * @param trsId transaction Id.
     * 
     * @return list of templates.
     */
     public List getTemplates(Long trsId){  
        
        //check transaction id;  
        if(trsId == null){
            throw new BusinessException("Null Transaction Id");  
        }
        
        //Get  CML Authority Noc DAO.
        ProcedureDAO dao = null;
        
        try {      
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getTemplates(trsId);  
             
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            this.close(dao);
        }
     }

    

    /**
     * Get all defined Initial NOCs
     *
     * @param templateCodes Template Codes.
     * @param trsId Trs Id.
     * 
     * @return List of Procedure VO
     */
     public List getAllDefinedInitialNOCs(String templateCodes, Long trsId){
        
        //validate Parameter 

        if(isBlankOrNull(templateCodes)){
            
            throw new BusinessException ("Null Template Codes");
        }
        
         if(trsId == null){
            
            throw new BusinessException ("Null Trs Id");
        }
        
        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class );
            return dao.getAllDefinedInitialNOCs(templateCodes,trsId);

        } catch (DataAccessException ex) { 
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
     }
     
    /**
     * Get Procedures To Be Moved To Delivery Step.
     * 
     * @return List Of Procedure Value Objects.
     */
    public List getProceduresToBeMovedToDeliveryStep() {
        ProcedureDAO dao = null;
        
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getProceduresToBeMovedToDeliveryStep();
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get active user procedure count
     * 
     * @return number of active user procedure
     * @param userId user ID
     * @param centerId center ID
     */
    public Integer getActiveUserProcedureCount(Long userId){
        // Validate parameters
        if (userId == null) {
            throw new BusinessException("NULL Procedure value object");
        }
 
        // calling related DAO 
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            Integer count = null;

            count = dao.getActiveUserProcedureCount(userId);

            return count;
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
     /**
     * Check if transaction id Has active Procedure.
     *
     * @param transactionId transaction Id
     * @return true if Transaction Id Has active Procedure.
     */
    public boolean hasProcedure(Long transactionId){
       
        if (transactionId == null)  {
            throw new BusinessException("Invalid Transaction Id.");
        }
      
        ProcedureDAO dao = null;
        
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.hasProcedure(transactionId);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {                  
            throw ex;         
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    
    
    }
    /**
     * Assign Coordinator Task Note
     * 
     * @param vo Procedure VO
     */
    public void assignCoordinatorTaskNote(ProcedureVO vo) {
        if (vo == null)  {
            throw new BusinessException("Procedure VO Null.");
        }
        if (vo.getId() == null)  {
            throw new BusinessException("Procedure Id Null.");
        }
        if (vo.getAssignedToUserId() == null) {
            throw new BusinessException("Assigned To User Id Null.");
        }
        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("Updated By Null.");
        }
        if (vo.getLastHumanTaskStep() == null) {
            throw new BusinessException("Last Human Task Step Null.");
        }
        if (vo.getLastHumanTaskStep().getSequenceNo() == null) {
            throw new BusinessException("Last Human Task Step Id Null.");
        }
        if (isBlankOrNull(vo.getLastNote())) {
            throw new BusinessException("Last Note Null.");
        }
        ProcedureDAO dao = null;
        
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            dao.assignCoordinatorTaskNote(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }      
    }
    
    /**
     * Get Traffic File Active Transactional Procedures
     * 
     * @param trafficId : Traffic File ID
     * @return Traffic File Active Transactional Procedures
     */
    public List getTrafficFileActiveProcedures(Long trafficId) {
       
        if (trafficId == null)  {
            throw new BusinessException("Invalid Traffic File ID.");
        }
      
        ProcedureDAO dao = null;
        
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getTrafficFileActiveProcedures(trafficId);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {                  
            throw ex;         
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    
    /**
    * Get All Proceduers By Transaction ID
    * 
    * @param transactionId Transaction ID
    * 
    * @return List Of Procedure Value Object.
    */
    public List getAllProceduersByTrsId(Long transactionId,Connection conn){
        
        // Validate Parameter
        if(transactionId == null) {
            throw new BusinessException("NULL transaction ID");
        }
        
        ProcedureDAO dao = null;
        dao = (ProcedureDAO) getDAO(ProcedureDAO.class,conn);
        return dao.getAllProceduersByTrsId(transactionId);
        
    }
    
    /**
    * Get All Proceduers By Transaction ID
    * 
    * @param transactionId Transaction ID
    * 
    * @return List Of Procedure Value Object.
    */
    public List getAllProceduersByTrsId(Long transactionId){
        
        // Validate Parameter
        if(transactionId == null) {
            throw new BusinessException("NULL transaction ID");
        }
        
        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class );
            return dao.getAllProceduersByTrsId(transactionId);

        } catch (DataAccessException ex) { 
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get By Transaction ID
     * 
     * @param transactionId Transaction ID
     * @param daoParent dao Parent
     * 
     * @return Procedure Value Object.
     */
    public ProcedureVO getByTransactionId(Long transactionId,DataAccessObject daoParent) {
        
        // Validate Parameter
        if(transactionId == null) {
            throw new BusinessException("NULL transaction ID");
        }
        
        ProcedureDAO dao = null;            
        
          dao = (ProcedureDAO) getDAO(ProcedureDAO.class,daoParent);
          return dao.getByTransactionId(transactionId);
             
    }  
    
    
    /**
     * get Count Of Assigned Eps.
     * 
     * @param userId  : user Id.
     * @param epsCode : eps Code.
     * 
     * @return Count Of Assigned Eps.
     */
    public Integer getCountOfAssignedEps(Long userId ,String epsCode) {
    
        if (userId == null)  {
            throw new BusinessException("NULL user Id.");
        }
        
        if (epsCode == null)  {
            throw new BusinessException("NULL eps Code.");
        }
        
        ProcedureDAO dao = null;
        try {
        
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getCountOfAssignedEps(userId,epsCode);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {                  
            throw ex;         
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
        
    /**
     * get Active Procedures By Traffic File.
     * 
     * @param procedureVO : procedure VO.
     * 
     * @return list of Active Procedures.
     */
    public List getActiveProceduresByTrafficFile(ProcedureVO procedureVO) {
        if ( procedureVO == null || 
             procedureVO.getTrafficFile() == null ||   
             procedureVO.getTrafficFile().getId() == null)  {
            throw new BusinessException("NULL Traffic File Id.");
        }
        
        if (procedureVO.getCode() == null)  {
            throw new BusinessException("NULL eps Code.");
        }
        
        ProcedureDAO dao = null;
        try {
        
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getActiveProceduresByTrafficFile(procedureVO);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {                  
            throw ex;         
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * get All Procedures By Traffic File.
     * 
     * @param procedureVO : procedure VO.
     * 
     * @return list  Procedures.
     */
    public List getAllProceduresByTrafficFile(ProcedureVO procedureVO) {
        if ( procedureVO == null || 
             procedureVO.getTrafficFile() == null ||   
             procedureVO.getTrafficFile().getId() == null)  {
            throw new BusinessException("NULL Traffic File Id.");
        }
        
        if (procedureVO.getCode() == null)  {
            throw new BusinessException("NULL eps Code.");
        }
        
        ProcedureDAO dao = null;
        try {
        
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getAllProceduresByTrafficFile(procedureVO);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {                  
            throw ex;         
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    


    /**
     * has Another Procedure  
     * 
     * @param trsId Transaction Id.
     * @param code Transaction code.
     * 
     * @return (true) if there is other procedure has the same transaction ID and has the same code (belongs to the same template).
     */
    public  boolean hasAnotherProcedures(Long trsId ,Integer code  ) {
          
        // Validate parameters
        if (trsId == null) {
            throw new BusinessException("NULL Transaction ID");
        }
        
        if (code == null) {
            throw new BusinessException("NULL code");
        }

        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class );
            return dao.hasAnotherProcedure(trsId , code );

        } catch (DataAccessException ex) { 
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
        
    }
     
    /**
     * Execute procedure Block.
     * 
     * @param procedureId : Procedure Id.
     * @param caseCode    : Case Code.
     */
    public void executeBlock(Long procedureId,
                             Integer caseCode, 
                             DataAccessObject callerDao) {
        
        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }
        
        if (caseCode == null) {
            throw new BusinessException("NULL Case Code");
        }
        
        if (callerDao == null) {
            throw new BusinessException("NULL EPS DAO");
        }
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class,callerDao);
            dao.executeBlock(procedureId,caseCode);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }
    }
    



    /**
     * Clear Assigned to
     * 
     * @param vo Procedure step value object.
     * @param conn Connection
     */
    public void clearAssignTo(ProcedureVO vo,Connection conn){
        // Validate parameters
        if (conn == null) {
            throw new BusinessException("NULL transactional DAO");
        }

        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("NULL updatedBy username");
        }

        // Invoke Related DAO Object.
        ProcedureDAO dao = null;
        dao = (ProcedureDAO) getDAO(ProcedureDAO.class , conn);
        
        // Call The Real Method.
        dao.clearAssignTo(vo);
    }

    
    
    
     
    
    
    
    /**
     * send Sms For Customer Has Rejected Noc.
     * 
     * @param TransactionVO : Transaction VO.
     * @param procedure  : procedure.
     * @param mobileNo  : mobile No.  (optional)
     * @param callerDao  : caller Dao.
     */
    public void sendSmsForCustomerHasRejectedNoc(TransactionVO transactionVO, 
                                                 ProcedureVO procedure ,
                                                 String mobileNo ) {

        if (procedure == null || procedure.getId() == null) {
            throw new BusinessException("NULL Procedure ID");
        }
        
        if (transactionVO == null || transactionVO.getId() == null ) {
            throw new BusinessException("NULL transaction VO");    
        }
        
        if (transactionVO.getService() == null || transactionVO.getService().getCode() == null) {
            throw new BusinessException("NULL Service Code ");
        }
        
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            Long trsId = transactionVO.getId();
            Integer serviceCode = transactionVO.getService().getCode();
            Long currentProcedureId = procedure.getId();
            Long trafficNo = procedure.getTrafficFile() != null ? procedure.getTrafficFile().getTrafficNo() : null;
            Long trafficId = procedure.getTrafficFile() != null ? procedure.getTrafficFile().getId() : null; 
            
            
            // send Sms 
            String messageCodeForSms = "REJECT_SMS_MESSAGE_FOR_NOC";
            NotificationFacade notificationHandler= new NotificationHandler();
            notificationHandler.sendSmsNotificaton( trafficId , 
                                                    null,
                                                    dao,
                                                    messageCodeForSms,
                                                    mobileNo, 
                                                    getString(trsId), 
                                                    getString(trafficNo),
                                                    trsId,
                                                   NotificationVO.REASON_TYPE_REJECT_NOC_REQUEST);
            
            
        } catch (final Exception ex) {
            // add trace Log
             ex.printStackTrace();
        } finally {
            close(dao);
        }
    }

    
    
    /**
     * Find Eps On Verify Step Template
     * 
     * @param currentPageNo Current Page No
     * @param epsVerifySearchVO Eps Verify Search VO
     * 
     * @return SearchPageVO
     */
    public SearchPageVO findEpsOnVerifyStep( int currentPageNo,
                                             EpsVerifySearchVO epsVerifySearchVO) {

        
        if (epsVerifySearchVO == null) {
            throw new BusinessException("NULL Eps Verify Search VO ");
        }
        
        if (epsVerifySearchVO.getFromDate() == null) {
            throw new BusinessException("NULL From Date");
        }        
        
        if (epsVerifySearchVO.getToDate() == null) {
            throw new BusinessException("NULL To Date ");
        }        
        
        if (epsVerifySearchVO.getUserVO() == null || 
            epsVerifySearchVO.getUserVO().getId() == null) {
            throw new BusinessException("NULL User VO ");
        }         
        
        ProcedureDAO dao = null;
        
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.findEpsOnVerifyStep(currentPageNo,epsVerifySearchVO);
            
        } catch (DataAccessException ex) {
            throw ex;
            
        } catch(BusinessException ex) {                  
            throw ex;         
            
        } catch (Exception ex) {
            throw new BusinessException(ex);
            
        } finally {
            close(dao);
        }
        
    }
    
    /**
     * Close procedure.
     * 
     * @param procedureId Procedure ID.
     * @param username Current active employee username.
     * @return True if the procedure were closed successfully.
     */
    public boolean closeProcedure(Long procedureId, String username) {

        if (isBlankOrNull(username)) {
            throw new BusinessException("NULL username");
        }

        if (procedureId == null) {
            throw new BusinessException("NULL Procedure ID");
        }

        // Invoke related DAO
        ProcedureDAO procedureDAO = null;
        try {
            procedureDAO = (ProcedureDAO) getDAO(ProcedureDAO.class);
            boolean closed = procedureDAO.closeProcedure(procedureId, username);
            procedureDAO.commit();
            return closed;
        } catch (DataAccessException ex) {
            rollback( procedureDAO );
            throw ex;
            
        } catch(BusinessException ex) {   
            rollback( procedureDAO );
            throw ex;         
            
        } catch (Exception ex) {
            rollback( procedureDAO );
            throw new BusinessException(ex);
            
        } finally {
            
            close(procedureDAO);
        }
    }
    
    /**
     * Get EPS procedure which it has been printed and it's active step is printing
     *
     * @return ProcedureVO
     */
    public ProcedureVO getEPSProcedureVO() {
        // Invoke related DAO object
        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            return dao.getEPSProcedureVO();
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Finalize EPS procedure
     *
     * @param procedureId
     * @param userName
     * @param userId
     */
    public void finalizeEPSProcedure(Long procedureId,String userName,Long userId) {
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(userName)) {
            throw new BusinessException("NULL user Name");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        ProcedureDAO dao = null;
        try {
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class);
            dao.finalizeEPSProcedure(procedureId,userName,userId);

            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Update Eps Traffic File Id.
     * 
     * @param transactionId : Transaction Id
     * @param trfId : Traffic File Id
     * @param userName : User Name
     */
    public void updateEpsTrafficFileId(Long procedureId, Long trfId, String userName, Connection conn){
      // Validate parameters
      if (conn == null) {
          throw new BusinessException("Null Connection. ");
      }

      if (procedureId == null) {
          throw new BusinessException("Null Procedure Id. ");
      }

      if (trfId == null) {
          throw new BusinessException("Null Traffic File Id. ");
      }

      if (isBlankOrNull(userName)) {
          throw new BusinessException("Null User Name. ");
      }

      // Invoke Related DAO Object.
      ProcedureDAO dao = null;
      
      try {
          // Get Data Access Object From Bc4j Connection
          dao = (ProcedureDAO) getDAO(ProcedureDAO.class , conn);
          
          // Call The Real Method.
          dao.updateEpsTrafficFileId(procedureId, trfId, userName);
      } catch (DataAccessException ex) {
          throw ex;
      } catch(BusinessException ex) {                  
          throw ex;         
      } catch (Exception ex) {
          throw new BusinessException(ex);
      }
    }

   
    /**
     * Update Eps Status.
     * 
     * @param transactionId : Transaction Id
     * @param status : New Status
     * @param userName : User Name
     * @param conn : Bc4j Connection
     */
    public void updateEpsStatus(Long transactionId , Integer status , String userName , Connection conn){
        // Validate parameters
        if (conn == null) {
            throw new BusinessException("Null Connection. ");
        }

        if (transactionId == null) {
            throw new BusinessException("Null Transaction Id. ");
        }

        if (status == null) {
            throw new BusinessException("Null Status. ");
        }

        if (isBlankOrNull(userName)) {
            throw new BusinessException("Null User Name. ");
        }

        // Invoke Related DAO Object.
        ProcedureDAO dao = null;
        
        try {
            // Get Data Access Object From Bc4j Connection
            dao = (ProcedureDAO) getDAO(ProcedureDAO.class , conn);
            
            // Call The Real Method.
            dao.updateEpsStatus(transactionId, status, userName);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {                  
            throw ex;         
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }
    }
    
}