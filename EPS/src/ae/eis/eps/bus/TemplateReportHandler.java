/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/10/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateReportDAO;
import ae.eis.eps.vo.TemplateReportVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure template reports business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateReportHandler extends BusinessObject {
 
    /** Report Parameter Business Object */
    private ReportParameterHandler reportParameterHandler = new ReportParameterHandler();
 
    /**
     * Get related template reports.
     * 
     * @param templateId EPS Template ID.
     * @param userId Active user ID.
     * @return related template reports.
     */
    public List getByTemplateId(Long templateId, Long userId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        TemplateReportDAO dao = null;
        try {
            dao = (TemplateReportDAO) getDAO(TemplateReportDAO.class);
            return dao.getByTemplateId(templateId, userId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Get related template reports.
     * 
     * @param reportId EPS Remplate ID.
     * @return related template reports.
     */
    public TemplateReportVO getById(Long reportId){
        // Validate parameters
        if (reportId == null) {
            throw new BusinessException("NULL report ID");
        }
        TemplateReportDAO dao = null;
        try {
            dao = (TemplateReportDAO) getDAO(TemplateReportDAO.class);
            return dao.getById(reportId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Find template report
     * 
     * @param templateId EPS Template ID.
     * @param userId Active user ID.
     * @return template reports.
     */
    public SearchPageVO find(Long templateId,int pageNo){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template ID");
        }
        TemplateReportDAO dao = null;
        try {
            dao = (TemplateReportDAO) getDAO(TemplateReportDAO.class);
            return dao.find(templateId,pageNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Add Template Report
     * 
     * @return Template Report Value Object
     * @param vo Template Report Value Object
     */
    public TemplateReportVO add(TemplateReportVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Report Value Object ");
        }

        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL Procedure Template Value Object ");
        }
        
        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL Procedure Template ID ");
        }
        
        /* ********************************************************** */
        /* ******************* Report Name Mandatory **************** */
        /* ********************************************************** */
        if(isBlankOrNull(vo.getNameAr())){
            throw new RuleException("EPS_ETR_001");
        }
        
        /* ********************************************************** */
        /* ******************* Report Type Mandatory **************** */
        /* ********************************************************** */
        if(vo.getReportType() == null){
            throw new RuleException("EPS_ETR_002");
        }
        
        /* ********************************************************** */
        /* ******************* Report Link Mandatory **************** */
        /* ********************************************************** */        
        if(isBlankOrNull(vo.getReportLink())){
            throw new RuleException("EPS_ETR_003");
        }        
        

        TemplateReportDAO dao = null;
        try {
            dao = (TemplateReportDAO) getDAO(TemplateReportDAO.class);
            TemplateReportVO newVO = dao.add(vo);
            dao.commit();
            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Update Template Report
     * 
     * @param vo Template Report Value Object
     */
    public void update(TemplateReportVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Report Value Object ");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Report ID ");
        }
        
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL Procedure Template Value Object ");
        }
        
        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL Procedure Template ID ");
        }
        
        /* ********************************************************** */
        /* ******************* Report Name Mandatory **************** */
        /* ********************************************************** */
        if(isBlankOrNull(vo.getNameAr())){
            throw new RuleException("EPS_ETR_001");
        }
        
        /* ********************************************************** */
        /* ******************* Report Type Mandatory **************** */
        /* ********************************************************** */
        if(vo.getReportType() == null){
            throw new RuleException("EPS_ETR_002");
        }
        
        /* ********************************************************** */
        /* ******************* Report Link Mandatory **************** */
        /* ********************************************************** */        
        if(isBlankOrNull(vo.getReportLink())){
            throw new RuleException("EPS_ETR_003");
        }        
        
        /* ********************************************************** */
        /* *********** Static Report Cannot have parameters ********* */
        /* ********************************************************** */                
        if(vo.isReportTypeStatic()){
            List paramList = reportParameterHandler.getByReportId(vo.getId());
            if(paramList != null && paramList.size() > 0){
                throw new RuleException("EPS_ETR_004");
            }
        }

        TemplateReportDAO dao = null;
        try {
            dao = (TemplateReportDAO) getDAO(TemplateReportDAO.class);
            dao.update(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }              
    }
    
    /**
     * delete report
     * 
     * @param vo Template Report Value Object
     */
    public void delete(TemplateReportVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Report Value Object ");
        }
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Report ID ");
        }

        /* ****************************************************************** */
        /* *** Cannot delete the report because it's related to parameters ** */
        /* ****************************************************************** */     
        if(hasParameters(vo.getId())){
            throw new RuleException("EPS_ETR_005");
        }
        
        TemplateReportDAO dao = null;
        try {
            dao = (TemplateReportDAO) getDAO(TemplateReportDAO.class);
            dao.delete(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Has Parameters
     * 
     * @return true if this report has at least one parameters, 
     *                                                  otherwise return false
     * @param reportId Report ID
     */
    public boolean hasParameters(Long reportId){
        // Validate parameters
        if (reportId == null) {
            throw new BusinessException("NULL Template Report ID ");
        }

        TemplateReportDAO dao = null;
        try {
            dao = (TemplateReportDAO) getDAO(TemplateReportDAO.class);
            return dao.hasParameters(reportId);

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
}