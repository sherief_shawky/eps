/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mena Emiel  23/02/2011  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.TableRowVO;
import ae.eis.eps.vo.TemplateTableHeaderVO;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.BusinessException;
import ae.eis.eps.dao.TableColumnValueDAO;
import ae.eis.eps.vo.TableColumnValueVO;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.web.UserProfile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Table column value business object.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public class TableColumnValueHandler extends BusinessObject {
    /*
     * Business Handlers
     */
     
     /** Template Table Header Business Handler */
     TemplateTableHeaderHandler templateTableHeaderHandler = 
                                        new TemplateTableHeaderHandler();
    
    /*
     * Business Methods
     */
     
    /**
     * Add Table Rows
     * 
     * @param procedureFieldVO Procedure Field VO
     * @param tableRow Table Row
     * @param dao Data Access Object Connection
     */
    public void addTableRows(ProcedureFieldVO procedureFieldVO, List tableRows, 
                       DataAccessObject dao, UserProfile userProfile) {
        // Validate Parameters
        if (procedureFieldVO == null) {
            throw new BusinessException("Null Procedure Field Info");
        }
        
        if (procedureFieldVO == null) {
            throw new BusinessException("Null Procedure Field Info");
        }
        
        if (procedureFieldVO.getId() == null) {
            throw new BusinessException("Null Procedure Field Id");
        }
        
        if (!procedureFieldVO.isTableField()) {
            throw new BusinessException("The Provided Field Is Not a Table");
        }
        
        if (procedureFieldVO.getProcedure() == null) {
            throw new BusinessException("Null Procedure");
        }
        
        if (procedureFieldVO.getProcedure().getId() == null) {
            throw new BusinessException("Null Procedure ID");
        }
        
        // Get Headers For The Provided Table Field
        List tableHeaders = templateTableHeaderHandler.
                            getHeaders(procedureFieldVO.getCode(), dao);
        
        
        if (tableHeaders == null || tableHeaders.size() == 0) {
            throw new BusinessException("No Headers Defined For This Table Field");
        }
        
        
        if (tableRows == null || tableRows.size() == 0) {
            throw new BusinessException("Empty table rows");            
        }
        
        for (int i = 0; i < tableRows.size() ; i++) {
            // Get The First Row
            TableRowVO tableRow = (TableRowVO)tableRows.get(i);
            
            // Get Columns Map In The First Row
            List column = tableRow.getColumns();
            int columnsSize = column.size();
            
            // Initialize List For Collecting All Column Values For The First Row 
            List rowColumns = new ArrayList();
            
            /* Iterate over table headers and try to get 
               the column value for each header from the columns map */
            for (int x = 0; x < tableHeaders.size() ; x++) {
                TemplateTableHeaderVO tableHeader = (TemplateTableHeaderVO)
                                                            tableHeaders.get(x);
                for (int j = 0; j < columnsSize ; j++) {
                    // Get Column Value From Map By Header Code
                    TableColumnValueVO tableColumnValueVO = (TableColumnValueVO)
                                                             column.get(tableHeader.getCode().intValue());
                    // Check if the column mandatory or not
                    if (TemplateTableHeaderVO.COLUMN_MANDATORY.
                        equals(tableHeader.getIsMandatory())) {
                        // Check if the column's value
                        if (tableColumnValueVO == null || 
                            isBlankOrNull(tableColumnValueVO.getValue())) {
                            throw new RuleException("EPS_TTH_010");
                        } 
                        // Add column info in row
                        rowColumns.add(tableColumnValueVO);
                    } 
                }
            }
            
            // Check columns to insert its values for each row
            if (rowColumns == null || rowColumns.size() == 0) {
                throw new BusinessException("No rows found to be added");
            }
            
            insertRowValues(rowColumns, userProfile, 
                            procedureFieldVO.getProcedure().getId(), dao);
            
        }
        
    }
    
    /**
     * Insert Row Values
     * 
     * @param rowColumns Single Row Which Includs Columns
     * @param userProfile User Profile
     * @param procedureId Procedure Id
     * @param callerDAO Data Access Object Connection
     */
    private void insertRowValues(List rowColumns, UserProfile userProfile, 
                                 Long procedureId, DataAccessObject callerDAO) {
        // Validate parameters
        if (rowColumns == null || rowColumns.size() == 0) {
            throw new BusinessException("No rows found to be added");
        }
        
        if (userProfile == null) {
            throw new BusinessException("Null User Profile");
        }
        
        if (userProfile.getUserId() == null) {
            throw new BusinessException("Null User ID");
        }
        
        if (isBlankOrNull(userProfile.getUsername())) {
            throw new BusinessException("Null User Name");
        }
        
        TableColumnValueDAO dao = (TableColumnValueDAO) getDAO(TableColumnValueDAO.class, callerDAO);
        dao.insertRowValues(rowColumns, userProfile, procedureId);
    }
    
    /**
     * Insert Table Row Values
     * 
     * @param rowsList    Rows List
     * @param userProfile User Profile
     * @param procedureId Procedure Id
     * 
     * @return Number Of Update Record
     */
    public Long insertTableRowValues(List rowsList, UserProfile userProfile, 
                                                            Long procedureId, DataAccessObject callerDAO) {
        // Validate parameters
        if (rowsList == null || rowsList.size() == 0) {
            throw new BusinessException("No rows found to be added");
        }
                
        if (userProfile == null) {
            throw new BusinessException("Null User Profile");
        }
        
        if (userProfile.getUserId() == null) {
            throw new BusinessException("Null User ID");
        }
        
        if (isBlankOrNull(userProfile.getUsername())) {
            throw new BusinessException("Null User Name");
        }
        
        if (procedureId == null) {
            throw new BusinessException("Null Proceduer Id");
        }
        
        TableColumnValueDAO dao = (TableColumnValueDAO) getDAO(TableColumnValueDAO.class, callerDAO);
        return dao.insertTableRowValues(rowsList, userProfile, procedureId);        
        
    }      
    
    /**
     * Insert Table Row Values
     * 
     * @param rowsList    Rows List
     * @param userProfile User Profile
     * @param procedureId Procedure Id
     * 
     * @return Number Of Update Record
     */
    public Long insertTableRowValues(List rowsList, 
                                     UserProfile userProfile, 
                                     Long procedureId) {
        // Validate parameters
        if (rowsList == null || rowsList.size() == 0) {
            throw new BusinessException("No rows found to be added");
        }
                
        if (userProfile == null) {
            throw new BusinessException("Null User Profile");
        }
        
        if (userProfile.getUserId() == null) {
            throw new BusinessException("Null User ID");
        }
        
        if (isBlankOrNull(userProfile.getUsername())) {
            throw new BusinessException("Null User Name");
        }
        
        if (procedureId == null) {
            throw new BusinessException("Null Proceduer Id");
        }
        
        TableColumnValueDAO dao = null;
        try {
             dao = (TableColumnValueDAO) getDAO(TableColumnValueDAO.class);
             return dao.insertTableRowValues(rowsList, userProfile, procedureId);        
         } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }  
    
}