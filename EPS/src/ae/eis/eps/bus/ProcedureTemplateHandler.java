/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 * 
 * 1.01  Alaa Salem         04/01/2010  - Adding getByCode() Method.
 * 
 * 1.02  Bashar Alnemrawi   13/09/2012  - updating File.
 * 
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureTemplateDAO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;

import java.util.HashMap;
import java.util.List;

/**
 * Procedure template business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureTemplateHandler extends BusinessObject {
    
    /** Template step business object */
    private TemplateStepHandler stepHandler = new TemplateStepHandler();
    
    /** Template Step Forward business object */
    private TemplateStepForwardHandler stepForwardHandler = 
        new TemplateStepForwardHandler();
    
    /** Template Step Group business object */
    private TemplateStepUserHandler userHandler = 
        new TemplateStepUserHandler();
    
    /** Template Step Notification business object */
    private TemplateStepNotificationHandler ntfHandler = 
        new TemplateStepNotificationHandler();
    
    /** Template Step Field business object */
    private TemplateStepFieldHandler stepFieldHandler = 
        new TemplateStepFieldHandler();
      
    /** Template Field Business Object */
    private TemplateFieldHandler templateFieldHandler = 
        new TemplateFieldHandler();
    
    /** Template Step User Business Object */
    private TemplateStepUserHandler stepUserHandler =
            new TemplateStepUserHandler();
            
    /** Procedure Business Object */
    private ProcedureHandler procedureHandler = new ProcedureHandler();
            
    /** Template Table Header Business Object */
    private TemplateTableHeaderHandler tableHeaderHandler = 
            new TemplateTableHeaderHandler();            
            
    /*
     * Business methods
     */
    
    /**
     * Create new procedure template.
     * 
     * @param vo Procedure template value object.
     * @return Procedure template value object created.
     */
    public ProcedureTemplateVO create(ProcedureTemplateVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template value object");
        }
        
        if (isBlankOrNull(vo.getTemplateNameAr())) {
            throw new RuleException("EPS_PTL_001");
        }

        if (vo.getPriority() == null) {
            throw new RuleException("EPS_PTL_002");
        }

        if (isBlankOrNull(vo.getDescription())) {
            throw new RuleException("EPS_PTL_003");
        }
        
        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("Template CreatedBy username is mandatory");
        }
        
        if(isNameExists(vo.getTemplateNameAr())){
            throw new RuleException("EPS_PTL_007");            
        }
        
        if(vo.getTemplateType()==null){
            throw new RuleException("EPS_PTL_008");            
        }
        
        if(vo.getCode()==null){
            throw new RuleException("EPS_PTL_009");            
        }
        
        if(vo.getTemplateType().equals(ProcedureTemplateVO.TEMPLATE_TYPE_USER)){
            if(vo.getCode().intValue()>8999 || vo.getCode().intValue()<1){
                throw new RuleException("EPS_PTL_010");
            }
        }
        
        if(vo.getTemplateType().equals(ProcedureTemplateVO.TEMPLATE_TYPE_SYSTEM)){
            if(vo.getCode().intValue()<9001 || vo.getCode().intValue()>9999){
                throw new RuleException("EPS_PTL_011");
            }
        }
        
        if(isCodeExists(vo.getCode(),vo.getId())){
            throw new RuleException("EPS_PTL_012");
        }
        
        
        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            ProcedureTemplateVO newVO = dao.create(vo);
            
            // Add Two Default Steps (Begin & End)
            TemplateStepVO begin = new TemplateStepVO();
            begin.setTemplate(newVO);
            begin.setStepType(TemplateStepVO.TYPE_BEGIN);
            begin.setSequenceNo(new Integer(1));
            stepHandler.createSystemStep(begin,dao);
            
            TemplateStepVO end   = new TemplateStepVO();
            end.setTemplate(newVO);
            end.setStepType(TemplateStepVO.TYPE_END);
            end.setSequenceNo(new Integer(999));
            stepHandler.createSystemStep(end,dao);
            
            dao.commit();
            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Update procedure template.
     * 
     * @param vo Procedure template value object.
     * @return Procedure template value object created.
     */
    public void update(ProcedureTemplateVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template value object");
        }
        
        if (isBlankOrNull(vo.getTemplateNameAr())) {
            throw new RuleException("EPS_PTL_001");
        }

        if (vo.getPriority() == null) {
            throw new RuleException("EPS_PTL_002");
        }

        if (isBlankOrNull(vo.getDescription())) {
            throw new RuleException("EPS_PTL_003");
        }        

        if (vo.getStatus().equals(ProcedureTemplateVO.TEMP_STATUS_ACTIVE)) {
            throw new RuleException("EPS_PTL_004");
        }                
        
        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("Template UpdatedBy username is mandatory");
        }
        
        if(vo.getTemplateType()==null){
            throw new RuleException("EPS_PTL_008");            
        }
        
        if(vo.getCode()==null){
            throw new RuleException("EPS_PTL_009");            
        }
        
        if(vo.getTemplateType().equals(ProcedureTemplateVO.TEMPLATE_TYPE_USER)){
            if(vo.getCode().intValue()>8999 || vo.getCode().intValue()<1){
                throw new RuleException("EPS_PTL_010");
            }
        }
        if(vo.getTemplateType().equals(ProcedureTemplateVO.TEMPLATE_TYPE_SYSTEM)){
            if(vo.getCode().intValue()<9001 || vo.getCode().intValue()>9999){
                throw new RuleException("EPS_PTL_011");
            }
        }
        if(isCodeExists(vo.getCode(),vo.getId())){
            throw new RuleException("EPS_PTL_012");
        }
        
        if (vo.getTemplateType().equals(ProcedureTemplateVO.TEMPLATE_TYPE_SYSTEM)) {
            if(stepHandler.hasUsers(vo.getId(),new Integer(1))){
                throw new RuleException("EPS_PTT_025");
            }
        } 

        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            dao.update(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * find Procedure Template
     * 
     * @return SearchPageVO
     * @param pageNo
     * @param vo
     */
    public SearchPageVO find(int pageNo,ProcedureTemplateVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Procedure template value object");
        }
        
        if ( isBlankOrNull(vo.getTemplateNameAr()) &&
             vo.getStatus() == null && vo.getCode() == null &&
             vo.getPriority() == null && vo.getStatusDate() == null &&
             vo.getId() == null ) {
                throw new BusinessException("��� ����� ��� ������ �����");    
        }


        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.find(pageNo,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * get Procedure Template By Id
     * 
     * @return 
     * @param templateId
     */
    public ProcedureTemplateVO getById(Long templateId){
        //validate parameter
        if(templateId == null){
            throw new BusinessException("Null Procedure Template Id ");
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getById(templateId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Get all active templates Templates
     *  
     * @return List of templates
     */
    public List getAvailableTemplates(){
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getAvailableTemplates();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get all Templates
     * 
     * @return list of templates
     */
    public List getAllTemplates(){
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getAllTemplates();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get Requester templates Templates
     * 
     * @return list of templates
     */
    public List getRequesterTemplates(Long userId){
        // Validate Parameter
        if(userId == null){
            throw new BusinessException("Null User Id");
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getRequesterTemplates(userId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Activate procedure template.
     * 
     * @param templateId Procedure template ID.
     * @param username Current active username.
     */
    public void activate(Long templateId, String username, Integer templateType) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template ID");
        }
        
        if (isBlankOrNull(username)) {
            throw new BusinessException("Username is mandatory");
        }
        
        // check if the procedure define steps except begin and end
        if(!stepHandler.hasSteps(templateId)){
            throw new RuleException("EPS_PTL_005");
        }
        
//        // check if step has attachment and step type is not human action
//        if(stepHandler.hasInvalidAttachments(templateId)){
//            throw new RuleException("EPS_PTT_004");
//        }
        
        // check if step didn't define next allowed step
        List forwardDesc = stepForwardHandler.getInvalidForwards(templateId);
        if(forwardDesc != null && forwardDesc.size() > 0){
            throw new RuleException("EPS_PTT_007",forwardDesc.get(0).toString()); 
        }
        
//        // check if step didn't granted security user
//        List securityUserDesc = userHandler.getInvalidSecurityUsersSteps(templateId);
//        if(securityUserDesc != null && securityUserDesc.size() > 0){ 
//            throw new RuleException("EPS_PTT_008",securityUserDesc.get(0).toString());
//        }
        
        // check if step does not define notification message if step type is notification
        List invalidNtfStepList = ntfHandler.getInvalidNotificationSteps(templateId);
        if(invalidNtfStepList != null && invalidNtfStepList.size() > 0){
            TemplateStepVO invalidNtfStepVO = (TemplateStepVO)invalidNtfStepList.get(0);
            if(invalidNtfStepVO != null && !isBlankOrNull(invalidNtfStepVO.getNameAr())){
                throw new RuleException("EPS_PTT_010",invalidNtfStepVO.getNameAr());
            }            
        }
        
        // check if business action step does not define action class name
        List businessActionDesc = stepHandler.getInvalidBusinessActionStep(templateId);
        if(businessActionDesc != null && businessActionDesc.size() > 0){
            throw new RuleException("EPS_PTT_016",businessActionDesc.get(0).toString());
        }
                
        // EPS_TSF_002                
        List rejectedStepForwardList = stepForwardHandler.getRejectedStepForwards(templateId);
        if(rejectedStepForwardList != null && rejectedStepForwardList.size() > 0){
            TemplateStepVO rejectedStepForwardVO = (TemplateStepVO)rejectedStepForwardList.get(0);
            if(rejectedStepForwardVO != null && !isBlankOrNull(rejectedStepForwardVO.getNameAr())){
                throw new RuleException("EPS_TSF_002",rejectedStepForwardVO.getNameAr());
            }            
        }

        // EPS_TSF_003
        List rejectedForwardStepList = stepForwardHandler.getInValidRejectedForwardSteps(templateId);
        if(rejectedForwardStepList != null && rejectedForwardStepList.size() > 0){
            TemplateStepVO rejectedForwardStepVO = (TemplateStepVO)rejectedForwardStepList.get(0);
            if(rejectedForwardStepVO != null && !isBlankOrNull(rejectedForwardStepVO.getNameAr())){
                throw new RuleException("EPS_TSF_003",rejectedForwardStepVO.getNameAr());
            }            
        }


        // EPS_TSF_004
        if(!stepForwardHandler.isValidEndStepForward(templateId) ){
            throw new RuleException("EPS_TSF_004");
        }
        
        /* Must Define step forward with action type reject for step type 
                          human action and has allowed action which is reject */
        List invalidAlloedStepList = stepForwardHandler.getInValidAllowedStep(templateId);                          
        if(invalidAlloedStepList != null && invalidAlloedStepList.size() > 0){
            TemplateStepVO invalidAlloedStepVO = (TemplateStepVO)invalidAlloedStepList.get(0);
            if(invalidAlloedStepVO != null && !isBlankOrNull(invalidAlloedStepVO.getNameAr())){
                throw new RuleException("EPS_TSF_007",invalidAlloedStepVO.getNameAr());    
            }            
        }

        /*  EPS_TSF_009  */
        List nonApprovedList = stepForwardHandler.getNonApprovedForwardSteps(templateId);
        if(nonApprovedList != null && nonApprovedList.size() > 0){
            TemplateStepVO nonApprovedVO = (TemplateStepVO)nonApprovedList.get(0);
            if(nonApprovedVO != null && !isBlankOrNull(nonApprovedVO.getNameAr())){
                throw new RuleException("EPS_TSF_009",nonApprovedVO.getNameAr());                
            }
        }

        // Cannot add fields to steps where step type is notification or business action
        if(!stepFieldHandler.isValidStepField(templateId)){
            throw new RuleException("EPS_TSL_001");
        }
        
        // Cannot add the field which it's type is drop down list because it does not have option value
        List validBoxFieldStepsList = templateFieldHandler.getValidComboBoxFieldSteps(templateId);
        if(validBoxFieldStepsList != null && validBoxFieldStepsList.size() > 0){
           TemplateFieldVO validBoxFieldStepsVO = (TemplateFieldVO)validBoxFieldStepsList.get(0); 
                if(validBoxFieldStepsVO != null && !isBlankOrNull(validBoxFieldStepsVO.getLabel())){
                    throw new RuleException("EPS_TSL_002",validBoxFieldStepsVO.getLabel());
                }               
        }

        /* This check is Careless to To allow the amendment 
         * if the form of inactive to active even if he his end, (TRF-8025)
         */
        // The Field must not be editable if the step type is end
//        if(!stepFieldHandler.isValidEditableStepField(templateId)){
//            throw new RuleException("EPS_TSL_005");    
//        }
        
        // The Field must not be mandatory if the step type is end
        if(!stepFieldHandler.isValidMandatoryStepField(templateId)){
            throw new RuleException("EPS_TSL_006");    
        }
        
        // The Field must be editable if the field is mandatory
        if(stepFieldHandler.isMandatoryAndNotEditable(templateId)){
            throw new RuleException("EPS_TSL_009");    
        }
                     
        if( !tableHeaderHandler.isTemplateTablesHasHeader(templateId) ) {
            throw new RuleException("EPS_TSL_014");
        }
                
//        List inActiveUsersDesc = stepUserHandler.getInActiveUsersSteps(templateId);
//        if(inActiveUsersDesc != null && inActiveUsersDesc.size() > 0){
//            throw new RuleException("EPS_TSG_002",inActiveUsersDesc.get(0).toString());
//        }
         
        if(templateType != null && templateType.intValue()==2){ 
            if(stepHandler.hasUsers(templateId,new Integer(1))){
                throw new RuleException("EPS_PTT_025");
            }
        }               

        // #####################################################################
        // ################ Update option-fields dependencies ##################
        // #####################################################################
        templateFieldHandler.updateOptionFieldsDependencies(templateId, username);

        // #####################################################################
        // ################### Activate procedure template #####################
        // #####################################################################
        
        ProcedureTemplateVO template = getById(templateId);
        Integer versionNo = template.getVersionNo();
        
        if (versionNo.intValue() == 0) {
           versionNo = new Integer(1);
        } else {
            Integer count = procedureHandler.getTemplateVersionCount(templateId, versionNo);
            if (count.intValue() > 0) {
               versionNo = new Integer(versionNo.intValue() + 1); 
            }
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            dao.activate(templateId, username,versionNo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * De-activate procedure template.
     * 
     * @param templateId Procedure template ID.
     * @param username Current active username.
     */
    public void deActivate(Long templateId, String username) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template ID");
        }
        
        if (isBlankOrNull(username)) {
            throw new BusinessException("Username is mandatory");
        }
                        
        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            dao.deActivate(templateId, username);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is Active Procedure
     * return true if procedure template status is active, otherwise return false
     * 
     * @return boolean(true / false)
     */
    public boolean isActiveProcedure(Long templateId){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template id ");
        }
                        
        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.isActiveProcedure(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }                  
    }
    
    
    /**
     * Is Name Exists
     * 
     * @return true if template name already exists, otherwise retrun false
     * @param templateName
     */
    public boolean isNameExists(String templateName){
        // Validate parameters
        if (isBlankOrNull(templateName)) {
            throw new BusinessException("NULL template id ");
        }
                        
        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.isNameExists(templateName);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }

    /**
     * Get user follow up templates.
     * 
     * @param userId User ID.
     * @return user follow up templates.
     */
    public List getUserFollowupTemplates(Long userId) {
        // Validate Parameter
        if (userId == null) {
            throw new BusinessException("Null User ID parameter");
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getUserFollowupTemplates(userId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * get available template code
     * 
     * @return available template code
     */
    public List getTemplateAvailableCode(){
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getTemplateAvailableCode();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Code Exists
     * 
     * @return true if template code already exists, otherwise retrun false
     * @param code template code
     */
    public boolean isCodeExists(Integer code,Long templateId){
        // Validate parameters
        if (code==null) {
            throw new BusinessException("NULL code ");
        }
                        
        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.isCodeExists(code,templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }

    /**
     * get Procedure Template By Template Code
     * 
     * @return Procedure Template VO.
     * @param templateCode Procedure Template Code.
     */
    public ProcedureTemplateVO getByCode(Integer templateCode) {
        //validate parameter
        if(templateCode == null){
            throw new BusinessException("Null Procedure Template Code Value.");
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getByCode(templateCode);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * get Procedure Template By Service Code
     * 
     * @param serviceCode : Procedure Service Code.
     * @return Procedure Template VO.
     */
    public ProcedureTemplateVO getByServiceCode(Integer serviceCode) {
        //validate parameter
        if(serviceCode == null){
            throw new BusinessException("Null Procedure Template Code Value.");
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getByServiceCode(serviceCode);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Check if this template has active Procedure.
     * 
     * @param templateCode Template Code.
     * @param trafficId Traffic File Id.
     * @return true if this template has active Procedure.
     */
    public boolean hasActiveProcedure(Integer templateCode, Long trafficId) {
        
        // validate parameter
        if(templateCode == null){
            throw new BusinessException("Null Procedure Template Code Value.");
        }
        
        if (trafficId == null) {
            throw new BusinessException("Null Traffic File Id Value.");
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.hasActiveProcedure(templateCode, trafficId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get Initial Noc Templates.
     * 
     * @param templates codes .
     * @return List Of Initial Noc Templates.
     */
     public List getInitialNocTemplates(String templateCodes){
     
       ProcedureTemplateDAO dao = null;
     
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            
            return dao.getInitialNocTemplates(templateCodes);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        } 
    
     }
    
    /**
     * get Procedure Template Step By Sequence Number.
     * 
     * @param procedureTemplateId : procedure Template Id.
     * @param sequenceNumber : sequence Number.
     * 
     * @return Template Step VO.
     */
    public TemplateStepVO getProcedureTemplateStepBySequenceNumber(Long procedureTemplateId ,
                                                                   Long sequenceNumber ) {
                                                                   
        // validate parameter
        if(procedureTemplateId == null){
            throw new BusinessException("Null procedure Template Id.");
        }
        
        if (sequenceNumber == null) {
            throw new BusinessException("Null sequence Number.");
        }
        
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getProcedureTemplateStepBySequenceNumber(procedureTemplateId, sequenceNumber);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Get Template By Code.
     * 
     * @return Procedure Template VO.
     */
    public ProcedureTemplateVO getTemplateByCode(Integer code) {
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.getTemplateByCode(code);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }

    /**
     * Is Template Available For User
     * 
     * @param templateId Template Id
     * @param userId User Id
     * 
     * @return true if tempalte available for user
     */
    public boolean isTemplateAvailableForUser(Long templateId,Long userId){
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template id ");
        }
        if (userId == null) {
            throw new BusinessException("NULL user id ");
        }          
        
        // Invoke related DAO object
        ProcedureTemplateDAO dao = null;
        try {
            dao = (ProcedureTemplateDAO) getDAO(ProcedureTemplateDAO.class);
            return dao.isTemplateAvailableForUser(templateId,userId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }       
    }
    

    /**
     * lookup for Procedure Templates based on query search
     * 
     * @param query : text to search
     * @param lang : language for logged-in user
     * @param paramsMap : extra where condetion parameters Map
     * 
     * @return list of Procedure Templates matches this query
     */
    @Override
    public List<ProcedureTemplateVO> lookup(String query, String lang, int pageSize, HashMap<String,String> paramsMap) {
        // Validate Parameters.
        if(isBlankOrNull(query)) {
            throw new BusinessException("Null Procedure Templates Desc Lookup Query.");
        }
        ProcedureTemplateDAO dao = null;
        try {        
            dao = (ProcedureTemplateDAO)getDAO(ProcedureTemplateDAO.class);
            return dao.lookup(query, lang, pageSize, paramsMap);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }  
    }

    /**
     *  validate (id, text) for Procedure Templates recived from lookup componant
     * @param id : id of the Procedure Templates
     * @param text : Procedure Templates description based on language
     * @param lang : language for loogged-in user
     * @return true if data is valid
     */
    @Override
    public boolean validateLookupValue(String id, String text, String lang) {
        // Validate Parameters.
        if(isBlankOrNull(id) || isBlankOrNull(text)) {
            throw new BusinessException("Null Procedure Templates lookup result");
        }
        
        ProcedureTemplateDAO dao = null;
        try {        
            dao = (ProcedureTemplateDAO)getDAO(ProcedureTemplateDAO.class);
            return dao.validateLookupValue(id, text, lang);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }  
    }   
        
}