/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureFieldOptionDAO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import java.util.List;

/**
 * Procedure field-options business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureFieldOptionHandler extends BusinessObject {
    /**
     * Create DAO using transaction owner DAO instance.
     * 
     * @param trsDAO Transaction owner DAO instance.
     * @return DAO which uses the same trsDAO transaction.
     */
    private ProcedureFieldOptionDAO getProcedureFieldOptionDAO(DataAccessObject trsDAO) {
        return (ProcedureFieldOptionDAO) getDAO(ProcedureFieldOptionDAO.class, trsDAO);
    }

    /**
     * Get procedure field options.
     * 
     * @param fieldId procedure field ID.
     * @return procedure field options.
     */
    protected List getFieldOptions(Long fieldId, DataAccessObject dao) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL procedure field ID");
        }

        return getProcedureFieldOptionDAO(dao).getFieldOptions(fieldId);
    }

    /**
     * Get procedure field options.
     * 
     * @param fieldId procedure field ID.
     * @return procedure field options.
     */
    public List getFieldOptions(Long fieldId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL procedure field ID");
        }

        ProcedureFieldOptionDAO dao = null;
        try {
            dao = (ProcedureFieldOptionDAO) getDAO(ProcedureFieldOptionDAO.class);
            return dao.getFieldOptions(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
}