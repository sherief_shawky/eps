/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 */
 
package ae.eis.eps.bus;

import ae.eis.eps.dao.SearchResultDAO;
import ae.eis.eps.vo.SearchResultVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;

/**
 * Search result business Object.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public class SearchResultHandler extends BusinessObject {
    
    /**
     * Find search fields
     * 
     * @return searchPageVO search page value object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO find(int pageNo, Long templateId){
        
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL templateId");
        }

        SearchResultDAO dao = null;
        try {
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            return dao.find(pageNo, templateId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Update search result 
     * 
     * @param vo search result value object
     */
    public void update(SearchResultVO vo){
              
        if (vo.getTemplateField().getProcedureTemplate().getId() == null) {
            throw new BusinessException("NULL template ID");
        }
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL Search field ID");
        }
        
        validateUpdate(vo);
        
        SearchResultDAO dao = null;
        try {
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            dao.update(vo);          
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get by ID
     * 
     * @return  search result value object
     * @param searchResultId search result ID
     */
    public SearchResultVO getById(Long searchResultId){
        
        // Validate parameters
        if (searchResultId == null) {
            throw new BusinessException("NULL search result ID");
        }

        SearchResultDAO dao = null;
        try {
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            return dao.getById(searchResultId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Creat search result 
     * 
     * @param vo search result value object
     */
    public void create(SearchResultVO vo){
        if (vo == null) {
            throw new BusinessException("NULL Search Result Value Object");
        }
        if (vo.getTemplateField() == null) {
            throw new BusinessException("NULL template Field value object");
        }
        if (vo.getTemplateField().getProcedureTemplate() == null) {
            throw new BusinessException("NULL procedure template value object");
        }
        if (vo.getTemplateField().getProcedureTemplate().getId() == null) {
            throw new BusinessException("NULL template ID");
        }
        
        validateAdd(vo);
        
        SearchResultDAO dao = null;
        try {
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            dao.create(vo);          
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Validate update
     * 
     * @param vo search result value object
     */
    private void validateUpdate(SearchResultVO vo){
        
        if (vo.getOrderNo()== null) {
            throw new RuleException("EPS_SFI_003");
        }
        if (isOrderNoExists(vo.getTemplateField().getProcedureTemplate().getId(),
                            vo.getOrderNo(),vo.getId())) {
            throw new RuleException("EPS_SFI_012");
        }
    }
    
    /**
     * validate business rules before add
     * 
     * @param vo search result value object
     */
    private void validateAdd(SearchResultVO vo){
        
        if (vo.getTemplateField().getId() == null) {
            throw new RuleException("EPS_SFI_001");
        }
        if (isSearchResultExists(vo.getTemplateField().getId())) {
            throw new RuleException("EPS_SFI_002");
        }
        if (vo.getOrderNo()== null) {
            throw new RuleException("EPS_SFI_003");
        }
        if (isOrderNoExists(vo.getTemplateField().getProcedureTemplate().getId(),
                            vo.getOrderNo(),vo.getId())) {
            throw new RuleException("EPS_SFI_012");
        }
    }
    
    /**
     * Delete search result 
     * 
     * @param templateId template ID
     * @param vo search result value object
     */
    public void delete(SearchResultVO vo,Long templateId){
        
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL templateId");
        }
        if (vo.getId() == null) {
            throw new BusinessException("NULL search result ID");
        }

        SearchResultDAO dao = null;
        try {
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            dao.delete(vo,templateId);          
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is search result exists 
     * 
     * @return true if exist
     * @param fieldId
     */
    public boolean isSearchResultExists(Long fieldId){
        
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field Id");
        }

        SearchResultDAO dao = null;
        try {
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            return dao.isSearchResultExists(fieldId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is order number exists 
     * 
     * @return true if exist
     * @param templateId template ID
     * @param orderNo order number
     * @param searchResultId search result ID
     */
    public boolean isOrderNoExists(Long templateId, Integer orderNo,Long searchResultId){
        
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        if (orderNo == null) {
            throw new BusinessException("NULL order number");
        }

        SearchResultDAO dao = null;
        try {
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            return dao.isOrderNoExists(templateId,orderNo,searchResultId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get Maximum Search Result Order Number
     * 
     * @return Maximum Search Result Order Number
     * @param templateId Template Id
     */
     public Integer getMaxSearchResultOrderNo(Long templateId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }

        SearchResultDAO dao = null;
        try { 
            dao = (SearchResultDAO) getDAO(SearchResultDAO.class);
            return dao.getMaxSearchResultOrderNo(templateId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) { 
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
     }
}