/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  01/06/2009  - File created.
 * 
 * 1.10  Alaa Salem         05/01/2010  - Adding 2 Overloaded Methods
 *                                        updateProcedureField().
 * 
 * 1.02  Eng. Mohammad Ababneh 13/09/2012 - Add methods (addLog,getTraceLogVO)
 */

package ae.eis.eps.bus;

import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.eps.vo.ProcedureVO;

import ae.eis.util.bus.RuleException;
import javax.servlet.http.HttpServletRequest;

import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.web.UserProfile;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Step business action abstract class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public abstract class BusinessAction extends BusinessObject {
    /*
     * Business Objects
     */
     
    /** Procedure field business object. */
    private ProcedureFieldHandler fieldHandler = new ProcedureFieldHandler();
        /** TraceLogHandler business object. */
    

    /*
     * Abstract methods
     */

    /**
     * Execute business action logic.
     * 
     * @param procedureId Procedure business action.
     * @param activeStepVO Procedure active step value object.
     * @param userProfile User Profile.
     * @return Next step sequence number if the business actionis designed to
     *         select the next step, or null if the business action has only 
     *         one step forward.
     */
    public abstract Integer execute(Long procedureId, 
                                    ProcedureStepVO activeStepVO,
                                    UserProfile userProfile,
                                    DataAccessObject dao);
                                    
    /*
     * Helper methods
     */
     
    /**
     * Get procedure field info.
     * 
     * @param procedureId Procedure ID.
     * @param fieldCode Field code.
     * @param dao Data Access Object
     * @return procedure field info
     */
    protected ProcedureFieldVO getFieldByCode(Long procedureId, 
                                              Integer fieldCode, 
                                              DataAccessObject dao) {
        // Get field info
        return fieldHandler.getByCode(procedureId, fieldCode, dao);
    }

    /**
     * Get procedure fields
     * 
     * @param procedureId Procedure ID.
     * @param codes Fields codes.
     * @return List of procedure fields.
     */
    protected List getProcedureFields(Long procedureId, Integer[] codes, 
                                   DataAccessObject dao) {
        // Get procedure fields
        return fieldHandler.getProcedureFields(procedureId, codes, dao);
    }

    /**
     * Get procedure fields
     * 
     * @param procedureId Procedure ID.
     * @return List of procedure fields.
     */
    protected List getProcedureFields(Long procedureId, DataAccessObject dao) {
        // Get procedure fields
        return fieldHandler.getProcedureFields(procedureId, dao);
    }

    /**
     * Get procedure fields as a "java.util.Map" where:
     * <P> - Key = field code.</P>
     * <P> - Value = ae.rta.eps.vo.ProcedureFieldVO.</P>
     * 
     * @param procedureId Procedure ID.
     * @param codes Fields codes.
     * @return List of procedure fields.
     */
    protected Map getProcedureFieldsMap(Long procedureId, Integer[] codes, 
                                   DataAccessObject dao) {
        // Get procedure fields
        List list = fieldHandler.getProcedureFields(procedureId, codes, dao);
        
        if (list == null || list.size() == 0) {
            return new HashMap();
        }

        Map fieldsMap = new HashMap();
        for (int i = 0; i < list.size(); i++)  {
            ProcedureFieldVO vo = (ProcedureFieldVO) list.get(i);
            fieldsMap.put(vo.getCode(), vo);
        }
        
        return fieldsMap;
    }

    /**
     * Update Procedure Field.
     * 
     * @param dao Data Access Object.
     * @param fieldVO Procedure Field VO.
     */
    protected void updateProcedureField(ProcedureFieldVO fieldVO,
                                                        DataAccessObject dao) {
        fieldHandler.update(fieldVO, dao);
    }
    
    /**
     * Update Procedure Field.
     * 
     * @param dao Data Access Object.
     * @param fieldValue Procedure Field Value.
     * @param fieldCode Procedure Field Code.
     * @param procedureId Procedure ID.
     */
    protected void updateProcedureField(Long procedureId, Integer fieldCode,
                                    String fieldValue, DataAccessObject dao) {
        ProcedureFieldVO fieldVO = getFieldByCode(procedureId, fieldCode, dao);
        if(fieldVO != null) {
            fieldVO.setValue(fieldValue);
        }
        updateProcedureField(fieldVO, dao);
    }
    
    
      
    
     /**
     * Add Log
     * 
     * @param svcCode Service Code
     * @param request The HTTP request we are processing
     * @param ex Exception
     * @throws ae.rta.esrv.util.bus.BusinessException
     */
    public void addLog(Exception ex,ProcedureVO procedureVO ) throws BusinessException {
        //TODO Add new logging implementation
        ex.printStackTrace();
//        TraceLogVO tracelogVO= getTraceLogVO(procedureVO);
//        // Review This Code
//        tracelogVO.setLogSummeryAr(ex.getMessage());
//        tracelogVO.setLogSummeryEn(ex.getMessage());
//        tracelogVO.setLogDetails(ex.getStackTrace().toString());
//        // check on type of Exception
//        if(ex instanceof RuleException) {
//            tracelogVO.setLogType(new Long(TraceLogVO.LOG_TYPE_BUSINESS_RULE.toString()));
//        } else {
//            tracelogVO.setLogType(new Long(TraceLogVO.LOG_TYPE_EXCEPTION.toString()));
//        }
//        // add on table 
//        traceLogHandler.addLog(tracelogVO);
    }
    
   
}