/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  12/05/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureStepDAO;
import ae.eis.eps.vo.ProcedureStepForwardVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.web.UserProfile;
import java.util.List;
import ae.eis.util.dao.jdbc.NamedQuery;
import java.sql.PreparedStatement;

/**
 * Procedure step business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepHandler extends BusinessObject {
     
    /*
     * Constants
     */
     
    /** Unclime Role Code */
    private static String UNCLAIM_ROLE_CODE = "EPS_001";
    
    /*
     * Business Methods
     */
     
    /**
     * Get procedure step DAO instance.
     * 
     * @param trsDAO Transaction owner DAO.
     * @return procedure step DAO instance.
     */
    private ProcedureStepDAO getProcedureStepDAO(DataAccessObject trsDAO) {
        return (ProcedureStepDAO) getDAO(ProcedureStepDAO.class, trsDAO);
    }

    /**
     * Get active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @return active procedure step.
     */
    public ProcedureStepVO getActiveStep(Long procedureId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        ProcedureStepDAO dao = null;
        try {
            dao = (ProcedureStepDAO)getDAO(ProcedureStepDAO.class);
            return dao.getActiveStep(procedureId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }

    /**
     * Get active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param trsDAO Transaction data access object.
     * @return active procedure step.
     */
    protected ProcedureStepVO getActiveStep(Long procedureId, DataAccessObject trsDAO) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        return getProcedureStepDAO(trsDAO).getActiveStep(procedureId);
    }

    /**
     * Get allowed step actions.
     * 
     * @param stepId Step ID.
     * @return allowed step actions.
     */
    public List getStepActions(Long stepId) {
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL procedure step ID");
        }

        ProcedureStepDAO dao = null;
        try {
            dao = (ProcedureStepDAO)getDAO(ProcedureStepDAO.class);
            return dao.getStepActions(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }

    /**
     * Get allowed step actions for active procedure step.
     * 
     * @param procedureId Step ID.
     * @return allowed step actions.
     */
    public List getActiveStepActions(Long procedureId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        ProcedureStepDAO dao = null;
        try {
            dao = (ProcedureStepDAO) getDAO(ProcedureStepDAO.class);
            return dao.getActiveStepActions(procedureId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }

    /**
     * Get step forwards.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    public List getStepForwards(Long stepId) {
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step ID");
        }

        ProcedureStepDAO dao = null;
        try {
            dao = (ProcedureStepDAO) getDAO(ProcedureStepDAO.class);
            return dao.getStepForwards(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }

    /**
     * Get step forwards.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    public List getStepForwards(Long stepId, DataAccessObject dao) {
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step ID");
        }

        return getProcedureStepDAO(dao).getStepForwards(stepId);
    }

    /**
     * Get approve step forwards.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    public List getApproveStepForwards(Long stepId) {
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step ID");
        }

        ProcedureStepDAO dao = null;
        try {
            dao = (ProcedureStepDAO) getDAO(ProcedureStepDAO.class);
            return dao.getApproveStepForwards(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Get approve step forwards with no security.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    public List getApproveStepForwardsNoSecurity(Long prdId , Long activeStepId) {
        // Validate parameters
        if (prdId == null) {
            throw new BusinessException("NULL Procedure ID");
        }
        if (activeStepId == null) {
            throw new BusinessException("NULL Active Step ID");
        }

        ProcedureStepDAO dao = null;
        try {
            dao = (ProcedureStepDAO) getDAO(ProcedureStepDAO.class);
            return dao.getApproveStepForwardsNoSecurity(prdId , activeStepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    protected void claimStep(Long procedureId, 
                             Long userId, 
                             DataAccessObject trsDAO) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        boolean status = getProcedureStepDAO(trsDAO).claimStep(procedureId, userId);
        
        if (status == false) {
            throw new RuleException("EPS_PRD_005");
        }
    }
    
    /**
     * Claim procedure step with no security.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     * @param trsDAO Transaction data access object.
     */
    protected void claimStepNoSecurity(Long procedureId, 
                             Long userId, 
                             DataAccessObject trsDAO) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        boolean status = getProcedureStepDAO(trsDAO).claimStepNoSecurity(procedureId, userId);
        
        if (status == false) {
            throw new RuleException("EPS_PRD_005");
        }
    }

    /**
     * Un-claim active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    protected void unclaimActiveStep(Long procedureId, 
                                     UserProfile userProfile, 
                                     DataAccessObject trsDAO) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        
        if (userProfile == null) {
            throw new BusinessException("NULL user Profile");
        }
        
        if (userProfile.getUserId() == null) {
            throw new BusinessException("NULL user ID");
        }

                            
        // If user has Rule "EPS_001" then slki[p this validation..
        if(!userProfile.isUserInRole(UNCLAIM_ROLE_CODE)) {
        
            boolean status = getProcedureStepDAO(trsDAO).unclaimActiveStep(
                                procedureId, userProfile.getUserId());
            
            if (status == false && !isBlankOrNull(userProfile.getUsername()) &&
                !userProfile.getUsername().equalsIgnoreCase("TRAFFIC_USER")) { 
                throw new RuleException("EPS_PRD_006");
            }
        } else {
            boolean status = getProcedureStepDAO(trsDAO).unclaimActiveForAdmin(
                                procedureId, userProfile.getUserId());
                                
            if (status == false) {
                throw new RuleException("EPS_PRD_027");
            }
        }
    }

    /**
     * Un-claim old procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    protected void unclaimOldStep(Long procedureId, 
                                  Long userId, 
                                  Integer stepSeqNo,
                                  DataAccessObject trsDAO) {
            // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL user ID");
        }

        if (stepSeqNo == null) {
            throw new BusinessException("NULL step sequenece number");
        }

        boolean status = getProcedureStepDAO(trsDAO).unclaimOldStep(
                            procedureId, userId, stepSeqNo);
        
        if (status == false) {
            throw new RuleException("EPS_PRD_006");
        }
}
    public boolean hasForwardStep(Long activeStepId , Integer stepSeq, DataAccessObject dao){
      
      if (activeStepId == null) {
          throw new BusinessException("NULL active Step Id");
      }

      if (stepSeq == null) {
          throw new BusinessException("NULL step Seq");
      }
      
      boolean hasForwad =false;
      
      List<ProcedureStepForwardVO> forwardsList = getStepForwards(activeStepId,dao);
      
      if(forwardsList != null){
        
        for(ProcedureStepForwardVO procStepForwardVo : forwardsList){
          
          if(procStepForwardVo != null){
            
            Integer nextStepSeq = procStepForwardVo.getNextStep().getSequenceNo();
            
            if(nextStepSeq != null && nextStepSeq.intValue() == stepSeq.intValue()){
              
              hasForwad = true;
              
            }
          }
        }
      }
      return hasForwad;
    }
    
    /**
     * Delete User Steps
     * 
     * @param procedureId
     * @param callerDAO Data Access Object 
     */
    public void deleteUserSteps(Long procedureId,DataAccessObject callerDAO){
        
        // Validate parameters
        if (procedureId == null) {
            
            throw new BusinessException("NULL procedure ID");
        }
        
        getProcedureStepDAO(callerDAO).deleteUserSteps(procedureId);
        
    }
    
    /**
     * Add Procedure Steps 
     * 
     * @param procedureId
     * @param userIdAssingTo
     * @param userName
     * @param callerDAO Data Access Object 
     */
    public void addUserSteps(Long procedureId, Long userIdAssingTo, String userName, DataAccessObject callerDAO){
        
        // Validate parameters
        if (procedureId == null) {
            
            throw new BusinessException("NULL procedure ID");
        }
        
        if (userIdAssingTo == null) {
            
            throw new BusinessException("NULL ID Assing TO");
        }
        
        getProcedureStepDAO(callerDAO).addUserSteps(procedureId,userIdAssingTo, userName); 
        
    }
    
    /**
     * Add User Steps without Assing To User ID
     * 
     * @param procedureId
     * @param userName
     * @param callerDAO Data Access Object 
     */
    public void addUserStepsNoAssingUsrId(Long procedureId,  String userName, DataAccessObject callerDAO){
        
        // Validate parameters
        if (procedureId == null) {
            
            throw new BusinessException("NULL procedure ID");
        }
        
        getProcedureStepDAO(callerDAO).addUserStepsNoAssingUsrId(procedureId, userName);
        
    }
    
    /**
     * Add Procedure Steps 
     * 
     * @param userIdAssingTo
     * @param userName
     * @param callerDAO Data Access Object 
     */
    public void addUserSteps(Long userIdAssingTo, String userName, DataAccessObject callerDAO){
        
        // Validate parameters
                if (userIdAssingTo == null) {
            
            throw new BusinessException("NULL ID Assing TO");
        }
        
        getProcedureStepDAO(callerDAO).addUserSteps(userIdAssingTo, userName); 
        
    }
    
    /**
     * Delete User Steps
     * 
     * @param procedureId
     */
    public void deletePrdUserStep(Long procedureId, Long userId, Long stepId, DataAccessObject callerDAO){
        
        // Validate parameters
        if (procedureId == null) {
            
            throw new BusinessException("NULL procedure ID");
        }
        
        getProcedureStepDAO(callerDAO).deletePrdUserStep(procedureId, userId, stepId);
        
    }
}