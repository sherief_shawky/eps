/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 * 1.01  Ali Abdel-Aziz     07/01/2010  - correcting business rule throwing in case
 *                                        of update Step while Proceeudre is Active.
 */

package ae.eis.eps.bus;

import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.TemplateStepDAO;
import ae.eis.eps.vo.TemplateStepActionVO;
import ae.eis.eps.vo.TemplateStepForwardVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure template step business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepHandler extends BusinessObject {
    /*
     * Business Objects
     */

    /** Template Step Action Business Object */
    private TemplateStepActionHandler actionHandler = new TemplateStepActionHandler();

    /** Template Step Forward Business Object */
    private TemplateStepForwardHandler forwardHandler = new TemplateStepForwardHandler();
    
    /** Template step-fields group business object. */
    private TemplateStepFieldGroupHandler groupHandler = new TemplateStepFieldGroupHandler();

    /** Procedure Handler */
    private ProcedureHandler procedureHandler = new ProcedureHandler();
    
    /*
     * Business methods
     */

    /**
     * Create new procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public TemplateStepVO create(TemplateStepVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step value object");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new RuleException("EPS_PTT_001");
        }
        
        if (vo.getStepType() == null) {
            throw new RuleException("EPS_PTT_002");
        }
        
        if (vo.getTaskAssignmentType() == null && vo.isHumanTaskStep()) {
            throw new RuleException("EPS_PTT_010");
        }
        
        if(vo.getStepType().equals(vo.TYPE_BUSINESS_ACTION) &&
           vo.getActionClass() == null ){
            throw new RuleException("EPS_PTT_016");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL Template step CreatedBy username");
        }
        
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL TemplateStep template VO");
        }

        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL TemplateStep template ID");
        }
        
        if(!isValidStepName(vo.getNameAr(),vo.getId(),vo.getTemplate().getId())){
            throw new RuleException("EPS_PTT_019");
        }
        
        if(!vo.getStepType().equals(TemplateStepVO.TYPE_BEGIN) &&
           !vo.getStepType().equals(TemplateStepVO.TYPE_HUMAN_TASK)){
            if(vo.isViewTypeStatic()){
                throw new RuleException("EPS_PTT_021");
            }
        }
                                        
        if(vo.isViewTypeStatic() && isBlankOrNull(vo.getViewLink())){
            throw new RuleException("EPS_PTT_022");
        }
                    
        if(vo.isViewTypeDynamic() && !isBlankOrNull(vo.getViewLink())){
            throw new RuleException("EPS_PTT_023");
        }
        
        // Invoke related DAO object
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            TemplateStepVO newVO = dao.create(vo);
            
            // call related Actions
            if (vo.isHumanTaskStep()) {
                actionHandler.create(newVO, dao);
            }

            dao.commit();

            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Create new procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    protected void createSystemStep(TemplateStepVO vo,DataAccessObject trsDAO) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step value object");
        }

        if (vo.getStepType() == null) {
            throw new BusinessException("NULL template step type");
        }

        if (! vo.isProcedureStartStep() && ! vo.isProcedureEndStep()) {
            throw new BusinessException("Invalid system step type: " + 
                vo.getStepType());
        }
        
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL TemplateStep template VO");
        }
                
        if (isBlankOrNull(vo.getTemplate().getCreatedBy())) {
            throw new BusinessException("NULL Template step CreatedBy username");
        }
        
        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL TemplateStep template ID");
        }

        // Invoke related DAO object
        TemplateStepDAO dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class,trsDAO);
        dao.createSystemStep(vo);
    }
    
    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateStep(TemplateStepVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step value object");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new RuleException("EPS_PTT_001");
        }
        
        if (vo.getStepType() == null) {
            throw new RuleException("EPS_PTT_002");
        }

        if (vo.getTaskAssignmentType() == null) {
            throw new RuleException("EPS_PTT_010");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL Template step CreatedBy username");
        }
        
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL TemplateStep template VO");
        }
        
        // check if step status is active
        if (new Integer(2).equals(vo.getTemplate().getStatus())) {
            throw new RuleException("EPS_PTT_006");
        }
        
        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL TemplateStep template ID");
        }
        
        // check if step type is end                
        if (vo.getStepType().equals(TemplateStepVO.TYPE_END)) {
            throw new RuleException("EPS_PTT_011");
        }

        if(!isValidStepName(vo.getNameAr(),vo.getId(),vo.getTemplate().getId())){
            throw new RuleException("EPS_PTT_019");
        }        

        if(!vo.getStepType().equals(TemplateStepVO.TYPE_BEGIN) &&
           !vo.getStepType().equals(TemplateStepVO.TYPE_HUMAN_TASK)){
            if(vo.isViewTypeStatic()){
                throw new RuleException("EPS_PTT_021");
            }
        }
                                        
        if(vo.isViewTypeStatic() && isBlankOrNull(vo.getViewLink())){
            throw new RuleException("EPS_PTT_022");
        }
                    
        if(vo.isViewTypeDynamic() && !isBlankOrNull(vo.getViewLink())){
            throw new RuleException("EPS_PTT_023");
        }
        
        // cannot change sequence number of step if procedure has instance
        TemplateStepVO existVO = getById(vo.getId());
        if(procedureHandler.hasInstance(vo.getTemplate().getId(),vo.getSequenceNo()) && 
           !existVO.getSequenceNo().equals(vo.getSequenceNo())){
                throw new RuleException("EPS_PTT_024");
        }
        
        // Invoke related DAO object 
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            dao.updateTemplateStep(vo);
             
            // call related Actions
            if(vo.getStepType().equals(vo.TYPE_HUMAN_TASK)){
                actionHandler.delete(vo,dao);
                actionHandler.create(vo,dao);    
            }

            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }

    /**
     * Update procedure template Business Action step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateBusinessStep(TemplateStepVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step value object");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new RuleException("EPS_PTT_001");
        }
        
        if (vo.getStepType() == null) {
            throw new RuleException("EPS_PTT_002");
        }
        
        if (vo.getActionClass() == null) {
            throw new RuleException("EPS_PTT_016");
        }
        
        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL Template step CreatedBy username");
        }
        
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL TemplateStep template VO");
        }
        
        // check if step status is active
        if (new Integer(2).equals(vo.getTemplate().getStatus())) {
            throw new RuleException("EPS_PTT_006");
        }
        
        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL TemplateStep template ID");
        }
                              
        // Invoke related DAO object 
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            dao.updateTemplateBusinessStep(vo);
             
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateNtfStep(TemplateStepVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step value object");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new RuleException("EPS_PTT_001");
        }
        
        if (vo.getStepType() == null) {
            throw new RuleException("EPS_PTT_002");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL Template step CreatedBy username");
        }
        
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL TemplateStep template VO");
        }
        
        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL TemplateStep template ID");
        }
        
        // check if step status is active
        if (new ProcedureTemplateHandler().getById(vo.getTemplate().getId()).isActiveTemplate()) {
            throw new RuleException("EPS_PTT_006");
        }
        
        // check if step type is end                
        if (vo.getStepType().equals(TemplateStepVO.TYPE_END)) {
            throw new RuleException("EPS_PTT_011");
        }
                   
        if(!isValidStepName(vo.getNameAr(),vo.getId(),vo.getTemplate().getId())){
            throw new RuleException("EPS_PTT_019");
        }
                      
        // Invoke related DAO object 
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            dao.updateTemplateNtfStep(vo);
             
            // call related Actions
            if(vo.getStepType().equals(vo.TYPE_HUMAN_TASK)){
                actionHandler.delete(vo,dao);
                actionHandler.create(vo,dao);    
            }

            if (vo.getStepType().equals(vo.TYPE_NOTIFICATION) || 
                vo.getStepType().equals(vo.TYPE_BEGIN)){
                TemplateStepVO nextStep = null;
                if (vo.getForwardslist() != null && vo.getForwardslist().size() > 0) {
                    nextStep = (TemplateStepVO) vo.getForwardslist().get(0);    
                }                
                
                int totalRecords = 0;
                SearchPageVO searchPageVO = forwardHandler.find(0,vo.getId());
                SearchPageVO existSearchPageVO = forwardHandler.find(0,vo.getId());
                totalRecords = searchPageVO.getTotalRecords();
                                
                if (totalRecords > 1) {
                    throw new RuleException("EPS_TSF_008");
                }
                
                if(totalRecords == 0){
                    if (nextStep != null && nextStep.getId() != null) {
                        // 1 - if next step exists ==> add
                        TemplateStepForwardVO forwardStepVO = new TemplateStepForwardVO();
                        forwardStepVO.setParentStep(vo);
                        forwardStepVO.setNextStep(nextStep);
                        forwardStepVO.setActionType(new Integer(1));
                        forwardStepVO.setCreatedBy(vo.getCreatedBy());
                                                
                        forwardHandler.create(forwardStepVO,dao);
                    }
                    
                }else{
                    TemplateStepVO oldStep = ((TemplateStepForwardVO) searchPageVO.getRecords()[0]).getParentStep();        
                    TemplateStepVO existStep = ((TemplateStepForwardVO) existSearchPageVO.getRecords()[0]).getParentStep();       
                    
                    oldStep.setId(vo.getId());
                    oldStep.setStepType(vo.getStepType());
                    TemplateStepForwardVO deleteForwardStepVO = new TemplateStepForwardVO();
                    TemplateStepForwardVO createForwardStepVO = new TemplateStepForwardVO();
                                        
                    if (nextStep == null || nextStep.getId() == null) {
                        // 1 - if NO next step ==> delete
                        deleteForwardStepVO.setParentStep(oldStep);
                        forwardHandler.deleteByStepId(oldStep.getId(),dao);
                        
                    } else {
                        // Delete old step
                        deleteForwardStepVO.setParentStep(oldStep); 
                        
                        deleteForwardStepVO.setNextStep(existStep);
                        
                        deleteForwardStepVO.setActionType(new Integer(1));
                        deleteForwardStepVO.setCreatedBy(vo.getCreatedBy());      
                        
                        forwardHandler.delete(deleteForwardStepVO,dao);
                                                
                        // Add new step                                    
                        createForwardStepVO.setParentStep(oldStep);   
                        createForwardStepVO.setNextStep(nextStep);
                        createForwardStepVO.setActionType(new Integer(1));
                        createForwardStepVO.setCreatedBy(vo.getCreatedBy());                        
                        
                        forwardHandler.create(createForwardStepVO,dao);
                        
                    }
                }
            }  

            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }



    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateBeginStep(TemplateStepVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step value object");
        }

        if (isBlankOrNull(vo.getNameAr())) {
            throw new RuleException("EPS_PTT_001");
        }
        
        if (vo.getStepType() == null) {
            throw new RuleException("EPS_PTT_002");
        }

        if(!vo.isProcedureStartStep()){
            throw new BusinessException(" Not Begin Step ");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL Template step CreatedBy username");
        }
        
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL TemplateStep template VO");
        }
        
        // check if step status is active
        if (new Integer(2).equals(vo.getTemplate().getStatus())) {
            throw new RuleException("EPS_PTT_006");
        }
        
        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL TemplateStep template ID");
        }
        
        // check if step type is end                
        if (vo.getStepType().equals(TemplateStepVO.TYPE_END)) {
            throw new RuleException("EPS_PTT_011");
        }
              
//        if(!vo.getStepType().equals(TemplateStepVO.TYPE_BEGIN) &&
//           !vo.getStepType().equals(TemplateStepVO.TYPE_HUMAN_TASK)){
//            if(vo.isStaticView()){
//                throw new RuleException("EPS_PTT_021");
//            }
//        }
                                        
        if(vo.isViewTypeStatic() && isBlankOrNull(vo.getViewLink())){
            throw new RuleException("EPS_PTT_022");
        }
                    
        if(vo.isViewTypeDynamic() && !isBlankOrNull(vo.getViewLink())){
            throw new RuleException("EPS_PTT_023");
        }        
        
                      
        // Invoke related DAO object 
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            dao.updateTemplateBeginStep(vo);
             
            TemplateStepVO nextStep = null;
            if (vo.getForwardslist() != null && vo.getForwardslist().size() > 0) {
                nextStep = (TemplateStepVO) vo.getForwardslist().get(0);    
            }                
            
            int totalRecords = 0;
            SearchPageVO searchPageVO = forwardHandler.find(0,vo.getId());
            SearchPageVO existSearchPageVO = forwardHandler.find(0,vo.getId());
            totalRecords = searchPageVO.getTotalRecords();
                            
            if (totalRecords > 1) {
                throw new RuleException("EPS_TSF_008");
            }
            
            if(totalRecords == 0){
                if (nextStep != null && nextStep.getId() != null) {
                    // 1 - if next step exists ==> add
                    TemplateStepForwardVO forwardStepVO = new TemplateStepForwardVO();
                    forwardStepVO.setParentStep(vo);
                    forwardStepVO.setNextStep(nextStep);
                    forwardStepVO.setActionType(new Integer(1));
                    forwardStepVO.setCreatedBy(vo.getCreatedBy());
                                            
                    forwardHandler.create(forwardStepVO,dao);
                }
                
            }else{
                TemplateStepForwardVO oldStep = (TemplateStepForwardVO) searchPageVO.getRecords()[0];        
                TemplateStepForwardVO existStep = (TemplateStepForwardVO) existSearchPageVO.getRecords()[0];        
                
                oldStep.setId(vo.getId());
                oldStep.getParentStep().setStepType(vo.getStepType());
                TemplateStepForwardVO deleteForwardStepVO = new TemplateStepForwardVO();
                TemplateStepForwardVO createForwardStepVO = new TemplateStepForwardVO();
                                    
                if (nextStep == null || nextStep.getId() == null) {
                    // 1 - if NO next step ==> delete
                    deleteForwardStepVO.setParentStep(oldStep.getParentStep());
                    forwardHandler.deleteByStepId(oldStep.getId(),dao);
                    
                } else {
                    // Delete old step
                    deleteForwardStepVO.setParentStep(oldStep.getParentStep());
                    deleteForwardStepVO.getParentStep().setId(oldStep.getId());
                    
                    deleteForwardStepVO.setNextStep(existStep.getParentStep());
                    deleteForwardStepVO.getNextStep().setId((existStep.getParentStep().getId()));
                    
                    deleteForwardStepVO.setActionType(new Integer(1));
                    deleteForwardStepVO.setCreatedBy(vo.getCreatedBy());      
                    
                    forwardHandler.delete(deleteForwardStepVO,dao);
                                            
                    // Add new step                                    
                    createForwardStepVO.setParentStep(oldStep.getParentStep());
                    createForwardStepVO.setNextStep(nextStep);
                    createForwardStepVO.setActionType(new Integer(1));
                    createForwardStepVO.setCreatedBy(vo.getCreatedBy());                        
                    
                    forwardHandler.create(createForwardStepVO,dao);
                    
                }
            }

            updateAttachmentActions(vo,dao);

            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    } 
    
    

    /**
     * Find Procedure Step
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure template Id
     * @param page Number 
     */
    public SearchPageVO find(int pageNo,Long ptlId){
        if(ptlId == null){
            throw new BusinessException("NULL template Id");
        }
        
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class); 
            return dao.find(pageNo,ptlId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
        
    /**
     * Get Step By Id
     * 
     * @return Template Step Value Object
     * @param stepId Template Step Id
     */
    public TemplateStepVO getById(Long stepId){
        if(stepId == null){
            throw new BusinessException("NULL template step Id");
        }
        
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class); 
            return dao.getById(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Find Security Groups
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findStepUsers(int pageNo,Long stepId,UserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL User value object");
        }        
        
        if (vo.getEmployee() == null) {
            throw new BusinessException("NULL Employee value object");
        } 
        
        if (stepId == null) {
            throw new BusinessException("NULL Template Step Id");
        } 
        
        // Invoke related DAO object
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.findStepUsers(pageNo,stepId,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }     
    
    
    /**
     * Delete procedure template step.
     * 
     * @param vo Procedure Template Value Object
     */
    public void delete(TemplateStepVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step value object");
        }

        if (vo.getId() == null) {
            throw new BusinessException("NULL template step Id");
        }
        
        if (vo.getStepType() != null) {
            if (vo.getStepType().equals(new Integer(4)) || 
                vo.getStepType().equals(new Integer(5))) {
                throw new RuleException("EPS_PTT_013");                                
            }            
        }
        
        if(vo.getTemplate() == null){
            throw new BusinessException("NULL Template Value Object");
        }
        
        if(vo.getTemplate().getStatus().equals(new Integer(2))){
            throw new RuleException("EPS_PTT_014");
        }
        
        if(isNextStep(vo.getId())){
            throw new RuleException("EPS_PTT_015");
        }
        
        if(hasFieldGroups(vo.getId())){
            
            throw new RuleException("EPS_PTT_026");
        }
        
        // Invoke related DAO object
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            dao.delete(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * has Steps
     * return TRUE if the template has defined steps except begin and end
     *                                                  otherwise reyurn FALSE
     *                                                  
     * @return boolean (true/false)
     * @param templateId
     */
    public boolean hasSteps(Long templateId){
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.hasSteps(templateId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Next Step
     *  - EPS_PTT_015
     *  - Cannot delete step because its related to another steps
     * return TRUE if the Step is next step for another step
     * 
     * @return boolean (true/false)
     * @param stepId
     */
    public boolean isNextStep(Long stepId){
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.isNextStep(stepId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Has Invalid Attachments
     * The step can't have attachment if the step type is not human action
     *  - EPS_PTT_004
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public boolean hasInvalidAttachments(Long templateId){
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.hasInvalidAttachments(templateId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Invalid Business Action Step
     * the file name must be added for business action step
     *  - EPS_PTT_016
     *   
     * @return boolean (true/false)
     * @param templateId Template Id
     */
    public List getInvalidBusinessActionStep(Long templateId){
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.getInvalidBusinessActionStep(templateId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Get template start step.
     * 
     * @param templateId Template ID.
     * @return start step for this template.
     */
    public TemplateStepVO getTemplateStartStep(Long templateId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }
        
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class); 
            return dao.getTemplateStartStep(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get Attachment settings.
     * 
     * @param stepId Step Id.
     * @return attachment settings for this step
     */
    public TemplateStepVO getAttachmentActions(Long stepId){
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step Id");
        }
        
        List actionList = actionHandler.getAttachmentActions(stepId);
        
        
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class); 
            TemplateStepVO vo = dao.getAttachmentActions(stepId);
            if(actionList != null && actionList.size() > 0){
                vo.setActionsList(actionList);  
            }
            
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Update Attachment Settings
     * 
     * @param stepId Step Id
     */
    public void updateAttachmentActions(TemplateStepVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Step Value Object");
        } 
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Step Id");
        }
        
        if(vo.getActionsList() != null && vo.getActionsList().size() > 0){
            boolean isUpload = false;
            for (int i = 0; i < vo.getActionsList().size(); i++)  {
                TemplateStepActionVO actionVO = (TemplateStepActionVO)vo.getActionsList().get(i);
                if(vo.getStepType() != null &&
                   vo.getStepType().intValue() == TemplateStepVO.TYPE_END.intValue()){
                        if(actionVO.isUploadAttachmentsAction()){
                            throw new RuleException("EPS_PTT_012");
                        }                    
                }

            }
            
        }
        
        TemplateStepDAO dao = null;
        try { 
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class); 
            dao.updateAttachmentActions(vo);
            
            TemplateStepActionVO actionVO = new TemplateStepActionVO();
            actionVO.setTemplateStep(vo);
            actionHandler.updateAttachmentActions(actionVO,dao);            
            
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);        
            throw ex;
        } catch (Exception ex) {
            rollback(dao);        
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Update Attachment Settings
     * 
     * @param stepId Step Id
     */
    protected void updateAttachmentActions(TemplateStepVO vo,DataAccessObject trsDAO){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Step Value Object");
        }
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL Template Step Id");
        }
        
        if(vo.getActionsList() != null && vo.getActionsList().size() > 0){
            boolean isUpload = false;
            for (int i = 0; i < vo.getActionsList().size(); i++)  {
                TemplateStepActionVO actionVO = (TemplateStepActionVO)vo.getActionsList().get(i);
                if(actionVO.isUploadAttachmentsAction()){
                    isUpload = true;
                }
            }
            
            if(isUpload){
                if (vo.getMinAttachmentsCount() == null) {
                    throw new RuleException("EPS_PTT_005");
                }
            
                if (vo.getMaxAttachmentsCount() == null) {
                    throw new RuleException("EPS_PTT_020");
                }
                
                if ((vo.getMinAttachmentsCount().intValue() < 0 ) ||
                    (vo.getMinAttachmentsCount().intValue() > vo.getMaxAttachmentsCount().intValue())) {
                    throw new RuleException("EPS_PTT_017");
                }     

                if ((vo.getMaxAttachmentsCount().intValue() <= vo.getMinAttachmentsCount().intValue()) ||
                    (vo.getMaxAttachmentsCount().intValue() <= 0)) {
                    throw new RuleException("EPS_PTT_018");
                }                 
            }          
            
        }        
        
        TemplateStepDAO dao = null;
        dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class,trsDAO); 
        
        TemplateStepActionVO actionVO = new TemplateStepActionVO();
        actionVO.setTemplateStep(vo);
        actionHandler.updateAttachmentActions(actionVO,dao);   
            
    }
    
    /**
     * Is Valid Step Name
     * the step name must uniqe
     *  - EPS_PTT_019
     *   
     * @return boolean (true/false)
     * @param stepName Step Name
     * @param stepId Step Id
     */
    public boolean isValidStepName(String stepName,Long stepId,Long templateId){
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.isValidStepName(stepName,stepId,templateId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Check if template step Has user
     *
     * @return true if template step has users
     * @param stepSeqNo step sequence number
     * @param templateId template ID
     */
    public boolean hasUsers(Long templateId, Integer stepSeqNo){
        if(templateId == null){
            throw new BusinessException("NULL Template ID");
        }
        
        if(stepSeqNo == null){
            throw new BusinessException("NULL Template step sequence number");
        }
        
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.hasUsers(templateId,stepSeqNo);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Get human task steps.
     * 
     * @param templateId Template ID.
     * @return human task steps.
     */
    public List getHumanTaskSteps(Long templateId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL Template ID");
        }

        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.getHumanTaskSteps(templateId);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Check if step has field groups
     * 
     * @return true if step has field groups
     * @param procedureTemplateStepId template step ID
     */
    public boolean hasFieldGroups(Long templateStepId) {
        // Validate parameters
        if (templateStepId == null) {
            throw new BusinessException("NULL template step ID");
        }
        
        TemplateStepDAO dao = null;
        try {
            dao = (TemplateStepDAO) getDAO(TemplateStepDAO.class);
            return dao.hasFieldGroups(templateStepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
}