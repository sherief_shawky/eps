/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateStepActionDAO;
import ae.eis.eps.vo.TemplateStepActionVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import java.util.List;

/**
 * Procedure template step action business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepActionHandler extends BusinessObject {
    /*
     * Business methods
     */
     
     private TemplateStepActionDAO getTemplateStepActionDAO(DataAccessObject trsDAO) {
         return (TemplateStepActionDAO) getDAO(TemplateStepActionDAO.class, trsDAO);
     }

    /**
     * Create new procedure template step action.
     * 
     * @param actionsList Procedure template step actions List.
     */
    protected void create(TemplateStepVO vo, DataAccessObject trsDAO) {
        // Skip in case of empty list
        if (vo.getActionsList() == null || vo.getActionsList().size() == 0) {
            return;
        }

        // Invoke related DAO object
        getTemplateStepActionDAO(trsDAO).create(vo);
    }
    
    /**
     * Delete procedure template step action.
     * 
     * @param actionsList Procedure template step actions List.
     */
    protected void delete(TemplateStepVO vo, DataAccessObject trsDAO) {
        // Validate actions
        if(vo == null){
            throw new BusinessException(" Null Template Step Value Object");
        }
        
        // Invoke related DAO object
        getTemplateStepActionDAO(trsDAO).delete(vo);
    }    
    
    /**
     * get Step Type Actions By Id
     * 
     * @return List of Template Step Action Value Object
     * @param stepId
     */
    public List getByStepId(Long stepId){
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step Id");
        }

        // Invoke related DAO object
        TemplateStepActionDAO dao = null;
        try {
            dao = (TemplateStepActionDAO) getDAO(TemplateStepActionDAO.class);
            return dao.getByStepId(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Can Update Attachment
     * - EPS_TSA_001
     * return TRUE if can add or delete attachment otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param stepId
     */
    public boolean canUpdateAttachment(Long stepId){
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step Id");
        }

        // Invoke related DAO object
        TemplateStepActionDAO dao = null;
        try {
            dao = (TemplateStepActionDAO) getDAO(TemplateStepActionDAO.class);
            return dao.canUpdateAttachment(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get requester allowed actions.
     * 
     * @param templateId Template ID.
     * @return  requester allowed actions.
     */
    public List getRequesterActions(Long templateId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template Id");
        }

        // Invoke related DAO object
        TemplateStepActionDAO dao = null;
        try {
            dao = (TemplateStepActionDAO) getDAO(TemplateStepActionDAO.class);
            return dao.getRequesterActions(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Get Attachment Settings
     * 
     * @return List of step actions
     * @param stepId Step Id
     */
    public List getAttachmentActions(Long stepId){
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step Id");
        }
 
        // Invoke related DAO object
        TemplateStepActionDAO dao = null;
        try {
            dao = (TemplateStepActionDAO) getDAO(TemplateStepActionDAO.class);
            return dao.getAttachmentActions(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Update Attachment Settings
     * 
     * @param trsDAO transaction Data Access Object
     * @param vo Template Step Action Value Object
     */
    public void updateAttachmentActions(TemplateStepActionVO vo,DataAccessObject trsDAO){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step action value object");
        }
        
        if (vo.getTemplateStep() == null) {
            throw new BusinessException("NULL template value object");
        }
        
        if (vo.getTemplateStep().getId() == null) {
            throw new BusinessException("NULL template step Id");
        }
        
        getTemplateStepActionDAO(trsDAO).updateAttachmentActions(vo);
       
    }
    
    /**
     * Get User Actions
     * 
     * @return List of step actions
     * @param stepId Step Id
     */
    public List getUserActions(Long stepId){
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL step Id");
        }
 
        // Invoke related DAO object
        TemplateStepActionDAO dao = null;
        try {
            dao = (TemplateStepActionDAO) getDAO(TemplateStepActionDAO.class);
            return dao.getUserActions(stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
}