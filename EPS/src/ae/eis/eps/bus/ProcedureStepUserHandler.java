/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  28/09/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureStepUserDAO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessException;
import java.util.List;

/**
 * Procedure step-users business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepUserHandler extends BusinessObject {

    /**
     * Get step authorized users.
     * 
     * @param procedureId Procedure ID.
     * @param sequenceNo Step sequence number.
     * @return step authorized users.
     */
    public List getStepUsers(Long procedureId, Integer sequenceNo) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (sequenceNo == null) {
            throw new BusinessException("NULL procedure step sequence number");
        }

        ProcedureStepUserDAO dao = null;
        try {
            dao = (ProcedureStepUserDAO) getDAO(ProcedureStepUserDAO.class);
            return dao.getStepUsers(procedureId, sequenceNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Check if this is a valid step user.
     * 
     * @param stepId Step ID.
     * @param userId Step user ID.
     * @return true if this is a valid step user.
     */
    public boolean isValidStepUser(Long stepId, Long userId) {
        // Validate parameters
        if (stepId == null) {
            throw new BusinessException("NULL procedure step ID");
        }

        if (userId == null) {
            throw new BusinessException("NULL procedure user ID");
        }

        ProcedureStepUserDAO dao = null;
        try {
            dao = (ProcedureStepUserDAO) getDAO(ProcedureStepUserDAO.class);
            return dao.isValidStepUser(stepId, userId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
}