/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  04/06/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateFieldOptionDAO;
import ae.eis.eps.vo.TemplateFieldOptionVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;

import java.util.List;

/**
 * Procedure template field-options business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateFieldOptionHandler extends BusinessObject {
    /**
     * Get template field options.
     * 
     * @param fieldId Template field ID.
     * @return template field options.
     */
    public List getFieldOptions(Long fieldId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field ID");
        }

        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            return dao.getFieldOptions(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Find template field options. 
     * 
     * @return SearchPageVO Search Page Value Object
     * @param pageNo page Number
     * @param fieldId field Id
     */
    public SearchPageVO find(int pageNo,Long fieldId){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field ID");
        }

        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            return dao.find(pageNo,fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }
    
    /**
     * Get template field option By Field Id and Order Number
     * 
     * @return TemplateFieldOptionVO Template Field Option Value Object
     * @param orderNo Order Number
     * @param fieldId Field Id
     */
    public TemplateFieldOptionVO get(Long fieldId, Integer orderNo){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field ID");
        }

        if (orderNo == null) {
            throw new BusinessException("NULL template field order number");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            return dao.get(fieldId,orderNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Create Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    public void createOption(TemplateFieldOptionVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template field option value object");
        }
        
        if (vo.getOrderNo() == null) {
            throw new RuleException("EPS_TFO_001");
        }        

        if (vo.getNameAr() == null) {
            throw new RuleException("EPS_TFO_002");
        }                

        if (vo.getValue() == null) {
            throw new RuleException("EPS_TFO_003");
        }                

        if(vo.getTemplateField() == null){
            throw new BusinessException("NULL template field value object");
        }
        
        // check if option arabic name is exist or not
        if (isArabicNameExist(vo.getTemplateField().getId(),vo.getNameAr(),null)) {
            throw new RuleException("EPS_TFO_004");
        }                
        
        // check if this option is exist or not
        if(isOptionExist(vo.getTemplateField().getId(),vo.getOrderNo(),null)){
            throw new RuleException("EPS_TFO_005");
        }
        
        // check if option name is exist or not
        if(isValueExist(vo.getTemplateField().getId(),vo.getValue(),null)){
            throw new RuleException("EPS_TFO_006");
        }
        
        // check if option english name is exist or not        
        if(isEnglishNameExist(vo.getTemplateField().getId(),vo.getNameEn(),null)){
            throw new RuleException("EPS_TFO_007");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            dao.createOption(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    
    /**
     * Update Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    public void updateOption(TemplateFieldOptionVO vo,Integer oldOrderNo,
                             String oldNameAr,String oldValue,String oldNameEn){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template field option value object");
        }
        
        if (vo.getOrderNo() == null) {
            throw new RuleException("EPS_TFO_001");
        }        

        if (vo.getNameAr() == null) {
            throw new RuleException("EPS_TFO_002");
        }                

        if (vo.getValue() == null) {
            throw new RuleException("EPS_TFO_003");
        }                

        if(vo.getTemplateField() == null){
            throw new BusinessException("NULL template field value object");
        }
        
        // check if option arabic name is exist or not
        if (isArabicNameExist(vo.getTemplateField().getId(),vo.getNameAr(),oldNameAr)){
            throw new RuleException("EPS_TFO_004");
        }                
        
        // check if this option is exist or not
        if(isOptionExist(vo.getTemplateField().getId(),vo.getOrderNo(),oldNameAr)){
            throw new RuleException("EPS_TFO_005");
        }
        
        // check if option value is exist or not
        if(isValueExist(vo.getTemplateField().getId(),vo.getValue(),oldValue)){
            throw new RuleException("EPS_TFO_006");
        }
        
        // check if option english name is exist or not        
        if(isEnglishNameExist(vo.getTemplateField().getId(),vo.getNameEn(),oldNameEn)){
            throw new RuleException("EPS_TFO_007");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            dao.updateOption(vo,oldOrderNo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Delete Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    public void deleteOption(TemplateFieldOptionVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template field option value object");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            dao.deleteOption(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Arabic Name Exist
     *  - EPS_TFO_004
     *  The Arabic name must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param nameAr Arabic Name
     */
    public boolean isArabicNameExist(Long fieldId,String nameAr,String oldNameAr){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field Id");
        }

        if (nameAr == null) {
            throw new BusinessException("NULL template field Arabic Name");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            return dao.isArabicNameExist(fieldId,nameAr,oldNameAr);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is Option Exist
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param orderNo Order Number
     */
    public boolean isOptionExist(Long fieldId,Integer orderNo,String oldNameAr){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field Id");
        }
        
        if (orderNo == null) {
            throw new BusinessException("NULL template field order Number");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            return dao.isOptionExist(fieldId,orderNo,oldNameAr);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Is Value Exist
     *  - EPS_TFO_006
     *  The Value must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param value Option Value
     */
    public boolean isValueExist(Long fieldId,String value,String oldValue){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field Id");
        }

        if (value == null) {
            throw new BusinessException("NULL template field value ");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            return dao.isValueExist(fieldId,value,oldValue);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is English Name Exist
     *  - EPS_TFO_007
     *  The Arabic name must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param nameEn English Name
     */
    public boolean isEnglishNameExist(Long fieldId,String nameEn,String oldNameEn){
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field Id");
        }

        if (nameEn == null) {
            throw new BusinessException("NULL template field english name ");
        }
        
        TemplateFieldOptionDAO dao = null;
        try {
            dao = (TemplateFieldOptionDAO) getDAO(TemplateFieldOptionDAO.class);
            return dao.isEnglishNameExist(fieldId,nameEn,oldNameEn);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
}