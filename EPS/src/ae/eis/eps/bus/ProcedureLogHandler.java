/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  19/05/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureLogDAO;
import ae.eis.eps.vo.ProcedureLogVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.Date;
import java.util.List;

/**
 * Procedure logs business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureLogHandler extends BusinessObject {
    /*
     * Business objects
     */
     
    /** Procedure field business object. */
    private ProcedureFieldHandler fieldsHandler = new ProcedureFieldHandler();

    /*
     * Methods.
     */
    
    /** 
     * Get by id
     * 
     * @return ProcedureLogVO procedure log value object
     * @param procedureLogId procedure log id
     */
    public ProcedureLogVO getById(Long procedureLogId){
        if (procedureLogId == null) {
            throw new BusinessException("NULL procedureLogId");
        }
        
        ProcedureLogDAO dao = null;
        try { 
            dao = (ProcedureLogDAO) getDAO(ProcedureLogDAO.class);
            return dao.getById(procedureLogId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }   
    }
    
    /**
     * Find proceudre history
     * 
     * @return search page value object
     * @param procedureId  procedure id
     * @param vo Proceure log value object
     * @param dateFrom from date
     * @param dateTo to date
     * @param pageNo page number
     */
    public SearchPageVO find(int pageNo,Long procedureId, ProcedureLogVO vo,
                                            Date dateFrom, Date dateTo){
        if (procedureId == null) {
            throw new BusinessException("NULL procedureId");
        }
        
        ProcedureLogDAO dao = null;
        try { 
            dao = (ProcedureLogDAO) getDAO(ProcedureLogDAO.class);
            return dao.find(pageNo,procedureId,vo,dateFrom,dateTo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }   
    }
    /**
     * Create procedure log DAO using transaction owner DAO instance.
     * 
     * @param dao Transaction owner DAO instance.
     * @return ProcedureLog DAO which uses the same trsDAO transaction.
     */
    private ProcedureLogDAO getProcedureLogDAO(DataAccessObject dao) {
        return (ProcedureLogDAO) getDAO(ProcedureLogDAO.class, dao);
    }

    /**
     * Add step notification log.
     * 
     * @param procedureId Procedure ID
     */
    protected void addNotificationLog(Long procedureId,Long userCenterId,DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        if(userCenterId==null){
            throw new BusinessException("NULL user center ID");
        }
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO instance");
        }

        // Add notification log
        boolean success = getProcedureLogDAO(dao).addNotificationLog(procedureId,userCenterId);

        if (success == false) {
            throw new BusinessException("Failed to insert notification step log");
        }
    }

    /**
     * Add business action log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId center id.
     * @param dao DataAccessObject
     * @return true if the log was saved successfuly
     */
    protected void addBusinessActionLog(Long procedureId,Long userCenterId, DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        if(userCenterId==null){
            throw new BusinessException("NULL user center ID");
        }
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO instance");
        }

        // Add notification log
        boolean success = getProcedureLogDAO(dao).addBusinessActionLog(procedureId,userCenterId);

        if (success == false) {
            throw new BusinessException("Failed to insert business action log");
        }
    }

    /**
     * Add end procedure log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId center id.
     * @param dao DataAccessObject
     */
    public void addEndProcedureLog(Long procedureId,Long userCenterId, DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        if(userCenterId==null){
            throw new BusinessException("NULL user center ID");
        }
        if (dao == null) {
            throw new BusinessException("NULL transactional DAO instance");
        }

        // Add notification log
        boolean success = getProcedureLogDAO(dao).addEndProcedureLog(procedureId,userCenterId);

        if (success == false) {
            throw new BusinessException("Failed to insert end procedure log");
        }
    }

    /**
     * Add step action log.
     * 
     * @param ProcedureLogVO Procedure Log VO
     * @param userCenterId center id.
     * @param dao DataAccessObject
     */
    protected void addActionLog(ProcedureLogVO vo, Long userCenterId,DataAccessObject dao) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure log value object");
        }

        if (vo.getStep() == null) {
            throw new BusinessException("NULL log procedure step VO");
        }
        
        if (vo.getStep().getProcedure() == null) {
            throw new BusinessException("NULL log procedure VO");
        }

        if (vo.getStep().getProcedure().getId() == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL user name");
        } 
        
        if (vo.getActionType() == null) {
            throw new BusinessException("NULL action type");
        }
        if(userCenterId==null){
            throw new BusinessException("NULL user center ID");
        }

        if (dao == null) {
            throw new BusinessException("NULL transactional DAO instance");
        }
        
        if(!isBlankOrNull(vo.getNote()) && vo.getNote().length() > 1024){ 
            throw new RuleException("EPS_PRD_028");
        } 
        
        // Add notification log
        boolean success = getProcedureLogDAO(dao).addActionLog(vo,userCenterId);

        if (success == false) {
            throw new BusinessException("Failed to insert step action log");
        }
    }
    
    /**
     * Find User logs
     * 
     * @return search page value object      
     * @param pageNo page number
     * @param vo Proceure log value object
     * @param creationDateFrom
     * @param creationDateTo
     * @param actionDateFrom
     * @param actionDateTo
     */
    public SearchPageVO findUserLogs(int pageNo, ProcedureLogVO vo, 
                                    String createdBy, Date creationDateFrom, 
                                    Date creationDateTo, Date actionDateFrom, 
                                    Date actionDateTo, List searchFields) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Procedure Log value object");
        }

        if (isBlankOrNull(createdBy)) {
            throw new BusinessException("NULL active user name");
        }

        if (pageNo < 0) {
            throw new BusinessException("Invalid page number: " + pageNo);
        }

        // Invoke related DAO object
        ProcedureLogDAO dao = null;
        try {
            dao = (ProcedureLogDAO) getDAO(ProcedureLogDAO.class);
            SearchPageVO searchPage = dao.findUserLogs(
                       pageNo, vo, createdBy,
                       creationDateFrom, creationDateTo,
                       actionDateFrom, actionDateTo,
                       searchFields);

            // Get extra search results according to template followup settings
            if (vo.getStep() != null && 
                vo.getStep().getProcedure() != null &&
                vo.getStep().getProcedure().getTemplate() != null &&
                vo.getStep().getProcedure().getTemplate().getId() != null) {
                // Loop on search results to set extra fields
                Object[] procedures = searchPage.getRecords();
                for (int i = 0; i < procedures.length; i++) {
                    ProcedureLogVO record = (ProcedureLogVO) procedures[i];
                    ProcedureVO procecureVO = record.getStep().getProcedure();
                    procecureVO.setFieldsList(
                        fieldsHandler.getProcedureSearchResults(procecureVO.getId()));
                }
            }

            // Return user requests
            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }                                     
   }
}