/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 * 
 * 1.10  Alaa Salem         04/01/2010  - Adding templateHandler
 *                                        Business Handler.
 *                                      - Adding userHandler Business Handler.
 *                                      - Adding createTransactionProcedure()
 *                                        Method.
 * 
 */

package ae.eis.eps.bus;


import ae.eis.common.bus.TrafficFileHandler;
import ae.eis.common.bus.UserHandler;
import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.TrafficFileVO;
import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.ProcedureDAO;
import ae.eis.eps.dao.ProcedureStepDAO;
import ae.eis.eps.vo.ProcedureFieldOptionVO;
import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.ProcedureLogVO;
import ae.eis.eps.vo.ProcedureStepFieldGroupVO;
import ae.eis.eps.vo.ProcedureStepFieldVO;
import ae.eis.eps.vo.ProcedureStepForwardVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateStepUserVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.ntf.bus.NotificationFacade;
import ae.eis.ntf.bus.NotificationHandler;
import ae.eis.ntf.vo.NotificationVO;
import ae.eis.trs.bus.TransactionHandler;
import ae.eis.trs.vo.ServiceVO;
import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.DomainVO;
import ae.eis.util.web.UserProfile;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Procedure workflow manager.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class WorkflowHandler extends BusinessObject {
    /*
     * Business Objects
     */
     
    /** Procedure business object. */
    private ProcedureHandler procedureHandler = new ProcedureHandler();
    
    /** Procedure step business object. */
    private ProcedureStepHandler stepHandler = new ProcedureStepHandler();

    /** Procedure step notifications business object. */
    private StepNotificationHandler notificationHandler = new StepNotificationHandler();

    /** Procedure logs business object. */
    private ProcedureLogHandler logHandler = new ProcedureLogHandler();
    
    /** Procedure step fields business object. */
    private ProcedureStepFieldHandler stepFieldHandler = 
        new ProcedureStepFieldHandler();
    
    /** Procedure field business object. */
    private ProcedureFieldHandler procedureFieldHandler = 
        new ProcedureFieldHandler();

    /** Template step business object. */
    private TemplateStepHandler templateStepHandler = new TemplateStepHandler();
    
    /** Procedure step-users business object. */
    private ProcedureStepUserHandler stepUserHandler = 
        new ProcedureStepUserHandler();
    
    /** Temporary attachments business object. */
    private TemporaryAttachmentHandler temporaryAttachmentHandler = 
        new TemporaryAttachmentHandler();

    /** Procedure Template Business Object. */
    private ProcedureTemplateHandler templateHandler = new ProcedureTemplateHandler();
    
    /** User Business Object. */
    private UserHandler userHandler = new UserHandler();
    
    /** Traffic File Business Object. */
    private TrafficFileHandler trafficFileHandler = new TrafficFileHandler();
   
   
    /** template Step User Handler. */
    private TemplateStepUserHandler templateStepUserHandler = new TemplateStepUserHandler();
       
  
    /*
     * Constants
     */
    
     /** comma SEPARATOR */
    private final String COMMA_SEPARATOR = ",";
     
    /*
     * Business Methods
     */
    
    /**
     * Validate procedure requester attachments.
     * 
     * @param vo Procedure value object to be created.
     * @param attachmentsRefNo Reference number for procedure attachments.
     */
    private void validateRequesterAttachments(ProcedureVO vo, Long attachmentsRefNo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL ProcedureVO parameter");
        }
    
        if (vo.getTemplate() == null) {
            throw new BusinessException("NULL ProcedureVO.template");
        }

        if (vo.getTemplate().getId() == null) {
            throw new BusinessException("NULL ProcedureVO.template.id");
        }

        // Get template start step info
        TemplateStepVO startStep = templateStepHandler.getTemplateStartStep(
            vo.getTemplate().getId());
            
        if (startStep == null) {
            throw new BusinessException("Start step not found for template ID: "
                + vo.getTemplate().getId());
        }

        int max = startStep.getMaxAttachmentsCount() == null
                ? 0 : startStep.getMaxAttachmentsCount().intValue();
                
        int min = startStep.getMinAttachmentsCount() == null
                ? 0 : startStep.getMinAttachmentsCount().intValue();
                
        int totalCount = 0;
        if (attachmentsRefNo != null) {
            totalCount = temporaryAttachmentHandler.getAttachmentsCount(
                attachmentsRefNo);
        }
        
        if (totalCount > max) {
            throw new RuleException("EPS_ATC_004");
        }
        
        if (min > 0 && totalCount < min) {
            throw new RuleException("EPS_ATC_005");
        }
    }

    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object to be created.
     * @param attachmentsRefNo Reference number for procedure attachments.
     * @param con Connection.
     * @param userProfile user profile.
     * @return procedure value object.
     */
    public ProcedureVO createProcedure(ProcedureVO vo, 
                                       Long attachmentsRefNo, 
                                       Connection con,
                                       UserProfile userProfile) {
        // Validate parameters
        if (con == null) {
            throw new BusinessException("NULL Connection parameter");
        }
        if(userProfile==null){
            throw new BusinessException("Null user profile");
            
        }
        // Create DAO instance using same JDBC connection
        DataAccessObject dao = getDAO(ProcedureDAO.class, con);

        // Call overloaded method
        return createProcedure(vo, attachmentsRefNo, dao, userProfile);
    }

    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object to be created.
     * @param attachmentsRefNo Reference number for procedure attachments.
     * @param dao data access object
     * @param userProfile user profile.
     * @return Created procedure value object.
     */
    public ProcedureVO createProcedure(ProcedureVO vo, 
                                       Long attachmentsRefNo, 
                                       DataAccessObject dao,
                                       UserProfile userProfile) {
        // Validate DAO instance
        if (dao == null) {
            throw new BusinessException("NULL DataAccessObject parameter");
        }
        
        if(userProfile==null){
            throw new BusinessException("Null user profile");
        }
                                       
        // Get template start step info
        validateRequesterAttachments(vo, attachmentsRefNo);

        // Create new procedure
        procedureHandler.create(vo, attachmentsRefNo, dao);
        
        // Get requester fields
        List requesterFields = stepFieldHandler.getRequesterStepFields(
            vo.getId(), dao);

        // Update procedure step-fields
        updateStepFields(vo, requesterFields, dao);

        // Process new active step
        processActiveStep(vo.getId(), userProfile, dao);
        
 
        return vo;
    }
    
    /**
     * Create New Incident.
     * 
     * @param ProcedureVO : Procedure VO.
     * @param dao data access object
     * @param userProfile : user profile.
     * 
     * Description:
     * If no employee is assigned on the procedure and 
     * No employee has privilege on the step (eps_%temp_steps) and 
     * Step type is human action and 
     * Template is not in the approved template list (property) 
     * Create new incident in the TF_STP_DATA_DISINTEGRITY_LOG and send notification to support team.  
     */
    public void createNewIncident(ProcedureVO vo, 
                                  DataAccessObject dao,
                                  UserProfile userProfile) {
        
        if (userProfile == null) {
            throw new BusinessException("Null user profile");
        }
        
        if (vo == null || vo.getId() == null ) {
            throw new BusinessException("Null procedure Id");    
        }
        
        // Get active step
        ProcedureStepVO activeStepVO = stepHandler.getActiveStep(vo.getId(), dao);
        if (activeStepVO == null) {
            throw new RuleException("EPS_PRD_010");
        } 
        
        if (dao == null) {
            throw new BusinessException("NULL DataAccessObject parameter");
        }
        
        // Skip if current active step is not a humean task.
        if (!activeStepVO.isHumanTaskStep()) {
            return;
        }
        
        // If there is employee is assigned on the procedure skip.
        if (activeStepVO.getProcedure() != null && 
            activeStepVO.getProcedure().getAssignedTo() != null &&
            activeStepVO.getProcedure().getAssignedTo().getId() != null ) {
            return;
        }
        
        if (activeStepVO.getProcedure() == null ||
            activeStepVO.getProcedure().getTemplate() == null || 
            activeStepVO.getProcedure().getTemplate().getId() == null ) {
            return;
        }
        
        Long templateId = activeStepVO.getProcedure().getTemplate().getId();
        if (activeStepVO.getProcedure() == null || 
            activeStepVO.getProcedure().getActiveStep() == null) {
            return;  
        }
        
        Long activeStepId = activeStepVO.getProcedure().getActiveStep().getId();
        TemplateStepVO templateStepVO = templateHandler.getProcedureTemplateStepBySequenceNumber(templateId,activeStepId);
        if (templateStepVO == null || templateStepVO.getId() == null) {
            return;    
        }
        
        TemplateStepUserVO templateStepUserVO = new TemplateStepUserVO();
        templateStepUserVO.setStep(templateStepVO);
        
        List usersOfStep = templateStepUserHandler.getUsersOfStep(templateStepUserVO);
        //If there is employee has privilege on the step skip.
        if (usersOfStep != null && usersOfStep.size() > 0 ) {
            return;     
        }
        
        //removed code that checks if no user is assigned on human task then created incident
        if(true){
            return;
        }
        
        //Create new incident in the TF_STP_DATA_DISINTEGRITY_LOG and send notification to support team.  
        //procedureHandler.createIncident(vo.getId(),activeStepVO.getNameAr(),dao);                                                                     
    }
    

    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object to be created.
     * @param attachmentsRefNo Reference number for procedure attachments.
     * @return Created procedure value object.
     * @param userProfile user profile.
     */
    public ProcedureVO createProcedure(ProcedureVO vo, Long attachmentsRefNo, UserProfile userProfile) {
        
        if(userProfile == null){
            throw new BusinessException("Null user profile");
        }
        
        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            // Call overloaded method
            vo = createProcedure(vo, attachmentsRefNo, dao,userProfile);
            // Commit changes
            dao.commit();

            if (vo.getTemplate() != null && 
                vo.getTemplate().getId() != null) {
                
                // get the details of the template
                ProcedureTemplateVO templateVO = templateHandler.getById(vo.getTemplate().getId());
                
            }
            
            // Return initialized procedure VO
            return vo;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Get all group step-fields.
     * 
     * @param fieldsGroupsList List of fields groups.
     * @return all group step-fields.
     */
    private List getAllGroupsStepFields(List fieldsGroupsList) {
        if (fieldsGroupsList == null || fieldsGroupsList.size() == 0) {
            return new ArrayList();
        }
    
        List  fieldsList = new ArrayList();
        for (int i = 0; i < fieldsGroupsList.size(); i++)  {
            ProcedureStepFieldGroupVO groupVO = (ProcedureStepFieldGroupVO)
                fieldsGroupsList.get(i);
                
            ProcedureStepFieldVO[] fields = groupVO.getProcedureStepFields();
            if (fields == null || fields.length == 0) {
                continue;
            }
            
            for (int j = 0; j < fields.length; j++)  {
                fieldsList.add(fields[j]);
            }
        }
        
        return fieldsList;
    }

    /**
     * Update procedure step-fields.
     * 
     * @param procedureVO Procedure value object.
     * @param stepFieldsGroups Current step fields groups info.
     */
    private void updateStepFields(ProcedureVO procedureVO, 
                                  List stepFieldsGroups, 
                                  DataAccessObject dao) {
        // Skip if this step has no fields
        if (stepFieldsGroups == null || stepFieldsGroups.size() <= 0) {
            return;
        }
        
        // Get user fields
        List userFields = procedureVO.getFieldsList();
        
        // Process step fields
        List stepFields = getAllGroupsStepFields(stepFieldsGroups);
        for (int i = 0; i < stepFields.size(); i++) {
            ProcedureStepFieldVO stepFieldVO = (ProcedureStepFieldVO) stepFields.get(i);
            ProcedureFieldVO fieldVO = stepFieldVO.getProcedureField();
            
            // Skip if teh step field is not editable
            if (! stepFieldVO.isEditable()) {
                continue;
            }
            
            // Get user input using field code.
            TemplateFieldVO userFieldVO = getFieldByCode(
                stepFieldVO.getField().getCode(), userFields);
                
            // If the field not found
            if (userFieldVO == null) {
                if (! fieldVO.isCheckBoxField() && stepFieldVO.isMandatory()) {
                    throw new RuleException("EPS_PRF_001");

                } else {
                    if (! fieldVO.isCheckBoxField()) {
                        continue;
                    }
                
                    userFieldVO = new ProcedureFieldVO();
                    userFieldVO.setCode(fieldVO.getCode());
                    if (!isBlankOrNull(fieldVO.getValue()))  {
                        userFieldVO.setValue(fieldVO.getValue());
                    } else {
                        userFieldVO.setValue(getString(Choice.getInteger(false)));
                    }
                    userFieldVO.setId(fieldVO.getId());
                }
            }
            
            // Check if the field is mandatory
            if (! fieldVO.isCheckBoxField() &&
                stepFieldVO.isMandatory() && 
                stepFieldVO.isEditable() &&                    
                isBlankOrNull(userFieldVO.getValue())) {
                throw new RuleException("EPS_PRF_001");
            }
            
            // Validate field size
            if (! fieldVO.isComboBoxField() &&
                ! fieldVO.isDateField() &&
                ! fieldVO.isCheckBoxField() &&
                ! isBlankOrNull(userFieldVO.getValue())&
                fieldVO.getSize() != null &&
                userFieldVO.getValue().length() > fieldVO.getSize().intValue()) {
                throw new RuleException("EPS_PRF_002", fieldVO.getLabel(), fieldVO.getSize().toString());
            }

            // Validate numeric fields values
            if (fieldVO.isNumberField() &&
                ! isBlankOrNull(userFieldVO.getValue()) &&
                !GlobalUtilities.isLong(userFieldVO.getValue())) {
                throw new RuleException("EPS_PRF_003");
            }

            // Validate date fields values
            if (fieldVO.isDateField() &&
                ! isBlankOrNull(userFieldVO.getValue())&&
                !GlobalUtilities.isValidDate(userFieldVO.getValue())) {
                throw new RuleException("EPS_PRF_004");
            }
            
            // Check if combo box value is a valid option
            if (fieldVO.isComboBoxField() &&
                ! isBlankOrNull(userFieldVO.getValue())) {
                if (! isValidOption(userFieldVO.getValue(), fieldVO.getOptionsList())) {
                    throw new RuleException("EPS_PRF_006");
                }
            }

            // Save field value to database
            ProcedureFieldVO procedureFieldVO = new ProcedureFieldVO();
            procedureFieldVO.setProcedure(procedureVO);
            procedureFieldVO.setUpdatedBy(procedureVO.getCreatedBy());
            procedureFieldVO.setCode(userFieldVO.getCode());
            procedureFieldVO.setValue(userFieldVO.getValue());
            procedureFieldHandler.update(procedureFieldVO, dao);
        }
    }
    
    /**
     * Check Check if combo box value is a valid option
     * 
     * @param userValue User input value
     * @param fieldOptions Field options
     * @return true Check if combo box value is a valid option
     */
    private boolean isValidOption(String userValue, List fieldOptions) {
        if (fieldOptions == null || fieldOptions.size() == 0) {
            throw new RuleException("EPS_PRF_007");
        }
        
        for (int i = 0; i < fieldOptions.size(); i++)  {
            ProcedureFieldOptionVO optionVO = (ProcedureFieldOptionVO) fieldOptions.get(i);
            if (optionVO.getValue().equals(userValue)) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Get field using its code.
     * 
     * @param fieldCode Field code.
     * @param userFields User step fields (user input).
     * @return step field value object.
     */
    private TemplateFieldVO getFieldByCode(Integer fieldCode, List userFields) {
        // Check user input
        if (userFields == null || userFields.size() <= 0) {
            return null;
        }
        
        // Search for user input field
        for (int i = 0; i < userFields.size(); i++)  {
            TemplateFieldVO userFieldVO = null;
            
          
                userFieldVO = (TemplateFieldVO) userFields.get(i);
             
            
            if (userFieldVO != null && 
                userFieldVO.getCode() != null &&
                userFieldVO.getCode().equals(fieldCode)) {
                return userFieldVO;
            }
        }
        
        // Field not found
        return null;
    }

    /**
     * Proces active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userProfile User Profile.
     * @param dao Data Access Object.
     * @param dao BC4J Application ModuleHandler
     */
    private void processActiveStep(Long procedureId, UserProfile userProfile, DataAccessObject dao) {
        if (userProfile == null) {
            throw new BusinessException("Null user profile");
        }
        
        // Get active step
        ProcedureStepVO activeStepVO = stepHandler.getActiveStep(procedureId, dao);
        if (activeStepVO == null) {
            throw new RuleException("EPS_PRD_010");
        }

        // Check if this is the start step
        if (activeStepVO.isProcedureStartStep()) {
            throw new RuleException("EPS_PRD_011");
        }

        // If close step, Close procedure and return
        if (activeStepVO.isProcedureEndStep()) {
            // Close procedure
            procedureHandler.closeProcedure(procedureId, userProfile.getUsername(), dao);

            // Save action log
            logHandler.addEndProcedureLog(procedureId,userProfile.getCenterId(), dao);


            processAddEpsSteps(procedureId, activeStepVO.getProcedure().getAssignedTo().getId(), userProfile.getUsername(), dao);

            return;
        }

        // Check if a notification must be sent to the requester
        if (activeStepVO.hasRequesterNotification() && !activeStepVO.isNotificationStep() ) {
            notificationHandler.notifiyRequester(activeStepVO.getId(), dao);
        }

        if (activeStepVO.hasStepUsersNotification()) {

            boolean skipNotification = canBeSkipped(activeStepVO);

            if (!skipNotification) {
                notificationHandler.notifiyStepUsers(activeStepVO.getId(),
                                                     activeStepVO.getProcedure().getAssignedTo().getId(), dao);
            }
        }

        processAddEpsSteps(procedureId, activeStepVO.getProcedure().getAssignedTo().getId(),userProfile.getUsername(), dao);

        //***********************(Create New Incident)**************************
        createNewIncident(activeStepVO.getProcedure(),dao,userProfile);
        
        // Skip if current active step is a humean task
        if (activeStepVO.isHumanTaskStep()) {
            return;
        }

        // Check if this is a notification step
        if (activeStepVO.isNotificationStep()) {
        
            notificationHandler.sendStepNotifications(activeStepVO.getId(), dao);

            // Add log record
            logHandler.addNotificationLog(procedureId,userProfile.getCenterId(), dao);

            // Activate next procedure step
            activateNextStep(procedureId, userProfile, dao);

            return;
        }

        // Check if this is a business action step
        if (activeStepVO.isBusinessAction()) {
            executeBusinessAction(procedureId, activeStepVO, userProfile, dao);
            return;
        }
    }

    /**
     * Skip notification in request plate for first time
     * and notify users from fillAllocateAuctionProcedureFieldsInfo because the plate id still empty
     * @param activeStepVO
     * @return
     */
    private boolean canBeSkipped(ProcedureStepVO activeStepVO) {

        if (activeStepVO.getProcedure() == null || activeStepVO.getProcedure().getCode() == null) {
            return false;
        }

        if (activeStepVO.getProcedure().getCode().equals(9001) && activeStepVO.getSequenceNo() == 3) {
            return true;
        }
        return false;
    }

    /**
     * Excecute business action logic.
     * 
     * @param procedureId Current procedure ID.
     * @param activeStepVO Procedure active step value object.
     * @param userProfile User Profile.
     * @param dao Current transaction data access obkect.
     * @param dao BC4J Application ModuleHandler
     */
    private void executeBusinessAction(Long procedureId,
                                       ProcedureStepVO activeStepVO,
                                       UserProfile userProfile,
                                       DataAccessObject dao) {
        // Get business action class name
        String className = activeStepVO.getActionClass();
        if (isBlankOrNull(className)) {
            throw new RuleException("EPS_PRD_014");
        }
        
        // Load business action implementation class
        BusinessAction action = null;
        try {
            action = (BusinessAction) Class.forName(className).newInstance();
            
           

        } catch (BusinessException ex) {
            ex.printStackTrace();
            throw ex;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuleException("EPS_PRD_014");
        }

        // Exceute business action
        Integer nextStepSeqNo = null;
        try {
            nextStepSeqNo = action.execute(procedureId, activeStepVO, userProfile, dao);

        } catch (BusinessException ex)  {
            ex.printStackTrace();
            throw ex;
        } catch (Exception ex)  {
            System.out.println("<<<<<<<<<<<<<<<<<<< Execute Business Action Exception >>>>>>>>>>>>>>>>>>");
            ex.printStackTrace();
            
            String stackTrace = GlobalUtilities.getStackTrace(ex);
            if (!isBlankOrNull(stackTrace) && stackTrace.indexOf("NTL_NOTIFIED_CHECK") >= 0)  {
                throw new RuleException("EPS_PRD_001");
            }
            
            System.out.println("<<<<<<<<<<<<<<<<<<< Execute Business Action Exception >>>>>>>>>>>>>>>>>>");
            ex.printStackTrace();
            throw new RuleException("EPS_PRD_015");
        }

        // Save business action log
        logHandler.addBusinessActionLog(procedureId,userProfile.getCenterId(),dao);

        // Get business action step forwards
        List forwardsList = stepHandler.getStepForwards(activeStepVO.getId(), dao);

        // Validate foeward business rules
        if (forwardsList == null || forwardsList.size() == 0) {
            throw new BusinessException(
                "No forwards were found for business action step");
        }
        
        // If no step number is returned, get next step from step forwards
        if (nextStepSeqNo == null) {
            if (forwardsList.size() > 1) {
                throw new RuleException("EPS_PRD_016");
            }
            
            ProcedureStepForwardVO forwardVO = (ProcedureStepForwardVO) 
                forwardsList.get(0);

            nextStepSeqNo = forwardVO.getNextStep().getSequenceNo();
        
        // Check if the returned step number has a valid step forward
        } else if (! isValidStepForward(nextStepSeqNo, forwardsList)) {
            throw new RuleException("EPS_PRD_017");
        }

        // Activate next procedure step
        activateStep(procedureId, nextStepSeqNo, userProfile, dao);
    }

    /**
     * Check if the next step number is valid.
     * 
     * @param nextStepSeqNo next step sequenece number.
     * @param forwardsList Foerwards list.
     * @return true if the next step number is valid.
     */
    private boolean isValidStepForward(Integer nextStepSeqNo, List forwardsList) {
        for (int i = 0; i < forwardsList.size(); i++)  {
            ProcedureStepForwardVO vo = (ProcedureStepForwardVO) forwardsList.get(i);
            if (vo.getNextStep().getSequenceNo().equals(nextStepSeqNo)) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Activate next procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userProfile User Profile.
     * @param dao Data Access Object.
     */
    private void activateNextStep(Long procedureId, UserProfile userProfile, DataAccessObject dao) {
        // Activate next procedure step
        procedureHandler.activateNextStep(procedureId, userProfile.getUsername(), dao);

        // Process new active step
        processActiveStep(procedureId, userProfile, dao);
    }

    /**
     * Activate next procedure step
     * 
     * @param procedureId Procedure ID.
     * @param stepSeqNo Step sequence number.
     * @param userProfile User Profile.
     * @param dao Current transaction DAO.
     */
    private void activateStep(Long procedureId, Integer stepSeqNo, UserProfile userProfile, DataAccessObject dao) {
        // Activate next procedure step
        procedureHandler.activateStep(procedureId, stepSeqNo, userProfile.getUsername(), dao);
        
        // Process new active step
        processActiveStep(procedureId, userProfile, dao);
    }

    /**
     * Activate next procedure step
     * 
     * @param procedureId Procedure ID.
     * @param stepSeqNo Step sequence number.
     * @param userProfile User Profile.
     * @param dao Current transaction DAO.
     */
    public void activateStep(Long procedureId, Integer stepSeqNo, UserProfile userProfile, Connection conn) {
        
        DataAccessObject dao = getDAO(ProcedureDAO.class,conn);
        activateStep(procedureId, stepSeqNo, userProfile, dao);
    }
    
    /**
     * Save action log.
     * 
     * @param procedureVO Procedure value object.
     * @param actionType Log action type.
     * @param dao Transaction DAO.
     */
    private void logAction(ProcedureVO procedureVO, 
                           Integer actionType, 
                           DataAccessObject dao,
                           Long userCenterId) {
        // Create log value object
        logAction(procedureVO.getId(), 
                  procedureVO.getLastNote(),
                  procedureVO.getCreatedBy(), 
                  userCenterId,
                  actionType, 
                  dao);
    }

    /**
     * Save action log.
     * 
     * @param dao Date Access Object
     * @param actionType action type
     * @param userCenterId user center ID
     * @param username user name
     * @param note Notes
     * @param procedureId procedure ID
     */
    private void logAction(Long procedureId, 
                           String note,
                           String username,
                           Long userCenterId,
                           Integer actionType, 
                           DataAccessObject dao) {
        // Create log value object
        ProcedureLogVO logVO = new ProcedureLogVO();
        logVO.setStep(new ProcedureStepVO());
        logVO.getStep().setProcedure(new ProcedureVO());

        logVO.setCreatedBy(username);
        logVO.setActionType(actionType);
        logVO.getStep().getProcedure().setId(procedureId);
        logVO.setNote(note);
        // to do add user center Id to parameters 
        logHandler.addActionLog(logVO,userCenterId, dao);
    }

    /**
     * Process user approve action.
     * 
     * @param vo Procedure value object.
     * @param userProfile User Profile.
     */
    public void approve(ProcedureVO vo, UserProfile userProfile) {
        approve(vo, userProfile, null);
    }
    
    /**
     * Process user approve action.
     * 
     * @param vo Procedure value object.
     * @param userProfile User Profile.
     * @param dao BC4J Application ModuleHandler
     */
    public void approve(ProcedureVO vo, 
                        UserProfile userProfile, 
                        DataAccessObject dao) {
        approve(vo, userProfile, dao, true);                   
    }
    
    /**
     * Process user approve action.
     * 
     * @param vo Procedure value object.
     * @param userProfile User Profile.
     * @param dao BC4J Application ModuleHandler
     */
    public void approve(ProcedureVO vo, 
                        UserProfile userProfile, 
                        DataAccessObject dao,
                        boolean withCommit) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }
        
        if (userProfile == null) {
            throw new BusinessException("NULL user Profile");
        }
        
        

        if (! procedureHandler.isUserActionAllowed(vo.getId(), 
                  userProfile.getUserId())) {
            throw new RuleException("EPS_PRD_020");    
        }
        ProcedureVO procedure = null;
        if (vo.getId() != null) {
            procedure = procedureHandler.getById(vo.getId());
        }
        if (procedure != null && procedure.getStatus() != null && 
            procedure.getStatus().equals(ProcedureVO.STATUS_PENDING) ) {
                throw new RuleException("EPS_PRD_023");
        }

       
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);            
                       
            // Get old active step
            ProcedureStepVO oldActiveStep = stepHandler.getActiveStep(
                                                        vo.getId(), dao);
                                                        
            if (oldActiveStep == null) {
                throw new RuleException("EPS_PRD_010");
            }

            if (! oldActiveStep.isHumanTaskStep()) {
                throw new RuleException("EPS_PRD_012");
            }
            
            if (vo.getActiveStep() == null || 
                vo.getActiveStep().getSequenceNo() == null) {
                
                // next step
                List forwardsList = new ArrayList();
                if (procedure.getActiveStep()!=null && 
                    procedure.getActiveStep().getId()!=null) {
                    
                    forwardsList = stepHandler.getApproveStepForwards(
                                procedure.getActiveStep().getId());
                }
                
                if (forwardsList.size()!=1) {
                    throw new RuleException("EPS_PRD_009");
                    
                } else {
                    ProcedureStepForwardVO procedureStepForward =
                        (ProcedureStepForwardVO) forwardsList.get(0); 
                    vo.setActiveStep(new ProcedureStepVO());
                    vo.getActiveStep().setSequenceNo(
                        procedureStepForward.getNextStep().getSequenceNo());
                }
            }
            
            // Check if a specific user must be selected for task assignment
            if (oldActiveStep.isTaskAssignmentMandatory()) {
                if (vo.getAssignedToUserId() == null) {
                    throw new RuleException("EPS_PRD_021");
                }

//                // Check if the selected user is a valid step-user
//                Long stepId = vo.getActiveStep().getId();
//                Long userId = vo.getAssignedToUserId();
//                
//                if (! stepUserHandler.isValidStepUser(stepId, userId)) {
//                    throw new RuleException("EPS_PRD_022");
//                }
            }
            
            // Check if the task must be assigned to procedure requester
            if (oldActiveStep.isTaskAssignmentRequester()) {
                if (vo.getAssignedTo() == null) {
                    vo.setAssignedTo(new UserVO());
                }
                
                vo.getAssignedTo().setId(
                    oldActiveStep.getProcedure().getRequester().getId());
            }

            // Save action log
            logAction(vo, ProcedureLogVO.ACTION_APPROVE, dao,userProfile.getCenterId());

            // Get requester fields
            List stepFields = stepFieldHandler.getActiveStepFields(vo.getId());

            // Update procedure step-fields
            updateStepFields(vo, stepFields, dao);

            // Update procedure info according to user input
            procedureHandler.approve(vo, dao);

            // Unclaim old step
            stepHandler.unclaimOldStep(vo.getId(), 
                                       userProfile.getUserId(),
                                       oldActiveStep.getSequenceNo(), 
                                       dao);

            // Process new active step
            processActiveStep(vo.getId(), userProfile, dao);
            
            if (withCommit) {
                // Commit changes
                dao.commit();
            }
        } catch (DataAccessException ex) {
            rollback(dao );
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
       
        } catch (Exception ex) {
            rollback(dao );
            throw new BusinessException(ex);
        } finally {
            // Commit changes
            close(dao );
        }
    }
    


   

    /**
     * Process user push-back action.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     */
    public void pushBack(ProcedureVO vo, UserProfile userProfile) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }
        if(userProfile==null){
            throw new BusinessException("Null user profile");
        }
        if(userProfile.getCenterId()==null){
            throw new BusinessException("NULL user center ID");
        }
        if (! procedureHandler.isUserActionAllowed(vo.getId(), 
              vo.getActiveStep().getClaimedBy().getId())) {
            throw new RuleException("EPS_PRD_020");
        }

        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            
            // Get old active step
            ProcedureStepVO oldActiveStep = stepHandler.getActiveStep(
                                                        vo.getId(), dao);

            if (oldActiveStep == null) {
                throw new RuleException("EPS_PRD_010");
            }

            if (! oldActiveStep.isHumanTaskStep()) {
                throw new RuleException("EPS_PRD_012");
            }
            
            if (oldActiveStep.getProcedure().getLastHumanTaskStep().getSequenceNo() == null) {
                throw new RuleException("EPS_PRD_013");
            }

            // Save action log
            logAction(vo, ProcedureLogVO.ACTION_PUSH_BACK, dao,userProfile.getCenterId());

            // Update procedure info according to user input
            procedureHandler.pushBack(vo, dao);

            // Unclaim old step
            stepHandler.unclaimOldStep(vo.getId(), 
                                       vo.getActiveStep().getClaimedBy().getId(),
                                       oldActiveStep.getSequenceNo(), 
                                       dao);

            // Process new active step
            processActiveStep(vo.getId(), userProfile, dao);

            // Commit changes
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }

    /**
     * Process user reject action.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     */
    public void reject(ProcedureVO vo,UserProfile userProfile) {
        reject(vo, userProfile, true, false, null);
    }
    
    /**
     * Process user reject action.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     */
    public void reject(ProcedureVO vo, UserProfile userProfile,
                       DataAccessObject dao) {
        reject(vo, userProfile, true, false, dao);
    }
    /**
     * Process user reject action.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     * @param dao BC4J Application ModuleHandler
     */
    public void reject(ProcedureVO vo, UserProfile userProfile, boolean withCommit,
                       boolean withUpdateFields, DataAccessObject dao) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (userProfile == null) {
            throw new BusinessException("NULL userProfile");
        }

        if (userProfile.getCenterId() == null) {
            throw new BusinessException("NULL user center ID");
        }

        if (! procedureHandler.isUserActionAllowed(vo.getId(), 
              vo.getActiveStep().getClaimedBy().getId())) {
            throw new RuleException("EPS_PRD_020");
        }

        ProcedureVO procedure = procedureHandler.getById(vo.getId());
        if (procedure != null && procedure.getStatus() != null && 
            procedure.getStatus().equals(ProcedureVO.STATUS_PENDING) ) {
                throw new RuleException("EPS_PRD_023");
        }
        
         
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            
            // Connect DAO instance with dao if exists
            if (dao != null) {
                dao.connect(dao);
            }

            // Get old active step
            ProcedureStepVO oldActiveStep = stepHandler.getActiveStep(
                                                        vo.getId(), dao);

            if (oldActiveStep == null) {
                throw new RuleException("EPS_PRD_010");
            }

            if (! oldActiveStep.isHumanTaskStep()) {
                throw new RuleException("EPS_PRD_012");
            }

            // Save action log
            logAction(vo, ProcedureLogVO.ACTION_REJECT, dao,userProfile.getCenterId());
            
            if (withUpdateFields) {
                // Get requester fields
                List stepFields = stepFieldHandler.getActiveStepFields(vo.getId());
        
                // Update procedure step-fields
                updateStepFields(vo, stepFields, dao);
            }
            // Update procedure info according to user input
            procedureHandler.reject(vo, dao);

            // Unclaim old step
            stepHandler.unclaimOldStep(vo.getId(), 
                                       vo.getActiveStep().getClaimedBy().getId(),
                                       oldActiveStep.getSequenceNo(), 
                                       dao);

            // Process new active step
            processActiveStep(vo.getId(), userProfile, dao);

            // Commit changes
            if (withCommit) {
                dao.commit();
            }
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao );
            throw ex;
        } catch (Exception ex) {
            rollback(dao );
            throw new BusinessException(ex);
        } finally {
            close(dao );
        }    
    }
    
    /**
     * Claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     * @param userProfile user profile.
     */
    public void claimStep(Long procedureId, UserProfile userProfile) {
        // Validate parameters
        if (isBlankOrNull(userProfile.getUsername())) {
            throw new BusinessException("NULL username");
        }
    
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        if (userProfile == null) {
            throw new BusinessException("NULL userProfile");
        }
        if (userProfile.getUserId() == null) {
            throw new BusinessException("NULL user ID");
        }
        if(userProfile.getCenterId()==null){
            throw new BusinessException("NULL user center ID");
        }
        if (!userProfile.getUsername().equals(UserVO.TRAFFIC_USER_NAME) &&
                ! procedureHandler.isUserActionAllowed(procedureId, userProfile.getUserId())) {
            throw new RuleException("EPS_PRD_020");
        }
        ProcedureVO vo = procedureHandler.getById(procedureId);
        if( vo != null && vo.getStatus() != null && 
            vo.getStatus().equals(ProcedureVO.STATUS_PENDING) ) {
                throw new RuleException("EPS_PRD_023");
        }

        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureStepDAO.class);
            
            // Claim procedure step
            stepHandler.claimStep(procedureId, userProfile.getUserId(), dao);
            
            // Save action log
            logAction(procedureId, null, userProfile.getUsername(),userProfile.getCenterId(),
                      ProcedureLogVO.ACTION_CLAIM, dao);

            // Commit changes
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }

    /**
     * Un-claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userProfile user profile.
     */
    public void unclaimStep(Long procedureId, UserProfile userProfile) {
        unclaimStep(procedureId, null,userProfile);    
    }
    
    /**
     * Un-claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userProfile user profile.
     */
    public void unclaimStep(Long procedureId,String lastNote, UserProfile userProfile) {
        unclaimStep(procedureId, lastNote, userProfile, null);
    }
    /**
     * Un-claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userProfile user profile.
     * @param passedDao Data Access Object.
     */
    public void unclaimStep(Long procedureId,String lastNote, UserProfile userProfile, DataAccessObject passedDao) {
        // Validate parameters
        if (isBlankOrNull(userProfile.getUsername())) {
            throw new BusinessException("NULL username");
        }
    
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        if (userProfile == null) {
            throw new BusinessException("NULL userProfile");
        }
        if (userProfile.getUserId() == null) {
            throw new BusinessException("NULL user ID");
        }
        if(userProfile.getCenterId()==null){
            throw new BusinessException("NULL user center ID");
        }
        DataAccessObject dao = null;
        try {
            
            if (passedDao == null) {
                // Get new DAO instance
                dao = getDAO(ProcedureStepDAO.class);
            } else {
                // Get new DAO instance
                dao = getDAO(ProcedureStepDAO.class, passedDao);

            }
            
            // Claim procedure step   
            stepHandler.unclaimActiveStep(procedureId, userProfile, dao);
            
            // Save action log
            logAction(procedureId, lastNote, userProfile.getUsername(),userProfile.getCenterId(),
                      ProcedureLogVO.ACTION_UNCLAIM, dao);
            
            if (passedDao == null) {
                // Commit changes
                dao.commit();
            }

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            if (passedDao == null) {
                close(dao);
            }
        }    
    }
    
    /**
     * Process user update action.
     * 
     * @param vo Procedure value object.
     * @param userProfile user profile.
     */
    public void update(ProcedureVO vo, UserProfile userProfile) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }
        if(userProfile==null){
            throw new BusinessException("NULL userProfile");
        }
        if(userProfile.getCenterId()==null){
            throw new BusinessException("NULL user center ID");
        }
        if (! procedureHandler.isUserActionAllowed(vo.getId(), 
              vo.getActiveStep().getClaimedBy().getId())) {
            throw new RuleException("EPS_PRD_020");
        }
        
        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            
            // Get old active step
            ProcedureStepVO oldActiveStep = stepHandler.getActiveStep(
                                                        vo.getId(), dao);
                                                        
            if (oldActiveStep == null) {
                throw new RuleException("EPS_PRD_010");
            }

            if (! oldActiveStep.isHumanTaskStep()) {
                throw new RuleException("EPS_PRD_012");
            }

            if( oldActiveStep.getProcedure() != null &&
                oldActiveStep.getProcedure().getStatus() != null &&
                oldActiveStep.getProcedure().getStatus().
                                        equals(ProcedureVO.STATUS_PENDING) ) {
                    throw new RuleException("EPS_PRD_023");
            }
        
            // Save action log
            vo.setLastNote(vo.getActiveNote());
            logAction(vo, ProcedureLogVO.ACTION_UPDATE, dao,userProfile.getCenterId());

            // Get requester fields
            List stepFields = stepFieldHandler.getActiveStepFields(vo.getId());

            // Update procedure step-fields
            updateStepFields(vo, stepFields, dao);

            // Update procedure info according to user input
            procedureHandler.update(vo.getId(), 
                                    vo.getActiveNote(), 
                                    vo.getUpdatedBy(), 
                                    dao);

            // Commit changes
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param con Connection
     * @param userProfile user profile.
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  UserProfile userProfile) {
        return createTransactionProcedure(trsId, null, 
                                          templateCode, 
                                          trafficFileId, 
                                          userProfile, 
                                          null, null, false);
            
    }
    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param trsIdFieldCode transaction field code
     * @param templateCode template code
     * @param con Connection
     * @param userProfile user profile.
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer trsIdFieldCode,
                                                  Integer templateCode,
                                                  Connection con,
                                                  UserProfile userProfile) {
        return createTransactionProcedure(trsId,
                                          trsIdFieldCode,
                                          templateCode,
                                          null,
                                          con,
                                          userProfile,
                                          null,
                                          null,
                                          false);
    }
    
    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param con Connection
     * @param userProfile user profile.
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  Connection con,
                                                  UserProfile userProfile) {
        return createTransactionProcedure(trsId, null, templateCode, 
            trafficFileId, con, userProfile, null, null, false);
    }
    
    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param con Connection
     * @param userProfile user profile.
     * @param mobileNo Mobile Number
     * @param email E-Mail
     * 
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  Connection con,
                                                  UserProfile userProfile,
                                                  String mobileNo,
                                                  String email) {
        
        return createTransactionProcedure(trsId, null, templateCode, 
            trafficFileId, con, userProfile, mobileNo, email, false);
    }
    
    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param con Connection
     * @param userProfile user profile.
     * @param isBackOfficeTransaction Is Back Office Transaction
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  Connection con,
                                                  UserProfile userProfile,
                                                  boolean isBackOfficeTransaction) {
        return createTransactionProcedure(trsId, null, templateCode, 
            trafficFileId, con, userProfile, null, null, isBackOfficeTransaction);
    }    
    
    
    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param trsIdFieldCode transaction field code
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param con Connection
     * @param userProfile user profile.
     * @param mobileNo Mobile Number
     * @param email E-Mail
     * @param isBackOfficeTransaction Is Back Office Transaction
     * 
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer trsIdFieldCode,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  Connection con,
                                                  UserProfile userProfile,
                                                  String mobileNo,
                                                  String email,
                                                  boolean isBackOfficeTransaction) {
        
        return createTransactionProcedure(trsId, trsIdFieldCode, 
                            templateCode, trafficFileId, con, userProfile, 
                            mobileNo, email, isBackOfficeTransaction, false);
    }
    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param trsIdFieldCode transaction field code
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param con Connection
     * @param userProfile user profile.
     * @param mobileNo Mobile Number
     * @param email E-Mail
     * @param isBackOfficeTransaction Is Back Office Transaction
     * @param mobileFlag take the mobile number directly from parameter.
     * 
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer trsIdFieldCode,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  Connection con,
                                                  UserProfile userProfile,
                                                  String mobileNo,
                                                  String email,
                                                  boolean isBackOfficeTransaction,
                                                  boolean mobileFlag) {
        
        System.out.println(">>>>>>> createTransactionProcedure :: ");
        System.out.println(">>>>>>> trafficFileId :: " + trafficFileId);
        System.out.println(">>>>>>> mobileNo :: " + mobileNo);
        System.out.println(">>>>>>> email :: " + email);
        System.out.println(">>>>>>> trsId :: " + trsId);
        // Validate parameters
        if (userProfile == null) {
            throw new BusinessException("Null userProfile.");
        }
        
        if (trsId == null) {
            throw new BusinessException("Null Transaction ID Value.");
        }

//        if (trsIdFieldCode == null) {
//            throw new BusinessException("Null Procedure Field Code, " +
//                                            "trsIdFieldCode=" + trsIdFieldCode);
//        }

        if (templateCode == null) {
            throw new BusinessException("Null Procedure Template Code Value.");
        }

        ProcedureTemplateVO tempVO = templateHandler.getByCode(templateCode);                               
        if(tempVO == null || tempVO.getId() == null) {
            throw new BusinessException("No Defined Procedure Templates, " +
                                                "templateCode" + templateCode);
        }        
        
        // check the procedure template is not active
        if(!tempVO.isActiveTemplate()) {
            throw new RuleException("EPS_PRD_026");
        }
        
        TransactionVO trsVO = TransactionHandler.getById(trsId);
        if(trsVO == null || trsVO.getId() == null) {
            throw new BusinessException("No Transaction For trsId=" + trsId);
        }
        
          
       
        UserVO userVO = userHandler.getByUserId(trsVO.getUserProfile().getUserId());
        
        // Initialize Procedure VO.
        ProcedureVO  procedureVO = new ProcedureVO();
        procedureVO.setTemplate(tempVO);
        procedureVO.setRequester(new UserVO());
         
        procedureVO.setFieldsList(new ArrayList());
         
        procedureVO.setTrafficFile(new TrafficFileVO());
        
        procedureVO.getRequester().setId(trsVO.getUserProfile().getUserId());
        
        procedureVO.setCreatedBy(userVO.getName());
        procedureVO.getTrafficFile().setId(trafficFileId);
        
        
        if (!mobileFlag) {              
            System.out.println(">>>>>>> DEBUG MSG 1 :: " );
            // if traffic id not null ( transaction procedure ), get mobile and email
            //    from traffic file ( prefered shipment info )
            
            boolean isInitialNocTemplateCode = false;
        
            
            if(trafficFileId != null && !isBackOfficeTransaction) {
                System.out.println(">>>>>>> DEBUG MSG 2 :: " );
                TrafficFileVO tempTrfVO = trafficFileHandler.getTrfFileById(trafficFileId); 

                if(tempTrfVO != null && 
                   (!isBlankOrNull(tempTrfVO.getPreferedShipmentMobile()) ||
                    !isBlankOrNull(tempTrfVO.getPreferedShipmentEmail())) &&
                    !isInitialNocTemplateCode) {
                            System.out.println(">>>>>>> DEBUG MSG 3 :: " );
                            
                            // If Service Is Seasonal And Traffic File Is Transferable Then Get Transaction Shippment.
                            if(trafficFileId.equals(new Long(TrafficFileVO.TRANSFERABLE_TRAFFIC_FILE_ID)) &&
                               trsVO.getServiceCode() != null  ) {
                                
                                
                                
                                    procedureVO.setRequesterMobile(tempTrfVO.getPreferedShipmentMobile());
                                    procedureVO.setRequesterEmail(tempTrfVO.getPreferedShipmentEmail());                                    
                                
                            } else {
                            
                                procedureVO.setRequesterMobile(getRequesterMobile(tempTrfVO));
                                procedureVO.setRequesterEmail(getRequesterEmail(tempTrfVO));
                            }
               
                } else {
                    System.out.println(">>>>>>> DEBUG MSG 7 :: " );
                    procedureVO.setRequesterMobile(mobileNo);
                    procedureVO.setRequesterEmail(email);   
                }
                
            } else {
            // if traffic id is null then ( back end ), get info from user info
                System.out.println(">>>>>>> DEBUG MSG 8 :: " );
                
                if(trsVO.getServiceCode() != null &&
                   trsVO.getServiceCode().equals(ServiceVO.SVC_RENEW_PERMITS)) {
                    TrafficFileVO tempTrfVO = trafficFileHandler.getTrfFileById(trafficFileId); 
                    procedureVO.setRequesterMobile(getRequesterMobile(tempTrfVO));
                    procedureVO.setRequesterEmail(getRequesterEmail(tempTrfVO));                    
                } else {
                    if(userVO.getEmployee().getMobileNo()!=null){
                        System.out.println(">>>>>>> DEBUG MSG 9 :: " );
                        procedureVO.setRequesterMobile(userVO.getEmployee().getMobileNo().toString());
                    } 
                    procedureVO.setRequesterEmail(userVO.getEmployee().getEmail());
                }
            }
        } else {
            System.out.println(">>>>>>> DEBUG MSG 10 :: " );
            procedureVO.setRequesterMobile(mobileNo);
            procedureVO.setRequesterEmail(email);
        }

        // Get Procedure Fields.
        if (trsIdFieldCode != null) {
            TemplateFieldVO fieldVO = new TemplateFieldVO();
            procedureVO.getFieldsList().add(fieldVO);
            fieldVO.setCode(trsIdFieldCode);
            fieldVO.setValue(trsId.toString());
        }
        System.out.println(">>>>>>> DEBUG MSG 11 :: " );
        if (procedureVO != null) {
            System.out.println(">>>>>>> procedureVO.getRequesterMobile :: " + procedureVO.getRequesterMobile());
            System.out.println(">>>>>>> procedureVO.getRequesterEmail :: " + procedureVO.getRequesterEmail());
        }
        return createProcedure(procedureVO, null, con,userProfile);
    }




    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param trsIdFieldCode transaction field code
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param userProfile user profile.
     * @param mobileNo Mobile Number
     * @param email E-Mail
     * @param isBackOfficeTransaction Is Back Office Transaction
     * 
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer trsIdFieldCode,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  UserProfile userProfile,
                                                  String mobileNo,
                                                  String email,
                                                  boolean isBackOfficeTransaction) {
        
        // Validate parameters
        if (userProfile == null) {
            throw new BusinessException("Null userProfile.");
        }
        
        if (trsId == null) {
            throw new BusinessException("Null Transaction ID Value.");
        }
        
        if (templateCode == null) {
            throw new BusinessException("Null Procedure Template Code Value.");
        }

        ProcedureTemplateVO tempVO = templateHandler.getByCode(templateCode);                               
        if(tempVO == null || tempVO.getId() == null) {
            throw new BusinessException("No Defined Procedure Templates, " +
                                                "templateCode" + templateCode);
        }        

        TransactionVO trsVO = TransactionHandler.getById(trsId);
        if(trsVO == null || trsVO.getId() == null) {
            throw new BusinessException("No Transaction For trsId=" + trsId);
        }
           
        UserVO userVO = userHandler.getByUserId(trsVO.getUserProfile().getUserId());
        
        // Initialize Procedure VO.
        ProcedureVO  procedureVO = new ProcedureVO();
        procedureVO.setTemplate(tempVO);
        procedureVO.setRequester(new UserVO());
        
        procedureVO.setFieldsList(new ArrayList());
        procedureVO.setTrafficFile(new TrafficFileVO());
        
        procedureVO.getRequester().setId(trsVO.getTrafficFile().getId());
        procedureVO.setCreatedBy(userVO.getName());
        procedureVO.getTrafficFile().setId(trafficFileId);

        // if traffic id not null ( transaction procedure ), get mobile and email
        // from traffic file ( prefered shipment info )
        if(trafficFileId != null && !isBackOfficeTransaction) {
            TrafficFileVO tempTrfVO = trafficFileHandler.getTrfFileById(trafficFileId); 

            if(tempTrfVO != null && 
               (!isBlankOrNull(tempTrfVO.getPreferedShipmentMobile()) ||
                !isBlankOrNull(tempTrfVO.getPreferedShipmentEmail())) ) { 
                    procedureVO.setRequesterMobile(tempTrfVO.getPreferedShipmentMobile());
                    procedureVO.setRequesterEmail(tempTrfVO.getPreferedShipmentEmail());                            
           
            } else {
                procedureVO.setRequesterMobile(mobileNo);
                procedureVO.setRequesterEmail(email);
            }
        } else {
            // if traffic id is null then ( back end ), get info from user info
            if(userVO.getEmployee().getMobileNo()!=null){
                procedureVO.setRequesterMobile(userVO.getEmployee().getMobileNo().toString());
            }
            procedureVO.setRequesterEmail(userVO.getEmployee().getEmail());
        }

        // Get Procedure Fields.
        if (trsIdFieldCode != null) {
            TemplateFieldVO fieldVO = new TemplateFieldVO();
            procedureVO.getFieldsList().add(fieldVO);
            fieldVO.setCode(trsIdFieldCode);
            fieldVO.setValue(trsId.toString());
        }
        
        return createProcedure(procedureVO, null,userProfile);
    }


    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object to be created.
     * @param userProfile user profile.
     * @param attachmentsRefNo Reference number for procedure attachments.
     * 
     * @return Created procedure value object.
     */
    public ProcedureVO createProcedure(ProcedureVO vo, 
                                       UserProfile userProfile,
                                       Long attachmentsRefNo) {
        // Get template start step info
        validateRequesterAttachments(vo, attachmentsRefNo);

        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);

            // Create new procedure
            procedureHandler.create(vo, attachmentsRefNo, dao);
            
            // Get requester fields
            List requesterFields = stepFieldHandler.getRequesterStepFields(
                vo.getId(), dao);
    
            // Update procedure step-fields
            updateStepFields(vo, requesterFields, dao);
    
            // Process new active step
            procedureFieldHandler.updateProcedureFields(vo, dao);

            // Commit changes
            dao.commit();

            return vo;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }  
    }

    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object to be created.
     * @param userProfile user profile.
     * @param attachmentsRefNo Reference number for procedure attachments.
     * 
     * @return Created procedure value object.
     */
    public ProcedureVO createProcedure(ProcedureVO vo, 
                                       UserProfile userProfile,
                                       Long attachmentsRefNo,
                                       List rowsList) {
        // Get template start step info
        validateRequesterAttachments(vo, attachmentsRefNo);

        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);

            // Create new procedure
            procedureHandler.create(vo, attachmentsRefNo, dao);
            
            // Get requester fields
            List requesterFields = stepFieldHandler.getRequesterStepFields(
                vo.getId(), dao);
    
            // Update procedure step-fields
            updateStepFields(vo, requesterFields, dao);
    
            // Process new active step
            procedureFieldHandler.updateProcedureFields(vo, dao);
            
          
            // Commit changes
            dao.commit();
  
            return vo;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }  
    }    
    

    /**
     * Get Requester Mobile
     * @param trfVO Traffic File VO
     * @return Requester Mobile
     */
    private String getRequesterMobile(TrafficFileVO trfVO) {
        if(trfVO == null) {
            return null;
        }
        
        if(!isBlankOrNull(trfVO.getPreferedShipmentMobile()) &&
            GlobalUtilities.isValidUAEMobile(trfVO.getPreferedShipmentMobile()) ) {
            return trfVO.getPreferedShipmentMobile();
        }
        
         
        return null;
    }

    /**
     * Get Requester Email
     * @param trfVO Traffic File VO
     * @return Requester Email
     */
    private String getRequesterEmail(TrafficFileVO trfVO) {
        if(trfVO == null) {
            return null;
        }
        
        if(!isBlankOrNull(trfVO.getPreferedShipmentEmail()) &&
            GlobalUtilities.isValidUAEEmail(trfVO.getPreferedShipmentEmail()) ) {
            return trfVO.getPreferedShipmentEmail();
        }
        
        return null;
    }    


    /**
     * Create New Transaction Procedure.
     * 
     * @param trsId transaction ID
     * @param trsIdFieldCode transaction field code
     * @param templateCode template code
     * @param trafficFileId traffic file ID
     * @param con Connection
     * @param userProfile user profile.
     * @param mobileNo Mobile Number
     * @param email E-Mail
     * @param isBackOfficeTransaction Is Back Office Transaction
     * @param mobileFlag take the mobile number directly from parameter.
     * @param userId User Id
     * 
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer trsIdFieldCode,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  Connection con,
                                                  UserProfile userProfile,
                                                  String mobileNo,
                                                  String email,
                                                  boolean isBackOfficeTransaction,
                                                  boolean mobileFlag,
                                                  Long userId) {
        
        // Validate parameters
        if (userProfile == null) {
            throw new BusinessException("Null userProfile.");
        }
        
        if (trsId == null) {
            throw new BusinessException("Null Transaction ID Value.");
        }

        if (templateCode == null) {
            throw new BusinessException("Null Procedure Template Code Value.");
        }

        ProcedureTemplateVO tempVO = templateHandler.getByCode(templateCode);                               
        if(tempVO == null || tempVO.getId() == null) {
            throw new BusinessException("No Defined Procedure Templates, " +
                                                "templateCode" + templateCode);
        }        
        
        // check the procedure template is not active
        if(!tempVO.isActiveTemplate()) {
            throw new RuleException("EPS_PRD_026");
        }
        
        TransactionVO trsVO = TransactionHandler.getById(trsId);
        if(trsVO == null || trsVO.getId() == null) {
            throw new BusinessException("No Transaction For trsId=" + trsId);
        }
       
        
        UserVO userVO = userHandler.getByUserId(userId);
        
        // Initialize Procedure VO.
        ProcedureVO  procedureVO = new ProcedureVO();
        procedureVO.setTemplate(tempVO);
        procedureVO.setRequester(new UserVO());
        procedureVO.setFieldsList(new ArrayList());
        procedureVO.setTrafficFile(new TrafficFileVO());
        
        procedureVO.getRequester().setId(userId);
        procedureVO.setCreatedBy(userVO.getName());
        procedureVO.getTrafficFile().setId(trafficFileId);
        procedureVO.setRequesterMobile(mobileNo);
        procedureVO.setRequesterEmail(email);

        return createProcedure(procedureVO, null, userProfile);
    }

    /**
     * Claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     * @param userProfile user profile.
     * @param conn Connection
     */
    public void claimStep(Long procedureId, UserProfile userProfile, Connection conn) {
        // Validate parameters
        if (isBlankOrNull(userProfile.getUsername())) {
            throw new BusinessException("NULL username");
        }
    
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        if (userProfile == null) {
            throw new BusinessException("NULL userProfile");
        }
        if (userProfile.getUserId() == null) {
            throw new BusinessException("NULL user ID");
        }
        if(userProfile.getCenterId()==null){
            throw new BusinessException("NULL user center ID");
        }
        if (!userProfile.getUsername().equals(UserVO.TRAFFIC_USER_NAME) &&
                ! procedureHandler.isUserActionAllowed(procedureId, userProfile.getUserId())) {
            throw new RuleException("EPS_PRD_020");
        }

        ProcedureVO vo = procedureHandler.getById(procedureId);
        if( vo != null && vo.getStatus() != null && 
            vo.getStatus().equals(ProcedureVO.STATUS_PENDING) ) {
                throw new RuleException("EPS_PRD_023");
        }

        DataAccessObject dao = null;

        // Get new DAO instance
        dao = getDAO(ProcedureStepDAO.class, conn);
        
        // Claim procedure step
        stepHandler.claimStep(procedureId, userProfile.getUserId(), dao);
        
        // Save action log
        logAction(procedureId, null, userProfile.getUsername(),userProfile.getCenterId(), ProcedureLogVO.ACTION_CLAIM, dao);

       
    }

    /**
     * Process user approve action.
     * 
     * @param vo Procedure value object.
     * @param userProfile User Profile.
     * @param dao BC4J Application ModuleHandler
     */
    public void approveEPS(ProcedureVO vo, 
                        UserProfile userProfile, 
                        Connection conn) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }
        
        if (userProfile == null) {
            throw new BusinessException("NULL user Profile");
        }
        
        if (userProfile.getCenterId() == null) {
            throw new BusinessException("NULL user center ID");
        }

        if (! procedureHandler.isUserActionAllowed(vo.getId(), 
                  userProfile.getUserId())) {
            throw new RuleException("EPS_PRD_020");
        } 

        ProcedureVO procedure = procedureHandler.getById(vo.getId());
        if (procedure != null && procedure.getStatus() != null && 
            procedure.getStatus().equals(ProcedureVO.STATUS_PENDING) ) {
                throw new RuleException("EPS_PRD_023");
        }

        DataAccessObject dao = null;
       // Get new DAO instance
        dao = getDAO(ProcedureDAO.class, conn);
        
        
        // Get old active step
        ProcedureStepVO oldActiveStep = stepHandler.getActiveStep(
                                                    vo.getId(), dao);
                                                    
        if (oldActiveStep == null) {
            throw new RuleException("EPS_PRD_010");
        }

        if (! oldActiveStep.isHumanTaskStep()) {
            throw new RuleException("EPS_PRD_012");
        }
        
        if (vo.getActiveStep() == null || 
            vo.getActiveStep().getSequenceNo() == null) {
            
            // next step
            List forwardsList = new ArrayList();
            if (procedure.getActiveStep()!=null && 
                procedure.getActiveStep().getId()!=null) {
                
                forwardsList = stepHandler.getApproveStepForwards(
                            procedure.getActiveStep().getId());
            }
            
            if (forwardsList.size()!=1) {
                throw new RuleException("EPS_PRD_009");
                
            } else {
                ProcedureStepForwardVO procedureStepForward =
                    (ProcedureStepForwardVO) forwardsList.get(0); 
                vo.setActiveStep(new ProcedureStepVO());
                vo.getActiveStep().setSequenceNo(
                    procedureStepForward.getNextStep().getSequenceNo());
            }
        }
        
        // Check if a specific user must be selected for task assignment
        if (oldActiveStep.isTaskAssignmentMandatory()) {
            if (vo.getAssignedToUserId() == null) {
                throw new RuleException("EPS_PRD_021");
            }

        }
        
        // Check if the task must be assigned to procedure requester
        if (oldActiveStep.isTaskAssignmentRequester()) {
            if (vo.getAssignedTo() == null) {
                vo.setAssignedTo(new UserVO());
            }
            
            vo.getAssignedTo().setId(
                oldActiveStep.getProcedure().getRequester().getId());
        }

        // Save action log
        logAction(vo, ProcedureLogVO.ACTION_APPROVE, dao,userProfile.getCenterId());

        // Get requester fields
        List stepFields = stepFieldHandler.getActiveStepFields(vo.getId());

        // Update procedure step-fields
        updateStepFields(vo, stepFields, dao);

        // Update procedure info according to user input
        procedureHandler.approve(vo, dao);

        // Unclaim old step
        stepHandler.unclaimOldStep(vo.getId(), 
                                   userProfile.getUserId(),
                                   oldActiveStep.getSequenceNo(), 
                                   dao);

    }
    
    
    /**
     * Populate notification value object.
     * 
     * @param employeeVO
     * @param procedureTitle
     * @param procedureId
     * @param sendingType
     * @param lanCode
     * @return
     */
    private NotificationVO populateNotificationVO(EmployeeVO employeeVO,
                                                  String procedureTitle, 
                                                  Long procedureId,
                                                  Integer sendingType,
                                                  String lanCode) {

        DomainVO domainVO =
            GlobalUtilities.getDomainValue("NTF_REASON_TYPE", NotificationVO.REASON_TYPE_UPDATE_TRIAL_RESULT);
        NotificationFacade notificationHandler = new NotificationHandler();
        
        // get the corresponding message for this service.
        NotificationVO notificationVO = notificationHandler.getMessage("EPS_NTF_CODE_1996", lanCode);
        notificationVO.setMessage(notificationVO.getMessage()
                                                .replaceFirst("<procedureTitle>", procedureTitle)
                                                .replaceFirst("<procedureId>", String.valueOf(procedureId)));

        // employeeVO.
        notificationVO.setEmail(employeeVO.getEmail());
        notificationVO.setMailFromAlias(employeeVO.getEmail());
        notificationVO.setMobile(String.valueOf(employeeVO.getMobileNo()));
        notificationVO.setSendingType(sendingType);
        
        // domain NTF_REASON_TYPE.
        notificationVO.setSubject(domainVO.getDescriptionAr());
        notificationVO.setReasonType(domainVO.getValue());
        notificationVO.setReasonTypeDescription(domainVO.getDescriptionEn());
                        
        return notificationVO;
    }
    /**
     * Create New Transaction Procedure.
     *
     * @param trsId transaction ID
     * @param trsIdFieldCode transaction field code
     * @param templateCode template code
     * @param trafficFileId
     * @param con Connection
     * @param userProfile user profile.
     * @return Created procedure value object.
     */
    public ProcedureVO createTransactionProcedure(Long trsId,
                                                  Integer trsIdFieldCode,
                                                  Integer templateCode,
                                                  Long trafficFileId,
                                                  Connection con,
                                                  UserProfile userProfile) {
        return createTransactionProcedure(trsId,
                                          trsIdFieldCode,
                                          templateCode,
                                          trafficFileId,
                                          con,
                                          userProfile,
                                          null,
                                          null,
                                          false);
    }
    
    /** 
      * Process approve action with no security. 
      *  
      * @param vo Procedure value object. 
      * @param userProfile User Profile. 
      * @param dao BC4J Application ModuleHandler 
      */ 
     public void approveNoSecurity(ProcedureVO vo,  
                         UserProfile userProfile,  
                         DataAccessObject dao) { 
         approveNoSecurity(vo, userProfile, dao, true);                    
     } 
    
    /**
     * Process approve action with no security. 
     * 
     * @param vo Procedure value object.
     * @param userProfile User Profile.
     * @param dao BC4J Application ModuleHandler.
     * @param withCommit with Commit.
     */
    public void approveNoSecurity(ProcedureVO vo, 
                        UserProfile userProfile, 
                        DataAccessObject dao,
                        boolean withCommit 
                                   ) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }
        
        if (userProfile == null) {
            throw new BusinessException("NULL user Profile");
        }
        
        if (userProfile.getCenterId() == null) {
            throw new BusinessException("NULL user center ID");
        }

        ProcedureVO procedure = procedureHandler.getById(vo.getId());    
        if (procedure != null && procedure.getStatus() != null && 
            procedure.getStatus().equals(ProcedureVO.STATUS_PENDING) ) {
                throw new RuleException("EPS_PRD_023");
        }

        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            
            // Connect DAO instance with dao if exists
            if (dao != null) {
                dao.connect( dao);
            }
            
            // Get old active step
            ProcedureStepVO oldActiveStep = stepHandler.getActiveStep(
                                                        vo.getId(), dao);
                                                        
            if (oldActiveStep == null) {
                throw new RuleException("EPS_PRD_010");
            }
            
            if (vo.getActiveStep() == null || 
                vo.getActiveStep().getSequenceNo() == null) {
                
                // next step
                List forwardsList = new ArrayList();
                if (procedure.getActiveStep()!=null && 
                    procedure.getActiveStep().getId()!=null) {
                    
                    forwardsList = stepHandler.getApproveStepForwards(
                                procedure.getActiveStep().getId());
                }
                
                if (forwardsList.size()!=1) {
                    throw new RuleException("EPS_PRD_009");
                    
                } else {
                    ProcedureStepForwardVO procedureStepForward =
                        (ProcedureStepForwardVO) forwardsList.get(0); 
                    vo.setActiveStep(new ProcedureStepVO());
                    vo.getActiveStep().setSequenceNo(
                        procedureStepForward.getNextStep().getSequenceNo());
                }
            }
  
            // Save action log
            logAction(vo, ProcedureLogVO.ACTION_APPROVE, dao,userProfile.getCenterId());
            
            // Approve
            procedureHandler.approve(vo, dao);

            // Unclaim old step
            stepHandler.unclaimOldStep(vo.getId(), 
                                       userProfile.getUserId(),
                                       oldActiveStep.getSequenceNo(), 
                                       dao);
            
            // Process new active step
            processActiveStep(vo.getId(), userProfile, dao);
            
            if (withCommit) {
                // Commit changes
                dao.commit();
            }
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Claim procedure step without security.
     * 
     * @param procedureId Procedure ID.
     * @param userProfile user profile.
     */
    public void claimStepNoSecurity(Long procedureId, UserProfile userProfile) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        if (userProfile == null) {
            throw new BusinessException("NULL userProfile");
        }
        if (isBlankOrNull(userProfile.getUsername())) {
            throw new BusinessException("NULL username");
        }
        if (userProfile.getUserId() == null) {
            throw new BusinessException("NULL user ID");
        }
        if(userProfile.getCenterId()==null){
            throw new BusinessException("NULL user center ID");
        }

        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureStepDAO.class);
            
            // Claim procedure step
            stepHandler.claimStepNoSecurity(procedureId, userProfile.getUserId(), dao);
            
            // Save action log
            logAction(procedureId, null, userProfile.getUsername(),userProfile.getCenterId(),
                      ProcedureLogVO.ACTION_CLAIM, dao);

            // Commit changes
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
        
    /**
     * Process approve Completed Procedure action without security.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     */
    public void approveCompletedProcedure(ProcedureVO vo, UserProfile userProfile,
                       DataAccessObject dao) {
        approveCompletedProcedure(vo, userProfile, true, dao);
    }
    
    /**
     * Process reject action without security.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     * @param withCommit with Commit
     * @param dao BC4J Application ModuleHandler
     */
    public void rejectNoSecurity(ProcedureVO vo, UserProfile userProfile, boolean withCommit,
                       DataAccessObject dao) {        
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (userProfile == null) {
            throw new BusinessException("NULL userProfile");
        }

        if (userProfile.getCenterId() == null) {
            throw new BusinessException("NULL user center ID");
        }
        ProcedureVO procedure = null;
        if (vo.getId() != null) {
            procedure = procedureHandler.getById(vo.getId());
        }
        if (procedure != null && procedure.getStatus() != null && 
            procedure.getStatus().equals(ProcedureVO.STATUS_PENDING) ) {
                throw new RuleException("EPS_PRD_023");
        }
        
        
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            
            // Connect DAO instance with dao if exists
            if (dao != null) {
                dao.connect( dao);
            }

            // Get old active step
            ProcedureStepVO oldActiveStep = stepHandler.getActiveStep(
                                                        vo.getId(), dao);

            if (oldActiveStep == null) {
                throw new RuleException("EPS_PRD_010");
            }

            // Save action log
            logAction(vo, ProcedureLogVO.ACTION_REJECT, dao,userProfile.getCenterId());

            // reject 
            procedureHandler.rejectNoSecurity(vo, dao);

            // Unclaim old step
            if (vo.getId() != null && vo.getActiveStep() != null && vo.getActiveStep().getClaimedBy() != null &&
                vo.getActiveStep().getClaimedBy().getId() != null && oldActiveStep.getSequenceNo() != null &&
                dao != null) {
                stepHandler.unclaimOldStep(vo.getId(), vo.getActiveStep().getClaimedBy().getId(),
                                           oldActiveStep.getSequenceNo(), dao);
            }
            // Process new active step
            processActiveStep(vo.getId(), userProfile, dao);

            // Commit changes
            if (withCommit) {
                dao.commit();
            }
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Process approve Completed Procedure without security.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     * @param dao BC4J Application ModuleHandler
     * @param withCommit with Commit 
     */
    public void approveCompletedProcedure(ProcedureVO vo, UserProfile userProfile, boolean withCommit,
                       DataAccessObject dao) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }

        if (userProfile == null) {
            throw new BusinessException("NULL userProfile");
        }

        if (userProfile.getCenterId() == null) {
            throw new BusinessException("NULL user center ID");
        }
        
         
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            
            // Connect DAO instance with dao if exists
            if (dao != null) {
                dao.connect( dao);
            }

            // Save action log
            logAction(vo, ProcedureLogVO.ACTION_APPROVE, dao,userProfile.getCenterId());
            
            // approve Completed Procedure
            procedureHandler.approveCompletedProcedure(vo, dao);

            processAddEpsSteps(vo.getId(), null, userProfile.getUsername(), dao);


            // Commit changes
            if (withCommit) {
                dao.commit();
            }
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Process push-back action with no security.
     * 
     * @param vo Procedure step value object.
     * @param userProfile user profile.
     */
    public void pushBackNoSecurity(ProcedureVO vo, UserProfile userProfile) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL procedure VO");
        }
        if(userProfile==null){
            throw new BusinessException("Null user profile");
        }
        if(userProfile.getCenterId()==null){
            throw new BusinessException("NULL user center ID");
        }

        DataAccessObject dao = null;
        try {
            // Get new DAO instance
            dao = getDAO(ProcedureDAO.class);
            
            ProcedureStepVO oldActiveStep = null;
            if (vo.getId() != null) {
                // Get old active step
                 oldActiveStep = stepHandler.getActiveStep(vo.getId(), dao);
            }

            if (oldActiveStep == null) {
                throw new RuleException("EPS_PRD_010");
            }

            // Save action log
            logAction(vo, ProcedureLogVO.ACTION_PUSH_BACK, dao,userProfile.getCenterId());

            // push back
            procedureHandler.pushBack(vo, dao);

            // Unclaim old step
            if (vo.getActiveStep() != null && vo.getActiveStep().getClaimedBy() != null &&
                vo.getActiveStep().getClaimedBy().getId() != null && oldActiveStep.getSequenceNo() != null &&
                dao != null) {
                stepHandler.unclaimOldStep(vo.getId(), vo.getActiveStep().getClaimedBy().getId(),
                                           oldActiveStep.getSequenceNo(), dao);
            }

            ProcedureStepVO activeStepVO = stepHandler.getActiveStep(vo.getId(), dao);
            if (activeStepVO != null) {
                // Process new active step
                processActiveStep(vo.getId(), userProfile, dao);
            }
            // Commit changes
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    
   
    /**
     * Process Adding EPS steps and delete old steps
     * 
     * @param vo Procedure VO 
     * @param dao Caller Data Access 
     */
    private void processAddEpsSteps(Long procedureId,Long userIdAssignedTo, String userName, DataAccessObject dao) {

        stepHandler.deleteUserSteps(procedureId, dao);

        if (userIdAssignedTo != null) {
            stepHandler.addUserSteps(procedureId, userIdAssignedTo, userName, dao);
        } else {
            stepHandler.addUserStepsNoAssingUsrId(procedureId, userName, dao);
        }
    }
}