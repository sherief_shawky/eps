/*
 * Copyright (c) i-Soft 2007.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   18/05/2009  - File created.
 */
 
package ae.eis.eps.bus;
import ae.eis.eps.dao.TemplateStepNotificationDAO;
import ae.eis.eps.vo.TemplateStepNotificationVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template Step Notification Business Object
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public class TemplateStepNotificationHandler extends BusinessObject {
    
    /**
     * Create New Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public TemplateStepNotificationVO create(TemplateStepNotificationVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step notification value object");
        }
        
        if(vo.getNotificationType() == null){
            throw new RuleException("EPS_TSN_001");
        }
        
        if(isBlankOrNull(vo.getTitle())){
            throw new RuleException("EPS_TSN_002");
        }
        
        if(isBlankOrNull(vo.getMessageBody())){
            throw new RuleException("EPS_TSN_003");
        }
                
        if(vo.getTemplateStep() == null){
            throw new BusinessException("NULL Template Step Value Object");
        }

        if(vo.getTemplateStep().getId() == null){
            throw new BusinessException("NULL Template Step Id");
        }
        
        // Invoke related DAO object
        TemplateStepNotificationDAO dao = null;
        try {
            dao = (TemplateStepNotificationDAO) getDAO(TemplateStepNotificationDAO.class);
            TemplateStepNotificationVO newVO = dao.create(vo);
            dao.commit();
            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Update Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public void update(TemplateStepNotificationVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step notification value object");
        }
        
        if(vo.getNotificationType() == null){
            throw new RuleException("EPS_TSN_001");
        }
        
        if(isBlankOrNull(vo.getTitle())){
            throw new RuleException("EPS_TSN_002");
        }
        
        if(isBlankOrNull(vo.getMessageBody())){
            throw new RuleException("EPS_TSN_003");
        }
                
        // Invoke related DAO object
        TemplateStepNotificationDAO dao = null;
        try {
            dao = (TemplateStepNotificationDAO) getDAO(TemplateStepNotificationDAO.class);
            dao.update(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * find Template Step Notification
     * 
     * @return SearchPageVO
     * @param pageNo
     * @param Template Step Notification Value Object
     */
    public SearchPageVO find(int pageNo,TemplateStepNotificationVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step notification value object");
        }
        
        // Invoke related DAO object
        TemplateStepNotificationDAO dao = null;
        try {
            dao = (TemplateStepNotificationDAO) getDAO(TemplateStepNotificationDAO.class);
            return dao.find(pageNo,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * get Template Step Notification
     * 
     * @return Template Step Notification Value Object
     * @param ntfStepId
     */
    public TemplateStepNotificationVO getById(Long ntfStepId){
        // Validate parameters
        if (ntfStepId == null) {
            throw new BusinessException("NULL step notification Id");
        }
        
        // Invoke related DAO object
        TemplateStepNotificationDAO dao = null;
        try {
            dao = (TemplateStepNotificationDAO) getDAO(TemplateStepNotificationDAO.class);
            return dao.getById(ntfStepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }             
    }
    
    
    /**
     * Delete Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public void delete(TemplateStepNotificationVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL template step notification value object");
        }
        
        if (vo.getId() == null) {
            throw new BusinessException("NULL template step notification Id ");
        }
        
        // Invoke related DAO object
        TemplateStepNotificationDAO dao = null;
        try {
            dao = (TemplateStepNotificationDAO) getDAO(TemplateStepNotificationDAO.class);
            dao.delete(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }    
    
    /**
     * Has Invalid Notifications
     * The notification message must be added when the step type is notification
     * -  EPS_PTT_010
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidNotificationSteps(Long templateId){
        // Invoke related DAO object
        TemplateStepNotificationDAO dao = null;
        try {
            dao = (TemplateStepNotificationDAO) getDAO(TemplateStepNotificationDAO.class);
            return dao.getInvalidNotificationSteps(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
}