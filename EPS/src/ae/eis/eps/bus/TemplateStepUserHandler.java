/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 * 
 * 1.01  Tariq Abu Amireh   17/06/2009  - re-Name File from Step Group 
 *                                                       to Step User
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateStepUserDAO;
import ae.eis.eps.vo.TemplateStepUserVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure template step security groups business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepUserHandler extends BusinessObject {
    /*
     * Business methods
     */

    /**
     * Create new procedure template step security group.
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    public TemplateStepUserVO create(TemplateStepUserVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL TemplateSetpGroup value object");
        }

        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("NULL TemplateSetpGroup CreatedBy username");
        }
        
        if (vo.getStep() == null) {
            throw new BusinessException("NULL TemplateSetpGroup.Step VO");
        }

        if (vo.getStep().getId() == null) {
            throw new BusinessException("NULL TemplateSetpGroup.Step.Id");
        }

        if (vo.getUser() == null) {
            throw new BusinessException("NULL TemplateSetpGroup.SecurityGroup VO");
        }

        if (vo.getUser().getId() == null) {
            throw new BusinessException("NULL TemplateSetpGroup.SecurityGroup.Id");
        }

        // Invoke related DAO object
        TemplateStepUserDAO dao = null;
        try {
            dao = (TemplateStepUserDAO) getDAO(TemplateStepUserDAO.class);
            TemplateStepUserVO newVO = dao.create(vo);

            dao.commit();
            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
    
    /**
     * Delete procedure template step security group.
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    public void delete(TemplateStepUserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL TemplateSetpGroup value object");
        }
        
        if (vo.getStep() == null ) {
            throw new BusinessException("NULL Template Setp value object");
        }

        if (vo.getStep().getId() == null ) {
            throw new BusinessException("NULL Template Setp Id ");
        }
        
        // Invoke related DAO object
        TemplateStepUserDAO dao = null;
        try {
            dao = (TemplateStepUserDAO) getDAO(TemplateStepUserDAO.class);
            dao.delete(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Get template step Security Groups
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    public SearchPageVO find(int pageNo,TemplateStepUserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL TemplateSetpGroup value object");
        }

        // Invoke related DAO object
        TemplateStepUserDAO dao = null;
        try {
            dao = (TemplateStepUserDAO) getDAO(TemplateStepUserDAO.class);
            return dao.find(pageNo,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Find Security Groups
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findUsers(int pageNo,TemplateStepUserVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Step Group value object");
        }        
        
        // Invoke related DAO object
        TemplateStepUserDAO dao = null;
        try {
            dao = (TemplateStepUserDAO) getDAO(TemplateStepUserDAO.class);
            return dao.findUsers(pageNo,vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    
    /**
     * Has Invalid Security Groups
     * The step must have granted security group
     * -  EPS_PTT_008
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidSecurityUsersSteps(Long templateId){
        // Invoke related DAO object
        TemplateStepUserDAO dao = null;
        try {
            dao = (TemplateStepUserDAO) getDAO(TemplateStepUserDAO.class);
            return dao.getInvalidSecurityUsersSteps(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
 
     /**
     * Has InActive Users
     * Can't activate the procedure because the step has user with status not active
     * - EPS_TSG_002
     * 
     * @return boolean (true/false)
     * @param templateId template Id
     */
    public List getInActiveUsersSteps(Long templateId){
        // Invoke related DAO object
        TemplateStepUserDAO dao = null;
        try {
            dao = (TemplateStepUserDAO) getDAO(TemplateStepUserDAO.class);
            return dao.getInActiveUsersSteps(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Get Users Of Step.
     * 
     * @param vo Procedure template step value object.
     * 
     * @return List of security groups value object.
     */
    public List getUsersOfStep(TemplateStepUserVO vo) {
        // Validate parameters
        if (vo == null) {
            throw new BusinessException("NULL Template Step User value object");
        } 
        
        if (vo.getStep() == null  || 
            vo.getStep().getId() == null) {
            throw new BusinessException("NULL Template Step User value object");
        } 
         
        // Invoke related DAO object
        TemplateStepUserDAO dao = null;
        try {
            dao = (TemplateStepUserDAO) getDAO(TemplateStepUserDAO.class);
            return dao.getUsersOfStep(vo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
}