/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureStepFieldDAO;
import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.ProcedureStepFieldGroupVO;
import ae.eis.eps.vo.ProcedureStepFieldVO;
import ae.eis.eps.vo.TemplateStepFieldVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;
import java.util.List;

/**
 * Procedure step-field business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepFieldHandler extends BusinessObject {
    /*
     * Business objects
     */
     
    /** Procedure field-options business object. */
    private ProcedureFieldOptionHandler optionsHandler = 
        new ProcedureFieldOptionHandler();

    /*
     * Business methods
     */

    /**
     * Create DAO using transaction owner DAO instance.
     * 
     * @param trsDAO Transaction owner DAO instance.
     * @return DAO which uses the same trsDAO transaction.
     */
    private ProcedureStepFieldDAO getProcedureStepFieldDAO(DataAccessObject trsDAO) {
        return (ProcedureStepFieldDAO) getDAO(ProcedureStepFieldDAO.class, trsDAO);
    }

    /**
     * Get requester step fields.
     * 
     * @param procedureId procedure ID.
     * @return requester step fields.
     */
    protected List getRequesterStepFields(Long procedureId, DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        // Get fields groups
        List groupsList = getProcedureStepFieldDAO(dao).getRequesterStepFields(
        procedureId);
        
        // Get fields options
        return loadComboBoxOptions(groupsList, dao);
    }


    /**
     * Get requester step fields.
     * 
     * @param procedureId procedure ID.
     * @return requester step fields.
     */
    public List getRequesterStepFields(Long procedureId) {
        ProcedureStepFieldDAO dao = null;
        try {
            dao = (ProcedureStepFieldDAO) getDAO(ProcedureStepFieldDAO.class);
            return getRequesterStepFields(procedureId, dao);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }

    /**
     * Get active procedure step fields.
     * 
     * @param procedureId procedure ID.
     * @return active procedure step fields.
     */
    protected List getActiveStepFields(Long procedureId, DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (dao == null) {
            throw new BusinessException("NULL DataAccessObject");
        }
        
        // Get fields groups
        List groupsList = getProcedureStepFieldDAO(dao).getActiveStepFields(procedureId);

        // Get fields options
        return loadComboBoxOptions(groupsList, dao);
    }
    
    /**
     * Load combo box fiels options.
     * 
     * @param groupsList Fields groups list to be initialized.
     */
    private List loadComboBoxOptions(List groupsList, DataAccessObject dao) {
        // Validate parameters
        if (groupsList == null || groupsList.size() <= 0) {
            return groupsList;
        }

        // Get fields options
        for (int i = 0; i < groupsList.size(); i++)  {
            ProcedureStepFieldGroupVO groupVO = (ProcedureStepFieldGroupVO) 
                groupsList.get(i);
            
            ProcedureStepFieldVO[] fields = groupVO.getProcedureStepFields();
            for (int j = 0; j < fields.length; j++)  {
                ProcedureFieldVO fieldVO = fields[j].getProcedureField();
                if (! fieldVO.isComboBoxField()) {
                    continue;
                }

                fieldVO.setOptionsList(optionsHandler.getFieldOptions(
                    fieldVO.getId(), dao));
            }
        }
        
        return groupsList;
    }

    /**
     * Get active procedure step fields.
     * 
     * @param procedureId procedure ID.
     * @return active procedure step fields.
     */
    public List getActiveStepFields(Long procedureId) {
        ProcedureStepFieldDAO dao = null;
        try {
            dao = (ProcedureStepFieldDAO) getDAO(ProcedureStepFieldDAO.class);
            return getActiveStepFields(procedureId, dao);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }
    }
}