/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 * 
 * 1.01  Sami Abudayeh      19/09/2013  - Add getTableData(Long procedureId, 
 *                                                         Integer fieldCode,
 *                                                         Long procedureStepId) 
 * 
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.ProcedureFieldDAO;
import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.eps.vo.TableVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.DataAccessObject;

import java.sql.Connection;

import java.util.List;

/**
 * Procedure field business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureFieldHandler extends BusinessObject {
    /**
     * Create DAO using transaction owner DAO instance.
     * 
     * @param trsDAO Transaction owner DAO instance.
     * @return DAO which uses the same trsDAO transaction.
     */
    private ProcedureFieldDAO getProcedureFieldDAO(DataAccessObject trsDAO) {
        return (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class, trsDAO);
    }

    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     */
     public void update(ProcedureFieldVO fieldVO) {
        update(fieldVO, false);         
    }
    
    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     * @param dao : Data Access Object.
     * @return true if the field was updated successfully.
     */
    public void update(ProcedureFieldVO fieldVO, DataAccessObject dao) {
        update(fieldVO,dao,false);
    }
    
    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     * @param conn Connection
     */
     public void update(ProcedureFieldVO fieldVO, Connection conn) {
        
        // Validate Parameter
        if (conn == null) {
            throw new BusinessException("Null Connection");
        }
        
        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class, conn);
            update(fieldVO,dao);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }
    }


    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     * @param conn Connection
     * @param acceptNull Accept Null Flag
     */
     public void update(ProcedureFieldVO fieldVO, Connection conn, boolean acceptNull) {
        
        // Validate Parameter
        if (conn == null) {
            throw new BusinessException("Null Connection");
        }
        
        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class, conn);
            update(fieldVO,dao,acceptNull);
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }
    }

    /**
     * Get procedure field info.
     * 
     * @param procedureId Procedure ID.
     * @param fieldCode Field code.
     * @param dao Data Access Object
     * @return procedure field info
     */
    public ProcedureFieldVO getByCode(Long procedureId, 
                                         Integer fieldCode, 
                                         DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        if (fieldCode == null) {
            throw new BusinessException("NULL field code");
        }
        
        return getProcedureFieldDAO(dao).getByCode(procedureId, fieldCode);
    }
    
    /**
     * Get procedure field info.
     * 
     * @param procedureId Procedure ID.
     * @param fieldCode Field code.
     * @return procedure field info
     */
    public ProcedureFieldVO getByCode(Long procedureId, 
                                         Integer fieldCode) {
        

        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            return getByCode(procedureId,fieldCode,dao);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Get procedure fields
     * 
     * @param procedureId Procedure ID.
     * @param codes Fields codes.
     * @return List of procedure fields.
     */
    protected List getProcedureFields(Long procedureId, Integer[] codes, 
                                   DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }

        return getProcedureFieldDAO(dao).getProcedureFields(procedureId, codes);
    }
    
    /**
     * Get procedure fields
     * 
     * @param procedureId Procedure ID.
     * @param codes Fields codes.
     * 
     * @return List of procedure fields.
     */
    public List getProcedureFields(Long procedureId, Integer[] codes) {
        // Validation
        if(procedureId == null){
            throw new BusinessException("NULL Procedure Id ");
        }
        
        if (codes == null && codes.length == 0) {
            throw new BusinessException("NULL Procedure Codes ");    
        }

        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            return dao.getProcedureFields(procedureId,codes);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }  
        
    }

    /**
     * Get procedure fields
     * 
     * @param procedureId Procedure ID.
     * @return List of procedure fields.
     */
    protected List getProcedureFields(Long procedureId, DataAccessObject dao) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure ID");
        }
        
        return getProcedureFieldDAO(dao).getProcedureFields(procedureId, null);
    }

    /**
     * Get Field By Procedure Id
     * 
     * @return Procedure Field Value Object
     * @param fieldId field Id
     */
    public ProcedureFieldVO getById(Long fieldId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL field Id");
        }

        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            return dao.getById(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }

    /**
     * Get procedure search results fields.
     * 
     * @param procedureId Procedure ID.
     * @return Get procedure search results fields.
     */
    public List getProcedureSearchResults(Long procedureId) {
        // Validate parameters
        if (procedureId == null) {
            throw new BusinessException("NULL procedure Id");
        }

        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            return dao.getProcedureSearchResults(procedureId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }

    
    /**
     * Get Table Date
     * 
     * @param procedureId Procedure Id
     * @param fieldCode Field Code
     * 
     * @return Table Value Object
     */
    public TableVO getTableData(Long procedureId, Long fieldCode){
        // Validation
        if(procedureId == null){
            throw new BusinessException("NULL Procedure Id ");
        }
        if(fieldCode == null){
            throw new BusinessException("NULL Field Code ");
        }

        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            return dao.getTableData(procedureId,fieldCode);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
	/**
	 * Get Table Date
	 * 
	 * @param procedureId Procedure Id
	 * @param fieldCode Field Code
     * @param procedureStepId Procedure Step Id
	 * 
     * @return Table Value Object
	 */
	public TableVO getTableData(Long procedureId, Integer fieldCode,Long procedureStepId){
        // Validation
        if(procedureId == null){
            throw new BusinessException("NULL Procedure Id ");
        }
        if(fieldCode == null){
            throw new BusinessException("NULL Field Code ");
        }
        if (procedureStepId == null) {
            throw new BusinessException("NULL Procedure Step Id");
        } 
        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            return dao.getTableData(procedureId,fieldCode,procedureStepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        } 

    }
    
    /**
     * Update Procedure Fields
     * 
     * @param procedureFields Procedure Fields
     * @param dao             Data Access Object
     */
    public void updateProcedureFields(ProcedureVO vo,DataAccessObject dao){
        
        // Validation
        if(vo == null){
            throw new BusinessException("NULL Procedure VO ");
        }
        // Get Procedure Fields
        List procedureFields = vo.getFieldsList();
        if(procedureFields == null || procedureFields.size() < 0){
            throw new BusinessException("NULL Procedure Fields ");
        }
        
        if(dao == null){
            throw new BusinessException("NULL Data Access Object ");
        }
        
        for (int i = 0; i < procedureFields.size() ; i++)  {
            ProcedureFieldVO procedureFieldVO = (ProcedureFieldVO)procedureFields.get(i);
            procedureFieldVO.setProcedure(vo);
            update(procedureFieldVO,dao);
        }
    }
    
    /**
     * Get Table Date
     * 
     * @param procedureId Procedure Id
     * @param fieldCode Field Code
     * @param dao        Data Access Object
     * 
     * @return Table Value Object
     */
    public TableVO getTableData(Long procedureId, Long fieldCode,DataAccessObject dao){
        // Validation
        if(procedureId == null){
            throw new BusinessException("NULL Procedure Id ");
        }
        if(fieldCode == null){
            throw new BusinessException("NULL Field Code ");
        }

        ProcedureFieldDAO procedureFieldDAO = null;
        try {
            procedureFieldDAO = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class,dao);
            return procedureFieldDAO.getTableData(procedureId,fieldCode);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }          
    }    
    
    /**
     * Get procedure field info.
     * 
     * @param procedureId Procedure ID.
     * @param fieldCode Field code.
     * @return procedure field info
     */
    public ProcedureFieldVO getByCode(Long procedureId, 
                                         Integer fieldCode
                                         , Connection con) {
        

        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class , con);
            return getByCode(procedureId,fieldCode,dao);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        }          
    }    
    

    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     * @param acceptNull true:set new value even if null or false: keep old 
     * value if the new value is null.
     */
     public void update(ProcedureFieldVO fieldVO, boolean acceptNull) {
        ProcedureFieldDAO dao = null;
        try {
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            update(fieldVO,dao, acceptNull);
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     * @param acceptNull true:set new value even if null or false: keep old 
     * 
     * @return true if the field was updated successfully.
     */
    private void update(ProcedureFieldVO fieldVO, DataAccessObject dao, boolean acceptNull) {
        validateUpdateParameters(fieldVO);
        
        // Update field value
        boolean status = getProcedureFieldDAO(dao).update(fieldVO, acceptNull);
        
        if (status == false) {
            throw new RuleException("EPS_PRF_005");
        }
    }    
    
    /**
     * Validates Procedure Field VO before update.
     * @param fieldVO
     */
    private void validateUpdateParameters(ProcedureFieldVO fieldVO){
        if (fieldVO == null) {
            throw new BusinessException("NULL ProcedureFieldVO");
        }
        
        if (isBlankOrNull(fieldVO.getUpdatedBy())) {
            throw new BusinessException("NULL ProcedureFieldVO.createdBy");
        }
        
        if (fieldVO.getCode() == null) {
            throw new BusinessException("NULL ProcedureFieldVO.code");
        }

        if (fieldVO.getProcedure() == null) {
            throw new BusinessException("NULL ProcedureFieldVO.procedure");
        }

        if (fieldVO.getProcedure().getId() == null) {
            throw new BusinessException("NULL ProcedureFieldVO.procedure.id");
        }    
        
    }
    
    /**
     * Insert
     * 
     * @param vo Procedure Field VO
     */
    public void insert(ProcedureFieldVO vo) {
        if (vo == null) {
            throw new BusinessException("Null Procedure Field VO");
        }
        if (vo.getProcedureTemplate() == null ) {
            throw new BusinessException("Null Procedure Template VO");
        }
        if (vo.getProcedureTemplate().getCode() == null) {
            throw new BusinessException("Null Procedure Template Code"); 
        } 
        if (vo.getProcedure() == null) {
            throw new BusinessException("Null Procedure Vo is Null"); 
        }
        if (vo.getProcedure().getId() == null) {
            throw new BusinessException("Null Procedure Id is Null"); 
        }
        if (isBlankOrNull(vo.getCreatedBy())) {
            throw new BusinessException("Null Created By is Null"); 
        }
        if (isBlankOrNull(vo.getUpdatedBy())) {
            throw new BusinessException("Null Updated By is Null"); 
        }
        ProcedureFieldDAO dao = null;
        try { 
            dao = (ProcedureFieldDAO) getDAO(ProcedureFieldDAO.class);
            dao.insert(vo);
            dao.commit();
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }      
    }
    
}