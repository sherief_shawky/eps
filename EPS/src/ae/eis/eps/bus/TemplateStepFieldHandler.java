/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.TemplateStepFieldDAO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateStepFieldGroupVO;
import ae.eis.eps.vo.TemplateStepFieldVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.RuleException;
import ae.eis.util.dao.DataAccessException;

import ae.eis.util.vo.Choice;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template step-field data business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepFieldHandler extends BusinessObject {
    /*
     * Business objects
     */

    /** Procedure template field-options business object. */
    private TemplateFieldOptionHandler optionsHandler = 
        new TemplateFieldOptionHandler();

    /** Template step-fields group business object. */
    private TemplateStepFieldGroupHandler groupHandler = 
        new TemplateStepFieldGroupHandler();

    /** Template field business object. */
    private TemplateFieldHandler fieldHandler = new TemplateFieldHandler();

    /** Template Table Header Handler */
    private TemplateTableHeaderHandler tableHeaderHandler = new TemplateTableHeaderHandler();

    /*
     * Business methods
     */

    /**
     * Get requester step fields.
     * 
     * @param templateId procedure template ID.
     * @return requester step fields.
     */
    public List getRequesterStepFields(Long templateId) {
        // Validate parameters
        if (templateId == null) {
            throw new BusinessException("NULL template ID");
        }

        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            List groupsList = dao.getRequesterStepFields(templateId);
            
            if (groupsList == null || groupsList.size() <= 0) {
                return groupsList;
            }
            
            // Get fields options
            for (int i = 0; i < groupsList.size(); i++) {
                TemplateStepFieldGroupVO groupVO = (TemplateStepFieldGroupVO) 
                    groupsList.get(i); 
                
                TemplateStepFieldVO[] fields = groupVO.getTemplateStepFields();
                for (int j = 0; j < fields.length; j++)  {
                    TemplateFieldVO fieldVO = fields[j].getField();
                    if (! fieldVO.isComboBoxField()) {
                        continue;
                    }

                    fieldVO.setOptionsList(optionsHandler.getFieldOptions(
                        fieldVO.getId()));
                }
            }
            
            return groupsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }

    /**
     * Get template step group fields.
     * 
     * @param pageNo Page Number
     * @param groupId Template step fields group ID.
     * @return template step group fields.
     */
    public SearchPageVO findByGroupId(int pageNo, Long groupId) {
        // Validate parameters
        if (groupId == null) {
            throw new BusinessException("NULL groupId");
        }

        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.findByGroupId(pageNo, groupId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }

    /**
     * Get step field info.
     * 
     * @param fieldId Template field ID.
     * @param groupId Fields group ID.
     * @return step field info.
     */
    public TemplateStepFieldVO getStepField(Long fieldId, Long groupId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field ID");
        }
        
        if (groupId == null) {
            throw new BusinessException("NULL fields group ID");
        }

        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.getStepField(fieldId, groupId);
                        
        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }
    
    /**
     * Create Step Field
     * 
     * @return TemplateStepFieldVO Template Step Field Value Object
     * @param vo Template Step Field Value Object
     */
    public TemplateStepFieldVO create(TemplateStepFieldVO vo){
        // Validate parameters
        validateForCreate(vo);
                            
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            TemplateStepFieldVO newVO = dao.create(vo);
            dao.commit();
            return newVO;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Validate For Create
     * 
     * @param vo Template Step Field Value Object
     */
    private void validateForCreate(TemplateStepFieldVO vo){
        // Validate create method parameters
        if (vo == null) {
            throw new BusinessException("NULL TemplateStepFieldVO value object");
        }
        
        if (vo.getTemplateStepFieldGroup() == null) {
            throw new BusinessException(
                "NULL TemplateStepFieldVO.templateStepFieldGroup");
        }

        if (vo.getTemplateStepFieldGroup().getId() == null) {
            throw new BusinessException(
                "NULL TemplateStepFieldVO.templateStepFieldGroup.id");
        }

        if (vo.getField() == null) {
            throw new BusinessException("NULL TemplateStepFieldVO.field");
        }

        if (vo.getField().getId() == null) {
            throw new BusinessException("NULL TemplateStepFieldVO.field.id");
        }
        
        // Get related group info
        TemplateStepFieldGroupVO groupVO = groupHandler.getById(
            vo.getTemplateStepFieldGroup().getId());
            
        if (groupVO == null) {
            throw new BusinessException("Fields group not found, group ID="
                + vo.getTemplateStepFieldGroup().getId());
        }
        
        // Get template field info
        TemplateFieldVO fieldVO = fieldHandler.getById(vo.getField().getId());
        if (fieldVO == null) {
            throw new BusinessException("Template field not found, field ID="
                + fieldHandler.getById(vo.getField().getId()));
        }
        
        // Set text area default values
        if (fieldVO.isTextAreaField()) {
            vo.setColumnSpan(new Integer(2));
            vo.setViewInNewLine(new Integer(2));
        }

        // Cannot add field to steps where step type is start or notification or business actions.
        if (groupVO.getTemplateStep().isNotificationStep()) {
            throw new RuleException("EPS_TSL_001");
        }

        if (fieldVO.isComboBoxField() && isEmptyComboBox(fieldVO.getId())  ){
           throw new RuleException("EPS_TSL_002");
        }
        
        // Can't add field to step because this step alresdy exist for this step
        if (isFieldExist(fieldVO.getId(), groupVO.getTemplateStep().getId())) {
            throw new RuleException("EPS_TSL_003");
        }     

        // Can't add fields to step because the template procedure status is active.
        if (groupVO.getTemplateStep().getTemplate().isActiveTemplate()) {
            throw new RuleException("EPS_TSL_004");
        }        
        
        // The field must not be editable if the step type is end
        if (groupVO.getTemplateStep().isProcedureEndStep() && vo.isEditable()) {
            throw new RuleException("EPS_TSL_005");
        }   
        
        // The field must not be mandatory if the step type is end
        if (groupVO.getTemplateStep().isProcedureEndStep() && vo.isMandatory()) {
            throw new RuleException("EPS_TSL_006");
        }

        // The field must be editable if the it's mandatory
        if (vo.isMandatory() && ! vo.isEditable()) {
            throw new RuleException("EPS_TSL_009");
        }
        
        // The field order does not exist 
        if (vo.getOrderNo() == null) {
            throw new RuleException("EPS_TSL_010");
        }

        // The order must be unique for the procedure
        if(isOrderExist(fieldVO.getId(), groupVO.getId(), vo.getOrderNo())){
            throw new RuleException("EPS_TSL_011");
        }        

        // The check box can�t be mandatory
        if (fieldVO.isCheckBoxField() && vo.isMandatory()) {
            throw new RuleException("EPS_TSL_012");
        }

        // to be change by new BR Key ==>> by Tariq
        if (vo.getColumnSpan() == null) {
            throw new RuleException("EPS_TPE_004");
        }
        
        if (fieldVO.isTextAreaField()) {
            if (! vo.getColumnSpan().equals(new Integer(2))) {
                throw new RuleException("EPS_TPE_019");
            }
            
            if (vo.getNoOfRows() == null) {
                throw new RuleException("EPS_TPE_017");
            }

            if (vo.getNoOfRows().intValue() <= 1) {
                throw new RuleException("EPS_TPE_018");
            }
        }        
        
        
        if( fieldVO.isTableField() ) {
            if( !tableHeaderHandler.isFieldTemplateHasHeader(vo.getField().getId()) ) {
                throw new RuleException("EPS_TSL_014");
            }

            if(!vo.getColumnSpan().equals(new Integer(2))){
                throw new RuleException("EPS_TSF_010");
            }

            if (vo.getViewInNewLine() != null && 
                vo.getViewInNewLine().equals(Choice.getInteger(Choice.NO))){
                throw new RuleException("EPS_TSF_011");
            }

            if (vo.getIsMandatory() != null && 
                vo.getIsMandatory().equals(Choice.getInteger(Choice.YES)) && 
                (vo.getMinNoOfRows() == null || vo.getMinNoOfRows().intValue() < 1 )) {
                throw new RuleException("EPS_TPE_027");
            }
        }
         
    }

    /**
     * Delete step field.
     * 
     * @param fieldId Template field ID.
     * @param groupId Fields group ID.
     */
    public void delete(Long fieldId, Long groupId) {
        // Validate parameters
        if (fieldId == null) {
            throw new BusinessException("NULL template field ID");
        }

        if (groupId == null) {
            throw new BusinessException("NULL group ID");
        }
        
        // Get group info
        TemplateStepFieldGroupVO groupVO = groupHandler.getById(groupId);
        if (groupVO == null) {
            throw new BusinessException("Template fields group not found, ID=" 
                + groupId);
        }

        // Can't delete fields because the template procedure status is active.
        if (groupVO.getTemplateStep().getTemplate().isActiveTemplate()) {
            throw new RuleException("EPS_TSL_008");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            dao.delete(fieldId, groupId);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Update Step Field
     * 
     * @param vo Template Step Field Value Object
     */
    public void update(TemplateStepFieldVO vo){
        // Validate parameters
        validateForUpdate(vo);
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            dao.update(vo);
            dao.commit();

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }   
    
    /**
     * Validate For Update
     * 
     * @param vo Template Step Field Value Object
     */
    private void validateForUpdate(TemplateStepFieldVO vo) {
        // Validate create method parameters
        if (vo == null) {
            throw new BusinessException("NULL TemplateStepFieldVO value object");
        }
        
        if (vo.getTemplateStepFieldGroup() == null) {
            throw new BusinessException(
                "NULL TemplateStepFieldVO.templateStepFieldGroup");
        }

        if (vo.getTemplateStepFieldGroup().getId() == null) {
            throw new BusinessException(
                "NULL TemplateStepFieldVO.templateStepFieldGroup.id");
        }

        if (vo.getField() == null) {
            throw new BusinessException("NULL TemplateStepFieldVO.field");
        }

        if (vo.getField().getId() == null) {
            throw new BusinessException("NULL TemplateStepFieldVO.field.id");
        }
        
        // Get related group info
        TemplateStepFieldGroupVO groupVO = groupHandler.getById(
            vo.getTemplateStepFieldGroup().getId());
            
        if (groupVO == null) {
            throw new BusinessException("Fields group not found, group ID="
                + vo.getTemplateStepFieldGroup().getId());
        }
        
        // Get template field info
        TemplateFieldVO fieldVO = fieldHandler.getById(vo.getField().getId());
        if (fieldVO == null) {
            throw new BusinessException("Template field not found, field ID="
                + fieldHandler.getById(vo.getField().getId()));
        }

        // Set text area default values
        if (fieldVO.isTextAreaField()) {
            vo.setColumnSpan(new Integer(2));
            vo.setViewInNewLine(new Integer(2));
        }

        // The field must not be editable if the step type is end
        if (groupVO.getTemplateStep().isProcedureEndStep() && vo.isEditable()) {
            throw new RuleException("EPS_TSL_005");
        }

        // The field must not be mandatory if the step type is end
        if(groupVO.getTemplateStep().isProcedureEndStep() && vo.isMandatory()) {
            throw new RuleException("EPS_TSL_006");
        }
        
        // Can't add fields to step because the template procedure status is active.
        if (groupVO.getTemplateStep().getTemplate().isActiveTemplate()){
            throw new RuleException("EPS_TSL_007");
        }
        
        // The field must be editable if the it's mandatory
        if (vo.isMandatory() && ! vo.isEditable()) {
            throw new RuleException("EPS_TSL_009");
        }

        // The field order does not exist 
        if (vo.getOrderNo() == null) {
            throw new RuleException("EPS_TSL_010");
        }

        // The order must be unique for the procedure
        if (isOrderExist(fieldVO.getId(), groupVO.getId(), vo.getOrderNo())) {
            throw new RuleException("EPS_TSL_011");
        }
        
        // The check box can�t be mandatory        
        if(fieldVO.isCheckBoxField() && vo.isMandatory()){
            throw new RuleException("EPS_TSL_012");
        }

        /* ****************************************************************** */
        /*   to be change by new BR Key ==>> by Tariq  */
        /* ****************************************************************** */

        if (vo.getColumnSpan() == null) {
            throw new RuleException("EPS_TPE_004");
        }

        if (fieldVO.isTextAreaField()) {
            if (! vo.getColumnSpan().equals(new Integer(2))) {
                throw new RuleException("EPS_TPE_019");
            }
            
            if (vo.getNoOfRows() == null) {
                throw new RuleException("EPS_TPE_017");
            }

            if (vo.getNoOfRows().intValue() <= 1) {
                throw new RuleException("EPS_TPE_018");
            }
        }

        if( fieldVO.isTableField() ) {
            
            if(!vo.getColumnSpan().equals(new Integer(2))){
                throw new RuleException("EPS_TSF_010");
            }

            if (vo.getViewInNewLine() != null &&
                vo.getViewInNewLine().equals(Choice.getInteger(Choice.NO))){
                throw new RuleException("EPS_TSF_011");
            }
        
            if (vo.getIsMandatory() != null && 
                vo.getIsMandatory().equals(Choice.getInteger(Choice.YES)) && 
                (vo.getMinNoOfRows() == null || vo.getMinNoOfRows().intValue() < 1 )) {
                throw new RuleException("EPS_TPE_027");
            }
        }
        
        /* ****************************************************************** */
        /* ****************************************************************** */
    }
    
    /**
     * Is Empty Combo Box
     * 
     * @return boolean (true / false)
     * @param fieldId Field Id
     */
    public boolean isEmptyComboBox(Long fieldId){
        // Validate parameters
        if (fieldId== null) {
            throw new BusinessException("NULL template field Id ");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.isEmptyComboBox(fieldId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is Field Exist
     * 
     * @return boolean (true / false)
     * @param stepId Step Id
     * @param fieldId Field Id
     */
    public boolean isFieldExist(Long fieldId,Long stepId){
        // Validate parameters
        if (fieldId== null) {
            throw new BusinessException("NULL template field Id ");
        }
        
        if (stepId== null) {
            throw new BusinessException("NULL template step Id ");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.isFieldExist(fieldId,stepId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
    
    /**
     * Is Valid Step Field
     * Cannot add fields to steps where step type is notification or business action
     *  - EPS_TSL_001
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidStepField(Long templateId){
        // Validate parameters
        if (templateId== null) {
            throw new BusinessException("NULL Template Id ");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.isValidStepField(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is Valid Editable Step Field
     * The Field must not be editable if the step type is end
     * -  EPS_TSL_005
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidEditableStepField(Long templateId){
        // Validate parameters
        if (templateId== null) {
            throw new BusinessException("NULL Template Id ");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.isValidEditableStepField(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is Valid Mandatory Step Field
     * The Field must not be mandatory if the step type is end
     * -  EPS_TSL_006
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidMandatoryStepField(Long templateId){
        // Validate parameters
        if (templateId== null) {
            throw new BusinessException("NULL Template Id ");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.isValidMandatoryStepField(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
    
    /**
     * Is Mandatory And Not Editable
     * The Field must be editable if the field is mandatory
     * -  EPS_TSL_009
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isMandatoryAndNotEditable(Long templateId){
        // Validate parameters
        if (templateId== null) {
            throw new BusinessException("NULL Template Id ");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.isMandatoryAndNotEditable(templateId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Is Order Exist
     *  - EPS_TSL_011 : The order must be unique for the procedure
     *  
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param fieldId field Id
     * @param groupId group Id
     * @param order order
     */
    private boolean isOrderExist(Long fieldId,Long groupId,Integer order){
        // Validate parameters
        if (groupId == null) {
            throw new BusinessException("NULL fields group ID");
        }

        if (fieldId == null) {
            throw new BusinessException("NULL Template Filed Id ");
        }

        if (order == null) {
            throw new BusinessException("NULL Field Order ");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.isOrderExist(fieldId,groupId,order);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }  
    }
    
    /**
     * Get Maximum Step Field Order Number
     * 
     * @return Maximum Step Field Order Number
     * @param groupId Group Id
     * @param stepId Step Id
     */
    public Integer getMaxStepFieldOrderNo(Long stepId,Long groupId){
        // Validate parameters
        
        if (stepId == null) {
            throw new BusinessException("NULL step Id ");
        }
        
        if (groupId == null) {
            throw new BusinessException("NULL fields group ID");
        }
        
        // Invoke related DAO object
        TemplateStepFieldDAO dao = null;
        try {
            dao = (TemplateStepFieldDAO) getDAO(TemplateStepFieldDAO.class);
            return dao.getMaxStepFieldOrderNo(stepId,groupId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }        
    }
}