/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/10/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.vo.ReportParameterVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.bus.RuleException;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

import ae.eis.eps.dao.ReportParameterDAO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessException;
import java.util.List;

/**
 * Procedure template report-parameters business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ReportParameterHandler extends BusinessObject {
    /*
     * Business Objects
     */
     
    /** Procedure template field-options business object. */
    private TemplateFieldOptionHandler fieldOptionHandler = 
        new TemplateFieldOptionHandler();

    /*
     * Business Methods
     */
    /**
     * Get report parameters.
     * 
     * @param reportId EPS report ID.
     * @return report parameters.
     */
    public List getByReportId(Long reportId) {
        // Validate parameters
        if (reportId == null) {
            throw new BusinessException("NULL report ID");
        }

        ReportParameterDAO dao = null;
        try {
            dao = (ReportParameterDAO) getDAO(ReportParameterDAO.class);
            List list = dao.getByReportId(reportId);
            
            // Get field options for combobox fields
            for (int i = 0; i < list.size(); i++) {
                ReportParameterVO vo = (ReportParameterVO) list.get(i);
                if (vo.getTemplateField().isComboBoxField()) {
                    vo.getTemplateField().setOptionsList(
                        fieldOptionHandler.getFieldOptions(
                            vo.getTemplateField().getId()));
                }
            }
            
            return list;

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }    
    }
    
    /**
     * Find report parameter
     * 
     * @return report parameters
     * @param pageNo pagination Page Number
     * @param reportId EPS report ID
     */
    public SearchPageVO find(Long reportId, int pageNo){
        // Validate parameters
        if (reportId == null) {
            throw new BusinessException("NULL report ID");
        }

        ReportParameterDAO dao = null;
        try {
            dao = (ReportParameterDAO) getDAO(ReportParameterDAO.class);
            return dao.find(reportId,pageNo);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }            
    }
    
    /**
     * Get report parameter
     * 
     * @return report parameter value object
     * @param reportId EPS param ID
     */
    public ReportParameterVO get(Long paramId){
        // Validate parameters
        if (paramId == null) {
            throw new BusinessException("NULL param ID");
        }

        ReportParameterDAO dao = null;
        try {
            dao = (ReportParameterDAO) getDAO(ReportParameterDAO.class);
            return dao.get(paramId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Add Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    public ReportParameterVO addReportParameter(ReportParameterVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException(" NULL Report Parameter Value Object ");
        } 
        
        if(vo.getReport() == null){
            throw new BusinessException(" NULL Report Value Object ");
        }
        
        if(vo.getReport().getId() == null){
            throw new BusinessException(" NULL Report ID ");
        }

        if(vo.getReport().getReportType() == null){
            throw new BusinessException(" NULL Report Type ");
        }
        
        if(vo.getTemplateField() == null){
            throw new BusinessException(" NULL Template Field Value Object");
        }

        /* ****************************************************************** */
        /* ************************* EPS_ERP_007 **************************** */
        /* ********************** Field Name Mandatory ********************** */
        /* ****************************************************************** */
        if(vo.getTemplateField().getType() == null){
            throw new RuleException("EPS_ERP_007");
        }
        
        /* ****************************************************************** */
        /* ************************* EPS_ERP_001 **************************** */
        /* ********************** Field Name Mandatory ********************** */
        /* ****************************************************************** */
        if(vo.getTemplateField().getId() == null){
            throw new RuleException("EPS_ERP_001");
        }        
       
        /* ****************************************************************** */
        /* ************************* EPS_ERP_002 **************************** */
        /* ******************** Parameter Name mandatory ******************** */
        /* ****************************************************************** */
        if(isBlankOrNull(vo.getName())){
            throw new RuleException("EPS_ERP_002");
        }        

        /* ****************************************************************** */
        /* ************************* EPS_ERP_003 **************************** */
        /* ********************* Report Order mandatory ********************* */
        /* ****************************************************************** */
        if(vo.getOrderNo() == null){
            throw new RuleException("EPS_ERP_003");
        }        
        
        /* ****************************************************************** */
        /* ************************* EPS_ERP_004 **************************** */
        /* *** Check If parameter Order is already exists for this report *** */
        /* ****************************************************************** */
        if(isOrderExist(vo.getReport().getId(),vo.getOrderNo(),null)){
            throw new RuleException("EPS_ERP_004");
        }

        /* ****************************************************************** */
        /* ************************* EPS_ERP_005 **************************** */
        /* ****************** Max Search Days must be null ****************** */
        /* ****************************************************************** */        
        if(!vo.getTemplateField().getType().equals(TemplateFieldVO.TYPE_DATE)){
            if(vo.getMaxSearchDays() != null){
                throw new RuleException("EPS_ERP_005");
            }
        }
        
        /* ****************************************************************** */
        /* ************************* EPS_ERP_006 **************************** */
        /* ****************** Max Search Days must be null ****************** */
        /* ****************************************************************** */        
        if(vo.getMaxSearchDays() !=  null){
            if(vo.getMaxSearchDays().intValue() < 0 || 
               vo.getMaxSearchDays().intValue() > 1000){
                throw new RuleException("EPS_ERP_006"); 
            }
        }
        
        /* ****************************************************************** */
        /* ************************* EPS_ERP_008 **************************** */
        /* ***************** static report cannot have parameters *********** */
        /* ****************************************************************** */         
        if(vo.getReport().isReportTypeStatic() ){
            throw new RuleException("EPS_ETR_004");
        }

        ReportParameterDAO dao = null;
        try {
            dao = (ReportParameterDAO) getDAO(ReportParameterDAO.class);
            ReportParameterVO newVO = dao.addReportParameter(vo);
            dao.commit();
            
            return vo;

        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }         
    }
    
    /**
     * Update Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    public void updateReportParameter(ReportParameterVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException(" NULL Report Parameter Value Object ");
        }
        
        if(vo.getTemplateField() == null){
            throw new BusinessException(" NULL Template Field Value Object");
        }

        if(vo.getTemplateField().getType() == null){
            throw new BusinessException(" NULL Template Field Type ");
        }
        
        /* ****************************************************************** */
        /* ************************* EPS_ERP_001 **************************** */
        /* ********************** Field Name Mandatory ********************* */
        /* ****************************************************************** */
        if(vo.getTemplateField().getId() == null){
            throw new RuleException("EPS_ERP_001");
        }        
       
        /* ****************************************************************** */
        /* ************************* EPS_ERP_002 **************************** */
        /* ******************** Parameter Name mandatory ******************** */
        /* ****************************************************************** */
        if(isBlankOrNull(vo.getName())){
            throw new RuleException("EPS_ERP_002");
        }        

        /* ****************************************************************** */
        /* ************************* EPS_ERP_003 **************************** */
        /* ********************* Report Order mandatory ********************* */
        /* ****************************************************************** */
        if(vo.getOrderNo() == null){
            throw new RuleException("EPS_ERP_003");
        }        
        
        /* ****************************************************************** */
        /* ************************* EPS_ERP_004 **************************** */
        /* *** Check If parameter Order is already exists for this report *** */
        /* ****************************************************************** */
        if(isOrderExist(vo.getReport().getId(),vo.getOrderNo(),vo.getId())){
            throw new RuleException("EPS_ERP_004");
        } 

        /* ****************************************************************** */
        /* ************************* EPS_ERP_005 **************************** */
        /* ****************** Max Search Days must be null ****************** */
        /* ****************************************************************** */        
        if(!vo.getTemplateField().getType().equals(TemplateFieldVO.TYPE_DATE)){
            if(vo.getMaxSearchDays() != null){
                throw new RuleException("EPS_ERP_005");
            }
        }
        
        /* ****************************************************************** */
        /* ************************* EPS_ERP_006 **************************** */
        /* ****************** Max Search Days must be null ****************** */
        /* ****************************************************************** */        
        if(vo.getMaxSearchDays() !=  null){
            if(vo.getMaxSearchDays().intValue() < 0 || 
               vo.getMaxSearchDays().intValue() > 999){
                throw new RuleException("EPS_ERP_006"); 
            }
        }

        ReportParameterDAO dao = null;
        try {
            dao = (ReportParameterDAO) getDAO(ReportParameterDAO.class);
            dao.updateReportParameter(vo);
            dao.commit();
            
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Is Order Exist
     * 
     * @return true if parameter order for this report is exists, 
     *                                                  otherwise return false
     * @param orderNo Parameter Order
     * @param reportId Report Id
     */
    public boolean isOrderExist(Long reportId,Integer orderNo,Long paramId){
        // Validate parameters
        if (reportId == null) {
            throw new BusinessException("NULL Report ID ");
        }
        if (orderNo == null) {
            throw new BusinessException("NULL Report Parameter Order ");
        }
        
        ReportParameterDAO dao = null;
        try {
            dao = (ReportParameterDAO) getDAO(ReportParameterDAO.class);
            return dao.isOrderExist(reportId,orderNo,paramId);

        } catch (DataAccessException ex) {
            throw ex;
        } catch(BusinessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }          
    }
    
    /**
     * Update Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    public void deleteReportParameter(ReportParameterVO vo){
        // Validate parameters
        if (vo == null) {
            throw new BusinessException(" NULL Report Parameter value object ");
        }
        
        if (vo.getId() == null) {
            throw new BusinessException(" NULL Report Parameter ID ");
        }

        ReportParameterDAO dao = null;
        try {
            dao = (ReportParameterDAO) getDAO(ReportParameterDAO.class);
            dao.deleteReportParameter(vo);
            dao.commit();
            
        } catch (DataAccessException ex) {
            rollback(dao);
            throw ex;
        } catch(BusinessException ex) {
            rollback(dao);
            throw ex;
        } catch (Exception ex) {
            rollback(dao);
            throw new BusinessException(ex);
        } finally {
            close(dao);
        }           
    }
}