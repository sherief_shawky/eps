/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  13/05/2009  - File created.
 */

package ae.eis.eps.bus;

import ae.eis.eps.dao.StepNotificationDAO;
import ae.eis.util.bus.BusinessException;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.dao.DataAccessObject;

/**
 * Procedure notifications business object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class StepNotificationHandler extends BusinessObject {
    /*
     * Business Methods
     */
    
    /**
     * Notifiy requester about the current procedure status.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    protected int notifiyRequester(Long stepId, DataAccessObject trsDAO) {
//        if (stepId == null) {
//            throw new BusinessException("NULL procedure ID");
//        }

//        return ((StepNotificationDAO) getDAO(StepNotificationDAO.class, trsDAO))
//               .notifiyRequester(stepId);
        return 1;
    }

    /**
     * Notifiy step users that a new procedure was received and requires action.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    protected int notifiyStepUsers(Long stepId, Long assignedToUserId, 
                                   DataAccessObject trsDAO) {
//        if (stepId == null) {
//            throw new BusinessException("NULL procedure ID");
//        }

//        return ((StepNotificationDAO) getDAO(StepNotificationDAO.class, trsDAO))
//               .notifiyStepUsers(stepId,assignedToUserId);
        return 1;
    }

    /**
     * Send step notifications.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    protected int sendStepNotifications(Long stepId, DataAccessObject trsDAO) {
//        if (stepId == null) {
//            throw new BusinessException("NULL procedure ID");
//        }

//        return ((StepNotificationDAO) getDAO(StepNotificationDAO.class, trsDAO))
//               .sendStepNotifications(stepId);
        return 1;
    }
}