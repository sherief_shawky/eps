/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  19/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.ProcedureLogVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.Date;
import java.util.List;

/**
 * Procedure logs access object interface.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureLogDAO extends DataAccessObject {
    
    /**
     * Get by id
     * 
     * @return ProcedureLogVO
     * @param procedureLogId
     */
    public ProcedureLogVO getById(Long procedureLogId);
    
    /**
     * Find proceudre history
     * 
     * @return search page value object
     * @param procedureId 
     * @param vo Proceure log value object
     * @param dateFrom
     * @param dateTo
     * @param pageNo page number
     */
    public SearchPageVO find(int pageNo,Long procedureId, ProcedureLogVO vo,
                                            Date dateFrom, Date dateTo);
    
    /**
     * Add step notification log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    boolean addNotificationLog(Long procedureId,Long userCenterId);

    /**
     * Add business action log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    boolean addBusinessActionLog(Long procedureId, Long userCenterId);

    /**
     * Add step action log.
     * 
     * @param vo Procedure log value object.
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    boolean addActionLog(ProcedureLogVO vo,Long userCenterId);

    /**
     * Add end procedure log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    boolean addEndProcedureLog(Long procedureId, Long userCenterId);
    
    /**
     * Find User logs
     * 
     * @return search page value object      
     * @param pageNo page number
     * @param vo Proceure log value object
     * @param creationDateFrom
     * @param creationDateTo
     * @param actionDateFrom
     * @param actionDateTo
     */
    SearchPageVO findUserLogs(
                 int pageNo, ProcedureLogVO vo, String createdBy,
                  Date creationDateFrom, Date creationDateTo,
                  Date actionDateFrom, Date actionDateTo,
                  List searchFields);    
}