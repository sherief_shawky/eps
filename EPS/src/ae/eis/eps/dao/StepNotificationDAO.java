/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  13/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.util.dao.DataAccessObject;

/**
 * Procedure step notifications data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface StepNotificationDAO extends DataAccessObject {
    /**
     * Notifiy requester about the current procedure status.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    int notifiyRequester(Long stepId);

    /**
     * Notifiy step users that a new procedure was received and requires action.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    int notifiyStepUsers(Long stepId, Long assignedToUserId);

    /**
     * Send step notifications.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    int sendStepNotifications(Long stepId);
}