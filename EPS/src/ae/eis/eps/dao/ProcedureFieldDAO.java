/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 * 
 * 1.01  Sami Abudayeh      19/09/2013  - Add getTableData(Long procedureId, 
 *                                                         Integer fieldCode,
 *                                                         Long procedureStepId) 
 * 
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.TableVO;
import ae.eis.util.dao.DataAccessObject;

import java.util.List;

/**
 * Procedure field data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureFieldDAO extends DataAccessObject {
    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     * @param acceptNull true:set new value even if null or false: keep old 
     * 
     * @return true if the field was updated successfully.
     */
    boolean update(ProcedureFieldVO fieldVO, boolean acceptNull);

    /**
     * Get procedure field info.
     * 
     * @param procedureId Procedure ID.
     * @param fieldCode Field code.
     * @param dao Data Access Object
     * @return procedure field info
     */
    ProcedureFieldVO getByCode(Long procedureId, Integer fieldCode);

    /**
     * Get procedure fields
     * 
     * @param procedureId Procedure ID.
     * @param codes Fields codes.
     * @return List of procedure fields.
     */
    List getProcedureFields(Long procedureId, Integer[] codes);

    /**
     * Get Field By Procedure Id
     * 
     * @return Procedure Field Value Object
     * @param fieldId field Id
     */
    ProcedureFieldVO getById(Long fieldId);

    /**
     * Get procedure search results fields.
     * 
     * @param procedureId Procedure ID.
     * @return Get procedure search results fields.
     */
    List getProcedureSearchResults(Long procedureId);
    
    
    
    /**
     * Get Table Date
     * 
     * @param procedureId Procedure Id
     * @param fieldCode Field Code
     * 
     * @return Table Value Object
     */
    TableVO getTableData(Long procedureId, Long fieldCode);   
     
	/**
	 * Get Table Date
	 * 
	 * @param procedureId Procedure Id
	 * @param fieldCode Field Code
     * @param procedureStepId Procedure Step Id
	 * 
     * @return Table Value Object
	 */
	TableVO getTableData(Long procedureId, Integer fieldCode,Long procedureStepId);
    
    
    /**
     * Insert
     * 
     * @param vo Procedure Field VO
     */
    void insert(ProcedureFieldVO vo);
    
}