/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateStepActionVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessObject;
import java.util.List;

/**
 * Procedure template step action data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateStepActionDAO extends DataAccessObject {

    /**
     * Create new procedure template step action.
     * 
     * @param actionsList Procedure template step actions List.
     */
    TemplateStepActionVO create(TemplateStepVO vo);
    
    /**
     * delete procedure template step action.
     * 
     * @param actionsList Procedure template step actions List.
     */
    void delete(TemplateStepVO vo);
    
    /**
     * get Step Type Actions By Id
     * 
     * @return List of Template Step Action Value Object
     * @param stepId
     */
    List getByStepId(Long stepId);
    
    /**
     * Can Update Attachment
     * - EPS_TSA_001
     * return TRUE if can add or delete attachment otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param stepId
     */
    boolean canUpdateAttachment(Long stepId);
    
    /**
     * Get requester allowed actions.
     * 
     * @param templateId Template ID.
     * @return  requester allowed actions.
     */
    List getRequesterActions(Long templateId);
    
    /**
     * Get Attachment Settings
     * 
     * @return List of step actions
     * @param stepId Step Id
     */
    List getAttachmentActions(Long stepId);
    
    /**
     * Update Attachment Actions
     * 
     * @param vo Template Step Value Object
     */
    void updateAttachmentActions(TemplateStepActionVO vo);

    /**
     * Get User Actions
     * 
     * @return List of step actions
     * @param stepId Step Id
     */
    List getUserActions(Long stepId);
    
}