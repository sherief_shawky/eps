/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  12/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.util.dao.DataAccessObject;
import java.util.List;

/**
 * Procedure step data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureStepDAO extends DataAccessObject {
    /**
     * Get active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @return active procedure step.
     */
    ProcedureStepVO getActiveStep(Long procedureId);

    /**
     * Get allowed step actions.
     * 
     * @param stepId Step ID.
     * @return allowed step actions.
     */
    List getStepActions(Long stepId);

    /**
     * Get allowed step actions for active procedure step.
     * 
     * @param procedureId Step ID.
     * @return allowed step actions.
     */
    List getActiveStepActions(Long procedureId);

    /**
     * Get approve step forwards.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    List getApproveStepForwards(Long stepId);
    
    /**
     * Get approve step forwards with no security.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    List getApproveStepForwardsNoSecurity(Long prdId , Long activeStepId);
    
    /**
     * Get step forwards.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    List getStepForwards(Long stepId);

    /**
     * Claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    boolean claimStep(Long procedureId, Long userId);
    
    /**
     * Claim procedure step with no security.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    boolean claimStepNoSecurity(Long procedureId, Long userId);
    
    /**
     * Un-claim active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    boolean unclaimActiveStep(Long procedureId, Long userId);
    
    /**
     * Un-claim active procedure step for Admin.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    boolean unclaimActiveForAdmin(Long procedureId, Long userId);

    /**
     * Un-claim old procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    boolean unclaimOldStep(Long procedureId, Long userId, Integer stepSeqNo);
    
    /**
     * Delete User Steps
     * 
     * @param procedureId
     */
    void deleteUserSteps(Long procedureId);
    
    /**
     * Add Procedure Steps 
     * 
     * @param procedureId
     * @param userIdAssingTo
     * @param userName
     */
    void addUserSteps(Long procedureId, Long userIdAssingTo, String userName);
    
    /**
     * Add User Steps without Assing To User ID
     * 
     * @param procedureId
     * @param userName
     */
    void addUserStepsNoAssingUsrId(Long procedureId, String userName);
    
    /**
     * Add Procedure Steps 
     * 
     * @param userIdAssingTo
     * @param userName
     */
    void addUserSteps(Long userIdAssingTo, String userName);
    
    /**
     * Delete User Steps
     * 
     * @param procedureId
     */
    void deletePrdUserStep(Long procedureId, Long userId, Long stepId);
}