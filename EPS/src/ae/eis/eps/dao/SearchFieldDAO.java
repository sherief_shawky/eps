/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          02/12/2009  - File created.
 */
 
package ae.eis.eps.dao;

import ae.eis.eps.vo.SearchFieldVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Search field data access object intefrace.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public interface SearchFieldDAO  extends DataAccessObject  {
   
   /**
     * Find search fields
     * 
     * @return searchPageVO search page value object
     * @param templateId template ID
     * @param pageNo page number
     */
    SearchPageVO find(int pageNo, Long templateId);
    
    /**
     * Delete search field 
     * 
     * @param templateId template ID
     * @param vo search field value object
     */
    void delete(SearchFieldVO vo,Long templateId);
    
    /**
     * Creat search field 
     * 
     * @param vo search field value object
     */
    public void create(SearchFieldVO vo);
    
    /**
     * Is search field exists 
     * 
     * @return true if exist
     * @param fieldId
     */
    boolean isSearchFieldExists(Long fieldId);
    
    /**
     * Get by ID
     * 
     * @return  search field value object
     * @param searchFieldId search field ID
     */
    SearchFieldVO getById(Long searchFieldId);
    
    /**
     * Update search field 
     * 
     * @param vo search field value object
     */
    void update(SearchFieldVO vo);
    
    /**
     * Is order number exists 
     * 
     * @return true if exist
     * @param templateId template ID
     * @param orderNo order number
     * @param searchFieldId search field ID
     */
     boolean isOrderNoExists(Long templateId, Integer orderNo,Long searchFieldId);

    /**
     * Get template search fields.
     * 
     * @param templateId EPS template ID.
     * @return List of search fields.
     */
    List getSearchFields(Long templateId);
    
    /**
     * Get Maximun Search Field Order Number
     * 
     * @return Maximun Search Field Order Number
     * @param templateId Template Id
     */
    Integer getMaxSearchFieldOrderNo(Long templateId);
}