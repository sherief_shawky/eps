/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  28/09/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.util.dao.DataAccessObject;

import java.util.List;

/**
 * Procedure step-users data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureStepUserDAO extends DataAccessObject {
    /**
     * Get step authorized users.
     * 
     * @param procedureId Procedure ID.
     * @param sequenceNo Step sequence number.
     * @return step authorized users.
     */
    List getStepUsers(Long procedureId, Integer sequenceNo);

    /**
     * Check if this is a valid step user.
     * 
     * @param stepId Step ID.
     * @param userId Step user ID.
     * @return true if this is a valid step user.
     */
    boolean isValidStepUser(Long stepId, Long userId);
}