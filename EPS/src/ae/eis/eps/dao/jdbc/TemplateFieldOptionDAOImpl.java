/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  04/06/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateFieldOptionDAO;
import ae.eis.eps.vo.TemplateFieldOptionVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Procedure template field data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateFieldOptionDAOImpl extends JdbcDataAccessObject  
                                        implements TemplateFieldOptionDAO {
    /*
     * JDBC SQL and DML statements.
     */

    /** Get field options. */
    private static final String GET_FIELD_OPTIONS
    = " SELECT TFO.ORDER_NO,"
    +        " TFO.VALUE,"
    +        " TFO.NAME_A,"
    +        " TFO.NAME_E,"
    +        " TFO.RELATED_ORDER_NO,"
    +        " TFO.RELATED_TPE_ID,"
    +        " (SELECT VALUE FROM TF_EPS_TEMPLATE_FIELD_OPTIONS"
    +         " WHERE ORDER_NO = TFO.RELATED_ORDER_NO"
    +           " AND TPE_ID = TFO.RELATED_TPE_ID) AS RELATED_OPTION_VALUE"
    + " FROM TF_EPS_TEMPLATE_FIELD_OPTIONS TFO"
    + "  WHERE TFO.TPE_ID = ?"
    + " ORDER BY TFO.ORDER_NO";

    /** Get field option by field id and order number. */
    private static final String GET_FIELD_OPTION
    = " SELECT TFO.ORDER_NO,"
    +        " TFO.VALUE,"
    +        " TFO.NAME_A,"
    +        " TFO.NAME_E,"
    +        " TFO.RELATED_ORDER_NO,"
    +        " TFO.RELATED_TPE_ID,"
    +        " RELATED_TPE.LABLE AS RELATED_TPE_LABLE,"
    +        " (SELECT VALUE"
    +         " FROM TF_EPS_TEMPLATE_FIELD_OPTIONS"
    +         " WHERE TPE_ID = TFO.RELATED_TPE_ID"
    +           " AND ORDER_NO = TFO.RELATED_ORDER_NO) AS RELATED_OPTION_VALUE"
    + " FROM TF_EPS_TEMPLATE_FIELD_OPTIONS TFO,"
    +      " TF_EPS_TEMP_PROCEDURE_FIELDS RELATED_TPE"
    + " WHERE TFO.RELATED_TPE_ID = RELATED_TPE.ID(+)"
    +   " AND TFO.TPE_ID = ?" // Template field ID
    +   " AND TFO.ORDER_NO = ?"; // Order Number

    /** Create Field Option */
    private static final String CREATE_FIELD_OPTION    
    = " INSERT INTO TF_EPS_TEMPLATE_FIELD_OPTIONS"
 	+ " (ORDER_NO, NAME_A, NAME_E, VALUE, TPE_ID, CREATED_BY, UPDATE_DATE,"
    +  " UPDATED_BY, CREATION_DATE, RELATED_TPE_ID, RELATED_ORDER_NO)"
	+ " VALUES(?," // order number
    +         "?," // name arabic
    +         "?," // name english
    +         "?," // value
    +         "?," // field id
    +         "?," // created by
    +         "SYSDATE," // updated date
    +         "?," // updated by
    +         "SYSDATE," // created date
    +         "?," // RELATED_TPE_ID
    +         "DECODE(?, NULL, NULL, "
    +            "(SELECT ORDER_NO FROM TF_EPS_TEMPLATE_FIELD_OPTIONS PARENT"
    +            " WHERE PARENT.TPE_ID = ? AND PARENT.VALUE = ?)))";

    /** Update Field Option */
    private static final String UPDATE_FIELD_OPTION    
    =  " UPDATE TF_EPS_TEMPLATE_FIELD_OPTIONS "
    +         " SET ORDER_NO = ?,"
	+         " NAME_A = ?,"
	+         " NAME_E = ?,"
	+         " VALUE = ?,"
	+         " UPDATE_DATE = SYSDATE,"
	+         " UPDATED_BY = ?,"
    +         " RELATED_TPE_ID = ?,"
    +         " RELATED_ORDER_NO = DECODE(?, NULL, NULL, "
    +                              "(SELECT ORDER_NO FROM TF_EPS_TEMPLATE_FIELD_OPTIONS PARENT"
    +                              " WHERE PARENT.TPE_ID = ? AND PARENT.VALUE = ?))"
    +   " WHERE TPE_ID = ?"
    +     " AND ORDER_NO = ?";
    
    /** Delete Field Option */
    private static final String DELETE_FIELD_OPTION
    = "DELETE FROM TF_EPS_TEMPLATE_FIELD_OPTIONS WHERE TPE_ID=? AND ORDER_NO=?";   
    
    /** Is Arabic Name Exist Query */
    private static final String IS_ARABIC_NAME_EXIST
    = "SELECT 1 FROM TF_EPS_TEMPLATE_FIELD_OPTIONS WHERE TPE_ID=? AND NAME_A=?";   
    
    /** Is Arabic Name Exist Query */
    private static final String IS_OPTION_EXIST
    = "SELECT 1 FROM TF_EPS_TEMPLATE_FIELD_OPTIONS WHERE TPE_ID=? AND ORDER_NO=?"; 

    /** Is Value Exist Query */
    private static final String IS_VALUE_EXIST
    = "SELECT 1 FROM TF_EPS_TEMPLATE_FIELD_OPTIONS WHERE TPE_ID=? AND VALUE = ? ";   
    
    /** Is English Name Exist Query */
    private static final String IS_ENGLISH_NAME_EXIST
    = "SELECT 1 FROM TF_EPS_TEMPLATE_FIELD_OPTIONS WHERE TPE_ID=? AND NAME_E=?";   
    
    /*
     * Methods
     */

    /**
     * Get template field options.
     * 
     * @param fieldId Template field ID.
     * @return template field options.
     */
    public List getFieldOptions(Long fieldId) {
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        debugQuery(GET_FIELD_OPTIONS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_FIELD_OPTIONS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            List optionsList = new ArrayList();
            while (rs.next()) {
                TemplateFieldOptionVO vo = new TemplateFieldOptionVO();
                TemplateFieldVO templateField = new TemplateFieldVO();
                TemplateFieldOptionVO relatedOption = new TemplateFieldOptionVO();
                TemplateFieldVO relatedField = new TemplateFieldVO();

                vo.setTemplateField(templateField);
                vo.setRelatedTemplateFieldOption(relatedOption);
                vo.getRelatedTemplateFieldOption().setTemplateField(relatedField);
                optionsList.add(vo);

                templateField.setId(fieldId);
                vo.setOrderNo(getInteger(rs, "ORDER_NO"));
                vo.setValue(rs.getString("VALUE"));
                vo.setNameAr(getString(rs, "NAME_A"));
                vo.setNameEn(getString(rs, "NAME_E"));

                vo.getRelatedTemplateFieldOption().setOrderNo(
                    getInteger(rs, "RELATED_ORDER_NO"));
                vo.getRelatedTemplateFieldOption().setValue(
                    getString(rs, "RELATED_OPTION_VALUE"));

                vo.getRelatedTemplateFieldOption().getTemplateField().setId(
                    getLong(rs, "RELATED_TPE_ID"));
            }

            return optionsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    
    /**
     * Find template field options. 
     * 
     * @return SearchPageVO Search Page Value Object
     * @param pageNo page Number
     * @param fieldId field Id
     */
    public SearchPageVO find(int pageNo,Long fieldId){

        List params=new ArrayList();
        params.add(fieldId);
        
        debugQuery(GET_FIELD_OPTIONS, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(
                                GET_FIELD_OPTIONS.toString() , params);
                                
            prepStmt =doSearch(GET_FIELD_OPTIONS.toString(), 
                                            params, pageNo , pageSize);
                                            
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO searchPage = 
                        new SearchPageVO(pageNo, pageSize ,totalRecordCounts);
                        
            while(resultSet.next()) {
                
                TemplateFieldOptionVO vo = new TemplateFieldOptionVO();
                vo.setOrderNo(getInteger(resultSet,"ORDER_NO"));
                vo.setNameAr(getString(resultSet,"NAME_A"));
                vo.setNameEn(getString(resultSet,"NAME_E"));
                vo.setValue(getString(resultSet,"VALUE"));

                searchPage.addRecord(vo);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }     
    }
    
    
    /**
     * Get template field option By Field Id and Order Number
     * 
     * @return TemplateFieldVO Template Field Value Object
     * @param orderNo Order Number
     * @param fieldId Field Id
     */
    public TemplateFieldOptionVO get(Long fieldId, Integer orderNo){
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        params.add(orderNo);
        debugQuery(GET_FIELD_OPTION, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_FIELD_OPTION);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            List optionsList = new ArrayList();
            if (! rs.next()) {
                return null;
            }

            TemplateFieldOptionVO vo = new TemplateFieldOptionVO();
            TemplateFieldVO templateField = new TemplateFieldVO();
            TemplateFieldOptionVO relatedFieldOption = new TemplateFieldOptionVO();
            TemplateFieldVO relatedFieldVO = new TemplateFieldVO();

            vo.setTemplateField(templateField);
            vo.setRelatedTemplateFieldOption(relatedFieldOption);
            vo.getRelatedTemplateFieldOption().setTemplateField(relatedFieldVO);
            optionsList.add(vo);

            templateField.setId(fieldId);
            vo.setOrderNo(getInteger(rs, "ORDER_NO"));
            vo.setValue(rs.getString("VALUE"));
            vo.setNameAr(getString(rs, "NAME_A"));
            vo.setNameEn(getString(rs, "NAME_E"));
            
            relatedFieldOption.setOrderNo(getInteger(rs, "RELATED_ORDER_NO"));
            relatedFieldOption.setValue(rs.getString("RELATED_OPTION_VALUE"));
            relatedFieldVO.setId(getLong(rs, "RELATED_TPE_ID"));
            relatedFieldVO.setLabel(rs.getString("RELATED_TPE_LABLE"));

            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    
    /**
     * Create Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    public void createOption(TemplateFieldOptionVO vo){
        // Debug query
        List params = new ArrayList();
        params.add(vo.getOrderNo());
        params.add(vo.getNameAr());
        params.add(vo.getNameEn());
        params.add(vo.getValue());
        params.add(vo.getTemplateField().getId());
        params.add(vo.getCreatedBy());
        params.add(vo.getUpdatedBy());
        
        if (vo.hasRelatedTemplateFieldOption()) {
            params.add(vo.getRelatedTemplateFieldOption().getTemplateField().getId());
            params.add(vo.getRelatedTemplateFieldOption().getValue());
            params.add(vo.getRelatedTemplateFieldOption().getTemplateField().getId());
            params.add(vo.getRelatedTemplateFieldOption().getValue());
        } else {
            params.add(null);
            params.add(null);
            params.add(null);
            params.add(null);
        }

        debugQuery(CREATE_FIELD_OPTION, params);
        PreparedStatement stm = null;
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_FIELD_OPTION);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }             
    }
    
    /**
     * Update Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    public void updateOption(TemplateFieldOptionVO vo,Integer oldOrderNo){
        // Debug query
        List params = new ArrayList();
        params.add(vo.getOrderNo());
        params.add(vo.getNameAr());
        params.add(vo.getNameEn());
        params.add(vo.getValue());
        params.add(vo.getUpdatedBy());
        
        if (vo.hasRelatedTemplateFieldOption()) {
            params.add(vo.getRelatedTemplateFieldOption().getTemplateField().getId());
            params.add(vo.getRelatedTemplateFieldOption().getValue());
            params.add(vo.getRelatedTemplateFieldOption().getTemplateField().getId());
            params.add(vo.getRelatedTemplateFieldOption().getValue());
        } else {
            params.add(null);
            params.add(null);
            params.add(null);
            params.add(null);
        }

        params.add(vo.getTemplateField().getId());
        params.add(oldOrderNo);

        debugQuery(UPDATE_FIELD_OPTION, params);
        PreparedStatement stm = null;
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_FIELD_OPTION);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if (count <= 0) {
                throw new DataAccessException(" Cannot Update Field Option ");
            }            
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Delete Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    public void deleteOption(TemplateFieldOptionVO vo){
        PreparedStatement stm = null;
        
        // Debug query
        List params = new ArrayList();
        params.add(vo.getTemplateField().getId());
        params.add(vo.getOrderNo());

        debugQuery(DELETE_FIELD_OPTION, params);
            
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_FIELD_OPTION);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }            
    }
    
    /**
     * Is Arabic Name Exist
     *  - EPS_TFO_004
     *  The Arabic name must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param nameAr Arabic Name
     */
    public boolean isArabicNameExist(Long fieldId,String nameAr,String oldNameAr){
        // Debug query
        List params = new ArrayList();
        StringBuffer sqlQuery = new StringBuffer(IS_ARABIC_NAME_EXIST);
        params.add(fieldId);
        params.add(nameAr);
        if(!isBlankOrNull(oldNameAr)){
            sqlQuery.append(" AND (NAME_A <> ?)");
            params.add(oldNameAr);     
        }
                
        debugQuery(sqlQuery.toString(), params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(sqlQuery.toString());
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if(!rs.next()) {
                return false;
            }

            return true;
                
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }           
    }
    
    /**
     * Is Option Exist
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param orderNo Order Number
     */
    public boolean isOptionExist(Long fieldId,Integer orderNo,String oldNameAr){
        // Debug query
        List params = new ArrayList();
        StringBuffer sqlQuery = new StringBuffer(IS_OPTION_EXIST);
        params.add(fieldId);
        params.add(orderNo);
        if(!isBlankOrNull(oldNameAr)){
            sqlQuery.append(" AND (NAME_A <> ?)");
            params.add(oldNameAr);     
        }        
        
        debugQuery(sqlQuery.toString(), params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(sqlQuery.toString());
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if(!rs.next()) {
                return false;
            }

            return true;
                
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }
    
    /**
     * Is Value Exist
     *  - EPS_TFO_006
     *  The Value must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param value Option Value
     */
    public boolean isValueExist(Long fieldId,String value,String oldValue){
        // Debug query
        List params = new ArrayList();
        StringBuffer sqlQuery = new StringBuffer(IS_VALUE_EXIST);
        params.add(fieldId);
        params.add(value);
        if(!isBlankOrNull(oldValue)){
            sqlQuery.append(" AND (VALUE <> ?)");
            params.add(oldValue);     
        }
                
        debugQuery(sqlQuery.toString(), params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(sqlQuery.toString());
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if(!rs.next()) {
                return false;
            }

            return true;
                
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }              
    }
    
    /**
     * Is English Name Exist
     *  - EPS_TFO_007
     *  The Arabic name must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param nameEn English Name
     */
    public boolean isEnglishNameExist(Long fieldId,String nameEn,String oldNameEn){
        // Debug query
        List params = new ArrayList();
        StringBuffer sqlQuery = new StringBuffer(IS_ENGLISH_NAME_EXIST);
        params.add(fieldId);
        params.add(nameEn);
        if(!isBlankOrNull(oldNameEn)){
            sqlQuery.append(" AND (NAME_E <> ?)");
            params.add(oldNameEn);     
        }
                
        debugQuery(sqlQuery.toString(), params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(sqlQuery.toString());
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if(!rs.next()) {
                return false;
            }

            return true;
                
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }               
    }
}