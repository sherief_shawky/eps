/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   30/12/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateTableHeaderDAO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateTableHeaderVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Template Table Header Data Access Object Implimentation
 * 
 * @version 1.0
 * @author Tariq Abu Amireh
 */
public class TemplateTableHeaderDAOImpl extends JdbcDataAccessObject
                                           implements TemplateTableHeaderDAO {
    
    /** Get By Field Id */
    private static final String GET_BY_FIELD_ID
        =   " SELECT ID,"
        +          " NAME_A,"
        +          " NAME_E,"
        +          " LABLE_WIDTH,"
        +          " COLUMN_TYPE,"
        +          " IS_HIDDEN,"
        +          " ORDER_NO,"
        +          " MAX_SIZE, "
        +          " CODE, "
        +          " IS_MANDATORY, "
        +          " (SELECT RV_MEANING "
        +          " FROM CG_REF_CODES "
        +          " WHERE RV_DOMAIN = 'TF_EPS_COLUMN_TYPE' "
        +          " AND RV_LOW_VALUE = COLUMN_TYPE) AS COLUMN_TYPE_DESC "
        +     " FROM TF_EPS_TEMP_TABLE_HEADERS "
        +    " WHERE TPE_ID = ? "
        +    " ORDER BY ORDER_NO " ;
                                           
    /** Get Table Header Maximum Order Number */
    private static final String GET_TABLE_HEADER_MAX_ORDER_NO
        =   " SELECT MAX(ORDER_NO) MAX_ORDER_NO "
        +     " FROM TF_EPS_TEMP_TABLE_HEADERS "
        +    " WHERE TPE_ID = ? ";
        
    /** Get Table Header Maximum Order Number */
    private static final String GET_TABLE_HEADER_MAX_CODE
        =   " SELECT MAX(CODE) MAX_CODE "
        +     " FROM TF_EPS_TEMP_TABLE_HEADERS "
        +    " WHERE TPE_ID = ? ";
                            
    /** Add  */
    private static final String ADD
        =   " INSERT INTO TF_EPS_TEMP_TABLE_HEADERS"
        +   "(ID,COLUMN_TYPE,NAME_A,LABLE_WIDTH,NAME_E,IS_HIDDEN,ORDER_NO,"
        +   " MAX_SIZE,TPE_ID,CREATED_BY,UPDATE_DATE,UPDATED_BY,CREATION_DATE,CODE,IS_MANDATORY) "
        +   " VALUES(?,"        // ID
        +           "?,"        // COLUMN_TYPE
        +           "?,"        // NAME_A
        +           "?,"        // LABLE_WIDTH
        +           "?,"        // NAME_E
        +           "?,"        // IS_HIDDEN
        +           "?,"        // ORDER_NO
        +           "?,"        // MAX_SIZE
        +           "?,"        // TPE_ID
        +           "?,"        // CREATED_BY
        +           "SYSDATE,"  // UPDATE_DATE
        +           "?,"        // UPDATED_BY
        +           "SYSDATE,"   // CREATION_DATE
        +           "?,"
        +           "?)";

                 
    /** Get By Id */
    private static final String GET_BY_ID
        =   " SELECT ID,"
        +          " NAME_A,"
        +          " NAME_E,"
        +          " LABLE_WIDTH,"
        +          " COLUMN_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          "('TF_EPS_COLUMN_TYPE',COLUMN_TYPE) COLUMN_TYPE_DESC," 
        +          " IS_HIDDEN,"
        +          " ORDER_NO,"
        +          " MAX_SIZE,"
        +          " TPE_ID, "
        +          " CODE, "
        +          " IS_MANDATORY "
        +     " FROM TF_EPS_TEMP_TABLE_HEADERS "
        +    " WHERE ID = ? ";
        
    /** Update */        
    private static final String UPDATE
        =   " UPDATE TF_EPS_TEMP_TABLE_HEADERS "
        +      " SET NAME_A = ?,"
        +          " NAME_E = ?,"
        +          " LABLE_WIDTH = ?,"
        +          " COLUMN_TYPE = ?,"
        +          " IS_HIDDEN = ?,"
        +          " ORDER_NO = ?,"
        +          " MAX_SIZE = ?,"
        +          " UPDATED_BY = ?,"
        +          " UPDATE_DATE = SYSDATE, "
        +          " CODE = ?, "
        +          " IS_MANDATORY = ? "
        +    " WHERE ID = ? " ;
        
    /** Delete */
    private static final String DELETE
        =   " DELETE FROM TF_EPS_TEMP_TABLE_HEADERS WHERE ID =  ? ";
        
    /** Is Arabic Name Exist */
    private static final String IS_ARABIC_NAME_EXIST
        =   " SELECT 1 "
        +     " FROM TF_EPS_TEMP_TABLE_HEADERS "
        +    " WHERE TPE_ID = ? "
        +      " AND NAME_A = ? "
        +      " AND ROWNUM = 1 ";

    /** Is English Name Exist */
    private static final String IS_ENGLISH_NAME_EXIST
        =   " SELECT 1 "
        +     " FROM TF_EPS_TEMP_TABLE_HEADERS "
        +    " WHERE TPE_ID = ? "
        +      " AND NAME_E = ? "
        +      " AND ROWNUM = 1 ";
        
    /** Is Order Number Exist */
    private static final String IS_ORDER_EXIST
        =   " SELECT 1 "
        +     " FROM TF_EPS_TEMP_TABLE_HEADERS "
        +    " WHERE TPE_ID = ? "
        +      " AND ORDER_NO = ? "
        +      " AND ROWNUM = 1 ";
        
    /** Field Template Has Header */        
    private static final String FIELD_TEMP_HAS_HEADER
        =   " SELECT 1 FROM TF_EPS_TEMP_TABLE_HEADERS WHERE TPE_ID = ? AND ROWNUM = 1 ";
        
    /** Template Tables Has Header */        
    private static final String TEMPLATE_TABLES_HAS_HEADER
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_PROCEDURE_TEMPLATES PTL,"
        +          " TF_EPS_TEMP_PROCEDURE_FIELDS TPE"
        +    " WHERE PTL_ID = ? "
        +      " AND PTL.ID = TPE.PTL_ID"
        +      " AND FIELD_TYPE = 10 "
        +      " AND NOT EXISTS(SELECT 1 FROM TF_EPS_TEMP_TABLE_HEADERS TTH "
        +                   " WHERE TPE.ID = TTH.TPE_ID)" ;
        
    /**
     * Find
     * 
     * @return list of table headers
     * @param fieldId Field Id
     */
    public SearchPageVO find(int pageNo, Long fieldId) {
        List params = new ArrayList();
        params.add(fieldId);
        debugQuery(GET_BY_FIELD_ID, params);
        
        ResultSet rs = null;
        PreparedStatement stm = null;
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(GET_BY_FIELD_ID, params);
            
            stm = doSearch(GET_BY_FIELD_ID, params, pageNo , pageSize);
            rs  = stm.executeQuery();
            
            SearchPageVO searchPage = 
                    new SearchPageVO(pageNo, pageSize ,totalRecordCounts);   
            
            while(rs.next()){
                TemplateTableHeaderVO vo = new TemplateTableHeaderVO();
                
                vo.setId(getLong(rs,"ID"));
                vo.setNameAr(getString(rs,"NAME_A"));
                vo.setNameEn(getString(rs,"NAME_E"));
                vo.setOrderNo(getLong(rs,"ORDER_NO"));
                vo.setColumnType(getLong(rs,"COLUMN_TYPE"));
                vo.setLableWidth(getLong(rs,"LABLE_WIDTH"));
                vo.setMaxSize(getLong(rs,"MAX_SIZE"));
                vo.setIsHidden(getInteger(rs,"IS_HIDDEN"));
                vo.setColumnTypeDesc(getString(rs, "COLUMN_TYPE_DESC"));
                vo.setCode(getInteger(rs, "CODE"));
                vo.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
                
                searchPage.addRecord(vo);

            }

            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }       
    }
    
    /**
     * Get Table Header Maximum Order Number
     * 
     * @return Table Header Maximum Order Number
     * @param fieldId Field Id
     */
    public Integer getTableHeaderMaxOrderNo(Long fieldId) {
        ArrayList params = new ArrayList();
        params.add(fieldId);
        debugQuery(GET_TABLE_HEADER_MAX_ORDER_NO, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        // default order number
        Integer maxOrderNo = new Integer(0);  
        
        try {
            prepStmt = getConnection().prepareStatement(GET_TABLE_HEADER_MAX_ORDER_NO);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
                maxOrderNo = getInteger(resultSet,"MAX_ORDER_NO");
                if(maxOrderNo == null)
                    maxOrderNo = new Integer(0);                
            }
        
            return maxOrderNo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Get Table Header Maximum Code
     * 
     * @param fieldId Field Id
     * @return Table Header Maximum Code
     */
    public Integer getTableHeaderMaxCode(Long fieldId) {
        ArrayList params = new ArrayList();
        params.add(fieldId);
        debugQuery(GET_TABLE_HEADER_MAX_CODE, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        // default order number
        Integer maxCode = new Integer(0);  
        
        try {
            prepStmt = getConnection().prepareStatement(GET_TABLE_HEADER_MAX_CODE);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
                maxCode = getInteger(resultSet,"MAX_CODE");
                if(maxCode == null)
                    maxCode = new Integer(0);                
            }
        
            return maxCode;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Add Template Table Header
     * 
     * @return table header id
     * @param vo Template Table Header Value Object
     */
    public Long add(TemplateTableHeaderVO vo) {
        
        Long newId = generateSequence("TTH_SEQ");
        ArrayList params = new ArrayList();
        params.add(newId);
        params.add(vo.getColumnType());
        params.add(vo.getNameAr());
        params.add(vo.getLableWidth());
        params.add(vo.getNameEn());
        params.add(vo.getIsHidden());
        params.add(vo.getOrderNo());
        params.add(vo.getMaxSize());
        params.add(vo.getTemplateField().getId());
        params.add(vo.getCreatedBy());
        params.add(vo.getUpdatedBy());
        params.add(vo.getCode());
        params.add(vo.getIsMandatory());
        
        debugQuery(ADD, params);     
        
        PreparedStatement prepStmt = null;
                
        try {
            prepStmt = getConnection().prepareStatement(ADD);
            setQueryParameters(prepStmt,params);
            prepStmt.executeUpdate();
            
        
            return newId;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(prepStmt);
        }             
    }
    
    /**
     * Get By Header Id
     * 
     * @return Template Table Header Value Object
     * @param headerId Header Id
     */
    public TemplateTableHeaderVO getById(Long headerId) {
        List params = new ArrayList();
        params.add(headerId);
        debugQuery(GET_BY_ID, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
                
        try {
            prepStmt = getConnection().prepareStatement(GET_BY_ID);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if( !resultSet.next() ){
                return null;
            }
        
            TemplateTableHeaderVO vo = new TemplateTableHeaderVO();
            vo.setTemplateField(new TemplateFieldVO());
            
            vo.setId(headerId);
            vo.setNameAr(getString(resultSet,"NAME_A"));
            vo.setNameEn(getString(resultSet,"NAME_E"));
            vo.setLableWidth(getLong(resultSet,"LABLE_WIDTH"));
            vo.setIsHidden(getInteger(resultSet,"IS_HIDDEN"));
            vo.setColumnType(getLong(resultSet,"COLUMN_TYPE"));
            vo.setColumnTypeDesc(getString(resultSet,"COLUMN_TYPE_DESC"));
            vo.setMaxSize(getLong(resultSet,"MAX_SIZE"));
            vo.setOrderNo(getLong(resultSet,"ORDER_NO"));
            vo.getTemplateField().setId(getLong(resultSet,"TPE_ID"));
            vo.setCode(getInteger(resultSet,"CODE"));
            vo.setIsMandatory(getInteger(resultSet, "IS_MANDATORY"));
            
            return vo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Update
     * 
     * @param vo Template Table Header Value Object
     */
    public void update(TemplateTableHeaderVO vo) {
        List params = new ArrayList();
        params.add(vo.getNameAr());
        params.add(vo.getNameEn());
        params.add(vo.getLableWidth());
        params.add(vo.getColumnType());
        params.add(vo.getIsHidden());
        params.add(vo.getOrderNo());
        params.add(vo.getMaxSize());
        params.add(vo.getUpdatedBy());
        params.add(vo.getCode());
        params.add(vo.getIsMandatory());
        params.add(vo.getId());
        
        debugQuery(UPDATE, params);     
        
        PreparedStatement prepStmt = null;
                
        try {
            prepStmt = getConnection().prepareStatement(UPDATE);
            setQueryParameters(prepStmt,params);
            int count = prepStmt.executeUpdate();
            
            if(count <= 0){
                throw new DataAccessException("UPDATE OPERATION FAILED " + vo.getId());
            }
            
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(prepStmt);
        }         
    }
    
    /**
     * Delete
     * 
     * @param headerId Header Id
     */
    public void delete(Long headerId) {
        List params = new ArrayList();
        params.add(headerId);
        
        debugQuery(DELETE, params);     
        
        PreparedStatement prepStmt = null;
                
        try {
            prepStmt = getConnection().prepareStatement(DELETE);
            setQueryParameters(prepStmt,params);
            int count = prepStmt.executeUpdate();
            
            if(count <= 0){
                throw new DataAccessException("DELETE OPERATION FAILED " + headerId);
            }
            
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(prepStmt);
        }         
    }
    
    /**
     * Is Arabic Name Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param nameA Arabic Name
     */
    public boolean isArabicNameExist(String nameA,Long fieldId,Long headerId) {
        List params = new ArrayList();
        params.add(fieldId);
        params.add(nameA);
        
        StringBuffer sql = new StringBuffer(IS_ARABIC_NAME_EXIST);
        if( headerId != null ) {
            sql.append(" AND ID <> ? ");
            params.add(headerId);       
        }

        debugQuery(sql.toString(), params);  
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
                
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if( resultSet.next() ){
                return true;
            }
        
            return false;
            
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Is Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param nameE English Name
     */
    public boolean isEnglishNameExist(String nameE,Long fieldId,Long headerId) {
        List params = new ArrayList();
        params.add(fieldId);
        params.add(nameE);
        
        StringBuffer sql = new StringBuffer(IS_ENGLISH_NAME_EXIST);
        if( headerId != null ) {
            sql.append(" AND ID <> ? ");
            params.add(headerId);       
        }

        debugQuery(sql.toString(), params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
                
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if( resultSet.next() ){
                return true;
            }
        
            return false;
            
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Is Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param orderNo order Number
     */
    public boolean isOrderExist(Long orderNo,Long fieldId,Long headerId) {
        List params = new ArrayList();
        params.add(fieldId);
        params.add(orderNo);

        StringBuffer sql = new StringBuffer(IS_ORDER_EXIST);
        if( headerId != null ) {
            sql.append(" AND ID <> ? ");
            params.add(headerId);       
        }
        
        debugQuery(sql.toString(), params);    

        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
                
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if( resultSet.next() ){
                return true;
            }
        
            return false;
            
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }    
    
    /**
     * Field Template Has Header
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     */
    public boolean isFieldTemplateHasHeader(Long fieldId) {
        List params = new ArrayList();
        params.add(fieldId);
        
        debugQuery(FIELD_TEMP_HAS_HEADER, params);    
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
                
        try {
            prepStmt = getConnection().prepareStatement(FIELD_TEMP_HAS_HEADER);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if( resultSet.next() ){
                return true;
            }
            
            return false;
            
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }        
    }
    
    /**
     * Is Template Tables Has Header
     * 
     * @return boolean ( true / false )
     * @param templateId Template Id
     */
    public boolean isTemplateTablesHasHeader(Long templateId) {
        List params = new ArrayList();
        params.add(templateId);
        
        debugQuery(TEMPLATE_TABLES_HAS_HEADER, params);    
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
                
        try {
            prepStmt = getConnection().prepareStatement(TEMPLATE_TABLES_HAS_HEADER);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if( !resultSet.next() ){
                return false;
            }
            
            return ( resultSet.getInt("COUNT") == 0 ) ;
            
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Is Column Mandatory
     * 
     * @param fieldId Field Id
     * @param columnCode Column Code
     * 
     * @return True if the column mandatory
     *         False if the column optional
     */
    public boolean isColumnMandatory(Long fieldId, Integer columnCode) {
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateTableHeader.isColumnMandatory"); 
        namedQuery.setParameter("fieldId", fieldId);
        namedQuery.setParameter("columnCode", columnCode); 
        
        // Debug Query
        debugQuery(namedQuery);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        try {
            prepStmt = prepareStatement(namedQuery);
            resultSet = prepStmt.executeQuery();
            
            Integer isMandatory = null;
            
            if (resultSet.next()) {
                isMandatory = getInteger(resultSet, "IS_MANDATORY");
                if (isMandatory != null && 
                    isMandatory.equals(TemplateTableHeaderVO.COLUMN_MANDATORY)) {
                    return true;
                }
            }
        
            return false;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }
    }
    
    /**
     * Get Headers
     * 
     * @param fieldCode Field Code
     * 
     * @return List Of Template Table Header Info
     */
    public List getHeaders(Integer fieldCode) {
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateTableHeader.getHeaders"); 
        namedQuery.setParameter("fieldCode", fieldCode);
        
        // Debug Query
        debugQuery(namedQuery);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        try {
            prepStmt = prepareStatement(namedQuery);
            resultSet = prepStmt.executeQuery();
            
            List headersList = new ArrayList();
            
            while (resultSet.next()) {
             
                // Initiate instance of type TemplateTableHeaderVO
                TemplateTableHeaderVO templateTableHeaderVO = 
                                        new TemplateTableHeaderVO();
                                
                // Assign values for the initiated object
                templateTableHeaderVO.setId(getLong(resultSet, "ID"));
                templateTableHeaderVO.setColumnType(getLong(resultSet, "COLUMN_TYPE"));
                templateTableHeaderVO.setNameAr(getString(resultSet, "NAME_A"));
                templateTableHeaderVO.setNameEn(getString(resultSet, "NAME_E"));
                templateTableHeaderVO.setLableWidth(getLong(resultSet, "LABLE_WIDTH"));
                templateTableHeaderVO.setIsHidden(getInteger(resultSet, "IS_HIDDEN"));
                templateTableHeaderVO.setOrderNo(getLong(resultSet, "ORDER_NO"));
                templateTableHeaderVO.setMaxSize(getLong(resultSet, "MAX_SIZE"));
                templateTableHeaderVO.setTemplateField(new TemplateFieldVO());
                templateTableHeaderVO.getTemplateField().setId(getLong(resultSet, "TPE_ID"));
                templateTableHeaderVO.setCode(getInteger(resultSet, "CODE"));
                templateTableHeaderVO.setIsMandatory(getInteger(resultSet, "IS_MANDATORY"));
            
                headersList.add(templateTableHeaderVO);
            }
            // Return templateTableHeaderVO instance
            return headersList;
        
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }
    }
}