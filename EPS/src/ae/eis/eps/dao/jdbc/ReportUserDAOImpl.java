/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   14/10/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.ReportUserDAO;
import ae.eis.eps.vo.ReportUserVO;
import ae.eis.eps.vo.TemplateReportVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Procedure template report-users access object JDBC implementation class.
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public class ReportUserDAOImpl extends JdbcDataAccessObject  
                                    implements ReportUserDAO {
    /*
     * JDBC SQL and DML statements.
     */


    /** Find report users by id. */
    private static final String FIND_REPORT_USERS
    = " SELECT EMP.NAME_A EMP_NAME_A, "
    +        " USR.NAME USR_NAME, "
    +        " USR.ID USR_ID "
    +   " FROM TF_EPS_REPORT_USERS ERU,"
    +        " SF_INF_USERS USR ,"
    +        " TF_STP_EMPLOYEES EMP,"
    +        " TF_STP_EMP_VS_USERS EVU"
    +  " WHERE ERU.USR_ID = USR.ID"
    +    " AND USR.ID = EVU.USR_USR_ID"
    +    " AND EVU.EMP_ID = EMP.ID"
    +    " AND ERU.ETR_ID = ?";
    
    /** Find report users lookup. */
    private static final String FIND_USERS
        = " SELECT USR.ID USER_ID,"
        +        " USR.NAME USR_NAME,"
        +        " EMP.ID EMP_ID,"
        +        " EMP.NAME_A EMP_NAME_A"
        +  " FROM SF_INF_USERS USR,"
        +       " TF_STP_EMPLOYEES EMP,"
        +       " TF_STP_EMP_VS_USERS EMP_USR"
        + " WHERE USR.STATUS = 2"
        +   " AND USR.ID = EMP_USR.USR_USR_ID"
        +   " AND EMP_USR.EMP_ID = EMP.ID"
        +   " AND NOT EXISTS(SELECT 1 FROM TF_EPS_REPORT_USERS ERU"
        +                          " WHERE ERU.USR_ID = USR.ID"
        +                            " AND ETR_ID = ?)  ";
    
    /** add security user */    
    private static final String ADD_SECURITY_USER
        = " INSERT INTO TF_EPS_REPORT_USERS"
        +       "(ETR_ID,USR_ID,CREATED_BY,"
        +        "CREATION_DATE,UPDATED_BY,UPDATE_DATE) "
        + " VALUES (? ,"
        +          "? ,"
        +          "? ,"
        +          "SYSDATE ,"
        +          "? ,"
        +          "SYSDATE)";
    
    /** delete security user */        
    private static final String DELETE_SECURITY_USER
        = " DELETE FROM TF_EPS_REPORT_USERS WHERE USR_ID = ? AND ETR_ID = ? ";
        
    /** is user exist */        
    private static final String IS_USER_EXIST
        = " SELECT 1 FROM TF_EPS_REPORT_USERS WHERE USR_ID = ? AND ETR_ID = ? ";
        
    /*
     * Methods
     */


    
    /**
     * Find report parameter
     * 
     * @return report parameters
     * @param pageNo pagination Page Number
     * @param reportId EPS report ID
     */
    public SearchPageVO find(Long reportId, int pageNo){
        List params = new ArrayList();
        params.add(reportId);      
        debugQuery(FIND_REPORT_USERS, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(FIND_REPORT_USERS, params);
            prepStmt =doSearch(FIND_REPORT_USERS, params, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                             totalRecordCounts);
            while(resultSet.next()) {
                ReportUserVO vo = new ReportUserVO();
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());
                vo.setReport(new TemplateReportVO());
                
                vo.getUser().setName(getString(resultSet,"USR_NAME"));
                vo.getUser().setId(getLong(resultSet,"USR_ID")); 
                vo.getUser().getEmployee().setEmployeeNameAr(getString(resultSet,"EMP_NAME_A"));
                
                searchPage.addRecord(vo); 
            }
            
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Find template step Security Groups
     * 
     * @return Search Page Value Object
     * @param vo Report User Value Object
     * @param pageNo page Number 
     */
    public SearchPageVO findUsers(ReportUserVO vo,int pageNo){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        StringBuffer sqlQuery = new StringBuffer(FIND_USERS);
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(vo.getReport().getId());
        
        if(vo.getUser() != null){
            if(vo.getUser().getId() != null ){
                sqlQuery.append(" AND USR.ID = ? ");
                listOfparams.add(vo.getUser().getId());
            }
            
            if(vo.getUser().getName() != null ){
                sqlQuery.append(" AND USR.NAME LIKE ? ");
                listOfparams.add("%"+vo.getUser().getName()+"%");
            }          
            
            if(vo.getUser().getEmployee() != null){
                if(vo.getUser().getEmployee().getId() != null){
                    sqlQuery.append(" AND EMP.ID = ? ");
                    listOfparams.add(vo.getUser().getEmployee().getId());
                }
                
                if(vo.getUser().getEmployee().getEmployeeNameAr() != null){
                   sqlQuery.append(" AND EMP.NAME_A LIKE ? ");
                   listOfparams.add("%"+vo.getUser().getEmployee().getEmployeeNameAr()+"%");
                }
            }
            
        }
        
        debugQuery(sqlQuery, listOfparams);
        
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString(),listOfparams);
            prepStmt = doSearch(sqlQuery.toString(),listOfparams,pageNo,pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO searchPage =
                        new SearchPageVO(pageNo, pageSize , totalRecordCounts);
                        
            while(resultSet.next()){                
                EmployeeVO employee = new EmployeeVO();
                employee.setId(getLong(resultSet,"EMP_ID"));
                employee.setEmployeeNameAr(getString(resultSet,"EMP_NAME_A"));
                
                UserVO user = new UserVO();
                user.setId(getLong(resultSet,"USER_ID"));
                user.setName(getString(resultSet,"USR_NAME"));
                user.setEmployee(employee);
                
                ReportUserVO reportUserVO = new ReportUserVO();
                reportUserVO.setUser(user);
                
                searchPage.addRecord(reportUserVO);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }
    
    /**
     * Add Security User
     * 
     * @param vo Report User Value Object
     */
    public void addSecurityUser(ReportUserVO vo){
        // Debug query
        List params = new ArrayList();
        params.add(vo.getReport().getId());
        params.add(vo.getUser().getId());
        params.add(vo.getCreatedBy());
        params.add(vo.getUpdatedBy());
        debugQuery(ADD_SECURITY_USER, params);

        PreparedStatement stm = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(ADD_SECURITY_USER);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        } 
        
    }
    
    /**
     * Delete Security User
     * 
     * @param reportId Report Id
     * @param userId User Id
     */
    public void deleteSecurityUser(Long userId,Long reportId){
        // Debug query
        List params = new ArrayList();
        params.add(userId);
        params.add(reportId);
        debugQuery(DELETE_SECURITY_USER, params);

        PreparedStatement stm = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(DELETE_SECURITY_USER);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        } 
    }
    
    /**
     * Is User Exist
     * 
     * @return true if the user is exist for current report, otherwise return false
     * @param reportId Report Id
     * @param userId User Id
     */
    public boolean isUserExist(Long userId,Long reportId){
        // Debug query
        List params = new ArrayList();
        params.add(userId);
        params.add(reportId);
        debugQuery(IS_USER_EXIST, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(IS_USER_EXIST);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if(rs.next()){
                return true;
            }
            
            return false;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        } 
    }
}