/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 * 
 * 1.01  Alaa Salem         07/01/2010  - Modify create() Method To Insert
 *                                        Transaction ID Into Procedures Table.
 * 1.02  Ali Abdel-Aziz     10/01/2010  - moving hasInstance query to xml.
 * 
 * 1.03  Alaa Salem         09/02/2010  - Moving ASSIGN_TASK To XML, And Update
 *                                        assignTask() Method Accordingly.
 * 1.04  Ali Abdel-Aziz     22/03/2010  - fixing appendSearchFieldSubQuery Like Search.
 * 
 * 1.05  Moh Fayek          16/07/2012  - TRF-5237
 * 
 * 1.06  Sami Abudayeh      27/06/2012  - Adding find(Long transactionId,int pageNo)
 * 
 * 1.07  Mohammad Ababneh   15/08/2012  - add - getDriverOrganizationId
 *                                            - getAssessmentDetails
 *                                            
 * 1.08  Mahmoud Atiyeh     15/05/2013  - Added getProceduresToBeMovedToDeliveryStep() method.
 * 
 * 1.09  Sami Abudayeh      19/09/2013   - Add assignCoordinatorTaskNote(ProcedureVO vo) method
 * 
 * 2.00  Bashar Alnemrawi   24/11/2013   - Add getAllApprovedNOCs() Method.
 */

package ae.eis.eps.dao.jdbc;

import isoft.com.util.common.ServiceLocator;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.TrafficFileVO;
import ae.eis.common.vo.UserVO; 
import ae.eis.eps.dao.ProcedureDAO;
import ae.eis.eps.vo.EpsVerifySearchVO;
import ae.eis.eps.vo.ProcedureStepActionVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.eps.vo.SearchFieldVO;
import ae.eis.eps.vo.TemplateFieldVO; 
import ae.eis.trs.vo.ServiceVO; 
import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile; 

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Procedure data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.06
 */
public class ProcedureDAOImpl extends JdbcDataAccessObject  
                              implements ProcedureDAO {
    /*
     * JDBC SQL and DML statements.
     */
  
    /** Create procedure DML. */
    private static final String CREATE_PROC
    = " {CALL PKG_EPS.CREATE_PROCEDURE(?,?,?,?,?,?,?,?)}";
    
    /** Close procedure DML. */
    private static final String CLOSE_PROC
    = " UPDATE TF_EPS_PROCEDURES"
    + " SET STATUS = DECODE(LAST_ACTION, 1, 2, 3),"
    +     " STATUS_DATE = SYSDATE,"
    +     " UPDATE_DATE = SYSDATE,"
    +     " ACTIVE_NOTE = NULL,"
    +     " UPDATED_BY = ?" // Username
    + " WHERE STATUS = 1"
    +   " AND ID = ?"; // Procedure ID
    
    /** Process user approve action. */
    private static final String DO_APPROVE
    = " UPDATE TF_EPS_PROCEDURES"
    + " SET LAST_ACTION_DATE = SYSDATE,"
    +     " LAST_NOTE = ?," // LAST_NOTE
    +     " LAST_ACTION = " + ProcedureStepActionVO.TYPE_APPROVE + ","
    +     " ACTIVE_STEP_SEQ_NO = ?," // ACTIVE_STEP_SEQ_NO
    +     " UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY = ?," // UPDATED_BY
    +     " ACTIVE_NOTE = NULL,"
    +     " USR_ID_ASSIGNED_TO = ?," // USR_ID_ASSIGNED_TO
    +     " LAST_HT_PRE_SEQ_NO = ACTIVE_STEP_SEQ_NO,"
    +     " LAST_HT_USR_ID = (SELECT ID FROM CI_UM_USERS"
    +                       " WHERE STATUS = 2"
    +                         " AND UPPER(USERNAME) = UPPER(?))" // UPDATED_BY
    + " WHERE STATUS = 1"
    +   " AND ID = ?"; // procedure ID

    /** Process user push-back action.. */
    private static final String DO_PUSH_BACK
    = " UPDATE TF_EPS_PROCEDURES PRD"
    + " SET LAST_ACTION_DATE = SYSDATE,"
    +     " LAST_NOTE = ?," // LAST_NOTE
    +     " LAST_ACTION = " + ProcedureStepActionVO.TYPE_PUSH_BACK + ","
    +     " UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY = ?," // UPDATED_BY
    +     " ACTIVE_NOTE = NULL,"
    +     " USR_ID_ASSIGNED_TO = (DECODE ((SELECT PTS.TASK_ASSIGNMENT_TYPE "  
    +     "                                  FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
    +     "                                 WHERE PTS.PTL_ID = PRD.PTL_ID "  
    +     "                                   AND PRD.LAST_HT_PRE_SEQ_NO = PTS.SEQ_NO),1,'',LAST_HT_USR_ID)),"
    +     " LAST_HT_PRE_SEQ_NO = ACTIVE_STEP_SEQ_NO,"
    +     " LAST_HT_USR_ID = (SELECT ID FROM CI_UM_USERS WHERE UPPER(USERNAME) = UPPER(?)),"
    +     " ACTIVE_STEP_SEQ_NO=NVL(LAST_HT_PRE_SEQ_NO, ACTIVE_STEP_SEQ_NO)"
    + " WHERE STATUS = 1"
    +   " AND ID = ?"; // procedure ID

    /** Process user reject action. */
    private static final String DO_REJECT
    = " UPDATE TF_EPS_PROCEDURES PRD"
    + " SET LAST_ACTION_DATE = SYSDATE,"
    +     " LAST_NOTE = ?," // LAST_NOTE
    +     " LAST_ACTION = " + ProcedureStepActionVO.TYPE_REJECT + ","
    +     " UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY = ?," // UPDATED_BY
    +     " ACTIVE_NOTE = NULL,"
    +     " USR_ID_ASSIGNED_TO = NULL,"
    +     " LAST_HT_PRE_SEQ_NO = ACTIVE_STEP_SEQ_NO,"
    +     " LAST_HT_USR_ID = (SELECT ID FROM CI_UM_USERS WHERE UPPER(USERNAME) = UPPER(?)),"
    +     " ACTIVE_STEP_SEQ_NO=(SELECT NEXT_PRE.SEQ_NO"
    +                         " FROM TF_EPS_PROCEDURE_STEPS PRE,"
    +                              " TF_EPS_STEP_FORWARDS SFD,"
    +                              " TF_EPS_PROCEDURES PRD2,"
    +                              " TF_EPS_PROCEDURE_STEPS NEXT_PRE"
    +                         " WHERE SFD.PRE_ID = PRE.ID"
    +                           " AND PRE.PRD_ID = PRD2.ID"
    +                           " AND PRE.SEQ_NO = PRD2.ACTIVE_STEP_SEQ_NO"
    +                           " AND SFD.PRE_ID_NEXT_STEP = NEXT_PRE.ID"
    +                           " AND SFD.ACTION_TYPE = " + ProcedureStepActionVO.TYPE_REJECT
    +                           " AND PRE.PRD_ID = PRD.ID)"
    + " WHERE STATUS = 1"
    +   " AND ID = ?"; // procedure ID

    /** Activate next procedure step. */
    private static final String ACTIVATE_NEXT_STEP
    = " UPDATE TF_EPS_PROCEDURES PRD"
    + " SET UPDATE_DATE = SYSDATE,"
    +     " ACTIVE_NOTE = NULL,"
    +     " UPDATED_BY = ?," // UPDATED_BY
    +     " ACTIVE_STEP_SEQ_NO=(SELECT NEXT_PRE.SEQ_NO"
    +                         " FROM TF_EPS_PROCEDURE_STEPS PRE,"
    +                              " TF_EPS_STEP_FORWARDS SFD,"
    +                              " TF_EPS_PROCEDURES PRD2,"
    +                              " TF_EPS_PROCEDURE_STEPS NEXT_PRE"
    +                         " WHERE SFD.PRE_ID = PRE.ID"
    +                               " AND PRE.PRD_ID = PRD2.ID"
    +                               " AND PRE.SEQ_NO = PRD2.ACTIVE_STEP_SEQ_NO"
    +                               " AND SFD.PRE_ID_NEXT_STEP = NEXT_PRE.ID"
    +                               " AND PRE.PRD_ID = PRD.ID)"
    + " WHERE STATUS = 1"
    +   " AND ID = ?"; // procedure ID

    /** Activate procedure step. */
    private static final String ACTIVATE_STEP
    = " UPDATE TF_EPS_PROCEDURES PRD"
    + " SET UPDATE_DATE = SYSDATE,"
    +     " ACTIVE_NOTE = NULL,"
    +     " UPDATED_BY = ?," // UPDATED_BY
    +     " ACTIVE_STEP_SEQ_NO = ?" // Step sequence number
    + " WHERE STATUS = 1"
    +   " AND ID = ?"; // procedure ID

    /** Is user action allowed query. */
    private static final String IS_USER_ACTION_ALLOWED
    =    " SELECT SUM(SEC_COUNT) FROM ( "
    +    " SELECT COUNT(1) AS SEC_COUNT "
    +    " FROM TF_EPS_PROCEDURES PRD, "
    +    "      TF_EPS_PROCEDURE_STEPS PRE, "
    +    "      TF_EPS_PROCEDURE_TEMPLATES PTL, "
    +    "      TF_EPS_PROCEDURE_TEMP_STEPS PTS, "
    +    "      TF_EPS_TEMP_STEP_USERS TSU, "
    +    "      CI_UM_USERS USR "
    +    " WHERE PRE.PRD_ID = PRD.ID "
    +    " AND   PRD.PTL_ID = PTL.ID "
    +    " AND   PTL.ID	 = PTS.PTL_ID "
    +    " AND   PTS.ID     = TSU.PTS_ID "
    +    " AND   TSU.USR_ID = USR.ID "
    +    " AND   PTS.SEQ_NO = PRE.SEQ_NO "
    +    " AND   PRE.SEQ_NO = PRD.ACTIVE_STEP_SEQ_NO "
    +    " AND   PRD.USR_ID_ASSIGNED_TO IS NULL "
    +    " AND   PRD.STATUS = " + ProcedureVO.STATUS_UNDER_PROCESSING
    +    " AND   USR.STATUS = 2 "
    +    " AND   PRD.ID = ? " // Procedure ID
    +    " AND   USR.ID = ? " // User ID
    +    " UNION "
    +    " SELECT COUNT(1) "
    +    " FROM TF_EPS_PROCEDURES PRD, "
    +    " CI_UM_USERS USR "
    +    " WHERE PRD.USR_ID_ASSIGNED_TO = USR.ID "
    +    " AND PRD.STATUS = " + ProcedureVO.STATUS_UNDER_PROCESSING
    +    " AND USR.STATUS = 2 "
    +    " AND PRD.ID = ? " // Procedure ID
    +    " AND USR.ID = ? " // User ID
    +    ")    ";
    
    /** Process user update action. */
    private static final String DO_UPDATE
    = " UPDATE TF_EPS_PROCEDURES"
    + " SET UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY = ?," // Username
    +     " ACTIVE_NOTE = ?" // User notes
    + " WHERE STATUS = 1"
    +   " AND ID = ?"; // Procedure ID
    

    /** Get Drl Statistics */
    private static final String FIND_DRL_STATISTICS
        = " SELECT PRD.ID,"
        + "        PRF.VALUE AS REPORT_NAME,"
        + "        PRD.STATUS,"
        + "        PRD.CREATION_DATE,"
        + "        PRD.LAST_ACTION_DATE,"
        + "        PRD.TEMPLATE_NAME,"
        + "        PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_PROCEDURE_STATUS',PRD.STATUS) STATUS_DESC "
        + "   FROM TF_EPS_PROCEDURES PRD,"
        + "        TF_EPS_PROCEDURE_TEMPLATES PTL,"
        + "        TF_EPS_TEMPLATE_REPORTS ETR,"
        + "        TF_EPS_PROCEDURE_FIELDS PRF,"
        + "        TF_EPS_REPORT_USERS ERU "
        + "  WHERE PRD.PTL_ID = PTL.ID"
        + "    AND ETR.PTL_ID = PTL.ID"
        + "    AND PRF.PRD_ID = PRD.ID"
        + "    AND ERU.ETR_ID = ETR.ID"
        + "    AND PRF.CODE = 1 "
        + "    AND ERU.USR_ID = ? " 
        + "    AND ETR.ID = ? ";

    /** Field Template Has Instance Query */
    private static final String FIELD_TEMPLATE_HAS_INSTANCE
        = " SELECT COUNT(*) COUNT "
        +   " FROM TF_EPS_PROCEDURES PRD,"
        +        " TF_EPS_PROCEDURE_TEMPLATES PTL,"
        +        " TF_EPS_PROCEDURE_FIELDS PRF "
        +  " WHERE PRD.PTL_ID = PTL.ID "
        +    " AND PRD.STATUS = 2 "
        +    " AND PRF.PRD_ID = PRD.ID "
        +    " AND PRF.CODE = (SELECT CODE FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE "
        +                     " WHERE TPE.ID = ? AND TPE.PTL_ID = PTL.ID) ";

    /** Get template version count */
    private static final String GET_TEMPLATE_VERSION_COUNT
    = " SELECT COUNT(*) "
    + " FROM TF_EPS_PROCEDURES "
    + " WHERE PTL_ID = ? "
    +   " AND TEMP_VERSION_NO = ?";
    
    /*
     * Methods
     */

    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object.
     * @param tempAttachmentsRefNo Temporal attachments reference numbet.
     * @return Created procedure ID.
     */
    public Long create(ProcedureVO vo, Long tempAttachmentsRefNo) {
        CallableStatement call = null;
        try {
            call = getConnection().prepareCall(CREATE_PROC);

            // Registre CallableStatement parameters
            setLong(  call, 1, vo.getTemplate().getId());
            setString(call, 2, vo.getRequesterEmail());
            setString(call, 3, vo.getRequesterMobile());
            setLong(  call, 4, vo.getRequester().getId());
            setLong(  call, 5, tempAttachmentsRefNo);
            call.registerOutParameter(6 , Types.VARCHAR);
             
                setLong(call, 7, null);
           
               
            if(vo.getTrafficFile() != null) {
                setLong(call, 8, vo.getTrafficFile().getId());
            } else {
                setLong(call, 8, null);
            }
            
            call.execute();

            // Return created procedure ID
            return new Long(getString(call, 6));

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(call);
        }
    }

    /**
     * Close procedure.
     * 
     * @param procedureId Procedure ID.
     * @param username Current active employee username.
     * @return True if the procedure were closed successfully.
     */
    public boolean closeProcedure(Long procedureId, String username) {
        // Debug query
        List params = new ArrayList();
        params.add(username);
        params.add(procedureId);
        debugQuery(CLOSE_PROC, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(CLOSE_PROC);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();

            return count > 0;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Append search field "date period" sub-query to main search query.
     * 
     * @param mainNamedQuery Main Named Query.
     * @param vo procedure search field value object.
     */
    private void appendDatePeriodSearchFieldSubQuery(NamedQuery mainNamedQuery,
                                                     SearchFieldVO vo) {
        // Validate search field value
        if (vo.getDateFrom() == null && vo.getDateTo() == null) {
            return;
        }

        // Build sub-query
        NamedQuery subNamedQuery = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.searchDateFieldSubQuery");
        subNamedQuery.appendWhereClause(" AND S_PRF.FIELD_TYPE = " + TemplateFieldVO.TYPE_DATE);

        String fieldCodeParamterName = "fieldCode"+"_"+vo.getTemplateField().getCode();
        String dateFromParamterName = "dateFrom"+"_"+vo.getTemplateField().getCode();
        String dateToParamterName = "dateTo"+"_"+vo.getTemplateField().getCode();

        subNamedQuery.appendWhereClause(" AND S_PRF.CODE = :"+fieldCodeParamterName);
        mainNamedQuery.setParameter(fieldCodeParamterName, 
                                    vo.getTemplateField().getCode().toString());

        if (vo.getDateFrom() != null) {
            subNamedQuery.appendWhereClause(" AND TO_DATE(S_PRF.VALUE, 'dd-mm-yyyy') >= :" + 
                                            dateFromParamterName);
            mainNamedQuery.setParameter(dateFromParamterName,
                                            vo.getDateFrom());
        }

        if (vo.getDateTo() != null) {
            subNamedQuery.appendWhereClause(" AND TO_DATE(S_PRF.VALUE, 'dd-mm-yyyy') <= :" + 
                                            dateToParamterName);
            mainNamedQuery.setParameter(dateFromParamterName, vo.getDateTo());
        }

        subNamedQuery.appendWhereClause(") ");

        // Append sub-query to main search query
        mainNamedQuery.appendWhereClause(subNamedQuery.getQuery());
    }
    
    /**
     * Append search field sub-query to main named query.
     * 
     * @param mainNamedQuery Main search query.
     * @param vo procedure search field value object.
     */
    private void appendSearchFieldSubQuery(NamedQuery mainNamedQuery,
                                           SearchFieldVO vo) {
        // Validate search field value
        if (vo == null) {
            throw new DataAccessException("NULL SearchFieldVO");
        }

        if (vo.getTemplateField() == null) {
            throw new DataAccessException("NULL SearchFieldVO.templateField");
        }

        if (vo.getTemplateField().getCode() == null) {
            throw new DataAccessException("NULL SearchFieldVO.templateField.code");
        }

        if (vo.getTemplateField().getType() == null) {
            throw new DataAccessException("NULL SearchFieldVO.templateField.type");
        }

        // Check if field type is "Date" and search type is between
        if (vo.getTemplateField().isDateField() && vo.isSearchTypeBetween()) {
            appendDatePeriodSearchFieldSubQuery(mainNamedQuery, vo);
            return;
        }

        // Skip if field value was not initialized
        if (vo.getTemplateField().getValue() == null ||
            isBlankOrNull(vo.getTemplateField().getValue().toString())) {
            return;
        }

        // Build sub-query
        NamedQuery subNamedQuery = null;
        if (!vo.getTemplateField().isTableField()) {
        
            subNamedQuery = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.searchFieldSubQuery");
            String fieldCodeParamterName = "fieldCode"+"_"+vo.getTemplateField().getCode();
            String fieldValueParameterName = "fieldValue"+"_"+vo.getTemplateField().getCode();
        
            subNamedQuery.appendWhereClause(" AND S_PRF.CODE = :"+fieldCodeParamterName+" ");
            mainNamedQuery.setParameter(fieldCodeParamterName, 
                                        vo.getTemplateField().getCode());
        
            if (vo.isSearchTypeEqual()) {
                subNamedQuery.appendWhereClause(" AND S_PRF.VALUE = :" + fieldValueParameterName);
                mainNamedQuery.setParameter(fieldValueParameterName, 
                                            vo.getTemplateField().getValue());

            } else if (vo.isSearchTypeLike()) {
                subNamedQuery.appendWhereClause(" AND S_PRF.VALUE LIKE :"+fieldValueParameterName);
                mainNamedQuery.setParameter(fieldValueParameterName, "%" + 
                                        vo.getTemplateField().getValue() + "%");
            } else {
                throw new DataAccessException("Invalid search type: " + vo.getSearchType());
            }
            
        } else {
            // Skip if getTemplateTableHeaderVO was not initialized
            if (vo.getTemplateTableHeaderVO()== null) {
                throw new DataAccessException("NULL Template Table Header VO");
            }
            
            if (vo.getTemplateTableHeaderVO().getCode() == null) {
                throw new DataAccessException("NULL Template Table Header Code");
            }
            
            subNamedQuery = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.searchColumnSubQuery");
            
            String fieldCodeParamterName = "fieldCode"+"_"+vo.getTemplateField().getCode();
            String fieldValueParameterName = "fieldValue"+"_"+vo.getTemplateField().getCode();
            String columnCodeParameterValue = "columnCode" + "_" + vo.getTemplateTableHeaderVO().getCode();
        
            subNamedQuery.appendWhereClause(" AND S_PRF.CODE = :" + fieldCodeParamterName + " ");
            mainNamedQuery.setParameter(fieldCodeParamterName, 
                                        vo.getTemplateField().getCode());
                                        
            subNamedQuery.appendWhereClause(" AND S_THD.CODE = :" + columnCodeParameterValue + " ");
            mainNamedQuery.setParameter(columnCodeParameterValue, 
                                        vo.getTemplateTableHeaderVO().getCode());
        
            if (vo.isSearchTypeEqual()) {
                subNamedQuery.appendWhereClause(" AND S_TCU.VALUE = :"+fieldValueParameterName);
                mainNamedQuery.setParameter(fieldValueParameterName, 
                                            vo.getTemplateField().getValue());

            } else if (vo.isSearchTypeLike()) {
                subNamedQuery.appendWhereClause(" AND S_TCU.VALUE LIKE :"+fieldValueParameterName);
                mainNamedQuery.setParameter(fieldValueParameterName, "%" + 
                                        vo.getTemplateField().getValue() + "%");
            } else {
                throw new DataAccessException("Invalid search type: " + vo.getSearchType());
            }
        }
        subNamedQuery.appendWhereClause(") ");
        // Append sub-query to main search query
        mainNamedQuery.appendWhereClause(subNamedQuery.getQuery());
    }
    
    /**
     * Search for user procedures requests.
     * 
     * @param vo Procedure value object.
     * @param pageNo Search page number.
     * @param fromDate Start search date.
     * @param toDate End search date.
     * @return Search results.
     */ 
    public SearchPageVO findUserRequests(int pageNo, ProcedureVO vo, 
                                         Date fromDate, Date toDate,
                                         List searchFields) {
        // Build query
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.findUserRequests");
        query.setParameter("createdBy", vo.getCreatedBy());

        if (vo.getId() != null ) {
            query.appendWhereClause(" AND ID = :procedureId ", "procedureId", vo.getId());
        }
        
        if (vo.getStatus() != null ) {
            query.appendWhereClause(" AND STATUS = :procedureStatus ", 
                                    "procedureStatus", vo.getStatus());
        }
        
        if (vo.getPriority() != null ) {
            query.appendWhereClause(" AND PRIORITY = :priority ", 
                                    "priority", vo.getPriority());
        }
        
        if (vo.getTemplate().getId() != null ) {
            query.appendWhereClause("  AND PTL_ID = :templateId ", "templateId", 
                                    vo.getTemplate().getId());
        }
        
        if (fromDate != null) {
            query.appendWhereClause(" AND CREATION_DATE >= :fromDate ", 
                                    "fromDate", fromDate);
        }
        
        if (toDate != null) {
            query.appendWhereClause(" AND CREATION_DATE < :toDate + 1 ", 
                                    "toDate", toDate);
        }

        // Add search fields filters code
        for (int i = 0; searchFields!= null && i < searchFields.size(); i++) {
            SearchFieldVO searcfFieldVO = (SearchFieldVO) searchFields.get(i);
            appendSearchFieldSubQuery(query, searcfFieldVO);
        }

        query.appendWhereClause(" ORDER BY ID ");
        
        // Debug query
        debugQuery(query.getSqlQuery(), query.getSqlParameters());
        
        PreparedStatement stmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(query);
            stmt = doSearch(query, pageNo, pageSize);
            resultSet = stmt .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, 
                                                      pageSize,
                                                      totalRecordCounts);
            while(resultSet.next()) {
                ProcedureVO procedureVO = new ProcedureVO();
                procedureVO.setRequester(new UserVO());

                procedureVO.setId(getLong(resultSet,"ID"));
                procedureVO.setTemplateNameAr(getString(resultSet,"TEMPLATE_NAME"));
                procedureVO.setStatus(getInteger(resultSet,"STATUS"));
                procedureVO.setStatusDesc(getString(resultSet,"STATUS_DESC"));
                procedureVO.setCreationDate(resultSet.getDate("CREATION_DATE"));
                procedureVO.setPriority(getInteger(resultSet,"PRIORITY"));
                procedureVO.setPriorityDesc(getString(resultSet,"PRIORITY_DESC"));
                procedureVO.setLastActionDate(getDate(resultSet,"LAST_ACTION_DATE"));
                procedureVO.setCreationDate(getDate(resultSet,"CREATION_DATE"));
                procedureVO.setStatusDate(resultSet.getDate("STATUS_DATE"));
                
                searchPage.addRecord(procedureVO);
            }

            return searchPage;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(stmt);
        }        
    }

    /**
     * Process user approve action.
     * 
     * @param vo Procedure value object.
     */
    public void approve(ProcedureVO vo) {
        // Debug query
        List params = new ArrayList();
        params.add(vo.getLastNote());
        params.add(vo.getActiveStep().getSequenceNo());
        params.add(vo.getUpdatedBy());
        params.add(vo.getAssignedToUserId());
        params.add(vo.getUpdatedBy());
        params.add(vo.getId());
        debugQuery(DO_APPROVE, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DO_APPROVE);
            stm.setString(1, vo.getLastNote());
            setInteger(stm, 2, vo.getActiveStep().getSequenceNo());
            setString(stm, 3, vo.getUpdatedBy());
            setLong(stm, 4, vo.getAssignedToUserId());
            setString(stm, 5, vo.getUpdatedBy());
            setLong(stm, 6, vo.getId());

            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(
                    "Active procedure not found, ID = " + vo.getId());
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Activate next procedure step.
     * 
     * @param procedureId Procedure ID.
     */
    public void activateNextStep(Long procedureId, String username) {
        // Debug query
        List params = new ArrayList();
        params.add(username);
        params.add(procedureId);
        debugQuery(ACTIVATE_NEXT_STEP, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(ACTIVATE_NEXT_STEP);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(
                    "Next active procedure step not found, ID = " + procedureId);
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Activate next procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param stepSeqNo next step sequence number.
     */
    public void activateStep(Long procedureId, Integer stepSeqNo, String username) {
        // Debug query
        List params = new ArrayList();
        params.add(username);
        params.add(stepSeqNo);
        params.add(procedureId);
        debugQuery(ACTIVATE_STEP, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(ACTIVATE_STEP);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(
                    "failed to activate step, step no = " + stepSeqNo);
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Process user push-back action.
     * 
     * @param vo Procedure step value object.
     */
    public void pushBack(ProcedureVO vo) {
        // Debug query
        List params = new ArrayList();
        params.add(vo.getLastNote());
        params.add(vo.getUpdatedBy());
        params.add(vo.getUpdatedBy());
        params.add(vo.getId());
        debugQuery(DO_PUSH_BACK, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DO_PUSH_BACK);
            stm.setString(1, vo.getLastNote());
            setString(stm, 2, vo.getUpdatedBy());
            setString(stm, 3, vo.getUpdatedBy());
            setLong(stm, 4, vo.getId());

            int count = stm.executeUpdate();
            if (count <= 0) {
                throw new DataAccessException(
                    "Active procedure not found, ID = " + vo.getId());
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Process user reject action.
     * 
     * @param vo Procedure step value object.
     */
    public void reject(ProcedureVO vo) {
         // Debug query
        List params = new ArrayList();
        params.add(vo.getLastNote());
        params.add(vo.getUpdatedBy());
        params.add(vo.getUpdatedBy());
        params.add(vo.getId());
        debugQuery(DO_REJECT, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DO_REJECT);
            stm.setString(1, vo.getLastNote());
            setString(stm, 2, vo.getUpdatedBy());
            setString(stm, 3, vo.getUpdatedBy());
            setLong(stm, 4, vo.getId());

            int count = stm.executeUpdate();
            if (count <= 0) {
                throw new DataAccessException(
                    "Active procedure not found, ID = " + vo.getId());
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Process user reject action with no security.
     *
     * @param vo Procedure value object.
     */
    public void rejectNoSecurity(ProcedureVO vo) {

        // Get Named Query.
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.doRejectNoSecurity");

        // Set Named Query Parameter.
        query.setParameter("lastNote", vo.getLastNote());
        query.setParameter("updatedBy", vo.getUpdatedBy());
        query.setParameter("name", vo.getUpdatedBy());
        query.setParameter("procedureId", vo.getId());

        // Debug Query.
        debugQuery(query);
        PreparedStatement stmt = null;

        try {
            // Execute DML statement
            stmt = prepareStatement(query);
            stmt.executeUpdate();
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stmt);
        }
    }

    /**
     * Process user approve Completed Procedure action.
     *
     * @param vo Procedure value object.
     */
    public void approveCompletedProcedure(ProcedureVO vo) {

        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.doApproveCompletedProcedure");

        // Set Named Query Parameter.
        query.setParameter("activeStepSeqNo", vo.getActiveStep().getSequenceNo());
        query.setParameter("updatedBy", vo.getUpdatedBy());
        query.setParameter("procedureId", vo.getId());

        // Debug Query.
        debugQuery(query);
        PreparedStatement stmt = null;

        try {
            // Execute DML statement
            stmt = prepareStatement(query);
            stmt.executeUpdate();
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stmt);
        }
    }
        
    /**
     * Get procedure info.
     * 
     * @param procedureId Procedure ID
     * @return Procedure value object.
     */
    public ProcedureVO getById(Long procedureId) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getById");
            query.setParameter("procedureId", procedureId);
            debugQuery(query); // Debug message
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            if (! rs.next()) {
                return null;
            }

            ProcedureVO vo = new ProcedureVO();
            vo.setLastHumanTaskStep(new ProcedureStepVO());
            vo.setActiveStep(new ProcedureStepVO());
            vo.setRequester(new UserVO());
            vo.getRequester().setEmployee(new EmployeeVO());
            vo.setId(procedureId);
            vo.setAssignedTo(new UserVO());
            vo.setTrafficFile(new TrafficFileVO());
            vo.setTemplate(new ProcedureTemplateVO());
            
            // Set procedure info
            vo.setCreatedBy(getString(rs, "CREATED_BY"));
            vo.setStatusDesc(getString(rs, "STATUS_DESC"));
            vo.setPriorityDesc(getString(rs, "PRIORITY_DESC"));
            vo.setTemplateNameAr(getString(rs, "TEMPLATE_NAME"));
            vo.setCode(getInteger(rs, "CODE"));
            vo.setRequesterEmail(getString(rs, "REQUESTER_EMAIL"));
            vo.setRequesterMobile(getString(rs, "REQUESTER_MOBILE_NO"));
            
            vo.setRequestorNameAr(getString(rs, "REQUESTER_NAME_A"));
            vo.setRequestorNameEn(getString(rs, "REQUESTOR_NAME_E"));
            
            vo.setCreationDate(getDate(rs, "CREATION_DATE"));
            vo.setStatusDate(getDate(rs, "STATUS_DATE"));
            vo.setLastActionDate(getDate(rs, "LAST_ACTION_DATE"));
            vo.setStatus(getInteger(rs, "STATUS"));
            vo.setPriority(getInteger(rs, "PRIORITY"));
            vo.setLastNote(rs.getString("LAST_NOTE"));
            vo.setLastAction(getInteger(rs, "LAST_ACTION"));
            vo.setLastActionDesc(getString(rs, "LAST_ACTION_DESC"));
            vo.setActiveNote(rs.getString("ACTIVE_NOTE"));
            vo.setConsumesTimeInMinutes(getLong(rs, "CONSUMES_TIME_IN_MINUTES"));
            vo.getAssignedTo().setId(getLong(rs, "USR_ID_ASSIGNED_TO"));
            vo.getTemplate().setId(getLong(rs,"PTL_ID"));

            vo.setRequesterAttachmentsCount(
                getInteger(rs, "REQUESTER_ATTACHMENTS_COUNT"));

            vo.setUsersAttachmentsCount(
                getInteger(rs, "USERS_ATTACHMENTS_COUNT"));

            vo.getRequester().getEmployee().setEmployeeNameAr(
               getString(rs, "REQUESTER_NAME_A"));
            vo.getRequester().setId(getLong(rs, "REQUESTER_ID"));
            // Set active step info
            vo.getActiveStep().setNameAr(getString(rs, "ACTIVE_STEP_NAME"));
            vo.getActiveStep().setId(getLong(rs, "ACTIVE_STEP_ID"));
            vo.getActiveStep().setClaimedBy(new UserVO());
            vo.getActiveStep().getClaimedBy().setId(getLong(rs, "CLAIMED_BY_USR_ID"));
            vo.getActiveStep().getClaimedBy().setEmployee(new EmployeeVO());
            vo.getActiveStep().getClaimedBy().getEmployee().setEmployeeNameAr(getString(rs,"CLAIMED_BY_EMP_NAME_A"));
            vo.getActiveStep().setTaskAssignmentType(getInteger(rs, "TASK_ASSIGNMENT_TYPE"));
            vo.getActiveStep().setViewType(getInteger(rs, "VIEW_TYPE"));
            vo.getActiveStep().setViewLink(getString(rs, "VIEW_LINK"));
            vo.getActiveStep().setSequenceNo(getInteger(rs, "SEQ_NO"));

            // Set last human task info
            Integer lastHumanTaskSeqNo = getInteger(rs, "LAST_HT_PRE_SEQ_NO");
            vo.getLastHumanTaskStep().setStepType(getInteger(rs, "LAST_HT_STEP_TYPE"));
            if (lastHumanTaskSeqNo != null && vo.getLastHumanTaskStep().isHumanTaskStep()) {
                vo.getLastHumanTaskStep().setNameAr(getString(rs, "LAST_HT_STEP_NAME"));
                vo.getLastHumanTaskStep().setEmployeeName(getString(rs, "LAST_EMP_NAME"));
                vo.getLastHumanTaskStep().setSequenceNo(lastHumanTaskSeqNo);
                vo.getLastHumanTaskStep().setClaimedBy(new UserVO());
                vo.getLastHumanTaskStep().getClaimedBy().setId(getLong(rs,"LAST_HT_USR_ID"));
            }

            
            // set traffic file info
            vo.getTrafficFile().setId(getLong(rs,"CUSTOMER_ID"));
            
            
            if(!isBlankOrNull(getString(rs,"USR_ASSIGNED_TO"))) {
                 vo.setUserAssignedTo(getString(rs,"USR_ASSIGNED_TO"));
            }
            vo.setCurrDateTime(getDate(rs, "CURR_DATE"));
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }

    /**
     * Find work list procedures.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @param actionDateFrom Last action date start search.
     * @param actionDateTo Last action date end search.
     * @return Search results.
     */
    public SearchPageVO findWorklistProcedures(
                        int pageNo, ProcedureVO procedureVO, Long userId,
                        Date creationDateFrom, Date creationDateTo,
                        Date actionDateFrom, Date actionDateTo,
                        List searchFields) {

        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.findWorklistProcedures");
        query.setParameter("userId", userId);
        
        // Append worklist procedures where clause
        if (creationDateFrom != null) {
            query.appendWhereClause(" AND PRD.CREATION_DATE >= :creationDateFrom ");
            query.setParameter("creationDateFrom", creationDateFrom);
        }

        if (creationDateTo != null) {
            query.appendWhereClause(" AND PRD.CREATION_DATE < :creationDateTo + 1");
            query.setParameter("creationDateTo", creationDateTo);
        }

        if (actionDateFrom != null) {
            query.appendWhereClause(" AND PRD.LAST_ACTION_DATE >= :actionDateFrom ");
            query.setParameter("actionDateFrom", actionDateFrom);
        }

        if (actionDateTo != null) {
            query.appendWhereClause(" AND PRD.LAST_ACTION_DATE < :actionDateTo + 1");
            query.setParameter("actionDateTo", actionDateTo);
        }

        if (procedureVO.getId() != null) {
            query.appendWhereClause(" AND PRD.ID = :procedureId ");
            query.setParameter("procedureId", procedureVO.getId());
        }
 
        if (procedureVO.getPriority() != null) {
            query.appendWhereClause(" AND PRD.PRIORITY = :priority ");
            query.setParameter("priority", procedureVO.getPriority());
        }

        if (procedureVO.getTemplate() != null && procedureVO.getTemplate().getId() != null) {
            query.appendWhereClause(" AND PRD.PTL_ID = :templateId ");
            query.setParameter("templateId", procedureVO.getTemplate().getId());
        }

        if (! isBlankOrNull(procedureVO.getCreatedBy())) {
            query.appendWhereClause(" AND UPPER(PRD.CREATED_BY) LIKE UPPER(:procedureCreatedBy) ");
            query.setParameter("procedureCreatedBy", "'%"+procedureVO.getCreatedBy()+"%'");
        }

        if ( procedureVO.getTrafficFile() != null && procedureVO.getTrafficFile().getId() != null) {
            query.appendWhereClause(" AND PRD.CUSTOMER_ID = :trafficId ");
            query.setParameter("trafficId", procedureVO.getTrafficFile().getId() );
        }

         
        // Add search fields filters code
        for (int i = 0; searchFields!= null && i < searchFields.size(); i++) {
            SearchFieldVO searcfFieldVO = (SearchFieldVO) searchFields.get(i);
            appendSearchFieldSubQuery(query, searcfFieldVO);
        }

        // Append order by query
        query.appendWhereClause(" ORDER BY STATUS, PRIORITY, LAST_ACTION_DATE ");
 
        // Debug query
        debugQuery(query.getSqlQuery(), query.getSqlParameters());
        
        PreparedStatement stm = null;
        ResultSet rs = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(query);
            stm = doSearch(query, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, 
                                                      pageSize,
                                                      totalRecordCounts);
            while(rs.next()) {
                ProcedureStepVO vo = new ProcedureStepVO();  
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setRequester(new UserVO());
                vo.getProcedure().getRequester().setEmployee(new EmployeeVO());

                vo.setClaimedBy(new UserVO());
                vo.getClaimedBy().setEmployee(new EmployeeVO());

                vo.getProcedure().setId(getLong(rs, "ID"));
                vo.getProcedure().setTemplateNameAr(getString(rs, "TEMPLATE_NAME"));
                vo.getProcedure().setStatus(getInteger(rs, "STATUS"));
                vo.getProcedure().setPriority(getInteger(rs, "PRIORITY"));
                vo.getProcedure().setStatusDesc(getString(rs, "STATUS_DESC"));
                vo.getProcedure().setPriorityDesc(getString(rs, "PRIORITY_DESC"));
                vo.getProcedure().setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.getProcedure().setLastActionDate(getDate(rs, "LAST_ACTION_DATE"));
                vo.setNameAr(getString(rs, "STEP_NAME_A"));
                
                vo.getClaimedBy().getEmployee()
                  .setEmployeeNameAr(getString(rs, "CLAIMED_BY"));

                vo.getProcedure().getRequester().getEmployee()
                   .setEmployeeNameAr(getString(rs, "REQUESTED_BY"));
                   
                vo.getProcedure().setTrsId(getLong(rs, "TRS_ID")); 
                
                vo.getProcedure().setTrafficFile(new TrafficFileVO());
                vo.getProcedure().getTrafficFile().setId(getLong(rs, "CUSTOMER_ID"));  
            }

            return searchPage; 

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }

    /**
     * Find Eps On Verify Step Template
     * 
     * @param currentPageNo Current Page No
     * @param epsVerifySearchVO Eps Verify Search VO
     * 
     * @return SearchPageVO
     */
    public SearchPageVO findEpsOnVerifyStep( int currentPageNo,EpsVerifySearchVO epsVerifySearchVO) {

        // Get related named query
        NamedQuery query = null;
        if (epsVerifySearchVO.isWithoutEid()) { 
            query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.findEpsOnVerifyStep");
        } else {
            query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.findEpsOnVerifyStepWithEid");
        }
            
        
        if (epsVerifySearchVO.getProcedureTemplateVO() != null && epsVerifySearchVO.getProcedureTemplateVO().getId() != null) {
            query.appendWhereClause( " AND PLT.ID = :ptlId ", "ptlId", epsVerifySearchVO.getProcedureTemplateVO().getId());
        } 
        
        if (epsVerifySearchVO.getProcedureTemplateVO() != null && epsVerifySearchVO.getProcedureTemplateVO().getId() != null) {
            query.appendWhereClause( " AND PLT.ID = :id ", "id", epsVerifySearchVO.getProcedureTemplateVO().getId());
        }
        if (epsVerifySearchVO.getEmployeeVO() != null && epsVerifySearchVO.getEmployeeVO().getId() != null) {
            query.appendWhereClause( " AND EXISTS( SELECT 1 FROM CI_UM_EMPLOYEES EMP, CI_UM_USERS USR " +
                                                "WHERE EMP.USR_ID = USR.ID " +
                                                "AND upper(USR.USERNAME) = upper(TRS.CREATED_BY) AND EMP.ID = :empId)","empId",epsVerifySearchVO.getEmployeeVO().getId());
        }
        
        if (epsVerifySearchVO.getFromDate() != null ) {
            query.appendWhereClause( " AND PRD.STATUS_DATE >= :fromDate", "fromDate", epsVerifySearchVO.getFromDate());
        }
        
        if (epsVerifySearchVO.getToDate() != null ) {
            query.appendWhereClause( " AND TRUNC(PRD.STATUS_DATE) <= :toDate", "toDate", epsVerifySearchVO.getToDate());
        }
        
        if (!isBlankOrNull(epsVerifySearchVO.getRequestName()) ) { 
            query.appendWhereClause( "AND PRD.REQUESTOR_NAME = :requestName", "requestName", epsVerifySearchVO.getRequestName());
        }
        
        
        if (epsVerifySearchVO.getTrsVO() != null && epsVerifySearchVO.getTrsVO().getId() != null) {
            query.appendWhereClause( " AND TRS.ID = :trsId", "trsId", epsVerifySearchVO.getTrsVO().getId());
        }
        
        
        if (epsVerifySearchVO.getUserVO() != null && epsVerifySearchVO.getUserVO().getId() != null) {
            query.setParameter("userId", epsVerifySearchVO.getUserVO().getId()); 

        }
        query.appendWhereClause(" ORDER BY PRD.STATUS_DATE ");
         // Debug query
        debugQuery(query.getSqlQuery(), query.getSqlParameters());
        
        PreparedStatement stm = null;
        ResultSet rs = null;          
        try { 
            SearchPageVO searchPageVO = getSearchPage(query, currentPageNo);
            stm = doSearch(query, currentPageNo);
            rs = stm.executeQuery(); 
            while(rs.next()) {
                ProcedureStepVO vo = new ProcedureStepVO(); 
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setRequester(new UserVO());
                vo.getProcedure().getRequester().setEmployee(new EmployeeVO());

                vo.setClaimedBy(new UserVO());
                vo.getClaimedBy().setEmployee(new EmployeeVO());
                
                vo.getProcedure().setId(getLong(rs, "ID"));
                vo.getProcedure().setTemplateNameAr(getString(rs, "TEMPLATE_NAME"));
                vo.getProcedure().setTemplateNameEn(getString(rs, "TEMPLATE_NAME_EN"));
                
                vo.getProcedure().getRequester().getEmployee()
                                        .setEmployeeNameAr(getString(rs, "EMP_NAME_AR")); 
                vo.getProcedure().getRequester().getEmployee()
                                        .setEmployeeNameEn(getString(rs, "EMP_NAME_EN"));
                
                
                vo.getProcedure().setStatus(getInteger(rs, "STATUS"));
                vo.getProcedure().setPriority(getInteger(rs, "PRIORITY"));
                vo.getProcedure().setStatusDesc(getString(rs, "STATUS_DESC"));
                vo.getProcedure().setPriorityDesc(getString(rs, "PRIORITY_DESC"));
                vo.getProcedure().setCreationDate(getDate(rs, "CREATION_DATE"));
               
                vo.getProcedure().setTrsId(getLong(rs, "TRS_ID"));
                
                vo.getProcedure().setTrafficFile(new TrafficFileVO());
                vo.getProcedure().getTrafficFile().setId(getLong(rs, "CUSTOMER_ID")); 
               
                
                searchPageVO.addRecord(vo);
            }

            return searchPageVO; 

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }


    /**
     * Check if this user is allowed to take actions on this procedure.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     * @return true if this user is allowed to take actions on this procedure.
     */
    public boolean isUserActionAllowed(Long procedureId, Long userId) {
        // Debug message
        List params = new ArrayList();
        params.add(procedureId);
        params.add(userId);
        params.add(procedureId);
        params.add(userId);
        debugQuery(IS_USER_ACTION_ALLOWED, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(IS_USER_ACTION_ALLOWED);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if (! rs.next()) {
                return false;
            }
            
            return rs.getInt(1) > 0;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    
    
    /**
     * Update procedure info.
     * 
     * @param procedureId Procedure ID.
     * @param notes User nores.
     * @param username Current username.
     */
    public void update(Long procedureId, String notes, String username) {
        // Debug query
        List params = new ArrayList();
        params.add(username);
        params.add(notes);
        params.add(procedureId);
        debugQuery(DO_UPDATE, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DO_UPDATE);
            setString(stm, 1, username);
            stm.setString(2, notes);
            setLong(stm, 3, procedureId);

            int count = stm.executeUpdate();
            if (count <= 0) {
                throw new DataAccessException(
                    "Failed to update active procedure: ID = " + procedureId);
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Clear Assigned to
     * 
     * @param vo Procedure step value object.
     */
    public void clearAssignTo(ProcedureVO vo) {
    
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.clearAssignTo");
        
        query.setParameter("procedureId", vo.getId());
        query.setParameter("updatedBy", vo.getUpdatedBy());
        
        
        // Debug Query.
        debugQuery(query);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to clear assign task to user: procedureId=")
                        .append(vo.getId()).toString());
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Assign task to specific user.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID to be assigned to this task.
     */
    public void assignTask(Long procedureId, Long userId) {
    
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.assignTask");
        
        query.setParameter("procedureId", procedureId);
        query.setParameter("userIdAssignedTo", userId);
        
        // Debug Query.
        debugQuery(query);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to assign task to user: procedureId=")
                        .append(procedureId).append(", userId=")
                            .append(userId).toString());
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Update procedure notes.
     * 
     * @param procedureId Procedure ID.
     * @param notes User nores.
     * @param username Current username.
     */
    public void updateNotes(Long procedureId, String notes, String username){
    
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.updateNote");
        
        query.setParameter("procedureId", procedureId);
        query.setParameter("updatedBy", username);
        query.setParameter("lastNote", notes);
        
        // Debug Query.
        debugQuery(query);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to assign task to user: procedureId=")
                        .append(procedureId).append(", userId=")
                            .append(username).toString());
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Assign Coordinator Task Note
     * 
     * @param vo Procedure VO
     */
    public void assignCoordinatorTaskNote(ProcedureVO vo) {
    
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.assignCoordinatorTaskNote");
        if (vo != null) {
            
            query.setParameter("procedureId", vo.getId());
            query.setParameter("userIdAssignedTo", vo.getAssignedToUserId());
            query.setParameter("lastNote", vo.getLastNote());
            query.setParameter("updatedBy",vo.getUpdatedBy());
            if (vo.getActiveStep() != null) {
                query.setParameter("activeStepSeqNo",vo.getActiveStep().getSequenceNo());
            }
            if (vo.getLastHumanTaskStep() != null) {
                query.setParameter("lastActiveStepSeqNo",vo.getLastHumanTaskStep().getSequenceNo());
            }
        
        }
        
        // Debug Query.
        debugQuery(query);

        NamedQuery unclaimQuery = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.unclaim");        
        if (vo != null) {
            unclaimQuery.setParameter("procedureId", vo.getId());        
        }
        // Debug Query.
        debugQuery(unclaimQuery); 

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to assign task to user").toString());
            }

            // Execute DML statement
            stm = prepareStatement(unclaimQuery);
            stm.executeUpdate();


        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Assign task to specific user.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID to be assigned to this task.
     */
    public void assignTask(Long procedureId, Long userId,String remarks) {
    
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.assignTaskNote");
        query.setParameter("procedureId", procedureId);
        query.setParameter("userIdAssignedTo", userId);
        query.setParameter("lastNote", remarks);
        // Debug Query.
        debugQuery(query);

        NamedQuery unclaimQuery = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.unclaim");        
        unclaimQuery.setParameter("procedureId", procedureId);        
        // Debug Query.
        debugQuery(unclaimQuery); 

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to assign task to user: procedureId=")
                        .append(procedureId).append(", userId=")
                            .append(userId).toString());
            }

            // Execute DML statement
            stm = prepareStatement(unclaimQuery);
            stm.executeUpdate();


        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Get active user procedure count
     * 
     * @return number of active user procedure
     * @param userId user ID
     * @param centerId center ID
     */
    public Integer getActiveUserProcedureCount(Long userId ){
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {        
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getActiveUserProcedureCount");
            query.setParameter("userId", userId);
            
            stm = prepareStatement(query);
            rs = stm.executeQuery();
            
            if (!rs.next()) {
                return new Integer(0);
            }
            
            return new Integer(rs.getInt(1));

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Get procedure template count
     * @return template version number
     * @param templateId template Id
     * @param versionNo version number
     */
    public Integer getTemplateVersionCount(Long templateId,Integer versionNo) {
        // Debug message
        List params = new ArrayList();

        params.add(templateId);
        params.add(versionNo);

        debugQuery(GET_TEMPLATE_VERSION_COUNT, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(GET_TEMPLATE_VERSION_COUNT);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            rs.next();

            return new Integer(rs.getInt(1));
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
        
    /**
     * Has Instance
     * 
     * @return true if has instance, otherwise return false
     * @param templateId Template Id
     * @param seqNo Sequence Number
     */
    public boolean hasInstance(Long templateId,Integer seqNo){
        // Query info
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.hasInstance");
        query.setParameter("templateId", templateId);
        query.setParameter("seqNo", seqNo);
        
        debugQuery(query.getSqlQuery(), query.getSqlParameters()); // Debug message
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        List params = new ArrayList();
        params.add(templateId);
        params.add(seqNo);
        
        int count = 0;
        
        try{
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
                count = getInteger(resultSet,"COUNT").intValue();
            }
            
            return count > 0;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }      
    }    
    
    /**
     * Field Template Has Instance
     * 
     * @return true if has instance, otherwise return false
     * @param fieldId Field Id
     */
    public boolean fieldTemplateHasInstance(Long fieldId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        List params = new ArrayList();
        params.add(fieldId);
                
        debugQuery(FIELD_TEMPLATE_HAS_INSTANCE, params);
        int count = 0;
        
        try{
            prepStmt = getConnection().prepareStatement(FIELD_TEMPLATE_HAS_INSTANCE);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
                count = getInteger(resultSet,"COUNT").intValue();
            }
            
            return count > 0;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }

    /**
     * Find follow-up procedures.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @return Search results.
     */
    public SearchPageVO findFollowupProcedures(int pageNo, 
                                              ProcedureVO procedureVO, 
                                              Long userId,
                                              Date creationDateFrom, 
                                              Date creationDateTo,
                                              List searchFields) {
        // Query info
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.findFollowupProcedures");
        query.setParameter("userId", userId);
        
        if(procedureVO.getTemplate()!=null&&
            procedureVO.getTemplate().getId() != null) {
            query.appendWhereClause(" AND FUU.PTL_ID = :templateId ", "templateId", 
                                    procedureVO.getTemplate().getId());
        }
        
        // Append worklist procedures where clause
        if (creationDateFrom != null) {
            query.appendWhereClause(" AND PRD.CREATION_DATE >= :creationDateFrom ", 
                                    "creationDateFrom", creationDateFrom);
        }
        
        if (creationDateTo != null) {
            query.appendWhereClause(" AND PRD.CREATION_DATE < :creationDateTo + 1 ", 
                                    "creationDateTo", creationDateTo);
        }

        if (procedureVO.getId() != null) {
            query.appendWhereClause(" AND PRD.ID = :procedureId ", "procedureId", 
                                    procedureVO.getId());
        }
 
        if (procedureVO.getRequester() != null && 
            procedureVO.getRequester().getId() != null) {
            query.appendWhereClause(" AND PRD.USR_ID = :requesterId ", "requesterId", 
                                    procedureVO.getRequester().getId());
        }

        if( procedureVO.getStatus() != null ){
            query.appendWhereClause(" AND PRD.STATUS = :procedreStatus ", 
                                    "procedreStatus", procedureVO.getStatus());
        }
        
        if (procedureVO.getActiveStep() != null && 
            procedureVO.getActiveStep().getId() != null) {
            StringBuffer activeSqlQuery = new StringBuffer(" AND PRD.ACTIVE_STEP_SEQ_NO =")
                    .append(" (SELECT SEQ_NO FROM TF_EPS_PROCEDURE_TEMP_STEPS ")
                    .append(" WHERE ID = :activeStepId)");
            query.appendWhereClause(activeSqlQuery.toString(), "activeStepId", 
                                    procedureVO.getActiveStep().getId());
        }
        
        if ( procedureVO.getTrafficFile() != null && procedureVO.getTrafficFile().getId() != null) {
            query.appendWhereClause(" AND (PRD.CUSTOMER_ID = :trafficId\n" + 
            "            OR (PTL.CODE = 1994\n" + 
            "                AND PRD.TRS_ID = (SELECT TRF.TRAFFIC_FILE_TRS_ID\n" + 
            "                                    FROM TF_STP_TRAFFIC_FILES TRF\n" + 
            "                                   WHERE TRF.ID = :trafficId))) ");
            query.setParameter("trafficId", procedureVO.getTrafficFile().getId() );
        }

         
        
        // Add search fields filters code
        for (int i = 0; searchFields!= null && i < searchFields.size(); i++) {
            SearchFieldVO searcfFieldVO = (SearchFieldVO) searchFields.get(i);
            appendSearchFieldSubQuery(query, searcfFieldVO);
        }

        // Append order by query
        query.appendWhereClause(" ORDER BY PRD.ID");
 
        // Debug query
        debugQuery(query.getSqlQuery(), query.getSqlParameters());
        
        PreparedStatement stm = null;
        ResultSet rs = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(query);
            stm = doSearch(query, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, 
                                                      pageSize,
                                                      totalRecordCounts);
            while (rs.next()) {
                ProcedureVO vo = new ProcedureVO();
                vo.setActiveStep(new ProcedureStepVO());
                searchPage.addRecord(vo);
                
                vo.setId(getLong(rs, "ID"));
                vo.setTemplateNameAr(rs.getString("TEMPLATE_NAME"));
                vo.setStatus(getInteger(rs, "STATUS"));
                vo.setStatusDesc(rs.getString("STATUS_DESC"));
                vo.setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.setLastActionDate(getDate(rs, "LAST_ACTION_DATE"));
                
                if (vo.isUnderProcessing()) {
                    vo.getActiveStep().setNameAr(rs.getString("ACTIVE_STEP_NAME"));
                }
                vo.setUserAssignedTo(rs.getString("USR_ASSIGNED_TO"));
                vo.setStepUser(rs.getString("STEPS_USR_ID"));
            }

            return searchPage;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * Get transactional procedure
     * 
     * @param procedureId procedure ID
     * @return procedure value object
     */
    public ProcedureVO getTransactionalProcedure(Long procedureId) {
        PreparedStatement stmt = null;   
        ResultSet rs = null;
        try {
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getTransactionalProcedure");
            query.setParameter("procedureId", procedureId);
            debugQuery(query); // Debug message
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            if (! rs.next()) {
                return null;
            }

            ProcedureVO vo = new ProcedureVO();
            vo.setId(procedureId);
            vo.setRequester(new UserVO());
            vo.getRequester().setEmployee(new EmployeeVO());
            
            vo.setTrafficFile(new TrafficFileVO());
            
                            
            // Set procedure info
            vo.setLastNote(getString(rs, "LAST_NOTE"));
            vo.setTemplateNameAr(getString(rs,"TEMPLATE_NAME"));
            vo.setRequesterEmail(getString(rs, "REQUESTER_EMAIL"));
            vo.setRequesterMobile(getString(rs, "REQUESTER_MOBILE_NO"));
            vo.setRequestorNameAr(getString(rs, "REQUESTER_NAME_A"));
            vo.getRequester().getEmployee().setEmployeeNameAr(getString(rs, "REQUESTER_NAME_A"));
            
            vo.getTrafficFile().setId(getLong(rs, "CUSTOMER_ID")); 
            
            
            vo.setCreatedBy(getString(rs, "CREATED_BY"));
            return vo;
            
        } catch (DataAccessException ex) {  
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    
   
    
    /**
     * Fetch Next Procedure
     * 
     * @param userId User ID
     * @param ctrId Center ID
     * 
     * @return Procedure Value Object
     */
    public ProcedureVO fetchNextProcedure(Long userId ) {
        NamedQuery namedQuery = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.findWorklistProcedures");
        namedQuery.setParameter("userId", userId);
        // Debug Query
        debugQuery(namedQuery);      
        
        PreparedStatement stmt = null;
        ResultSet rs = null;        
        
        try {
            stmt = prepareStatement(namedQuery);
            rs = stmt.executeQuery();
            
            if(!rs.next()) {
                return null;
            }
            
            ProcedureVO vo = new ProcedureVO();
            vo.setId(getLong(rs, "ID"));
            
            return vo;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }           
    }
    
    /**
     * Get By Transaction ID
     * 
     * @param transactionId Transaction ID
     * 
     * @return Procedure Value Object.
     */
    public ProcedureVO getByTransactionId(Long transactionId) { 

        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getByTransactionId");
        query.setParameter("transactionId", transactionId);

        // Debug Query
        debugQuery(query); 

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {

            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            if (! rs.next()) {
                return null;
            }

            ProcedureVO vo = new ProcedureVO();
            vo.setId(getLong(rs, "ID"));
            vo.setCreatedBy(getString(rs, "CREATED_BY"));
            vo.setUpdatedBy(getString(rs, "UPDATED_BY"));
            vo.setActiveStep(new ProcedureStepVO());
            vo.getActiveStep().setId(getLong(rs, "PRE_ID"));
            vo.getActiveStep().setSequenceNo(getInteger(rs, "ACTIVE_STEP_SEQ_NO"));
            vo.getActiveStep().setClaimedBy(new UserVO());
            vo.getActiveStep().getClaimedBy().setId(getLong(rs, "CLAIMED_BY"));
            vo.setAssignedTo(new UserVO());
            vo.getAssignedTo().setId(getLong(rs,"USR_ID_ASSIGNED_TO"));
            
            
            vo.setCode(getInteger(rs, "CODE"));
            vo.setLastHumanTaskStep(new ProcedureStepVO());
            
            vo.getLastHumanTaskStep().setClaimedBy(new UserVO());
            vo.getLastHumanTaskStep().getClaimedBy().setId(getLong(rs,"LAST_HT_USR_ID"));
            vo.setCode(getInteger(rs, "PROCEDURE_CODE"));

            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }        
    }
    
     
    /**
     *getByAuditTransactionInfo  
     * 
     * @param trsId Transaction Id.
     * @param code Transaction code.
     * 
     * @return procedure value object , check if transaction is already audited
     */
    public ProcedureVO getByAuditTransactionInfo(Long trsId ,Long code ) {
        
        PreparedStatement stmt = null;   
        ResultSet rs = null;
        try {
                NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getByAuditTransactionInfo");
                query.setParameter("trsId",trsId ); 
                query.setParameter("code",code ); 
            
                debugQuery(query); // Debug message
                stmt = prepareStatement(query);
                rs = stmt.executeQuery();
            
                if (! rs.next()) {
                    return null;
                }
                ProcedureVO vo = new ProcedureVO();   
                vo.setId(getLong(rs,"id"));
            
               return vo;
            
        } catch (DataAccessException ex) {   
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
        
    }

    /**
     * updateToApprove To Approve.
     * 
     * @param procedureId Procedure ID.
     * @param username Current active employee username.
     */
    public void updateToApprove(Long procedureId, String userName) {
        // Debug query
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.updateToApprove");
        query.setParameter("userName", userName);
        query.setParameter("procedureId", procedureId);
        
        PreparedStatement stm = null;
        try {
            // Execute DML statement
           stm = prepareStatement(query);
           int count = stm.executeUpdate();

          if (count != 1) {
              throw new DataAccessException("record not updated");
          }

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Has Active EPS Procedure
     * 
     * @param transactionId  Transaction Id
     * @return true if Has Active Procedure
     */
    public boolean hasActiveEPSProcedure(Long transactionId) {
        
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.hasActiveEPSProcedure");
        query.setParameter("transactionId", transactionId);
        
        debugQuery(query); // Debug message
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            
            stm = prepareStatement(query);
            rs = stm.executeQuery();
            
            
            if (! rs.next()) {
                return false;
            }
            
            int count = rs.getInt("COUNT");
            
            return count > 0;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     *has Another Procedure  
     * 
     * @param trsId Transaction Id.
     * @param code Transaction code.
     * 
     * @return (true) if there is other procedure has the same transaction ID and has the same code (belongs to the same template).
     */
    public boolean  hasAnotherProcedure(Long trsId ,Integer code ) {
        
        PreparedStatement stmt = null;   
        ResultSet rs = null;
        try {
                NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.hasAnotherProcedure");
                query.setParameter("trsId",trsId ); 
                query.setParameter("code",code );                
                
                debugQuery(query); // Debug message
                stmt = prepareStatement(query);
                rs = stmt.executeQuery();
            
                if (! rs.next()) {
                    return false;
                }
         
                Long count =  getLong(rs,"COUNT");
                if (count != null && count.intValue() >= 1 ) {
                    return true;
                }
            
               return false;
            
        } catch (DataAccessException ex) {   
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
        
    }
    

    /**
     * Find Procedure
     * 
     * @param transactionId transaction Id
     * @param pageNo page Number
     * 
     * @return SearchPageVO Object
     */ 
    public SearchPageVO find(Long transactionId,int pageNo) {
        
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.find");
        query.setParameter("trsId",transactionId);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        try {
            // search page value object
            SearchPageVO searchPageVO = getSearchPage(query, pageNo);
            
            prepStmt = doSearch(query, pageNo);
            resultSet = prepStmt.executeQuery();
            ProcedureVO vo = null;
            

            while(resultSet.next()) {
            

                vo = new ProcedureVO();         
                
                // Set procedure info
                vo.setId(getLong(resultSet, "ID"));
                vo.setTemplateNameAr(getString(resultSet, "TEMPLATE_NAME"));
                vo.setStatusDesc(getString(resultSet, "STATUS_DESC"));
                vo.setCreatedBy(getString(resultSet, "CREATED_BY"));
                vo.setUpdatedBy(getString(resultSet, "UPDATED_BY"));
                vo.setStatusDate(getDate(resultSet, "STATUS_DATE"));
                vo.setCreationDate(getDate(resultSet, "CREATION_DATE"));
                vo.setUpdateDate(getDate(resultSet, "UPDATE_DATE"));
                

                // add the procedure info value object
                searchPageVO.addRecord(vo);
            
            }
            return searchPageVO;
            

        } catch(Exception ex) {
            throw new DataAccessException(ex);

        } finally {
            close(prepStmt);
            close(resultSet);
        }
    }
    

    
    
    /**
     * Get all defined Initial NOCs
     *
     * @param templateCodes Template Codes.
     * @param trsId Trs Id.
     * 
     * @return List of Procedure VO
     */
     public List getAllDefinedInitialNOCs(String templateCodes, Long trsId){
        
        //Get Named Query 
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getAllDefinedInitialNOCs2");
        
        
        PreparedStatement stmt = null;   
        ResultSet rs = null;
        List procedureVoList = new ArrayList();
        
        try {
            
            query.setParameter("trsId", trsId); 
            String appendString   =" AND  PTL.CODE in ("+templateCodes+")";
            query.appendWhereClause(appendString);            
            debugQuery(query); // Debug message
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                
                ProcedureVO procedureVo = new ProcedureVO();
                ProcedureTemplateVO procedureTemplateVo = new ProcedureTemplateVO();
                procedureVo.setCode(getInteger(rs,"CODE"));
                procedureVo.setStatus(getInteger(rs,"STATUS"));
                procedureVo.setTemplate(procedureTemplateVo);
                procedureVoList.add(procedureVo);
            }
            return procedureVoList;
        
        } catch (DataAccessException ex) {   
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }

    
    /**
     * get List of templates by transaction id.
     *  
     * @param trsId transaction Id.
     * 
     * @return list of templates.
     */
     
     public List getTemplates(Long trsId){           
        
        //Get named query .  
        NamedQuery namedQuery = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getTemplates");  
         
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
             
        try {    
           
//            Set parameter in query.
            namedQuery.setParameter("P_TRS_ID",trsId);  
            stmt = prepareStatement(namedQuery);
            
            //Execute query
            resultSet = stmt.executeQuery();
            
            //Get list of Noces 
            List listOfTemplates = new ArrayList();    
           
            while(resultSet.next()){                  
              
               ProcedureVO procedureVO = new ProcedureVO();   
               procedureVO.setId(getLong(resultSet,"EPS_ID"));
               procedureVO.setStatusDesc(getString(resultSet,"PRD_STATUS"));
               procedureVO.setStatusDate(getDate(resultSet,"PRD_STATUS_DATE"));
               procedureVO.setTemplateNameAr(getString(resultSet,"EPS_TEMP_NAME"));   
               
               listOfTemplates.add(procedureVO);   
           }
          
          return listOfTemplates;
          
            } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(stmt);
        }     
     }
     
      
    /**
     * Get Procedures To Be Moved To Delivery Step.
     * 
     * @return List Of Procedure Value Objects.
     */
    public List getProceduresToBeMovedToDeliveryStep() {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getProceduresToBeMovedToDeliveryStep");
            
            debugQuery(query);
            
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            List proceduresList = new ArrayList();
            
            while(rs.next()) {
                ProcedureVO vo = new ProcedureVO();
                vo.setId(getLong(rs, "PRD_ID"));
                vo.setCode(getInteger(rs, "TEMPLATE_CODE"));
                vo.setCreatedBy(getString(rs, "PRD_CREATED_BY"));
                vo.setUpdatedBy(getString(rs, "PRD_UPDATED_BY"));
                
                
                ProcedureStepVO procedureStepVO = new ProcedureStepVO();
                procedureStepVO.setId(getLong(rs, "PRE_ID"));
                procedureStepVO.setSequenceNo(getInteger(rs, "SEQ_NO"));
                vo.setActiveStep(procedureStepVO);
                
                vo.setTrsId(getLong(rs, "TRS_ID"));
                
                proceduresList.add(vo);
            }
            
            return proceduresList;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    /**
     * Update Procedure for confirm printing operation (ws) 
     * 
     * @param username external user
     * @param transactionId transaction id
     */
    public void finalizTrsProcedure(Long transactionId, String username , Long userId) {
    
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.finalizTrsProcedure");
        
        query.setParameter("trsId", transactionId);
        query.setParameter("userName", username);
        query.setParameter("userId",userId);
        
        // Debug Query.
        debugQuery(query);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                    "error while finalize transaction procedure: transactionId=")
                        .append(transactionId).append(", username=")
                            .append(username).toString());
            }

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
   /**
     * Check if transaction id Has active Procedure.
     *
     * @param transactionId transaction Id
     * @return true if Transaction Id Has active Procedure.
     */
    public boolean hasProcedure(Long transactionId) {
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
        
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.hasProcedure");
            
            
            query.setParameter("trsId", transactionId);
            
            
            debugQuery(query); // Debug message
            
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            if(!rs.next()){
            
                return false;
            }
            
            return rs.getInt("count") > 0;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    
    
    /**
    * Get All Proceduers By Transaction ID
    * 
    * @param transactionId Transaction ID
    * 
    * @return List Of Procedure Value Object.
    */
    public List getAllProceduersByTrsId(Long transactionId){
    
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getByTransactionId");
        query.setParameter("transactionId", transactionId);

        // Debug Query
        debugQuery(query); 

        PreparedStatement stmt = null;
        ResultSet rs = null;
        List proceduerList = new ArrayList();

        try {

            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
               
                ProcedureVO vo = new ProcedureVO();
                vo.setId(getLong(rs, "ID"));
                vo.setStatus(getInteger(rs, "STATUS"));
                vo.setCreatedBy(getString(rs, "CREATED_BY"));
                vo.setUpdatedBy(getString(rs, "UPDATED_BY"));
                vo.setTemplateNameAr(getString(rs, "TEMPLATE_NAME"));
                vo.setTemplate(new ProcedureTemplateVO());
                vo.getTemplate().setId(getLong(rs,"PTL_ID"));
                vo.setActiveStep(new ProcedureStepVO());
                vo.getActiveStep().setSequenceNo(getInteger(rs, "ACTIVE_STEP_SEQ_NO"));
                vo.getActiveStep().setClaimedBy(new UserVO());
                vo.getActiveStep().getClaimedBy().setId(getLong(rs, "CLAIMED_BY"));
                vo.setTrsId(transactionId);
                vo.setCode(getInteger(rs, "CODE"));
                proceduerList.add(vo);
            }
            return proceduerList;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }        
    
        
    }

    /**
     * Get Traffic File Active Transactional Procedures
     * 
     * @param trafficFileNo : Traffic File No
     * @return Traffic File Active Transactional Procedures
     */
    public List getTrafficFileActiveProcedures(Long trafficId) {
    
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getTrafficFileActiveProcedures");
            
            debugQuery(query);
            query.setParameter("trafficId",trafficId);

            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            List proceduresList = new ArrayList();

            while(rs.next()) {
                ProcedureVO vo = new ProcedureVO();
                vo.setId(getLong(rs, "PRD_ID"));
                vo.setCode(getInteger(rs,"PRD_CODE"));
                vo.setLanguageCode(getString(rs, "PRD_LAN_CODE"));
                
                
                ProcedureStepVO procedureStepVO = new ProcedureStepVO();
                procedureStepVO.setSequenceNo(getInteger(rs, "PRD_STEP"));
                vo.setActiveStep(procedureStepVO);
                vo.setActiveNote(rs.getString("PRD_NOTE"));
                vo.setLastNote(rs.getString("PRD_LAST_NOTE"));


                
                vo.setTrsId(getLong(rs, "TRS_ID"));
                
                
                proceduresList.add(vo);
            }
            return proceduresList;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    
    
    /**
     * get Count Of Assigned Eps.
     * 
     * @param userId  : user Id.
     * @param epsCode : eps Code.
     * 
     * @return Count Of Assigned Eps.
     */
    public Integer getCountOfAssignedEps(Long userId ,String epsCode) {
        
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getCountOfAssignedEps");
        query.setParameter("userId", userId);
        query.setParameter("epsCode", epsCode);
        
        debugQuery(query); // Debug message
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = prepareStatement(query);
            rs = stm.executeQuery();
            
            if (! rs.next()) {
                return new Integer(0);
            }
            
            Integer count = getInteger(rs,"COUNT");
            
            return count;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
   
    
    /**
     * get Active Procedures By Traffic File.
     * 
     * @param procedureVO : procedure VO.
     * 
     * @return list of Active Procedures.
     */
    public List getActiveProceduresByTrafficFile(ProcedureVO procedureVO) {
    
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getActiveProceduresByTrafficFile");
            
            debugQuery(query);
            query.setParameter("trafficFileId",procedureVO.getTrafficFile().getId());
            query.setParameter("procedureCode",procedureVO.getCode());

            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            List proceduresList = new ArrayList();

            while(rs.next()) {
                
                ProcedureVO vo = new ProcedureVO();
                vo.setId(getLong(rs, "ID"));
                
                vo.setTrsId(getLong(rs, "TRS_ID"));
                
                proceduresList.add(vo);
            }
            return proceduresList;
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    
    /**
     * Execute procedure Block.
     * 
     * @param procedureId : Procedure Id.
     * @param caseCode    : Case Code.
     */
    public void executeBlock(Long procedureId,Integer caseCode) {

        CallableStatement callStmt = null;
        String query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.executeBlock").getQuery();
        Long generatedBatchNumber = null;
        
        try {
            callStmt = getConnection().prepareCall(query);
            
            setLong(callStmt, 1, procedureId);
            setInteger(callStmt, 2, caseCode);
            
            callStmt.execute();
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(callStmt);
        }
    }
    
     
    /**
     * get All Procedures By Traffic File.
     * 
     * @param procedureVO : procedure VO.
     * 
     * @return list Procedures.
     */
    public List getAllProceduresByTrafficFile(ProcedureVO procedureVO) {
        PreparedStatement stmt = null;
                ResultSet rs = null;
                
                try {
                    NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getAllProceduresByTrafficFile");
                    
                    debugQuery(query);
                    query.setParameter("trafficFileId",procedureVO.getTrafficFile().getId());
                    query.setParameter("procedureCode",procedureVO.getCode());

                    stmt = prepareStatement(query);
                    rs = stmt.executeQuery();
                    
                    List proceduresList = new ArrayList();

                    while(rs.next()) {
                        ProcedureVO vo = new ProcedureVO();
                        vo.setId(getLong(rs, "ID"));
                        vo.setStatus(getInteger(rs, "STATUS"));
                        proceduresList.add(vo);
                    }
                    return proceduresList;
                    
                } catch (DataAccessException ex) {
                    throw ex;
                } catch (Exception ex)  {
                    throw new DataAccessException(ex);
                } finally {
                    close(rs);
                    close(stmt);
                }
    }
    
    
    /**
     * Find work list procedures new.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @param actionDateFrom Last action date start search.
     * @param actionDateTo Last action date end search.
     * @return Search results.
     */
    public SearchPageVO findWorklistProceduresNew(
                        int pageNo, ProcedureVO procedureVO, Long userId,
                        Date creationDateFrom, Date creationDateTo,
                        Date actionDateFrom, Date actionDateTo,
                        List searchFields) {

        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.findWorklistProceduresNew");
        query.setParameter("userId", userId);
        
        // Append worklist procedures where clause
        if (creationDateFrom != null) {
            query.appendWhereClause(" AND PRD.CREATION_DATE >= :creationDateFrom ");
            query.setParameter("creationDateFrom", creationDateFrom);
        }

        if (creationDateTo != null) {
            query.appendWhereClause(" AND PRD.CREATION_DATE < :creationDateTo + 1");
            query.setParameter("creationDateTo", creationDateTo);
        }

        if (actionDateFrom != null) {
            query.appendWhereClause(" AND PRD.LAST_ACTION_DATE >= :actionDateFrom ");
            query.setParameter("actionDateFrom", actionDateFrom);
        }

        if (actionDateTo != null) {
            query.appendWhereClause(" AND PRD.LAST_ACTION_DATE < :actionDateTo + 1");
            query.setParameter("actionDateTo", actionDateTo);
        }

        if (procedureVO.getId() != null) {
            query.appendWhereClause(" AND PRD.ID = :procedureId ");
            query.setParameter("procedureId", procedureVO.getId());
        }
    
        if (procedureVO.getPriority() != null) {
            query.appendWhereClause(" AND PRD.PRIORITY = :priority ");
            query.setParameter("priority", procedureVO.getPriority());
        }

        if (procedureVO.getTemplate() != null && procedureVO.getTemplate().getId() != null) {
            query.appendWhereClause(" AND PRD.PTL_ID = :templateId ");
            query.setParameter("templateId", procedureVO.getTemplate().getId());
        }

        if (! isBlankOrNull(procedureVO.getCreatedBy())) {
            query.appendWhereClause(" AND UPPER(PRD.CREATED_BY) LIKE UPPER(:procedureCreatedBy) ");
            query.setParameter("procedureCreatedBy", "'%"+procedureVO.getCreatedBy()+"%'");
        }

        if ( procedureVO.getTrafficFile() != null && procedureVO.getTrafficFile().getId() != null) {
            query.appendWhereClause(" AND PRD.CUSTOMER_ID = :trafficId ");
            query.setParameter("trafficId", procedureVO.getTrafficFile().getId() );
        }

        if ( procedureVO.getTrsId() != null) {
            query.appendWhereClause(" AND PRD.TRS_ID = :transactionId ");
            query.setParameter("transactionId", procedureVO.getTrsId() );
        }  
        // Add search fields filters code
        for (int i = 0; searchFields!= null && i < searchFields.size(); i++) {
            SearchFieldVO searcfFieldVO = (SearchFieldVO) searchFields.get(i);
            appendSearchFieldSubQuery(query, searcfFieldVO);
        }

        // Append order by query
        query.appendWhereClause(" ORDER BY STATUS, PRIORITY, LAST_ACTION_DATE ");
    
        // Debug query
        debugQuery(query.getSqlQuery(), query.getSqlParameters());
        
        PreparedStatement stm = null;
        ResultSet rs = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(query);
            stm = doSearch(query, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, 
                                                      pageSize,
                                                      totalRecordCounts);
            while(rs.next()) {
                ProcedureStepVO vo = new ProcedureStepVO();  
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setRequester(new UserVO());
                vo.getProcedure().getRequester().setEmployee(new EmployeeVO());

                vo.setClaimedBy(new UserVO());
                vo.getClaimedBy().setEmployee(new EmployeeVO());

                vo.getProcedure().setId(getLong(rs, "ID"));
                vo.getProcedure().setTemplateNameAr(getString(rs, "TEMPLATE_NAME"));
                vo.getProcedure().setStatus(getInteger(rs, "STATUS"));
                vo.getProcedure().setPriority(getInteger(rs, "PRIORITY"));
                vo.getProcedure().setStatusDesc(getString(rs, "STATUS_DESC"));
                vo.getProcedure().setPriorityDesc(getString(rs, "PRIORITY_DESC"));
                vo.getProcedure().setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.getProcedure().setLastActionDate(getDate(rs, "LAST_ACTION_DATE"));
                vo.setNameAr(getString(rs, "STEP_NAME_A"));
                
                vo.getClaimedBy().getEmployee()
                  .setEmployeeNameAr(getString(rs, "CLAIMED_BY"));

                vo.getProcedure().getRequester().getEmployee()
                   .setEmployeeNameAr(getString(rs, "REQUESTED_BY"));
                   
                vo.getProcedure().setTrsId(getLong(rs, "TRS_ID")); 
                
                vo.getProcedure().setTrafficFile(new TrafficFileVO());
                vo.getProcedure().getTrafficFile().setId(getLong(rs, "CUSTOMER_ID"));  
            }

            return searchPage; 

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
     
    
    /**
     * Update Eps Status.
     * 
     * @param transactionId : Transaction Id
     * @param status : New Status
     * @param userName : User Name
     */
    public void updateEpsStatus(Long transactionId , Integer status , String userName){
        // Get Named Query.
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.updateEpsStatus");

        // Set Named Query Parameter.
        query.setParameter("transactionId", transactionId);
        query.setParameter("status", status);
        query.setParameter("userName", userName);

        PreparedStatement stmt = null;

        try {
            
            // Execute DML statement
            stmt = prepareStatement(query);
            stmt.executeUpdate();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stmt);
        }
    }
    
   
    /** Update Eps Traffic File Id.
     * 
     * @param transactionId : Transaction Id
     * @param trfId : Traffic File Id
     * @param userName : User Name
     */
    public void updateEpsTrafficFileId(Long procedureId, Long trfId, String userName){
        // Get Named Query.
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.updateEpsTrafficFileId");

        // Set Named Query Parameter.
        query.setParameter("procedureId", procedureId);
        query.setParameter("trfId", trfId);
        query.setParameter("userName", userName);

        PreparedStatement stmt = null;

        try {
            
            // Execute DML statement
            stmt = prepareStatement(query);
            stmt.executeUpdate();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stmt);
        }
    }

    /**
     * Update Eps Status.
     * 
     * @param procedure
     */
    public void updateEpsStatusById(ProcedureVO procedure){
        // Get Named Query.
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.updateEpsStatusById");

        // Set Named Query Parameter.
        query.setParameter("procedureId", procedure.getId());
        query.setParameter("status", procedure.getStatus());
        query.setParameter("userName", procedure.getUpdatedBy());
        query.setParameter("remarks", procedure.getLastNote());
        
        PreparedStatement stmt = null;

        try {
            
            // Execute DML statement
            stmt = prepareStatement(query);
            stmt.executeUpdate();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stmt);
        }
    }

    /**
     * Get EPS procedure which it has been printed and it's active step is printing
     *
     * @return ProcedureVO
     */
    public ProcedureVO getEPSProcedureVO() {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.getEPSProcedureVO");
            debugQuery(query);

            stmt = prepareStatement(query);
            rs = stmt.executeQuery();

            ProcedureVO procedureVO = new ProcedureVO();
            if(rs.next()) {
                procedureVO.setId(getLong(rs, "ID"));
            }

            return procedureVO;
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }

    /**
     * Finalize EPS procedure
     *
     * @param procedureId
     * @param userName
     * @param userId
     */
    public void finalizeEPSProcedure(Long procedureId,String userName,Long userId) {
        NamedQuery query = getNamedQuery("ae.eis.eps.dao.jdbc.Procedure.finalizeEPSProcedure");

        query.setParameter("procedureId", procedureId);
        query.setParameter("userName", userName);
        query.setParameter("userId",userId);

        // Debug Query.
        debugQuery(query);

        PreparedStatement stm = null;
        try {
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                        "error while finalize transaction procedure: procedureId=")
                        .append(procedureId).append(", userName=")
                        .append(userName).toString());
            }
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
  
}
