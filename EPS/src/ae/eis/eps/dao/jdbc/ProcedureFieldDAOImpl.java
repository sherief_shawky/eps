/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 * 
 * 1.01  Sami Abudayeh      19/09/2013  - Add getTableData(Long procedureId, 
 *                                                         Integer fieldCode,
 *                                                         Long procedureStepId)   
 * 
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.ProcedureFieldDAO;
import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.eps.vo.SearchResultVO;
import ae.eis.eps.vo.TableColumnValueVO;
import ae.eis.eps.vo.TableHeaderVO;
import ae.eis.eps.vo.TableRowVO;
import ae.eis.eps.vo.TableVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.common.GlobalUtilities;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Procedure field data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureFieldDAOImpl extends JdbcDataAccessObject  
                                   implements ProcedureFieldDAO {
    /*
     * JDBC SQL and DML statements.
     */

    /** Update Field and always accept new field value even if null . */
    private static final String UPDATE_FIELD_ACCEPT_NULL
    = " UPDATE TF_EPS_PROCEDURE_FIELDS"
    + " SET VALUE = ? ," // Value
    +     " UPDATED_BY = ?," // Username
    +     " UPDATE_DATE = SYSDATE"
    + " WHERE PRD_ID = ?" // Procedure ID
    +   " AND CODE = ?"; // Field Code
    
    /** Get requester fields. */
    private static final String UPDATE_FIELD
    = " UPDATE TF_EPS_PROCEDURE_FIELDS"
    + " SET VALUE = NVL( ? , VALUE)," // Value
    +     " UPDATED_BY = ?," // Username
    +     " UPDATE_DATE = SYSDATE"
    + " WHERE PRD_ID = ?" // Procedure ID
    +   " AND CODE = ?"; // Field Code

    /** Get field by code. */
    private static final String GET_BY_CODE
    = " SELECT PRF.ID,"
    +        " PRF.LABLE,"
    +        " PRF.FIELD_SIZE,"
    +        " PRF.FIELD_TYPE,"
    +        " PRF.VALUE"
    + " FROM TF_EPS_PROCEDURE_FIELDS PRF"
    + " WHERE CODE = ?" // Code
    +   " AND PRD_ID = ?"; // Procedure Id

    /** Get all procedure fields. */
    private static final String GET_ALL_FIELDS
    = " SELECT PRF.ID,"
    +        " PRF.CODE,"
    +        " PRF.VALUE,"
    +        " PRF.LABLE,"
    +        " PRF.FIELD_TYPE,"
    +        " PRF.FIELD_SIZE,"
    +        " PRF.HAS_RELATED_OPTIONS"
    + " FROM TF_EPS_PROCEDURE_FIELDS PRF"
    + " WHERE PRD_ID = ?"; // Procedure ID

    /** Get Field By Id Query */        
    private static final String GET_BY_ID
    = " SELECT ID,"
    +        " CODE,"
    +        " LABLE,"
    +        " FIELD_TYPE,"
    +        " VALUE,"
    +        " FIELD_SIZE"
    + " FROM TF_EPS_PROCEDURE_FIELDS"
    + " WHERE ID = ?";

    /** Get procedure search fields results. */
    private static final String GET_PROCEDURE_SEARCH_RESULTS
    = " SELECT TPE.LABLE,"
    +        " PRF.FIELD_TYPE,"
    +        " DECODE(PRF.FIELD_TYPE, 4,"
    +              " (SELECT NAME_A FROM TF_EPS_FIELD_OPTIONS FON"
    +               " WHERE FON.PRF_ID = PRF.ID"
    +                 " AND FON.VALUE = PRF.VALUE), PRF.VALUE) AS VALUE,"
    +        " SRL.COLUMN_WIDTH,"
    +        " SRL.ORDER_NO"
    + " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
    +      " TF_EPS_SEARCH_RESULTS SRL,"
    +      " TF_EPS_PROCEDURE_TEMPLATES PTL,"
    +      " TF_EPS_PROCEDURES PRD,"
    +      " TF_EPS_PROCEDURE_FIELDS PRF"
    + " WHERE PRD.PTL_ID = PTL.ID"
    +   " AND TPE.PTL_ID = PTL.ID"
    +   " AND TPE.ID = SRL.TPE_ID"
    +   " AND PRF.PRD_ID = PRD.ID"
    +   " AND PRF.CODE = TPE.CODE"
    +   " AND TPE.FIELD_TYPE = PRF.FIELD_TYPE"
    +   " AND PRD.ID = ?" // Procedure ID
    + " UNION"
    + " SELECT TPE.LABLE,"
    +        " TPE.FIELD_TYPE,"
    +        " NULL VALUE,"
    +        " SRL.COLUMN_WIDTH,"
    +        " SRL.ORDER_NO"
    + " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
    +      " TF_EPS_SEARCH_RESULTS SRL,"
    +      " TF_EPS_PROCEDURE_TEMPLATES PTL,"
    +      " TF_EPS_PROCEDURES PRD"
    + " WHERE PRD.PTL_ID = PTL.ID"
    +   " AND TPE.PTL_ID = PTL.ID"
    +   " AND TPE.ID = SRL.TPE_ID"
    +   " AND PRD.ID = ?" // Procedure ID
    +   " AND NOT EXISTS(SELECT 1 FROM TF_EPS_PROCEDURE_FIELDS PRF"
    +                  " WHERE PRF.PRD_ID = PRD.ID"
    +                    " AND PRF.CODE = TPE.CODE"
    +                    " AND PRF.FIELD_TYPE = TPE.FIELD_TYPE)"
    + " ORDER BY ORDER_NO";

    /*
     * Methods
     */

    /**
     * Update procedure field.
     * 
     * @param fieldVO Procedure field value object.
     * @param acceptNull true:set new value even if null or false: keep old 
     * 
     * @return true if the field was updated successfully.
     */
    public boolean update(ProcedureFieldVO fieldVO, boolean acceptNull) {
        // Debug query
        
        String query;
        if(acceptNull){
            query = UPDATE_FIELD_ACCEPT_NULL;
        }else{
            query = UPDATE_FIELD;
        }
        
        List params = new ArrayList();
        params.add(fieldVO.getValue());
        params.add(fieldVO.getUpdatedBy());
        params.add(fieldVO.getProcedure().getId());
        params.add(fieldVO.getCode());
        debugQuery(query, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(query);
            stm.setString(1, fieldVO.getValue());
            setString(stm, 2, fieldVO.getUpdatedBy());
            setLong(stm, 3, fieldVO.getProcedure().getId());
            setInteger(stm, 4, fieldVO.getCode());

            int count = stm.executeUpdate();
            return count > 0;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Get procedure field info.
     * 
     * @param procedureId Procedure ID.
     * @param fieldCode Field code.
     * @param dao Data Access Object
     * @return procedure field info
     */
    public ProcedureFieldVO getByCode(Long procedureId, Integer fieldCode) {
        // Debug query
        List params = new ArrayList();
        params.add(fieldCode);
        params.add(procedureId);
        debugQuery(GET_BY_CODE, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_BY_CODE);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            // Create VO
            ProcedureFieldVO vo = new ProcedureFieldVO();
            vo.setProcedure(new ProcedureVO());
            vo.getProcedure().setId(procedureId);
            vo.setCode(fieldCode);

            vo.setId(getLong(rs, "ID"));
            vo.setLabel(getString(rs, "LABLE"));
            vo.setSize(getInteger(rs, "FIELD_SIZE"));
            vo.setType(getInteger(rs, "FIELD_TYPE"));
            vo.setValue(rs.getString("VALUE"));

            // Return VO
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * Get procedure fields
     * 
     * @param procedureId Procedure ID.
     * @param codes Fields codes.
     * @return List of procedure fields.
     */
    public List getProcedureFields(Long procedureId, Integer[] codes) {
        StringBuffer query = new StringBuffer(GET_ALL_FIELDS);
        List params = new ArrayList();
        params.add(procedureId);
        
        // Append fields codes if any
        if (codes != null && codes.length > 0) {
            query.append(" AND PRF.CODE IN(-1");
            
            for (int i = 0; i < codes.length; i++) {
                if (codes[i] == null) {
                    continue;
                }
                
                query.append(",?");
                params.add(codes[i]);
            }

            query.append(")");
        }
        
        // Debug query
        debugQuery(query, params);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(query.toString());
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List fieldsList = new ArrayList();
            while (rs.next()) {
                // Create VO
                ProcedureFieldVO vo = new ProcedureFieldVO();
                fieldsList.add(vo);
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setId(procedureId);

                vo.setId(getLong(rs, "ID"));
                vo.setCode(getInteger(rs, "CODE"));
                vo.setValue(getString(rs, "VALUE"));
                vo.setLabel(getString(rs, "LABLE"));
                vo.setType(getInteger(rs, "FIELD_TYPE"));
                vo.setSize(getInteger(rs, "FIELD_SIZE"));
                vo.setHasRelatedOptions(getInteger(rs, "HAS_RELATED_OPTIONS"));
            }

            // Return fields list
            return fieldsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get Field By Procedure Id
     * 
     * @return Procedure Field Value Object
     * @param fieldId field Id
     */
    public ProcedureFieldVO getById(Long fieldId) {
        ArrayList params = new ArrayList();
        params.add(fieldId);
        debugQuery(GET_BY_ID, params);     
        
        PreparedStatement stm = null;
        ResultSet rs = null;    
        try {
            stm = getConnection().prepareStatement(GET_BY_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null ;
            }

            ProcedureFieldVO fieldVO = new ProcedureFieldVO();         
            fieldVO.setId(getLong(rs,"ID"));
            fieldVO.setCode(getInteger(rs,"CODE"));
            fieldVO.setLabel(getString(rs,"LABLE"));
            fieldVO.setType(getInteger(rs,"FIELD_TYPE"));
            fieldVO.setValue(getString(rs,"VALUE"));
            fieldVO.setSize(getInteger(rs,"FIELD_SIZE"));
        
            return fieldVO;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }           
    }

    /**
     * Get procedure search results fields.
     * 
     * @param procedureId Procedure ID.
     * @return Get procedure search results fields.
     */
    public List getProcedureSearchResults(Long procedureId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        params.add(procedureId);
        debugQuery(GET_PROCEDURE_SEARCH_RESULTS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_PROCEDURE_SEARCH_RESULTS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List fieldsList = new ArrayList();
            while (rs.next()) {
                TemplateFieldVO vo = new TemplateFieldVO();
                SearchResultVO result = new SearchResultVO();
                result.setTemplateField(vo);
                fieldsList.add(result);
                
                vo.setLabel(rs.getString("LABLE"));
                vo.setType(getInteger(rs, "FIELD_TYPE"));
                vo.setValue(rs.getString("VALUE"));
                result.setColumnWidth(getInteger(rs,"COLUMN_WIDTH"));
            }

            return fieldsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Insert
     * 
     * @param vo Procedure Field VO
     */
    public void insert(ProcedureFieldVO vo) {
        // named query
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureField.insert");
        namedQuery.setParameter("code",vo.getCode());
        namedQuery.setParameter("templateCode",vo.getProcedureTemplate().getCode());
        namedQuery.setParameter("prdValue",vo.getValue());
        namedQuery.setParameter("prdId",vo.getProcedure().getId());
        namedQuery.setParameter("createdBy",vo.getCreatedBy());
        namedQuery.setParameter("updatedBy",vo.getUpdatedBy());
        namedQuery.setParameter("hasReleatedOption",vo.getHasRelatedOptions());
        namedQuery.setParameter("maxRowNo",vo.getMaxRowNo());

        // Debug query
        debugQuery(namedQuery);

        PreparedStatement stm = null;

        try {
            // Execute query
            stm = prepareStatement(namedQuery);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }      
        
    }    
    
    
	/**
	 * Get Table Date
	 * 
	 * @param procedureId Procedure Id
	 * @param fieldCode Field Code
     * @param procedureStepId Procedure Step Id
	 * 
     * @return Table Value Object
	 */
	public TableVO getTableData(Long procedureId, Integer fieldCode,Long procedureStepId){
		// Table header query
		NamedQuery queryTableHeader = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureField.getTableHeader");
		queryTableHeader.setParameter("procedureId",procedureId);
		queryTableHeader.setParameter("fiedlCode",fieldCode);  

		PreparedStatement stmTableHeader = null;
		ResultSet rsTableHeader = null;

        // Table rows query
		NamedQuery queryTableColumnValues = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureField.getTableColumnValues");
		queryTableColumnValues.setParameter("procedureId",procedureId);
		queryTableColumnValues.setParameter("fiedlCode",fieldCode);  
        if ( procedureStepId != null ) {
            queryTableColumnValues.appendWhereClause(" AND TCU.PRE_ID = :procedureStepId",
                                                        "procedureStepId",procedureStepId);
        }
        queryTableColumnValues.appendWhereClause(" ORDER BY TCU.ROW_INDEX, THD.ORDER_NO");
		PreparedStatement stmTableColumnValues = null;
		ResultSet rsTableColumnValues = null;        
        
		try {
            // Table info VO
            TableVO tableVO = new TableVO();

			// Get table header info
			stmTableHeader = prepareStatement(queryTableHeader);
			rsTableHeader = stmTableHeader.executeQuery();
            
			while (rsTableHeader.next()) {
				TableHeaderVO tableHeaderVO = new TableHeaderVO();
				tableHeaderVO.setId(getLong(rsTableHeader,"ID"));
				tableHeaderVO.setIsHidden(getInteger(rsTableHeader,"IS_HIDDEN"));
				tableHeaderVO.setLableWidth(getLong(rsTableHeader,"LABLE_WIDTH"));
				tableHeaderVO.setOrderNo(getLong(rsTableHeader,"ORDER_NO"));
				tableHeaderVO.setNameAr(getString(rsTableHeader,"NAME_A"));
				tableHeaderVO.setCode(getInteger(rsTableHeader,"CODE"));
				tableVO.add(tableHeaderVO);
			}
            

			// Get table rows info
			stmTableColumnValues = prepareStatement(queryTableColumnValues);
			rsTableColumnValues = stmTableColumnValues.executeQuery();

            TableRowVO tableRowVO = new TableRowVO();
			while(rsTableColumnValues.next()){
				TableColumnValueVO tableColumnValueVO = new TableColumnValueVO();
				tableColumnValueVO.setValue(getString(rsTableColumnValues,"VALUE"));
				tableColumnValueVO.setRowIndex(getInteger(rsTableColumnValues,"ROW_INDEX"));                                                
				tableColumnValueVO.setTableHeaderVO(new TableHeaderVO());
				tableColumnValueVO.getTableHeaderVO().setId(getLong(rsTableColumnValues,"THD_ID"));
				tableColumnValueVO.getTableHeaderVO().setOrderNo(getLong(rsTableColumnValues,"ORDER_NO"));
				tableColumnValueVO.getTableHeaderVO().setCode(getInteger(rsTableColumnValues,"CODE"));
                tableColumnValueVO.getTableHeaderVO().setIsHidden(getInteger(rsTableColumnValues,"IS_HIDDEN"));
                tableColumnValueVO.getTableHeaderVO().setLableWidth(getLong(rsTableColumnValues,"LABLE_WIDTH"));
				tableRowVO.addColumn(tableColumnValueVO);
			}         
            if (tableRowVO != null && tableRowVO.getColumns() != null && 
                tableRowVO.getColumns().size() != 0) {
                tableVO.add(tableRowVO);
            }
            // Return table info
            return tableVO;

		} catch (DataAccessException ex) {
			throw ex;
		} catch (Exception ex)  {
			throw new DataAccessException(ex);
		} finally {
			close(rsTableColumnValues);
			close(stmTableColumnValues);
			close(rsTableHeader);
			close(stmTableHeader);
		}    
	}
    
    
	/**
	 * Get Table Date
	 * 
	 * @param procedureId Procedure Id
	 * @param fieldCode Field Code
	 * 
     * @return Table Value Object
	 */
	public TableVO getTableData(Long procedureId, Long fieldCode ){
		return getTableData(procedureId, GlobalUtilities.getInteger(fieldCode),null);
	}
}
