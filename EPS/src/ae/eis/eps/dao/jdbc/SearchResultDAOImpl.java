/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 */
 
package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.SearchResultDAO;
import ae.eis.eps.vo.ProcedureTemplateLogVO;
import ae.eis.eps.vo.SearchResultVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Search result data access object intefrace.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public class SearchResultDAOImpl  extends JdbcDataAccessObject  
                                      implements SearchResultDAO{
    /** Find serch result quiry */
    private static final String FIND_SEARCH_RESULT
        =   " SELECT SRL.ID SRL_ID,"
        +          " SRL.ORDER_NO,"
        +          " TPE.LABLE,"
        +          " TPE.ID TPE_ID,"
        +          " TPE.FIELD_TYPE,"
        +          " SRL.COLUMN_WIDTH,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          " ('TF_EPS_FIELD_TYPE',FIELD_TYPE) FIELD_TYPE_DESC"
        +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
		+          " TF_EPS_SEARCH_RESULTS SRL"
        +    " WHERE TPE.ID = SRL.TPE_ID"
        +      " AND TPE.PTL_ID = ? "
        +    " ORDER BY SRL.ORDER_NO ";
    
    /** deletete serch result */
    private static final String DELETE_SEARCH_RESULT
        =   " DELETE FROM TF_EPS_SEARCH_RESULTS WHERE ID = ? ";
    
    /** add log query */
    private static final String ADD_LOG_QUERY
        =    "INSERT INTO TF_EPS_PROCEDURE_TEMP_LOGS "
        +    "(ID,STATUS,VERSION_NO,NAME_E,ACTION_TYPE,PRIORITY,NAME_A, "
        +    "STATUS_DATE,PTL_ID,EMPLOYEE_NAME,CREATED_BY,CREATION_DATE) "
        +    "SELECT PTG_SEQ.NEXTVAL, "
        +          " PTL.STATUS,"
        +          " PTL.VERSION_NO,"
        +          " PTL.NAME_E,"
        +          " ?,"
        +          " PTL.PRIORITY,"
        +          " PTL.NAME_A,"
        +          " SYSDATE,"
        +          " PTL.ID,"
        +          " F_DB_GET_EMP_NAME_A(?),"
        +          " ?,"
        +          " SYSDATE"
        +     " FROM TF_EPS_PROCEDURE_TEMPLATES PTL"
        +    " WHERE PTL.ID = ? ";
    
     /** creat search result query */
     private static final String CREATE_SEARCH_RESULT
        =   " INSERT INTO TF_EPS_SEARCH_RESULTS "
		+         " (ID,ORDER_NO,TPE_ID,COLUMN_WIDTH, "
        +           "CREATED_BY,UPDATED_BY,CREATION_DATE,UPDATE_DATE) "
        +   " VALUES(?,"        // ID
        +           "?,"        // ORDER_NO
        +           "?,"        // TPE_ID
        +           "?,"        // COLUMN_WIDTH
        +           "?,"        // CREATED_BY
        +           "?,"        // UPDATED_BY
        +           "SYSDATE,"  // CREATION_DATE
        +           "SYSDATE)"; // UPDATE_DATE
        
    /** is exist search result */
    private static final String IS_EXIST_SEARCH_RESULT
        ="SELECT 1 FROM TF_EPS_SEARCH_RESULTS WHERE TPE_ID = ?";
        
    /** is order number exist */
    private static final String IS_EXIST_ORDER_NO
        ="SELECT 1 FROM TF_EPS_SEARCH_RESULTS SRL,"
                    +"  TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
                    +"  TF_EPS_PROCEDURE_TEMPLATES PTL"
                    +"  WHERE SRL.TPE_ID=TPE.ID "
                    +"  AND   TPE.PTL_ID=PTL.ID "
                    +"  AND   PTL.ID = ? "
                    +"  AND   SRL.ORDER_NO=? ";
    
     /** Get serch result quiry */
    private static final String GET_SEARCH_RESULT
        =   " SELECT"
        +          " SRL.ORDER_NO,"
        +          " SRL.COLUMN_WIDTH," 
        +          " SRL.TPE_ID,"
        +          " TPE.FIELD_TYPE,"
        +          " TPE.LABLE,"
        +          " TPE.CODE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          " ('TF_EPS_FIELD_TYPE',FIELD_TYPE) FIELD_TYPE_DESC"
        +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
		+          " TF_EPS_SEARCH_RESULTS SRL"
        +    " WHERE TPE.ID = SRL.TPE_ID"
        +      " AND SRL.ID = ? ";
    
    /** update search result */
    private static final String UPDATE_SEARCH_RESULT
        =   " UPDATE TF_EPS_SEARCH_RESULTS "
        +      " SET ORDER_NO = ?,"      
        +          " COLUMN_WIDTH = ?,"
        +          " UPDATE_DATE = SYSDATE,"
        +          " UPDATED_BY = ? "
        +    " WHERE ID = ?	" ;  
    
    /** Get Maximum Search Result Order Number */
    private static final String GET_MAX_SEARCH_RESULT_ORDER_NUMBER
        =   " SELECT MAX(ORDER_NO) MAX_ORDER_NO "
        +     " FROM TF_EPS_SEARCH_RESULTS "
        +    " WHERE TPE_ID IN (SELECT ID FROM TF_EPS_TEMP_PROCEDURE_FIELDS "
        +                      " WHERE PTL_ID = ?) ";
    
    /**
     * Is order number exists 
     * 
     * @return true if exist
     * @param templateId template ID
     * @param orderNo order number
     * @param searchResultId search result ID
     */
    public boolean isOrderNoExists(Long templateId, Integer orderNo,Long searchResultId){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_EXIST_ORDER_NO);
        params.add(templateId);
        params.add(orderNo);
        if(searchResultId != null){
            sql.append(" AND SRL.ID <> ? ");
            params.add(searchResultId);
        }
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Update search result 
     * 
     * @param vo search result value object
     */
    public void update(SearchResultVO vo){
        PreparedStatement stm = null;
        
        // Debug query
        List params = new ArrayList();             
        
        params.add(vo.getOrderNo());
        params.add(vo.getColumnWidth());
        params.add(vo.getCreatedBy());
        params.add(vo.getId());
        
        debugQuery(UPDATE_SEARCH_RESULT, params);
            
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_SEARCH_RESULT);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if (count <= 0) {
                throw new DataAccessException(" Cannot Update Field : " 
                                               + vo.getId());
            }
            
            addLog(vo,vo.getTemplateField().getProcedureTemplate().getId(), ProcedureTemplateLogVO.ACTION_UPDATE_SEARCH_RESULT);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * Get by ID
     * 
     * @return  search result value object
     * @param searchResultId search result ID
     */
    public SearchResultVO getById(Long searchResultId){
        // Debug query
        List params = new ArrayList();
        params.add(searchResultId);
        debugQuery(GET_SEARCH_RESULT, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_SEARCH_RESULT);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            SearchResultVO vo = new SearchResultVO();
            vo.setTemplateField(new TemplateFieldVO());
    
            vo.setOrderNo(getInteger(rs,"ORDER_NO"));
            vo.setColumnWidth(getInteger(rs,"COLUMN_WIDTH"));
            vo.getTemplateField().setId(getLong(rs,"TPE_ID"));
            vo.getTemplateField().setLabel(getString(rs,"LABLE"));
            vo.getTemplateField().setCode(getInteger(rs,"CODE"));
            vo.getTemplateField().setType(getInteger(rs,"FIELD_TYPE"));
            vo.getTemplateField().setTypeDesc(getString(rs,"FIELD_TYPE_DESC"));

            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Is search result exists 
     * 
     * @return true if exist
     * @param fieldId
     */
    public boolean isSearchResultExists(Long fieldId){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_EXIST_SEARCH_RESULT);
        params.add(fieldId);
        
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Creat search result 
     * 
     * @param templateId template ID
     * @param vo search result value object
     */
    public void create(SearchResultVO vo){
        PreparedStatement stm = null;
        try {
            // Debug query
            Long id = generateSequence("SRL_SEQ");
            List params = new ArrayList();
            params.add(id);
            params.add(vo.getOrderNo());
            params.add(vo.getTemplateField().getId());
            params.add(vo.getColumnWidth());
            params.add(vo.getCreatedBy());
            params.add(vo.getCreatedBy());

            debugQuery(CREATE_SEARCH_RESULT, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_SEARCH_RESULT);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            // add log
            addLog(vo,vo.getTemplateField().getProcedureTemplate().getId(), ProcedureTemplateLogVO.ACTION_ADD_SEARCH_RESULT);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    
    /**
     * Find search result
     * 
     * @return searchPageVO search page value object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO find(int pageNo, Long templateId) {
        ArrayList params = new ArrayList();
        params.add(templateId);
        debugQuery(FIND_SEARCH_RESULT, params);
        
        ResultSet rs = null;
        PreparedStatement stm = null;
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(FIND_SEARCH_RESULT, params);
            
            stm = doSearch(FIND_SEARCH_RESULT, params, pageNo , pageSize);
            rs  = stm.executeQuery();
            
            SearchPageVO searchPage = 
                    new SearchPageVO(pageNo, pageSize ,totalRecordCounts);   
            
            while (rs.next()) {
                SearchResultVO vo = new SearchResultVO();
                searchPage.addRecord(vo);
                vo.setTemplateField(new TemplateFieldVO());
                
                vo.setId(getLong(rs, "SRL_ID"));
                vo.setOrderNo(getInteger(rs,"ORDER_NO"));
                vo.getTemplateField().setId(getLong(rs,"TPE_ID"));
                vo.getTemplateField().setLabel(getString(rs,"LABLE"));
                vo.getTemplateField().setType(getInteger(rs,"FIELD_TYPE"));
                vo.getTemplateField().setTypeDesc(getString(rs,"FIELD_TYPE_DESC"));
                vo.setColumnWidth(getInteger(rs,"COLUMN_WIDTH"));
                
            }

            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Delete search result 
     * 
     * @param templateId template ID
     * @param vo search result value object
     */
    public void delete(SearchResultVO vo,Long templateId){
        
        PreparedStatement stm = null;
        List params = new ArrayList();
        params.add(vo.getId());
        
        debugQuery(DELETE_SEARCH_RESULT, params);
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_SEARCH_RESULT);
            setQueryParameters(stm, params);
            stm.executeUpdate();

            addLog(vo,templateId, ProcedureTemplateLogVO.ACTION_DELETE_SEARCH_RESULT);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Add log 
     * @param actionType action type
     * @param templateId template ID
     * @param vo search result value object
     */
    private void addLog(SearchResultVO vo,Long templateId, Integer actionType) {
        // Debug message
        List params = new ArrayList();
        
        params.add(actionType);
        params.add(vo.getCreatedBy());
        params.add(vo.getCreatedBy());
        params.add(templateId);
        
        debugQuery(ADD_LOG_QUERY, params);
        
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(ADD_LOG_QUERY);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Get Maximum Search Result Order Number
     * 
     * @return Maximum Search Result Order Number
     * @param templateId Template Id
     */
     public Integer getMaxSearchResultOrderNo(Long templateId) {
        ArrayList params = new ArrayList();
        params.add(templateId);
        
        debugQuery(GET_MAX_SEARCH_RESULT_ORDER_NUMBER, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(GET_MAX_SEARCH_RESULT_ORDER_NUMBER);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            Integer maxOrderNo = new Integer(0);
            if(resultSet.next()){
                maxOrderNo = getInteger(resultSet,"MAX_ORDER_NO");
                if(maxOrderNo == null)
                    maxOrderNo = new Integer(0);                
            }

            return maxOrderNo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }    
     }
     
}