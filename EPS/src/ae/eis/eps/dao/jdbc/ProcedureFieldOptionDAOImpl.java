/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.ProcedureFieldOptionDAO;
import ae.eis.eps.vo.ProcedureFieldOptionVO;
import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure field option data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureFieldOptionDAOImpl extends JdbcDataAccessObject  
                                         implements ProcedureFieldOptionDAO {
    /*
     * JDBC SQL and DML statements.
     */

    /** Get requester fields. */
    private static final String GET_FIELD_OPTIONS
    = " SELECT FON.ORDER_NO,"
    +        " FON.VALUE,"
    +        " FON.NAME_A,"
    +        " FON.NAME_E,"
    +        " FON.RELATED_PRF_ID,"
    +        " FON.RELATED_ORDER_NO,"
    +        " (SELECT VALUE FROM TF_EPS_FIELD_OPTIONS "
    +         " WHERE ORDER_NO = FON.RELATED_ORDER_NO"
    +           " AND PRF_ID = FON.RELATED_PRF_ID) AS RELATED_OPTION_VALUE"
    + " FROM TF_EPS_FIELD_OPTIONS FON"
    + " WHERE FON.PRF_ID = ?"
    + " ORDER BY FON.ORDER_NO";

    /*
     * Methods
     */

    /**
     * Get procedure field options.
     * 
     * @param fieldId procedure field ID.
     * @return procedure field options.
     */
    public List getFieldOptions(Long fieldId) {
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        debugQuery(GET_FIELD_OPTIONS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_FIELD_OPTIONS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            List optionsList = new ArrayList();
            while (rs.next()) {
                ProcedureFieldOptionVO vo = new ProcedureFieldOptionVO();
                vo.setProcedureField(new ProcedureFieldVO());
                vo.setRelatedFieldOption(new ProcedureFieldOptionVO());
                vo.getRelatedFieldOption().setProcedureField(new ProcedureFieldVO());
                optionsList.add(vo);

                vo.getProcedureField().setId(fieldId);
                vo.setOrderNo(getInteger(rs, "ORDER_NO"));
                vo.setValue(rs.getString("VALUE"));
                vo.setNameAr(getString(rs, "NAME_A"));
                vo.setNameEn(getString(rs, "NAME_E"));

                vo.getRelatedFieldOption().setOrderNo(
                    getInteger(rs, "RELATED_ORDER_NO"));
                vo.getRelatedFieldOption().setValue(
                    getString(rs, "RELATED_OPTION_VALUE"));

                vo.getRelatedFieldOption().getProcedureField().setId(
                    getLong(rs, "RELATED_PRF_ID"));
            }

            return optionsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
}