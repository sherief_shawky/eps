/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 * 
 * 1.01  Alaa Salem         04/01/2010  - Adding getByCode() Method.
 * 1.02  Ali Abdel-Aziz     05/01/2010  - Adding Code to Find Search Criteria.
 * 
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.ProcedureTemplateDAO;
import ae.eis.eps.vo.ProcedureTemplateLogVO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.trs.vo.ServiceVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Procedure template data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureTemplateDAOImpl extends JdbcDataAccessObject 
                                      implements ProcedureTemplateDAO {
    /*
     * Methods
     */
    
    /**
     * Get available template code
     * 
     * @return available template code
     */
    public List getTemplateAvailableCode(){
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = prepareStatement(getNamedQuery(
                "ae.rta.eps.dao.jdbc.ProcedureTemplate.getTemplateAvailableCode"));
            rs = stmt.executeQuery();

            List tempAvailableCode = new ArrayList();
            while ( rs.next()) {
                ProcedureTemplateVO vo = new ProcedureTemplateVO();
                tempAvailableCode.add(vo);

                vo.setCode(getInteger(rs,"CODE"));
                vo.setTemplateType(getInteger(rs,"TEMPLATE_TYPE"));
            }

            return tempAvailableCode;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }           
    }
    
    /**
     * Create new procedure template.
     * 
     * @param vo Procedure template value object.
     * @return Procedure template value object created.
     */
    public ProcedureTemplateVO create(ProcedureTemplateVO vo) {
        PreparedStatement stm = null;
        try {
            // Generate new sequence
            Long newId = generateSequence("PTL_SEQ");
            
            // Get/Initialized related named query
            NamedQuery query = getNamedQuery(
                "ae.rta.eps.dao.jdbc.ProcedureTemplate.create");

            query.setParameter("id", newId);
            query.setParameter("nameAr", vo.getTemplateNameAr());
            query.setParameter("nameEn", vo.getTemplateNameEn());
            query.setParameter("description", vo.getDescription());
            query.setParameter("priority", vo.getPriority());
            query.setParameter("createdBy", vo.getCreatedBy());
            query.setParameter("updatedBy", vo.getUpdatedBy());
            query.setParameter("templateType", vo.getTemplateType());
            query.setParameter("code", vo.getCode());
            
            // Execute DML statement
            stm = prepareStatement(query);
            stm.executeUpdate();
            
            // Save new ID
            vo.setId(newId);
            
            // Add Log
            addLog(vo, ProcedureTemplateLogVO.ACTION_CREATE_PROCEDURE);
            
            // Return updated value object
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Update procedure template.
     * 
     * @param vo Procedure template value object.
     * @return Procedure template value object created.
     */
    public void update(ProcedureTemplateVO vo) {
        // Get/Initialized related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.update");
        query.setParameter("nameAr", vo.getTemplateNameAr());
        query.setParameter("description", vo.getDescription());
        query.setParameter("priority", vo.getPriority());
        query.setParameter("updatedBy", vo.getUpdatedBy());
        query.setParameter("templateType", vo.getTemplateType());
        query.setParameter("code", vo.getCode());
        query.setParameter("id", vo.getId());

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(query);
            stm.executeUpdate();
            
            // Add Log
            addLog(vo, ProcedureTemplateLogVO.ACTION_UPDATE_PROCEDURE);
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Find Procedure Template
     * 
     * @return SearchPageVO
     * @param pageNo
     * @param vo
     */
    public SearchPageVO find(int pageNo,ProcedureTemplateVO vo) {
        // Get related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.find");
        
        if (vo.getId() != null) {
            query.appendWhereClause("AND ID = :id", "id", vo.getId());
        }
        
        if (vo.getCode() != null) {
            query.appendWhereClause(" AND CODE = :code", "code", vo.getCode());
        }
        
        if (! isBlankOrNull(vo.getTemplateNameAr())) {
            query.appendWhereClause("AND UPPER(NAME_A) LIKE UPPER(:templateNameAr)");
            query.setParameter("templateNameAr", "%"+vo.getTemplateNameAr()+"%");
        }
        
        if (vo.getStatus() != null ) {
            query.appendWhereClause("AND STATUS = :status", "status", vo.getStatus());
        }
        
        if (vo.getStatusDate() != null ) {
            query.appendWhereClause("AND STATUS_DATE >= :statusDate");
            query.appendWhereClause("AND STATUS_DATE < :statusDate + 1");
            query.setParameter("statusDate", vo.getStatusDate());
        }
        
        if (vo.getPriority() != null) {
            query.appendWhereClause("AND PRIORITY = :priority", 
                "priority", vo.getPriority());
        }

        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            SearchPageVO searchPage = getSearchPage(query, pageNo);
            prepStmt = doSearch(query, pageNo);
            resultSet = prepStmt.executeQuery();
            while(resultSet.next()) {
                ProcedureTemplateVO procedureTemplateVO = new ProcedureTemplateVO();
                procedureTemplateVO.setId(getLong(resultSet,"ID"));
                procedureTemplateVO.setCode(getInteger(resultSet,"CODE"));
                procedureTemplateVO.setTemplateNameAr(getString(resultSet,"NAME_A"));
                procedureTemplateVO.setPriority(getInteger(resultSet,"PRIORITY"));
                procedureTemplateVO.setPriorityDesc(getString(resultSet,"PRIORITY_DESC"));
                procedureTemplateVO.setStatus(getInteger(resultSet,"STATUS"));
                procedureTemplateVO.setStatusDesc(getString(resultSet,"STATUS_DESC"));
                procedureTemplateVO.setStatusDate(resultSet.getTimestamp("STATUS_DATE"));
                procedureTemplateVO.setTemplateType(getInteger(resultSet,"TEMPLATE_TYPE"));
                procedureTemplateVO.setVersionNo(getInteger(resultSet,"VERSION_NO"));

                searchPage.addRecord(procedureTemplateVO);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }        
    }
    
    /**
     * get Procedure Template By Id
     * 
     * @return 
     * @param templateId
     */
    public ProcedureTemplateVO getById(Long templateId) {
        // Get/Initialized related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getById");
        query.setParameter("templateId", templateId);
        
        ResultSet rs = null;    
        PreparedStatement prepStmt = null;
        try {
            prepStmt = prepareStatement(query);
            rs = prepStmt.executeQuery();
            
            if (! rs.next()){
                return null;
            }

            ProcedureTemplateVO vo = new ProcedureTemplateVO();
            TemplateStepVO stepVO = new TemplateStepVO();
            vo.setId(templateId);
            vo.addStep(stepVO);

            vo.setTemplateNameAr(getString(rs,"NAME_A"));
            vo.setTemplateNameEn(getString(rs,"NAME_E"));
            vo.setStatus(getInteger(rs,"STATUS"));
            vo.setStatusDesc(getString(rs,"STATUS_DESC"));
            vo.setPriority(getInteger(rs,"PRIORITY"));
            vo.setPriorityDesc(getString(rs, "PRIORITY_DESC"));
            vo.setDescription(getString(rs,"DESCRIPTION"));
            vo.setVersionNo(getInteger(rs,"VERSION_NO"));
            vo.setCode(getInteger(rs,"PTL_CODE"));
            vo.setTemplateType(getInteger(rs,"TEMPLATE_TYPE"));
            
            stepVO.setId(getLong(rs, "PTS_ID"));
            stepVO.setViewType(getInteger(rs, "PTS_VIEW_TYPE"));
            stepVO.setViewLink(getString(rs, "PTS_VIEW_LINK"));
            stepVO.setStepType(getInteger(rs, "PTS_STEP_TYPE"));

            return vo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(prepStmt);
        }           
    }
    
    /**
     * Get Templates
     * 
     * @return list of templates
     */
    public List getAvailableTemplates() {
        // Get related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getAvailableTemplates");

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();

            List templates = new ArrayList();
            while ( rs.next()) {
                ProcedureTemplateVO vo = new ProcedureTemplateVO();
                templates.add(vo);

                vo.setId(getLong(rs,"ID"));
                vo.setTemplateNameAr(getString(rs,"NAME_A"));
                vo.setPriority(getInteger(rs,"PRIORITY"));
                vo.setPriorityDesc(getString(rs,"PRIORITY_DESC"));
            }

            return templates;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
                                                                                                                                                                               
    /**
     * Get All Templates
     * 
     * @return list of templates
     */
    public List getAllTemplates() {
        // Get related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getAllTemplates");

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();

            List templates = new ArrayList();
            while (rs.next()) {
                ProcedureTemplateVO vo = new ProcedureTemplateVO();
                templates.add(vo);

                vo.setId(getLong(rs,"ID"));
                vo.setTemplateNameAr(getString(rs,"NAME_A"));
                vo.setPriority(getInteger(rs,"PRIORITY"));
                vo.setPriorityDesc(getString(rs,"PRIORITY_DESC"));
            }

            return templates;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    
    /**
     * Get Requester templates Templates
     * 
     * @return list of templates
     */
    public List getRequesterTemplates(Long userId){
        // Get/Initialized related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getRequesterTemplates");
        query.setParameter("userId", userId);

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            List templates = new ArrayList();
            while ( rs.next()) {
                ProcedureTemplateVO vo = new ProcedureTemplateVO();
                templates.add(vo);

                vo.setId(getLong(rs,"ID"));
                vo.setTemplateNameAr(getString(rs,"NAME_A"));
                vo.setPriority(getInteger(rs,"PRIORITY"));
                vo.setPriorityDesc(getString(rs,"PRIORITY_DESC"));
            }

            return templates;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }        
    }
    
    /**
     * Activate procedure template.
     * 
     * @param templateId Procedure template ID.
     * @param username Current active username.
     * @param versionNo Version number.
     */
    public void activate(Long templateId, String username,Integer versionNo) {
        // Get/Initialized related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.activate");
        query.setParameter("updatedBy", username);
        query.setParameter("versionNo", versionNo);
        query.setParameter("templateId", templateId);

        PreparedStatement stm = null;
        try {
            stm = prepareStatement(query);
            stm.executeUpdate();

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * De-activate procedure template.
     * 
     * @param templateId Procedure template ID.
     * @param username Current active username.
     */
    public void deActivate(Long templateId, String username) {
        // Get/Initialized related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.deActivate");
        query.setParameter("username", username);
        query.setParameter("templateId", templateId);

        PreparedStatement stm = null;
        try {
            stm = prepareStatement(query);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }          
    }
    
    /**
     * add Procedure Template Log
     * 
     * @param Procedure Template Log Value Object
     */
    private void addLog(ProcedureTemplateVO vo,Integer action) {
        // Get/Initialized related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.addLog");
        query.setParameter("status", vo.getStatus());
        query.setParameter("versionNo", vo.getVersionNo());
        query.setParameter("nameEn", vo.getTemplateNameEn());
        query.setParameter("actionType", action);
        query.setParameter("priority", vo.getPriority());
        query.setParameter("nameAr", vo.getTemplateNameAr());
        query.setParameter("templateId", vo.getId());
        query.setParameter("username", vo.getCreatedBy());
        
        PreparedStatement stm = null;
        try {
            stm = prepareStatement(query);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }    
    
    /**
     * Is Active Procedure
     * return true if procedure template status is active, otherwise return false
     * 
     * @return boolean(true / false)
     */
    public boolean isActiveProcedure(Long templateId){
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.isActiveProcedure");
        query.setParameter("templateId", templateId);

        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;
        try{
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
            return resultSet.next();

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }        
    }
    
    /**
     * Is Name Exists
     * 
     * @return true if template name already exists, otherwise retrun false
     * @param templateName
     */
    public boolean isNameExists(String templateName){
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.isNameExists");
        query.setParameter("nameAr", templateName);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
            return resultSet.next();

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Get user follow up templates.
     * 
     * @param userId User ID.
     * @return user follow up templates.
     */
    public List getUserFollowupTemplates(Long userId) {
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getUserFollowupTemplates");
        query.setParameter("userId", userId);

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            List templates = new ArrayList();
            while (rs.next()) {
                ProcedureTemplateVO vo = new ProcedureTemplateVO();
                templates.add(vo);
                
                vo.setId(getLong(rs,"ID"));
                vo.setTemplateNameAr(getString(rs, "NAME_A"));
            }

            return templates;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }        
    }
    
    /**
     * Is Code Exists
     * 
     * @return true if template code already exists, otherwise retrun false
     * @param code template code
     */
    public boolean isCodeExists(Integer code,Long templateId){
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.isCodeExists");
        query.setParameter("code", code);

        if (templateId != null) {
            query.appendWhereClause("AND ID <> :templateId", "templateId", templateId);
        }
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
            return resultSet.next();

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }

    /**
     * get Procedure Template By Template Code
     * 
     * @return Procedure Template VO.
     * @param templateCode Procedure Template Code.
     */
    public ProcedureTemplateVO getByCode(Integer templateCode) {
        // Get/Initialized related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getByCode");
        query.setParameter("templateCode", templateCode);
        
        ResultSet rs = null;    
        PreparedStatement prepStmt = null;
        try {
            prepStmt = prepareStatement(query);
            rs = prepStmt.executeQuery();
            
            if (! rs.next()){
                return null;
            }

            ProcedureTemplateVO vo = new ProcedureTemplateVO();
            TemplateStepVO stepVO = new TemplateStepVO();
            vo.addStep(stepVO);

            vo.setId(getLong(rs, "TEMPLATE_ID"));
            vo.setTemplateNameAr(getString(rs, "TEMPLATE_NAME_A"));
            vo.setStatus(getInteger(rs, "TEMPLATE_STATUS"));
            vo.setStatusDesc(getString(rs, "TEMPLATE_STATUS_DESC"));
            vo.setPriority(getInteger(rs, "TEMPLATE_PRIORITY"));
            vo.setPriorityDesc(getString(rs, "TEMPLATE_PRIORITY_DESC"));
            vo.setDescription(getString(rs, "TEMPLATE_DESCRIPTION"));
            vo.setVersionNo(getInteger(rs, "TEMPLATE_VERSION_NO"));
            vo.setCode(getInteger(rs, "TEMPLATE_CODE"));
            vo.setTemplateType(getInteger(rs, "TEMPLATE_TYPE"));
            
            stepVO.setId(getLong(rs, "STEP_ID"));
            stepVO.setViewType(getInteger(rs, "STEP_VIEW_TYPE"));
            stepVO.setViewLink(getString(rs, "STEP_VIEW_LINK"));
            stepVO.setStepType(getInteger(rs, "STEP_STEP_TYPE"));

            return vo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(prepStmt);
        }           
    }
    
    /**
     * Check if this template has active Procedure.
     * 
     * @param templateCode Template Code.
     * @param trafficId Traffic File Id.
     * @return true if this template has active Procedure.
     */
    public boolean hasActiveProcedure(Integer templateCode, Long trafficId) {
    
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.hasActiveProcedure");
            
        query.setParameter("templateCode", templateCode);
        query.setParameter("trafficId", trafficId);

        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;
        
        try {
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
            resultSet.next();
            return getInteger(resultSet, "COUNT").intValue() > 0;
            
        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        } 
    }
    
    /**
     * Get Initial Noc Templates.
     * 
     * @param templates codes .
     * @return List Of Initial Noc Templates.
     */
     public List getInitialNocTemplates(String templateCodes){
         NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getInitialNocTemplates");

        query.replaceQueryCode("#TempCpde#",templateCodes);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List templates = new ArrayList();
        ProcedureTemplateVO vo ;
        
        try {
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                vo = new ProcedureTemplateVO();   
                vo.setCode(getInteger(rs,"CODE"));
                vo.setDescription(getString(rs, "DESCRIPTION"));
                
                templates.add(vo);
            }

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }

        return templates;
     }
     
    /**
     * get Procedure Template Step By Sequence Number.
     * 
     * @param procedureTemplateId : procedure Template Id.
     * @param sequenceNumber : sequence Number.
     * 
     *@return Template Step VO.
     */
    public TemplateStepVO getProcedureTemplateStepBySequenceNumber(Long procedureTemplateId ,Long sequenceNumber ) {
        NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureTemplate.getProcedureTemplateStepBySequenceNumber");
        query.setParameter("procedureTemplateId", procedureTemplateId);
        query.setParameter("sequenceNumber",sequenceNumber);
        
        ResultSet rs = null;    
        PreparedStatement prepStmt = null;
        try {
            prepStmt = prepareStatement(query);
            rs = prepStmt.executeQuery();
            
            if (! rs.next()) {
                return null;
            }

            TemplateStepVO stepVO = new TemplateStepVO();
            stepVO.setId(getLong(rs,"ID"));
            
            return stepVO;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(prepStmt);
        }           
    }
    
    /**
     * Get Template By Code.
     * 
     * @return Procedure Template VO.
     */
    public ProcedureTemplateVO getTemplateByCode(Integer code) {
        // Get related named query
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureTemplate.getTemplateByCode");
            
        query.setParameter("code", code);
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            ProcedureTemplateVO vo = null;
            
            if (rs.next()) {
                vo = new ProcedureTemplateVO();
                vo.setId(getLong(rs,"ID"));
                vo.setTemplateNameAr(getString(rs,"NAME_A"));
                vo.setPriority(getInteger(rs,"PRIORITY"));
                vo.setPriorityDesc(getString(rs,"PRIORITY_DESC"));
            }
            
            return vo;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    
    /**
     * get Procedure Template By Service Code
     * 
     * @param serviceCode : Procedure Service Code.
     * @return Procedure Template VO.
     */
    public ProcedureTemplateVO getByServiceCode(Integer serviceCode) {
        
        // Get related named query
        NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureTemplate.getByServiceCode");
        
        query.setParameter("serviceCode", serviceCode);
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            ProcedureTemplateVO vo = null;
            
            if (rs.next()) {
                vo = new ProcedureTemplateVO();
                vo.setId(getLong(rs,"ID"));
                vo.setCode(getInteger(rs,"CODE"));
                vo.setTemplateNameAr(getString(rs,"NAME_A"));
                vo.setPriority(getInteger(rs,"PRIORITY"));
                vo.setPriorityDesc(getString(rs,"PRIORITY_DESC"));
                
            }
            
            return vo;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }

	/**
     * Is Template Available For User
     * 
     * @param templateId Template Id
     * @param userId User Id
     * 
     * @return true if tempalte available for user
     */
    public boolean isTemplateAvailableForUser(Long templateId,Long userId){
        NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureTemplate.isTemplateAvailableForUser");
        query.setParameter("tempId", templateId);
        query.setParameter("userId", userId);

        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;
        
        try{
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
            return resultSet.next();
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }        
    }    
    
    /**
     * lookup for Procedure Templates based on query search
     * 
     * @param query : text to search
     * @param lang : language for logged-in user
     * @param paramsMap : extra where condetion parameters Map
     * 
     * @return list of Procedure Templates matches this query
     */
    @Override
    public List<ProcedureTemplateVO> lookup(String query, String lang, int pageSize, HashMap<String,String> paramsMap) {

        // Get Named Query 
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureTemplate.lookup");
        
        PreparedStatement prepStmt = null; 
        ResultSet resultSet = null;  
        
        try {
            namedQuery.setParameter("rowNo", pageSize);
            namedQuery.setParameter("text", "%".concat(query).concat("%"));
            
            prepStmt = prepareStatement(namedQuery);
            resultSet = prepStmt.executeQuery();
            List<ProcedureTemplateVO> procedureTemplateList = new ArrayList<ProcedureTemplateVO>();
            
            while(resultSet.next()) {
                ProcedureTemplateVO procedureTemplateVO = new ProcedureTemplateVO();
                procedureTemplateVO.setId(getLong(resultSet,"ID"));
                procedureTemplateVO.setCode(getInteger(resultSet,"CODE"));
                procedureTemplateVO.setTemplateNameAr(getString(resultSet,"NAME_A"));
                procedureTemplateVO.setTemplateNameEn(getString(resultSet,"NAME_E"));
                procedureTemplateList.add(procedureTemplateVO);
            }
            return procedureTemplateList;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }
    }
    
    /**
     *  validate (id, text) for Procedure Templates recived from lookup componant
     * @param id : id of the Procedure Templates
     * @param text : Procedure Templates description based on language
     * @param lang : language for loogged-in user
     * @return true if data is valid
     */
    @Override
    public boolean validateLookupValue(String id, String text, String lang) {
 
        // Get Named Query 
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureTemplate.validateLookup");
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;  
        
        try {
            namedQuery.setParameter("id", id);
            namedQuery.setParameter("text", text);

            prepStmt = prepareStatement(namedQuery);
            resultSet = prepStmt.executeQuery();
            return resultSet.next();
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        } 
    }
    
}