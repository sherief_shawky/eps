/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.FollowUpUserDAO;
import ae.eis.eps.vo.FollowUpUserVO;
import ae.eis.eps.vo.ProcedureTemplateLogVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Follow up user data access object JDBC implementation calss.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public class FollowUpUserDAOImpl extends JdbcDataAccessObject 
                                      implements FollowUpUserDAO {
    /*
     * JDBC SQL and DML statements.
     */
    
    /** Create follow up userL. */
    private static final String CREATE_FOLLOW_UP_USER
    = " INSERT INTO TF_EPS_FOLLOW_UP_USERS"
    +  "(ID,PTL_ID,USR_ID,CREATED_BY,CREATION_DATE)"
    +  " VALUES(?,"  // ID
    +         " ?,"  // PTL_ID
    +         " ?,"  // USR_ID
    +         " ?,"  // CREATED_BY
    +         " SYSDATE)"; // CREATION_DATE
    
    /** Delete follow up user */
    private static final String DELETE_FOLLOW_UP_USER
        =   " DELETE FROM TF_EPS_FOLLOW_UP_USERS WHERE PTL_ID = ? AND USR_ID = ?";
        
    /** Find User */
    private static final String FIND_USERS
        =   " SELECT USR.ID USER_ID,"
	    +          " USR.NAME USR_NAME,"
        +          " USR.STATUS USR_STATUS,"
        +          " EMP.ID EMP_ID,"
        +          " EMP.NAME_A EMP_NAME_A"
        +    " FROM SF_INF_USERS USR,"
        +         " TF_STP_EMPLOYEES EMP,"
        +         " TF_STP_EMP_VS_USERS EMP_USR"
        +   " WHERE USR.STATUS = 2"
        +     " AND USR.ID = EMP_USR.USR_USR_ID"
        +     " AND EMP_USR.EMP_ID = EMP.ID"
        +     " AND NOT EXISTS(SELECT 1 FROM TF_EPS_FOLLOW_UP_USERS FUU "
        +                             " WHERE FUU.USR_ID = USR.ID"
        +                               " AND FUU.PTL_ID = ?) ";
    
    /** Get follow up User */    
    private static final String FIND_FOLLOW_UP_USERS
        =   " SELECT FUU.USR_ID FUU_USR_ID,"
		+          " USR.NAME USER_NAME,"
        +          " EMP.NAME_A EMP_NAME_A"
        +     " FROM TF_EPS_FOLLOW_UP_USERS FUU,"
		+          " SF_INF_USERS USR,"
        +          " TF_STP_EMPLOYEES EMP,"
        +          " TF_STP_EMP_VS_USERS EMP_USR"
        +    " WHERE FUU.USR_ID = USR.ID"
        +      " AND USR.ID = EMP_USR.USR_USR_ID"
        +      " AND EMP_USR.EMP_ID = EMP.ID"
	   	+      " AND FUU.PTL_ID = ?";
        
    /** add log query */
    private static final String ADD_LOG_QUERY
        =    "INSERT INTO TF_EPS_PROCEDURE_TEMP_LOGS "
        +    "(ID,STATUS,VERSION_NO,NAME_E,ACTION_TYPE,PRIORITY,NAME_A, "
        +    "STATUS_DATE,PTL_ID,EMPLOYEE_NAME,CREATED_BY,CREATION_DATE) "
        +    "SELECT PTG_SEQ.NEXTVAL, "
        +          " PTL.STATUS,"
        +          " PTL.VERSION_NO,"
        +          " PTL.NAME_E,"
        +          " ?,"
        +          " PTL.PRIORITY,"
        +          " PTL.NAME_A,"
        +          " SYSDATE,"
        +          " PTL.ID,"
        +          " F_DB_GET_EMP_NAME_A(?),"
        +          " ?,"
        +          " SYSDATE"
        +     " FROM TF_EPS_PROCEDURE_TEMPLATES PTL"
        +    " WHERE PTL.ID = ? ";
    
    /** Is follow up user exist */
    private static final String IS_FOLLOW_UP_USER_EXIST
        ="SELECT 1 FROM TF_EPS_FOLLOW_UP_USERS WHERE USR_ID = ? AND PTL_ID =?";
        
    /*
     * Methods
     */
     
    /**
     * Is follow up user exists
     * 
     * @return true if foolow up user exist
     * @param templateId template ID
     * @param usrId user ID
     */
    public boolean isFollowUpUserExists(Long usrId, Long templateId){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_FOLLOW_UP_USER_EXIST);
        params.add(usrId);
        params.add(templateId);
        
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
     /**
     * Add log 
     * @param actionType action type
     * @param templateId template ID
     * @param vo follow up user value object
     */
    private void addLog(FollowUpUserVO vo,Long templateId, Integer actionType) {
        // Debug message
        List params = new ArrayList();
        
        params.add(actionType);
        params.add(vo.getCreatedBy());
        params.add(vo.getCreatedBy());
        params.add(templateId);
        
        debugQuery(ADD_LOG_QUERY, params);
        
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(ADD_LOG_QUERY);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Create new follow up user.
     * 
     * @param vo follow up user value object.
     */
    public void create(FollowUpUserVO vo) {
        // Debug query
        Long id = generateSequence("FUU_SEQ");
        List params = new ArrayList();
        params.add(id);
        params.add(vo.getProcedureTemplate().getId());
        params.add(vo.getUser().getId());
        params.add(vo.getCreatedBy());
        debugQuery(CREATE_FOLLOW_UP_USER, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_FOLLOW_UP_USER);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            addLog(vo,vo.getProcedureTemplate().getId(), ProcedureTemplateLogVO.ACTION_ADD_SEARCH_SECURITY);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Delete follow up user.
     * 
     * @param vo follow up user value object.
     */
    public void delete(FollowUpUserVO vo){
        // Debug query
        List params = new ArrayList();
        params.add(vo.getProcedureTemplate().getId());
        params.add(vo.getUser().getId());
        debugQuery(DELETE_FOLLOW_UP_USER, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_FOLLOW_UP_USER);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            addLog(vo,vo.getProcedureTemplate().getId(), ProcedureTemplateLogVO.ACTION_DELETE_SEARCH_SECURITY);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Get follow up user.
     * 
     * @param vo follow up user value object.
     * @return follow up user value object created.
     */
    public SearchPageVO find(int pageNo,FollowUpUserVO vo){
        PreparedStatement stm = null;
        ResultSet rs = null;    
        StringBuffer sqlQuery = new StringBuffer(FIND_FOLLOW_UP_USERS);
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(vo.getProcedureTemplate().getId());
        debugQuery(FIND_FOLLOW_UP_USERS.toString(), listOfparams);

        try {
        
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString() , 
                                                            listOfparams);
            stm = doSearch(sqlQuery.toString(), listOfparams, pageNo , 
                                                                pageSize);
            rs=stm.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(rs.next()){                
                
                EmployeeVO employee = new EmployeeVO();
                employee.setEmployeeNameAr(getString(rs,"EMP_NAME_A"));
            
                UserVO user = new UserVO();
                user.setId(getLong(rs,"FUU_USR_ID"));
                user.setName(getString(rs,"USER_NAME"));
                user.setEmployee(employee);
                
                FollowUpUserVO followUpUser = new FollowUpUserVO();
                followUpUser.setUser(user);
                
                searchPage.addRecord(followUpUser);
            }
            
            return searchPage;
                                
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }            
    }
    
    /**
     * Find Security users
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findUsers(int pageNo,FollowUpUserVO vo){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        StringBuffer sqlQuery = new StringBuffer(FIND_USERS);
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(vo.getProcedureTemplate().getId());
        
        if(vo.getUser() != null){
            if(vo.getUser().getId() != null ){
                sqlQuery.append(" AND USR.ID = ? ");
                listOfparams.add(vo.getUser().getId());
            }
            
            if(!isBlankOrNull(vo.getUser().getName())){
                sqlQuery.append(" AND UPPER (USR.NAME) LIKE  UPPER (?) ");
                listOfparams.add("%"+vo.getUser().getName()+"%");
            }          
            
            if(vo.getUser().getEmployee() != null){
                if(vo.getUser().getEmployee().getId() != null){
                    sqlQuery.append(" AND EMP.ID = ? ");
                    listOfparams.add(vo.getUser().getEmployee().getId());
                }
                
                if(!isBlankOrNull(vo.getUser().getEmployee().getEmployeeNameAr())){
                   sqlQuery.append(" AND EMP.NAME_A LIKE ? ");
                   listOfparams.add("%"+vo.getUser().getEmployee().getEmployeeNameAr()+"%");
                }
            }
            
        }
        
        debugQuery(sqlQuery, listOfparams);
        
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString() , 
                                                            listOfparams);
            prepStmt = doSearch(sqlQuery.toString(), listOfparams, pageNo , 
                                                                pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()){                
                                
                EmployeeVO employee = new EmployeeVO();
                employee.setId(getLong(resultSet,"EMP_ID"));
                employee.setEmployeeNameAr(getString(resultSet,"EMP_NAME_A"));
                
                UserVO user = new UserVO();
                user.setId(getLong(resultSet,"USER_ID"));
                user.setStatus(getInteger(resultSet,"USR_STATUS"));
                user.setName(getString(resultSet,"USR_NAME"));
                user.setEmployee(employee);
                
                FollowUpUserVO followUpUser = new FollowUpUserVO();
                followUpUser.setUser(user);
                
                searchPage.addRecord(followUpUser);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }
    
}