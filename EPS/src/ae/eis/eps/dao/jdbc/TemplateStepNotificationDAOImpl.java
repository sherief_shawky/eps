/*
 * Copyright (c) i-Soft 2007.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   18/05/2009  - File created.
 */
 
package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateStepNotificationDAO;
import ae.eis.eps.vo.TemplateStepNotificationVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Template Step Notification Business Object
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public class TemplateStepNotificationDAOImpl extends JdbcDataAccessObject
                                         implements TemplateStepNotificationDAO{
                               
    /** Create Notification Query */                                         
    private static final String CREATE_NOTIFICATION
        =   " INSERT INTO TF_EPS_TEMP_STEP_NOTIFICATIONS("
        +               " ID,"
        +   			" TITLE,"
		+	            " MESSAGE_BODY,"
		+   	        " NOTIFICATION_TYPE,"
		+	            " PTS_ID,"
		+	            " CREATED_BY,"
		+	            " UPDATE_DATE,"
		+	            " UPDATED_BY,"
		+       	    " CREATION_DATE) "
        +       " VALUES(?,"        // ID
        +               "?,"        // TITLE
        +               "?,"        // MESSAGE_BODY
        +               "?,"        // NOTIFICATION_TYPE
        +               "?,"        // PTS_ID
        +               "?,"        // CREATED_BY
        +               "SYSDATE,"  
        +               "?,"        // UPDATED_BY
        +               "SYSDATE)";
    
    /** Update Notification Query */                                         
    private static final String UPDATE_NOTIFICATION
        = " UPDATE TF_EPS_TEMP_STEP_NOTIFICATIONS "
        +    " SET TITLE = ?,"
	    +        " MESSAGE_BODY = ?,"
	    +        " NOTIFICATION_TYPE = ?,"
	    +        " UPDATE_DATE = SYSDATE,"
        +        " UPDATED_BY = ?"
        +  " WHERE ID = ?  ";
    
    
    /** Find Notification Query */
    private static final String FIND_NOTIFICATION
        =   " SELECT ID,"
		+          " TITLE,"
		+          " NOTIFICATION_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_NOTIFICATION_TYPE',NOTIFICATION_TYPE) NOTIFICATION_TYPE_DESC, "
		+          " PTS_ID"
        +     " FROM TF_EPS_TEMP_STEP_NOTIFICATIONS"
        +    " WHERE PTS_ID = ? "; 
    
    /** get Notification Query */
    private static final String GET_NOTIFICATION
        =   " SELECT ID,"
		+          " TITLE,"
		+          " MESSAGE_BODY,"
		+          " NOTIFICATION_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_NOTIFICATION_TYPE',NOTIFICATION_TYPE) NOTIFICATION_TYPE_DESC "
        +     " FROM TF_EPS_TEMP_STEP_NOTIFICATIONS"
        +    " WHERE ID = ? ";     
        
    /** Delete Notification Query */                                         
    private static final String DELETE_NOTIFICATION
        =   " DELETE FROM TF_EPS_TEMP_STEP_NOTIFICATIONS WHERE ID = ?";

    /** Has Invalid Forward */        
    private static final String HAS_INVALID_NOTIFICATIONS
        =   " SELECT PTS.STEP_NAME_A "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTS.PTL_ID = ? "
        +      " AND PTS.STEP_TYPE = 2 "
        +      " AND NOT EXISTS(SELECT 1 FROM TF_EPS_TEMP_STEP_NOTIFICATIONS TSN "
        +                             " WHERE TSN.PTS_ID = PTS.ID ) ";
    
    /*
     * Methods
     */
    /**
     * Create New Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public TemplateStepNotificationVO create(TemplateStepNotificationVO vo) {
        Long id = generateSequence("TSN_SEQ");
        
        List params = new ArrayList();
        params.add(id);
        params.add(vo.getTitle());
        params.add(vo.getMessageBody());
        params.add(vo.getNotificationType());
        params.add(vo.getTemplateStep().getId());
        params.add(vo.getCreatedBy());
        params.add(vo.getUpdatedBy());
        
        // Debug query
        debugQuery(CREATE_NOTIFICATION, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_NOTIFICATION);
            setLong(stm, 1, id);
            stm.setString(2, vo.getTitle());
            stm.setString(3, vo.getMessageBody());
            setInteger(stm, 4, vo.getNotificationType());
            setLong(stm, 5, vo.getTemplateStep().getId());
            setString(stm, 6, vo.getCreatedBy());
            setString(stm, 7, vo.getUpdatedBy());

            stm.executeUpdate();
            
            // Save new ID and return updated value object
            vo.setId(id);
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }       
    
    /**
     * Update Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public void update(TemplateStepNotificationVO vo) {
        List params = new ArrayList();
        params.add(vo.getTitle());
        params.add(vo.getMessageBody());
        params.add(vo.getNotificationType());
        params.add(vo.getUpdatedBy());
        params.add(vo.getId());

        // Debug query
        debugQuery(UPDATE_NOTIFICATION, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_NOTIFICATION);
            stm.setString(1, vo.getTitle());
            stm.setString(2, vo.getMessageBody());
            setInteger(stm, 3, vo.getNotificationType());
            setString(stm, 4, vo.getUpdatedBy());
            setLong(stm, 5, vo.getId());

            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    
    /**
     * find Template Step Notification
     * 
     * @return SearchPageVO
     * @param pageNo
     * @param Template Step Notification Value Object
     */
    public SearchPageVO find(int pageNo,TemplateStepNotificationVO vo){
        List listOfparams=new ArrayList();
        listOfparams.add(vo.getTemplateStep().getId());        
        debugQuery(FIND_NOTIFICATION, listOfparams);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(FIND_NOTIFICATION, listOfparams);
            prepStmt =doSearch(FIND_NOTIFICATION, listOfparams, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize, 
                                                        totalRecordCounts);
            while(resultSet.next()) {
                
                TemplateStepVO  stepVO = new TemplateStepVO();
                stepVO.setId(getLong(resultSet,"PTS_ID"));
                
                TemplateStepNotificationVO stepNtfVO = new TemplateStepNotificationVO();
                stepNtfVO.setId(getLong(resultSet,"ID"));
                stepNtfVO.setTitle(getString(resultSet,"TITLE"));
                stepNtfVO.setNotificationType(getInteger(resultSet,"NOTIFICATION_TYPE"));
                stepNtfVO.setNotificationTypeDesc(getString(resultSet,"NOTIFICATION_TYPE_DESC"));
                stepNtfVO.setTemplateStep(stepVO);
                
                searchPage.addRecord(stepNtfVO);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }  
    }
    
    /**
     * get Template Step Notification
     * 
     * @return Template Step Notification Value Object
     * @param ntfStepId
     */
    public TemplateStepNotificationVO getById(Long ntfStepId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        String sql = GET_NOTIFICATION;
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(ntfStepId);
        debugQuery(sql, listOfparams);
        
        try{
            prepStmt = getConnection().prepareStatement(sql);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
            
            if (! resultSet.next()){
                return null;
            }

            TemplateStepNotificationVO vo = new TemplateStepNotificationVO();
            vo.setId(getLong(resultSet,"ID"));
            vo.setTitle(getString(resultSet,"TITLE"));
            vo.setMessageBody(resultSet.getString("MESSAGE_BODY"));
            vo.setNotificationType(getInteger(resultSet,"NOTIFICATION_TYPE"));
            vo.setNotificationTypeDesc(getString(resultSet,"NOTIFICATION_TYPE_DESC"));

            return vo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }            
    }
    
    /**
     * Delete Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public void delete(TemplateStepNotificationVO vo) {
        
        List params = new ArrayList();
        params.add(vo.getId());
        
        // Debug query
        debugQuery(DELETE_NOTIFICATION, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_NOTIFICATION);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }  
    
    /**
     * Has Invalid Notifications
     * The notification message must be added when the step type is notification
     * -  EPS_PTT_010
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidNotificationSteps(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(HAS_INVALID_NOTIFICATIONS.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List list = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(HAS_INVALID_NOTIFICATIONS);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            while(rs.next()){
                TemplateStepVO vo = new TemplateStepVO();
                vo.setNameAr(getString(rs,"STEP_NAME_A"));
                list.add(vo);
            }
            
            return list;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
    
}