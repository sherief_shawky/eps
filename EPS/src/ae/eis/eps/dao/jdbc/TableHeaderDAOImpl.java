/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mena Emiel  23/02/2011  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TableHeaderDAO;
import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.TableHeaderVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Table header data access object JDBC implementation.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public class TableHeaderDAOImpl extends JdbcDataAccessObject implements TableHeaderDAO {
    /*
     * Methods
     */
     
     /**
     * Get Column Info
     * 
     * @param fieldId Field Id
     * @param columnCode Column Code
     * 
     * @return TableHeaderVO Table Header Info
     */
    public TableHeaderVO getColumnInfo(Long fieldId, Integer columnCode) {
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TableHeader.getColumnInfo"); 
        namedQuery.setParameter("fieldId", fieldId);
        namedQuery.setParameter("columnCode", columnCode); 
        
        // Debug Query
        debugQuery(namedQuery);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        try {
            prepStmt = prepareStatement(namedQuery);
            resultSet = prepStmt.executeQuery();
            
            if (!resultSet.next()) {
                return null;    
            }
            
            // Initiate instance of type TemplateTableHeaderVO
            TableHeaderVO tableHeaderVO = new TableHeaderVO();
                                
            // Assign values for the initiated object
            tableHeaderVO.setId(getLong(resultSet, "ID"));
            tableHeaderVO.setColumnType(getLong(resultSet, "COLUMN_TYPE"));
            tableHeaderVO.setNameAr(getString(resultSet, "NAME_A"));
            tableHeaderVO.setNameEn(getString(resultSet, "NAME_E"));
            tableHeaderVO.setLableWidth(getLong(resultSet, "LABLE_WIDTH"));
            tableHeaderVO.setIsHidden(getInteger(resultSet, "IS_HIDDEN"));
            tableHeaderVO.setOrderNo(getLong(resultSet, "ORDER_NO"));
            tableHeaderVO.setMaxSize(getLong(resultSet, "MAX_SIZE"));
            tableHeaderVO.setProcedureFieldVO(new ProcedureFieldVO());
            tableHeaderVO.getProcedureFieldVO().setId(getLong(resultSet, "PRF_ID"));
            tableHeaderVO.setCode(getInteger(resultSet, "CODE"));
            tableHeaderVO.setIsMandatory(getInteger(resultSet, "IS_MANDATORY"));
            
            // Return templateTableHeaderVO instance
            return tableHeaderVO;
        
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }
    }
}