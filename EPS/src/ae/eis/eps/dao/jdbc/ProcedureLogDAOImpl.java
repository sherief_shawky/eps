/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  19/05/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.ProcedureLogDAO;
import ae.eis.eps.vo.ProcedureLogVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.eps.vo.SearchFieldVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Procedure logs access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureLogDAOImpl extends JdbcDataAccessObject 
                                 implements ProcedureLogDAO {
    /*
     * JDBC SQL and DML statements.
     */
       
    /**Get allocating Query */
    private static final String FIND_PROCEDURE_HISTORY
    = " SELECT ID "
        +" ,ACTION_TYPE "
        +" ,EMPLOYEE_NAME "
        +" ,STEP_NAME_A "
        +" ,STEP_TYPE "
        +" ,CREATION_DATE "
        +" ,PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC "
        +" ('TF_EPS_STEP_TYPE',STEP_TYPE) STEP_TYPE_DESC "
        +" ,PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC "
        +" ('TF_EPS_PLG_ACTION_TYPE',ACTION_TYPE) ACTION_TYPE_DESC "
        +"FROM  TF_EPS_PROCEDURE_LOGS "
        +"WHERE PRD_ID = ? ";
    
    /** Get info by ID. */
    private static final String GET_INFO_BY_ID
    = " SELECT ID "
        +" ,ACTION_TYPE "
        +" ,EMPLOYEE_NAME "
        +" ,STEP_NAME_A "
        +" ,STEP_TYPE "
        +" ,CREATION_DATE "
        +" ,NOTE "
        +" ,PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC "
        +" ('TF_EPS_STEP_TYPE',STEP_TYPE) STEP_TYPE_DESC "
        +" ,PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC "
        +" ('TF_EPS_PLG_ACTION_TYPE',ACTION_TYPE) ACTION_TYPE_DESC "
        +"FROM  TF_EPS_PROCEDURE_LOGS "
    +   " WHERE ID = ?";
    
    /** Find User Logs */
    private static final String FIND_USER_LOGS
    = " SELECT PLG.ID"
    +" ,PLG.PRD_ID"
    +" ,PLG.ACTION_TYPE "
    +" ,PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_PLG_ACTION_TYPE',PLG.ACTION_TYPE) ACTION_TYPE_DESC"
    +" ,PLG.STEP_NAME_A "        
    +" ,PLG.CREATION_DATE "
    +" ,PRD.CREATION_DATE PRD_CREATION_DATE"
    +" ,PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_PRIORITY',PRD.PRIORITY) PRIORITY_DESC"
    +" ,PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_PROCEDURE_STATUS',PRD.STATUS) STATUS_DESC"
    +" ,PRD.TEMPLATE_NAME "
    +"FROM "  
    + " TF_EPS_PROCEDURE_LOGS PLG, "
    + " TF_EPS_PROCEDURES PRD "
    +"WHERE PRD.ID =  PLG.PRD_ID "
    +"AND UPPER(PLG.CREATED_BY) = UPPER(?) ";//PLG.CREATED_BY    

    /** Date search field Sub-Query */
    private static final String DATE_PERIOD_SEARCH_FIELD_SUB_QUERY
        = " AND EXISTS(SELECT 1 FROM TF_EPS_PROCEDURE_FIELDS S_PRF"
        +            " WHERE S_PRF.PRD_ID = PRD.ID"
        +              " AND S_PRF.FIELD_TYPE = " + TemplateFieldVO.TYPE_DATE
        +              " AND S_PRF.CODE = ?"
        +              " :dateFrom"
        +              " :dateTo"
        +            " )";

    /** Search field Sub-Query */
    private static final String SEARCH_FIELD_SUB_QUERY
        = " AND EXISTS(SELECT 1 FROM TF_EPS_PROCEDURE_FIELDS S_PRF"
        +            " WHERE S_PRF.PRD_ID = PRD.ID"
        +              " AND S_PRF.CODE = ?"
        +              " AND S_PRF.VALUE :type ?"
        +            " )";

    /*
     * Methods
     */

    
    /**
     * Get by id
     * 
     * @return ProcedureLogVO
     * @param procedureLogId
     */
    public ProcedureLogVO getById(Long procedureLogId) {
        
        List params = new ArrayList();
        params.add(procedureLogId);
        debugQuery(GET_INFO_BY_ID, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(GET_INFO_BY_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            ProcedureLogVO procedureVo = new ProcedureLogVO(); 
            procedureVo.setStep(new ProcedureStepVO());
            
            procedureVo.setId(getLong(rs,"ID"));
            procedureVo.setEmployeeName(getString(rs,"EMPLOYEE_NAME"));
            procedureVo.setCreationDate(getDate(rs,"CREATION_DATE"));
            procedureVo.getStep().setNameAr(getString(rs,"STEP_NAME_A"));
            procedureVo.getStep().setStepTypeDesc(getString(rs,"STEP_TYPE_DESC"));
            procedureVo.setActionTypeDesc(getString(rs,"ACTION_TYPE_DESC"));
            procedureVo.setNote(getString(rs,"NOTE"));
            
            return procedureVo;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * Find proceudre history
     * 
     * @return search page value object
     * @param procedureId 
     * @param vo Proceure log value object
     * @param dateFrom
     * @param dateTo
     * @param pageNo page number
     */
    public SearchPageVO find(int pageNo,Long procedureId, ProcedureLogVO vo,
                                            Date dateFrom, Date dateTo){
        StringBuffer sqlQuery   = new StringBuffer(FIND_PROCEDURE_HISTORY.toString());
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(procedureId);
        
        if(dateFrom!=null){
          sqlQuery.append(" AND CREATION_DATE >= ? ")  ; 
          listOfparams.add(dateFrom);
        }
        
        if(dateTo!=null){
          sqlQuery.append(" AND CREATION_DATE < ? + 1  ")  ;         
          listOfparams.add(dateTo);
        }
        
        if(vo.getActionType()!=null){
          sqlQuery.append(" AND  ACTION_TYPE = ?  ");         
          listOfparams.add(vo.getActionType());
        }
        
        if(vo.getStep().getStepType()!=null){
          
          sqlQuery.append(" AND STEP_TYPE = ? ");         
          listOfparams.add(vo.getStep().getStepType());
        }
        
        sqlQuery.append(" ORDER BY CREATION_DATE DESC ");      
        debugQuery(sqlQuery, listOfparams);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString() , 
                                                            listOfparams);
            prepStmt = doSearch(sqlQuery.toString(), listOfparams, pageNo, pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()){                
                ProcedureLogVO procedureVo = new ProcedureLogVO(); 
                procedureVo.setStep(new ProcedureStepVO());
                
                procedureVo.setId(getLong(resultSet,"ID"));
                procedureVo.setEmployeeName(getString(resultSet,"EMPLOYEE_NAME"));
                procedureVo.setCreationDate(getDate(resultSet,"CREATION_DATE"));
                procedureVo.getStep().setNameAr(getString(resultSet,"STEP_NAME_A"));
                procedureVo.getStep().setStepTypeDesc(getString(resultSet,"STEP_TYPE_DESC"));
                procedureVo.setActionTypeDesc(getString(resultSet,"ACTION_TYPE_DESC"));
                                
                searchPage.addRecord(procedureVo);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }
    
    /**
     * Add step notification log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    public boolean addNotificationLog(Long procedureId,Long userCenterId) {
        
        NamedQuery insertLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.insertLog");
        NamedQuery addNtfLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.addNtfLog");
        
        insertLog.appendWhereClause(addNtfLog.getQuery());
        
        insertLog.setParameter("actionType", ProcedureLogVO.ACTION_SEND_NOTIFICATION);
        insertLog.setParameter("centerId", userCenterId);
        insertLog.setParameter("stepType", ProcedureStepVO.TYPE_NOTIFICATION);
        insertLog.setParameter("procedureId", procedureId);
        
        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(insertLog);
            int count = stm.executeUpdate();
            return count > 0;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Add business action log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    public boolean addBusinessActionLog(Long procedureId,Long userCenterId) {
                
        NamedQuery insertLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.insertLog");
        NamedQuery addBusinessLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.addBusinessActionLog");
        
        insertLog.appendWhereClause(addBusinessLog.getQuery());
        
        insertLog.setParameter("actionType", ProcedureLogVO.ACTION_RUN_BUSINESS_ACTION);
        insertLog.setParameter("centerId", userCenterId);
        insertLog.setParameter("stepType", ProcedureStepVO.TYPE_BUSINESS_ACTION);
        insertLog.setParameter("procedureId", procedureId);
        
        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm =  prepareStatement(insertLog);
            int count = stm.executeUpdate();

            return count > 0;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Add end procedure log.
     * 
     * @param procedureId Procedure ID
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    public boolean addEndProcedureLog(Long procedureId,Long userCenterId) {
        NamedQuery insertLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.insertLog");
        NamedQuery addEndProcLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.addEndProcedureLog");
        
        insertLog.appendWhereClause(addEndProcLog.getQuery());
        
        insertLog.setParameter("actionType", ProcedureLogVO.ACTION_END_PROCEDURE);
        insertLog.setParameter("centerId", userCenterId);
        insertLog.setParameter("stepType", ProcedureStepVO.TYPE_END);
        insertLog.setParameter("procedureId", procedureId);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = stm =  prepareStatement(insertLog);
            int count = stm.executeUpdate();

            return count > 0;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Add step action log.
     * 
     * @param vo Procedure log value object.
     * @param userCenterId user Center ID
     * @return true if the log was saved successfuly
     */
    public boolean addActionLog(ProcedureLogVO vo, Long userCenterId) {
        
        NamedQuery insertLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.insertLog");
        NamedQuery addActionLog = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureLog.addActionLog");
        
        insertLog.appendWhereClause(addActionLog.getQuery());
        
        insertLog.setParameter("actionType", vo.getActionType());
        insertLog.setParameter("lastNote", vo.getNote());
        insertLog.setParameter("centerId", userCenterId);
        insertLog.setParameter("userName", vo.getCreatedBy());
        insertLog.setParameter("procedureId", vo.getStep().getProcedure().getId());
                    
        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = prepareStatement(insertLog);
            int count = stm.executeUpdate();

            return count > 0;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Append search field "date period" sub-query to main search query.
     * 
     * @param searchQuery Main search query.
     * @param parameters Query parameters.
     * @param vo procedure search field value object.
     */
    private void appendDatePeriodSearchFieldSubQuery(StringBuffer searchQuery,
                                                     List parameters,
                                                     SearchFieldVO vo) {
        // Validate search field value
        if (vo.getDateFrom() == null && vo.getDateTo() == null) {
            return;
        }

        // Build sub-query
        StringBuffer sql = new StringBuffer(DATE_PERIOD_SEARCH_FIELD_SUB_QUERY);
        parameters.add(vo.getTemplateField().getCode());

        if (vo.getDateFrom() != null) {
            replaceAll(sql, ":dateFrom", "AND TO_DATE(S_PRF.VALUE, 'dd-mm-yyyy') >= ?");
            parameters.add(vo.getDateFrom());

        } else {
            replaceAll(sql, ":dateFrom", "");
        }

        if (vo.getDateTo() != null) {
            replaceAll(sql, ":dateTo", "AND TO_DATE(S_PRF.VALUE, 'dd-mm-yyyy') <= ?");
            parameters.add(vo.getDateTo());

        } else {
            replaceAll(sql, ":dateTo", "");
        }

        // Append sub-query to main search query
        searchQuery.append(sql.toString());
    }

    /**
     * Append search field sub-query to main search query.
     * 
     * @param searchQuery Main search query.
     * @param parameters Query parameters.
     * @param vo procedure search field value object.
     */
    private void appendSearchFieldSubQuery(StringBuffer searchQuery,
                                           List parameters,
                                           SearchFieldVO vo) {
        // Validate search field value
        if (vo == null) {
            throw new DataAccessException("NULL SearchFieldVO");
        }

        if (vo.getTemplateField() == null) {
            throw new DataAccessException("NULL SearchFieldVO.templateField");
        }

        if (vo.getTemplateField().getCode() == null) {
            throw new DataAccessException("NULL SearchFieldVO.templateField.code");
        }

        if (vo.getTemplateField().getType() == null) {
            throw new DataAccessException("NULL SearchFieldVO.templateField.type");
        }

        // Check if field type is "Date" and search type is between
        if (vo.getTemplateField().isDateField() && vo.isSearchTypeBetween()) {
            appendDatePeriodSearchFieldSubQuery(searchQuery, parameters, vo);
            return;
        }

        // Skip if field value was not initialized
        if (vo.getTemplateField().getValue() == null ||
            isBlankOrNull(vo.getTemplateField().getValue().toString())) {
            return;
        }

        // Build sub-query
        StringBuffer sql = new StringBuffer(SEARCH_FIELD_SUB_QUERY);
        parameters.add(vo.getTemplateField().getCode());

        if (vo.isSearchTypeEqual()) {
            replaceAll(sql, ":type", "=");
            parameters.add(vo.getTemplateField().getValue().toString());

        } else if (vo.isSearchTypeLike()) {
            replaceAll(sql, ":type", "LIKE");
            parameters.add("%" + vo.getTemplateField().getValue() + "%");

        } else {
            throw new DataAccessException("Invalid search type: " + vo.getSearchType());
        }

        // Append sub-query to main search query
        searchQuery.append(sql.toString());
    }


    /**
     * Find User logs
     * 
     * @return search page value object      
     * @param pageNo page number
     * @param vo Proceure log value object
     * @param creationDateFrom
     * @param creationDateTo
     * @param actionDateFrom
     * @param actionDateTo
     */
    public SearchPageVO findUserLogs(
                                    int pageNo, ProcedureLogVO vo, String createdBy,
                                    Date creationDateFrom, Date creationDateTo,
                                    Date actionDateFrom, Date actionDateTo,
                                    List searchFields) {
        
        StringBuffer query   = new StringBuffer(FIND_USER_LOGS);
        ArrayList params = new ArrayList();
        params.add(createdBy);
        
        ProcedureStepVO procedureStepVO = vo.getStep();
        
        if(actionDateFrom!=null){
          query.append(" AND PLG.CREATION_DATE >= ? ")  ; 
          params.add(actionDateFrom);
        }
        
        if(actionDateTo!=null){
          query.append(" AND PLG.CREATION_DATE < ? + 1  ")  ;         
          params.add(actionDateTo);
        }
        
        if (creationDateFrom != null) {
            query.append(" AND PRD.CREATION_DATE >= ?");
            params.add(creationDateFrom);
        }

        if (creationDateTo != null) {
            query.append(" AND PRD.CREATION_DATE < ? + 1");
            params.add(creationDateTo);
        }

        if(procedureStepVO!=null) {
            
            ProcedureVO procedureVO = procedureStepVO.getProcedure();
            
            if(procedureVO!=null) {
                
                if(procedureVO.getRequester()!=null) {
                    query.append(" AND PRD.USR_ID = ? ");         
                    params.add(procedureVO.getRequester().getId());                    
                }
                                
                if(procedureVO.getId()!=null) {
                    query.append(" AND PRD.ID = ?");
                    params.add(procedureVO.getId());
                }
                
                if(procedureVO.getStatus()!=null) {
                    query.append(" AND PRD.STATUS = ?");
                    params.add(procedureVO.getStatus());                    
                }
                
                if (procedureVO.getTemplate() != null && procedureVO.getTemplate().getId() != null) {
                    query.append(" AND PRD.PTL_ID = ?");
                    params.add(procedureVO.getTemplate().getId());
                }
                
                if (procedureVO.getPriority() != null) {
                    query.append(" AND PRD.PRIORITY = ?");
                    params.add(procedureVO.getPriority());
                }                
            }
        }

        // Add search fields filters code
        for (int i = 0; searchFields!= null && i < searchFields.size(); i++) {
            SearchFieldVO searcfFieldVO = (SearchFieldVO) searchFields.get(i);
            appendSearchFieldSubQuery(query, params, searcfFieldVO);
        }

        query.append(" ORDER BY PLG.CREATION_DATE DESC ");      
        debugQuery(query, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(query.toString() , 
                                                            params);
            prepStmt = doSearch(query.toString(), params, pageNo, pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                    
                                                        totalRecordCounts);
                                                                    
            while(resultSet.next()){                
                ProcedureLogVO procedureLogVo = new ProcedureLogVO(); 
                
                procedureLogVo.setStep(new ProcedureStepVO());
                procedureLogVo.getStep().setProcedure(new ProcedureVO());

                procedureLogVo.setCreationDate(getDate(resultSet,"CREATION_DATE"));
                procedureLogVo.getStep().getProcedure().setCreationDate(getDate(resultSet,"PRD_CREATION_DATE"));
                
                procedureLogVo.getStep().getProcedure().setPriorityDesc(getString(resultSet,"PRIORITY_DESC"));
                procedureLogVo.getStep().getProcedure().setStatusDesc(getString(resultSet,"STATUS_DESC"));
                procedureLogVo.getStep().setNameAr(getString(resultSet,"STEP_NAME_A"));
                procedureLogVo.setActionType(getInteger(resultSet,"ACTION_TYPE"));
                procedureLogVo.setActionTypeDesc(getString(resultSet,"ACTION_TYPE_DESC"));
                procedureLogVo.getStep().getProcedure().setTemplateNameAr(getString(resultSet,"TEMPLATE_NAME"));
                procedureLogVo.getStep().getProcedure().setId(getLong(resultSet,"PRD_ID"));
                procedureLogVo.setId(getLong(resultSet,"ID"));
                                
                searchPage.addRecord(procedureLogVo);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }                                                
    }
}