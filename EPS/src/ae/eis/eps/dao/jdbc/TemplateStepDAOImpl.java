/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 * 1.01  Ali Abdel-Aziz     11/01/2010  - Adding NotifyRequester to ALL 
 *                                        TF_EPS_PROCEDURE_TEMP_STEPS Queries.
 * 1.02  Ali Abdel-Aziz     11/01/2010  - Moding queries to XML
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.TemplateStepDAO;
import ae.eis.eps.vo.ProcedureTemplateLogVO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure template step data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepDAOImpl extends JdbcDataAccessObject implements TemplateStepDAO {
    /*
     * JDBC SQL and DML statements.
     */
     

    /** Get human task steps. */
    private static final String GET_HUMAN_TASK_STEPS
    = " SELECT PTS.ID,"
    +        " PTS.STEP_NAME_A,"
    +        " PTS.STEP_NAME_E,"
    +        " PTS.SEQ_NO"
    + " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS"
    + " WHERE PTS.STEP_TYPE = " + TemplateStepVO.TYPE_HUMAN_TASK
    +   " AND PTS.PTL_ID = ?" // Template ID
    + " ORDER BY PTS.SEQ_NO";

    /** Update template step */
    private static final String UPDATE_STEP
        =   " UPDATE TF_EPS_PROCEDURE_TEMP_STEPS"
        +      " SET STEP_NAME_A = ?,"  
		+          " STEP_TYPE = ?,"
		+          " HAS_NTF_REQUESTER = ?,"  
		+          " HAS_NTF_STEP_USER = ?,"  
		+          " SEQ_NO = ?,"
		+          " UPDATED_BY = ?,"
		+          " TASK_ASSIGNMENT_TYPE = ?,"
		+          " UPDATE_DATE = SYSDATE,"
        +          " VIEW_TYPE = ?,"
        +          " VIEW_LINK = ?"
        +    " WHERE ID = ?";
    
    /** Update template step */
    private static final String UPDATE_BUSINESS_ACTION
        =   " UPDATE TF_EPS_PROCEDURE_TEMP_STEPS"
        +      " SET STEP_NAME_A = ?,"  
		+          " SEQ_NO = ?,"
		+          " UPDATED_BY = ?,"
		+          " UPDATE_DATE = SYSDATE,"
        +          " ACTION_CLASS = ? " 
        +    " WHERE ID = ?";
        
    /** Update template step */
    private static final String UPDATE_BEGIN_STEP
        =   " UPDATE TF_EPS_PROCEDURE_TEMP_STEPS"
        +      " SET STEP_NAME_A = ?,"  
		+          " STEP_TYPE = ?,"
		+          " UPDATED_BY = ?,"
		+          " UPDATE_DATE = SYSDATE,"
        +          " MIN_ATTACHMENT_COUNT = ?," 
        +          " MAX_ATTACHMENT_COUNT = ?," 
        +          " VIEW_TYPE = ?," 
        +          " VIEW_LINK = ?" 
        +    " WHERE ID = ?";
        
    /** Update ATTACHMENT SETTINGS */
    private static final String UPDATE_ATTACHMENT_ACTIONS
        =   " UPDATE TF_EPS_PROCEDURE_TEMP_STEPS"
        +      " SET MIN_ATTACHMENT_COUNT = ?,"
		+          " MAX_ATTACHMENT_COUNT = ?"
        +    " WHERE ID = ? ";

    /** Find Step Users */    
    private static final String FIND_STEP_USERS
        =   " SELECT DISTINCT " 
		+          " USR.NAME USER_NAME,"
		+          " EMP.NAME_A EMPLOYEE_NAME,"
        +          " EMP.EMPLOYEE_NO" 
        +     " FROM SF_INF_USERS USR,"
		+          " SF_INF_GROUPS GRP,"
		+          " SF_INF_USER_GROUPS USR_GRP,"
		+          " TF_EPS_TEMP_STEP_GROUPS TSG,"
		+          " TF_STP_EMPLOYEES EMP,"
		+          " TF_STP_EMP_VS_USERS EMP_USR"
        +    " WHERE TSG.GRP_ID = GRP.ID"
        +      " AND GRP.ID = USR_GRP.GRP_ID"
        +      " AND USR_GRP.USR_ID = USR.ID"
        +      " AND USR.ID = EMP_USR.USR_USR_ID"
        +      " AND EMP_USR.EMP_ID = EMP.ID"
        +      " AND TSG.PTS_ID = ? ";
        
    /** Delete Step */
    private static final String DELETE_STEP
        =   " DELETE FROM TF_EPS_PROCEDURE_TEMP_STEPS WHERE ID = ? ";

    /** Delete Step Actions */
    private static final String DELETE_STEP_ACTIONS
        =   " DELETE FROM TF_EPS_TEMP_STEP_ACTIONS WHERE PTS_ID = ? ";
        
    /** Delete Step Forwards */
    private static final String DELETE_STEP_FORWARDS
        =   " DELETE FROM TF_EPS_TEMP_STEP_FORWARDS WHERE PTS_ID = ? ";
        
    /** Delete Step Notifications */
    private static final String DELETE_STEP_NOTIFICATIONS
        =   " DELETE FROM TF_EPS_TEMP_STEP_NOTIFICATIONS WHERE PTS_ID = ? ";
        
    /** Delete Step Groups */
    private static final String DELETE_STEP_USERS
        =   " DELETE FROM TF_EPS_TEMP_STEP_USERS WHERE PTS_ID = ? ";    
        
    /** Delete Step Groups */
    private static final String DELETE_STEP_FIELDS
        =   " DELETE FROM TF_EPS_TEMP_STEP_FIELDS WHERE PTS_ID = ? ";    
        
    /** Has users  */
    private static final String HAS_USERS_QUERY    
        =   " SELECT COUNT(*) COUNT"
        +     " FROM TF_EPS_TEMP_STEP_USERS TSU, "
        +     "      TF_EPS_PROCEDURE_TEMP_STEPS PTS, "
        +     "      TF_EPS_PROCEDURE_TEMPLATES PTL "
        +    " WHERE TSU.PTS_ID = PTS.ID "
        +      " AND PTS.PTL_ID = PTL.ID "
        +      " AND PTS.PTL_ID = ? "        
        +      " AND PTS.SEQ_NO = ? ";        
    
    /** Has Steps */
    private static final String HAS_STEPS_QUERY    
        =   " SELECT COUNT(*) "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS "
        +    " WHERE PTL_ID = ? "
        +      " AND STEP_TYPE <> 4 "
        +      " AND STEP_TYPE <> 5 ";     
    
    private static final String IS_NEXT_STEP
        =   " SELECT 1 FROM TF_EPS_TEMP_STEP_FORWARDS WHERE PTS_ID_NEXT_STEP = ?";
        
    /** Has Invalid Attachments */
    private static final String HAS_INVALID_ATTACHMENTS_QUERY    
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTS.PTL_ID = ? "
        +      " AND STEP_TYPE <> 1 "
        +      " AND EXISTS ( SELECT 1 FROM TF_EPS_TEMP_STEP_ACTIONS "
        +                            " WHERE PTS_ID = PTS.ID) ";
                
    /** Has Invalid Attachments */
    private static final String IS_INVALID_BUSINESS_ACTION_STEP
        =   " SELECT STEP_NAME_A "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS "
        +    " WHERE PTL_ID = ? "
        +      " AND STEP_TYPE = 3 "
        +      " AND ACTION_CLASS IS NULL";
        
    /** Add Log Query */
    private static final String ADD_LOG_QUERY
        =    "INSERT INTO TF_EPS_PROCEDURE_TEMP_LOGS "
        +    "(ID,STATUS,VERSION_NO,NAME_E,ACTION_TYPE,PRIORITY,NAME_A, "
        +    "STATUS_DATE,PTL_ID,EMPLOYEE_NAME,CREATED_BY,CREATION_DATE) "
        +    "SELECT PTG_SEQ.NEXTVAL, "
        +          " PTL.STATUS,"
        +          " PTL.VERSION_NO,"
        +          " PTL.NAME_E,"
        +          " ?,"
        +          " PTL.PRIORITY,"
        +          " PTL.NAME_A,"
        +          " SYSDATE,"
        +          " PTL_ID,"
        +          " F_DB_GET_EMP_NAME_A(?),"
        +          " ?,"
        +          " SYSDATE"
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS,"
        +          " TF_EPS_PROCEDURE_TEMPLATES PTL"
        +    " WHERE PTS.PTL_ID = PTL.ID"    
        +      " AND PTS.ID = ? ";

    /** Get template start step. */
    private static final String GET_START_STEP_BY_TEMPLATE_ID
    = " SELECT PTS.ID,"
    +        " PTS.STEP_NAME_A,"
    +        " PTS.STEP_NAME_E,"
    +        " PTS.SEQ_NO,"
    +        " PTS.MAX_ATTACHMENT_COUNT,"
    +        " PTS.MIN_ATTACHMENT_COUNT"
    + " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS"
    + " WHERE PTS.STEP_TYPE = 4"
    +   " AND PTS.PTL_ID = ?"; // Template ID

    /**  */
    private static final String GET_ATTACHMENT_SETTING
        =   " SELECT MIN_ATTACHMENT_COUNT,"
		+          " MAX_ATTACHMENT_COUNT"
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS"
        +    " WHERE ID = ? ";

    private static final String IS_VALID_STEP_NAME
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS "
        +    " WHERE STEP_NAME_A = ? "
        +      " AND PTL_ID = ? ";
        
    
    /*
     * Methods
     */
    /**
     * Create new procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public TemplateStepVO create(TemplateStepVO vo) {
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateStep.create");
        PreparedStatement stm = null;
        try {
            // Generate new sequence
            Long newId = generateSequence("PTS_SEQ");
            
            namedQuery.setParameter("id", newId);
            namedQuery.setParameter("stepNameEnglish", vo.getNameAr());
            namedQuery.setParameter("stepNameArabic", vo.getNameEn());
            namedQuery.setParameter("stepType", vo.getStepType());
            namedQuery.setParameter("hasRequesterNotification", vo.getHasRequesterNotification());
            namedQuery.setParameter("hasStepUsersNotification", vo.getHasStepUsersNotification());
            namedQuery.setParameter("templateId", vo.getTemplate().getId());
            namedQuery.setParameter("createdBy", vo.getCreatedBy());
            namedQuery.setParameter("sequenceNo", vo.getSequenceNo());
//            namedQuery.setParameter("notifyRequester", vo.getNotifyRequester());
            namedQuery.setParameter("actionClass", vo.getActionClass());
            namedQuery.setParameter("taskAssignmentType", vo.getTaskAssignmentType());
            namedQuery.setParameter("viewType", vo.getViewType());
            namedQuery.setParameter("viewLink", vo.getViewLink());
            
            debugQuery(namedQuery.getSqlQuery(), namedQuery.getSqlParameters());

            // Execute DML statement
            stm = prepareStatement(namedQuery);
            stm.executeUpdate();
            
            // Save new ID 
            vo.setId(newId);
            
            //add Log 
            addLog(vo, ProcedureTemplateLogVO.ACTION_ADD_STEP);            
            
            // return updated value object
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Create new procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public TemplateStepVO createSystemStep(TemplateStepVO vo) {
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateStep.createSystemStep");
        PreparedStatement stm = null;
        try {
            // Generate new sequence
            Long newId = generateSequence("PTS_SEQ");
            namedQuery.setParameter("id", newId);
            namedQuery.setParameter("stepType", vo.getStepType());
            namedQuery.setParameter("templateId", vo.getTemplate().getId());
            namedQuery.setParameter("createdBy", vo.getTemplate().getCreatedBy());
            namedQuery.setParameter("sequenceNo", vo.getSequenceNo());
            
            debugQuery(namedQuery.getSqlQuery(), namedQuery.getSqlParameters());

            // Execute DML statement
            stm = prepareStatement(namedQuery);
            stm.executeUpdate();
            
            // Save new ID and return updated value object
            vo.setId(newId);
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }    
    
    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateStep(TemplateStepVO vo){
        PreparedStatement stm = null;
        try {
            
            // Debug query
            List params = new ArrayList();
            params.add(vo.getNameAr());
            params.add(vo.getStepType());
            params.add(vo.getHasRequesterNotification());
            params.add(vo.getHasStepUsersNotification());
            params.add(vo.getSequenceNo());
            params.add(vo.getUpdatedBy());
            params.add(vo.getTaskAssignmentType());
            params.add(vo.getViewType());
            params.add(vo.getViewLink());
            params.add(vo.getId());
            debugQuery(UPDATE_STEP, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_STEP);
            setQueryParameters(stm, params);
            stm.executeUpdate();

            //add Log 
            addLog(vo, ProcedureTemplateLogVO.ACTION_UPDATE_STEP);  

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Update procedure template Business Action step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateBusinessStep(TemplateStepVO vo){
        
                PreparedStatement stm = null;
        try {
            
            // Debug query
            List params = new ArrayList();
            params.add(vo.getNameAr());

            params.add(vo.getSequenceNo());
            params.add(vo.getUpdatedBy());
            params.add(vo.getActionClass());
            params.add(vo.getId());
            debugQuery(UPDATE_BUSINESS_ACTION, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_BUSINESS_ACTION);
            setQueryParameters(stm, params);
            stm.executeUpdate();

            //add Log 
            addLog(vo, ProcedureTemplateLogVO.ACTION_UPDATE_STEP);  

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }      
    }
    
    
    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateNtfStep(TemplateStepVO vo){
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateStep.updateTemplateNtfStep");
        PreparedStatement stm = null;
        try {
            // Debug query
            namedQuery.setParameter("nameAr", vo.getNameAr());
            namedQuery.setParameter("stepType", vo.getStepType());
            namedQuery.setParameter("sequenceNo", vo.getSequenceNo());
            namedQuery.setParameter("hasRequesterNotification", vo.getHasRequesterNotification());
            namedQuery.setParameter("updatedBy", vo.getUpdatedBy());
            namedQuery.setParameter("id", vo.getId());
            
            debugQuery(namedQuery.getSqlQuery(), namedQuery.getSqlParameters());

            // Execute DML statement
            stm = prepareStatement(namedQuery);
            stm.executeUpdate();

            //add Log 
            addLog(vo, ProcedureTemplateLogVO.ACTION_UPDATE_STEP);  

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    public void updateTemplateBeginStep(TemplateStepVO vo){
        PreparedStatement stm = null;
        try {
            
            // Debug query
            List params = new ArrayList();
            params.add(vo.getNameAr());
            params.add(vo.getStepType());
            params.add(vo.getUpdatedBy());
            params.add(vo.getMinAttachmentsCount());
            params.add(vo.getMaxAttachmentsCount());
            params.add(vo.getViewType()); 
            params.add(vo.getViewLink());
            params.add(vo.getId());
            debugQuery(UPDATE_BEGIN_STEP, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_BEGIN_STEP);
            setQueryParameters(stm, params);
            stm.executeUpdate();

            //add Log 
            addLog(vo, ProcedureTemplateLogVO.ACTION_UPDATE_STEP);  

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }    
    
    /**
     * Find Procedure Step
     * 
     * @return Search Page Value Object
     * @param vo Procedure template step value object
     * @param page Number 
     */
    public SearchPageVO find(int pageNo, Long ptlId){
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateStep.find");
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;
        
        namedQuery.setParameter("templateId", ptlId);
        debugQuery(namedQuery.getSqlQuery(), namedQuery.getSqlParameters());
        
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(namedQuery);
            prepStmt = doSearch(namedQuery, pageNo, pageSize);
            resultSet = prepStmt.executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()) {
                TemplateStepVO vo = new TemplateStepVO();
                vo.setId(getLong(resultSet,"ID"));
                vo.setNameAr(getString(resultSet,"STEP_NAME_A"));
                vo.setStepType(getInteger(resultSet,"STEP_TYPE"));
                vo.setStepTypeDesc(getString(resultSet,"STEP_TYPE_DESC"));
                vo.setHasRequesterNotification(getInteger(resultSet,"HAS_NTF_REQUESTER"));
                vo.setHasStepUsersNotification(getInteger(resultSet,"HAS_NTF_STEP_USER"));
                vo.setSequenceNo(getInteger(resultSet,"SEQ_NO"));
//                vo.setNotifyRequester(getInteger(resultSet,"NOTIFY_REQUESTER"));
        
                searchPage.addRecord(vo);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
        
    /**
     * Get Step By Id
     * 
     * @return Template Step Value Object
     * @param stepId Template Step Id
     */
    public TemplateStepVO getById(Long stepId) {
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateStep.getById");
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;
        
        namedQuery.setParameter("stepId", stepId);
        debugQuery(namedQuery.getSqlQuery(), namedQuery.getSqlParameters());
        
        try{
            prepStmt = prepareStatement(namedQuery);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return null ;
            }
            
            TemplateStepVO vo = new TemplateStepVO();
            vo.setId(getLong(resultSet,"ID"));
            vo.setNameAr(getString(resultSet,"STEP_NAME_A"));
            vo.setHasRequesterNotification(getInteger(resultSet,"HAS_NTF_REQUESTER"));
            vo.setHasStepUsersNotification(getInteger(resultSet,"HAS_NTF_STEP_USER"));
            vo.setStepType(getInteger(resultSet,"STEP_TYPE"));
            vo.setStepTypeDesc(getString(resultSet,"STEP_TYPE_DESC"));
            vo.setSequenceNo(getInteger(resultSet,"SEQ_NO"));
//            vo.setNotifyRequester(getInteger(resultSet,"NOTIFY_REQUESTER"));
            vo.setActionClass(getString(resultSet, "ACTION_CLASS"));
            vo.setTaskAssignmentType(getInteger(resultSet, "TASK_ASSIGNMENT_TYPE"));
            vo.setMinAttachmentsCount(getInteger(resultSet,"MIN_ATTACHMENT_COUNT"));
            vo.setMaxAttachmentsCount(getInteger(resultSet,"MAX_ATTACHMENT_COUNT"));
            vo.setViewLink(getString(resultSet,"VIEW_LINK"));
            vo.setViewType(getInteger(resultSet,"VIEW_TYPE"));
            
            ProcedureTemplateVO template = new ProcedureTemplateVO();
            template.setId(getLong(resultSet,"PTL_ID"));
            template.setStatus(getInteger(resultSet,"PTL_STATUS"));
            vo.setTemplate(template);
            return vo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    

    /**
     * Find Security Groups
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findStepUsers(int pageNo,Long stepId,UserVO vo){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        StringBuffer sqlQuery = new StringBuffer(FIND_STEP_USERS);
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(stepId);
        
        if(vo.getName() != null ){
            sqlQuery.append(" AND USR.NAME like ? ");
            listOfparams.add("%"+vo.getName()+"%");
        }
        
        if(vo.getEmployee().getEmployeeNameAr() != null ){
            sqlQuery.append(" AND EMP.NAME_A like ? ");
            listOfparams.add("%"+vo.getEmployee().getEmployeeNameAr()+"%");
        }
       
        if(vo.getEmployee().getEmployeeNo() != null ){
            sqlQuery.append(" AND EMP.EMPLOYEE_NO = ? ");
            listOfparams.add(vo.getEmployee().getEmployeeNo());
        }            
        
        debugQuery(sqlQuery, listOfparams);
        
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString() , 
                                                            listOfparams);
            prepStmt = doSearch(sqlQuery.toString(), listOfparams, pageNo , 
                                                                pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()){             
                
                EmployeeVO employee = new EmployeeVO();
                employee.setEmployeeNameAr(getString(resultSet,"EMPLOYEE_NAME"));
                employee.setEmployeeNo(getString(resultSet,"EMPLOYEE_NO"));
                
                UserVO userVO = new UserVO();
                userVO.setName(getString(resultSet,"USER_NAME"));
                userVO.setEmployee(employee);
                
                searchPage.addRecord(userVO);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }     
        
    /**
     * Delete procedure template step.
     * 
     * @param vo Procedure Template Value Object
     */
    public void delete(TemplateStepVO vo){
        
        //add Log 
        addLog(vo, ProcedureTemplateLogVO.ACTION_DELETE_STEP);
        
        // Delete actions,forwards,notifications and groups related to selected step
        deleteStepActions(vo.getId());
        deleteStepForwards(vo.getId());
        deleteStepNotifications(vo.getId());
        deleteStepUsers(vo.getId());
        deleteStepFields(vo.getId());
        
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(vo.getId());

            debugQuery(DELETE_STEP, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP);
            setQueryParameters(stm, params);
            stm.executeUpdate();


        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Delete procedure template step actions .
     * 
     * @param vo Procedure Template Value Object
     */
    private void deleteStepActions(Long stepId){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(stepId);

            debugQuery(DELETE_STEP_ACTIONS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_ACTIONS);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }    
    
    /**
     * Delete procedure template step forwards.
     * 
     * @param vo Procedure Template Value Object
     */
    private void deleteStepForwards(Long stepId){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(stepId);

            debugQuery(DELETE_STEP_FORWARDS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_FORWARDS);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }      
    
    
    /**
     * Delete procedure template step forwards.
     * 
     * @param vo Procedure Template Value Object
     */
    private void deleteStepNotifications(Long stepId){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(stepId);

            debugQuery(DELETE_STEP_NOTIFICATIONS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_NOTIFICATIONS);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }   
    
    /**
     * Delete procedure template step forwards.
     * 
     * @param vo Procedure Template Value Object
     */
    private void deleteStepUsers(Long stepId){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(stepId);

            debugQuery(DELETE_STEP_USERS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_USERS);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }     
    
    /**
     * Delete procedure template step fields.
     * 
     * @param vo Procedure Template Value Object
     */
    private void deleteStepFields(Long stepId){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(stepId);

            debugQuery(DELETE_STEP_FIELDS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_FIELDS);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }         
    
    /**
     * add Procedure Template Log
     * 
     * @param Procedure Template Log Value Object
     */
    private void addLog(TemplateStepVO vo, Integer actionType) {
        // Debug message
        List params = new ArrayList();
        
        params.add(actionType);
        params.add(vo.getCreatedBy());
        params.add(vo.getCreatedBy());
        params.add(vo.getId());
        
        debugQuery(ADD_LOG_QUERY, params);
        
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(ADD_LOG_QUERY);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    
    /**
     * has Steps
     * return TRUE if the template has defined steps except begin and end
     *                                                  otherwise reyurn FALSE                                                
     * @return boolean (true/false)
     * @param templateId
     */
    public boolean hasSteps(Long templateId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(templateId);
        debugQuery(HAS_STEPS_QUERY, listOfparams);
        
        try{
            prepStmt = getConnection().prepareStatement(HAS_STEPS_QUERY);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
            
            return resultSet.next();
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }       
    }    
    
    /**
     * Is Next Step
     *  - EPS_PTT_015
     *  - Cannot delete step because its related to another steps
     * return TRUE if the Step is next step for another step
     * 
     * @return boolean (true/false)
     * @param stepId
     */
    public boolean isNextStep(Long stepId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(stepId);
        debugQuery(IS_NEXT_STEP, listOfparams);
        
        try{
            prepStmt = getConnection().prepareStatement(IS_NEXT_STEP);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
            
            return resultSet.next();
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Has Invalid Attachments
     * The step can't have attachment if the step type is not human action
     *  - EPS_PTT_004
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public boolean hasInvalidAttachments(Long templateId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(templateId);
        debugQuery(HAS_INVALID_ATTACHMENTS_QUERY, listOfparams);
        int count = 0;
        
        try{
            prepStmt = getConnection().prepareStatement(HAS_INVALID_ATTACHMENTS_QUERY);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
                count = getInteger(resultSet,"COUNT").intValue();
            }
            
            return count != 0;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    
    /**
     * Is Invalid Business Action Step
     * the file name must be added for business action step
     *  - EPS_PTT_016
     *   
     * @return boolean (true/false)
     * @param templateId Template Id
     */
    public List getInvalidBusinessActionStep(Long templateId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(templateId);
        debugQuery(IS_INVALID_BUSINESS_ACTION_STEP, listOfparams);
        List description = new ArrayList();
        
        try{
            prepStmt = getConnection().prepareStatement(IS_INVALID_BUSINESS_ACTION_STEP);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
            
            while(resultSet.next()){
                description.add(getString(resultSet,"STEP_NAME_A")) ;
            }
            
            return description;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Get template start step.
     * 
     * @param templateId Template ID.
     * @return start step for this template.
     */
    public TemplateStepVO getTemplateStartStep(Long templateId) {
        ArrayList params = new ArrayList();
        params.add(templateId);
        debugQuery(GET_START_STEP_BY_TEMPLATE_ID, params);
        
        PreparedStatement stm = null;
        ResultSet rs = null;    
        try{
            stm = getConnection().prepareStatement(GET_START_STEP_BY_TEMPLATE_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if (! rs.next()) {
                return null;
            }
            
            TemplateStepVO vo = new TemplateStepVO();
            vo.setId(getLong(rs, "ID"));
            vo.setNameAr(getString(rs, "STEP_NAME_A"));
            vo.setNameEn(getString(rs, "STEP_NAME_E"));
            vo.setSequenceNo(getInteger(rs, "SEQ_NO"));
            vo.setMaxAttachmentsCount(getInteger(rs, "MAX_ATTACHMENT_COUNT"));
            vo.setMinAttachmentsCount(getInteger(rs, "MIN_ATTACHMENT_COUNT"));
  
            return vo;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }
    
    /**
     * Get Attachment settings.
     * 
     * @param stepId Step Id.
     * @return attachment settings for this step
     */
    public TemplateStepVO getAttachmentActions(Long stepId){
        ArrayList params = new ArrayList();
        params.add(stepId);
        debugQuery(GET_ATTACHMENT_SETTING, params);
        
        PreparedStatement stm = null;
        ResultSet rs = null;    
        try{
            stm = getConnection().prepareStatement(GET_ATTACHMENT_SETTING);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if (! rs.next()) {
                return null;
            }
            
            TemplateStepVO vo = new TemplateStepVO();
            vo.setId(stepId);
            vo.setMinAttachmentsCount(getInteger(rs, "MIN_ATTACHMENT_COUNT"));
            vo.setMaxAttachmentsCount(getInteger(rs, "MAX_ATTACHMENT_COUNT"));
  
            return vo;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }

    /**
     * Update Attachment Settings
     * 
     * @param stepId Step Id
     */
    public void updateAttachmentActions(TemplateStepVO vo){
        PreparedStatement stm = null;
        try {
            
            // Debug query
            List params = new ArrayList();
            params.add(vo.getMinAttachmentsCount());
            params.add(vo.getMaxAttachmentsCount());
            params.add(vo.getId());
            debugQuery(UPDATE_ATTACHMENT_ACTIONS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_ATTACHMENT_ACTIONS);
            setQueryParameters(stm, params);
            stm.executeUpdate();


        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * Is Valid Step Name
     * the step name must uniqe
     *  - EPS_PTT_019
     *   
     * @return boolean (true/false)
     * @param stepName step Name
     */
    public boolean isValidStepName(String stepName,Long stepId,Long templateId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        StringBuffer sqlQuery = new StringBuffer(IS_VALID_STEP_NAME);
        
        List params = new ArrayList();
        params.add(stepName.trim());
        params.add(templateId);
        
        if(stepId != null){
            sqlQuery.append(" AND ID <> ? ");
            params.add(stepId);    
        }
        
        debugQuery(sqlQuery.toString(), params);
        int count = 0;
        
        try{
            prepStmt = getConnection().prepareStatement(sqlQuery.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
                count = getInteger(resultSet,"COUNT").intValue();
            }
            
            return count == 0;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }
    
    /**
     * Check if template step Has user
     *
     * @return true if template step has users
     * @param stepSeqNo step sequence number
     * @param templateId template ID
     */
    public boolean hasUsers(Long templateId, Integer stepSeqNo){
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(templateId);
        listOfparams.add(stepSeqNo);
        debugQuery(HAS_USERS_QUERY, listOfparams);

        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(HAS_USERS_QUERY);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
            
            if (resultSet.next()) {
                return false;
            }
            
            return resultSet.getInt("COUNT") > 0;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }       
    }
    
    /**
     * Get human task steps.
     * 
     * @param templateId Template ID.
     * @return human task steps.
     */
    public List getHumanTaskSteps(Long templateId) {
        ArrayList params = new ArrayList();
        params.add(templateId);
        debugQuery(GET_HUMAN_TASK_STEPS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;    
        try {
            stm = getConnection().prepareStatement(GET_HUMAN_TASK_STEPS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List stepsList = new ArrayList();
            while (rs.next()) {
                TemplateStepVO vo = new TemplateStepVO();
                stepsList.add(vo);

                vo.setId(getLong(rs, "ID"));
                vo.setSequenceNo(getInteger(rs, "SEQ_NO"));
                vo.setNameAr(rs.getString("STEP_NAME_A"));
                vo.setNameEn(rs.getString("STEP_NAME_E"));
            }
            
            return stepsList;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }       
    }
    
    /**
     * Check if step has field groups
     * 
     * @return true if step has field groups
     * @param procedureTemplateStepId template step ID
     */
    public boolean hasFieldGroups(Long templateStepId){
        
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.TemplateStep.hasFieldGroups");
        query.setParameter("templateStepId", templateStepId);

        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;  
        
        try{
            prepStmt = prepareStatement(query);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
              return true;  
            }
            
            return false;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }       
    } 
}