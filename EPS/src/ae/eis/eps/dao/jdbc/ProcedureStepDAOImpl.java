/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  12/05/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.ProcedureStepDAO;
import ae.eis.eps.vo.ProcedureStepActionVO;
import ae.eis.eps.vo.ProcedureStepForwardVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.util.dao.jdbc.NamedQuery;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Procedure step data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepDAOImpl extends JdbcDataAccessObject  implements ProcedureStepDAO {
    /*
     * JDBC SQL and DML statements.
     */
    
    /** Get active procedure step. */
    private static final String GET_ACTIVE_STEP
    = "SELECT PRE.ID AS STEP_ID,"
    +       " PRE.HAS_NTF_REQUESTER,"
    +       " PRE.HAS_NTF_STEP_USER,"
    +       " PRE.USR_ID AS CLAIMED_BY_USR_ID,"
    +       " PRE.STEP_TYPE,"
    +       " PRE.SEQ_NO,"
    +       " PRE.ACTION_CLASS,"
    +       " PRE.MAX_ATTACHMENT_COUNT,"
    +       " PRE.MIN_ATTACHMENT_COUNT,"
    +       " PRE.TASK_ASSIGNMENT_TYPE,"
    +       " PRD.USR_ID_ASSIGNED_TO,"
    +       " PRD.LAST_ACTION,"  
    +       " PRE.STEP_NAME_A ,"
    +       " PRD.CODE ,"
    +       " PRD.PTL_ID ,"
    +       " PRD.ACTIVE_STEP_SEQ_NO,"
    +       " PRD.LAST_HT_PRE_SEQ_NO,"
    +       " PRD.USR_ID AS REQUESTER_USR_ID"
    + " FROM TF_EPS_PROCEDURE_STEPS PRE,"
    +      " TF_EPS_PROCEDURES PRD"
    + " WHERE PRE.PRD_ID = PRD.ID"
    +   " AND PRE.SEQ_NO = PRD.ACTIVE_STEP_SEQ_NO"
    +   " AND PRD.STATUS = 1"
    +   " AND PRE.PRD_ID = ?"; // Procedure ID
    
    /** Get allowed procedure step actions. */
    private static final String GET_STEP_ACTIONS
    = " SELECT ID,"
    +        " ACTION_TYPE"
    + " FROM TF_EPS_STEP_ACTIONS"
    + " WHERE PRE_ID = ?";

    /** Get allowed procedure active step actions. */
    private static final String GET_ACTIVE_STEP_ACTIONS
    = " SELECT SAT.ID,"
    +        " SAT.ACTION_TYPE"
    + " FROM TF_EPS_STEP_ACTIONS SAT,"
    +      " TF_EPS_PROCEDURE_STEPS PRE,"
    +      " TF_EPS_PROCEDURES PRD"
    + " WHERE SAT.PRE_ID = PRE.ID"
    +   " AND PRE.PRD_ID = PRD.ID"
    +   " AND PRD.ACTIVE_STEP_SEQ_NO = PRE.SEQ_NO"
    +   " AND PRD.STATUS = DECODE(PRE.SEQ_NO,999,2,1)"
    +   " AND PRD.ID = ?";

    /** Get approve step forwards. */
    private static final String GET_APPROVE_STEP_FORWARDS
    = " SELECT ACTION_TYPE,"
    +        " NEXT_PRE.ID AS NEXT_STEP_ID,"
    +        " NEXT_PRE.STEP_NAME_A AS NEXT_STEP_NAME_A,"
    +        " NEXT_PRE.SEQ_NO AS NEXT_STEP_SEQ_NO"
    + " FROM TF_EPS_STEP_FORWARDS SFD,"
    +      " TF_EPS_PROCEDURE_STEPS NEXT_PRE"
    + " WHERE SFD.PRE_ID_NEXT_STEP = NEXT_PRE.ID"
    +   " AND SFD.ACTION_TYPE = 1"
    +   " AND SFD.PRE_ID = ?";

    /** Get all step forwards. */
    private static final String GET_STEP_FORWARDS
    = " SELECT ACTION_TYPE,"
    +        " NEXT_PRE.ID AS NEXT_STEP_ID,"
    +        " NEXT_PRE.STEP_NAME_A AS NEXT_STEP_NAME_A,"
    +        " NEXT_PRE.SEQ_NO AS NEXT_STEP_SEQ_NO"
    + " FROM TF_EPS_STEP_FORWARDS SFD,"
    +      " TF_EPS_PROCEDURE_STEPS NEXT_PRE"
    + " WHERE SFD.PRE_ID_NEXT_STEP = NEXT_PRE.ID"
    +   " AND SFD.PRE_ID = ?";

    /** Claim procedure step. */
    private static final String CLAIM_STEP
    = " UPDATE TF_EPS_PROCEDURE_STEPS"
    + " SET ASSIGN_DATE = SYSDATE,"
    +     " UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY=(SELECT NAME FROM SF_INF_USERS WHERE ID=?)," // User ID
    +     " USR_ID = ?" // User ID
    + " WHERE PRD_ID = ?" // Procedure ID
    +   " AND (USR_ID IS NULL or USR_ID = ?)"
    +   " AND SEQ_NO = (SELECT PRD.ACTIVE_STEP_SEQ_NO"
    +                 " FROM TF_EPS_PROCEDURES PRD"
    +                 " WHERE PRD.ID  = ?" // Procedure ID
    +                   " AND STATUS = 1"
    +                 ")";

    /** Un-claim active procedure step. */
    private static final String UNCLAIM_ACTIVE_STEP
    = " UPDATE TF_EPS_PROCEDURE_STEPS"
    + " SET ASSIGN_DATE = NULL,"
    +     " UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY=(SELECT NAME FROM SF_INF_USERS WHERE ID=?)," // User ID
    +     " USR_ID = NULL"
    + " WHERE PRD_ID = ?" // Procedure ID
    +   " AND USR_ID = ? " // User ID
    +   " AND SEQ_NO = (SELECT PRD.ACTIVE_STEP_SEQ_NO"
    +                 " FROM TF_EPS_PROCEDURES PRD"
    +                 " WHERE PRD.ID  = ?" // Procedure ID
    +                 " AND STATUS = 1"
    +                 ")"
    + " OR (PRD_ID = ? " // Procedure Id
    + "     AND ? IN (( "
    + "           SELECT     REGEXP_SUBSTR "
    + "                            ((SELECT VALUE "
    + "                                FROM tf_stp_traffic_properties "
    + "                               WHERE property = "
    + "                                        'ae.rta.eps.unclimePrd.adminUsersId'), "
    + "                             '[^,]+', "
    + "                             1, "
    + "                             LEVEL "
    + "                            ) "
    + "                 FROM DUAL "
    + "           CONNECT BY REGEXP_SUBSTR "
    + "                            ((SELECT VALUE "
    + "                                FROM tf_stp_traffic_properties "
    + "                               WHERE property = "
    + "                                        'ae.rta.eps.unclimePrd.adminUsersId'), "
    + "                             '[^,]+', "
    + "                             1, "
    + "                             LEVEL "
    + "                            ) IS NOT NULL)) " // user Id , traffic property value
    + " AND SEQ_NO = (SELECT PRD.ACTIVE_STEP_SEQ_NO "
    + "                 FROM TF_EPS_PROCEDURES PRD "
    + " WHERE PRD.ID = ? AND STATUS = 1)) "; //Procedure Id


    /** Un-claim active procedure step for admin. */
    private static final String UNCLAIM_ACTIVE_STEP_FOR_ADMIN
    = " UPDATE TF_EPS_PROCEDURE_STEPS"
    + " SET ASSIGN_DATE = NULL,"
    +     " UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY=(SELECT NAME FROM SF_INF_USERS WHERE ID=?)," // User ID
    +     " USR_ID = NULL"
    + " WHERE PRD_ID = ?" // Procedure ID
    +   " AND SEQ_NO = (SELECT PRD.ACTIVE_STEP_SEQ_NO"
    +                 " FROM TF_EPS_PROCEDURES PRD"
    +                 " WHERE PRD.ID  = ?" // Procedure ID
    +                 " AND STATUS = 1"
    +                 ")"
    + " OR (PRD_ID = ? " // Procedure Id
    + " AND SEQ_NO = (SELECT PRD.ACTIVE_STEP_SEQ_NO "
    + "                 FROM TF_EPS_PROCEDURES PRD "
    + " WHERE PRD.ID = ? AND STATUS = 1)) "; //Procedure Id
    
    
    /** Un-claim old procedure step. */
    private static final String UNCLAIM_OLD_STEP
    = " UPDATE TF_EPS_PROCEDURE_STEPS"
    + " SET ASSIGN_DATE = NULL,"
    +     " UPDATE_DATE = SYSDATE,"
    +     " UPDATED_BY=(SELECT NAME FROM SF_INF_USERS WHERE ID=?)," // User ID
    +     " USR_ID = NULL"
    + " WHERE PRD_ID = ?" // Procedure ID
    +   " AND USR_ID = ?" // User ID
    +   " AND SEQ_NO = ?"; // Step Sequence number

    /*
     * Methods
     */

    /**
     * Get active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @return active procedure step.
     */
    public ProcedureStepVO getActiveStep(Long procedureId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        debugQuery(GET_ACTIVE_STEP, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_ACTIVE_STEP);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            // Create VO
            ProcedureStepVO vo = new ProcedureStepVO();
            vo.setClaimedBy(new UserVO());
            vo.setProcedure(new ProcedureVO());
            vo.getProcedure().setId(procedureId);
            vo.getProcedure().setStatus(ProcedureVO.STATUS_UNDER_PROCESSING);
            vo.getProcedure().setLastHumanTaskStep(new ProcedureStepVO());
            vo.getProcedure().setRequester(new UserVO());
            vo.getProcedure().setAssignedTo(new UserVO());

            vo.setId(getLong(rs, "STEP_ID"));
            vo.setHasRequesterNotification(getInteger(rs, "HAS_NTF_REQUESTER"));
            vo.setHasStepUsersNotification(getInteger(rs, "HAS_NTF_STEP_USER"));
            vo.setStepType(getInteger(rs, "STEP_TYPE"));
            vo.setSequenceNo(getInteger(rs, "SEQ_NO"));
            vo.setTaskAssignmentType(getInteger(rs, "TASK_ASSIGNMENT_TYPE"));
            vo.setActionClass(rs.getString("ACTION_CLASS"));
            vo.setMaxAttachmentsCount(getInteger(rs, "MAX_ATTACHMENT_COUNT"));
            vo.setMinAttachmentsCount(getInteger(rs, "MIN_ATTACHMENT_COUNT")); 
            vo.setNameAr(rs.getString("STEP_NAME_A"));
            
            vo.getProcedure().setLastAction(getInteger(rs, "LAST_ACTION"));
            vo.getProcedure().getLastHumanTaskStep().setSequenceNo(
                getInteger(rs, "LAST_HT_PRE_SEQ_NO"));
            vo.getProcedure().getRequester().setId(getLong(rs, "REQUESTER_USR_ID"));
            vo.getProcedure().getAssignedTo().setId(getLong(rs, "USR_ID_ASSIGNED_TO")); 
            vo.getProcedure().setCode(getInteger(rs, "CODE"));
            
            ProcedureTemplateVO template = new ProcedureTemplateVO();
            template.setId(getLong(rs, "PTL_ID"));
            vo.getProcedure().setTemplate(template);
            ProcedureStepVO procedureStepVO = new ProcedureStepVO();
            procedureStepVO.setId(getLong(rs, "ACTIVE_STEP_SEQ_NO"));
            vo.getProcedure().setActiveStep(procedureStepVO);

            vo.getClaimedBy().setId(getLong(rs, "CLAIMED_BY_USR_ID"));

            // Return VO
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get allowed step actions.
     * 
     * @param stepId Step ID.
     * @return allowed step actions.
     */
    public List getStepActions(Long stepId) {
        // Debug query
        List params = new ArrayList();
        params.add(stepId);
        debugQuery(GET_STEP_ACTIONS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_STEP_ACTIONS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List actionsList = new ArrayList();
            while (rs.next()) {
                ProcedureStepActionVO vo = new ProcedureStepActionVO();
                actionsList.add(vo);
                
                vo.setId(getLong(rs, "ID"));
                vo.setActionType(getInteger(rs, "ACTION_TYPE"));
            }

            // Return actions list
            return actionsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get allowed step actions for active procedure step.
     * 
     * @param procedureId Step ID.
     * @return allowed step actions.
     */
    public List getActiveStepActions(Long procedureId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        debugQuery(GET_ACTIVE_STEP_ACTIONS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_ACTIVE_STEP_ACTIONS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List actionsList = new ArrayList();
            while (rs.next()) {
                ProcedureStepActionVO vo = new ProcedureStepActionVO();
                actionsList.add(vo);
                
                vo.setId(getLong(rs, "ID"));
                vo.setActionType(getInteger(rs, "ACTION_TYPE"));
            }

            // Return actions list
            return actionsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get step forwards.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    public List getStepForwards(Long stepId) {
        // Debug query
        List params = new ArrayList();
        params.add(stepId);
        debugQuery(GET_STEP_FORWARDS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_STEP_FORWARDS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List forwardsList = new ArrayList();
            while (rs.next()) {
                ProcedureStepForwardVO vo = new ProcedureStepForwardVO();
                vo.setNextStep(new ProcedureStepVO());
                forwardsList.add(vo);
                
                vo.setActionType(getInteger(rs, "ACTION_TYPE"));
                vo.getNextStep().setId(getLong(rs, "NEXT_STEP_ID"));
                vo.getNextStep().setNameAr(getString(rs, "NEXT_STEP_NAME_A"));
                vo.getNextStep().setSequenceNo(getInteger(rs, "NEXT_STEP_SEQ_NO"));
            }

            // Return actions list
            return forwardsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get approve step forwards.
     * 
     * @param stepId Step ID.
     * @return step forwards list.
     */
    public List getApproveStepForwards(Long stepId) {
        // Debug query
        List params = new ArrayList();
        params.add(stepId);
        debugQuery(GET_APPROVE_STEP_FORWARDS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_APPROVE_STEP_FORWARDS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List forwardsList = new ArrayList();
            while (rs.next()) {
                ProcedureStepForwardVO vo = new ProcedureStepForwardVO();
                vo.setNextStep(new ProcedureStepVO());
                forwardsList.add(vo);
                
                vo.setActionType(getInteger(rs, "ACTION_TYPE"));
                vo.getNextStep().setId(getLong(rs, "NEXT_STEP_ID"));
                vo.getNextStep().setNameAr(getString(rs, "NEXT_STEP_NAME_A"));
                vo.getNextStep().setSequenceNo(getInteger(rs, "NEXT_STEP_SEQ_NO"));
            }

            // Return actions list
            return forwardsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * Claim procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.   
     */
    public boolean claimStep(Long procedureId, Long userId) {
        
        PreparedStatement stm = null;
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.claimStep");
            query.setParameter("procedureId",procedureId);
            query.setParameter("userId",userId);
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            return (count == 1);
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Claim procedure step with no security.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.   
     */
    public boolean claimStepNoSecurity(Long procedureId, Long userId) {
        
        PreparedStatement stm = null;
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.claimStepNoSecurity");
            query.setParameter("procedureId",procedureId);
            query.setParameter("userId",userId);
            // Execute DML statement
            stm = prepareStatement(query);
            int count = stm.executeUpdate();

            return (count == 1);
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Un-claim active procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    public boolean unclaimActiveStep(Long procedureId, Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(userId);
        params.add(procedureId);
        params.add(userId);
        params.add(procedureId);
        params.add(procedureId);
        params.add(userId);
        params.add(procedureId);
        
        debugQuery(UNCLAIM_ACTIVE_STEP, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(UNCLAIM_ACTIVE_STEP);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();

            return (count == 1);

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Un-claim active procedure step for Admin.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    public boolean unclaimActiveForAdmin(Long procedureId, Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(userId);
        params.add(procedureId);
        params.add(procedureId);
        params.add(procedureId);
        params.add(procedureId);
        
        debugQuery(UNCLAIM_ACTIVE_STEP_FOR_ADMIN, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(UNCLAIM_ACTIVE_STEP_FOR_ADMIN);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();

            return (count == 1);

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Un-claim old procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     */
    public boolean unclaimOldStep(Long procedureId, 
                                  Long userId, 
                                  Integer stepSeqNo) {
        // Debug query
        List params = new ArrayList();
        params.add(userId);
        params.add(procedureId);
        params.add(userId);
        params.add(stepSeqNo);
        debugQuery(UNCLAIM_OLD_STEP, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(UNCLAIM_OLD_STEP);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();

            return (count == 1);

        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Get approve step forwards with no security.
     * 
     * @param prdId procedure Id
     * @param activeStepId active StepId
     * @return step forwards list .
     */
    public List getApproveStepForwardsNoSecurity(Long prdId , Long activeStepId) {
        
        // Get Named Query.
        NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.getApproveStepForwardsNoSecurity");

        query.setParameter("procedureId",prdId);
        query.setParameter("procedureStepId",activeStepId);
        
        // Debug Query.
        debugQuery(query);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            // Execute query
            stmt = prepareStatement(query);
            rs = stmt.executeQuery();
            
            List forwardslistNoSecurity = new ArrayList();
            while (rs.next()) {
                ProcedureStepForwardVO vo = new ProcedureStepForwardVO();
                vo.setNextStep(new ProcedureStepVO());
                forwardslistNoSecurity.add(vo);
                vo.getNextStep().setId(getLong(rs, "ID"));
                vo.getNextStep().setNameAr(getString(rs, "STEP_NAME_A"));
                vo.getNextStep().setSequenceNo(getInteger(rs, "SEQ_NO"));
            }
            // Return actions list
            return forwardslistNoSecurity;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stmt);
        }
    }
    
    /**
     * Delete User Steps
     * 
     * @param procedureId
     */
    public void deleteUserSteps(Long procedureId){
        
        PreparedStatement stm = null;
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.deleteUserSteps");
            query.setParameter("procedureId",procedureId);
            
            stm = prepareStatement(query);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
        
    }
    
    /**
     * Add Procedure Steps 
     * 
     * @param procedureId
     * @param userIdAssingTo
     * @param userName
     */
    public void addUserSteps(Long procedureId, Long userIdAssingTo, String userName){
        
        PreparedStatement stm = null;
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.addUserSteps");
            query.setParameter("procedureId",procedureId);
            query.setParameter("userIdAssingTo",userIdAssingTo);
            query.setParameter("userName",userName);
            
            stm = prepareStatement(query);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
        
    }
    
    /**
     * Add User Steps without Assing To User ID
     * 
     * @param procedureId
     * @param userName
     */
    public void addUserStepsNoAssingUsrId(Long procedureId, String userName){
        
        PreparedStatement stm = null;
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.addUserStepsNoAssingUsrId");
            query.setParameter("procedureId",procedureId);
            query.setParameter("userName",userName);
            
            stm = prepareStatement(query);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
        
    }
    
    /**
     * Add Procedure Steps 
     * 
     * @param userIdAssingTo
     * @param userName
     */
    public void addUserSteps(Long userIdAssingTo, String userName){
        
        PreparedStatement stm = null;
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.addUserStepsNoProcedure");
            query.setParameter("userIdAssingTo",userIdAssingTo);
            query.setParameter("userName",userName);
            
            stm = prepareStatement(query);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
        
    }
    
    /**
     * Delete User Steps
     * 
     * @param procedureId
     */
    public void deletePrdUserStep(Long procedureId, Long userId, Long stepId){
        
        PreparedStatement stm = null;
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureStep.deletePrdUserStep");
            query.setParameter("procedureId",procedureId);
            query.setParameter("userId",userId);
            query.setParameter("stepId",stepId);
            
            stm = prepareStatement(query);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
        
    }
    
}