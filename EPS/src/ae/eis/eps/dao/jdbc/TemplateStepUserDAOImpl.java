/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 * 
 * 1.01  Tariq Abu Amireh   17/06/2009  - re-Name File from Step Group 
 *                                                       to Step User
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.TemplateStepUserDAO;
import ae.eis.eps.vo.TemplateStepUserVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Template step security group data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepUserDAOImpl extends JdbcDataAccessObject 
                                      implements TemplateStepUserDAO {
    /*
     * JDBC SQL and DML statements.
     */
    
    /** Create procedure template step forward DML. */
    private static final String CREATE_STEP_GROUP
    = " INSERT INTO TF_EPS_TEMP_STEP_USERS"
    +  "(PTS_ID,USR_ID,CREATED_BY,CREATION_DATE)"
    +  " VALUES(?,"  // PTS_ID
    +         " ?,"  // USR_ID
    +         " ?,"  // CREATED_BY
    +         " SYSDATE)"; // CREATION_DATE
    
    /** Delete Step Groups */
    private static final String DELETE_STEP_GROUPS
        =   " DELETE FROM TF_EPS_TEMP_STEP_USERS WHERE PTS_ID = ? AND USR_ID = ?";
        
    /** Find Security User */
    private static final String FIND_USERS
        =   " SELECT USR.ID USER_ID,"
	    +          " USR.NAME USR_NAME,"
        +          " EMP.ID EMP_ID,"
        +          " EMP.NAME_A EMP_NAME_A"
        +    " FROM SF_INF_USERS USR,"
        +         " TF_STP_EMPLOYEES EMP,"
        +         " TF_STP_EMP_VS_USERS EMP_USR"
        +   " WHERE USR.STATUS = 2"
        +     " AND USR.ID = EMP_USR.USR_USR_ID"
        +     " AND EMP_USR.EMP_ID = EMP.ID"
        +     " AND NOT EXISTS(SELECT 1 FROM TF_EPS_TEMP_STEP_USERS TSU "
        +                             " WHERE TSU.USR_ID = USR.ID"
        +                               " AND TSU.PTS_ID = ?) ";
    
    /** Get Security User */    
    private static final String GET_SECURITY_USERS
        =   " SELECT TSU.USR_ID TSU_USR_ID,"
		+          " USR.NAME USER_NAME,"
		+          " TSU.CREATION_DATE,"
        +          " EMP.NAME_A EMP_NAME_A"
        +     " FROM TF_EPS_TEMP_STEP_USERS TSU,"
		+          " SF_INF_USERS USR,"
        +          " TF_STP_EMPLOYEES EMP,"
        +          " TF_STP_EMP_VS_USERS EMP_USR"
        +    " WHERE TSU.USR_ID = USR.ID"
        +      " AND USR.ID = EMP_USR.USR_USR_ID"
        +      " AND EMP_USR.EMP_ID = EMP.ID"
	   	+      " AND TSU.PTS_ID = ?";
        
    /** Has Invalid Security Users */        
    private static final String HAS_INVALID_SECURITY_USERS
        =   " SELECT PTS.STEP_NAME_A "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTS.PTL_ID = ? "
        +      " AND PTS.STEP_TYPE = 1 "
        +      " AND NOT EXISTS(SELECT 1 FROM TF_EPS_TEMP_STEP_USERS USR "
        +                             " WHERE USR.PTS_ID = PTS.ID ) ";
    
    private static final String GET_INACTIVE_USERS_STEPS
        = " SELECT PTS.STEP_NAME_A "
        +   " FROM TF_EPS_TEMP_STEP_USERS TSU, "
        +        " SF_INF_USERS USR, "
        +        " TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +  " WHERE TSU.PTS_ID = PTS.ID "
        +    " AND TSU.USR_ID = USR.ID "
        +    " AND USR.STATUS = 1 "
        +    " AND PTS.PTL_ID = ? ";
    
    
    /*
     * Methods
     */

    /**
     * Create new procedure template step security group.
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    public TemplateStepUserVO create(TemplateStepUserVO vo) {
        // Debug query
        List params = new ArrayList();
        params.add(vo.getStep().getId());
        params.add(vo.getUser().getId());
        params.add(vo.getCreatedBy());
        debugQuery(CREATE_STEP_GROUP, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_STEP_GROUP);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Delete procedure template step security group.
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    public void delete(TemplateStepUserVO vo){
        // Debug query
        List params = new ArrayList();
        params.add(vo.getStep().getId());
        params.add(vo.getUser().getId());
        debugQuery(DELETE_STEP_GROUPS, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_GROUPS);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Get template step Security Groups
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    public SearchPageVO find(int pageNo,TemplateStepUserVO vo) {
        PreparedStatement stm = null;
        ResultSet rs = null;    
        StringBuffer sqlQuery = new StringBuffer(GET_SECURITY_USERS);
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(vo.getStep().getId());
        debugQuery(GET_SECURITY_USERS.toString(), listOfparams);

        try {
        
        
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString() , 
                                                            listOfparams);
            stm = doSearch(sqlQuery.toString(), listOfparams, pageNo , 
                                                                pageSize);
            rs=stm.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(rs.next()){                
                
                EmployeeVO employee = new EmployeeVO();
                employee.setEmployeeNameAr(getString(rs,"EMP_NAME_A"));
                
                UserVO user = new UserVO();
                user.setId(getLong(rs,"TSU_USR_ID"));
                user.setName(getString(rs,"USER_NAME"));
                user.setCreationDate(rs.getDate("CREATION_DATE"));
                user.setEmployee(employee);
                
                TemplateStepUserVO groupVO = new TemplateStepUserVO();
                groupVO.setUser(user);
                
                searchPage.addRecord(groupVO);
            }
            
            return searchPage;
                                
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }            
    }
    
    /**
     * Get Users Of Step.
     * 
     * @param vo Procedure template step value object.
     * 
     * @return List of security groups value object.
     */
    public List getUsersOfStep(TemplateStepUserVO vo) {
        PreparedStatement stm = null;
        ResultSet rs = null;    
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(vo.getStep().getId());
        debugQuery(GET_SECURITY_USERS.toString(), listOfparams);

        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_SECURITY_USERS);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            List groups = new ArrayList();
            
            while(rs.next()) {                
                
                EmployeeVO employee = new EmployeeVO();
                employee.setEmployeeNameAr(getString(rs,"EMP_NAME_A"));
                
                UserVO user = new UserVO();
                user.setId(getLong(rs,"TSU_USR_ID"));
                user.setName(getString(rs,"USER_NAME"));
                user.setCreationDate(rs.getDate("CREATION_DATE"));
                user.setEmployee(employee);
                
                TemplateStepUserVO groupVO = new TemplateStepUserVO();
                groupVO.setUser(user);
                
                groups.add(groupVO);
            }
            
            return groups;
                                
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }            
    }
    
    /**
     * Find Security Groups
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findUsers(int pageNo,TemplateStepUserVO vo){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        StringBuffer sqlQuery = new StringBuffer(FIND_USERS);
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(vo.getStep().getId());
        
        if(vo.getUser() != null){
            if(vo.getUser().getId() != null ){
                sqlQuery.append(" AND USR.ID = ? ");
                listOfparams.add(vo.getUser().getId());
            }
            
            if(vo.getUser().getName() != null ){
                sqlQuery.append(" AND USR.NAME LIKE ? ");
                listOfparams.add("%"+vo.getUser().getName()+"%");
            }          
            
            if(vo.getUser().getEmployee() != null){
                if(vo.getUser().getEmployee().getId() != null){
                    sqlQuery.append(" AND EMP.ID = ? ");
                    listOfparams.add(vo.getUser().getEmployee().getId());
                }
                
                if(vo.getUser().getEmployee().getEmployeeNameAr() != null){
                   sqlQuery.append(" AND EMP.NAME_A LIKE ? ");
                   listOfparams.add("%"+vo.getUser().getEmployee().getEmployeeNameAr()+"%");
                }
            }
            
        }
        
        debugQuery(sqlQuery, listOfparams);
        
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString() , 
                                                            listOfparams);
            prepStmt = doSearch(sqlQuery.toString(), listOfparams, pageNo , 
                                                                pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()){                
                                
                EmployeeVO employee = new EmployeeVO();
                employee.setId(getLong(resultSet,"EMP_ID"));
                employee.setEmployeeNameAr(getString(resultSet,"EMP_NAME_A"));
                
                UserVO user = new UserVO();
                user.setId(getLong(resultSet,"USER_ID"));
                user.setName(getString(resultSet,"USR_NAME"));
                user.setEmployee(employee);
                
                TemplateStepUserVO stepGroupVO = new TemplateStepUserVO();
                stepGroupVO.setUser(user);
                
                searchPage.addRecord(stepGroupVO);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }
    
    /**
     * Has Invalid Security Groups
     * The step must have granted security group
     * -  EPS_PTT_008
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidSecurityUsersSteps(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(HAS_INVALID_SECURITY_USERS.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List description = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(HAS_INVALID_SECURITY_USERS);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            while(rs.next()){
                description.add(getString(rs,"STEP_NAME_A"));
            }
            
            return description;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }            
    }
    
     /**
     * Has InActive Users
     * Can't activate the procedure because the step has user with status not active
     * - EPS_TSG_002
     * 
     * @return boolean (true/false)
     * @param templateId template Id
     */
    public List getInActiveUsersSteps(Long templateId){
        List params=new ArrayList();
        params.add(templateId);
        debugQuery(GET_INACTIVE_USERS_STEPS.toString(), params);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List description = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_INACTIVE_USERS_STEPS);
            setQueryParameters(stm,params);
            rs = stm.executeQuery();
            
            while(rs.next()){
                description.add(getString(rs,"STEP_NAME_A"));
            }
            
            return description;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
  
}