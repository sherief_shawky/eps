/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.ProcedureAttachmentDAO;
import ae.eis.eps.proc.itp.bus.FieldsCodes;
import ae.eis.eps.vo.ProcedureAttachmentVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.trs.vo.TransactionAttachmentVO;
import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.SearchPageVO;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure attachments data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureAttachmentDAOImpl extends JdbcDataAccessObject  
                                        implements ProcedureAttachmentDAO {
    /*
     * JDBC SQL and DML statements.
     */

    /** Create new attachment record DML. */
    private static final String CREATE_ATTACHMENT
    = " INSERT INTO TF_EPS_ATTACHMENTS"
    + " (ID,ATTACHMENT_NAME,IS_DELETED,MTP_EXTENSION,ATTACHMENT,PRD_ID,USR_ID,"
    +  " PRE_ID, CREATED_BY,UPDATE_DATE,UPDATED_BY,CREATION_DATE)"
    + " VALUES(?," // ID
    +        " ?," // ATTACHMENT_NAME
    +        " 1," // IS_DELETED
    +        " ?," // MTP_EXTENSION
    +        " EMPTY_BLOB(),"
    +        " ?," // PRD_ID
    +        " ?," // USR_ID
    +        " ?," // PRE_ID
    +        " ?," // CREATED_BY
    +        " SYSDATE,"
    +        " ?," // UPDATED_BY
    +        " SYSDATE)";

    /** Get attachment record for update. */
    private static final String GET_ATTACHMENT_FOR_UPDATE
    = "SELECT ATTACHMENT FROM TF_EPS_ATTACHMENTS WHERE ID = ? FOR UPDATE";

    /** Update attachment record BLOB. */
    private static final String UPDATE_ATTACHMENT_BLOB
    = "UPDATE TF_EPS_ATTACHMENTS SET ATTACHMENT = ? WHERE ID = ?";
    
    /** Update attachment record BLOB. */
    private static final String UPDATE_ATTACHMENT_AND_STEP_BLOB
    = "UPDATE TF_EPS_ATTACHMENTS SET ATTACHMENT = ?, PRE_ID=? WHERE ID = ?";

    /** Get attachment record content by ID. */
    private static final String GET_CONTENT_BY_ID
    = " SELECT MTP.MIME_TYPE,"
    +        " ATC.ATTACHMENT"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND ATC.ID = ?";
    
    /** Get attachment record info by ID. */
    private static final String GET_INFO_BY_ID
    = " SELECT ATC.ATTACHMENT_NAME,"
    +        " ATC.CREATION_DATE,"
    +        " MTP.DESCRIPTION_A,"
    +        " EMP.NAME_A EMP_NAME_A"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP,"
    +      " SF_INF_USERS USR,"
    +      " TF_STP_EMP_VS_USERS EVU,"
    +      " TF_STP_EMPLOYEES EMP"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.USR_ID = USR.ID"
    +   " AND EVU.USR_USR_ID = USR.ID"
    +   " AND EVU.EMP_ID = EMP.ID"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND ATC.ID = ?";

    /** Find procedure requester attachment. */
    private static final String FIND_REQUESTER_ATTACHMENTS
    = " SELECT ATC.ID,"
    +        " MTP.DESCRIPTION_A,"
    +        " ATC.ATTACHMENT_NAME,"
    +        " MTP.VIEWABLE_INSIDE_BROWSER,"
    +        " ATC.CREATION_DATE,"
    +        " F_GET_EMP_NAME_A_BY_USR_ID(ATC.USR_ID) AS EMP_NAME"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.PRE_ID IS NULL"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND ATC.PRD_ID = ?"
    + " ORDER BY ID";

    /** Find procedure requester attachment. */
    private static final String FIND_END_STEP_ATTACHMENTS
    = " SELECT ATC.ID,"
    +        " MTP.DESCRIPTION_A,"
    +        " ATC.ATTACHMENT_NAME,"
    +        " MTP.VIEWABLE_INSIDE_BROWSER,"
    +        " ATC.CREATION_DATE,"
    +        " F_GET_EMP_NAME_A_BY_USR_ID(ATC.USR_ID) AS EMP_NAME"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND ATC.PRD_ID = ?"
    + " ORDER BY ID";
    
    /** Find procedure active step attachment. */
    private static final String FIND_ACTIVE_STEP_ATTACHMENTS
    = " SELECT ATC.ID,"
    +        " MTP.DESCRIPTION_A,"
    +        " ATC.ATTACHMENT_NAME,"
    +        " MTP.VIEWABLE_INSIDE_BROWSER,"
    +        " ATC.CREATION_DATE,"
    +        " F_GET_EMP_NAME_A_BY_USR_ID(ATC.USR_ID) AS EMP_NAME"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP,"
    +      " TF_EPS_PROCEDURES PRD"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.PRD_ID = PRD.ID"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND PRD.STATUS = 1"
    +   " AND ATC.PRD_ID = ?"
    + " ORDER BY ID";
    
    /** Find procedure step attachment. */
    private static final String FIND_STEP_ATTACHMENTS
    = " SELECT ATC.ID,"
    +        " MTP.DESCRIPTION_A,"
    +        " ATC.ATTACHMENT_NAME,"
    +        " MTP.VIEWABLE_INSIDE_BROWSER,"
    +        " ATC.CREATION_DATE,"
    +        " F_GET_EMP_NAME_A_BY_USR_ID(ATC.USR_ID) AS EMP_NAME"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP,"
    +      " TF_EPS_PROCEDURES PRD"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.PRD_ID = PRD.ID"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND ATC.PRD_ID = ?"
    + " ORDER BY ID";

    /** Find procedure active step requester attachment. */
    private static final String FIND_ACTIVE_STEP_REQUESTER_ATTACHMENTS
    = " SELECT ATC.ID,"
    +        " MTP.DESCRIPTION_A,"
    +        " ATC.ATTACHMENT_NAME,"
    +        " MTP.VIEWABLE_INSIDE_BROWSER,"
    +        " ATC.CREATION_DATE,"
    +        " F_GET_EMP_NAME_A_BY_USR_ID(ATC.USR_ID) AS EMP_NAME"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP,"
    +      " TF_EPS_PROCEDURES PRD"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.PRD_ID = PRD.ID"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND PRD.STATUS = 1"
    +   " AND ATC.PRD_ID = ?"
    +   " AND (ATC.PRE_ID IS NULL OR ATC.USR_ID = ?)" // User ID
    + " ORDER BY ID";

    /** Find procedure active users attachment. */
    private static final String FIND_ACTIVE_STEP_USERS_ATTACHMENTS
    = " SELECT ATC.ID,"
    +        " MTP.DESCRIPTION_A,"
    +        " ATC.ATTACHMENT_NAME,"
    +        " MTP.VIEWABLE_INSIDE_BROWSER,"
    +        " ATC.CREATION_DATE,"
    +        " F_GET_EMP_NAME_A_BY_USR_ID(ATC.USR_ID) AS EMP_NAME"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_STP_MIME_TYPES MTP,"
    +      " TF_EPS_PROCEDURES PRD"
    + " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND ATC.PRD_ID = PRD.ID"
    +   " AND ATC.IS_DELETED <> 2"
    +   " AND PRD.STATUS = 1"
    +   " AND ATC.PRD_ID = ?"
    +   " AND (ATC.USR_ID = ? OR PRE_ID IS NULL)"
    + " ORDER BY ID";
    
    /** Delete procedure attachment. */
    private static final String DELETE_ATTACHMENT
    = " UPDATE TF_EPS_ATTACHMENTS"
    + " SET IS_DELETED	= 2,"
    +     " UPDATE_DATE	= SYSDATE,"
    +     " UPDATED_BY = (SELECT NAME FROM SF_INF_USERS WHERE ID = ?)" // User ID
    + " WHERE PRD_ID = ?" // Procedure ID
    +   " AND ID = ?"; // Attachment ID
    
    /** Is user allowed to delete attachment query. */
    private static final String IS_ALLOWED_TO_DELETE
    = " SELECT DECODE(ATC.PRE_ID, NULL, 2, 1) IS_REQUESTER_ATT,"
    +        " DECODE(ATC.USR_ID, ?, 2, 1) IS_CURRENT_USER_ATT," // User ID
    +        " NVL((SELECT 2 FROM TF_EPS_STEP_ACTIONS SAT"
    +             " WHERE SAT.PRE_ID = PRE.ID"
    +               " AND SAT.ACTION_TYPE = 7), 1) CAN_DELETE_REQUESTER_ATT,"
    +        " NVL((SELECT 2 FROM TF_EPS_STEP_ACTIONS SAT"
    +             " WHERE SAT.PRE_ID = PRE.ID"
    +               " AND SAT.ACTION_TYPE = 8), 1) CAN_DELETE_OTHER_STEPS_ATT,"
    +        " ATC.PRE_ID AS ATC_PRE_ID,"
    +        " PRE.ID AS ACTIVE_STEP_ID"
    + " FROM TF_EPS_ATTACHMENTS ATC,"
    +      " TF_EPS_PROCEDURES PRD,"
    +      " TF_EPS_PROCEDURE_STEPS PRE"
    + " WHERE ATC.PRD_ID = PRD.ID"
    +   " AND PRE.PRD_ID = PRD.ID"
    +   " AND PRE.SEQ_NO = PRD.ACTIVE_STEP_SEQ_NO"
    +   " AND ATC.IS_DELETED = 1"
    +   " AND PRD.STATUS = 1"
    +   " AND PRD.ID = ?" // Procedure ID
    +   " AND ATC.ID = ?"; // Attachment ID

    /** Get Attachment Count */
    private static final String GET_ATTACHMENT_COUNT
        =   " SELECT ATC.ID "
        +     " FROM TF_EPS_ATTACHMENTS ATC,"
        +          " TF_STP_MIME_TYPES MTP,"
        +          " TF_EPS_PROCEDURES PRD,"
        +          " TF_EPS_PROCEDURE_STEPS PRE"
        +    " WHERE ATC.MTP_EXTENSION = MTP.EXTENSION"
        +      " AND ATC.PRD_ID = PRD.ID"
        +      " AND PRE.PRD_ID = PRD.ID"
        +      " AND PRE.SEQ_NO = PRD.ACTIVE_STEP_SEQ_NO"
        +      " AND ATC.PRE_ID = PRE.ID"
        +      " AND ATC.IS_DELETED <> 2"
        +      " AND PRD.STATUS = 1"
        +      " AND ATC.PRD_ID = ?"
        + " ORDER BY ID";        

    /*
     * Methods
     */

    /**
     * Save attachment.
     * 
     * @param vo Attachment value object.
     */
    public void save(ProcedureAttachmentVO vo) {
        try {
            // Create new attachment record
            createAttachment(vo);

            // Get attachment BLOB to be updated
            Blob attachmentBlob = getAttachmentForUpdate(vo.getId());

            // Set attachment bytes to BLOB
            updateBlob(attachmentBlob, vo.getAttachment());

            // Save BLOB data to database
            updateAttachmentData(vo.getId(), (oracle.sql.BLOB) attachmentBlob);

       } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        }
    }

    /**
     * Create new attachment record.
     * 
     * @param vo Attachment value object.
     * @return new attachments ID.
     */
    private Long createAttachment(ProcedureAttachmentVO vo) {
        PreparedStatement stm = null;
        try {
            // generate new attachment ID
            Long newId = generateSequence("ATC_SEQ");
            vo.setId(newId);

            // Debug query
            List params = new ArrayList();
            params.add(newId);
            params.add(vo.getNameAr());
            params.add(vo.getFileExtension().toString());
            params.add(vo.getProcedure().getId());
            params.add(vo.getUser().getId());

            if (vo.getStep() != null && vo.getStep().getId() != null) {
                params.add(vo.getStep().getId());
            } else {
                params.add(null);
            }

            params.add(vo.getCreatedBy());
            params.add(vo.getCreatedBy());
            debugQuery(CREATE_ATTACHMENT, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_ATTACHMENT);
            setQueryParameters(stm, params);
            stm.executeUpdate();

            // Return new attachments ID.
            return newId;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Get attachment Blob object to be updated.
     * 
     * @param id attachment ID.
     * @return Attachment Blob object.
     */
    private Blob getAttachmentForUpdate(Long id) {
        // Debug query
        List params = new ArrayList();
        params.add(id);
        debugQuery(GET_ATTACHMENT_FOR_UPDATE, params);

        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(GET_ATTACHMENT_FOR_UPDATE);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                throw new DataAccessException(
                        new StringBuffer("Attachment not found: ")
                           .append(id).toString());
            }

            return (Blob) rs.getObject(1);

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Update attachment data
     * 
     * @param id attachment ID.
     * @param blob Attachment data
     */
    private void updateAttachmentData(Long id, oracle.sql.BLOB blob)  {
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(UPDATE_ATTACHMENT_BLOB);
            stm.setBlob(1, blob);
            setLong(stm, 2, id);

            stm.executeUpdate();

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Update attachment data
     * 
     * @param id attachment ID.
     * @param blob Attachment data
     */
    private void updateAttachmentAndStepData(Long id, Long preId, oracle.sql.BLOB blob)  {
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(UPDATE_ATTACHMENT_AND_STEP_BLOB);
            stm.setBlob(1, blob);
            setLong(stm, 2, preId);
            setLong(stm, 3, id);

            stm.executeUpdate();

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Get attachment content by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    public ProcedureAttachmentVO getContentById(Long attachmentId) {
        // Debug query
        List params = new ArrayList();
        params.add(attachmentId);
        debugQuery(GET_CONTENT_BY_ID, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(GET_CONTENT_BY_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            // Create/Initialize attachment value object
            ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
            vo.setId(attachmentId);

            vo.setMimeType(rs.getString("MIME_TYPE"));
            vo.setAttachment(getBlob(rs, "ATTACHMENT"));

            // Return value object
            return vo;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get procedure requester attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findRequesterAttchments(int pageNo, Long procedureId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        debugQuery(FIND_REQUESTER_ATTACHMENTS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int pageSize = getDefaultPageSize();
            int totalCount=getTotalCount(FIND_REQUESTER_ATTACHMENTS, params);

            stm  = doSearch(FIND_REQUESTER_ATTACHMENTS, params, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize, totalCount);
            while (rs.next()) {
                ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setId(procedureId);
                vo.setId(getLong(rs, "ID"));
                vo.setDescriptionAr(getString(rs, "DESCRIPTION_A"));
                vo.setNameAr(getString(rs, "ATTACHMENT_NAME"));
                vo.setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.setViewableInsideBrowser(getInteger(rs, "VIEWABLE_INSIDE_BROWSER"));
                vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "EMP_NAME"));
            }

            return searchPage;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get end step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findEndStepAttchments(int pageNo, Long procedureId){
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        debugQuery(FIND_END_STEP_ATTACHMENTS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int pageSize = getDefaultPageSize();
            int totalCount=getTotalCount(FIND_END_STEP_ATTACHMENTS, params);

            stm  = doSearch(FIND_END_STEP_ATTACHMENTS, params, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize, totalCount);
            while (rs.next()) {
                ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setId(procedureId);
                vo.setId(getLong(rs, "ID"));
                vo.setDescriptionAr(getString(rs, "DESCRIPTION_A"));
                vo.setNameAr(getString(rs, "ATTACHMENT_NAME"));
                vo.setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.setViewableInsideBrowser(getInteger(rs, "VIEWABLE_INSIDE_BROWSER"));
                vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "EMP_NAME"));
            }

            return searchPage;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Get attachment info by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    public ProcedureAttachmentVO getInfoById(Long attachmentId) {
        // Debug query
        List params = new ArrayList();
        params.add(attachmentId);
        debugQuery(GET_INFO_BY_ID, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(GET_INFO_BY_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            // Create/Initialize attachment value object
            ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
            vo.setUser(new UserVO());
            vo.getUser().setEmployee(new EmployeeVO());
            vo.setId(attachmentId);
            
            vo.setNameAr(getString(rs, "ATTACHMENT_NAME"));
            vo.setCreationDate(getDate(rs, "CREATION_DATE"));
            vo.setDescriptionAr(getString(rs, "DESCRIPTION_A"));
            vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "EMP_NAME_A"));

            // Return value object
            return vo;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get procedure active step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     * @param userId current user ID.
     */
    public SearchPageVO findActiveStepRequesterAttchments(int pageNo, 
                                                          Long procedureId,
                                                          Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        params.add(userId);
        debugQuery(FIND_ACTIVE_STEP_REQUESTER_ATTACHMENTS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int pageSize = getDefaultPageSize();
            int totalCount=getTotalCount(FIND_ACTIVE_STEP_REQUESTER_ATTACHMENTS, params);

            stm  = doSearch(FIND_ACTIVE_STEP_REQUESTER_ATTACHMENTS, params, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize, totalCount);
            while (rs.next()) {
                ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setId(procedureId);
                vo.setId(getLong(rs, "ID"));
                vo.setDescriptionAr(getString(rs, "DESCRIPTION_A"));
                vo.setNameAr(getString(rs, "ATTACHMENT_NAME"));
                vo.setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.setViewableInsideBrowser(getInteger(rs, "VIEWABLE_INSIDE_BROWSER"));
                vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "EMP_NAME"));
            }

            return searchPage;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get procedure active step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findActiveStepAttchments(int pageNo, Long procedureId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        debugQuery(FIND_ACTIVE_STEP_ATTACHMENTS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int pageSize = getDefaultPageSize();
            int totalCount=getTotalCount(FIND_ACTIVE_STEP_ATTACHMENTS, params);

            stm  = doSearch(FIND_ACTIVE_STEP_ATTACHMENTS, params, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize, totalCount);
            while (rs.next()) {
                ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setId(procedureId);
                vo.setId(getLong(rs, "ID"));
                vo.setDescriptionAr(getString(rs, "DESCRIPTION_A"));
                vo.setNameAr(getString(rs, "ATTACHMENT_NAME"));
                vo.setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.setViewableInsideBrowser(getInteger(rs, "VIEWABLE_INSIDE_BROWSER"));
                vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "EMP_NAME"));
            }

            return searchPage;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * find Step Attachments.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findStepAttachments(int pageNo, Long procedureId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        debugQuery(FIND_STEP_ATTACHMENTS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int pageSize = getDefaultPageSize();
            int totalCount=getTotalCount(FIND_STEP_ATTACHMENTS, params);

            stm  = doSearch(FIND_STEP_ATTACHMENTS, params, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize, totalCount);
            while (rs.next()) {
                ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setId(procedureId);
                vo.setId(getLong(rs, "ID"));
                vo.setDescriptionAr(getString(rs, "DESCRIPTION_A"));
                vo.setNameAr(getString(rs, "ATTACHMENT_NAME"));
                vo.setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.setViewableInsideBrowser(getInteger(rs, "VIEWABLE_INSIDE_BROWSER"));
                vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "EMP_NAME"));
            }

            return searchPage;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get procedure active step-user attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    public SearchPageVO findActiveStepUsersAttchments(int pageNo,
                        Long procedureId, Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        params.add(userId);
        debugQuery(FIND_ACTIVE_STEP_USERS_ATTACHMENTS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int pageSize = getDefaultPageSize();
            int totalCount=getTotalCount(FIND_ACTIVE_STEP_USERS_ATTACHMENTS, params);

            stm  = doSearch(FIND_ACTIVE_STEP_USERS_ATTACHMENTS, params, pageNo, pageSize);
            rs = stm .executeQuery();
            
            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize, totalCount);
            while (rs.next()) {
                ProcedureAttachmentVO vo = new ProcedureAttachmentVO();
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());
                searchPage.addRecord(vo);
                
                vo.setProcedure(new ProcedureVO());
                vo.getProcedure().setId(procedureId);
                vo.setId(getLong(rs, "ID"));
                vo.setDescriptionAr(getString(rs, "DESCRIPTION_A"));
                vo.setNameAr(getString(rs, "ATTACHMENT_NAME"));
                vo.setCreationDate(getDate(rs, "CREATION_DATE"));
                vo.setViewableInsideBrowser(getInteger(rs, "VIEWABLE_INSIDE_BROWSER"));
                vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "EMP_NAME"));
            }

            return searchPage;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Delete attachment.
     * 
     * @param procedureId Procedure ID.
     * @param attachmentId Attachment ID.
     * @param userId Current user ID.
     * @return true if the attachment deleted successfuly.
     */
    public boolean delete(Long procedureId, Long attachmentId, Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(userId);
        params.add(procedureId);
        params.add(attachmentId);
        debugQuery(DELETE_ATTACHMENT, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_ATTACHMENT);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();

            return count > 0;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Check if the user is allowed to delete this attachment.
     * 
     * @param procedureId Procedure ID.
     * @param attachmentId Attachment ID.
     * @param userId User ID
     * @return <LU>
     *           <LI>0</LI>: User is allowed to delete this attachment.
     *           <LI>1</LI>: User is not allowed to delete requester attachments.
     *           <LI>2</LI>: User is not allowed to delete other steps attachments.
     *           <LI>3</LI>: Active attachment not found.
     *         </LU>
     */
    public int isAllowedToDelete(Long procedureId, Long attachmentId, Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(userId);
        params.add(procedureId);
        params.add(attachmentId);
        debugQuery(IS_ALLOWED_TO_DELETE, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(IS_ALLOWED_TO_DELETE);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return 3;
            }
            
            int isRequesterAttachment = rs.getInt("IS_REQUESTER_ATT");
            int isCurrentUserAttachment = rs.getInt("IS_CURRENT_USER_ATT");
            int canDeleteRequesterAttachments = rs.getInt("CAN_DELETE_REQUESTER_ATT");
            int canDeleteOtherStepsAttachments = rs.getInt("CAN_DELETE_OTHER_STEPS_ATT");
            long attachmentStepId = rs.getLong("ATC_PRE_ID");
            long activeStepId = rs.getLong("ACTIVE_STEP_ID");

            // User is not allowed to delete requester attachments
            if (isRequesterAttachment == 2 && canDeleteRequesterAttachments == 1) {
                return 1;
            }

            // User can always delete his own attachments
            if (isCurrentUserAttachment == 2 && attachmentStepId == activeStepId) {
                return 0;
            }
            
            // User is not allowed to delete other steps attachments
            if (canDeleteOtherStepsAttachments == 1) {
                return 2;
            }
            
            // User is allowed to delete this attachment
            return 0;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get Attachment Count
     * 
     * @param procedureId Procedure ID.
     */
    public Long getAttachmentCount(Long procedureId){
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        debugQuery(GET_ATTACHMENT_COUNT, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(GET_ATTACHMENT_COUNT);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            int count = 0;
            
            while ( rs.next()) {
                count++;
            }

            return new Long(count);

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Copy transaction attachments.
     * 
     * @param transactionVO Transaction Value Object.
     * @param procedureVO Procedure Value Object.
     */
    public void copyTrsAttachments(TransactionVO transactionVO, 
                                    ProcedureVO procedureVO) {
        NamedQuery query = 
            getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureAttachment.add");
        
        debugQuery(query);
        PreparedStatement prepStmt = null;
        try {
            prepStmt = prepareStatement(query);
            int recordsSize = (transactionVO.getTransactionAttachments() == null) 
                               ? 0 
                               : transactionVO.getTransactionAttachments().size();
            List procAttVOList = new ArrayList();
            for (int i = 0; i <recordsSize ; i++) {
                // generate new attachment ID
                Long newId = generateSequence("ATC_SEQ");
                
                // fill created attachment in list to be used 
                // to update attachment BLOB
                ProcedureAttachmentVO procAttVO = new ProcedureAttachmentVO();
                procAttVO.setId(newId);
                procAttVOList.add(procAttVO);
                
                TransactionAttachmentVO trsAttVO = (TransactionAttachmentVO)transactionVO.getTransactionAttachments().get(i);
                
                prepStmt.setLong(1, newId.longValue());
                prepStmt.setString(2, "Attachment " + (i + 1));
                prepStmt.setString(3, trsAttVO.getFileExtension());
                prepStmt.setLong(4, procedureVO.getId().longValue());
                prepStmt.setLong(5, transactionVO.getUserProfile().getUserId().longValue());
                if (procedureVO.getActiveStep() != null
                        && procedureVO.getActiveStep().getId() != null) {
                    prepStmt.setLong(6, procedureVO.getActiveStep().getId().longValue());
                } else {
                    prepStmt.setString(6, null);
                }
                prepStmt.setString(7, procedureVO.getCreatedBy());
                prepStmt.setString(8, procedureVO.getCreatedBy());
                
                prepStmt.setString(9, null);
                
                prepStmt.addBatch();
            }
            
            prepStmt.executeBatch();
            
            
            // fill Attachment bytes as BLOB
            for (int i = 0; i < procAttVOList.size(); i++) {
                ProcedureAttachmentVO procAttVO = (ProcedureAttachmentVO)procAttVOList.get(i);
                TransactionAttachmentVO trsAttVO = (TransactionAttachmentVO)transactionVO.getTransactionAttachments().get(i);
                // Get attachment BLOB to be updated
                Blob attachmentBlob = getAttachmentForUpdate(procAttVO.getId());
    
                // Set attachment bytes to BLOB
                updateBlob(attachmentBlob, trsAttVO.getAttachment());
                
                Long preId = getStepByProcIdAndSeqNo(procedureVO.getId().longValue(),
                                            FieldsCodes.STEP_VERIFY_DOCUMENTS.longValue());
                
                // Save BLOB data to database
                updateAttachmentAndStepData(procAttVO.getId(), preId, (oracle.sql.BLOB) attachmentBlob);
            }

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(prepStmt);
        }
    }
    
    /**
     * Get Step ID by ProcID and Sequence No
     * 
     * @return Step ID
     * 
     * @param seqNo
     * @param procId
     */
    private Long getStepByProcIdAndSeqNo(long procId, long seqNo) {
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        Long preId = null;
        
        try {
            NamedQuery query = getNamedQuery("ae.rta.eps.dao.jdbc.ProcedureAttachment.getStepByProcIdAndSeqNo");
            stmt = prepareStatement(query);
            stmt.setLong(1, procId);
            stmt.setLong(2, seqNo);
            
            rs = stmt.executeQuery();
            
            if(rs.next()) {
                preId = getLong(rs, "ID");
            }
            
        } catch(SQLException sqle) {
            throw new DataAccessException(sqle);
        } finally {
            close(rs);
            close(stmt);
        }
        
        return preId;
    }
}