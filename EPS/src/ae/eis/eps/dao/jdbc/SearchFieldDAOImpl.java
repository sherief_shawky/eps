/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA  02/06/2009  - File created.
 */
 
package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.SearchFieldDAO;
import ae.eis.eps.vo.ProcedureTemplateLogVO;
import ae.eis.eps.vo.SearchFieldVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.eps.vo.TemplateTableHeaderVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Search field data access object intefrace.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public class SearchFieldDAOImpl extends JdbcDataAccessObject  
                                implements SearchFieldDAO {
    /** Find serch fields quiry */
    private static final String FIND_SEARCH_FIELD
        =   " SELECT SFI.ID SFI_ID,"
        +          " SFI.SEARCH_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          " ('TF_EPS_SEARCH_TYPE',SEARCH_TYPE) SEARCH_TYPE_DESC,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          " ('TF_EPS_FIELD_TYPE',FIELD_TYPE) FIELD_TYPE_DESC,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          " ('TF_EPS_COLUMN_SPAN',COLUMN_SPAN) COLUMN_SPAN_DESC,"        
        +          " SFI.ORDER_NO,"
        +          " SFI.COLUMN_SPAN,"
        +          " SFI.VIEW_IN_NEW_LINE,"
        +          " TPE.LABLE,"
        +          " TPE.ID TPE_ID,"
        +          " TPE.FIELD_TYPE,"
        +          " SFI.TTH_ID,"
        +          " TTH.COLUMN_TYPE,"
        +          " TTH.NAME_A,"
        +          " TTH.MAX_SIZE,"
        +          " TTH.CODE AS COLUMN_CODE,"
        +          " TPE.CODE"
        +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
		+          " TF_EPS_SEARCH_FIELDS SFI,"
        +          " TF_EPS_TEMP_TABLE_HEADERS TTH"
        +    " WHERE TPE.ID = SFI.TPE_ID"
        +      " AND SFI.TTH_ID = TTH.ID(+) "
        +      " AND TPE.PTL_ID = ? "
        +    " ORDER BY SFI.ORDER_NO ";
    
    /** delete serch field */
    private static final String DELETE_SEARCH_FIELD
        =   " DELETE FROM TF_EPS_SEARCH_FIELDS WHERE ID = ? ";
    
    /** add log query */
    private static final String ADD_LOG_QUERY
        =    "INSERT INTO TF_EPS_PROCEDURE_TEMP_LOGS "
        +    "(ID,STATUS,VERSION_NO,NAME_E,ACTION_TYPE,PRIORITY,NAME_A, "
        +    "STATUS_DATE,PTL_ID,EMPLOYEE_NAME,CREATED_BY,CREATION_DATE) "
        +    "SELECT PTG_SEQ.NEXTVAL, "
        +          " PTL.STATUS,"
        +          " PTL.VERSION_NO,"
        +          " PTL.NAME_E,"
        +          " ?,"
        +          " PTL.PRIORITY,"
        +          " PTL.NAME_A,"
        +          " SYSDATE,"
        +          " PTL.ID,"
        +          " F_DB_GET_EMP_NAME_A(?),"
        +          " ?,"
        +          " SYSDATE"
        +     " FROM TF_EPS_PROCEDURE_TEMPLATES PTL"
        +    " WHERE PTL.ID = ? ";
    
     /** creat search field query */
     private static final String CREATE_SEARCH_FIELD
        =   " INSERT INTO TF_EPS_SEARCH_FIELDS "
		+         " (ID,SEARCH_TYPE,ORDER_NO,COLUMN_SPAN,TPE_ID,VIEW_IN_NEW_LINE,"
        +           "CREATED_BY,UPDATED_BY,CREATION_DATE,UPDATE_DATE,TTH_ID) "
        +   " VALUES(?,"        // ID
        +           "?,"        // SEARCH_TYPE
        +           "?,"        // ORDER_NO
        +           "?,"        // COLUMN_SPAN
        +           "?,"        // TPE_ID
        +           "?,"        // VIEW_IN_NEW_LINE
        +           "?,"        // CREATED_BY
        +           "?,"        // UPDATED_BY
        +           "SYSDATE,"  // CREATION_DATE
        +           "SYSDATE,"  // UPDATE_DATE
        +           "?)"; // Table Header
        
    /** is exist search field */
    private static final String IS_EXIST_SEARCH_FIELD
        ="SELECT 1 FROM TF_EPS_SEARCH_FIELDS WHERE TPE_ID = ?";
        
    /** is order number exist */
    private static final String IS_EXIST_ORDER_NO
        ="SELECT 1 FROM TF_EPS_SEARCH_FIELDS SFI,"
                    +"  TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
                    +"  TF_EPS_PROCEDURE_TEMPLATES PTL"
                    +"  WHERE SFI.TPE_ID=TPE.ID "
                    +"  AND   TPE.PTL_ID=PTL.ID "
                    +"  AND   PTL.ID = ? "
                    +"  AND   SFI.ORDER_NO=? ";
    
     /** Get serch fields quiry */
    private static final String GET_SEARCH_FIELD
        =   " SELECT SFI.SEARCH_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          " ('TF_EPS_SEARCH_TYPE',SEARCH_TYPE) SEARCH_TYPE_DESC,"
        +          " SFI.ORDER_NO,"
        +          " SFI.COLUMN_SPAN,"
        +          " SFI.VIEW_IN_NEW_LINE,"
        +          " SFI.TPE_ID,"
        +          " TPE.FIELD_TYPE,"
        +          " TPE.LABLE,"
        +          " TPE.CODE,"
        +          " SFI.TTH_ID,"
        +          " TTH.NAME_A,"
        +          " TTH.COLUMN_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          " ('TF_EPS_FIELD_TYPE',FIELD_TYPE) FIELD_TYPE_DESC"
        +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
		+          " TF_EPS_SEARCH_FIELDS SFI,"
        +          " TF_EPS_TEMP_TABLE_HEADERS TTH"
        +    " WHERE TPE.ID = SFI.TPE_ID"
        +      " AND SFI.TTH_ID = TTH.ID(+) "
        +      " AND SFI.ID = ? ";
    
    /** update search field */
    private static final String UPDATE_SEARCH_FIELD
        =   " UPDATE TF_EPS_SEARCH_FIELDS "
        +      " SET SEARCH_TYPE = ?,"                 
        +          " ORDER_NO = ?,"
        +          " COLUMN_SPAN = ?,"
        +          " VIEW_IN_NEW_LINE = ?,"
        +          " UPDATE_DATE = SYSDATE,"
        +          " UPDATED_BY = ?, "
        +          " TTH_ID = ?"
        +    " WHERE ID = ?	" ;  
    
    /** Get Maximun Search Field Order Number */
    private static final String GET_MAX_SEARCH_FIELD_ORDER_NUMBER
        = " SELECT MAX(ORDER_NO) MAX_ORDER_NO "
        +   " FROM TF_EPS_SEARCH_FIELDS "
        +  " WHERE TPE_ID IN( SELECT ID FROM TF_EPS_TEMP_PROCEDURE_FIELDS "
        +                            " WHERE PTL_ID = ? ) ";
    
    
    /**
     * Is order number exists 
     * 
     * @return true if exist
     * @param templateId template ID
     * @param orderNo order number
     * @param searchFieldId search field ID
     */
    public boolean isOrderNoExists(Long templateId, Integer orderNo,Long searchFieldId){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_EXIST_ORDER_NO);
        params.add(templateId);
        params.add(orderNo);
        if(searchFieldId != null && !"".equals(searchFieldId)){
            sql.append(" AND SFI.ID <> ? ");
            params.add(searchFieldId);
        }
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Update search field 
     * 
     * @param vo search field value object
     */
    public void update(SearchFieldVO vo){
        PreparedStatement stm = null;
        
        // Debug query
        List params = new ArrayList();             
        
        params.add(vo.getSearchType());
        params.add(vo.getOrderNo());
        params.add(vo.getColumnSpan());
        params.add(vo.getViewInNewLine());
        params.add(vo.getCreatedBy());
        if (vo.getTemplateTableHeaderVO() != null) {
            params.add(vo.getTemplateTableHeaderVO().getId());       
        } else {
            params.add(null);
        }
        
        params.add(vo.getId());
        
        debugQuery(UPDATE_SEARCH_FIELD, params);
            
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_SEARCH_FIELD);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if (count <= 0) {
                throw new DataAccessException(" Cannot Update Field : " 
                                               + vo.getId());
            }
            
            addLog(vo,vo.getTemplateField().getProcedureTemplate().getId(), ProcedureTemplateLogVO.ACTION_UPDATE_SEARCH_FIELD);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * Get by ID
     * 
     * @return  search field value object
     * @param searchFieldId search field ID
     */
    public SearchFieldVO getById(Long searchFieldId){
        // Debug query
        List params = new ArrayList();
        params.add(searchFieldId);
        debugQuery(GET_SEARCH_FIELD, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_SEARCH_FIELD);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            SearchFieldVO vo = new SearchFieldVO();
            vo.setTemplateField(new TemplateFieldVO());
            vo.setTemplateTableHeaderVO(new TemplateTableHeaderVO());
    
            vo.setColumnSpan(getInteger(rs,"COLUMN_SPAN"));
            vo.setOrderNo(getInteger(rs,"ORDER_NO"));
            vo.setSearchType(getInteger(rs,"SEARCH_TYPE"));
            vo.setSearchTypeDescAr(getString(rs,"SEARCH_TYPE_DESC"));
            vo.setViewInNewLine(getInteger(rs,"VIEW_IN_NEW_LINE"));
            vo.getTemplateField().setId(getLong(rs,"TPE_ID"));
            vo.getTemplateField().setLabel(getString(rs,"LABLE"));
            vo.getTemplateField().setCode(getInteger(rs,"CODE"));
            vo.getTemplateField().setType(getInteger(rs,"FIELD_TYPE"));
            vo.getTemplateField().setTypeDesc(getString(rs,"FIELD_TYPE_DESC"));
            vo.getTemplateTableHeaderVO().setId(getLong(rs,"TTH_ID"));
            vo.getTemplateTableHeaderVO().setNameAr(getString(rs,"NAME_A"));
            vo.getTemplateTableHeaderVO().setColumnType(getLong(rs,"COLUMN_TYPE"));
            
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Is search field exists 
     * 
     * @return true if exist
     * @param fieldId
     */
    public boolean isSearchFieldExists(Long fieldId){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_EXIST_SEARCH_FIELD);
        params.add(fieldId);
        
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Creat search field 
     * 
     * @param templateId template ID
     * @param vo search field value object
     */
    public void create(SearchFieldVO vo){
        PreparedStatement stm = null;
        try {
            // Debug query
            Long id = generateSequence("SFI_SEQ");
            List params = new ArrayList();
            params.add(id);
            params.add(vo.getSearchType());
            params.add(vo.getOrderNo());
            params.add(vo.getColumnSpan());
            params.add(vo.getTemplateField().getId());
            params.add(vo.getViewInNewLine());
            params.add(vo.getCreatedBy());
            params.add(vo.getCreatedBy());
            if (vo.getTemplateTableHeaderVO() != null) {
                params.add(vo.getTemplateTableHeaderVO().getId());       
            } else {
                params.add(null);
            }
            
            debugQuery(CREATE_SEARCH_FIELD, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_SEARCH_FIELD);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            // add log
            addLog(vo,vo.getTemplateField().getProcedureTemplate().getId(), ProcedureTemplateLogVO.ACTION_ADD_SEARCH_FIELD);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }

    /**
     * Find search fields
     * 
     * @return searchPageVO search page value object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO find(int pageNo, Long templateId) {
        ArrayList params = new ArrayList();
        params.add(templateId);
        debugQuery(FIND_SEARCH_FIELD, params);
        
        ResultSet rs = null;
        PreparedStatement stm = null;
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(FIND_SEARCH_FIELD, params);
            
            stm = doSearch(FIND_SEARCH_FIELD, params, pageNo , pageSize);
            rs  = stm.executeQuery();
            
            SearchPageVO searchPage = 
                    new SearchPageVO(pageNo, pageSize ,totalRecordCounts);   
            
            while (rs.next()) {
                searchPage.addRecord(getValueObject(rs));
            }

            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Get template search fields.
     * 
     * @param templateId EPS template ID.
     * @return List of search fields.
     */
    public List getSearchFields(Long templateId) {
        ArrayList params = new ArrayList();
        params.add(templateId);
        debugQuery(FIND_SEARCH_FIELD, params);
        
        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(FIND_SEARCH_FIELD);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List fieldsList = new ArrayList();
            while (rs.next()) {
                fieldsList.add(getValueObject(rs));
            }

            return fieldsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Parse JDBC result set and populate SearchFieldVO object.
     * 
     * @param rs JDBC result set.
     * @return SearchFieldVO object.
     */
    private SearchFieldVO getValueObject(ResultSet rs) throws SQLException {
        SearchFieldVO vo = new SearchFieldVO();
        vo.setTemplateField(new TemplateFieldVO());
        vo.setTemplateTableHeaderVO(new TemplateTableHeaderVO());
        
        vo.setId(getLong(rs, "SFI_ID"));
        vo.setColumnSpan(getInteger(rs,"COLUMN_SPAN"));
        vo.setColumnSpanDesc(getString(rs,"COLUMN_SPAN_DESC"));
        vo.setOrderNo(getInteger(rs,"ORDER_NO"));
        vo.setSearchType(getInteger(rs,"SEARCH_TYPE"));
        vo.setSearchTypeDescAr(getString(rs,"SEARCH_TYPE_DESC"));
        vo.setViewInNewLine(getInteger(rs,"VIEW_IN_NEW_LINE"));
        vo.getTemplateField().setId(getLong(rs,"TPE_ID"));
        vo.getTemplateField().setLabel(getString(rs,"LABLE"));
        vo.getTemplateField().setType(getInteger(rs,"FIELD_TYPE"));
        vo.getTemplateField().setTypeDesc(getString(rs,"FIELD_TYPE_DESC"));
        vo.getTemplateField().setCode(getInteger(rs, "CODE"));
        vo.getTemplateTableHeaderVO().setNameAr(getString(rs, "NAME_A"));
        vo.getTemplateTableHeaderVO().setColumnType(getLong(rs, "COLUMN_TYPE"));
        vo.getTemplateTableHeaderVO().setId(getLong(rs, "TTH_ID"));
        vo.getTemplateTableHeaderVO().setMaxSize(getLong(rs, "MAX_SIZE"));
        vo.getTemplateTableHeaderVO().setCode(getInteger(rs, "COLUMN_CODE"));
        
        return vo;
    }

    /**
     * Delete search field 
     * 
     * @param templateId template ID
     * @param vo search field value object
     */
    public void delete(SearchFieldVO vo,Long templateId){
        
        PreparedStatement stm = null;
        List params = new ArrayList();
        params.add(vo.getId());
        
        debugQuery(DELETE_SEARCH_FIELD, params);
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_SEARCH_FIELD);
            setQueryParameters(stm, params);
            stm.executeUpdate();

            addLog(vo,templateId, ProcedureTemplateLogVO.ACTION_DELETE_SEARCH_FIELD);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Add log 
     * @param actionType action type
     * @param templateId template ID
     * @param vo search filed value object
     */
    private void addLog(SearchFieldVO vo,Long templateId, Integer actionType) {
        // Debug message
        List params = new ArrayList();
        
        params.add(actionType);
        params.add(vo.getCreatedBy());
        params.add(vo.getCreatedBy());
        params.add(templateId);
        
        debugQuery(ADD_LOG_QUERY, params);
        
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(ADD_LOG_QUERY);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Get Maximun Search Field Order Number
     * 
     * @return Maximun Search Field Order Number
     * @param templateId Template Id
     */
    public Integer getMaxSearchFieldOrderNo(Long templateId) {
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        
        ArrayList params = new ArrayList();
        params.add(templateId);

        // Debug Query
        debugQuery(GET_MAX_SEARCH_FIELD_ORDER_NUMBER, params);     
        
        try {
            prepStmt = getConnection().prepareStatement(GET_MAX_SEARCH_FIELD_ORDER_NUMBER);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            Integer maxOrderNo = new Integer(0);
            
            if(resultSet.next()){
                maxOrderNo = getInteger(resultSet,"MAX_ORDER_NO");
                if(maxOrderNo == null)
                    maxOrderNo = new Integer(0);
            }

            return maxOrderNo;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }   
    }
}