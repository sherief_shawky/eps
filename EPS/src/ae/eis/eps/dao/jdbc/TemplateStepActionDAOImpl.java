/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateStepActionDAO;
import ae.eis.eps.vo.TemplateStepActionVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure template step action data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepActionDAOImpl extends JdbcDataAccessObject 
                                       implements TemplateStepActionDAO {
    /*
     * JDBC SQL and DML statements.
     */
    
    /** Create procedure template step action DML. */
    private static final String CREATE_ACTION
    = " INSERT INTO TF_EPS_TEMP_STEP_ACTIONS"
    +  "(ID,ACTION_TYPE,PTS_ID,CREATED_BY,CREATION_DATE)"
    +  " VALUES(?," // ID
    +         " ?," // ACTION_TYPE
    +         " ?," // PTS_ID
    +         " ?," // CREATED_BY
    +         " SYSDATE)"; // CREATION_DATE

    /** Delete procedure template step action. */
    private static final String DELETE_ACTION
        =   " DELETE FROM TF_EPS_TEMP_STEP_ACTIONS "
        +         " WHERE ACTION_TYPE IN (2,3)"
        +           " AND PTS_ID = ? ";
    
    /** Get Action Bt Step Id */
    private static final String GET_ACTION_BY_ID
        =   " SELECT ACTION_TYPE "
        +     " FROM TF_EPS_TEMP_STEP_ACTIONS "
        +    " WHERE PTS_ID = ? ";
    
    /** Check if update attachment is allowed for a specific step. */
    private static final String CAN_UPDATE_ATTACHMENT
        =   " SELECT 1 "
        +     " FROM TF_EPS_TEMP_STEP_ACTIONS "
        +    " WHERE PTS_ID = ? "
        +      " AND (ACTION_TYPE = 4 OR ACTION_TYPE = 8)" ;
     
    /** Get requester allowed actions. */
    private static final String GET_REQUESTER_ACTIONS
    = " SELECT TSA.ID,"
    +        " TSA.ACTION_TYPE"
    + " FROM TF_EPS_TEMP_STEP_ACTIONS TSA,"
    +      " TF_EPS_PROCEDURE_TEMP_STEPS PTS"
    + " WHERE TSA.PTS_ID = PTS.ID"
    +   " AND PTS.STEP_TYPE = 4"
    +   " AND PTS.PTL_ID = ?"; // Template ID

    /** Get Attachment Settings Query */
    private static final String GET_ATTACHMENT_ACTIONS
        =   " SELECT ID,ACTION_TYPE "
        +     " FROM TF_EPS_TEMP_STEP_ACTIONS "
        +    " WHERE PTS_ID = ? "
        +      " AND ACTION_TYPE NOT IN (2,3)" ;

    /** Get Attachment Settings Query */
    private static final String GET_USER_ACTIONS
        =   " SELECT ID,ACTION_TYPE "
        +     " FROM TF_EPS_TEMP_STEP_ACTIONS "
        +    " WHERE PTS_ID = ? "
        +      " AND ACTION_TYPE IN (2,3)" ;
        
    /** Delete Step Actions */
    private static final String DELETE_STEP_ACTIONS
        =   " DELETE FROM TF_EPS_TEMP_STEP_ACTIONS "
        +         " WHERE ACTION_TYPE NOT IN (2,3)"
        +           " AND PTS_ID = ? ";
        
    /** Create Step Actions */
    private static final String CREATE_STEP_ACTIONS
        =   " INSERT INTO TF_EPS_TEMP_STEP_ACTIONS "
        +          "(ID,ACTION_TYPE,PTS_ID,CREATED_BY,CREATION_DATE)" 
        +   " VALUES(TSA_SEQ.NEXTVAL," // ID
        +           "?,"        // ACTION TYPE
        +           "?,"        // STEP ID
        +           "?,"        // CREATED BY
        +           "SYSDATE)"; // CREATION DATE
        
    /*
     * Methods
     */

    /**
     * Create new procedure template step action.
     * 
     * @param actionsList Procedure template step actions List.
     */
    private TemplateStepActionVO create(TemplateStepActionVO vo) {
        PreparedStatement stm = null;
        try {
            // Generate new sequence
            Long newId = generateSequence("TSA_SEQ");
            
            // Debug query
            List params = new ArrayList();
            params.add(newId);
            params.add(vo.getActionType());
            params.add(vo.getTemplateStep().getId());
            params.add(vo.getTemplateStep().getCreatedBy());
            debugQuery(CREATE_ACTION, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_ACTION);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            // Save new ID and return updated value object
            vo.setId(newId);
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
       
       
    /**
     * Delete procedure template step action.
     * 
     * @param actionsList Procedure template step actions List.
     */
    public void delete(TemplateStepVO vo){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(vo.getId());
            debugQuery(DELETE_ACTION, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_ACTION);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    /**
     * Create new procedure template step action.
     * 
     * @param actionsList Procedure template step actions List.
     */
    public TemplateStepActionVO create(TemplateStepVO stepVO) {
        List actionsList = stepVO.getActionsList();
        TemplateStepActionVO stepActionVO = new TemplateStepActionVO();
        for (int i = 0; i < actionsList.size(); i++)  {
            TemplateStepActionVO vo = new TemplateStepActionVO();
            vo.setTemplateStep(stepVO);
            vo.setActionType(((TemplateStepActionVO)actionsList.get(i)).getActionType());
            stepActionVO = create(vo);
        }
        return stepActionVO;
    }
    
    /**
     * get Step Type Actions By Id
     * 
     * @return List of Template Step Action Value Object
     * @param stepId
     */
    public List getByStepId(Long stepId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        String sql = GET_ACTION_BY_ID;
        ArrayList listOfparams = new ArrayList();
        List list = new ArrayList();
        listOfparams.add(stepId);
        debugQuery(sql, listOfparams);
        
        try{
            prepStmt = getConnection().prepareStatement(sql);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
            
            while(resultSet.next()){
                TemplateStepActionVO vo = new TemplateStepActionVO();
                vo.setActionType(getInteger(resultSet,"ACTION_TYPE"));
                list.add(vo);                
            }

            return list;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Can Update Attachment
     * return TRUE if can add or delete attachment otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param stepId
     */
    public boolean canUpdateAttachment(Long stepId){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(stepId);
        debugQuery(CAN_UPDATE_ATTACHMENT, listOfparams);
        
        try{
            prepStmt = getConnection().prepareStatement(CAN_UPDATE_ATTACHMENT);
            setQueryParameters(prepStmt,listOfparams);
            resultSet = prepStmt.executeQuery();
                        
            return resultSet.next();
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Get requester allowed actions.
     * 
     * @param templateId Template ID.
     * @return  requester allowed actions.
     */
    public List getRequesterActions(Long templateId) {
        ArrayList params = new ArrayList();
        params.add(templateId);
        debugQuery(GET_REQUESTER_ACTIONS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;    
        try {
            stm = getConnection().prepareStatement(GET_REQUESTER_ACTIONS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List list = new ArrayList();
            while (rs.next()) {
                TemplateStepActionVO vo = new TemplateStepActionVO();
                list.add(vo);                
                
                vo.setId(getLong(rs, "ID"));
                vo.setActionType(getInteger(rs, "ACTION_TYPE"));
            }

            return list;

        } catch(Exception ex) {
            throw new DataAccessException(ex);

        } finally {
            close(rs);
            close(stm);
        }         
    }
    
    /**
     * Get Attachment Settings
     * 
     * @return List of step actions
     * @param stepId Step Id
     */
    public List getAttachmentActions(Long stepId){
        ArrayList params = new ArrayList();
        params.add(stepId);
        debugQuery(GET_ATTACHMENT_ACTIONS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;    
        try {
            stm = getConnection().prepareStatement(GET_ATTACHMENT_ACTIONS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List list = new ArrayList();
            while (rs.next()) {
                TemplateStepActionVO vo = new TemplateStepActionVO();
                vo.setId(getLong(rs, "ID"));
                vo.setActionType(getInteger(rs, "ACTION_TYPE"));
                list.add(vo);                
            }

            return list;

        } catch(Exception ex) {
            throw new DataAccessException(ex);

        } finally {
            close(rs);
            close(stm);
        }          
    }
    
    /**
     * Update Attachment Settings
     * 
     * @param vo Template Step Action Value Object
     */
    public void updateAttachmentActions(TemplateStepActionVO vo){
        deleteStepActions(vo.getTemplateStep().getId());
 
        List actionList = vo.getTemplateStep().getActionsList();
        
        if(actionList != null && actionList.size() > 0){
            for (int i = 0; i < actionList.size(); i++)  {
                TemplateStepActionVO newVO = (TemplateStepActionVO)actionList.get(i);
                createStepActions(vo.getTemplateStep().getId(),newVO.getActionType(),vo.getTemplateStep().getCreatedBy());
            }  
        }
    }        
    
    /**
     * Delete template step actions .
     * 
     * @param stepId Template step Id
     */
    private void deleteStepActions(Long stepId){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(stepId);

            debugQuery(DELETE_STEP_ACTIONS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_ACTIONS);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }     
    
    /**
     * Delete template step actions .
     * 
     * @param stepId Template step Id
     */
    private void createStepActions(Long stepId,Integer actionType,String createdBy){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(actionType);
            params.add(stepId);
            params.add(createdBy);

            debugQuery(CREATE_STEP_ACTIONS, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_STEP_ACTIONS);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    } 
    
    
    /**
     * Get Attachment Settings
     * 
     * @return List of step actions
     * @param stepId Step Id
     */
    public List getUserActions(Long stepId){
        ArrayList params = new ArrayList();
        params.add(stepId);
        debugQuery(GET_USER_ACTIONS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;    
        try {
            stm = getConnection().prepareStatement(GET_USER_ACTIONS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            List list = new ArrayList();
            while (rs.next()) {
                TemplateStepActionVO vo = new TemplateStepActionVO();
                list.add(vo);                
                
                vo.setId(getLong(rs, "ID"));
                vo.setActionType(getInteger(rs, "ACTION_TYPE"));
            }

            return list;

        } catch(Exception ex) {
            throw new DataAccessException(ex);

        } finally {
            close(rs);
            close(stm);
        }          
    }    
}