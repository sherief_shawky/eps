/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemporaryAttachmentDAO;
import ae.eis.eps.vo.TemporaryAttachmentVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Temporary attachments data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemporaryAttachmentDAOImpl extends JdbcDataAccessObject  
                                        implements TemporaryAttachmentDAO {
    /*
     * JDBC SQL and DML statements.
     */
    
    /** Create new attachment record DML. */
    private static final String CREATE_ATTACHMENT
    = " INSERT INTO TF_EPS_TEMPORARY_ATTACHMENTS"
    + " (ID,MTP_EXTENSION,CREATED_BY,CREATION_DATE,ATTACHMENT_NAME,ATTACHMENT,SEQ_NO)"
    + " VALUES(?," // ID
    +        " ?," // MTP_EXTENSION
    +        " ?," // CREATED_BY
    +        " SYSDATE,"
    +        " ?," // ATTACHMENT_NAME
    +        " EMPTY_BLOB(),"
    +        " ?)"; // SEQ_NO
    
    /** Get attachment record for update. */
    private static final String GET_ATTACHMENT_FOR_UPDATE
    = " SELECT ATTACHMENT FROM TF_EPS_TEMPORARY_ATTACHMENTS"
    + " WHERE ID = ?" // ID
    + " FOR UPDATE";

    /** Update attachment record BLOB. */
    private static final String UPDATE_ATTACHMENT_BLOB
    = "UPDATE TF_EPS_TEMPORARY_ATTACHMENTS SET ATTACHMENT = ? WHERE ID = ?";
    
    /** Get attachment record by ID. */
    private static final String GET_CONTENT_BY_ID
    = " SELECT TAM.ATTACHMENT,"
    +        " MTP.MIME_TYPE"
    + " FROM TF_EPS_TEMPORARY_ATTACHMENTS TAM,"
    +      " TF_STP_MIME_TYPES MTP"
    + " WHERE TAM.MTP_EXTENSION = MTP.EXTENSION"
    + " AND ID = ?";

    /** Find attachments */
    private static final String FIND_ATTACHMENT
    = " SELECT TAM.ID,"
    +        " MTP.DESCRIPTION_A,"
    +        " TAM.ATTACHMENT_NAME"
    + " FROM TF_EPS_TEMPORARY_ATTACHMENTS TAM,"
    +      " TF_STP_MIME_TYPES MTP"
    + " WHERE TAM.MTP_EXTENSION = MTP.EXTENSION"
    +   " AND TAM.SEQ_NO = ?"
    + " ORDER BY ID";
    
    /** delete attachment. */
    private static final String DELETE_ATTACHMENT
    = "DELETE TF_EPS_TEMPORARY_ATTACHMENTS WHERE ID = ?"; 
    
    /** Get attachment total number. */
    private static final String GET_ATTACHMENTS_COUNT
    = " SELECT COUNT(*) FROM TF_EPS_TEMPORARY_ATTACHMENTS WHERE SEQ_NO = ?";

    /*
     * Methods
     */

    /**
     * Save attachment.
     * 
     * @param vo Attachment value object.
     */
    public void save(TemporaryAttachmentVO vo) {
        try {
            // Generate new sequenceNo for first attachment.
            if (vo.getSequenceNo() == null) {
                vo.setSequenceNo(generateSequence("TAM_SEQ"));
            }

            // Create new attachment record
            createAttachment(vo);

            // Get attachment BLOB to be updated
            Blob attachmentBlob = getAttachmentForUpdate(vo.getId());

            // Set attachment bytes to BLOB
            updateBlob(attachmentBlob, vo.getAttachment());

            // Save BLOB data to database
            updateAttachmentData(vo.getId(), (oracle.sql.BLOB) attachmentBlob);

       } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        }
    }

    /**
     * Create new attachment record.
     * 
     * @param vo Attachment value object.
     * @return new attachments ID.
     */
    private Long createAttachment(TemporaryAttachmentVO vo) {
        PreparedStatement stm = null;
        try {
            // generate new attachment ID
            Long newId = generateSequence("TAM_SEQ");
            vo.setId(newId);
            
            // Debug query
            List params = new ArrayList();
            params.add(newId);
            params.add(vo.getFileExtension().toString());
            params.add(vo.getCreatedBy());
            params.add(vo.getNameAr());
            params.add(vo.getSequenceNo());
            debugQuery(CREATE_ATTACHMENT, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_ATTACHMENT);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            // Return new attachments ID.
            return newId;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Get attachment Blob object to be updated.
     * 
     * @param id attachment ID.
     * @return Attachment Blob object.
     */
    private Blob getAttachmentForUpdate(Long id) {
        // Debug query
        List params = new ArrayList();
        params.add(id);
        debugQuery(GET_ATTACHMENT_FOR_UPDATE, params);

        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(GET_ATTACHMENT_FOR_UPDATE);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (! rs.next()) {
                throw new DataAccessException(
                        new StringBuffer("Attachment not found: ")
                           .append(id).toString());
            }

            return (Blob) rs.getObject(1);

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Update attachment data
     * 
     * @param id attachment ID.
     * @param blob Attachment data
     */
    private void updateAttachmentData(Long id, oracle.sql.BLOB blob)  {
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(UPDATE_ATTACHMENT_BLOB);
            stm.setBlob(1, blob);
            setLong(stm, 2, id);

            stm.executeUpdate();

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Get attachment content by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    public TemporaryAttachmentVO getContentById(Long attachmentId) {
        // Debug query
        List params = new ArrayList();
        params.add(attachmentId);
        debugQuery(GET_CONTENT_BY_ID, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(GET_CONTENT_BY_ID);
            setLong(stm, 1, attachmentId);
            rs = stm.executeQuery();

            if (! rs.next()) {
                return null;
            }

            // Create/Initialize attachment value object
            TemporaryAttachmentVO vo = new TemporaryAttachmentVO();
            vo.setId(attachmentId);

            vo.setMimeType(rs.getString("MIME_TYPE"));
            vo.setAttachment(getBlob(rs, "ATTACHMENT"));

            // Return value object
            return vo;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * get attachments
     * 
     * @return list of attachments
     * @param sequenceNo
     */
    public SearchPageVO find(int pageNo,Long sequenceNo){
        ArrayList params = new ArrayList();
        params.add(sequenceNo);
        debugQuery(FIND_ATTACHMENT, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(FIND_ATTACHMENT, params);
            prepStmt = doSearch(FIND_ATTACHMENT, params, pageNo, pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()) {
                TemporaryAttachmentVO vo = new TemporaryAttachmentVO(); 
                vo.setId(getLong(resultSet,"ID"));
                vo.setDescriptionAr(getString(resultSet,"DESCRIPTION_A"));
                vo.setNameAr(getString(resultSet,"ATTACHMENT_NAME"));
                
                searchPage.addRecord(vo);
            }
            
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }
    }

    /**
     * Delete attachment.
     * 
     * @param attachmentId Attachment ID.
     * @return true if the attachment was deleted successfully.
     */
    public boolean delete(Long attachmentId) {
        // Debug query
        List params = new ArrayList();
        params.add(attachmentId);
        debugQuery(DELETE_ATTACHMENT, params);

        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(DELETE_ATTACHMENT);
            setQueryParameters(stm, params);
            return stm.executeUpdate() > 0;

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Get total number of attachments.
     * 
     * @param sequenceNo Attachment sequence number.
     * @return total number of attachments.
     */
    public int getAttachmentsCount(Long sequenceNo) {
        // Debug query
        List params = new ArrayList();
        params.add(sequenceNo);
        debugQuery(GET_ATTACHMENTS_COUNT, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            stm = getConnection().prepareStatement(GET_ATTACHMENTS_COUNT);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if (! rs.next()) {
                return 0;
            }
            
            return rs.getInt(1);

        } catch (Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
}