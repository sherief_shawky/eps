/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  13/05/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.StepNotificationDAO;
import ae.eis.ntf.vo.NotificationVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure step notifications data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class StepNotificationDAOImpl extends JdbcDataAccessObject 
                                     implements StepNotificationDAO {
    /*
     * JDBC SQL and DML statements.
     */
     
    /** Common INSERT statement used for all notifications. */
    private static final String INSERT_NTF_LOG
    = " INSERT INTO TF_STP_NOTIFICATION_LOGS"
    +   " (ID, SUBJECT, MESSAGE, LANGUAGE, REMARKS, IS_LOCKED, STATUS,"
    +   " STATUS_DATE, TRIALS, MOBILE, EMAIL, FAX, TRAFFIC_NO, APP_ID, BKT_ID,"
    +   " PLT_ID, DLC_ID, CTK_ID, TCK_ID, CREATION_DATE, UPDATE_DATE,"
    +   " MAIL_FROM, MAIL_FROM_ALIAS,TRS_ID,COURIER_NO,IS_ENCRYPTED,PRIORITY,REASON_TYPE)";
    
    /** Requester notification title. */
    private static final String REQUESTER_NTF_TITLE
    = " SELECT MESSAGE FROM TF_STP_MESSAGES"
    + " WHERE LAN_CODE = 'AR' AND CODE = 'EPS_NTF_001_TITLE'";

    /** Procedure status domain description. */
    private static final String PROCEDURE_STATUS
    = " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC("
    +                       "'TF_EPS_PROCEDURE_STATUS', PRD.STATUS)";
    
    /** Mail-From alias query. */
    private static final String MAIL_FROM_ALIAS
    = " SELECT MESSAGE FROM TF_STP_MESSAGES"
    + " WHERE LAN_CODE = 'AR'"
    +   " AND CODE = 'STP_EMAIL_FROM'";

    /** Send requester SMS notification DML. */
    private static final String NOTIFIY_REQUESTER_BY_SMS
    =   INSERT_NTF_LOG
    + " SELECT NTL_SEQ.NEXTVAL,"
    +        " (" + REQUESTER_NTF_TITLE + "),"
    +        " REPLACE(REPLACE(REPLACE((SELECT MESSAGE"
    +                                 " FROM TF_STP_MESSAGES"
    +                                 " WHERE LAN_CODE = 'AR'"
    +                                   " AND CODE = 'EPS_NTF_001_SMS'),"
    +                   " '<<procedureNo>>', PRD.ID),"
    +                   " '<<stepName>>', PRE.STEP_NAME_A),"
    +                   " '<<procedureStatus>>'," + PROCEDURE_STATUS + "),"
    +        " 'AR' AS LAN_CODE,"
    +        " NULL, 1, 1, SYSDATE, 0,"
    +        " PRD.REQUESTER_MOBILE_NO,"
    +        " NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, SYSDATE,"
    +        " SYSDATE, NULL, NULL, NULL, NULL, 1"
    +        " , (SELECT PRIORITY FROM TF_EPS_PROCEDURE_TEMPLATES WHERE ID = PRD.PTL_ID)," +
        NotificationVO.REASON_TYPE_EPS_WORKFLOW_MOVE_STEP
    + " FROM TF_EPS_PROCEDURES PRD,"
    +      " TF_EPS_PROCEDURE_STEPS PRE"
    + " WHERE PRE.PRD_ID = PRD.ID"
    +   " AND PRD.REQUESTER_MOBILE_NO IS NOT NULL"
    +   " AND PRE.ID = ?"; // Procedure step ID

    /** Send requester email notification DML. */
    private static final String NOTIFIY_REQUESTER_BY_EMAIL
    =   INSERT_NTF_LOG
    + " SELECT NTL_SEQ.NEXTVAL,"
    +        " (" + REQUESTER_NTF_TITLE + "),"
    +        " REPLACE(REPLACE(REPLACE((SELECT MESSAGE"
    +                                 " FROM TF_STP_MESSAGES"
    +                                 " WHERE LAN_CODE = 'AR'"
    +                                   " AND CODE = 'EPS_NTF_001_EMAIL'),"
    +                   " '<<procedureNo>>', PRD.ID),"
    +                   " '<<stepName>>', PRE.STEP_NAME_A),"
    +                   " '<<procedureStatus>>'," + PROCEDURE_STATUS + "),"
    +        " 'AR' AS LAN_CODE,"
    +        " NULL,1, 1, SYSDATE, 0, NULL,"
    +        " PRD.REQUESTER_EMAIL,"
    +        " NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,SYSDATE,SYSDATE,'notifications@rta.ae',"
    +        " (" + MAIL_FROM_ALIAS + "),"
    +        " NULL, NULL, 1"
    +        " , (SELECT PRIORITY FROM TF_EPS_PROCEDURE_TEMPLATES WHERE ID = PRD.PTL_ID)," +
        NotificationVO.REASON_TYPE_EPS_WORKFLOW_MOVE_STEP
    + " FROM TF_EPS_PROCEDURES PRD,"
    +      " TF_EPS_PROCEDURE_STEPS PRE"
    + " WHERE PRE.PRD_ID = PRD.ID"
    +   " AND PRD.REQUESTER_EMAIL IS NOT NULL"
    +   " AND PRE.ID = ?"; // Procedure step ID 
  
    /*
     * Methods
     */

    /**
     * Notifiy requester about the current procedure status.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    public int notifiyRequester(Long stepId) {
        // Notification sending flag
        int count = 0;
        
        // Send SMS notification
        List params = new ArrayList();
        params.add(stepId);
        debugQuery(NOTIFIY_REQUESTER_BY_SMS, params);
        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(NOTIFIY_REQUESTER_BY_SMS);
            setQueryParameters(stm, params);
            count += stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }

        // Send email notification
        debugQuery(NOTIFIY_REQUESTER_BY_EMAIL, params);
        stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(NOTIFIY_REQUESTER_BY_EMAIL);
            setQueryParameters(stm, params);
            count += stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }

        // Return number of notifications sent
        return count;
    }

    /**
     * Notifiy step users that a new procedure was received and requires action.
     * 
     * @param stepId Procedure step ID.
     * @param assignedToUserId Notification Assigned To User Id
     * @return Number of notifications sent.
     */
    public int notifiyStepUsers(Long stepId, Long assignedToUserId) {
        // Notification sending flag
        int count = 0;
        
        NamedQuery namedQueryBySms = null;
        NamedQuery namedQueryByEMail = getNamedQuery("ae.rta.eps.dao.jdbc."
                                    +"StepNotification.notifiyStepUsersByEMail");  
        PreparedStatement stm = null;
        
        if (assignedToUserId == null) {
            namedQueryBySms = getNamedQuery("ae.rta.eps.dao.jdbc."
                            +"StepNotification.notifiyStepUsersBySms");                
        } else {
            namedQueryBySms = getNamedQuery("ae.rta.eps.dao.jdbc."
                            +"StepNotification.notifiyAssignedUserBySms");
        }                    
              
        // Send SMS Notification                
        try {
            // Set Named Query Parameters
            namedQueryBySms.setParameter("stepId", stepId);
            
            // Debug Named Query
            debugQuery(namedQueryBySms.getSqlQuery(), 
                       namedQueryBySms.getSqlParameters());
        
            // Execute DML statement
            stm = prepareStatement(namedQueryBySms);
            count += stm.executeUpdate();

        } catch (Exception ex)  { 
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }

        // Send E-Mail Notification
        stm = null;
        try {
            // Set Named Query Parameters
            namedQueryByEMail.setParameter("stepId", stepId);
            
            // Debug Named Query
            debugQuery(namedQueryByEMail.getSqlQuery(),
                       namedQueryByEMail.getSqlParameters());
        
            // Execute DML statement
            stm = prepareStatement(namedQueryByEMail);
            count += stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }

        // Return number of notifications sent
        return count;
    }

    /**
     * Send step notifications.
     * 
     * @param stepId Procedure step ID.
     * @return Number of notifications sent.
     */
    public int sendStepNotifications(Long stepId) {
    
        // Notification sending flag
        int count = 0;
        
        NamedQuery namedQueryBySMS = getNamedQuery("ae.rta.eps.dao.jdbc."
                              + "StepNotification.sendStepNotificationsBySMS");  
        
        NamedQuery namedQueryRequesterBySms = getNamedQuery("ae.rta.eps.dao.jdbc."
                    +"StepNotification.sendRequesterStepNotificationsBySms");  
        
        NamedQuery namedQueryByEMAIL = getNamedQuery("ae.rta.eps.dao.jdbc."
                            +"StepNotification.sendStepNotificationsByEMAIL");  
        
        NamedQuery namedQueryRequesterByEmail = getNamedQuery("ae.rta.eps.dao."
                +"jdbc.StepNotification.sendRequesterStepNotificationsByEmail");  
        
        PreparedStatement stm = null;
        
        // Send SMS notification
        try {
            // Set Named Query Parameters
            namedQueryBySMS.setParameter("stepId", stepId);
            
            // Debug Named Query
            debugQuery(namedQueryBySMS.getSqlQuery(), 
                       namedQueryBySMS.getSqlParameters());     
                       
            // Execute DML statement
            stm = prepareStatement(namedQueryBySMS);
            count += stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }

        // Send Requester SMS notification
        stm = null;
        try {
            // Set Named Query Parameters
            namedQueryRequesterBySms.setParameter("stepId", stepId);
            
            // Debug Named Query
            debugQuery(namedQueryRequesterBySms.getSqlQuery(), 
                       namedQueryRequesterBySms.getSqlParameters());     
                       
            // Execute DML statement
            stm = prepareStatement(namedQueryRequesterBySms);
            count += stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
        
        // Send email notification
        stm = null;
        try {
            // Set Named Query Parameters
            namedQueryByEMAIL.setParameter("stepId", stepId);
            
            // Debug Named Query
            debugQuery(namedQueryByEMAIL.getSqlQuery(), 
                       namedQueryByEMAIL.getSqlParameters());             
        
            // Execute DML statement
            stm = prepareStatement(namedQueryByEMAIL);
            count += stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
        
        // Send requester email notification
        stm = null;
        try {
            // Set Named Query Parameters
            namedQueryRequesterByEmail.setParameter("stepId", stepId);
            
            // Debug Named Query
            debugQuery(namedQueryRequesterByEmail.getSqlQuery(), 
                       namedQueryRequesterByEmail.getSqlParameters());             
        
            // Execute DML statement
            stm = prepareStatement(namedQueryRequesterByEmail);
            count += stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        

        // Return number of notifications sent
        return count;
    }
}