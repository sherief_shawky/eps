/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateStepForwardDAO;
import ae.eis.eps.vo.TemplateStepForwardVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure template step foward data access object JDBC implementation calss.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepForwardDAOImpl extends JdbcDataAccessObject 
                                        implements TemplateStepForwardDAO {
    /*
     * JDBC SQL and DML statements.
     */
    
    /** Create procedure template step forward DML. */
    private static final String CREATE_STEP_FORWARD
    = " INSERT INTO TF_EPS_TEMP_STEP_FORWARDS"
    +  "(ACTION_TYPE,PTS_ID_NEXT_STEP,PTS_ID,CREATED_BY,CREATION_DATE)"
    +  " VALUES(?," // ACTION_TYPE
    +         " ?," // PTS_ID_NEXT_STEP
    +         " ?," // PTS_ID
    +         " ?," // CREATED_BY
    +         " SYSDATE)"; // CREATION_DATE

    /** Delete procedure template step forward. */
    private static final String DELETE_STEP_FORWARD
        =   " DELETE FROM TF_EPS_TEMP_STEP_FORWARDS WHERE PTS_ID = ? AND PTS_ID_NEXT_STEP = ? ";

    /** Delete procedure template step forward By Step Id */
    private static final String DELETE_STEP_FORWARD_BY_STEP_ID
        =   " DELETE FROM TF_EPS_TEMP_STEP_FORWARDS WHERE PTS_ID = ? ";
        
    /** Get Procedure Template Step Forwards */
    private static final String GET_STEP_FORWARD
        =   " SELECT NEXT_PTS.ID NEXT_ID, "
        +          " NEXT_PTS.STEP_NAME_A STEP_NAME," // STEP_NAME_A
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +           "('TF_EPS_STEP_TYPE',NEXT_PTS.STEP_TYPE) STEP_TYPE_DESC," //STEP_TYPE_DESC
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +           "('TF_EPS_ACTION_TYPE',TSF.ACTION_TYPE) ACTION_TYPE_DESC" // ACTION_TYPE_DESC
        +     " FROM TF_EPS_TEMP_STEP_FORWARDS TSF,"
        +          " TF_EPS_PROCEDURE_TEMP_STEPS PARENT_PTS,"
        +          " TF_EPS_PROCEDURE_TEMP_STEPS NEXT_PTS "
        +    " WHERE TSF.PTS_ID = PARENT_PTS.ID"
        +      " AND PARENT_PTS.ID = ? "
        +      " AND TSF.PTS_ID_NEXT_STEP = NEXT_PTS.ID";

    /** Get Sequence Number */
    private static final String GET_SEQUENCE_NUMBER 
        =   " SELECT MAX(SEQ_NO)+1 MAX_SEQ_NO"
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS "
        +    " WHERE SEQ_NO <> 999 "
        +      " AND PTL_ID = ? ";
        
    /** Has Rejected Forward */        
    private static final String HAS_REJECTED_FORWARD
        =   " SELECT 1 "
        +     " FROM TF_EPS_TEMP_STEP_FORWARDS "
        +    " WHERE PTS_ID = ? "
        +      " AND ACTION_TYPE = 2 ";
        
    /** Has Invalid Forward */        
    private static final String HAS_INVALID_FORWARDS
        =   " SELECT PTS.STEP_NAME_A  "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTS.PTL_ID = ? "
        +      " AND PTS.STEP_TYPE <> 5 "
        +      " AND NOT EXISTS(SELECT 1 FROM TF_EPS_TEMP_STEP_FORWARDS TSF "
        +                             " WHERE TSF.PTS_ID = PTS.ID )";
        
    /** Get Rejected Forward */        
    private static final String GET_REJECRED_FORWARDS        
        =   " SELECT COUNT(*), "
        +          " PTS.ID, "
        +          " PTS.STEP_NAME_A "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS, "
        +          " TF_EPS_TEMP_STEP_FORWARDS TSF "
        +    " WHERE TSF.PTS_ID = PTS.ID "
        +      " AND PTS.PTL_ID = ? "
        +      " AND PTS.STEP_TYPE <> 5 "
        +      " AND TSF.ACTION_TYPE = 2 "
        + " GROUP BY PTS.ID ,PTS.STEP_NAME_A "
        +   " HAVING COUNT(*) > 1 ";
        
    /** Is Valid Rejcted Forward */        
    private static final String GET_INVALID_REJECRED_FORWARDS        
        =   " SELECT PTS.ID , "
        +          " PTS.STEP_NAME_A "
        +    " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS, "
        +         " TF_EPS_TEMP_STEP_FORWARDS TSF "                    
        +   " WHERE PTS.ID = TSF.PTS_ID "
        +     " AND PTS.PTL_ID = ? "
        +     " AND TSF.ACTION_TYPE = 2 "
        +     " AND PTS.STEP_TYPE IN(2,3,4) "
        +" GROUP BY PTS.ID ,PTS.STEP_NAME_A ";
        
    /** Is Valid End Step Forward */        
    private static final String IS_VALID_END_STEP_FORWARDS
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTS.PTL_ID = ? "
        +      " AND PTS.STEP_TYPE IN(5) "
        +      " AND EXISTS(SELECT 1 FROM TF_EPS_TEMP_STEP_FORWARDS TSF "
        +                             " WHERE TSF.PTS_ID = PTS.ID )";
        
    /** Is Valid Forward */        
    private static final String IS_VALID_FORWARD
        =   " SELECT PARENT_PTS.STEP_TYPE PARENT_STEP_TYPE,"
		+          " PARENT_PTS.ID,"
		+          " NEXT_PTS.STEP_TYPE NEXT_STEP_TYPE ,"
		+          " NEXT_PTS.ID "
        +      "FROM TF_EPS_PROCEDURE_TEMP_STEPS PARENT_PTS,"
		+          " TF_EPS_PROCEDURE_TEMP_STEPS NEXT_PTS"
        +    " WHERE PARENT_PTS.PTL_ID = NEXT_PTS.PTL_ID"
        +      " AND PARENT_PTS.ID = ? "
        +      " AND NEXT_PTS.ID = ? "
        +      " AND PARENT_PTS.STEP_TYPE IN(4,5)"
        +      " AND NEXT_PTS.STEP_TYPE IN(4,5)" ;
    
    private static final String IS_EXIST_FORWARD
        =   " SELECT 1 FROM TF_EPS_TEMP_STEP_FORWARDS "
        +    " WHERE PTS_ID = ? AND PTS_ID_NEXT_STEP = ? "
        +    "    AND NOT EXISTS (SELECT ID "
        +		                  " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
		+			             " WHERE PTS_ID = PTS.ID) ";
    
    /** Find Procedure Template step */
    private static final String FIND_STEP_FORWARD
        =   " SELECT OTHER_PTS.ID OTHER_ID, "
        +          " OTHER_PTS.STEP_NAME_A OTHER_STEP_NAME, "
		+	       " OTHER_PTS.STEP_TYPE OTHER_STEP_TYPE, "
		+	       " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC"
        +          "('TF_EPS_STEP_TYPE',OTHER_PTS.STEP_TYPE) STEP_TYPE_DESC "
    	+     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS, "
	    + 	       " TF_EPS_PROCEDURE_TEMP_STEPS OTHER_PTS "
        +    " WHERE PTS.PTL_ID = OTHER_PTS.PTL_ID "
        +      " AND OTHER_PTS.ID <> PTS.ID "
        +      " AND PTS.ID = ? "
        +      " AND OTHER_PTS.STEP_TYPE <> 4 "
        +      " AND OTHER_PTS.ID NOT IN (SELECT PTS_ID_NEXT_STEP "
        +                                 " FROM TF_EPS_TEMP_STEP_FORWARDS "
        +                                " WHERE PTS_ID = PTS.ID "
        +                               " )";
        
    /** Is Valid Allowed Step */    
    private static final String IS_VALID_ALLOWED_STEP
        =   " SELECT PTS.ID , "
        +          " PTS.STEP_NAME_A "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTL_ID = ? "
        +      " AND STEP_TYPE = 1 "
        +      " AND EXISTS (SELECT 1 FROM TF_EPS_TEMP_STEP_ACTIONS "
        +                          " WHERE PTS_ID = PTS.ID "
        +                            " AND ACTION_TYPE = 2) "
        +      " AND NOT EXISTS (SELECT 1 FROM TF_EPS_TEMP_STEP_FORWARDS "
        +                              " WHERE PTS_ID = PTS.ID "
        +                                " AND ACTION_TYPE = 2) ";
            
    /** Get Non Approved Forward Step */            
    private static final String GET_NON_APPROVED_FORWARD_STEP
        = " SELECT ID,STEP_NAME_A"
        +   " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS"
        +  " WHERE PTL_ID = ?"
        +    " AND STEP_TYPE = 1"
        +    " AND EXISTS (SELECT 1 "
        +                  " FROM TF_EPS_TEMP_STEP_FORWARDS TSF"
        +                 " WHERE PTS_ID IN (PTS.ID)"
        +                   " AND ACTION_TYPE = 2) "
        +    " AND NOT EXISTS (SELECT 1 "
        +                    " FROM TF_EPS_TEMP_STEP_FORWARDS TSF"
        +                   " WHERE PTS_ID IN (PTS.ID)"
        +                     " AND ACTION_TYPE = 1) ";

                    
    /*
     * Methods
     */
    /**
     * Create new procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public TemplateStepForwardVO create(TemplateStepForwardVO vo) {
        // Debug query
        List params = new ArrayList();
        params.add(vo.getActionType());
        params.add(vo.getNextStep().getId());
        params.add(vo.getParentStep().getId());
        params.add(vo.getCreatedBy());
        debugQuery(CREATE_STEP_FORWARD, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_STEP_FORWARD);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Delete procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public void delete(TemplateStepForwardVO vo) {
        // Debug query
        List params = new ArrayList();
        params.add(vo.getParentStep().getId());
        params.add(vo.getNextStep().getId());        
        debugQuery(DELETE_STEP_FORWARD, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_FORWARD);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  { 
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Delete procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public void deleteByStepId(Long stepId) {
        // Debug query
        List params = new ArrayList();
        params.add(stepId);
    
        debugQuery(DELETE_STEP_FORWARD_BY_STEP_ID, params);

        PreparedStatement stm = null;
        try {
            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_FORWARD_BY_STEP_ID);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  { 
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Get procedure template step forward.
     * 
     * @param ptsId Procedure template step Id
     * @return Procedure template step forward value object created.
     */
    public SearchPageVO find(int pageNo,Long ptsId){
        StringBuffer sqlQuery = new StringBuffer(GET_STEP_FORWARD.toString());
        List listOfparams=new ArrayList();
        listOfparams.add(ptsId);
        debugQuery(sqlQuery, listOfparams);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(
                sqlQuery.toString(), listOfparams);

            prepStmt =doSearch(sqlQuery.toString(), listOfparams, pageNo, pageSize);
            resultSet=prepStmt .executeQuery();
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()) {
                TemplateStepForwardVO vo = new TemplateStepForwardVO();
                vo.setParentStep(new TemplateStepVO());

                vo.getParentStep().setId(getLong(resultSet,"NEXT_ID"));
                vo.setActionTypeDesc(getString(resultSet,"ACTION_TYPE_DESC"));
                vo.getParentStep().setNameAr(getString(resultSet,"STEP_NAME"));
                vo.getParentStep().setStepTypeDesc(getString(resultSet,"STEP_TYPE_DESC"));

                searchPage.addRecord(vo);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        } 
    }
    
    /**
     * Get Sequense Number
     * 
     * @return max of sequense number
     * @param Procedure Template Id
     */
    public Long getSequenseNo(Long ptlId){
        List listOfparams=new ArrayList();
        listOfparams.add(ptlId);
        debugQuery(GET_SEQUENCE_NUMBER.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_SEQUENCE_NUMBER);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            if(!rs.next()){
                return null;
            }
            Long maxSequense = getLong(rs,"MAX_SEQ_NO");
            // Max Sequense Number
            return maxSequense;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Has Rejected Forward
     * return TRUE if this step has rejected forward, otherwise return FALSE
     * 
     * @return boolean (true/false)
     * @param stepId
     */
    public boolean hasRejectedForward(Long stepId){
        List listOfparams=new ArrayList();
        listOfparams.add(stepId);
        debugQuery(HAS_REJECTED_FORWARD.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(HAS_REJECTED_FORWARD);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            return rs.next();

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }
    
    /**
     * Has Invalid Forwards
     * The step must have defined next allowed steps
     * -  EPS_PTT_007
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidForwards(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(HAS_INVALID_FORWARDS.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List description = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(HAS_INVALID_FORWARDS);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            while(rs.next()){
                description.add(getString(rs,"STEP_NAME_A"));
            }
            
            return description;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }
    
    /**
     * Is Valid Forward
     * Cannot transfer from begin to end
     * -  EPS_TSF_005
     * 
     * @return boolean (true/false)
     * @param parentStepId
     * @param nextStepId
     */
    public boolean isValidForward(Long parentStepId,Long nextStepId){
        List listOfparams=new ArrayList();
        listOfparams.add(parentStepId);
        listOfparams.add(nextStepId);
        debugQuery(IS_VALID_FORWARD.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(IS_VALID_FORWARD);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
                  
            if(!rs.next()){
                return true;
            }
                        
            return false;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
    
    /**
     * Is Exist Forward
     * Cannot transfer to same step twice
     * -  EPS_TSF_006
     * 
     * @return boolean (true/false)
     * @param nextStepId
     * @param parentStepId
     */
    public boolean isForwardExist(Long parentStepId,Long nextStepId){
        List listOfparams=new ArrayList();
        listOfparams.add(parentStepId);
        listOfparams.add(nextStepId);
        debugQuery(IS_EXIST_FORWARD.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(IS_EXIST_FORWARD);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
                  
            if(rs.next()){
                return true;
            }
                        
            return false;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }           
    }
    
    /**
     * Find Procedure Forword Steps
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findForwardSteps(int pageNo,TemplateStepVO stepVO){
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        StringBuffer sqlQuery = new StringBuffer(FIND_STEP_FORWARD);
        ArrayList listOfparams = new ArrayList();
        listOfparams.add(stepVO.getId());
        
        if(stepVO.getNameAr() != null ){
            sqlQuery.append(" AND UPPER(OTHER_PTS.STEP_NAME_A) LIKE UPPER(?) ");
            listOfparams.add("%"+stepVO.getNameAr()+"%");
        }
        
        sqlQuery.append(" ORDER BY OTHER_PTS.SEQ_NO ");
        
        debugQuery(sqlQuery, listOfparams);
        
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery.toString() , 
                                                            listOfparams);
            prepStmt = doSearch(sqlQuery.toString(), listOfparams, pageNo , 
                                                                pageSize);
            resultSet=prepStmt.executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()){                
                TemplateStepVO vo = new TemplateStepVO();
                vo.setId(getLong(resultSet,"OTHER_ID"));
                vo.setNameAr(getString(resultSet,"OTHER_STEP_NAME"));
                vo.setStepType(getInteger(resultSet,"OTHER_STEP_TYPE"));
                vo.setStepTypeDesc(getString(resultSet,"STEP_TYPE_DESC"));
        
                searchPage.addRecord(vo);
            }
            
            return searchPage;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }        
    }    
    
    
    /**
     * Get Rejected Step Forwards
     * The step cannot have more than one allowed next step with action type reject
     *  - EPS_TSF_002
     * 
     * @return Number of Rejected forwards
     * @param templateId Template Id
     */
    public List getRejectedStepForwards(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(GET_REJECRED_FORWARDS.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List list = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_REJECRED_FORWARDS);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            while(rs.next()){
                TemplateStepVO vo = new TemplateStepVO();
                vo.setNameAr(getString(rs,"STEP_NAME_A"));
                vo.setId(getLong(rs,"ID"));
                list.add(vo);
            }
            
            return list;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
    
    /**
     * Is Valid Rejected Forward
     *  - EPS_TSF_003
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public List getInValidRejectedForwardSteps(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(GET_INVALID_REJECRED_FORWARDS.toString(), listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List list = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_INVALID_REJECRED_FORWARDS);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            while(rs.next()){
                TemplateStepVO vo = new TemplateStepVO();
                vo.setNameAr(getString(rs,"STEP_NAME_A"));
                vo.setId(getLong(rs,"ID"));
                list.add(vo);
            }
            
            return list;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }
    
    /**
     * Is Valid End Step Forward
     * Cannot add allowed nextr step for step has step type end
     *  - EPS_TSF_004
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidEndStepForward(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(IS_VALID_END_STEP_FORWARDS, listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        int count = 0;
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(IS_VALID_END_STEP_FORWARDS);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            if(rs.next()){
                count = getInteger(rs,"COUNT").intValue();
            }
            
            return count == 0;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }

    /**
     * Is Valid Allowed Step
     * Must Define step forward with action type reject for step type human 
     *              action and has allowed action which is reject
     * -  EPS_TSF_007
     * 
     * @return boolean (true / false)
     * @param templateId
     */
    public List getInValidAllowedStep(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(IS_VALID_ALLOWED_STEP, listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List list = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(IS_VALID_ALLOWED_STEP);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            while(rs.next()){
                TemplateStepVO vo = new TemplateStepVO();
                vo.setNameAr(getString(rs,"STEP_NAME_A"));
                vo.setId(getLong(rs,"ID"));
                list.add(vo);
            }
            
            return list;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Get Non Approved Forward Steps
     * cannot activate procedure if there is forward step without approved forwards
     *  - EPS_TSF_009
     * 
     * @return list of non approved forward steps
     * @param templateId template Id
     */
    public List getNonApprovedForwardSteps(Long templateId){
        List listOfparams=new ArrayList();
        listOfparams.add(templateId);
        debugQuery(GET_NON_APPROVED_FORWARD_STEP, listOfparams);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        List list = new ArrayList();
        
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_NON_APPROVED_FORWARD_STEP);
            setQueryParameters(stm,listOfparams);
            rs = stm.executeQuery();
            
            while(rs.next()){
                TemplateStepVO vo = new TemplateStepVO();
                vo.setNameAr(getString(rs,"STEP_NAME_A"));
                vo.setId(getLong(rs,"ID"));
                list.add(vo);
            }
            
            return list;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
}