/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 * 1.01  Ali Abdel-Aziz     06/01/2010  - Altered getRequesterStepFields method 
 *                                        to retrive the following extra columns:
 *                                        LabelWidth, SingleColumnFieldWidth, 
 *                                        and MultiColumnFieldWidth.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.ProcedureStepFieldDAO;
import ae.eis.eps.vo.ProcedureFieldVO;
import ae.eis.eps.vo.ProcedureStepFieldGroupVO;
import ae.eis.eps.vo.ProcedureStepFieldVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure step-field data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepFieldDAOImpl extends JdbcDataAccessObject  
                                       implements ProcedureStepFieldDAO {
    /*
     * JDBC SQL and DML statements.
     */
    
    /** Get active procedure step fields. */
    private static final String GET_ACTIVE_STEP_FIELDS
    = " SELECT PRF.ID AS FIELD_ID,"
    +        " PRF.CODE,"
    +        " PRF.LABLE,"
    +        " PRF.VALUE,"
    +        " PRF.FIELD_TYPE,"
    +        " PRF.FIELD_SIZE,"
    +        " PRF.HAS_RELATED_OPTIONS,"
    +        " SFL.ORDER_NO,"
    +        " SFL.COLUMN_SPAN,"
    +        " SFL.IS_MANDATORY,"
    +        " SFL.IS_EDITABLE,"
    +        " SFL.VIEW_IN_NEW_LINE,"
    +        " SFL.ROWS_NO,"
    +        " SFL.PRE_ID AS STEP_ID,"
    +        " SFL.SFG_ID,"
    +        " SFG.NAME_A AS GROUP_NAME_A"
    + " FROM TF_EPS_PROCEDURE_FIELDS PRF,"
    +      " TF_EPS_STEP_FIELDS SFL,"
    +      " TF_EPS_PROCEDURE_STEPS PRE,"
    +      " TF_EPS_PROCEDURES PRD,"
    +      " TF_EPS_STEP_FIELD_GROUPS SFG"
    + " WHERE SFL.PRE_ID = PRE.ID"
    +   " AND SFL.SFG_ID = SFG.ID"
    +   " AND SFL.PRF_ID = PRF.ID"
    +   " AND PRF.PRD_ID = PRE.PRD_ID"
    +   " AND PRF.PRD_ID = PRD.ID"
    +   " AND PRE.PRD_ID = PRD.ID"
    +   " AND PRD.ACTIVE_STEP_SEQ_NO = PRE.SEQ_NO"
    +   " AND PRE.PRD_ID = ?" // Procedure ID
    + " ORDER BY SFG.ORDER_NO, SFL.ORDER_NO";

    /*
     * Methods
     */

    /**
     * Get requester step fields.
     * 
     * @param procedureId procedure ID.
     * @return requester step fields.
     */
    public List getRequesterStepFields(Long procedureId) {
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureStepGroupField.getRequesterStepFields");
        query.setParameter("procedureId", procedureId);
        // Debug query
        debugQuery(query);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = prepareStatement(query);
            rs = stm.executeQuery();

            List groupsList = new ArrayList();
            ProcedureStepFieldGroupVO groupVO = null;
            while (rs.next()) {
                // Add new group to groups list
                Long groupId = getLong(rs, "SFG_ID");
                if (groupVO == null || groupVO.getId() == null || 
                  ! groupVO.getId().equals(groupId)) {
                    //  Add new group to groups list
                    groupVO = new ProcedureStepFieldGroupVO();
                    groupsList.add(groupVO);

                    groupVO.setId(groupId);
                    groupVO.setNameAr(rs.getString("GROUP_NAME_A"));
                    groupVO.setLabelWidth(getInteger(rs, "LABEL_WIDTH"));
                    groupVO.setSingleColumnFieldWidth(getInteger(rs, "SINGLE_COLUMN_FIELD_WIDTH"));
                    groupVO.setMultiColumnFieldWidth(getInteger(rs, "MULTI_COLUMN_FIELD_WIDTH"));
                }

                ProcedureStepFieldVO vo = new ProcedureStepFieldVO();
                groupVO.addField(vo);
                vo.setField(new ProcedureFieldVO());
                vo.setProcedureStep(new ProcedureStepVO());

                vo.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
                vo.setIsEditable(getInteger(rs, "IS_EDITABLE"));
                vo.setOrderNo(getInteger(rs, "ORDER_NO"));
                vo.setColumnSpan(getInteger(rs, "COLUMN_SPAN"));
                vo.setNoOfRows(getInteger(rs, "ROWS_NO"));
                vo.setViewInNewLine(getInteger(rs,"VIEW_IN_NEW_LINE"));
                 
                vo.getField().setId(getLong(rs, "FIELD_ID"));
                vo.getField().setCode(getInteger(rs, "CODE"));
                vo.getField().setLabel(getString(rs, "LABLE"));
                vo.getField().setValue(rs.getString("VALUE"));
                vo.getField().setType(getInteger(rs, "FIELD_TYPE"));
                vo.getField().setSize(getInteger(rs, "FIELD_SIZE"));

                vo.getField().setHasRelatedOptions(
                    getInteger(rs, "HAS_RELATED_OPTIONS"));

                vo.getProcedureStep().setId(getLong(rs, "STEP_ID"));
            }

            return groupsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get active procedure step fields.
     * 
     * @param procedureId procedure ID.
     * @return active procedure step fields.
     */
    public List getActiveStepFields(Long procedureId) {
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.ProcedureStepGroupField.getActiveStepFields");
        query.setParameter("procedureId", procedureId);
        // Debug query
        debugQuery(query);
        
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = prepareStatement(query);
            rs = stm.executeQuery();

            List groupsList = new ArrayList();
            ProcedureStepFieldGroupVO groupVO = null;
            while (rs.next()) {
                // Add new group to groups list
                Long groupId = getLong(rs, "SFG_ID");
                if (groupVO == null || groupVO.getId() == null || 
                  ! groupVO.getId().equals(groupId)) {
                    //  Add new group to groups list
                    groupVO = new ProcedureStepFieldGroupVO();
                    groupsList.add(groupVO);

                    groupVO.setId(groupId);
                    groupVO.setNameAr(rs.getString("GROUP_NAME_A"));
                    groupVO.setLabelWidth(getInteger(rs, "LABEL_WIDTH"));
                    groupVO.setSingleColumnFieldWidth(getInteger(rs, "SINGLE_COLUMN_FIELD_WIDTH"));
                    groupVO.setMultiColumnFieldWidth(getInteger(rs, "MULTI_COLUMN_FIELD_WIDTH"));
                }

                ProcedureStepFieldVO vo = new ProcedureStepFieldVO();
                groupVO.addField(vo);
                vo.setField(new ProcedureFieldVO());
                vo.setProcedureStep(new ProcedureStepVO());

                vo.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
                vo.setIsEditable(getInteger(rs, "IS_EDITABLE"));
                vo.setColumnSpan(getInteger(rs, "COLUMN_SPAN"));
                vo.setNoOfRows(getInteger(rs, "ROWS_NO"));
                vo.setOrderNo(getInteger(rs, "ORDER_NO"));
                vo.setViewInNewLine(getInteger(rs,"VIEW_IN_NEW_LINE"));

                vo.getField().setId(getLong(rs, "FIELD_ID"));
                vo.getField().setCode(getInteger(rs, "CODE"));
                vo.getField().setLabel(getString(rs, "LABLE"));
                vo.getField().setValue(getString(rs, "VALUE"));
                vo.getField().setValueDesc(getString(rs, "VALUE_DESC"));
                vo.getField().setType(getInteger(rs, "FIELD_TYPE"));
                vo.getField().setSize(getInteger(rs, "FIELD_SIZE"));

                vo.getField().setHasRelatedOptions(
                    getInteger(rs, "HAS_RELATED_OPTIONS"));
                 
                vo.getProcedureStep().setId(getLong(rs, "STEP_ID"));
            }

            return groupsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
}