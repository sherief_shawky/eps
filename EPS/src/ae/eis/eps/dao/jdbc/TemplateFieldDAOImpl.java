/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateFieldDAO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.SearchPageVO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Template step-field data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateFieldDAOImpl extends JdbcDataAccessObject  
                                  implements TemplateFieldDAO {
    /*
     * JDBC SQL and DML statements.
     */

    /** Find Step Field Query */
    private static final String FIND_STEP_FIELDS
        =   " SELECT TPE.ID,"
	   	+          " TPE.CODE,"
	   	+          " TPE.LABLE,"
	   	+          " TPE.FIELD_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC("
        +          "'TF_EPS_FIELD_TYPE',TPE.FIELD_TYPE) FIELD_TYPE_DESC," 
        +          " (SELECT NAME_A FROM TF_EPS_TEMPLATE_FIELD_OPTIONS TFO "
        +                        " WHERE TFO.VALUE = TPE.VALUE "
        +                          " AND TFO.TPE_ID = TPE.ID) AS VALUE_DESC,"
	   	+          " TPE.FIELD_SIZE,TPE.MAX_ROW_NO "
        +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE"
        +    " WHERE TPE.PTL_ID = ? ";
        
    
    /** Get Field By Id Query */        
    private static final String GET_FIELD_BY_ID
    =   " SELECT ID,"
    +          " CODE,"
    +          " LABLE,"
    +          " FIELD_TYPE,"
    +          " VALUE,"
    +          " FIELD_SIZE,"
    +          " MAX_ROW_NO " 
    +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS "
    +     " WHERE ID = ? ";
    
    /** Create Field */        
    private static final String CREATE_FIELD
        =   " INSERT INTO TF_EPS_TEMP_PROCEDURE_FIELDS"
		+       "(ID,CODE,LABLE,FIELD_TYPE,VALUE,FIELD_SIZE,MAX_ROW_NO,PTL_ID,CREATED_BY,"
		+       "UPDATE_DATE,UPDATED_BY,CREATION_DATE) "
		+        "VALUES(?,"   // ID
        +               "?,"                // CODE
        +               "?,"                // LABLE
        +               "?,"                // FIELD_TYPE
        +               "?,"                // VALUE
        +               "?,"                // FIELD_SIZE
        +               "?,"                // MAX_ROW_NO
        +               "?,"                // PTL_ID
        +               "?,"                // CREATED_BY
        +               "SYSDATE,"          // UPDATED_DATE
        +               "?,"                // UPDATED_BY
        +               "SYSDATE)";          // CREATION_DATE

        
    /** Update Field */        
    private static final String UPDATE_FIELD
        =   " UPDATE TF_EPS_TEMP_PROCEDURE_FIELDS "
        +      " SET CODE = ?,"                 
        +          " LABLE = ?,"
        +          " FIELD_TYPE = ?,"
        +          " VALUE = ?,"
        +          " FIELD_SIZE = ?,"
        +          " MAX_ROW_NO = ?,"
        +          " UPDATE_DATE = SYSDATE,"
        +          " UPDATED_BY = ? "
        +    " WHERE ID = ?	" ;     
    
    /** Delete Field */    
    private static final String DELETE_FIELD
        =   " DELETE FROM TF_EPS_TEMP_PROCEDURE_FIELDS WHERE ID = ? ";
        
    /** Is Exist Query */        
    private static final String IS_EXIST_CODE
    ="SELECT 1 FROM TF_EPS_TEMP_PROCEDURE_FIELDS WHERE PTL_ID = ? AND CODE = ?";
    
    /** Is Order Query */        
    private static final String IS_EXIST_ORDER
    ="SELECT 1 FROM TF_EPS_TEMP_PROCEDURE_FIELDS WHERE PTL_ID = ? AND ORDER_NO = ?";
        
    /** Is Label Query */        
    private static final String IS_EXIST_LABEL
    ="SELECT 1 FROM TF_EPS_TEMP_PROCEDURE_FIELDS WHERE PTL_ID = ? AND LABLE = ?";        
        
    /** Is Linked to Step Query */        
    private static final String IS_LINKED_TO_STEP
        =   " SELECT 1 FROM TF_EPS_TEMP_STEP_FIELDS WHERE TPE_ID = ? " ; 

    /** Delete Field Option Field */
    private static final String DELETE_FIELD_OPTIONS
        =   " DELETE FROM TF_EPS_TEMPLATE_FIELD_OPTIONS WHERE TPE_ID = ? ";
    
    /** Is Valid ComboBox Field */
    private static final String IS_VALID_COMBO_BOX_FIELD
        =   " SELECT TPE.LABLE "
        +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE "
        +    " WHERE PTL_ID = ? "
        +      " AND FIELD_TYPE = 4 "
        +      " AND NOT EXISTS(SELECT 1 FROM TF_EPS_TEMPLATE_FIELD_OPTIONS "
        +                             " WHERE TPE_ID = TPE.ID) ";        
        
    /** Get Field By Report Id */        
    private static final String GET_BY_REPORT_ID 
        = " SELECT TPE.ID, "
        +        " TPE.CODE, "
        +        " TPE.LABLE, "
        +        " TPE.FIELD_TYPE, "
        +        " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC("
        +               " 'TF_EPS_FIELD_TYPE',TPE.FIELD_TYPE) FIELD_TYPE_DESC "
        +   " FROM TF_EPS_TEMPLATE_REPORTS ETR,"
        +        " TF_EPS_TEMP_PROCEDURE_FIELDS TPE "
        +  " WHERE TPE.PTL_ID = ETR.PTL_ID "
        +    " AND ETR.ID = ? "
        +    " AND NOT EXISTS(SELECT 1 "
        +                     " FROM TF_EPS_REPORT_PARAMETERS ERF "
        +                    " WHERE ERF.ETR_ID = ETR.ID "
        +                      " AND ERF.TPE_ID = TPE.ID ) ";
        
    /** Find parent options fields. */
    private static final String FIND_PARENT_OPTION_FIELDS
    = " SELECT TPE.ID,"
    +        " TPE.LABLE,"
    +        " TPE.CODE"
    + " FROM TF_EPS_TEMP_PROCEDURE_FIELDS DEPENDENT_TPE,"
    +      " TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
    +      " TF_EPS_PROCEDURE_TEMPLATES PTL"
    + " WHERE DEPENDENT_TPE.PTL_ID = PTL.ID"
    +   " AND TPE.PTL_ID = PTL.ID"
    +   " AND TPE.ID <> DEPENDENT_TPE.ID"
    +   " AND DEPENDENT_TPE.FIELD_TYPE = 4"
    +   " AND TPE.FIELD_TYPE = 4"
    +   " AND DEPENDENT_TPE.ID = ?"; // Dependent Filed ID

    /** Get parent template fields ID's. */
    private static final String GET_PARENT_FIELDS_IDS
    = " SELECT DISTINCT TFO.RELATED_TPE_ID"
    + " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
    +      " TF_EPS_TEMPLATE_FIELD_OPTIONS TFO"
    + " WHERE TFO.TPE_ID = TPE.ID"
    +   " AND TPE.FIELD_TYPE = 4"
    +   " AND TFO.RELATED_ORDER_NO IS NOT NULL"
    +   " AND TPE.PTL_ID = ?"; // Template ID

    /** Update fields with options dependency. */
    private static final String UPDATE_RELATED_OPTION_FIELDS
    = " UPDATE TF_EPS_TEMP_PROCEDURE_FIELDS PARENT_TPE"
    + " SET PARENT_TPE.UPDATE_DATE = SYSDATE,"
    +     " PARENT_TPE.UPDATED_BY = ?," // Username
    +     " PARENT_TPE.HAS_RELATED_OPTIONS = 2"
    + " WHERE PARENT_TPE.FIELD_TYPE = 4"
    +   " AND PARENT_TPE.PTL_ID = ?" // Template ID
    +   " AND PARENT_TPE.ID IN(" + GET_PARENT_FIELDS_IDS + ")"; // Template ID

    /** Update fields with no options dependency. */
    private static final String UPDATE_UNRELATED_OPTION_FIELDS
    = " UPDATE TF_EPS_TEMP_PROCEDURE_FIELDS PARENT_TPE"
    + " SET PARENT_TPE.UPDATE_DATE = SYSDATE,"
    +     " PARENT_TPE.UPDATED_BY = ?," // Username
    +     " PARENT_TPE.HAS_RELATED_OPTIONS = 1"
    + " WHERE PARENT_TPE.FIELD_TYPE = 4"
    +   " AND PARENT_TPE.PTL_ID = ?" // Template ID
    +   " AND PARENT_TPE.ID NOT IN(" + GET_PARENT_FIELDS_IDS + ")"; // Template ID

    /*
     * Methods
     */
    
    /** Find remaining search fields */
    private static final String FIND_REMAINING_SEARCH_FIELDS
        =   " SELECT TPE.ID,"
	   	+          " TPE.CODE,"
	   	+          " TPE.LABLE,"
	   	+          " TPE.FIELD_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC("
        +          "'TF_EPS_FIELD_TYPE',TPE.FIELD_TYPE) FIELD_TYPE_DESC," 
        +          " (SELECT NAME_A FROM TF_EPS_TEMPLATE_FIELD_OPTIONS TFO "
        +                        " WHERE TFO.VALUE = TPE.VALUE "
        +                          " AND TFO.TPE_ID = TPE.ID) AS VALUE_DESC,"
	   	+          " TPE.FIELD_SIZE "
        +    " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE"
        +    " WHERE TPE.PTL_ID = ? "
        +    " AND TPE.ID NOT IN(SELECT TPE_ID "
                                        +" FROM TF_EPS_SEARCH_FIELDS SFI,"
                                        +" TF_EPS_TEMP_PROCEDURE_FIELDS TPE2"
                                        +" WHERE SFI.TPE_ID = TPE2.ID"
                                        +" AND TPE2.PTL_ID = TPE.PTL_ID) "
        +    "ORDER BY TPE.CODE ";
        
    /** Find remaining search result fields */
    private static final String FIND_REMAINING_SEARCH_RESULT_FIELDS
        =   " SELECT TPE.ID,"
	   	+          " TPE.CODE,"
	   	+          " TPE.LABLE,"
	   	+          " TPE.FIELD_TYPE,"
        +          " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC("
        +          "'TF_EPS_FIELD_TYPE',TPE.FIELD_TYPE) FIELD_TYPE_DESC," 
        +          " (SELECT NAME_A FROM TF_EPS_TEMPLATE_FIELD_OPTIONS TFO "
        +                        " WHERE TFO.VALUE = TPE.VALUE "
        +                          " AND TFO.TPE_ID = TPE.ID) AS VALUE_DESC,"
	   	+          " TPE.FIELD_SIZE "
        +    " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE"
        +    " WHERE TPE.PTL_ID = ? "
        +    " AND TPE.ID NOT IN(SELECT TPE_ID "
                                        +" FROM TF_EPS_SEARCH_RESULTS SRL,"
                                        +" TF_EPS_TEMP_PROCEDURE_FIELDS TPE2"
                                        +" WHERE SRL.TPE_ID = TPE2.ID"
                                        +" AND TPE2.PTL_ID = TPE.PTL_ID) "
        +    "ORDER BY TPE.CODE ";
        
        
    /** Is Field Related To Active Procedure */
    private static final String IS_FIELD_RELATED_TO_ACTIVE_PROCEDURE 
        = " SELECT COUNT(*) COUNT "
        +   " FROM TF_EPS_PROCEDURES PRD, "
        +        " TF_EPS_PROCEDURE_TEMPLATES PTL, "
        +        " TF_EPS_PROCEDURE_FIELDS PRF "
        +  " WHERE PRD.PTL_ID = PTL.ID "
        +    " AND PTL.ID = ? "
        +    " AND PRD.STATUS = 2 "
        +    " AND PRF.PRD_ID = PRD.ID "
        +    " AND PRF.CODE = ? ";
    
    /**
     * Find remaining search result fields
     * 
     * @return Search Page Value Object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO findRemainingSearchResultFields(int pageNo,Long templateId){
        
        List params=new ArrayList();
        params.add(templateId);
                
        debugQuery(FIND_REMAINING_SEARCH_RESULT_FIELDS, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(FIND_REMAINING_SEARCH_RESULT_FIELDS, params);
            prepStmt =doSearch(FIND_REMAINING_SEARCH_RESULT_FIELDS, params, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()) {
                TemplateFieldVO templateFieldVO = new TemplateFieldVO();
                templateFieldVO.setId(getLong(resultSet,"ID"));
                templateFieldVO.setCode(getInteger(resultSet,"CODE"));
                templateFieldVO.setLabel(getString(resultSet,"LABLE"));
                templateFieldVO.setType(getInteger(resultSet,"FIELD_TYPE"));
                templateFieldVO.setTypeDesc(getString(resultSet,"FIELD_TYPE_DESC"));
                templateFieldVO.setValueDesc(getString(resultSet,"VALUE_DESC"));
                templateFieldVO.setSize(getInteger(resultSet,"FIELD_SIZE"));

                searchPage.addRecord(templateFieldVO);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        } 
        
    }
    
    /**
     * Find remaining search fields
     * 
     * @return Search Page Value Object
     * @param templateId template ID
     * @param pageNo page number
     */
    public SearchPageVO findRemainingSearchFields(int pageNo,Long templateId){
        
        List params=new ArrayList();
        params.add(templateId);
                
        debugQuery(FIND_REMAINING_SEARCH_FIELDS, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(FIND_REMAINING_SEARCH_FIELDS, params);
            prepStmt =doSearch(FIND_REMAINING_SEARCH_FIELDS, params, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()) {
                TemplateFieldVO templateFieldVO = new TemplateFieldVO();
                templateFieldVO.setId(getLong(resultSet,"ID"));
                templateFieldVO.setCode(getInteger(resultSet,"CODE"));
                templateFieldVO.setLabel(getString(resultSet,"LABLE"));
                templateFieldVO.setType(getInteger(resultSet,"FIELD_TYPE"));
                templateFieldVO.setTypeDesc(getString(resultSet,"FIELD_TYPE_DESC"));
                templateFieldVO.setValueDesc(getString(resultSet,"VALUE_DESC"));
                templateFieldVO.setSize(getInteger(resultSet,"FIELD_SIZE"));

                searchPage.addRecord(templateFieldVO);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }    
    }
    
    /**
     * Find Step Fields
     * 
     * @return Search Page Value Object
     * @param stepId Step Id
     * @param templateId template Id
     * @param pageNo pagination page Number
     */
    public SearchPageVO find(int pageNo,Long templateId,Long stepId){
        
        String sqlQuery = FIND_STEP_FIELDS;
        String subQuery = " AND NOT EXISTS (SELECT 1 FROM TF_EPS_TEMP_STEP_FIELDS "
             			+ " WHERE TPE_ID = TPE.ID "
                        + " AND PTS_ID = ? )";
                        
        List params=new ArrayList();
        params.add(templateId);
        if(stepId != null){
            sqlQuery += subQuery; 
            params.add(stepId);    
        }
        
        sqlQuery += " ORDER BY TPE.CODE ";
                
        debugQuery(sqlQuery, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(sqlQuery, params);
            prepStmt =doSearch(sqlQuery, params, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()) {
                TemplateFieldVO templateFieldVO = new TemplateFieldVO();
                templateFieldVO.setId(getLong(resultSet,"ID"));
                templateFieldVO.setCode(getInteger(resultSet,"CODE"));
                templateFieldVO.setLabel(getString(resultSet,"LABLE"));
                templateFieldVO.setType(getInteger(resultSet,"FIELD_TYPE"));
                templateFieldVO.setTypeDesc(getString(resultSet,"FIELD_TYPE_DESC"));
                templateFieldVO.setValueDesc(getString(resultSet,"VALUE_DESC"));
                templateFieldVO.setSize(getInteger(resultSet,"FIELD_SIZE"));
                templateFieldVO.setMaxRowNo(getInteger(resultSet,"MAX_ROW_NO"));
//                templateFieldVO.setColumnSpan(getInteger(resultSet,"COLUMN_SPAN"));
//                templateFieldVO.setColumnSpanDesc(getString(resultSet,"COLUMN_SPAN_DESC"));

                searchPage.addRecord(templateFieldVO);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        } 
        
    }
    
    /**
     * get Field By Template Id
     * 
     * @return Template Field Value Object
     * @param fieldId field Id
     */
    public TemplateFieldVO getById(Long fieldId){
        ArrayList params = new ArrayList();
        params.add(fieldId);
        debugQuery(GET_FIELD_BY_ID, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(GET_FIELD_BY_ID);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return null ;
            } 
            TemplateFieldVO templateFieldVO = new TemplateFieldVO();         
            templateFieldVO.setId(getLong(resultSet,"ID"));
            templateFieldVO.setCode(getInteger(resultSet,"CODE"));
            templateFieldVO.setLabel(getString(resultSet,"LABLE"));
            templateFieldVO.setType(getInteger(resultSet,"FIELD_TYPE"));
            templateFieldVO.setValue(getString(resultSet,"VALUE"));
            templateFieldVO.setSize(getInteger(resultSet,"FIELD_SIZE"));
            templateFieldVO.setMaxRowNo(getInteger(resultSet,"MAX_ROW_NO"));
        
            return templateFieldVO;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }
    
    
    /**
     * create Field
     * 
     * @param TemplateStepFieldVO Template Field Value Object
     */
    public TemplateFieldVO create(TemplateFieldVO vo){
        PreparedStatement stm = null;
        
        Long id = generateSequence("TPE_SEQ");
        
        // Debug query
        List params = new ArrayList();
        params.add(id);
        params.add(vo.getCode());
        params.add(vo.getLabel());
        params.add(vo.getType());
        params.add(vo.getValue());
        params.add(vo.getSize());
        params.add(vo.getMaxRowNo());
        params.add(vo.getProcedureTemplate().getId());
        params.add(vo.getCreatedBy());
        params.add(vo.getUpdatedBy());
//        params.add(vo.getColumnSpan());
    
        debugQuery(CREATE_FIELD, params);
            
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_FIELD);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            // return updated value object
            vo.setId(id);
            return vo;
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * update Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void update(TemplateFieldVO vo){
        PreparedStatement stm = null;
        
        // Debug query
        List params = new ArrayList();             
        
        params.add(vo.getCode());
        params.add(vo.getLabel());
        params.add(vo.getType());
        params.add(vo.getValue());
        params.add(vo.getSize());
        params.add(vo.getMaxRowNo());
        params.add(vo.getUpdatedBy());
        params.add(vo.getId());
        
        debugQuery(UPDATE_FIELD, params);
            
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_FIELD);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if (count <= 0) {
                throw new DataAccessException(" Cannot Update Field : " 
                                               + vo.getId());
            }
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * delete Field 
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    public void delete(Long fieldId){
        
        // delete field option in case of List Field
        deleteOptions(fieldId);
        
        PreparedStatement stm = null;
        
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        debugQuery(DELETE_FIELD, params);
            
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_FIELD);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * delete Field Options
     * 
     * @param TemplateFieldVO Template Field Value Object
     */    
    private void deleteOptions(Long fieldId){
        PreparedStatement stm = null;
        
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        debugQuery(DELETE_FIELD_OPTIONS, params);
            
        try {            

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_FIELD_OPTIONS);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * Is Exist Code
     *  - EPS_TPE_007 
     *  The code must be unique for the procedure
     *  
     * used to check if field code exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param templateId template Id
     * @param code code
     */
    public boolean isExistCode(Long fieldId,Long templateId,Integer code){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_EXIST_CODE);
        params.add(templateId);
        params.add(code);
        if(fieldId != null && !"".equals(fieldId)){
            sql.append(" AND ID <> ? ");
            params.add(fieldId);
        }
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Is Exist Order
     *  - EPS_TPE_008 : The order must be unique for the procedure
     *  
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param templateId template Id
     * @param order order
     */
    public boolean isExistOrder(Long fieldId,Long templateId,Integer order){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_EXIST_ORDER);        
        params.add(templateId);
        params.add(order);
        if(fieldId != null && !"".equals(fieldId)){
            sql.append(" AND ID <> ? ");
            params.add(fieldId);
        }     
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }            
    }
    
    /**
     * Is Exist Label
     *  - EPS_TPE_009 : The label must be unique for the procedure
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param templateId template Id
     * @param label label
     */
    public boolean isExistLabel(Long fieldId,Long templateId,String label){
        ArrayList params = new ArrayList();
        StringBuffer sql = new StringBuffer(IS_EXIST_LABEL);        
        params.add(templateId);
        params.add(label);
        if(fieldId != null && !"".equals(fieldId)){
            sql.append(" AND ID <> ? ");
            params.add(fieldId);
        }        
        debugQuery(sql, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(sql.toString());
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }           
    }
    
    /**
     * Is Linked To Step
     *  - EPS_PTE_010
     *  Cannot delete the field because it's linked with steps
     *  
     * @return boolean (true / false)
     * @param fieldId
     */
    public boolean isLinkedToStep(Long fieldId){
        ArrayList params = new ArrayList();
        params.add(fieldId);
        debugQuery(IS_LINKED_TO_STEP, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(IS_LINKED_TO_STEP);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if(!resultSet.next()){
                return false ;
            }

            return true;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Is Valid ComboBox Field
     * Cannot add the field which it's type is drop down list because 
     *       it does not have option value
     * -  EPS_TSL_002      
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public List getValidComboBoxFieldSteps(Long templateId){
        ArrayList params = new ArrayList();
        params.add(templateId);
        debugQuery(IS_VALID_COMBO_BOX_FIELD, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        List list = new ArrayList();
        
        try {
            prepStmt = getConnection().prepareStatement(IS_VALID_COMBO_BOX_FIELD);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            while(resultSet.next()){
                TemplateFieldVO vo = new TemplateFieldVO();
                vo.setLabel(getString(resultSet,"LABLE"));
                list.add(vo);
            }

            return list;
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Get By Report Id
     * 
     * @return Template Field Value Object
     * @param reportId Report Id
     */
    public SearchPageVO getByReportId(Long reportId,TemplateFieldVO vo,int pageNo){
        StringBuffer sqlQuery = new StringBuffer(GET_BY_REPORT_ID);
                        
        List params = new ArrayList();

        params.add(reportId);

        if(vo.getCode() != null){
            sqlQuery.append(" AND CODE = ? ");
            params.add(vo.getCode());
        }
        
        if(!isBlankOrNull(vo.getLabel())){
            sqlQuery.append(" AND LABLE LIKE ? ");
            params.add("%"+ vo.getLabel() +"%");
        }
                        
        debugQuery(sqlQuery, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(sqlQuery.toString(), params);
            prepStmt = doSearch(sqlQuery.toString(), params, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                        totalRecordCounts);
            while(resultSet.next()) {
                TemplateFieldVO templateFieldVO = new TemplateFieldVO();
                templateFieldVO.setId(getLong(resultSet,"ID"));
                templateFieldVO.setCode(getInteger(resultSet,"CODE"));
                templateFieldVO.setLabel(getString(resultSet,"LABLE"));
                templateFieldVO.setType(getInteger(resultSet,"FIELD_TYPE"));
                templateFieldVO.setTypeDesc(getString(resultSet,"FIELD_TYPE_DESC"));

                searchPage.addRecord(templateFieldVO);
            }
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }        
    }

    /**
     * Find option dependent fields.
     * 
     * @param dependentFieldId Dependent field ID.
     * @param label Template field label.
     * @param code Template field code.
     * @param pageNo Search page number.
     * @return List of parent template fields.
     */
    public SearchPageVO findParentOptionFields(Long dependentFieldId, 
                                               String label, 
                                               Integer code, 
                                               int pageNo) {
        // Construct search query
        StringBuffer query = new StringBuffer(FIND_PARENT_OPTION_FIELDS);
        List params = new ArrayList();
        params.add(dependentFieldId);

        if (code != null) {
            query.append(" AND TPE.CODE = ?");
            params.add(code);
        }

        if (! isBlankOrNull(label)) {
            query.append(" AND TPE.LABLE LIKE ?");
            params.add("%" + label + "%");
        }
                        
        debugQuery(query, params);
        
        PreparedStatement stm = null;
        ResultSet rs = null;          
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(query.toString(), params);
            stm = doSearch(query.toString(), params, pageNo , pageSize);
            rs = stm .executeQuery();

            SearchPageVO searchPage = new SearchPageVO(pageNo, pageSize ,
                                                       totalRecordCounts);
            while (rs.next()) {
                TemplateFieldVO vo = new TemplateFieldVO();
                searchPage.addRecord(vo);

                vo.setId(getLong(rs, "ID"));
                vo.setCode(getInteger(rs, "CODE"));
                vo.setLabel(getString(rs, "LABLE"));
            }

            return searchPage;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }

    /**
     * Update option-fields dependencies
     * 
     * @param templateId Procedure template ID.
     * @param username Active user name.
     */    
    public void updateOptionFieldsDependencies(Long templateId, String username) {
        // Initialize query parameters
        List params = new ArrayList();
        params.add(username);
        params.add(templateId);
        params.add(templateId);

        PreparedStatement stm1 = null;
        PreparedStatement stm2 = null;
        try {            
            // Update fields with options dependency
            debugQuery(UPDATE_RELATED_OPTION_FIELDS, params);
            stm1 = getConnection().prepareStatement(UPDATE_RELATED_OPTION_FIELDS);
            setQueryParameters(stm1, params);
            stm1.executeUpdate();

            // Update fields with no options dependency
            debugQuery(UPDATE_UNRELATED_OPTION_FIELDS, params);
            stm2 = getConnection().prepareStatement(UPDATE_UNRELATED_OPTION_FIELDS);
            setQueryParameters(stm2, params);
            stm2.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm1);
            close(stm2);
        }         
    }
    
    /**
     * Is Field Related To Active Procedure
     * 
     * @return boolean
     * @param code Field Code
     * @param templateId template Id
     */
    public boolean isFieldRelatedToActiveProcedure(Long templateId, Integer code){
        ArrayList params = new ArrayList();
        params.add(templateId);
        params.add(code);
      
        debugQuery(IS_FIELD_RELATED_TO_ACTIVE_PROCEDURE, params);     
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try {
            prepStmt = getConnection().prepareStatement(IS_FIELD_RELATED_TO_ACTIVE_PROCEDURE);
            setQueryParameters(prepStmt,params);
            resultSet = prepStmt.executeQuery();
            
            if (! resultSet.next()) {
                return false;
            }
            
            return resultSet.getInt(1) > 0;
            
        }
        catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Get Field Id By Code
     * 
     * @param code
     * @param templateId
     * 
     * @return Field Id
     */
    public Long getFieldIdByCode(Integer code,Long templateId){
        NamedQuery namedQuery = getNamedQuery("ae.rta.eps.dao.jdbc.TemplateField.getFieldIdByCode");   
        namedQuery.setParameter("fieldCode",code);
        namedQuery.setParameter("templateId",templateId);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;    
        try{   
            prepStmt = prepareStatement(namedQuery);
            resultSet = prepStmt.executeQuery();
            
            if(resultSet.next()){
                return getLong(resultSet,"ID");
            }           
            
            return null;

        }catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }                                                
    }
}