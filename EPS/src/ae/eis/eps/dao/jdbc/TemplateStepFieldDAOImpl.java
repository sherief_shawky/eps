/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 * 1.01  Ali Abdel-Aziz     06/01/2010  - Altered getRequesterStepFields method 
 *                                        to retrive the following extra columns:
 *                                        LabelWidth, SingleColumnFieldWidth, 
 *                                        and MultiColumnFieldWidth.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateStepFieldDAO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateStepFieldGroupVO;
import ae.eis.eps.vo.TemplateStepFieldVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Template step-field data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepFieldDAOImpl extends JdbcDataAccessObject  
                                      implements TemplateStepFieldDAO {
    /*
     * JDBC SQL and DML statements.
     */
       
    /** Get By Step Id */   
    private static final String GET_BY_GROUP_ID
    = " SELECT TPE.ID FIELD_ID,"
    +        " TPE.LABLE,"
    +        " TPE.FIELD_TYPE,"
    +        " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC("
    +                  " 'TF_EPS_FIELD_TYPE',TPE.FIELD_TYPE) FIELD_TYPE_DESC,"
    +        " TPE.FIELD_SIZE,"
    +        " SFL.IS_MANDATORY,"
    +        " SFL.IS_PRINTABLE,"
    +        " SFL.IS_EDITABLE,"
    +        " SFL.ORDER_NO,"
    +        " SFL.PTS_ID"
    + " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
    +      " TF_EPS_TEMP_STEP_FIELDS SFL"
    + " WHERE TPE.ID = SFL.TPE_ID"
    +   " AND SFL.TFG_ID = ?" // Group ID
    + " ORDER BY SFL.ORDER_NO";
    
    /** Get By Step Id and Field Id */      
    private static final String GET_STEP_FIELD
        =   " SELECT TPE.CODE,"
        +          " TPE.LABLE,"
        +          " TPE.FIELD_TYPE,"
        +          " ORDER_NO," 
		+          " SFL.IS_MANDATORY,"
		+          " SFL.IS_PRINTABLE,"
		+          " SFL.IS_EDITABLE,"
        +          " SFL.ROWS_NO,"
        +          " SFL.MIN_ROW_NO,"
        +          " SFL.COLUMN_SPAN,"        
        +          " SFL.VIEW_IN_NEW_LINE "
        +     " FROM TF_EPS_TEMP_PROCEDURE_FIELDS TPE,"
		+          " TF_EPS_TEMP_STEP_FIELDS SFL"
        +    " WHERE TPE.ID = SFL.TPE_ID"
        +      " AND TPE_ID = ? "
        +      " AND TFG_ID = ? " ;
    
    /** Create step field */    
    private static final String CREATE_STEP_FIELD
        =   " INSERT INTO TF_EPS_TEMP_STEP_FIELDS "
		+         " (IS_EDITABLE,IS_PRINTABLE,IS_MANDATORY,ORDER_NO,TPE_ID,PTS_ID,"
        +           "ROWS_NO,COLUMN_SPAN,VIEW_IN_NEW_LINE,TFG_ID,MIN_ROW_NO, "
        +           "CREATED_BY,CREATION_DATE) "
        +   " VALUES(?,"        // Is Editable
        +           "?,"        // Is Printable
        +           "?,"        // Is Mandatory
        +           "?,"        // Order Number 
        +           "?,"        // Field Id
        +           "(SELECT PTS_ID FROM TF_EPS_TEMP_STEP_FIELD_GROUPS WHERE ID = ?),"
        +           "?,"        // Rows Number
        +           "?,"        // Column Span
        +           "?,"        // view in new line
        +           "?,"        // TFG_ID1
        +           "?,"
        +           "?,"        // Created By
        +           "SYSDATE)";   
       
    /** Update Step Field */       
    private static final String UPDATE_STEP_FIELD
    = " UPDATE TF_EPS_TEMP_STEP_FIELDS "
    + " SET IS_EDITABLE = ?,"  // Is Editable
    +     " IS_PRINTABLE = ?," // Is Printable
    +     " IS_MANDATORY = ?," // Is Mandatory
    +     " ORDER_NO = ?,"     // Order Number
    +     " ROWS_NO = ?,"      // Rows Number
    +     " COLUMN_SPAN = ? ,"  // Column Span
    +     " VIEW_IN_NEW_LINE = ? ,"  // view in new line
    +     " MIN_ROW_NO = ? "  
    + " WHERE TPE_ID = ?"        // Field Id
    +   " AND PTS_ID = (SELECT PTS_ID FROM TF_EPS_TEMP_STEP_FIELD_GROUPS WHERE ID = ?)"
    +   " AND TFG_ID = ?" ;      // group Id
    
    /** Delete Step field */       
    private static final String DELETE_STEP_FIELD
      = "DELETE FROM TF_EPS_TEMP_STEP_FIELDS WHERE TPE_ID = ? AND TFG_ID = ?";
    
    /** Is Empty Combo Box */
    private static final String IS_EMPTY_COMBO_BOX
        =   " SELECT 1 FROM TF_EPS_TEMPLATE_FIELD_OPTIONS WHERE TPE_ID = ? ";
    
    /** Is Field Exist */
    private static final String IS_FIELD_EXIST
      = "SELECT 1 FROM TF_EPS_TEMP_STEP_FIELDS WHERE TPE_ID = ? AND PTS_ID = ?";

    /** Is Valid Step Field */
    private static final String IS_VALID_STEP_FIELD
      = " SELECT COUNT(*) COUNT "
      +   " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
      +  " WHERE PTL_ID = ? "
      +    " AND STEP_TYPE IN(2,3) "
      +    " AND EXISTS (SELECT 1 FROM TF_EPS_TEMP_STEP_FIELDS "
      +                        " WHERE PTS_ID = PTS.ID)";
      
    /** Is Valid Editable Step Field */
    private static final String IS_VALID_EDITABLE_STEP_FIELD
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTL_ID = ? "
        +      " AND STEP_TYPE = 5 "
        +      " AND EXISTS (SELECT 1 FROM TF_EPS_TEMP_STEP_FIELDS "
        +                           " WHERE PTS_ID = PTS.ID "
        +                             " AND IS_EDITABLE = 2 )";
    
    /** Is Valid Mandatory Step Field */
    private static final String IS_VALID_MANDATORY_STEP_FIELD
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTL_ID = ? "
        +      " AND STEP_TYPE = 5 "
        +      " AND EXISTS (SELECT 1 FROM TF_EPS_TEMP_STEP_FIELDS "
        +                           " WHERE PTS_ID = PTS.ID "
        +                             " AND IS_MANDATORY = 2 )";    
        
    /** Is Mandatory And Not Editable */
    private static final String IS_MANDATORY_AND_NOT_EDITABLE
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_PROCEDURE_TEMP_STEPS PTS "
        +    " WHERE PTL_ID = ? "
        +      " AND EXISTS (SELECT 1 FROM TF_EPS_TEMP_STEP_FIELDS "
        +                           " WHERE PTS_ID = PTS.ID "
        +                             " AND IS_MANDATORY = 2 "
        +                             " AND IS_EDITABLE = 1 )";    
        
    /** Is Order Exist */
    private static final String IS_ORDER_EXIST
        =   " SELECT COUNT(*) COUNT "
        +     " FROM TF_EPS_TEMP_STEP_FIELDS "
        +    " WHERE TFG_ID = ? "   // group Id
        +      " AND TPE_ID <> ? "   // field Id
        +      " AND ORDER_NO = ? "; // field order
        
    /** get maximum step field order number */        
    private static final String GET_MAX_STEP_FIELD_ORDER_NUMBER
        =   " SELECT MAX(ORDER_NO) MAX_ORDER_NO "
        +          " FROM TF_EPS_TEMP_STEP_FIELDS "
        +    " WHERE PTS_ID = ? "
        +      " AND TFG_ID = ? ";
    
    /*
     * Methods
     */

    /**
     * Get requester step fields.
     * 
     * @param templateId procedure template ID.
     * @return requester step fields.
     */
    public List getRequesterStepFields(Long templateId) {
        NamedQuery query = getNamedQuery(
            "ae.rta.eps.dao.jdbc.TemplateStepGroupField.getRequesterStepFields");
        query.setParameter("templateId", templateId);
        // Debug query
        debugQuery(query);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = prepareStatement(query);
            rs = stm.executeQuery();

            List groupsList = new ArrayList();
            TemplateStepFieldGroupVO groupVO = null;
            while (rs.next()) {
                // Add new group to groups list
                Long groupId = getLong(rs, "TFG_ID");
                if (groupVO == null || groupVO.getId() == null || 
                  ! groupVO.getId().equals(groupId)) {
                    //  Add new group to groups list
                    groupVO = new TemplateStepFieldGroupVO();
                    groupsList.add(groupVO);

                    groupVO.setId(groupId);
                    groupVO.setNameAr(rs.getString("GROUP_NAME_A"));
                    groupVO.setLabelWidth(getInteger(rs, "LABEL_WIDTH"));
                    groupVO.setSingleColumnFieldWidth(getInteger(rs, "SINGLE_COLUMN_FIELD_WIDTH"));
                    groupVO.setMultiColumnFieldWidth(getInteger(rs, "MULTI_COLUMN_FIELD_WIDTH"));
                }

                TemplateStepFieldVO vo = new TemplateStepFieldVO();
                groupVO.addField(vo);
                vo.setField(new TemplateFieldVO());
                vo.setTemplateStep(new TemplateStepVO());

                vo.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
                vo.setIsEditable(getInteger(rs, "IS_EDITABLE"));
                vo.setOrderNo(getInteger(rs, "ORDER_NO"));
                vo.setColumnSpan(getInteger(rs, "COLUMN_SPAN"));
                vo.setNoOfRows(getInteger(rs, "ROWS_NO"));
                vo.setViewInNewLine(getInteger(rs,"VIEW_IN_NEW_LINE"));
                 
                vo.getField().setId(getLong(rs, "FIELD_ID"));
                vo.getField().setCode(getInteger(rs, "CODE"));
                vo.getField().setLabel(getString(rs, "LABLE"));
                vo.getField().setValue(getString(rs, "VALUE"));
                vo.getField().setType(getInteger(rs, "FIELD_TYPE"));
                vo.getField().setSize(getInteger(rs, "FIELD_SIZE"));

                vo.getField().setHasRelatedOptions(
                    getInteger(rs, "HAS_RELATED_OPTIONS"));

                vo.getTemplateStep().setId(getLong(rs, "STEP_ID"));
            }

            return groupsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Get template step group fields.
     * 
     * @param pageNo Page Number
     * @param groupId Template step fields group ID.
     * @return template step group fields.
     */
    public SearchPageVO findByGroupId(int pageNo, Long groupId) {
        ArrayList params = new ArrayList();
        params.add(groupId);
        debugQuery(GET_BY_GROUP_ID, params);
        
        ResultSet rs = null;
        PreparedStatement stm = null;
        try{
            int pageSize = getDefaultPageSize();
            int totalRecordCounts=getTotalCount(GET_BY_GROUP_ID, params);
            
            stm = doSearch(GET_BY_GROUP_ID, params, pageNo , pageSize);
            rs  = stm.executeQuery();
            
            SearchPageVO searchPage = 
                    new SearchPageVO(pageNo, pageSize ,totalRecordCounts);   
            
            while (rs.next()) {
                TemplateStepFieldVO vo = new TemplateStepFieldVO();
                searchPage.addRecord(vo);
                
                vo.setTemplateStepFieldGroup(new TemplateStepFieldGroupVO());
                vo.getTemplateStepFieldGroup().setId(groupId);

                TemplateFieldVO fieldVO= new TemplateFieldVO();
                vo.setField(fieldVO);
                fieldVO.setId(getLong(rs, "FIELD_ID"));
                fieldVO.setLabel(getString(rs, "LABLE"));
                fieldVO.setType(getInteger(rs, "FIELD_TYPE"));
                fieldVO.setTypeDesc(getString(rs, "FIELD_TYPE_DESC"));               
                fieldVO.setSize(getInteger(rs, "FIELD_SIZE"));

                vo.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
                vo.setIsEditable(getInteger(rs, "IS_EDITABLE"));
                vo.setIsPrintable(getInteger(rs, "IS_PRINTABLE"));
                vo.setOrderNo(getInteger(rs,"ORDER_NO"));

                vo.setTemplateStep(new TemplateStepVO());
                vo.getTemplateStep().setId(getLong(rs, "PTS_ID"));
            }

            return searchPage;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Get step field info.
     * 
     * @param fieldId Template field ID.
     * @param groupId Fields group ID.
     * @return step field info.
     */
    public TemplateStepFieldVO getStepField(Long fieldId, Long groupId) {
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        params.add(groupId);
        debugQuery(GET_STEP_FIELD, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_STEP_FIELD);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            if (!rs.next()) {
                return null;
            }

            TemplateStepFieldVO stepFieldVO = new TemplateStepFieldVO();
            TemplateFieldVO fieldVO = new TemplateFieldVO();
            TemplateStepVO stepVO = new TemplateStepVO();

            fieldVO.setId(fieldId);
            fieldVO.setCode(getInteger(rs, "CODE"));
            fieldVO.setLabel(getString(rs, "LABLE"));
            fieldVO.setType(getInteger(rs, "FIELD_TYPE"));
            
            stepVO.setId(groupId);
            
            stepFieldVO.setOrderNo(getInteger(rs,"ORDER_NO"));
            stepFieldVO.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
            stepFieldVO.setIsEditable(getInteger(rs, "IS_EDITABLE"));
            stepFieldVO.setIsPrintable(getInteger(rs, "IS_PRINTABLE"));
            stepFieldVO.setColumnSpan(getInteger(rs ,"COLUMN_SPAN"));
            stepFieldVO.setNoOfRows(getInteger(rs ,"ROWS_NO")); 
            stepFieldVO.setMinNoOfRows(getInteger(rs, "MIN_ROW_NO")); 
            stepFieldVO.setViewInNewLine(getInteger(rs ,"VIEW_IN_NEW_LINE")); 
                        
            stepFieldVO.setTemplateStep(stepVO);
            stepFieldVO.setField(fieldVO);

            return stepFieldVO;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Create Step Field
     * 
     * @return TemplateStepFieldVO Template Step Field Value Object
     * @param vo Template Step Field Value Object
     */
    public TemplateStepFieldVO create(TemplateStepFieldVO vo){
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(vo.getIsEditable());
            params.add(vo.getIsPrintable());
            params.add(vo.getIsMandatory());
            params.add(vo.getOrderNo());
            params.add(vo.getField().getId());
            params.add(vo.getTemplateStepFieldGroup().getId());
            params.add(vo.getNoOfRows());
            params.add(vo.getColumnSpan());
            params.add(vo.getViewInNewLine());
            params.add(vo.getTemplateStepFieldGroup().getId());
            params.add(vo.getMinNoOfRows());
            params.add(vo.getCreatedBy());

            debugQuery(CREATE_STEP_FIELD, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_STEP_FIELD);
            setQueryParameters(stm, params);
            stm.executeUpdate();

            // return updated value object
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Delete step field.
     * 
     * @param fieldId Template field ID.
     * @param groupId Fields group ID.
     */
    public void delete(Long fieldId, Long groupId) {
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(fieldId);
            params.add(groupId);
            debugQuery(DELETE_STEP_FIELD, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_STEP_FIELD);
            setQueryParameters(stm, params);
            stm.executeUpdate();

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * Update Step Field
     * 
     * @param vo Template Step Field Value Object
     */
    public void update(TemplateStepFieldVO vo){
        PreparedStatement stm = null;
        try {
           
            // Debug query
            List params = new ArrayList();
            params.add(vo.getIsEditable());
            params.add(vo.getIsPrintable());
            params.add(vo.getIsMandatory());
            params.add(vo.getOrderNo());
            params.add(vo.getNoOfRows());
            params.add(vo.getColumnSpan());     
            params.add(vo.getViewInNewLine());
            params.add(vo.getMinNoOfRows());
            params.add(vo.getField().getId());
            params.add(vo.getTemplateStepFieldGroup().getId());
            params.add(vo.getTemplateStepFieldGroup().getId());
          
            debugQuery(UPDATE_STEP_FIELD, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_STEP_FIELD);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if (count <= 0) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to update template step field: ")
                        .append("groupId=")
                        .append(vo.getTemplateStepFieldGroup().getId())
                        .append(", fieldId=")
                        .append(vo.getField().getId())
                        .toString());
            }

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }            
    }
    
    /**
     * Is Empty Combo Box
     * 
     * @return boolean (true / false)
     * @param fieldId Field Id
     */
    public boolean isEmptyComboBox(Long fieldId){
        PreparedStatement stm = null;
        ResultSet rs = null;
     
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        debugQuery(IS_EMPTY_COMBO_BOX, params);
            
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(IS_EMPTY_COMBO_BOX);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(rs.next()){
                return false;
            }
            
            return true;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }
    
    /**
     * Is Field Exist
     * 
     * @return boolean (true / false)
     * @param stepId Step Id
     * @param fieldId Field Id
     */
    public boolean isFieldExist(Long fieldId,Long stepId){
        PreparedStatement stm = null;
        ResultSet rs = null;
     
        // Debug query
        List params = new ArrayList();
        params.add(fieldId);
        params.add(stepId);
        debugQuery(IS_FIELD_EXIST, params);
            
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(IS_FIELD_EXIST);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(!rs.next()){
                return false;
            }
            
            return true;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }              
    }
    
    /**
     * Is Valid Step Field
     * Cannot add fields to steps where step type is notification or business action
     *  - EPS_TSL_001
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidStepField(Long templateId){
        PreparedStatement stm = null;
        ResultSet rs = null;
     
        // Debug query
        List params = new ArrayList();
        params.add(templateId);
        debugQuery(IS_VALID_STEP_FIELD, params);
        int count = 0;    
        
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(IS_VALID_STEP_FIELD);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(rs.next()){
                count = getInteger(rs,"COUNT").intValue();
            }
            
            return count == 0;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }           
    }
    
    /**
     * Is Valid Editable Step Field
     * The Field must not be editable if the step type is end
     * -  EPS_TSL_005
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidEditableStepField(Long templateId){
        PreparedStatement stm = null;
        ResultSet rs = null;
     
        // Debug query
        List params = new ArrayList();
        params.add(templateId);
        debugQuery(IS_VALID_EDITABLE_STEP_FIELD, params);
        int count = 0;    
        
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(IS_VALID_EDITABLE_STEP_FIELD);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(rs.next()){
                count = getInteger(rs,"COUNT").intValue();
            }
            
            return count == 0;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }            
    }
    
    /**
     * Is Valid Mandatory Step Field
     * The Field must not be mandatory if the step type is end
     * -  EPS_TSL_006
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isValidMandatoryStepField(Long templateId){
        PreparedStatement stm = null;
        ResultSet rs = null;
     
        // Debug query
        List params = new ArrayList();
        params.add(templateId);
        debugQuery(IS_VALID_MANDATORY_STEP_FIELD, params);
        int count = 0;    
        
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(IS_VALID_MANDATORY_STEP_FIELD);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(rs.next()){
                count = getInteger(rs,"COUNT").intValue();
            }
            
            return count == 0;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
    
    /**
     * Is Mandatory And Not Editable
     * The Field must be editable if the field is mandatory
     * -  EPS_TSL_009
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    public boolean isMandatoryAndNotEditable(Long templateId){
        PreparedStatement stm = null;
        ResultSet rs = null;
     
        // Debug query
        List params = new ArrayList();
        params.add(templateId);
        debugQuery(IS_MANDATORY_AND_NOT_EDITABLE, params);
        int count = 0;    
        
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(IS_MANDATORY_AND_NOT_EDITABLE);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(rs.next()){
                count = getInteger(rs,"COUNT").intValue();
            }
            
            return count != 0;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
    
    /**
     * Is Order Exist
     *  - EPS_TSL_011 : The order must be unique for the procedure
     *  
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param fieldId field Id
     * @param groupId group Id
     * @param order order
     */
    public boolean isOrderExist(Long fieldId,Long groupId,Integer order){
        PreparedStatement stm = null;
        ResultSet rs = null;
     
        // Debug query
        List params = new ArrayList();
        params.add(groupId);
        params.add(fieldId);
        params.add(order);
        debugQuery(IS_ORDER_EXIST, params);
        int count = 0;    
        
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(IS_ORDER_EXIST);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(rs.next()){
                count = getInteger(rs,"COUNT").intValue();
            }
            
            return count != 0;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }              
    }
    
    /**
     * Get Maximum Step Field Order Number
     * 
     * @return Maximum Step Field Order Number
     * @param groupId Group Id
     * @param stepId Step Id
     */
    public Integer getMaxStepFieldOrderNo(Long stepId,Long groupId){
        PreparedStatement stm = null;
        ResultSet rs = null;
             
        List params = new ArrayList();
        params.add(stepId);
        params.add(groupId);
        
        // Debug query   
        debugQuery(GET_MAX_STEP_FIELD_ORDER_NUMBER, params);
        
        // default order number
        Integer maxOrderNo = new Integer(0);    
        
        try {

            // Execute DML statement
            stm = getConnection().prepareStatement(GET_MAX_STEP_FIELD_ORDER_NUMBER);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
                 
            if(rs.next()){
                maxOrderNo = getInteger(rs,"MAX_ORDER_NO");
                if(maxOrderNo == null)
                    maxOrderNo = new Integer(0);                
            }
            
            return maxOrderNo;
                        
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }          
    }
}