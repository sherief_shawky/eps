/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mena Emiel  23/02/2011  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TableColumnValueDAO;
import ae.eis.eps.vo.TableColumnValueVO;
import ae.eis.eps.vo.TableRowVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.dao.jdbc.NamedQuery;
import ae.eis.util.web.UserProfile;
import java.sql.PreparedStatement;
import java.util.List;

/**
 * Table column value data access object JDBC implementation.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public class TableColumnValueDAOImpl extends JdbcDataAccessObject 
                                        implements TableColumnValueDAO {
    /*
     * Methods
     */
     
    /**
     * Insert Row Values
     * 
     * @param rowColumns Single Row Which Includs Columns
     * @param userProfile User Profile
     * @param procedureId Procedure Id
     */
    public void insertRowValues(List rowColumns, UserProfile userProfile, 
                                                            Long procedureId) {
        PreparedStatement stm = null;
        try {
            // Get row index from and assign it for all columns in first row
            Long rowIndex = generateSequence("TCU_ROW_SEQ");
            
            // Get Initialized related named query
            NamedQuery query = getNamedQuery(
                "ae.rta.eps.dao.jdbc.TableColumnValue.insertRowValues");
            
            // 
            for (int i = 0; i < rowColumns.size() ; i++) {
                TableColumnValueVO columnValue = (TableColumnValueVO)
                                                            rowColumns.get(i);
                // Insert column values for each column in row
                query.setParameter("rowIndex", rowIndex);
                query.setParameter("columnValue", columnValue.getValue());
                query.setParameter("userId", userProfile.getUserId());
                query.setParameter("tableHeaderId", columnValue.
                                                    getTableHeaderVO().
                                                    getId());
                query.setParameter("procedureId", procedureId);
                query.setParameter("createdBy", userProfile.getUsername());
                
                // Execute DML statement
                stm = prepareStatement(query);
                stm.executeUpdate();
            }
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Insert Table Row Values
     * 
     * @param rowsList    Rows List
     * @param userProfile User Profile
     * @param procedureId Procedure Id
     * 
     * @return Number Of Update Record
     */
    public Long insertTableRowValues(List rowsList, UserProfile userProfile, 
                                                            Long procedureId) {
        PreparedStatement stm = null;
        try {   
            // Get/Initialized related named query
            NamedQuery query = getNamedQuery(
                "ae.rta.eps.dao.jdbc.TableColumnValue.insertTableRowValues");
            
            int noOfInsertedRecord = 0;
            // Row Index 
            for (int i = 0; i < rowsList.size() ; i++) {
            
                // Get Table Row Value Object
                TableRowVO tableRowVO = (TableRowVO)rowsList.get(i);
                for (int j = 0; j < tableRowVO.getColumns().size(); j++)  {
                    TableColumnValueVO columnValue = (TableColumnValueVO)
                                                                tableRowVO.getColumns().get(j);
                    // Insert column values for each column in row
                    query.setParameter("rowIndex", columnValue.getRowIndex());
                    query.setParameter("columnValue", columnValue.getValue()); 
                    query.setParameter("userId", userProfile.getUserId());
                    query.setParameter("procedureFieldId", columnValue.getTableHeaderVO().getId());
                    query.setParameter("proceduerId", procedureId);
                    query.setParameter("createdBy", userProfile.getUsername());
                    query.setParameter("orderNo", columnValue.getTableHeaderVO().getOrderNo());
                    
                    // Execute DML statement
                    stm = prepareStatement(query);
                    noOfInsertedRecord =+ stm.executeUpdate();
                }
                
            }        
            return new Long(noOfInsertedRecord);
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }                                                            
  
}