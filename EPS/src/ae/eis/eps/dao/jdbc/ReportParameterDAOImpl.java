/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/10/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.ReportParameterDAO;
import ae.eis.eps.vo.ProcedureTemplateLogVO;
import ae.eis.eps.vo.ReportParameterVO;
import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.eps.vo.TemplateReportVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure template report-parameters access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ReportParameterDAOImpl extends JdbcDataAccessObject  
                                    implements ReportParameterDAO {
    /*
     * JDBC SQL and DML statements.
     */

    /** Get field options. */
    private static final String GET_BY_REPORT_ID
    = " SELECT ERP.ID AS PARAM_ID,"
    +        " ERP.IS_MANDATORY,"
    +        " ERP.NAME,"
    +        " ERP.ORDER_NO,"
    +        " ERP.MAX_SEARCH_DAYS,"
    +        " ERP.TPE_ID AS FIELD_ID,"
    +        " TPE.CODE,"
    +        " TPE.FIELD_SIZE,"
    +        " TPE.FIELD_TYPE,"
    +        " TPE.LABLE "
    + " FROM TF_EPS_REPORT_PARAMETERS ERP,"
    +      " TF_EPS_TEMP_PROCEDURE_FIELDS TPE"
    + " WHERE ERP.TPE_ID = TPE.ID"
    +   " AND ERP.ETR_ID = ?" // REPORT ID
    + " ORDER BY ERP.ORDER_NO";

    /** Get field options. */
    private static final String FIND_REPORT_PARAMETERS
    = " SELECT ERP.ID AS PARAM_ID,"
    +        " ERP.IS_MANDATORY,"
    +        " ERP.NAME,"
    +        " ERP.ORDER_NO,"
    +        " ERP.MAX_SEARCH_DAYS,"
    +        " ERP.TPE_ID AS FIELD_ID,"
    +        " TPE.CODE,"
    +        " TPE.FIELD_SIZE,"
    +        " TPE.FIELD_TYPE,"
    +        " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_FIELD_TYPE'"
    +                                           ",FIELD_TYPE) FIELD_TYPE_DESC, "    
    +        " TPE.LABLE "
    + " FROM TF_EPS_REPORT_PARAMETERS ERP,"
    +      " TF_EPS_TEMP_PROCEDURE_FIELDS TPE"
    + " WHERE ERP.TPE_ID = TPE.ID"
    +   " AND ERP.ETR_ID = ?" // REPORT ID
    + " ORDER BY ERP.ORDER_NO";
    
    /** Add Report Parameter Query */
    private static final String ADD_REPORT_PARAMETER
        =   "INSERT INTO TF_EPS_REPORT_PARAMETERS "
        +        "(ID,IS_MANDATORY,NAME,ORDER_NO,"
        +         "MAX_SEARCH_DAYS,TPE_ID,ETR_ID,"
        +         "CREATED_BY,CREATION_DATE,"
        +         "UPDATED_BY,UPDATE_DATE) "
        +  " VALUES(?,"     // ID
        +         "?,"      // IS_MANDATORY
        +         "?,"      // NAME
        +         "?,"      // ORDER_NO
        +         "?,"      // MAX_SEARCH_DAYS
        +         "?,"      // TPE_ID
        +         "?,"      // ETR_ID
        +         "?,"      // CREATED_BY
        +         "SYSDATE,"
        +         "?,"      // UPDATED_BY
        +         "SYSDATE)";
    
    /** Update Report Parameter Query */
    private static final String UPDATE_REPORT_PARAMETER
        = " UPDATE TF_EPS_REPORT_PARAMETERS "
        +    " SET IS_MANDATORY = ?, "
        +        " NAME = ?, "       
        +        " ORDER_NO = ?, "
        +        " MAX_SEARCH_DAYS = ?, "
        +        " TPE_ID = ?, "
        +        " UPDATED_BY = ?,"		
        +        " UPDATE_DATE = SYSDATE "
        +  "  WHERE ID = ?";
    
    /** Delete Report Parameter Query */
    private static final String DELETE_REPORT_PARAMETER
        = " DELETE FROM TF_EPS_REPORT_PARAMETERS WHERE ID = ? ";
        
    /** Is Order Exist */
    private static final String IS_ORDER_EXISTS
        = " SELECT 1 FROM TF_EPS_REPORT_PARAMETERS WHERE ETR_ID = ? AND ORDER_NO = ? ";
    
    /** Get Parameter */
    private static final String GET_PAREMETER
        = " SELECT TPE.ID FIELD_ID, "
        +        " TPE.LABLE,"
        +        " TPE.FIELD_TYPE,"
        +        " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_FIELD_TYPE',FIELD_TYPE) FIELD_TYPE_DESC,"
        +        " ERP.NAME PARAM_NAME,"
        +        " ERP.ORDER_NO,"
        +        " ERP.MAX_SEARCH_DAYS,"
        +        " ERP.IS_MANDATORY"
        +   " FROM TF_EPS_REPORT_PARAMETERS ERP,"
        +        " TF_EPS_TEMP_PROCEDURE_FIELDS TPE "
        +  " WHERE ERP.TPE_ID = TPE.ID      "
        +    " AND ERP.ID = ?";
    
    /** Add Log Query */        
    private static final String ADD_LOG_QUERY
        =    "INSERT INTO TF_EPS_PROCEDURE_TEMP_LOGS "
        +    "(ID,STATUS,VERSION_NO,NAME_E,ACTION_TYPE,PRIORITY,NAME_A, "
        +    "STATUS_DATE,PTL_ID,EMPLOYEE_NAME,CREATED_BY,CREATION_DATE,REMARKS) "
        +    "SELECT PTG_SEQ.NEXTVAL, "
        +          " PTL.STATUS,"
        +          " PTL.VERSION_NO,"
        +          " PTL.NAME_E,"
        +          " ?,"
        +          " PTL.PRIORITY,"
        +          " PTL.NAME_A,"
        +          " SYSDATE,"
        +          " PTL_ID,"
        +          " F_DB_GET_EMP_NAME_A(?),"
        +          " ?,"
        +          " SYSDATE,"
        +          " ETR.NAME_A ||' - '|| ERP.NAME " 
        +     " FROM TF_EPS_REPORT_PARAMETERS ERP,"
        +          " TF_EPS_TEMPLATE_REPORTS ETR,"
        +          " TF_EPS_PROCEDURE_TEMPLATES PTL"
        +    " WHERE ERP.ETR_ID = ETR.ID "
        +      " AND ETR.PTL_ID = PTL.ID " 
        +      " AND ERP.ID = ? ";    
        
    /*
     * Methods
     */

    /**
     * Get report parameters.
     * 
     * @param reportId EPS report ID.
     * @return report parameters.
     */
    public List getByReportId(Long reportId) {
        // Debug query
        List params = new ArrayList();
        params.add(reportId);
        debugQuery(GET_BY_REPORT_ID, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_BY_REPORT_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            List paramList = new ArrayList();
            while (rs.next()) {
                ReportParameterVO vo = new ReportParameterVO();
                vo.setTemplateField(new TemplateFieldVO());
                vo.setReport(new TemplateReportVO());
                paramList.add(vo);

                vo.getReport().setId(reportId);
                vo.setId(getLong(rs, "PARAM_ID"));
                vo.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
                vo.setName(getString(rs, "NAME"));
                vo.setOrderNo(getInteger(rs, "ORDER_NO"));
                vo.setMaxSearchDays(getInteger(rs, "MAX_SEARCH_DAYS"));

                vo.getTemplateField().setId(getLong(rs, "FIELD_ID"));
                vo.getTemplateField().setCode(getInteger(rs, "CODE"));
                vo.getTemplateField().setSize(getInteger(rs, "FIELD_SIZE"));
                vo.getTemplateField().setType(getInteger(rs, "FIELD_TYPE"));
                vo.getTemplateField().setLabel(getString(rs, "LABLE"));
            }

            return paramList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * Find report parameter
     * 
     * @return report parameters
     * @param pageNo pagination Page Number
     * @param reportId EPS report ID
     */
    public SearchPageVO find(Long reportId, int pageNo){
        List params = new ArrayList();
        params.add(reportId);      
        debugQuery(FIND_REPORT_PARAMETERS, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(FIND_REPORT_PARAMETERS, params);
            prepStmt =doSearch(FIND_REPORT_PARAMETERS, params, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                             totalRecordCounts);
            while(resultSet.next()) {
                ReportParameterVO vo = new ReportParameterVO();
                vo.setTemplateField(new TemplateFieldVO());
                vo.setReport(new TemplateReportVO());

                vo.getReport().setId(reportId);
                vo.setId(getLong(resultSet, "PARAM_ID"));
                vo.setIsMandatory(getInteger(resultSet, "IS_MANDATORY"));
                vo.setName(getString(resultSet, "NAME"));
                vo.setOrderNo(getInteger(resultSet, "ORDER_NO"));
                vo.setMaxSearchDays(getInteger(resultSet, "MAX_SEARCH_DAYS"));

                vo.getTemplateField().setId(getLong(resultSet, "FIELD_ID"));
                vo.getTemplateField().setCode(getInteger(resultSet, "CODE"));
                vo.getTemplateField().setSize(getInteger(resultSet, "FIELD_SIZE"));
                vo.getTemplateField().setType(getInteger(resultSet, "FIELD_TYPE"));
                    vo.getTemplateField().setTypeDesc(getString(resultSet,"FIELD_TYPE_DESC"));
                vo.getTemplateField().setLabel(getString(resultSet, "LABLE"));
                
                searchPage.addRecord(vo); 
            }
            
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }          
    }
    
    /**
     * Get report parameter
     * 
     * @return report parameter value object
     * @param reportId EPS param ID
     */
    public ReportParameterVO get(Long paramId){
        // Debug query
        List params = new ArrayList();
        params.add(paramId);
        debugQuery(GET_PAREMETER, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_PAREMETER);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            List paramList = new ArrayList();
            
            if (! rs.next()) {
                return null;
            }
            
            ReportParameterVO vo = new ReportParameterVO();
            vo.setTemplateField(new TemplateFieldVO());
            vo.setReport(new TemplateReportVO());

            vo.setId(paramId);
            vo.setIsMandatory(getInteger(rs, "IS_MANDATORY"));
            vo.setName(getString(rs, "PARAM_NAME"));
            vo.setOrderNo(getInteger(rs, "ORDER_NO"));
            vo.setMaxSearchDays(getInteger(rs, "MAX_SEARCH_DAYS"));

            vo.getTemplateField().setId(getLong(rs, "FIELD_ID"));
            vo.getTemplateField().setType(getInteger(rs, "FIELD_TYPE"));
            vo.getTemplateField().setTypeDesc(getString(rs, "FIELD_TYPE_DESC"));
            vo.getTemplateField().setLabel(getString(rs, "LABLE"));        
            
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }    
    }
    
    
    
    
    /**
     * Add Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    public ReportParameterVO addReportParameter(ReportParameterVO vo){
        // Debug query
        
        Long newId = generateSequence("ERP_SEQ");
        List params = new ArrayList();
        
        params.add(newId);
        params.add(vo.getIsMandatory());       
        params.add(vo.getName());
        params.add(vo.getOrderNo());
        params.add(vo.getMaxSearchDays());
        params.add(vo.getTemplateField().getId());
        params.add(vo.getReport().getId());
        params.add(vo.getCreatedBy());
        params.add(vo.getUpdatedBy());

        debugQuery(ADD_REPORT_PARAMETER, params);

        PreparedStatement stm = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(ADD_REPORT_PARAMETER);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            // return updated report parameter value object
            vo.setId(newId);
            
            addLog(vo, ProcedureTemplateLogVO.ACTION_ADD_PARAMETER);
            
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
    
    /**
     * Update Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    public void updateReportParameter(ReportParameterVO vo){
        // Debug query
        
        List params = new ArrayList();
            
        params.add(vo.getIsMandatory());       
        params.add(vo.getName());
        params.add(vo.getOrderNo());
        params.add(vo.getMaxSearchDays());
        params.add(vo.getTemplateField().getId());
        params.add(vo.getUpdatedBy());
        params.add(vo.getId());

        debugQuery(UPDATE_REPORT_PARAMETER, params);

        PreparedStatement stm = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(UPDATE_REPORT_PARAMETER);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if(count <= 0){
                throw new DataAccessException(" Failed to update record, "
                                             + "Paramerter Id : " + vo.getId());
            }
            
            // ADD LOG
            addLog(vo, ProcedureTemplateLogVO.ACTION_UPDATE_PARAMETER);
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }             
    }
    
    /**
     * Update Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    public void deleteReportParameter(ReportParameterVO vo){
        
        // add log
        addLog(vo, ProcedureTemplateLogVO.ACTION_DELETE_PARAMETER);
        
        // Debug query
        List params = new ArrayList();  
        params.add(vo.getId());

        debugQuery(DELETE_REPORT_PARAMETER, params);

        PreparedStatement stm = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(DELETE_REPORT_PARAMETER);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if(count <= 0){
                throw new DataAccessException(" Failed to delete record, "
                                                + "Paramerter Id : " + vo.getId());                
            }
    
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * Is Order Exist
     * 
     * @return true if parameter order for this report is exists, 
     *                                                  otherwise return false
     * @param orderNo Parameter Order
     * @param reportId Report Id
     */
    public boolean isOrderExist(Long reportId,Integer orderNo,Long paramId){
        
        // Debug query
        StringBuffer sqlQuery = new StringBuffer(IS_ORDER_EXISTS);
        
        List params = new ArrayList();
        params.add(reportId);
        params.add(orderNo);
        
        if(paramId != null){
            sqlQuery.append(" AND ID <> ? ");
            params.add(paramId);
        }
        
        debugQuery(sqlQuery.toString(), params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(sqlQuery.toString());
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if( !rs.next()){
                return false;
            }
            
            return true;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    
    /**
     * Add Log
     * 
     * @param remarks
     * @param actionType
     * @param vo Template Report Value Object
     */
    private void addLog(ReportParameterVO vo,Integer actionType){
        // Debug message
        List params = new ArrayList();
        
        params.add(actionType);
        params.add(vo.getCreatedBy());
        params.add(vo.getCreatedBy());
        params.add(vo.getId());
         
        debugQuery(ADD_LOG_QUERY, params);
        
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(ADD_LOG_QUERY);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
        
}