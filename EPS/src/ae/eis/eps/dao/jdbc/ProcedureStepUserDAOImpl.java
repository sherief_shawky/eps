/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  28/09/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.UserVO;
import ae.eis.eps.dao.ProcedureStepUserDAO;
import ae.eis.eps.vo.ProcedureStepUserVO;
import ae.eis.eps.vo.ProcedureStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure step-users data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class ProcedureStepUserDAOImpl extends JdbcDataAccessObject  
                                      implements ProcedureStepUserDAO {
    /*
     * Constants and class variables
     */

    /** Step users query. */
    private static final String STEP_USERS
    =   " SELECT PRE.ID PRE_ID, "
    +   " TSU.USR_ID, "
    +   " USR.NAME AS USERNAME, "
    +   " EVU.EMP_ID, "
    +   " EMP.NAME_A ,"
    +   " EMP.E_MAIL "
    +   " FROM "
    +   "      TF_EPS_PROCEDURES PRD, "
    +   "      TF_EPS_PROCEDURE_STEPS PRE, "
    +   "      TF_EPS_PROCEDURE_TEMPLATES PTL, "
    +   "      TF_EPS_PROCEDURE_TEMP_STEPS PTS, "
    +   "      TF_EPS_TEMP_STEP_USERS TSU, "
    +   "     SF_INF_USERS USR, "
    +   "     TF_STP_EMPLOYEES EMP, "
    +   "     TF_STP_EMP_VS_USERS EVU "
    +   " WHERE PRE.PRD_ID = PRD.ID "
    +   " AND   PRD.PTL_ID = PTL.ID "
    +   " AND   PTL.ID	 = PTS.PTL_ID "
    +   " AND   PTS.ID     = TSU.PTS_ID "
    +   " AND   USR.STATUS = 2 "
    +   " AND   TSU.USR_ID = USR.ID "
    +   " AND   PTS.SEQ_NO = PRE.SEQ_NO "
    +   " AND EVU.USR_USR_ID = USR.ID "
    +   " AND EVU.EMP_ID = EMP.ID "
    +   " AND PRE.PRD_ID = ? "     // Procedure ID
    +   " AND PRE.SEQ_NO = ? "  ;  // Step sequence number


    /** Step user validation query. */
    private static final String IS_VALID_STEP_USER
    = " SELECT 1 "
    +  " FROM   TF_EPS_PROCEDURES PRD, "
    +  "         TF_EPS_PROCEDURE_STEPS PRE, "
    +  "         TF_EPS_PROCEDURE_TEMPLATES PTL, "
    +  "         TF_EPS_PROCEDURE_TEMP_STEPS PTS, "
    +  "         TF_EPS_TEMP_STEP_USERS TSU, "
    +  "         SF_INF_USERS USR "
    +  "  WHERE  PRE.PRD_ID = PRD.ID "
    +  "  AND    PRD.PTL_ID = PTL.ID "
    +  "  AND    PTL.ID	 = PTS.PTL_ID "
    +  "  AND    PTS.ID     = TSU.PTS_ID "
    +  "  AND    TSU.USR_ID = USR.ID "
    +  "  AND    PTS.SEQ_NO = PRE.SEQ_NO "
    +  "  AND    PRE.ID = ? "
    +  "  AND    USR.ID = ?"; // User ID


    /*
     * Methods
     */

    /**
     * Get step authorized users.
     * 
     * @param procedureId Procedure ID.
     * @param sequenceNo Step sequence number.
     * @return step authorized users.
     */
    public List getStepUsers(Long procedureId, Integer sequenceNo) {
        // Debug query
        List params = new ArrayList();
        params.add(procedureId);
        params.add(sequenceNo);
        debugQuery(STEP_USERS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(STEP_USERS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            List usersList = new ArrayList();
            while (rs.next()) {
                ProcedureStepUserVO vo = new ProcedureStepUserVO();
                usersList.add(vo);

                vo.setStep(new ProcedureStepVO());
                vo.setUser(new UserVO());
                vo.getUser().setEmployee(new EmployeeVO());

                vo.getStep().setSequenceNo(sequenceNo);
                
                vo.getStep().setId(getLong(rs, "PRE_ID"));
                vo.getUser().setId(getLong(rs, "USR_ID"));
                vo.getUser().setName(getString(rs, "USERNAME"));
                vo.getUser().getEmployee().setId(getLong(rs, "EMP_ID"));
                vo.getUser().getEmployee().setEmployeeNameAr(getString(rs, "NAME_A"));
                vo.getUser().getEmployee().setEmail(getString(rs, "E_MAIL"));
            }

            return usersList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }

    /**
     * Check if this is a valid step user.
     * 
     * @param stepId Step ID.
     * @param userId Step user ID.
     * @return true if this is a valid step user.
     */
    public boolean isValidStepUser(Long stepId, Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(stepId);
        params.add(userId);
        debugQuery(IS_VALID_STEP_USER, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(IS_VALID_STEP_USER);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            return rs.next();

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
}