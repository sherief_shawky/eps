/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/10/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateReportDAO;
import ae.eis.eps.vo.ProcedureTemplateLogVO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateReportVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Procedure template reports access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateReportDAOImpl extends JdbcDataAccessObject  
                                   implements TemplateReportDAO {
    /*
     * JDBC SQL and DML statements.
     */

    /** Get field options. */
    private static final String GET_BY_TEMPLATE_ID
    = " SELECT ID,"
    +        " NAME_A,"
    +        " REPORT_TYPE,"
    +        " REPORT_LINK"
    + " FROM TF_EPS_TEMPLATE_REPORTS ETR,"
    +      " TF_EPS_REPORT_USERS ERU"
    + " WHERE ERU.ETR_ID = ETR.ID"
    +   " AND ETR.PTL_ID = ?" // Template ID
    +   " AND ERU.USR_ID = ?"; // User ID

    /** Get By Id. */
    private static final String GET_BY_ID
    = " SELECT ID,"
    +        " NAME_A,"
    +        " REPORT_TYPE,"
    +        " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_REPORT_TYPE'"
    +                                           ",REPORT_TYPE) REPORT_TYPE_DESC, "
    +        " REPORT_LINK"
    + " FROM TF_EPS_TEMPLATE_REPORTS "
    + " WHERE ID = ? "; // Report ID
    
    /** Find. */
    private static final String FIND_QUERY
    = " SELECT ID,"
    +        " NAME_A,"
    +        " REPORT_TYPE,"
    +        " PKG_DB_MIL_CORE_TOOLS.F_DB_GET_REF_CODE_DESC('TF_EPS_REPORT_TYPE'"
    +                                           ",REPORT_TYPE) REPORT_TYPE_DESC, "
    +        " REPORT_LINK"
    + " FROM TF_EPS_TEMPLATE_REPORTS "
    + " WHERE PTL_ID = ? "; // Template ID
    
    /** Add new report */
    private static final String ADD
        = " INSERT INTO TF_EPS_TEMPLATE_REPORTS("
        +        " ID,"
        +        " NAME_A,"
        +        " REPORT_LINK,"
        +        " REPORT_TYPE,"
        +        " PTL_ID,"
        +        " CREATED_BY,"
        +        " CREATION_DATE,"
        +        " UPDATED_BY,"
        +        " UPDATE_DATE) "
        +        " VALUES(?,"        // report id
        +        " ?,"              // report name
        +        " ?,"              // report link
        +        " ?,"              // report type
        +        " ?,"              // template id
        +        " ?,"              // created by
        +        " SYSDATE,"
        +        " ?,"              // updated by
        +        " SYSDATE) ";
    
    /** Update report */
    private static final String UPDATE_QUERY
        = " UPDATE TF_EPS_TEMPLATE_REPORTS "
        +    " SET NAME_A=?,"
        +        " REPORT_LINK=?,"
        +        " REPORT_TYPE=?,"
        +        " UPDATED_BY=?,"
        +        " UPDATE_DATE=SYSDATE "
        +  " WHERE ID = ? ";

    /** Update report */
    private static final String DELETE_QUERY
        = " DELETE FROM TF_EPS_TEMPLATE_REPORTS WHERE ID = ?";
        
    /** Has Parameters */        
    private static final String HAS_PARAMETERS 
        = " SELECT 1 FROM TF_EPS_REPORT_PARAMETERS WHERE ETR_ID = ? ";
        
        
    /** Add Log Query */        
    private static final String ADD_LOG_QUERY
        =    "INSERT INTO TF_EPS_PROCEDURE_TEMP_LOGS "
        +    "(ID,STATUS,VERSION_NO,NAME_E,ACTION_TYPE,PRIORITY,NAME_A, "
        +    "STATUS_DATE,PTL_ID,EMPLOYEE_NAME,CREATED_BY,CREATION_DATE,REMARKS) "
        +    "SELECT PTG_SEQ.NEXTVAL, "
        +          " PTL.STATUS,"
        +          " PTL.VERSION_NO,"
        +          " PTL.NAME_E,"
        +          " ?,"
        +          " PTL.PRIORITY,"
        +          " PTL.NAME_A,"
        +          " SYSDATE,"
        +          " PTL_ID,"
        +          " F_DB_GET_EMP_NAME_A(?),"
        +          " ?,"
        +          " SYSDATE,"
        +          " ETR.NAME_A" 
        +     " FROM TF_EPS_TEMPLATE_REPORTS ETR,"
        +          " TF_EPS_PROCEDURE_TEMPLATES PTL"
        +    " WHERE ETR.PTL_ID = PTL.ID"    
        +      " AND ETR.ID = ? ";
        
    /*
     * Methods
     */

    /**
     * Get related template reports.
     * 
     * @param templateId EPS Template ID.
     * @param userId Active user ID.
     * @return related template reports.
     */
    public List getByTemplateId(Long templateId, Long userId) {
        // Debug query
        List params = new ArrayList();
        params.add(templateId);
        params.add(userId);
        debugQuery(GET_BY_TEMPLATE_ID, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_BY_TEMPLATE_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();

            List reportsList = new ArrayList();
            while (rs.next()) {
                TemplateReportVO vo = new TemplateReportVO();
                vo.setTemplate(new ProcedureTemplateVO());
                reportsList.add(vo);
                
                vo.getTemplate().setId(templateId);
                vo.setId(getLong(rs, "ID"));
                vo.setNameAr(getString(rs, "NAME_A"));
                vo.setReportType(getInteger(rs, "REPORT_TYPE"));
                vo.setReportLink(getString(rs, "REPORT_LINK"));
            }

            return reportsList;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }
    }
    
    /**
     * Get related template reports.
     * 
     * @param reportId EPS Remplate ID.
     * @return related template reports.
     */
    public TemplateReportVO getById(Long reportId){
        // Debug query
        List params = new ArrayList();
        params.add(reportId);
        debugQuery(GET_BY_ID, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(GET_BY_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if(! rs.next()){
                return null;
            }
            
            TemplateReportVO vo = new TemplateReportVO();
            
            vo.setId(reportId);
            vo.setNameAr(getString(rs, "NAME_A"));
            vo.setReportType(getInteger(rs, "REPORT_TYPE"));
            vo.setReportTypeDesc(getString(rs, "REPORT_TYPE_DESC"));
            vo.setReportLink(getString(rs, "REPORT_LINK"));

            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }        
    }
    
    /**
     * Find template report
     * 
     * @param templateId EPS Template ID.
     * @param userId Active user ID.
     * @return template reports.
     */
    public SearchPageVO find(Long templateId,int pageNo){
       
        List params = new ArrayList();
        params.add(templateId);
        debugQuery(FIND_QUERY, params);
        
        PreparedStatement prepStmt = null;
        ResultSet resultSet = null;          
        
        try {
            int pageSize = getDefaultPageSize();
            int totalRecordCounts = getTotalCount(FIND_QUERY, params);
            prepStmt =doSearch(FIND_QUERY, params, pageNo , pageSize);
            resultSet=prepStmt .executeQuery();
            
            SearchPageVO  searchPage = new SearchPageVO(pageNo, pageSize ,
                                                             totalRecordCounts);
            while(resultSet.next()) {
                TemplateReportVO vo = new TemplateReportVO();
                vo.setTemplate(new ProcedureTemplateVO());
                
                vo.getTemplate().setId(templateId);
                vo.setId(getLong(resultSet, "ID"));
                vo.setNameAr(getString(resultSet, "NAME_A"));
                vo.setReportType(getInteger(resultSet, "REPORT_TYPE"));
                vo.setReportTypeDesc(getString(resultSet,"REPORT_TYPE_DESC"));
                vo.setReportLink(getString(resultSet, "REPORT_LINK"));
                
                searchPage.addRecord(vo);
            }
            
            return searchPage;
        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(resultSet);
            close(prepStmt);
        }         
    }
    
    /**
     * Add Template Report
     * 
     * @return Template Report Value Object
     * @param vo Template Report Value Object
     */
    public TemplateReportVO add(TemplateReportVO vo){
        PreparedStatement stm = null;
        
        try {
            Long newId = generateSequence("ETR_SEQ");
            
            // Debug query
            List params = new ArrayList();
            params.add(newId);
            params.add(vo.getNameAr());
            params.add(vo.getReportLink());
            params.add(vo.getReportType());
            params.add(vo.getTemplate().getId());
            params.add(vo.getCreatedBy());
            params.add(vo.getUpdatedBy());
                 
            debugQuery(ADD, params);
        
            // Execute query
            stm = getConnection().prepareStatement(ADD);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            vo.setId(newId);

            addLog(vo, ProcedureTemplateLogVO.ACTION_ADD_REPORT);
            
            return vo;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Update Template Report
     * 
     * @param vo Template Report Value Object
     */
    public void update(TemplateReportVO vo){
        PreparedStatement stm = null;
        int count = 0;
        
        try {
            
            // Debug query
            List params = new ArrayList();
        
            params.add(vo.getNameAr());
            params.add(vo.getReportLink());
            params.add(vo.getReportType());
            params.add(vo.getUpdatedBy());
            params.add(vo.getId());
                 
            debugQuery(UPDATE_QUERY, params);
        
            // Execute query
            stm = getConnection().prepareStatement(UPDATE_QUERY);
            setQueryParameters(stm, params);
            count = stm.executeUpdate();
            
            if(count <=0){
                throw new DataAccessException(
                  new StringBuffer("Failed to update report, not found reportId: ")
                    .append(vo.getId()).toString());                
            }

            addLog(vo, ProcedureTemplateLogVO.ACTION_UPDATE_REPORT);

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
 
     /**
     * delete report
     * 
     * @param vo Template Report Value Object
     */
    public void delete(TemplateReportVO vo){

        addLog(vo, ProcedureTemplateLogVO.ACTION_DELETE_REPORT);
        
        PreparedStatement stm = null;
        try {
            
            // Debug query
            List params = new ArrayList();
            params.add(vo.getId());
                 
            debugQuery(DELETE_QUERY, params);
        
            // Execute query
            stm = getConnection().prepareStatement(DELETE_QUERY);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }         
    }
    
    /**
     * Has Parameters
     * 
     * @return true if this report has at least one parameters, 
     *                                                  otherwise return false
     * @param reportId Report ID
     */
    public boolean hasParameters(Long reportId){
        // Debug query
        List params = new ArrayList();
        params.add(reportId);
        debugQuery(HAS_PARAMETERS, params);

        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            // Execute query
            stm = getConnection().prepareStatement(HAS_PARAMETERS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if(rs.next()){
                return true;
            }
            
            return false;

        } catch (DataAccessException ex) {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }         
    }
    
    /**
     * Add Log
     * 
     * @param remarks
     * @param actionType
     * @param vo Template Report Value Object
     */
    private void addLog(TemplateReportVO vo,Integer actionType){
        // Debug message
        List params = new ArrayList();
        
        params.add(actionType);
        params.add(vo.getCreatedBy());
        params.add(vo.getCreatedBy());
        params.add(vo.getId());
         
        debugQuery(ADD_LOG_QUERY, params);
        
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(ADD_LOG_QUERY);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }        
    }
}