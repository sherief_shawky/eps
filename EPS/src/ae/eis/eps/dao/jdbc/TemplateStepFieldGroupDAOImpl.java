/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  23/11/2009  - File created.
 */

package ae.eis.eps.dao.jdbc;

import ae.eis.eps.dao.TemplateStepFieldGroupDAO;
import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateStepFieldGroupVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessException;
import ae.eis.util.dao.jdbc.JdbcDataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Template step-fields group data access object JDBC implementation class.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TemplateStepFieldGroupDAOImpl extends JdbcDataAccessObject  
                                           implements TemplateStepFieldGroupDAO {
    /*
     * Constants
     */

    /** Find step groups. */
    private static final String FIND_BY_STEP_ID
    = " SELECT TFG.ID,"
    +        " TFG.NAME_A,"
    +        " TFG.NAME_E,"
    +        " TFG.ORDER_NO"
    + " FROM TF_EPS_TEMP_STEP_FIELD_GROUPS TFG"
    + " WHERE TFG.PTS_ID = ?" // Step ID
    + " ORDER BY TFG.ORDER_NO";

    /** Create new step-fields group. */
    private static final String CREATE_GROUP
    = " INSERT INTO TF_EPS_TEMP_STEP_FIELD_GROUPS"
    + " (ID,PTS_ID,NAME_A,NAME_E,ORDER_NO,LABEL_WIDTH,SINGLE_COLUMN_FIELD_WIDTH,"
    +   "MULTI_COLUMN_FIELD_WIDTH,CREATED_BY,CREATION_DATE,UPDATED_BY,UPDATE_DATE)"
    + " VALUES(?," // ID
    +        " ?," // PTS_ID
    +        " ?," // NAME_A
    +        " ?," // NAME_E
    +        " ?," // ORDER_NO
    +        " ?," // LABEL_WIDTH
    +        " ?," // SINGLE_COLUMN_FIELD_WIDTH
    +        " ?," // MULTI_COLUMN_FIELD_WIDTH
    +        " ?," // CREATED_BY
    +        " SYSDATE,"
    +        " ?," // UPDATED_BY
    +        " SYSDATE"
    + ")";

    /** Update step-fields group. */
    private static final String UPDATE_GROUP
    = " UPDATE TF_EPS_TEMP_STEP_FIELD_GROUPS"
    + " SET NAME_A = ?," // NAME_A
    +     " NAME_E = ?," // NAME_E
    +     " ORDER_NO = ?," // ORDER_NO
    +     " LABEL_WIDTH = ?," // LABEL_WIDTH
    +     " SINGLE_COLUMN_FIELD_WIDTH = ?," // SINGLE_COLUMN_FIELD_WIDTH
    +     " MULTI_COLUMN_FIELD_WIDTH = ?," // MULTI_COLUMN_FIELD_WIDTH
    +     " UPDATED_BY = ?," // UPDATED_BY
    +     " UPDATE_DATE = SYSDATE"
    + " WHERE ID = ?"; // ID

    /** Check if a step group already exist. */
    private static final String IS_GROUP_EXIST
    = " SELECT 1 FROM TF_EPS_TEMP_STEP_FIELD_GROUPS"
    + " WHERE PTS_ID = ?"
    +   " AND ORDER_NO = ?";
    
    /** Check if a group has group fields. */
    private static final String HAS_GROUP_FIELDS
    = " SELECT 1 "
    +"  FROM " 
    +"      TF_EPS_TEMP_STEP_FIELD_GROUPS TFG,"
    +"      TF_EPS_TEMP_STEP_FIELDS TSL"
    +"  WHERE TSL.TFG_ID= TFG.ID "
    +"  AND   TFG.ID     = ?";

    /** Delete step-fields group. */
    private static final String DELETE_GROUP
    = "DELETE TF_EPS_TEMP_STEP_FIELD_GROUPS WHERE ID = ?";

    /** Get step-fields group by ID. */
    private static final String GET_BY_ID
    = " SELECT TFG.NAME_A,"
    +        " TFG.NAME_E,"
    +        " TFG.ORDER_NO,"
    +        " TFG.PTS_ID,"
    +        " TFG.LABEL_WIDTH,"
    +        " TFG.SINGLE_COLUMN_FIELD_WIDTH,"
    +        " TFG.MULTI_COLUMN_FIELD_WIDTH,"
    +        " PTS.STEP_TYPE,"
    +        " PTS.PTL_ID,"
    +        " PTL.STATUS AS PTL_STATUS"
    + " FROM TF_EPS_TEMP_STEP_FIELD_GROUPS TFG,"
    + " TF_EPS_PROCEDURE_TEMP_STEPS PTS,"
    + " TF_EPS_PROCEDURE_TEMPLATES PTL"
    + " WHERE TFG.PTS_ID = PTS.ID"
    +   " AND PTS.PTL_ID = PTL.ID"
    +   " AND TFG.ID = ?"; // Group ID

    /*
     * Methods
     */

    /**
     * Find step-fields groups.
     * 
     * @param stepId Template step ID.
     * @param pageNo Current search page number.
     * @return step-fields groups.
     */
    public SearchPageVO findByStepId(Long stepId, int pageNo) {
        List params = new ArrayList();
        params.add(stepId);
        debugQuery(FIND_BY_STEP_ID, params);

        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            int pageSize = getDefaultPageSize();
            int totalCount = getTotalCount(FIND_BY_STEP_ID, params);
            stm = doSearch(FIND_BY_STEP_ID, params, pageNo , pageSize);
            rs = stm.executeQuery();

            SearchPageVO page = new SearchPageVO(pageNo, pageSize ,totalCount);
            while(rs.next()) {
                TemplateStepFieldGroupVO vo = new TemplateStepFieldGroupVO();
                vo.setTemplateStep(new TemplateStepVO());
                page.addRecord(vo);
                
                vo.getTemplateStep().setId(stepId);
                vo.setId(getLong(rs, "ID"));
                vo.setNameAr(rs.getString("NAME_A"));
                vo.setNameEn(rs.getString("NAME_E"));
                vo.setOrderNo(getInteger(rs, "ORDER_NO"));
            }

            return page;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }     
    }

    /**
     * Create new template step-fields group.
     * 
     * @param vo group info.
     * @return new group details.
     */
    public TemplateStepFieldGroupVO create(TemplateStepFieldGroupVO vo) {
        PreparedStatement stm = null;
        try {
            // Generate new sequence
            Long newId = generateSequence("TFG_SEQ");

            // Debug query
            List params = new ArrayList();
            params.add(newId);
            params.add(vo.getTemplateStep().getId());
            params.add(vo.getNameAr());
            params.add(vo.getNameEn());
            params.add(vo.getOrderNo());
            params.add(vo.getLabelWidth());
            params.add(vo.getSingleColumnFieldWidth());
            params.add(vo.getMultiColumnFieldWidth());            
            params.add(vo.getCreatedBy());
            params.add(vo.getCreatedBy());
            debugQuery(CREATE_GROUP, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(CREATE_GROUP);
            setQueryParameters(stm, params);
            stm.executeUpdate();
            
            // Save new ID and return updated value object
            vo.setId(newId);
            return vo;

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Check if a step group already exist.
     * 
     * @param stepId Template step ID.
     * @param orderNo Group order number.
     * @return true if a step group already exist.
     */
    public boolean isGroupExist(Long stepId, Integer orderNo) {
        List params = new ArrayList();
        params.add(stepId);
        params.add(orderNo);
        debugQuery(IS_GROUP_EXIST, params);

        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(IS_GROUP_EXIST);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            return rs.next();

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }     
    }

    /**
     * Delete step-fields group.
     * 
     * @param groupId Group ID.
     */
    public void delete(Long groupId) {
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(groupId);
            debugQuery(DELETE_GROUP, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(DELETE_GROUP);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if (count != 1) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to delete group, ID: ").append(groupId)
                        .toString());
            }
            
        } catch (DataAccessException ex)  {
            throw ex;
        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }

    /**
     * Get step-fields group by ID.
     * 
     * @param groupId Group ID.
     * @return Group info.
     */
    public TemplateStepFieldGroupVO getById(Long groupId) {
        List params = new ArrayList();
        params.add(groupId);
        debugQuery(GET_BY_ID, params);

        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(GET_BY_ID);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if (! rs.next()) {
                return null;
            }
            
            TemplateStepFieldGroupVO vo = new TemplateStepFieldGroupVO();

            vo.setId(groupId);
            vo.setNameAr(rs.getString("NAME_A"));
            vo.setNameEn(rs.getString("NAME_E"));
            vo.setOrderNo(getInteger(rs, "ORDER_NO"));
            vo.setLabelWidth(getInteger(rs,"LABEL_WIDTH"));
            vo.setSingleColumnFieldWidth(getInteger(rs,"SINGLE_COLUMN_FIELD_WIDTH"));
            vo.setMultiColumnFieldWidth(getInteger(rs,"MULTI_COLUMN_FIELD_WIDTH"));

            vo.setTemplateStep(new TemplateStepVO());
            vo.getTemplateStep().setId(getLong(rs, "PTS_ID"));
            vo.getTemplateStep().setStepType(getInteger(rs, "STEP_TYPE"));
            
            vo.getTemplateStep().setTemplate(new ProcedureTemplateVO());
            vo.getTemplateStep().getTemplate().setId(getLong(rs, "PTL_ID"));
            vo.getTemplateStep().getTemplate().setStatus(getInteger(rs, "PTL_STATUS"));

            return vo;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }     
    }

    /**
     * Update step-fields group.
     * 
     * @param vo group info.
     */
    public void update(TemplateStepFieldGroupVO vo) {
        PreparedStatement stm = null;
        try {
            // Debug query
            List params = new ArrayList();
            params.add(vo.getNameAr());
            params.add(vo.getNameEn());
            params.add(vo.getOrderNo());
            params.add(vo.getLabelWidth());
            params.add(vo.getSingleColumnFieldWidth());
            params.add(vo.getMultiColumnFieldWidth());
            params.add(vo.getUpdatedBy());
            params.add(vo.getId());
            debugQuery(UPDATE_GROUP, params);

            // Execute DML statement
            stm = getConnection().prepareStatement(UPDATE_GROUP);
            setQueryParameters(stm, params);
            int count = stm.executeUpdate();
            
            if (count != 1) {
                throw new DataAccessException(new StringBuffer(
                    "Failed to update group, ID: ").append(vo.getId())
                        .toString());
            }

        } catch (Exception ex)  {
            throw new DataAccessException(ex);
        } finally {
            close(stm);
        }
    }
    
    /**
     * Check if group field has fields
     * 
     * @return true if group fields has fields
     * @param groupId group ID
     */
    public boolean hasGroupFields(Long groupId) {
        List params = new ArrayList();
        params.add(groupId);
        debugQuery(HAS_GROUP_FIELDS, params);

        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            stm = getConnection().prepareStatement(HAS_GROUP_FIELDS);
            setQueryParameters(stm, params);
            rs = stm.executeQuery();
            
            if(rs.next()){
              return true;  
            }
            
            return false;

        } catch(Exception ex) {
            throw new DataAccessException(ex);
        } finally {
            close(rs);
            close(stm);
        }     
    }
}