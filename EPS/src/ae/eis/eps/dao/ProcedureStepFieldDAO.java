/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.util.dao.DataAccessObject;

import java.util.List;

/**
 * Procedure step-field data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureStepFieldDAO extends DataAccessObject {
    /**
     * Get requester step fields.
     * 
     * @param procedureId procedure ID.
     * @return requester step fields.
     */
    List getRequesterStepFields(Long procedureId);

    /**
     * Get active procedure step fields.
     * 
     * @param procedureId procedure ID.
     * @return active procedure step fields.
     */
    List getActiveStepFields(Long procedureId);
}