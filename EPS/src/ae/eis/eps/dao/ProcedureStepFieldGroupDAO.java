/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  23/11/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.util.dao.DataAccessObject;

/**
 * Procedure step-fields group data access object interface.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureStepFieldGroupDAO extends DataAccessObject {
}