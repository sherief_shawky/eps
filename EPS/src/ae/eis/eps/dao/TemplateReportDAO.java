/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/10/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateReportVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure template reports data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateReportDAO extends DataAccessObject {

    /**
     * Get related template reports.
     * 
     * @param templateId EPS Template ID.
     * @param userId Active user ID.
     * @return related template reports.
     */
    List getByTemplateId(Long templateId, Long userId);
    
    /**
     * Get related template reports.
     * 
     * @param reportId EPS Remplate ID.
     * @return related template reports.
     */
    TemplateReportVO getById(Long reportId);
    
    /**
     * Find template report
     * 
     * @param templateId EPS Template ID.
     * @param pageNo Page Number
     * @return template reports.
     */
    SearchPageVO find(Long templateId,int pageNo);
    
    /**
     * Add Template Report
     * 
     * @return Template Report Value Object
     * @param vo Template Report Value Object
     */
    TemplateReportVO add(TemplateReportVO vo);

    /**
     * Update Template Report
     * 
     * @param vo Template Report Value Object
     */
    void update(TemplateReportVO vo);    
    
    /**
     * delete report
     * 
     * @param vo Template Report Value Object
     */
    void delete(TemplateReportVO vo);
    
    /**
     * Has Parameters
     * 
     * @return true if this report has at least one parameters, 
     *                                                  otherwise return false
     * @param reportId Report ID
     */
    boolean hasParameters(Long reportId);
}