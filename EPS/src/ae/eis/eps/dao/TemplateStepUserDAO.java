/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 * 
 * 1.01  Tariq Abu Amireh   17/06/2009  - re-Name File from Step Group 
 *                                                       to Step User
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateStepUserVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template step security group data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateStepUserDAO extends DataAccessObject {

    /**
     * Create new procedure template step security group.
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    TemplateStepUserVO create(TemplateStepUserVO vo);

    /**
     * Delete procedure template step security group.
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    void delete(TemplateStepUserVO vo);
    
    /**
     * Get template step Security Groups
     * 
     * @param vo Procedure template step security group value object.
     * @return Procedure template step security group value object created.
     */
    SearchPageVO find(int pageNo,TemplateStepUserVO vo);
    
    /**
     * Find template step Security Groups
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param pageNo page Number 
     */
    SearchPageVO findUsers(int pageNo,TemplateStepUserVO vo);    
    
    /**
     * Has Invalid Security Groups
     * The step must have granted security group
     * -  EPS_PTT_008
     * 
     * @return boolean (true/false)
     * @param templateId Template Id
     */
    List getInvalidSecurityUsersSteps(Long templateId);
       
    /**
     * Has InActive Users
     * Can't activate the procedure because the step has user with status not active
     * - EPS_TSG_002
     * 
     * @return boolean (true/false)
     * @param templateId template Id
     */
    List getInActiveUsersSteps(Long templateId);        
    
    /**
     * Get Users Of Step.
     * 
     * @param vo Procedure template step value object.
     * 
     * @return List of security groups value object.
     */
     List getUsersOfStep(TemplateStepUserVO vo);
}