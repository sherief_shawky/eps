/*
 * Copyright (c) i-Soft 2007.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   18/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateStepNotificationVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template Step Notification Data Access Object
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public interface TemplateStepNotificationDAO extends DataAccessObject {
    
    /**
     * Create New Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    TemplateStepNotificationVO create(TemplateStepNotificationVO vo);
    
    /**
     * Update Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public void update(TemplateStepNotificationVO vo);    
    
    /**
     * find Template Step Notification
     * 
     * @return SearchPageVO
     * @param pageNo
     * @param Template Step Notification Value Object
     */
    public SearchPageVO find(int pageNo,TemplateStepNotificationVO vo);   
    
    /**
     * get Template Step Notification
     * 
     * @return Template Step Notification Value Object
     * @param ntfStepId
     */
    public TemplateStepNotificationVO getById(Long ntfStepId);

    /**
     * Delete Template Step Notification .
     * 
     * @return Template Step Notification Value Object
     * @param vo
     */
    public void delete(TemplateStepNotificationVO vo);
    
    /**
     * Has Invalid Notifications
     * The notification message must be added when the step type is notification
     * -  EPS_PTT_010
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidNotificationSteps(Long templateId);
}