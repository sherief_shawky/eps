/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/06/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.util.dao.DataAccessObject;
import java.util.List;

/**
 * Procedure field option data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureFieldOptionDAO extends DataAccessObject {
    /**
     * Get procedure field options.
     * 
     * @param fieldId procedure field ID.
     * @return procedure field options.
     */
    List getFieldOptions(Long fieldId);
}