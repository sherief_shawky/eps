/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.ProcedureAttachmentVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure attachments data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureAttachmentDAO extends DataAccessObject {
    /**
     * Save attachment.
     * 
     * @param vo Attachment value object.
     */
    void save(ProcedureAttachmentVO vo);

    /**
     * Get attachment content by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    ProcedureAttachmentVO getContentById(Long attachmentId);

    /**
     * Get procedure requester attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    SearchPageVO findRequesterAttchments(int pageNo, Long procedureId);


    /**
     * Get end step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    SearchPageVO findEndStepAttchments(int pageNo, Long procedureId);
    
    /**
     * Get attachment info by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    ProcedureAttachmentVO getInfoById(Long attachmentId);

    /**
     * Get procedure active step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    SearchPageVO findActiveStepAttchments(int pageNo, Long procedureId);


    /**
     * Get procedure active step attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     * @param userId current user ID.
     */
    SearchPageVO findActiveStepRequesterAttchments(int pageNo, 
                                                   Long procedureId,
                                                   Long userId);

    /**
     * Get procedure active step-user attachment.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
    SearchPageVO findActiveStepUsersAttchments(int pageNo, 
                                              Long procedureId, 
                                              Long userId);

    /**
     * Delete attachment.
     * 
     * @param procedureId Procedure ID.
     * @param attachmentId Attachment ID.
     * @param userId User ID
     * @return true if the attachment deleted successfuly.
     */
    boolean delete(Long procedureId, Long attachmentId, Long userId);

    /**
     * Check if the user is allowed to delete this attachment.
     * 
     * @param procedureId Procedure ID.
     * @param attachmentId Attachment ID.
     * @param userId User ID
     * @return <LU>
     *           <LI>0</LI>: User is allowed to delete this attachment.
     *           <LI>1</LI>: User is not allowed to delete requester attachments.
     *           <LI>2</LI>: User is not allowed to delete other steps attachments.
     *           <LI>3</LI>: Active attachment not found.
     *         </LU>
     */
    int isAllowedToDelete(Long procedureId, Long attachmentId, Long userId);
    
    /**
     * Get Attachment Count
     * 
     * @param procedureId Procedure ID.
     */
    Long getAttachmentCount(Long procedureId);
    
    /**
     * find Step Attachments.
     * 
     * @param pageNo Active page number.
     * @param procedureId Procedure ID.
     */
     SearchPageVO findStepAttachments(int pageNo, Long procedureId);
     
     
     /**
     * Copy transaction attachments.
     * 
     * @param transactionVO Transaction Value Object.
     * @param procedureVO Procedure Value Object.
     */
    void copyTrsAttachments(TransactionVO transactionVO, ProcedureVO procedureVO);
    
}