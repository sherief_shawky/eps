/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   14/10/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.ReportUserVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

/**
 * Procedure template report-users data access object intefrace.
 *
 * @author Tariq Abu Amireh
 * @version 1.00
 */
public interface ReportUserDAO extends DataAccessObject {
    
    /**
     * Find report parameter
     * 
     * @return report parameters
     * @param pageNo pagination Page Number
     * @param reportId EPS report ID
     */
    SearchPageVO find(Long reportId, int pageNo);
 
    /**
     * Find template step Security Groups
     * 
     * @return Search Page Value Object
     * @param vo Report User Value Object
     * @param pageNo page Number 
     */
    SearchPageVO findUsers(ReportUserVO vo,int pageNo); 
    
    /**
     * Add Security User
     * 
     * @param vo Report User Value Object
     */
    void addSecurityUser(ReportUserVO vo);
    
    /**
     * Delete Security User
     * 
     * @param reportId Report Id
     * @param userId User Id
     */
    void deleteSecurityUser(Long userId,Long reportId);
    
    /**
     * Is User Exist
     * 
     * @return true if the user is exist for current report, otherwise return false
     * @param reportId Report Id
     * @param userId User Id
     */
    boolean isUserExist(Long userId,Long reportId);
}