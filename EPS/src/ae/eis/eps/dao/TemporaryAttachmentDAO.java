/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemporaryAttachmentVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

/**
 * Procedure temporary attachments data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemporaryAttachmentDAO extends DataAccessObject {
    /**
     * Save attachment.
     * 
     * @param vo Attachment value object.
     */
    void save(TemporaryAttachmentVO vo);

    /**
     * Get attachment content by ID.
     * 
     * @param attachmentId Attachment ID.
     */
    TemporaryAttachmentVO getContentById(Long attachmentId);
    
    /**
     * find attachment
     * 
     * @return list of attachment
     * @param sequenceNo
     */
    SearchPageVO find(int pageNo,Long sequenceNo);

    /**
     * Delete attachment.
     * 
     * @param attachmentId Attachment ID.
     * @return true if the attachment was deleted successfully.
     */
    boolean delete(Long attachmentId);

    /**
     * Get total number of attachments.
     * 
     * @param sequenceNo Attachment sequence number.
     * @return total number of attachments.
     */
    int getAttachmentsCount(Long sequenceNo);
}