/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateStepForwardVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template step forward data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateStepForwardDAO extends DataAccessObject {
  
    /**
     * Create new procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public TemplateStepForwardVO create(TemplateStepForwardVO vo);
    
    /**
     * Delete procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public void delete(TemplateStepForwardVO vo);
    
    /**
     * Delete procedure template step forward.
     * 
     * @param vo Procedure template step forward value object.
     * @return Procedure template step forward value object created.
     */
    public void deleteByStepId(Long stepId);
    
    /**
     * Find procedure template step forward.
     * 
     * @param ptsId Procedure template step Id
     * @return Procedure template step forward value object created.
     */
    public SearchPageVO find(int pageNo,Long ptsIds);    
    
    /**
     * Get Sequense Number
     * 
     * @return max of sequense number
     * @param Procedure Template Id
     */
    public Long getSequenseNo(Long ptlId);    
    
    /**
     * Has Rejected Forward
     * return TRUE if this step has rejected forward, otherwise return FALSE
     * 
     * @return boolean (true/false)
     * @param stepId
     */
    public boolean hasRejectedForward(Long stepId);
    
    /**
     * Has Invalid Forwards
     * The step must have defined next allowed steps
     * -  EPS_PTT_007
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    public List getInvalidForwards(Long templateId);
    
    /**
     * Is Valid Forward
     * Cannot transfer from begin to end
     * -  EPS_TSF_005
     * 
     * @return boolean (true/false)
     * @param parentStepId
     * @param nextStepId
     */
    public boolean isValidForward(Long parentStepId,Long nextStepId);
    
    /**
     * Is Exist Forward
     * Cannot transfer to same step twice
     * -  EPS_TSF_006
     * 
     * @return boolean (true/false)
     * @param nextStepId
     * @param parentStepId
     */
    public boolean isForwardExist(Long parentStepId,Long nextStepId);
    
    /**
     * Find Procedure Forword Steps
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    public SearchPageVO findForwardSteps(int pageNo,TemplateStepVO vo);    
    
    /**
     * Get Rejected Step Forwards
     * The step cannot have more than one allowed next step with action type reject
     *  - EPS_TSF_002
     * 
     * @return Number of Rejected forwards
     * @param templateId Template Id
     */
    List getRejectedStepForwards(Long templateId);
    
    /**
     * Is Valid Rejected Forward
     *  - EPS_TSF_003
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    List getInValidRejectedForwardSteps(Long templateId);    
    
    /**
     * Is Valid End Step Forward
     * Cannot add allowed nextr step for step has step type end
     *  -  EPS_TSF_004
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    boolean isValidEndStepForward(Long templateId);
    
    /**
     * Is Valid Allowed Step
     * Must Define step forward with action type reject for step type human 
     *              action and has allowed action which is reject
     * -  EPS_TSF_007
     * 
     * @return boolean (true / false)
     * @param templateId
     */
    List getInValidAllowedStep(Long templateId);    
    
    /**
     * Get Non Approved Forward Steps
     * cannot activate procedure if there is forward step without approved forwards
     *  - EPS_TSF_009
     * 
     * @return list of non approved forward steps
     * @param templateId template Id
     */
    List getNonApprovedForwardSteps(Long templateId);
}