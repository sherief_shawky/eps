/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 */
 
package ae.eis.eps.dao;

import ae.eis.eps.vo.SearchResultVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

/**
 * Search result data access object intefrace.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public interface SearchResultDAO  extends DataAccessObject  {
   
   /**
     * Find search result
     * 
     * @return searchPageVO search page value object
     * @param templateId template ID
     * @param pageNo page number
     */
    SearchPageVO find(int pageNo, Long templateId);
    
    /**
     * Delete search result 
     * 
     * @param templateId template ID
     * @param vo search result value object
     */
    void delete(SearchResultVO vo,Long templateId);
    
    /**
     * Creat search result 
     * 
     * @param vo search result value object
     */
    public void create(SearchResultVO vo);
    
    /**
     * Is search result exists 
     * 
     * @return true if exist
     * @param fieldId
     */
    boolean isSearchResultExists(Long fieldId);
    
    /**
     * Get by ID
     * 
     * @return  search result value object
     * @param searchResultId search result ID
     */
    SearchResultVO getById(Long searchResultId);
    
    /**
     * Update search result 
     * 
     * @param vo search result value object
     */
    void update(SearchResultVO vo);
    
    /**
     * Is order number exists 
     * 
     * @return true if exist
     * @param templateId template ID
     * @param orderNo order number
     * @param searchResultId search result ID
     */
     boolean isOrderNoExists(Long templateId, Integer orderNo,Long searchResultId);
      
    /**
     * Get Maximum Search Result Order Number
     * 
     * @return Maximum Search Result Order Number
     * @param templateId Template Id
     */
     Integer getMaxSearchResultOrderNo(Long templateId);
}