/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateStepFieldVO;
import ae.eis.util.dao.DataAccessObject;

import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template step-field data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateStepFieldDAO extends DataAccessObject {
    /**
     * Get requester step fields.
     * 
     * @param templateId procedure template ID.
     * @return requester step fields.
     */
    List getRequesterStepFields(Long templateId);
    
    /**
     * Get template step group fields.
     * 
     * @param pageNo Page Number
     * @param groupId Template step fields group ID.
     * @return template step group fields.
     */
    SearchPageVO findByGroupId(int pageNo, Long groupId);
    
    /**
     * Get step field info.
     * 
     * @param fieldId Template field ID.
     * @param groupId Fields group ID.
     * @return step field info.
     */
    TemplateStepFieldVO getStepField(Long fieldId, Long groupId);
    
    /**
     * Create Step Field
     * 
     * @return TemplateStepFieldVO Template Step Field Value Object
     * @param vo Template Step Field Value Object
     */
    TemplateStepFieldVO create(TemplateStepFieldVO vo);
    
    /**
     * Delete step field.
     * 
     * @param fieldId Template field ID.
     * @param groupId Fields group ID.
     */
    void delete(Long fieldId, Long groupId);
   
    /**
     * Update Step Field
     * 
     * @param vo Template Step Field Value Object
     */
    void update(TemplateStepFieldVO vo);
    
    /**
     * Is Empty Combo Box
     * 
     * @return boolean (true / false)
     * @param fieldId Field Id
     */
    boolean isEmptyComboBox(Long fieldId);
    
    /**
     * Is Field Exist
     * 
     * @return boolean (true / false)
     * @param stepId Step Id
     * @param fieldId Field Id
     */
    boolean isFieldExist(Long fieldId,Long stepId);
    
    /**
     * Is Valid Step Field
     * Cannot add fields to steps where step type is notification or business action
     *  - EPS_TSL_001
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    boolean isValidStepField(Long templateId);
    
    /**
     * Is Valid Editable Step Field
     * The Field must not be editable if the step type is end
     * -  EPS_TSL_005
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    boolean isValidEditableStepField(Long templateId);
    
    /**
     * Is Valid Mandatory Step Field
     * The Field must not be mandatory if the step type is end
     * -  EPS_TSL_006
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    boolean isValidMandatoryStepField(Long templateId);    

    /**
     * Is Mandatory And Not Editable
     * The Field must be editable if the field is mandatory
     * -  EPS_TSL_009
     * 
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    boolean isMandatoryAndNotEditable(Long templateId);   
    
    /**
     * Is Order Exist
     *  - EPS_TSL_011 : The order must be unique for the procedure
     *  
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param fieldId field Id
     * @param groupId group Id
     * @param order order
     */
    boolean isOrderExist(Long fieldId,Long groupId,Integer order);    
    
    /**
     * Get Maximum Step Field Order Number
     * 
     * @return Maximum Step Field Order Number
     * @param groupId Group Id
     * @param stepId Step Id
     */
    Integer getMaxStepFieldOrderNo(Long stepId,Long groupId);
 
    
    
}