/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mena Emiel  23/02/2011  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.util.dao.DataAccessObject;
import ae.eis.eps.vo.TableHeaderVO;

/**
 * Table header data access object interface.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public interface TableHeaderDAO extends DataAccessObject {
    /*
     * Methods
     */
     
    /**
     * Get Column Info
     * 
     * @param fieldId Field Id
     * @param columnCode Column Code
     * 
     * @return TableHeaderVO Table Header Info
     */
    TableHeaderVO getColumnInfo(Long fieldId, Integer columnCode);
}