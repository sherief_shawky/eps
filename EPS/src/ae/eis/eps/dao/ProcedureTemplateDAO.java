/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 * 
 * 1.01  Alaa Salem         04/01/2010  - Adding getByCode() Method.
 * 
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.ProcedureTemplateVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.util.HashMap;
import java.util.List;

/**
 * Procedure template data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ProcedureTemplateDAO extends DataAccessObject {
   
   /**
     * Create new procedure template.
     * 
     * @param vo Procedure template value object.
     * @return Procedure template value object created.
     */
    ProcedureTemplateVO create(ProcedureTemplateVO vo);
    
    /**
     * Update procedure template.
     * 
     * @param vo Procedure template value object.
     * @return Procedure template value object created.
     */
    void update(ProcedureTemplateVO vo);
    
    /**
     * find Procedure Template
     * 
     * @return SearchPageVO
     * @param pageNo
     * @param announcementVO
     */
    SearchPageVO find(int pageNo,ProcedureTemplateVO vo);    
    
    /**
     * get Procedure Template By Id
     * 
     * @return 
     * @param templateId
     */
    ProcedureTemplateVO getById(Long templateId);  
    
    /**
     * Get all active templates Templates
     * 
     * @return list of templates
     */
    List getAvailableTemplates();

    /**
     * Get all Templates
     * 
     * @return list of templates
     */
    List getAllTemplates();
    
    /**
     * Get Requester templates Templates
     * 
     * @return list of templates
     */
    List getRequesterTemplates(Long userId);
    
    /**
     * Activate procedure template.
     * 
     * @param templateId Procedure template ID.
     * @param username Current active username.
     * @param versionNo Version number.
     */
    void activate(Long templateId, String username,Integer versionNo);
    
    /**
     * De-activate procedure template.
     * 
     * @param templateId Procedure template ID.
     * @param username Current active username.
     */
    void deActivate(Long templateId, String username);
    
    /**
     * Is Active Procedure
     * return true if procedure template status is active, otherwise return false
     * 
     * @return boolean(true / false)
     */
    boolean isActiveProcedure(Long templateId);
    
    /**
     * Is Name Exists
     * 
     * @return true if template name already exists, otherwise retrun false
     * @param templateName
     */
    boolean isNameExists(String templateName);

    /**
     * Get user follow up templates.
     * 
     * @param userId User ID.
     * @return user follow up templates.
     */
    List getUserFollowupTemplates(Long userId);
    
    /**
     * get available template code
     * 
     * @return available template code
     */
    List getTemplateAvailableCode();
    
    /**
     * Is Code Exists
     * 
     * @return true if template code already exists, otherwise retrun false
     * @param code template code
     */
    boolean isCodeExists(Integer code,Long templateId);

    /**
     * get Procedure Template By Template Code
     * 
     * @return Procedure Template VO.
     * @param templateCode Procedure Template Code.
     */
    ProcedureTemplateVO getByCode(Integer templateCode);
    
    /**
     * Check if this template has active Procedure.
     * 
     * @param templateCode Template Code.
     * @param trafficId Traffic File Id.
     * @return true if this template has active Procedure.
     */
    boolean hasActiveProcedure(Integer templateCode, Long trafficId);
    
    /**
     * Get Initial Noc Templates.
     * 
     * @param templates codes .
     * @return List Of Initial Noc Templates.
     */
    List getInitialNocTemplates(String templateCodes);
    
    /**
     * get Procedure Template Step By Sequence Number.
     * 
     * @param procedureTemplateId : procedure Template Id.
     * @param sequenceNumber : sequence Number.
     * 
     * @return Template Step VO.
     */
     TemplateStepVO getProcedureTemplateStepBySequenceNumber(Long procedureTemplateId ,Long sequenceNumber);
     
    /**
     * Get Template By Code.
     * 
     * @return Procedure Template VO.
     */
     ProcedureTemplateVO getTemplateByCode(Integer code);

    /**
     * get Procedure Template By Service Code
     * 
     * @param serviceCode : Procedure Service Code.
     * @return Procedure Template VO.
     */
    ProcedureTemplateVO getByServiceCode(Integer serviceCode);

    /**
     * Is Template Available For User
     * 
     * @param templateId Template Id
     * @param userId User Id
     * 
     * @return true if tempalte available for user
     */
    boolean isTemplateAvailableForUser(Long templateId,Long userId);     

    /**
     * lookup for Procedure Templates based on query search
     * 
     * @param query : text to search
     * @param lang : language for logged-in user
     * @param paramsMap : extra where condetion parameters Map
     * 
     * @return list of Procedure Templates matches this query
     */
    List<ProcedureTemplateVO> lookup(String query, String lang, int pageSize, HashMap<String,String> paramsMap);

    /**
     *  validate (id, text) for Procedure Templates recived from lookup componant
     * @param id : id of the Procedure Templates
     * @param text : Procedure Templates description based on language
     * @param lang : language for loogged-in user
     * @return true if data is valid
     */
    boolean validateLookupValue(String id, String text, String lang);

}