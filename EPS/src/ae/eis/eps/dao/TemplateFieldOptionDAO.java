/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  04/06/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateFieldOptionVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.util.List;

/**
 * Procedure template field option data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateFieldOptionDAO extends DataAccessObject {
    
    /**
     * Get template field options.
     * 
     * @param fieldId Template field ID.
     * @return template field options.
     */
    List getFieldOptions(Long fieldId);
    
    /**
     * Find template field options. 
     * 
     * @return SearchPageVO Search Page Value Object
     * @param pageNo page Number
     * @param fieldId field Id
     */
    SearchPageVO find(int pageNo,Long fieldId);
    
    /**
     * Get template field option By Field Id and Order Number
     * 
     * @return TemplateFieldOptionVO Template Field Option Value Object
     * @param orderNo Order Number
     * @param fieldId Field Id
     */
    TemplateFieldOptionVO get(Long fieldId, Integer orderNo);
    
    /**
     * Create Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    void createOption(TemplateFieldOptionVO vo);
    
    /**
     * Update Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    void updateOption(TemplateFieldOptionVO vo,Integer oldOrderNo);
    
    /**
     * Delete Field Option
     * 
     * @param vo Template Field Option Value Object
     */
    void deleteOption(TemplateFieldOptionVO vo); 
    
    /**
     * Is Arabic Name Exist
     *  - EPS_TFO_004
     *  The Arabic name must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param nameAr Arabic Name
     */
    boolean isArabicNameExist(Long fieldId,String nameAr,String oldNameAr);
    
    /**
     * Is Option Exist
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param orderNo Order Number
     */
    boolean isOptionExist(Long fieldId,Integer orderNo,String oldNameAr);    
    
    /**
     * Is Value Exist
     *  - EPS_TFO_006
     *  The Value must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param value Option Value
     */
    boolean isValueExist(Long fieldId,String value,String oldValue);
    
    /**
     * Is English Name Exist
     *  - EPS_TFO_007
     *  The Arabic name must be unique in the list of values
     *  
     * @return boolean
     * @param fieldId Field Id
     * @param nameEn English Name
     */
    boolean isEnglishNameExist(Long fieldId,String nameEn,String oldNameEn);    
    
}