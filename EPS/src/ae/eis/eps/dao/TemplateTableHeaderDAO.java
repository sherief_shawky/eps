/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Tariq Abu Amireh   30/12/2009  - File created.
 */
 
package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateTableHeaderVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Template Table Header Data Access Object
 * 
 * @version 1.0
 * @author Tariq Abu Amireh
 */
public interface TemplateTableHeaderDAO extends DataAccessObject {

    /**
     * Find
     * 
     * @return list of table headers
     * @param fieldId Field Id
     */
    SearchPageVO find(int pageNo, Long fieldId);
    
    /**
     * Get Table Header Maximum Order Number
     * 
     * @return Table Header Maximum Order Number
     * @param fieldId Field Id
     */
    Integer getTableHeaderMaxOrderNo(Long fieldId);
    
    /**
     * Add Template Table Header
     * 
     * @return table header id
     * @param vo Template Table Header Value Object
     */
    Long add(TemplateTableHeaderVO vo);
    
    /**
     * Get By Header Id
     * 
     * @return Template Table Header Value Object
     * @param headerId Header Id
     */
    TemplateTableHeaderVO getById(Long headerId);
    
    /**
     * Update
     * 
     * @param vo Template Table Header Value Object
     */
    void update(TemplateTableHeaderVO vo);
    
    /**
     * Delete
     * 
     * @param headerId Header Id
     */
    void delete(Long headerId);
    
    /**
     * Is Arabic Name Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param nameA Arabic Name
     */
    boolean isArabicNameExist(String nameA,Long fieldId,Long headerId);
    
    /**
     * Is English Name Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param nameE English Name
     */
    boolean isEnglishNameExist(String nameE,Long fieldId,Long headerId);
    
    /**
     * Is Exist
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     * @param orderNo order Number
     */
    boolean isOrderExist(Long orderNo,Long fieldId,Long headerId);    
    
    /**
     * Field Template Has Header
     * 
     * @return boolean ( true / false )
     * @param fieldId Field Id
     */ 
    boolean isFieldTemplateHasHeader(Long fieldId);
    
    /**
     * Is Template Tables Has Header
     * 
     * @return boolean ( true / false )
     * @param templateId Template Id
     */
    boolean isTemplateTablesHasHeader(Long templateId);
    
    /**
     * Get Table Header Maximum Code
     * 
     * @param fieldId Field Id
     * @return Table Header Maximum Code
     */
    Integer getTableHeaderMaxCode(Long fieldId);
    
    /**
     * Is Column Mandatory
     * 
     * @param fieldId Field Id
     * @param columnCode Column Code
     * 
     * @return True if the column mandatory
     *         False if the column optional
     */
    boolean isColumnMandatory(Long fieldId, Integer columnCode);
    
    /**
     * Get Headers  
     * 
     * @param fieldCode Field Code
     * 
     * @return List Of Template Table Header Info
     */
    List getHeaders(Integer fieldCode);
    
}  