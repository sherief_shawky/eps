/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  11/05/2009  - File created.
 * 
 * 1.02  Moh Fayek          17/07/2012  - TRF-5237 
 * 
 * 1.03  Sami Abudayeh      27/06/2012  - Adding find(Long transactionId,int pageNo)
 * 
 * 1.04  Mohammad Ababneh   12/08/2012  - Adding getAssessmentDetails , getAssessmentDetailsByProcedureId
 * 
 * 1.05  Mahmoud Atiyeh     15/05/2013  - Added getProceduresToBeMovedToDeliveryStep() method.
 * 
 * 1.06  Sami Abudayeh      19/092013   - Add assignCoordinatorTaskNote(ProcedureVO vo) method
 * 
 */

package ae.eis.eps.dao;

import ae.eis.common.vo.TrafficFileVO;
import ae.eis.eps.vo.EpsVerifySearchVO;
import ae.eis.eps.vo.ProcedureVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import ae.eis.util.web.UserProfile;

import java.util.Date;
import java.util.List;

/**
 * Procedure data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.03
 */
public interface ProcedureDAO extends DataAccessObject {
    /**
     * Create new procedure.
     * 
     * @param vo Procedure value object.
     * @param tempAttachmentsRefNo Temporal attachments reference numbet.
     * @return Created procedure ID.
     */
    Long create(ProcedureVO vo, Long tempAttachmentsRefNo);

    /**
     * Close procedure.
     * 
     * @param procedureId Procedure ID.
     * @param username Current active employee username.
     * @return True if the procedure were closed successfully.
     */
    boolean closeProcedure(Long procedureId, String username);
    
    /**
     * Save user approve action.
     * 
     * @param vo Procedure value object.
     */
    void approve(ProcedureVO vo);

    /**
     * Process user reject action.
     * 
     * @param vo Procedure step value object.
     */
    void reject(ProcedureVO vo);
    
    /**
     * Process reject action with no security.
     * 
     * @param vo Procedure step value object.
     */
    void rejectNoSecurity(ProcedureVO vo);
    
    /**
     * Process approve Completed Procedure with no security.
     * 
     * @param vo Procedure step value object.
     */
    void approveCompletedProcedure(ProcedureVO vo);
    
    /**
     * Process user push-back action.
     * 
     * @param vo Procedure step value object.
     */
    void pushBack(ProcedureVO vo);

    /**
     * Activate next procedure step.
     * 
     * @param procedureId Procedure ID.
     */
    void activateNextStep(Long procedureId, String username);

    /**
     * Activate next procedure step.
     * 
     * @param procedureId Procedure ID.
     * @param stepSeqNo next step sequence number.
     */
    void activateStep(Long procedureId, Integer stepSeqNo, String username);

    /**
     * Search for user procedures requests.
     * 
     * @param vo Procedure value object.
     * @param pageNo Search page number.
     * @param fromDate Start search date.
     * @param toDate End search date.
     * @return Search results.
     */
    SearchPageVO findUserRequests(int pageNo, ProcedureVO vo, 
                                  Date fromDate, Date toDate,
                                  List searchFields);
    /**
     * Get procedure info.
     * 
     * @param procedureId Procedure ID
     * @return Procedure value object.
     */
    ProcedureVO getById(Long procedureId);

    /**
     * Find work list procedures.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @param actionDateFrom Last action date start search.
     * @param actionDateTo Last action date end search.
     * @return Search results.
     */
    SearchPageVO findWorklistProcedures(
                 int pageNo, ProcedureVO procedureVO, Long userId,
                  Date creationDateFrom, Date creationDateTo,
                  Date actionDateFrom, Date actionDateTo,
                  List searchFields);

    /**
     * Check if this user is allowed to take actions on this procedure.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID.
     * 
     * @return true if this user is allowed to take actions on this procedure.
     */
    boolean isUserActionAllowed(Long procedureId, Long userId);
       

    /**
     * Update procedure info.
     * 
     * @param procedureId Procedure ID.
     * @param notes User nores.
     * @param username Current username.
     */
    void update(Long procedureId, String notes, String username);
    
    /**
     * Update procedure notes.
     * 
     * @param procedureId Procedure ID.
     * @param notes User nores.
     * @param username Current username.
     */
    void updateNotes(Long procedureId, String notes, String username);

    /**
     * Assign task to specific user.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID to be assigned to this task.
     */
    void assignTask(Long procedureId, Long userId);
    
    /**
     * Assign task to specific user.
     * 
     * @param procedureId Procedure ID.
     * @param userId User ID to be assigned to this task.
     * @param remarks last notes.
     */
    void assignTask(Long procedureId, Long userId,String remarks);
    
    /**
     * Get active user procedure count
     * 
     * @return number of active user procedure
     * @param userId user ID
     * @param centerId center ID
     */
    Integer getActiveUserProcedureCount(Long userId );
                                            
    /**
     * Has Instance
     * 
     * @return true if has instance, otherwise return false
     * @param templateId Template Id
     * @param seqNo Sequence Number
     */
    boolean hasInstance(Long templateId,Integer seqNo);
    
    /**
     * Field Template Has Instance
     * 
     * @return true if has instance, otherwise return false
     * @param fieldId Field Id
     */
    boolean fieldTemplateHasInstance(Long fieldId);             
    
    /**
     * Get procedure template count
     * @return template version number
     * @param templateId template Id
     * @param versionNo version number
     */
    Integer getTemplateVersionCount(Long templateId,Integer versionNo);
    
    /**
     * Find follow-up procedures.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @return Search results.
     */
    SearchPageVO findFollowupProcedures(int pageNo, 
                                       ProcedureVO procedureVO, 
                                       Long userId,
                                       Date creationDateFrom, 
                                       Date creationDateTo,
                                       List searchFields);
    /**
     * Get transactional procedure
     * 
     * @param procedureId procedure ID
     * @return procedure value object
     */
    ProcedureVO getTransactionalProcedure(Long procedureId);
    
    /**
     * Clear Assigned to
     * 
     * @param vo Procedure step value object.
     */
    void clearAssignTo(ProcedureVO vo);
    
       
    /**
     * Fetch Next Procedure
     * 
     * @param userId User ID
     * @param ctrId Center Id
     * 
     * @return Procedure Value Object
     */
    ProcedureVO fetchNextProcedure(Long userId );
    
    /**
     * Get By Transaction ID
     * 
     * @param transactionId Transaction ID
     * 
     * @return Procedure Value Object.
     */
    ProcedureVO getByTransactionId(Long transactionId);
    
 



    
    
   /**
     *getByAuditTransactionInfo  
     * 
     * @param trsId Transaction Id.
     * @param code Transaction code.
     * 
     * @return procedure value object , check if transaction is already audited
     */
    ProcedureVO getByAuditTransactionInfo(Long trsId ,Long code );
 
    
    /**
     * updateToApprove To Approve.
     * 
     * @param procedureId Procedure ID.
     * @param username Current active employee username.
     */
    void updateToApprove(Long procedureId, String userName);
    

    /**
     * Has Active EPS Procedure
     * 
     * @param transactionId  Transaction Id
     * @return true if Has Active Procedure
     */
    boolean hasActiveEPSProcedure(Long transactionId);
    
    /**
     *has Another Procedure  
     * 
     * @param trsId Transaction Id.
     * @param code Transaction code.
     * 
     * @return (true) if there is other procedure has the same transaction ID and has the same code (belongs to the same template).
     */
    boolean hasAnotherProcedure(Long trsId, Integer code);
    
     
    /**
     * Find Procedure
     * 
     * @param transactionId transaction Id
     * @param pageNo page Number
     * 
     * @return SearchPageVO Object
     */
    SearchPageVO find(Long transactionId,int pageNo);
    

     
     
     
    /**
     * Get all defined Initial NOCs
     *
     * @param templateCodes Template Codes.
     * @param trsId Trs Id.
     * 
     * @return List of Procedure VO
     */
     List getAllDefinedInitialNOCs(String templateCodes, Long trsId);
     
     
     
    /**
     * get List of templates by transaction id.
     *  
     * @param trsId transaction Id.
     * 
     * @return list of templates.
     */
     List getTemplates(Long trsId);    

     
     
     
    
    
    /**
     * Get Procedures To Be Moved To Delivery Step.
     * 
     * @return List Of Procedure Value Objects.
     */
    List getProceduresToBeMovedToDeliveryStep();
    
    /**
     * Update Procedure for confirm printing operation (ws) 
     * 
     * @param username external user
     * @param transactionId transaction id
     */
     void finalizTrsProcedure(Long transactionId, String username , Long userId);
    
    /**
     * Check if transaction id Has active Procedure.
     *
     * @param transactionId transaction Id
     * @return true if Transaction Id Has active Procedure.
     */
     boolean hasProcedure(Long transactionId);
     
    /**
     * Assign Coordinator Task Note.
     * 
     * @param vo Procedure VO
     */
    void assignCoordinatorTaskNote(ProcedureVO vo);
    
    /**
     * Get Traffic File Active Transactional Procedures
     * 
     * @param trafficFileNo : Traffic File No
     * @return Traffic File Active Transactional Procedures
     */
    List getTrafficFileActiveProcedures(Long trafficFileNo);
    
    /**
    * Get All Proceduers By Transaction ID
    * 
    * @param transactionId Transaction ID
    * 
    * @return List Of Procedure Value Object.
    */
    List getAllProceduersByTrsId(Long transactionId);
    
                                   
                                   
     

    
    /**
     * get Count Of Assigned Eps.
     * 
     * @param userId  : user Id.
     * @param epsCode : eps Code.
     * 
     * @return Count Of Assigned Eps.
     */
     Integer getCountOfAssignedEps(Long userId ,String epsCode);
     
        
    /**
     * get Active Procedures By Traffic File.
     * 
     * @param procedureVO : procedure VO.
     * 
     * @return list of Active Procedures.
     */
     List getActiveProceduresByTrafficFile(ProcedureVO procedureVO);
    
    /**
     * get All Procedures By Traffic File.
     * 
     * @param procedureVO : procedure VO.
     * 
     * @return list Procedures.
     */
     List getAllProceduresByTrafficFile(ProcedureVO procedureVO);
     
     
    /**
     * Execute procedure Block.
     * 
     * @param procedureId : Procedure Id.
     * @param caseCode    : Case Code.
     */
    void executeBlock(Long procedureId,Integer caseCode);


   
     
    
    
    
    /**
     * Find Eps On Verify Step Template
     * 
     * @param currentPageNo Current Page No
     * @param epsVerifySearchVO Eps Verify Search VO
     * 
     * @return SearchPageVO
     */
    SearchPageVO findEpsOnVerifyStep( int currentPageNo,EpsVerifySearchVO epsVerifySearchVO);
    
    
    
    /**
     * Find work list procedures new.
     * 
     * @param pageNo Active search page number.
     * @param procedureVO Procedure value object.
     * @param userId Current active user ID.
     * @param creationDateFrom Creation date start search.
     * @param creationDateTo Creation date end search.
     * @param actionDateFrom Last action date start search.
     * @param actionDateTo Last action date end search.
     * @return Search results.
     */
    SearchPageVO findWorklistProceduresNew( int pageNo, ProcedureVO procedureVO, Long userId,
                                            Date creationDateFrom, Date creationDateTo,
                                            Date actionDateFrom, Date actionDateTo,
                                            List searchFields);
    
    
    /**
     * Update Eps Status.
     * 
     * @param transactionId : Transaction Id
     * @param status : New Status
     * @param userName : User Name
     */
    void updateEpsStatus(Long transactionId , Integer status , String userName);
    
     
     /**
      * Update Eps Traffic File Id.
      * 
      * @param transactionId : Transaction Id
      * @param trfId : Traffic File Id
      * @param userName : User Name
      */
    void updateEpsTrafficFileId(Long procedureId, Long trfId, String userName);


    /**
     * Get EPS procedure which it has been printed and it's active step is printing
     *
     * @return ProcedureVO
     */
    ProcedureVO getEPSProcedureVO();

    /**
     * Finalize EPS procedure
     *
     * @param procedureId
     * @param userName
     * @param userId
     */
    void finalizeEPSProcedure(Long procedureId,String userName,Long userId);

    void updateEpsStatusById(ProcedureVO procedure);     
}