/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  05/10/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.ReportParameterVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure template report-parameters data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface ReportParameterDAO extends DataAccessObject {
    /**
     * Get report parameters.
     * 
     * @param reportId EPS report ID.
     * @return report parameters.
     */
    List getByReportId(Long reportId);
    
    /**
     * Find report parameter
     * 
     * @return report parameters
     * @param pageNo pagination Page Number
     * @param reportId EPS report ID
     */
    SearchPageVO find(Long reportId, int pageNo);
    
    /**
     * Get report parameter
     * 
     * @return report parameter value object
     * @param reportId EPS param ID
     */
    ReportParameterVO get(Long paramId);
    
    /**
     * Add Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    ReportParameterVO addReportParameter(ReportParameterVO vo);

    /**
     * Update Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    void updateReportParameter(ReportParameterVO vo);

    /**
     * Update Report Parameter
     * 
     * @return Report Parameter Value Object
     * @param vo Report Parameter Value Object
     */
    void deleteReportParameter(ReportParameterVO vo);
    
    /**
     * Is Order Exist
     * 
     * @return true if parameter order for this report is exists, 
     *                                                  otherwise return false
     * @param orderNo Parameter Order
     * @param reportId Report Id
     */
    boolean isOrderExist(Long reportId,Integer orderNo,Long paramId);
}