/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Uqba OWDA          07/12/2009  - File created.
 * 
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.FollowUpUserVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Follow up user data access object intefrace.
 *
 * @author Uqba OWDA
 * @version 1.00
 */
public interface FollowUpUserDAO extends DataAccessObject {

    /**
     * Create new follow up user.
     * 
     * @param vo follow up user value object.
     */
    void create(FollowUpUserVO vo);
    
    /**
     * Delete follow up user.
     * 
     * @param vo follow up user value object.
     */
    void delete(FollowUpUserVO vo);
    
    /**
     * Get follow up user
     * 
     * @param vo follow up user value object.
     * @return follow up user value object.
     */
    SearchPageVO find(int pageNo,FollowUpUserVO vo);
    
    /**
     * Find Follow Up User 
     * 
     * @return Search Page Value Object
     * @param Follow Up User Value Object
     * @param pageNo page Number 
     */
    SearchPageVO findUsers(int pageNo,FollowUpUserVO vo);   
    
    /**
     * Is follow up user exists
     * 
     * @return true if foolow up user exist
     * @param templateId template ID
     * @param usrId user ID
     */
    boolean isFollowUpUserExists(Long usrId, Long templateId);
       
}