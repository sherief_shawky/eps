/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Mena Emiel  23/02/2011  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TableRowVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.eps.vo.TableColumnValueVO;
import ae.eis.util.web.UserProfile;
import java.util.List;

/**
 * Table column value data access object interface.
 *
 * @author Mena Emiel
 * @version 1.00
 */
public interface TableColumnValueDAO extends DataAccessObject {
    /*
     * Methods
     */
     
    /**
     * Insert Row Values
     * 
     * @param rowColumns Single Row Which Includs Columns
     * @param userProfile User Profile
     * @param procedureId Procedure Id
     */
    void insertRowValues(List rowColumns, UserProfile userProfile, Long procedureId);
    
    /**
     * Insert Table Row Values
     * 
     * @param rowsList    Rows List
     * @param userProfile User Profile
     * @param procedureId Procedure Id
     * 
     * @return Number Of Update Record
     */
    Long insertTableRowValues(List rowsList, UserProfile userProfile, 
                                                            Long procedureId);
}