/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  23/11/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateStepFieldGroupVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

/**
 * Template step-fields group data access object interface.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateStepFieldGroupDAO extends DataAccessObject {
    /**
     * Find step-fields groups.
     * 
     * @param stepId Template step ID.
     * @param pageNo Current search page number.
     * @return step-fields groups.
     */
    SearchPageVO findByStepId(Long stepId, int pageNo);

    /**
     * Create new template step-fields group.
     * 
     * @param vo group info.
     * @return new group details.
     */
    TemplateStepFieldGroupVO create(TemplateStepFieldGroupVO vo);

    /**
     * Check if a step group already exist.
     * 
     * @param stepId Template step ID.
     * @param orderNo Group order number.
     * @return true if a step group already exist.
     */
    boolean isGroupExist(Long stepId, Integer orderNo);

    /**
     * Delete step-fields group.
     * 
     * @param groupId Group ID.
     */
    void delete(Long groupId);

    /**
     * Get step-fields group by ID.
     * 
     * @param groupId Group ID.
     * @return Group info.
     */
    TemplateStepFieldGroupVO getById(Long groupId);

    /**
     * Update step-fields group.
     * 
     * @param vo group info.
     */
    void update(TemplateStepFieldGroupVO vo);
    
    /**
     * Check if group field has fields
     * 
     * @return true if group fields has fields
     * @param groupId group ID
     */
    public boolean hasGroupFields(Long groupId);
}