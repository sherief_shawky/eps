/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  10/05/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.common.vo.UserVO;
import ae.eis.eps.vo.TemplateStepVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;
import java.util.List;

/**
 * Procedure template step data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateStepDAO extends DataAccessObject {

    /**
     * Create new procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    TemplateStepVO create(TemplateStepVO vo);
    
    /**
     * Create new procedure template step.
     * create two initial steps (begin & end ) when create template
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    TemplateStepVO createSystemStep(TemplateStepVO vo);    
    
    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    void updateTemplateStep(TemplateStepVO vo);
    
    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    void updateTemplateNtfStep(TemplateStepVO vo);    
    
    /**
     * Update procedure template step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    void updateTemplateBeginStep(TemplateStepVO vo);
    
    /**
     * Update procedure template Business Action step.
     * 
     * @param vo Procedure template step value object.
     * @return Procedure template step value object created.
     */
    void updateTemplateBusinessStep(TemplateStepVO vo);
    
    /**
     * Find Procedure Step
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    SearchPageVO find(int pageNo,Long ptlId);
        
    /**
     * Get Step By Id
     * 
     * @return Template Step Value Object
     * @param stepId Template Step Id
     */
    TemplateStepVO getById(Long stepId);
    
    /**
     * Find template step Security Users
     * 
     * @return Search Page Value Object
     * @param ptlId Procedure Template Id
     * @param page Number 
     */
    SearchPageVO findStepUsers(int pageNo,Long stepId,UserVO vo);     
    
    /**
     * Delete procedure template step.
     * 
     * @param vo Procedure Template Value Object
     */
    void delete(TemplateStepVO vo);    
    
    /**
     * has Steps
     * return TRUE if the template has defined steps except begin and end
     *                                                  otherwise reyurn FALSE                                        
     * @return boolean (true/false)
     * @param templateId
     */
    boolean hasSteps(Long templateId);    
    
    /**
     * Is Next Step
     *  - EPS_PTT_015
     *  - Cannot delete step because its related to another steps
     * return TRUE if the Step is next step for another step
     * 
     * @return boolean (true/false)
     * @param stepId
     */
    boolean isNextStep(Long stepId);
    
    /**
     * Has Invalid Attachments
     * The step can't have attachment if the step type is not human action
     *  - EPS_PTT_004
     * 
     * @return boolean (true/false)
     * @param templateId
     */
    boolean hasInvalidAttachments(Long templateId);
    
    /**
     * Is Invalid Business Action Step
     * the file name must be added for business action step
     *  - EPS_PTT_016
     *   
     * @return boolean (true/false)
     * @param templateId Template Id
     */
    List getInvalidBusinessActionStep(Long templateId);

    /**
     * Get template start step.
     * 
     * @param templateId Template ID.
     * @return start step for this template.
     */
    TemplateStepVO getTemplateStartStep(Long templateId);
    
    /**
     * Get Attachment settings.
     * 
     * @param stepId Step Id.
     * @return attachment settings for this step
     */
    TemplateStepVO getAttachmentActions(Long stepId);    
    
    /**
     * Update Attachment Settings
     * 
     * @param stepId Step Id
     */
    void updateAttachmentActions(TemplateStepVO vo); 
    
    /**
     * Is Valid Step Name
     * the step name must uniqe
     *  - EPS_PTT_019
     *   
     * @return boolean (true/false)
     * @param stepName step Name
     * @param stepId step Id
     */
    boolean isValidStepName(String stepName,Long stepId,Long templateId);
    
    /**
     * Check if template step Has user
     *
     * @return true if template step has users
     * @param stepSeqNo step sequence number
     * @param templateId template ID
     */
    boolean hasUsers(Long templateId, Integer stepSeqNo);
    
    /**
     * Get human task steps.
     * 
     * @param templateId Template ID.
     * @return human task steps.
     */
    List getHumanTaskSteps(Long templateId);
    
    /**
     * Check if step has field groups
     * 
     * @return true if step has field groups
     * @param procedureTemplateStepId template step ID
     */
    boolean hasFieldGroups(Long templateStepId);
}