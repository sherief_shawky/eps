/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  02/06/2009  - File created.
 */

package ae.eis.eps.dao;

import ae.eis.eps.vo.TemplateFieldVO;
import ae.eis.util.dao.DataAccessObject;
import ae.eis.util.vo.SearchPageVO;

import java.util.List;

/**
 * Template step-field data access object intefrace.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public interface TemplateFieldDAO extends DataAccessObject {

    /**
     * Find Step Fields
     * 
     * @return Search Page Value Object
     * @param stepId Step Id
     * @param templateId template Id
     * @param pageNo pagination page Number
     */
    SearchPageVO find(int pageNo,Long templateId,Long stepId);
    
    /**
     * get Field By Template Id
     * 
     * @return Template Step Field Value Object
     * @param fieldId field Id
     */
    TemplateFieldVO getById(Long fieldId);
    
    /**
     * create Field
     * 
     * @param TemplateStepFieldVO Template Step Field Value Object
     */
    TemplateFieldVO create(TemplateFieldVO vo);
    
    /**
     * update Field 
     * 
     * @param TemplateStepFieldVO Template Step Field Value Object
     */    
    void update(TemplateFieldVO vo);
    
    /**
     * delete Field 
     * 
     * @param TemplateStepFieldVO Template Step Field Value Object
     */    
    void delete(Long fieldId);    
    
    /**
     * Is Exist Code
     *  - EPS_TPE_007 
     *  The code must be unique for the procedure
     *  
     * used to check if field code exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param fieldId field Id
     * @param templateId template Id
     * @param code code
     */
    boolean isExistCode(Long fieldId,Long templateId,Integer code);  
    
    /**
     * Is Exist Order
     *  - EPS_TPE_008 : The order must be unique for the procedure
     *  
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param fieldId field Id
     * @param templateId template Id
     * @param order order
     */
    boolean isExistOrder(Long fieldId,Long templateId,Integer order);
    
    /**
     * Is Exist Label
     *  - EPS_TPE_009 : The label must be unique for the procedure
     *  
     * used to check if field order exist or not
     * if exist return TRUE otherwise return FALSE
     * 
     * @return boolean (true / false)
     * @param fieldId field Id
     * @param templateId template Id
     * @param label label
     */
    boolean isExistLabel(Long fieldId,Long templateId,String label); 
    
    /**
     * Is Linked To Step
     *  - EPS_PTE_010
     *  Cannot delete the field because it's linked with steps
     *  
     * @return boolean (true / false)
     * @param fieldId Field Id
     */
    boolean isLinkedToStep(Long fieldId);
 
    /**
     * Is Valid ComboBox Field
     * Cannot add the field which it's type is drop down list because 
     *       it does not have option value
     * -  EPS_TSL_002      
     *       
     * @return boolean (true / false)
     * @param templateId Template Id
     */
    List getValidComboBoxFieldSteps(Long templateId);
    
    /**
     * Get By Report Id
     * 
     * @return Search Page Value Object
     * @param reportId Report Id
     */
    SearchPageVO getByReportId(Long reportId, TemplateFieldVO vo, int pageNo);

    /**
     * Find option dependent fields.
     * 
     * @param dependentFieldId Dependent field ID.
     * @param label Template field label.
     * @param code Template field code.
     * @param pageNo Search page number.
     * @return List of parent template fields.
     */
    SearchPageVO findParentOptionFields(Long dependentFieldId, String label, 
                                        Integer code, int pageNo);

    /**
     * Update option-fields dependencies
     * 
     * @param templateId Procedure template ID.
     * @param username Active user name.
     */    
    void updateOptionFieldsDependencies(Long templateId, String username);
    
    /**
     * Find remaining search fields
     * 
     * @return Search Page Value Object
     * @param templateId template ID
     * @param pageNo page number
     */
    SearchPageVO findRemainingSearchFields(int pageNo,Long templateId);
    
    /**
     * Find remaining search result fields
     * 
     * @return Search Page Value Object
     * @param templateId template ID
     * @param pageNo page number
     */ 
    SearchPageVO findRemainingSearchResultFields(int pageNo,Long templateId);

    /**
     * Is Field Related To Active Procedure
     * 
     * @return boolean
     * @param code Field Code
     * @param templateId template Id
     */
    boolean isFieldRelatedToActiveProcedure(Long templateId, Integer code);
    
    /**
     * Get Field Id By Code
     * 
     * @param code
     * @param templateId
     * 
     * @return Field Id
     */
    public Long getFieldIdByCode(Integer code,Long templateId);

}