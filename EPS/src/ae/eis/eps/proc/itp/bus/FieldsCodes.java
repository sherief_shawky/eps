/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- ------------------  ----------  ----------------------------------------
 * 1.00  Ibrahim Abu Ghosh   03/09/2014  - File created.
 *
 */

package ae.eis.eps.proc.itp.bus;

import ae.eis.eps.vo.ProcedureTemplateVO;

/**
 * Fields Codes Constants.
 *
 * @author Ibrahim Abu Ghosh.
 * @version 1.00
 */
public interface FieldsCodes  {

    /*
     * Template Code.
     */
    Integer TEMPLATE_CODE = ProcedureTemplateVO.CODE_ISSUE_TRADE_PLATE;
    
    /*
     * Procedure Fields
     */
    Integer FIELD_SERVICE_CHANNEL           = new Integer(1);
    Integer FIELD_PREFERED_LANGUAGE         = new Integer(2);
    Integer FIELD_REQUEST_STATUS            = new Integer(3);
    Integer FIELD_INITIAL_DELIVERY_TIME     = new Integer(4);
    Integer FIELD_ACTUAL_DELIVERY_TIME      = new Integer(5);
    Integer FIELD_INITIAL_PROCESS_TIME      = new Integer(6);
    Integer FIELD_ACTUAL_ACHIEVEMENT_TIME   = new Integer(7);
    Integer FIELD_SMS_TO_CUSTOMER           = new Integer(8);
    Integer FIELD_PLATE_ID                  = new Integer(9);
    Integer FIELD_PLATE_DETAILS             = new Integer(10);
    Integer FIELD_INSURANCE_ORG_ID          = new Integer(11);
    Integer FIELD_INSURANCE_ORG_NAME        = new Integer(12);
    Integer FIELD_INSURANCE_REF             = new Integer(13);
    Integer FIELD_INSURANCE_TYPE            = new Integer(14);
    Integer FIELD_INSURANCE_EXPIRY_DATE     = new Integer(15);
    Integer FIELD_IS_ELECTRONIC_INSURANCE   = new Integer(16);
    Integer FIELD_TRS_VIOLATIONS            = new Integer(17);
    Integer FIELD_DELIVERY_METHOD           = new Integer(18);
    Integer FIELD_DELIVERY_CENTER_ID        = new Integer(19);
    Integer FIELD_DELIVERY_CENTER_NAME      = new Integer(20);

    /*
     * Procedure steps
     */
    /** Made by person steps */
    Integer STEP_BEGIN                               = new Integer (1);
    Integer STEP_DISTRBUTE_REQUEST                   = new Integer (3);
    Integer STEP_VERIFY_REQUEST_DATA                 = new Integer (5);
    Integer STEP_PRINT_DOCUMENTS                     = new Integer (7);
    Integer STEP_VERIFY_DOCUMENTS                    = new Integer (9);
    Integer STEP_DOCUMENTS_DELIVERY_AT_CENTER        = new Integer (12);

    /** Internal Steps */
    Integer STEP_SUBMIT_REQUEST                      = new Integer (2);  // InitializationBA
    Integer STEP_DISTRBUTION_PROCEDURES              = new Integer (4);  // UpdateRequestToInProgressBA
    Integer STEP_CANCELATION_PROCEDURES              = new Integer (6);  // CancelationBA
    Integer STEP_DEFINE_DOCUMENTS                    = new Integer (8);  // DefineDocumentsBA
    Integer STEP_CLIENT_NOT_ATTENDED_PROCEDURES      = new Integer (10); // ClientNotAttendedBA
    Integer STEP_DELIVERY_CENTER_PROCEDURES          = new Integer (11); // CenterDeliveryBA
    
    /** Template Codes */
    Integer TEMPLATE_PLATE_MANUFACTURE = new Integer(9005);
    
    /** Plate Manufacture */
    Integer FIELD_LAN_CODE = new Integer(2);
    Integer FIELD_BKT_PLT_ID = new Integer(19);
    Integer FIELD_RCN_PLT_ID = new Integer(20);
    Integer FIELD_IS_MORTGAGED = new Integer(29);
    Integer FIELD_VEM_ID = new Integer(30);
    Integer FIELD_CTR_ID_DELIVERY = new Integer(33);
    Integer FIELD_CTR_NAME_DELIVERY = new Integer(34);
    Integer FIELD_NEW_PLT_DETAILS = new Integer(39);
    Integer FIELD_NEW_PLATE_SOURCE = new Integer(45);        
}