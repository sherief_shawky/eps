package ae.eis.ntf.bus;


import ae.eis.ntf.vo.NotificationVO;
import ae.eis.util.dao.DataAccessObject;

import java.sql.Connection;

public interface NotificationFacade {
    static final Integer SEC_APPROVAL_MSG_TYPE_REJECTED = 2;
    static final Integer SEC_APPROVAL_MSG_TYPE_APPROVED = 1;


    /**
     * Get Message Body And Subject.
     *
     * @param messageCode : message Code.
     * @param trafficFileId : traffic File Id.
     */
    NotificationVO getMessageBodyAndSubject(String messageCode, Long trafficFileId);


    /**
     * send Sms Notificaton.
     *
     * @param trfId : trf Id.  (used to get lang) (mandatory)
     *
     * // ************ One Of them (mandatory)**********************************
     * @param connection : connection.
     * @param callerDAO : caller DAO.
     * // **********************************************************************
     *
     * @param messageCodeForSms : message Code For Sms. (mandatory)
     * @param mobileNo : mobile No. (optional)
     * if mobileNo is null get mobile no from traffic file.
     * @param replacementParam1 : replacement Param1. (optional).
     * @param replacementParam2 : replacement Param2. (optional).
     * @param transactionId : transaction Id (optional).
     */
    void sendSmsNotificaton(Long trfId, Connection connection, DataAccessObject callerDAO, String messageCodeForSms,
                            String mobileNo, String replacementParam1, String replacementParam2, Long transactionId,
                            Integer reasonType);
    
    
    public NotificationVO getMessage(String messageCode, String lanCode);

  
}
