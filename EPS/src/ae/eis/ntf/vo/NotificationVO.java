/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  08/05/2008  - File created.
 * 
 * 1.01  Tamer Rawashdeh    18/7/2012   -Adding Instance variable
 *                                      -Adding Getter and Setter
 */

package ae.eis.ntf.vo;

import ae.eis.common.vo.CustomerAnnouncementVO;
import ae.eis.common.vo.EmployeeVO;
import ae.eis.common.vo.TrafficFileVO;
import ae.eis.util.vo.ValueObject;

import java.util.Date;
import java.util.List;

/**
 * Notification message value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class NotificationVO extends ValueObject {
    /*
     * Constants and class variables
     */
    
    /** Language values. */
    public static final String ARABIC = "ar";
    public static final String ENGLISH = "en";
    
    public static final String MAIL_FROM = "notifications@rta.ae";
    public static final String MAIL_FROM_ALIAS = "RTA";
    
    /** Notification status values. */
    public static final Integer STATUS_DRAFT_PUSH = new Integer(1);
    public static final Integer STATUS_AUDIT_PUSH = new Integer(2);
    public static final Integer STATUS_SEND_PUSH = new Integer(3);
    public static final Integer STATUS_REJECTED_PUSH = new Integer(4);
    
    public static final Integer STATUS_NOT_SENT = new Integer(1);
    public static final Integer STATUS_SENT = new Integer(2);
    public static final Integer STATUS_REJECTED = new Integer(3);
    public static final Integer MAX_EXCEED = new Integer(4);
    public static final Integer UNDER_REVIEW = new Integer(6);

    /** IS ENCRYPTED VALUES */
    public static final int NOTIFICATION_NOT_ENCRYPTED = 1;
    public static final int NOTIFICATION_ENCRYPTED = 2;
    
    /** Priority Values */
    public static final Integer PRIORITY_HIGH   = new Integer(3);
    public static final Integer PRIORITY_MEDIUM = new Integer(2);
    public static final Integer PRIORITY_LOW    = new Integer(1);    
    
    /** Reason Type Values */
    public static final Integer REASON_TYPE_GENERAL_NOTIFICATIONS = new Integer(47);
    public static final Integer REASON_TYPE_SUBMIT_EYE_TEST = new Integer(86);
    public static final Integer REASON_TYPE_SUBMIT_MEDICAL_TEST = new Integer(87);
    public static final Integer REASON_TYPE_APPROVE_OPEN_TRAFFIC_FILE = new Integer(91);
    public static final Integer REASON_TYPE_REJECT_OPEN_TRAFFIC_FILE = new Integer(92);
    public static final Integer REASON_TYPE_UPDATE_TRAFFIC_FILE_INFO = new Integer(93);
    public static final Integer REASON_TYPE_APPOINTMENT_REMINDER = new Integer(83);
    public static final Integer REASON_TYPE_APPOINTMENT_CANCELLATION = new Integer(84);   
    public static final Integer REASON_TYPE_APPOINTMENT_SECOND_REMINDER = new Integer(94);
    public static final Integer REASON_TYPE_APPOINTMENT_RESCHEDULE_REMINDER = new Integer(95);
    public static final Integer REASON_TYPE_EPS_WORKFLOW_MOVE_STEP = new Integer(96);
    public static final Integer REASON_TYPE_NO_SLOTS_FOR_RESHEDULE = new Integer(98);
    public static final Integer REASON_TYPE_UPDATE_TRIAL_RESULT = new Integer(100);
    public static final Integer REASON_TYPE_NOTIFY_EXAMINER_AFTER_DISTRIBUTION = new Integer(101);
    public static final Integer REASON_TYPE_NOTIFY_SUPERVISOR_AFTER_DISTRIBUTION = new Integer(104);
    public static final Integer REASON_TYPE_UPDATE_CONTACT_INFO = new Integer(120);
    public static final Integer REASON_TYPE_PASSWORD = new Integer(121);
    public static final Integer REASON_TYPE_VEHICLE_MORTGAGE_INFORMATION = new Integer(122);
    public static final Integer REASON_TYPE_SHORTAGE_IN_PLATES = new Integer(123);
    public static final Integer REASON_TYPE_DATA_INTEGRITY = new Integer(124);
    public static final Integer REASON_TYPE_GENERAL_NTF = new Integer(125); 
    public static final Integer REASON_TYPE_MORTGAGE_PLATE_CHANGED = new Integer(126);
    public static final Integer REASON_TYPE_ISSUE_ACCEPTANCE_LETTER = new Integer(127);
    public static final Integer REASON_TYPE_OPTICIAN_ZERO_BALANCE_NTF = new Integer(128);
    public static final Integer REASON_TYPE_SEND_RECEIPTS = new Integer(129);
    public static final Integer REASON_TYPE_FREE_MAKER_TEMPLATE = new Integer(130);
    public static final Integer REASON_TYPE_UPDATE_EYE_TEST = new Integer(131);
    public static final Integer REASON_TYPE_THREADS_IS_NOT_WORKING = new Integer(132);
    public static final Integer REASON_TYPE_SMART_BKT_RENEWAL = new Integer(133);
    public static final Integer REASON_TYPE_SMS_SENT_FROM_EPS = new Integer(134);
    public static final Integer REASON_TYPE_CANCEL_SALES_REQUEST = new Integer(135);
    public static final Integer REASON_TYPE_PUSH_NOTIFICATION_FOR_RENEW_PERMIT = new Integer(136);
    public static final Integer REASON_TYPE_APPROVE_TO_PURCHASE_PLATE = new Integer(137);
    public static final Integer REASON_TYPE_UPLOAD_SUBMIT_EYE_TEST = new Integer(138);
    public static final Integer REASON_TYPE_ADD_MORTGAGE_REQUEST = new Integer(139);
    public static final Integer REASON_TYPE_APPROVE_MORTGAGE_REQUEST = new Integer(140);
    public static final Integer REASON_TYPE_ISSUE_ENOC = new Integer(141);
    public static final Integer REASON_TYPE_CANCEL_ENOC = new Integer(142);
    public static final Integer REASON_TYPE_SUBMIT_MORTGAGE_REQUEST = new Integer(143);
    public static final Integer REASON_TYPE_REVIEW_MORTGAGE_REQUEST = new Integer(144);
    public static final Integer REASON_TYPE_REJECT_MORTGAGE_REQUEST = new Integer(145);
    public static final Integer REASON_TYPE_CANCEL_MORTGAGE_REQUEST = new Integer(146);
    public static final Integer REASON_TYPE_E_SALES_TRANSACTION = new Integer(147);
    public static final Integer REASON_TYPE_REJECT_NOC_REQUEST = new Integer(148);
    public static final Integer REASON_TYPE_NOC_REQUEST = new Integer(149);
    public static final Integer REASON_TYPE_UPDATE_NOC = new Integer(150);
    public static final Integer REASON_TYPE_ISSUE_NOC = new Integer(151);
    public static final Integer REASON_TYPE_SECURITY_APPROVAL = new Integer(152);
    public static final Integer REASON_TYPE_HAVE_TODAY = new Integer(153);
    public static final Integer REASON_TYPE_CANCEL_BUY_PLATE = new Integer(154);
    public static final Integer REASON_TYPE_TRANSFER_PLATE_VEHICLE_TO_HEIR  = new Integer(155);
    public static final Integer REASON_TYPE_APPROVE_LEAVE  = new Integer(156);
    public static final Integer REASON_TYPE_MODIFY_LEAVE  = new Integer(159);
    public static final Integer REASON_TYPE_NOTIFY_AFTER_FOUNDING_LICENSE  = new Integer(160);
    public static final Integer REASON_TYPE_CML_CONTRACT_UNDER_RENEWAL  = new Integer(161);
    public static final Integer REASON_TYPE_TRADE_PERMITS_EXTENTION  = new Integer(203);
    public static final Integer REASON_TYPE_TRADE_PERMITS_EXPIRED  = new Integer(204);
    public static final Integer REASON_TYPE_TRADE_PERMITS_WILL_EXPIRE  = new Integer(205);
    public static final Integer REASON_TYPE_NOTIFY_INSITITUES_WITH_EXPIRED_ISTRACTORS_PERMITS  = new Integer(210);

    /** REASON TYPE NOTIFY AFTER MANUFACTURE PLATE */
    public static final Integer REASON_TYPE_NOTIFY_MANUFACTURE_PLATE  = new Integer(162);
    public static final Integer REASON_TYPE_CML_FRANCHISE_EXCEEDING_OPERATIONAL_AGE  = new Integer(168);
    public static final Integer REASON_TYPE_CML_FUTURE_EXISTENCE_OF_REPLACEMENT_VEHICLES  = new Integer(169);
    public static final Integer REASON_TYPE_CREATE_TRAFFIC_FILE_NO_AUDIT = new Integer(220);
    public static final Integer REASON_TYPE_UPDATE_TRAFFIC_FILE_NO_AUDIT = new Integer(221);
    
    public static final Integer REASON_TYPE_CREATE_TRAFFIC_FILE_NEED_AUDIT = new Integer(192);
    public static final Integer REASON_TYPE_SUPPORT_ERROR  = 174;
    public static final Integer REASON_TYPE_FRANCHISE_CONTRACT_UPDATE_REQUEST  = new Integer(176);
    public static final Integer REASON_TYPE_FRANCHISE_CONTRACT_UPDATE_REQUEST_APPROVED  = new Integer(177);
    public static final Integer REASON_TYPE_FRANCHISE_CONTRACT_UPDATE_REQUEST_REJECTED  = new Integer(178);
 
    /** Evaluation Criteria Reason Type */
    public static final Integer REASON_TYPE_COMMON_EMP_EVALUATION_CRITERIA_ADD  = new Integer(181);  
    public static final Integer REASON_TYPE_COMMON_EMP_EVALUATION_CRITERIA_MINUS  = new Integer(182);   
    public static final Integer REASON_TYPE_COMMON_SUPERVISOR_EVALUATION_CRITERIA_MINUS  = new Integer(183);   
    public static final Integer REASON_TYPE_COMMON_SUPERVISOR_EVALUATION_CRITERIA_ADD  = new Integer(184);   
    // change property name from REASON_TYPE_MERGE_CHASSIS_ERROR to REASON_TYPE_MERGE_CHASSIS_ERROR
    //public static final Integer REASON_TYPE_MERGE_CHASSIS_ERROR  = new Integer(189);
    public static final Integer REASON_TYPE_SMART_LICENSING_EVENTS  = new Integer(189);
    /** notification types*/

    public static final Integer REASON_TYPE_REGISTRATION_OF_ADMINISTRATIVE_VEHICLE  = new Integer(185);
    
    public static final Integer REASON_TYPE_SELL_PLATE_SMS_TRANSACTION  = new Integer(202);   
    
    public static final Integer REASON_TYPE_SEND_SMS_SERVICE_TO_CUSTOMER  = new Integer(212);   
    
    /** Reason Type Send Available Plate to Customer. */
    public static final Integer REASON_TYPE_SEND_AVAILABLE_PLT_TO_CUSTOMER  = new Integer(236);   

    /** notification types*/
    public static final Integer REASON_TYPE_RENEW_DRIVING_LICENSE_REMINDER = new Integer(186);
    public static final Integer REASON_TYPE_SUBMIT_MEDICAL_TEST_RESULT = new Integer(215);
    public static final Integer REASON_TYPE_TRIALS_LOCATION_RESULT = new Integer(216);
    public static final Integer REASON_TYPE_TRIALS_SUPER_LOCATION_RESULT = new Integer(217);
    public static final Integer REASON_TYPE_NOTIFICATION_OF_TEREATMENT_ELECTRONIC_SYSTEM_TRANING_FORM  = new Integer(200); 
    public static final Integer REASON_TYPE_CONFIRM_TRANSACTION  = 193;
    public static final Integer REASON_TYPE_BOOK_SPECIAL_PLATE  = 194;
    public static final Integer REASON_TYPE_CANCEL_BOOK_SPECIAL_PLATE  = 195;
    public static final Integer REASON_TYPE_BOOK_EXPIRY_SPECIAL_PLATE  = 196;
    public static final Integer REASON_TYPE_CANCEL_SELLER_BOOK_SPECIAL_PLATE  = 197;
    public static final Integer REASON_TYPE_RETRIEVE_PLATE_BY_RTA  = 198;
    public static final Integer REASON_TYPE_NO_SELLER_PLATES  = 199;
    public static final Integer REASON_TYPE_NO_ENOUGH_SELLER_PLATES  = 201;
    public static final Integer REASON_TYPE_NEW_SELLER_PACKAGE = 240;
    public static final Integer SPL_PACKAGE_PUBLISHED = 241;
    public static final Integer SPL_PACKAGE_UN_PUBLISHED = 218;
    public static final Integer REASON_TYPE_E_VHL_LICENSE = 237;
    public static final Integer REASON_TYPE_CML_NOTARY_LETTER  = new Integer(250);
    public static final Integer REASON_TYPE_EIR_TRANSACTION  = 213;
    
    public static final Integer REASON_TYPE_VEHICLE_RENEWAL = 239;
    public static final Integer REASON_TYPE_UNPAID_ISNURANCE_CLIAMS = 238;

    /** Reason Type Send Available Saved Inquiry Plate to Customer. */
    public static final Integer REASON_TYPE_SEND_AVAILABLE_SAVED_INQ_PLT_TO_CUSTOMER  = new Integer(242);   

    /** Reason Type New Plate Design. */
    public static final Integer REASON_TYPE_NEW_PLATE_DESIGN  = new Integer(243);
    
    /** Reason Type EDC Bad Address. */
    public static final Integer REASON_TYPE_EDC_BAD_ADDRESS  = 228;  
    public static final Integer REASON_TYPE_EDC_BAD_ADDRESS_REMINDER  = 229;
    public static final Integer REASON_TYPE_EDC_CLOSE_EPS  = 230;


    /** Reason Type New TL from DED without NOC from RTA */
    public static final Integer REASON_TYPE_NEW_TL_WITHOUT_NOC = 245;
    
    /** Reason Type Renew TL from DED without NOC from RTA */
    public static final Integer REASON_TYPE_RENEW_TL_WITHOUT_NOC = 246;
    
    /** Reason Type Cancelation TL from DED without NOC from RTA */
    public static final Integer REASON_TYPE_CANCELATION_TL_WITHOUT_NOC = 247;

    /** Reason Type Exemption From Yar Exam for issue a held License */
    public static final Integer REASON_TYPE_EXEMPTION_FROM_YARD_EXAM_ISSUE_NEW_HELD_LICENSE = new Integer(252);

    /** Reason Type Exemption From Yard Exam for modifying a held License */
    public static final Integer REASON_TYPE_EXEMPTION_FROM_YARD_EXAM_MODIFY_HELD_LICENSE = new Integer(253);
    
    /** Reason Type Block Traffic File due Issueing TL from DED without NOC from RTA */
    public static final Integer REASON_TYPE_BLOCK_TF_DUE_ISSUE_TL_WITHOUT_NOC = 248;
    
    /** Reason Type Block Traffic File due Renewing TL from DED without NOC from RTA */
    public static final Integer REASON_TYPE_BLOCK_TF_DUE_RENEW_TL_WITHOUT_NOC = 249;
    
    /** Reason Type Block Traffic File due Canceling TL from DED without NOC from RTA */
    public static final Integer REASON_TYPE_BLOCK_TF_DUE_CANCELATION_TL_WITHOUT_NOC = 250;
    
    /** Reason Type Exemption From Yard Exam for modifying occupation and sponsor */
    public static final Integer REASON_TYPE_EXEMPTION_FROM_YARD_EXAM_MODIFY_OCC_AND_SPONSOR = new Integer(254);
    
    public static final Integer REASON_TYPE_NOTIFY_PUSHLISH_E_AUCTION  = new Integer(7);
    public static final Integer REASON_TYPE_NOTIFY_PUSHLISH_PACKAGE_AUCTION  = new Integer(32);
    
    /** notification types*/
    public static final Integer MAIL_NOTIFICATION = new Integer(4);
    public static final Integer SMS_NOTIFICATION = new Integer(3);
    public static final Integer FAX_NOTIFICATION = new Integer(2);
    public static final Integer E_MAIL_NOTIFICATION = new Integer(1);
    
    /** notify customer with objection eps*/
    public static final Integer NOTIFY_CUSTOMER_WITH_OBJECTION_EPS = new Integer(59);
    
    /** drl Submit Eye Test SMS*/
    public static final String DRL_SUBMIT_EYE_TEST_SMS = "DRL_Submit_Eye_Test_SMS";
    
    /** Notification source value fo RTA */
    public static final String NOTIFICATION_SOURCE_TYPE_RTA = "1";
    
    /** Notification source value fo DP */
    public static final String NOTIFICATION_SOURCE_TYPE_DP = "2";   
    
    public static final int REASON_TYPE_PLT_MAN_REQUEST = 65;

    /*
     * Instance variables
     */
    
    /** Notification subject. */
    private String subject;

    /** Notification arabic subject. */
    private String subjectAr;

    /** Notification english subject. */
    private String subjectEn;

    /** Notification message. */
    private String message;
    
    /** Notification arabic message. */
    private String messageAr;
    
    /** Notification english message. */
    private String messageEn;

    /** Notification message language, default is english. */
    private String language;
    
    /** Mobile numbner used to send the SMS notification. */
    private String mobile;
    
    /** Email address used used to send the SMS notification. */
    private String email;
    
    /** Fax address used used to send the SMS notification. */
    private String fax;
    
    /** Notification remarks. */
    private String remarks;
    
    /** Notification status. */
    private Integer status;
    
    /** Notification failed trials. */
    private Integer trials;
    
    /** Mail notification property <mail-from>. */
    private String mailFrom;
    
    /** Mail notification property <mail-from alias>. */
    private String mailFromAlias;
    
    /** Transaction Id */
    private Long trsId;
    
    /** Courier Number */
    private String courierNo;
    
    /** Status Arabic Description */
    private String statusDescAr;
    
    /** Status Arabic Description */
    private String statusDescEn;
    
    /** Is Encrypted */
    private Integer isEncrypted;
    
    /** Priority */
    private Integer priority;
     /** Priority Description*/
    private String priorityDescription;
    
    /** Traffic Instance*/
    private TrafficFileVO trafficFile;
    
    /** Status Date*/
    private Date statusDate;
    
    /** Reason Type*/
    private Integer reasonType;
    
    /** Reason Type Description*/
    private String reasonTypeDescription;
    
    private String reasonTypeDescriptionEn;
    
    /** Sending type description*/
    private String sendingTypeDescription;
    
    /** Sending type description*/
    private String sendingTypeDescriptionEn;
    
    /** Customer Announcement VO */
    private CustomerAnnouncementVO customerAnnouncementVO;
    
    /** Sending Type */
    private Integer sendingType;
    
    
    /** Circular Tickets */
    private List circularTickets;


    /** Circular tickets that belongs to a specific plate */
    private List circularTicketsByPlateInfo;

    /** Tickets that belongs to a specific plate */
    private List ticketsByPlateInfo;
    
    

    /** Different Minute. */
    private Double diffMinute;
    

    /** Employee VO */
    private EmployeeVO employeeVO;
    
    /** Is Locked Flag */
    private Integer locked;

    /*
     * Constructors
     */
    
    /**
     * Construct new NotificationVO object.
     */
    public NotificationVO() {
        setLanguage(ENGLISH);
    }
    
    /**
     * Construct new NotificationVO object.
     * 
     * @param subject notification subject
     * @param message notification message
     */
    public NotificationVO(String subject, String message) {
        this(subject, message, ENGLISH);
    }

    /**
     * Construct new NotificationVO object.
     * 
     * @param subject notification subject
     * @param message notification message
     * @param language notification language
     */
    public NotificationVO(String subject, String message, String language) {
        setLanguage(language);
        setSubject(subject);
        setMessage(message);
    }

    /**
     * Construct new NotificationVO object.
     * 
     * @param subject notification subject
     * @param message notification message
     * @param language notification language
     * @param remarks notification remarks
     */
    public NotificationVO(String subject, String message, String language, String remarks) {
        this(subject, message, language);
        setRemarks(remarks);
    }

    /*
     * Methods
     */

    /**
     * Notification subject.
     * 
     * @param subject Notification subject.
     */
    public void setSubject(String subject) {
        if (isBlankOrNull(subject)) {
            throw new IllegalArgumentException("NULL subject parameter");
        }

        this.subject = subject.trim();
    }

    /**
     * Get Notification subject.
     * 
     * @return Notification subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Set Notification message.
     * 
     * @param message Notification message.
     */
    public void setMessage(String message) {
        if (isBlankOrNull(message)) {
            throw new IllegalArgumentException("NULL message parameter");
        }

        this.message = message.trim();
    }

    /**
     * Get Notification message.
     * 
     * @return Notification message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set Notification message language, default is english.
     * 
     * @param lang Notification message language, default is english.
     *        <P>language format must be lowercase two-letter <B>ISO-639</B>
     *           code.
     */
    public void setLanguage(String lang) {
        if (isBlankOrNull(lang)) {
            throw new IllegalArgumentException("NULL language parameter");
        }
        
        lang = lang.trim();
        if (lang.equalsIgnoreCase(ENGLISH) || lang.equalsIgnoreCase(ARABIC)) {
            this.language = lang;
            return;
        }
        
        throw new IllegalArgumentException(new StringBuffer(
            "Invalid language parameter, ").append(lang)
              .append(", language format must be lowercase two-letter ISO-639")
                .toString());
    }

    /**
     * Get Notification message language.
     * 
     * @return Notification message language which is lowercase two-letter
     *         <B>ISO-639</B> String.
     */
    public String getLanguage() {
        return language;
    }
    
    /**
     * Check if message language is arabic
     * 
     * @return true if message language is arabic
     */
    public boolean isArabic() {
        return (language != null && language.trim().equalsIgnoreCase(ARABIC));
    }

    /**
     * Set Mobile numbner used to send the SMS notification.
     * 
     * @param mobile Mobile numbner used to send the SMS notification.
     */
    public void setMobile(String mobile) {
        if (isBlankOrNull(mobile)) {
            this.mobile = null;
        } else {
            this.mobile = mobile.trim();
        }
    }

    /**
     * Get Mobile numbner used to send the SMS notification.
     * 
     * @return Mobile numbner used to send the SMS notification.
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Set Email address used used to send the SMS notification.
     * 
     * @param email Email address used used to send the SMS notification.
     */
    public void setEmail(String email) {
        if (isBlankOrNull(email)) {
            this.email = null;
        } else {
            this.email = email.trim();
        }
    }

    /**
     * Get Email address used used to send the SMS notification.
     * 
     * @return Email address used used to send the SMS notification.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set Fax address used used to send the SMS notification.
     * 
     * @param fax Fax address used used to send the SMS notification.
     */
    public void setFax(String fax) {
        if (isBlankOrNull(fax)) {
            this.fax = null;
        } else {
            this.fax = fax.trim();
        }
    }

    /**
     * Get Fax address used used to send the SMS notification.
     * 
     * @return Fax address used used to send the SMS notification.
     */
    public String getFax() {
        return fax;
    }

    /**
     * Set Notification remarks.
     * 
     * @param remarks Notification remarks.
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * Get Notification remarks.
     * 
     * @return Notification remarks.
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Set Notification status.
     * 
     * @param status Notification status.
     */
    public void setStatus(Integer status) {
        if (status == null ||
            status.equals(STATUS_NOT_SENT) || 
            status.equals(STATUS_SENT) || 
            status.equals(STATUS_REJECTED) ||
            status.equals(UNDER_REVIEW) ||
            status.equals(MAX_EXCEED)) {
            // Valid status value
            this.status = status;

        } else {
            throw new IllegalArgumentException(
                "Invalid status parameter: " + status);
        }
    }

    /**
     * Get Notification status.
     * 
     * @return Notification status.
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Set Notification failed trials.
     * 
     * @param trials Notification failed trials.
     */
    public void setTrials(Integer trials) {
        this.trials = trials;
    }

    /**
     * Get Notification failed trials.
     * 
     * @return Notification failed trials.
     */
    public Integer getTrials() {
        return trials;
    }

    /**
     * Set Mail notification property <mail-from>.
     * 
     * @param mailFrom Mail notification property <mail-from>.
     */
    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    /**
     * Get Mail notification property <mail-from>.
     * 
     * @return Mail notification property <mail-from>.
     */
    public String getMailFrom() {
        return mailFrom;
    }

    /**
     * Set Mail notification property <mail-from alias>.
     * 
     * @param mailFromAlias Mail notification property <mail-from alias>.
     */
    public void setMailFromAlias(String mailFromAlias) {
        this.mailFromAlias = mailFromAlias;
    }

    /**
     * Get Mail notification property <mail-from alias>.
     * 
     * @return Mail notification property <mail-from alias>.
     */
    public String getMailFromAlias() {
        return mailFromAlias;
    }

    /**
     * Set Transaction id
     * 
     * @param trsId Transaction id
     */
    public void setTrsId(Long trsId) {
        this.trsId = trsId;
    }

    /**
     * Get Transaction id
     * 
     * @return Transaction id
     */
    public Long getTrsId() {
        return trsId;
    }

    /**
     * Set Courier Number
     * 
     * @param courierNo Courier Number
     */
    public void setCourierNo(String courierNo) {
        this.courierNo = courierNo;
    }

    /**
     * Get Courier Number
     * 
     * @return Courier Number
     */
    public String getCourierNo() {
        return courierNo;
    }

    /**
     * Set Status Arabic Description
     * 
     * @param statusDescAr Status Arabic Description
     */
    public void setStatusDescAr(String statusDescAr) {
        this.statusDescAr = statusDescAr;
    }

    /**
     * Get Status Arabic Description
     * 
     * @return Status Arabic Description
     */
    public String getStatusDescAr() {
        return statusDescAr;
    }

    /**
     * Set Is Encrypted
     * 
     * @param isEncrypted Is Encrypted
     */
    public void setIsEncrypted(Integer isEncrypted) {
        if(isEncrypted == null) {
            
            throw new IllegalArgumentException("Is Encrypted value cannot be null");
        }
        
        switch (isEncrypted.intValue())  {
            case NOTIFICATION_NOT_ENCRYPTED:
            case NOTIFICATION_ENCRYPTED:
                this.isEncrypted = isEncrypted;
                break;
        
            default:
                
                throw new IllegalArgumentException("Invalid is encrypted value, isEncrypted="+isEncrypted);
        }
    }

    /**
     * Get Is Encrypted
     * 
     * @return Is Encrypted
     */
    public Integer getIsEncrypted() {
        return isEncrypted;
    }
    
    /**
     * Check if the notification is encrypted or not
     * 
     * @return true if yes
     */
    public boolean isNotificationEncrypted() {
        return getIsEncrypted()!=null && getIsEncrypted().intValue() == NOTIFICATION_ENCRYPTED;
    }

    /**
     * Set Priority
     * 
     * @param priority Priority
     */
    public void setPriority(Integer priority) {
        if(priority == null || priority == new Integer(2)){
            priority = new Integer(1);
        }
        
        this.priority = priority;
    }
    
    /**
     * Get Priority
     * 
     * @return priority Priority
     */
    public Integer getPriority() {
        return priority;
    }
    
    /**
     *Set TrafficFile 
     * 
     * @param trafficFile
     */
    public void setTrafficFile(TrafficFileVO trafficFile){
        this.trafficFile = trafficFile;
    }
    
   /**
     *Get TrafficFile
     * 
     * @return trafficFile
     */
    public TrafficFileVO getTrafficFile() {
        return trafficFile;
    }

   /**
     *Set Status Date
     * 
     * @param statusDate
     */
    public void setStatusDate(Date statusDate){
        this.statusDate = statusDate;
    }

    /**
     * Get Status Date
     * @return status date
     */
    public Date getStatusDate(){
        return statusDate;
    }

   /**
     *set reason type description
     * 
     * @param reasonType
     */
    public void setReasonType(Integer reasonType) {
        this.reasonType = reasonType;
    }

   /**
     *Get reason type 
     * 
     * @return reason type
     */
    public Integer getReasonType(){
        return reasonType;
    }
   /**
     * Set Reason Type descripton 
     * 
     * @param reasonTypeDescription
     */
    public void setReasonTypeDescription(String reasonTypeDescription){
        this.reasonTypeDescription = reasonTypeDescription;
    }
    
   /**
     *Get Reason type description 
     * 
     * @return Reason type description
     */
    public String getReasonTypeDescription(){
        return reasonTypeDescription;
    }

    /**
     * Set Sending type description
     * 
     * @param sendingTypeDescription
     */
    public void setSendingTypeDescription(String sendingTypeDescription){
        this.sendingTypeDescription = sendingTypeDescription;
    }


    /**
     *Get Sending type description
     * 
     * @return sendingTypeDescription
     */
    public String getSendingTypeDescription(){
        return sendingTypeDescription;
    }


   /**
     *Set Priority Description
     * 
     * @param priorityDescription
     */
    public void setPriorityDescription(String priorityDescription){
        this.priorityDescription = priorityDescription;
    }


    /**
     * Get Priority Description
     * 
     * @return priorityDescription
     */
    public String getPriorityDescription() {
        return priorityDescription;
    }

    /**
     * Setter method for customerAnnouncementVO : Custmoer Announcement VO
     * 
     * @param customerAnnouncementVO : Custmoer Announcement VO
     */
    public void setCustomerAnnouncementVO(CustomerAnnouncementVO customerAnnouncementVO) {
        this.customerAnnouncementVO = customerAnnouncementVO;
    }

    /**
     * Getter method for customerAnnouncementVO : Custmoer Announcement VO
     * 
     * @return customerAnnouncementVO : Custmoer Announcement VO
     */
    public CustomerAnnouncementVO getCustomerAnnouncementVO() {
        return customerAnnouncementVO;
    }

    /**
     * Setter method for sendingType : Sending Type
     * 
     * @param sendingType : Sending Type
     */
    public void setSendingType(Integer sendingType) {
        this.sendingType = sendingType;
    }

    /**
     * Getter method for sendingType : Sending Type
     * 
     * @return sendingType : Sending Type
     */
    public Integer getSendingType() {
        return sendingType;
    }

    /**
     * Setter for Notification Arabic subject.
     * 
     * @param subjectAr : Notification Arabic subject
     */
    public void setSubjectAr(String subjectAr) {
        this.subjectAr = subjectAr;
    }

    /**
     * Getter for Notification Arabic subject.
     * 
     * @return Notification Arabic subject
     */
    public String getSubjectAr() {
        return subjectAr;
    }

    /**
     * Setter for Notification English subject.
     * 
     * @param subjectEn : Notification English subject
     */
    public void setSubjectEn(String subjectEn) {
        this.subjectEn = subjectEn;
    }

    /**
     * Getter for Notification English subject.
     * 
     * @return Notification English subject
     */
    public String getSubjectEn() {
        return subjectEn;
    }

    /**
     * Setter for Notification Arabic message.
     * 
     * @param messageAr : Notification Arabic message
     */
    public void setMessageAr(String messageAr) {
        this.messageAr = messageAr;
    }

    /**
     * Getter for Notification Arabic message.
     * 
     * @return Notification Arabic message
     */
    public String getMessageAr() {
        return messageAr;
    }

    /**
     * Setter for Notification English message.
     * 
     * @param messageEn : Notification English message
     */
    public void setMessageEn(String messageEn) {
        this.messageEn = messageEn;
    }

    /**
     * Getter for Notification English message.
     * 
     * @return Notification English message
     */
    public String getMessageEn() {
        return messageEn;
    }
    
    /**
     * Set For Diff Minute.
     * 
     * @param diffMinute
     */
    public void setDiffMinute(Double diffMinute) {
        this.diffMinute = diffMinute;
    }

    /**
     * Get For Diff Minute.
     * 
     * @return diffMinute
     */
    public Double getDiffMinute() {
        return diffMinute;
    }


    public void setStatusDescEn(String statusDescEn) {
        this.statusDescEn = statusDescEn;
    }

    public String getStatusDescEn() {
        return statusDescEn;
    }

    public void setIsEncrypted1(Integer isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public void setReasonTypeDescriptionEn(String reasonTypeDescriptionEn) {
        this.reasonTypeDescriptionEn = reasonTypeDescriptionEn;
    }

    public String getReasonTypeDescriptionEn() {
        return reasonTypeDescriptionEn;
    }

    public void setSendingTypeDescriptionEn(String sendingTypeDescriptionEn) {
        this.sendingTypeDescriptionEn = sendingTypeDescriptionEn;
    }

    public String getSendingTypeDescriptionEn() {
        return sendingTypeDescriptionEn;
    }

    public void setCircularTickets(List circularTickets) {
        this.circularTickets = circularTickets;
    }

    public List getCircularTickets() {
        return circularTickets;
    }

    public void setCircularTicketsByPlateInfo(List circularTicketsByPlateInfo) {
        this.circularTicketsByPlateInfo = circularTicketsByPlateInfo;
    }

    public List getCircularTicketsByPlateInfo() {
        return circularTicketsByPlateInfo;
    }

    public void setTicketsByPlateInfo(List ticketsByPlateInfo) {
        this.ticketsByPlateInfo = ticketsByPlateInfo;
    }

    public List getTicketsByPlateInfo() {
        return ticketsByPlateInfo;
    }

    public void setEmployeeVO(EmployeeVO employeeVO) {
        this.employeeVO = employeeVO;
    }

    public EmployeeVO getEmployeeVO() {
        return employeeVO;
    }

    public void setLocked(Integer locked) {
        this.locked = locked;
    }

    public Integer getLocked() {
        return locked;
    }

}
