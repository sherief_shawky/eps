/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  28/01/2008  - File created.
 * 
 * 1.01  Hamzeh Abu Lawi    04/03/2008  - Add getTransaction
 *                                      - Change modifier for getViolations 
 *                                         from private to protected
 *
 * 1.02  Eng. Ayman Atiyeh  06/03/2008  - Adding getBusinessRule().
 * 
 * 1.03 Hamzeh Abu Lawi     17/03/2008  - Add insertTransactionService()
 *                                      - Ass insertTransactionFee()
 *                                            
 * 1.04  Eng. Ayman Atiyeh  24/03/2008  - Adding isMultiPageTransaction().
 *                                      - Updating validate() from private to
 *                                        protected.
 *                                      - Updating validatePayment() from
 *                                        private to protected.
 *                                      - Adding public doCertify() method
 *
 * 1.05  Eng. Ayman Atiyeh  25/03/2008  - Remove isMultiPageTransaction().
 *                                      - Add isAutoCertify()
 *                                      - Add isAutoServiceCreation()
 *                                      
 * 1.06  Alaa Salem         27/02/2010  - Adding reCertifyTransaction() Method.
 * 
 * 1.07  Rami Nassar        14/10/2012  - TRF-7823 
 * 
 * 1.08  Bashar Alnemrawi   21/01/2014  - Add validateKioskDeliveryMethode() Method. 
 * 1.09  Mohamed Hmadi      28/05/2014  - Add updateTransactionServiceCode() Method. 

 */
package ae.eis.trs.bus;



import ae.eis.common.bus.UserHandler;
import ae.eis.eps.bus.ProcedureFieldHandler;
import ae.eis.eps.bus.ProcedureHandler;
import ae.eis.trs.vo.TransactionVO;
import ae.eis.util.bus.BusinessObject;
import ae.eis.util.bus.PreferencesHandler;
import ae.eis.util.dao.DataAccessObject;

import java.sql.Connection;


/**
 * Transaction Business object. This class was designed to implement common
 * functionality between all traffic transactions. All services transactions
 * business objects must extend this class to start and end service transaction.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.06
 */
public abstract class TransactionHandler extends BusinessObject {
    /*
     * Constants and class variables
     */

    /** Mpay User */
    private static final String MPAY_USER = "MPAY_USER";

    /** System preferences handler. */
    private PreferencesHandler preferencesHandler = new PreferencesHandler();
    
    
    /** user Handler */
    private static UserHandler userHandler = new UserHandler();
    

    /** Procedure field business object. */
    private static ProcedureFieldHandler fieldHandler = new ProcedureFieldHandler();
    
    /** Procedure Handler */
    private static ProcedureHandler procedureHandler = new ProcedureHandler();
    
  /*
     * Abstract methods
     */

    

    /*
     * Methods
     */
    
    
    

    
    
    
    /**
     * get By Id
     * 
     * @return Transaction Value Object
     * @param trsId Transaction Id
     */
    public static TransactionVO getById(Long trsId){
        // Validate parameters
        return new TransactionVO();
    }
    
    /**
     * Get Transaction By ID
     * 
     * @param trsId Transaction Id
     * @param callerDao Caller Dao
     * 
     * @return Transaction Value Object
     */
    public static TransactionVO getById(Long trsId, DataAccessObject callerDao){
     
        return new TransactionVO();

    }
    
    /**
     * Get Transaction By ID
     * 
     * @param trsId Transaction Id
     * @param callerDao Caller Dao
     * 
     * @return Transaction Value Object
     */
    public static TransactionVO getById(Long trsId, Connection connection) {
        // Validate parameters
       
        return new TransactionVO();

    }
  
}