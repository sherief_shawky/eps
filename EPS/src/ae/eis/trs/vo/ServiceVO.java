/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  07/02/2008  - File created.
 * 
 * 1.01  Alaa Salem         18/01/2010  - Adding mainServiceFlag Field & Accessors.
 *                                      - Adding system Field & Accessors.
 *                                      
 *                          19/01/2010  - Adding serviceCategory Field & Accessors.
 *                                      - Adding timeToCompleteInMinutes Field & Accessors.
 *                                      - Adding serviceDetailsAr Field & Accessors.
 *                                      - Adding serviceDetailsEn Field & Accessors.
 *                                      - Adding autoService Field & Accessors.
 *                                      - Adding hasEForm Field & Accessors.
 *                                      - Adding requiresManualAuditing Field & Accessors.
 *                                      - Adding requiresPersmissions Field & Accessors.
 *                          20/01/2010  - Modifing setHasFileCopy() Method.
 *                                      - Adding isHasFileCopy() Method.
 *                          24/01/2010  - Adding viewedInCatalog() Field & Accessors.
 *                                      - Adding needsPassportInfo() Field & Accessors.
 *                                      
 * 1.02   Rami Nassar       14/09/2010  - Adding isLockEntities Field & Accessors.                                
 * 1.03   Mohammed Kabeel   19/06/2012  - Adding Service Code 127 for Issue Exam Appointment Service.
 * 1.04   Mohammed Kabeel   19/06/2012  - Adding Service Codes SVC_RENEW_VEHICLE,
 *                                        SVC_RENEW_VEHICLE_WITH_CHANGE_PLATE,
 *                                        SVC_TRANSFER_VEHICLE,
 *                                        SVC_EXPORT_VEHICLE,
 *                                        SVC_UPDATE_REGISTERATION_INFO,
 *                                        SVC_TRANSFER_VEHICLE_OWNERSHIP,
 *                                        SVC_CHANGE_PLATE_NUMBER
 * 1.05  Hussam Abulibdeh     10/04/2014  - Adding SVC_REGISTER_AUCTION Field & Accessors.
 * 1.06  Hussam abulibdeh     22/04/2014    TRF-9413
 * 1.07  Eng. Yousry          25/03/2015  - Adding CML application availability 
 */
package ae.eis.trs.vo;



import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * Service value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.04
 */
public class ServiceVO extends ValueObject implements Cloneable{ 
    /*
     * Constants and class variables.
     */
    
    /** Service E-Certificate Availability. */
    public static final Integer ECERTIFICATE_ENABLED = new Integer(2);
    public static final Integer ECERTIFICATE_DISABLED = new Integer(1);
    
    /** Is kiosk Delivery */
    public static final Integer IS_KIOSK_AVAILABILITY  = new Integer(2);
    
    /** Service Codes. */
    public static final Integer EXPERIENCE_CERTIFICATE = new Integer(105);
    public static final Integer REPLACE_LOST_DAMAGE_DRIVING_LICENSE = new Integer(104);
    public static final Integer LICENSE_RENEWAL = new Integer(103);
    public static final Integer REPLACE_LOST_DAMAGE_POSESSION_CERTIFICATE = new Integer(220);
    public static final Integer REPLACE_LOST_DAMAGE_NUMBER_OWNERSHIP_CERTIFICATE = new Integer(402);
    public static final Integer ISSUE_NEW_LICENSE = new Integer(101);
    public static final Integer OPEN_LEARNING_FILE = new Integer(1);
    public static final Integer ADD_NEW_LICENSE_CLASS = new Integer(102);
    public static final Integer SVC_VEHICLE_POSSESSION = new Integer(205);
    public static final Integer SVC_TRANSFER_LICENSE_TO_EMIRATE = new Integer(107);  
    public static final Integer SVC_ISSUE_INTERNATIONAL_LICENSE_SERVICE_CODE = new Integer(115);  
    public static final Integer SVC_EIR_CALIM_FEES_SERVICE_CODE = new Integer(262);  
   
    /**
     * SVC APPLICATION TRANSFER
     * */
    public static final Integer SVC_APPLICATION_TRANSFER = new Integer(10); 
    public static final Integer SVC_TRAFFIC_BOOK = new Integer(118);   
    public static final Integer SVC_ISSUE_GENERAL_PERMIT = new Integer(124);
    public static final Integer SVC_RENEW_GENERAL_PERMIT = new Integer(125);
    public static final Integer SVC_REPRINT_GENERAL_PERMIT = new Integer(126);
    public static final Integer SVC_CLEARANCE_CERTIFICATE = new Integer(213);
    public static final Integer SVC_SITE_INSPECTION = new Integer(17);
    public static final Integer SVC_ISSUE_INHOUSING_LEARNING_PERMITS = new Integer(18);
    public static final Integer SVC_RENEWAL_INHOUSING_LEARNING_PERMITS = new Integer(19);
    public static final Integer SVC_ISSUE_EXAM_APPOINTMENT_FOR_INHOUSING = new Integer(20);
    public static final Integer SVC_REPRINT_HOUSING_LEARNING_PERMITS = new Integer(21);
    public static final Integer SVC_RENEW_PERMITS = new Integer(804);
    public static final Integer SVC_ADD_MODIFY_LETTERS = new Integer(811);
    public static final Integer SVC_ADD_BRANCH = new Integer(812);
    public static final Integer SVC_NEW_PERMIT_APPLICATION = new Integer(801);
    public static final Integer SVC_ISSUE_NEW_PERMIT = new Integer(805);
    public static final Integer SVC_MODIFY_PERMIT_APPLICATION = new Integer(802);
    public static final Integer SVC_ISSUE_APPROVAL_LETTER = new Integer(803);
    public static final Integer SVC_ISSUE_EXAM_APPOINTMENT = new Integer(127);
    public static final Integer SVC_ISSUE_SPECIAL_CERTIFICATE = new Integer(227);
    public static final Integer SVC_RENEW_VEHICLE = new Integer(204);
    public static final Integer CHANGE_OWNERSHIP =new  Integer(202);
    public static final Integer SVC_VEHICLE_REGISTRATION = new Integer(201);    
    public static final Integer SVC_RENEW_VEHICLE_WITH_CHANGE_PLATE = new Integer(224);
    public static final Integer SVC_TRANSFER_VEHICLE = new Integer(207);
    public static final Integer SVC_EXPORT_VEHICLE = new Integer(206);
    public static final Integer SVC_UPDATE_REGISTERATION_INFO = new Integer(208);
    public static final Integer SVC_TRANSFER_VEHICLE_OWNERSHIP = new Integer(203);
    public static final Integer SVC_CHANGE_PLATE_NUMBER = new Integer(209);
    public static final Integer SVC_PRINT_REG_VHL_REPORT = new Integer(241);
    
    public static final Integer SVC_SUSPEND_PERMIT_APPLICATION = new Integer(825);
    public static final Integer SVC_RESUME_PERMIT_APPLICATION = new Integer(826);
    
    public static final Integer SVC_FRONT_SHORT_PLATE = new Integer(933);
    public static final Integer SVC_FRONT_LONG_PLATE = new Integer(934);
    public static final Integer SVC_BACK_SHORT_PLATE = new Integer(935);
    public static final Integer SVC_BACK_LONG_PLATE = new Integer(936);
    
    public static final Integer SVC_DEPOSITE_INSURANCE = new Integer(404);
    /** Issue Commercial Permit Service*/
    public static final Integer SVC_ISSUE_COMMERCIAL_PERMIT = new Integer(124);
    
    /** Medical Assessment Test. */
     public static final Integer SVC_MEDICAL_ASSESSMENT_TEST = new Integer(132);
    
    /** Vehcile Sales Service*/
    public static final Integer SVC_VEHCILE_SALES = new Integer(228);
    
    /** Vehcile Sales Service*/
    public static final Integer SVC_ISSUE_LEARNING_PERMITS = new Integer(2);
    
    /** Issue Trade Plate Service Code */
    public static final Integer SVC_ISSUE_TRADE_PLATE = new Integer(221);    
    
    /** Renewal Trade Plate Service Code */
    public static final Integer SVC_RENEWAL_TRADE_PLATE = new Integer(222);    
    
    /** Sell Plate Service Code */
    public static final Integer SVC_SELL_PLATE = new Integer(401);    
    
    /** Register to Auction  */
    public static final Integer SVC_REGISTER_AUCTION = new Integer(406);  
    
    /** Allocate Plate Service Code */
    public static final Integer SVC_ALLOCATE_PLATE = new Integer(407);
    
    /** Remove Auctioneer From Black List */
    public static final Integer SVC_REMOVE_AUCTIONEER_FROM_BLACK_LIST= new Integer(416);

    /** Plate Number Booking Renewal Service */
    public static final Integer SVC_PLATE_NUMBER_BOOKING_RENEWAL = new Integer(210);

    /** Salik Fine Dispute */
    public static final Integer SALIK_FINE_DISPUTE = new Integer(502); 
    
    /** Pay Fines*/
    public static final Integer PAY_FINES = new Integer(301);  
    
    /** Pay other Emirates Fines  */
    public static final Integer PAY_OTHER_EMIRATES_FINES = new Integer(302);
    
    /** Pay Instead of Booking Fine  */
    public static final Integer PAY_INSTEAD_OF_BOOKING_FINE = new Integer(303); 
    
    /** Allocating Auction Plate Service Code */
    public static final Integer SVC_ALLOCATING_AUCTION_PLATES = new Integer(409);

    /** Personal Delegation Serice */
    public static final Integer SVC_PERSONAL_DELEGATION = new Integer(236);

    /** Change OwnerShip  with 1 year Registration Serice */
    public static final Integer SVC_CHANGE_OWNERSHIPE_ONE_YEAR_RENEWAL = new Integer(203);
    
    /** Pay Center Booking Fees Serice */
    public static final Integer SVC_PAY_CTR_BOOKING_FEES = new Integer(317);
    
    /** Bring Vehicle By Crane Service */
    public static final Integer SVC_BRING_VEHICLE_BY_CRANE = new Integer(318);
    
    /** Pay Confiscation Fees */
    public static final Integer SVC_CODE_PAY_CONFISCATION_FEES =new Integer(319);
    
    /** Issue New Appoint Date */
    public static final Integer SVC_ISSUE_NEW_APPOINT_DATE = new Integer(7);
	
    /** Cancle Exam Appointment Service Code */
    public static final Integer SVC_CANCLE_EXAM_APPOINT_DATE = new Integer(22);
        
    /**  Renew File */
    public static final Integer SVC_RENEW_FILE = new Integer(23);
    
    /** Modify Appoint Date */
    public static final Integer SVC_MODIFY_APPOINT_DATE = new Integer(8);
        
    /** Issue Hand Book */
    public static final Integer SVC_ISSUE_HAND_BOOK = new Integer(937);
    
    /** Lost Damage Plate */
    public static final Integer SVC_LOST_DAMAGE_PLATE = new Integer(218);
    
    /** Cancel Trade License */
    public static final Integer SVC_CANCEL_TRADE_PLATE = new Integer(223);
    /** Loss damage export. */
    public static final Integer SVC_LOSS_DAMAGE_EXPORT = new Integer(219);
    
    /** Allocate LCD Auction Plate */
    public static final Integer SVC_ALLOCATE_LCD_AUCTION_PLATE = new Integer(417);    
    
    /** Visibilty Status. */
    public static final Integer SVC_NOT_VISIBLE = new Integer(1);
    public static final Integer SVC_VISIBLE = new Integer(2);
    
    /*World Maufacture Status Approved*/
    public static final Integer WORLD_MUNFACTURE_STATS_APPROVED = new Integer(2);
    /* Maufacture Status Approved*/
    public static final Integer MANUNFACTURE_STATS_APPROVED = new Integer(2);
    /* Maufacture Status Rejected*/
    public static final Integer MANUFACTURE_STATS_REJECTED = new Integer(3);
    /*Model Status Approved*/
    public static final Integer MODEL_STATS_APPROVED = new Integer(2);
    /*Model Status Rejected*/
    public static final Integer MODEL_STATS_REJECTED = new Integer(3);
   
    /** Buy Plate Logo */
    public static final Integer SVC_BUY_PLATE_LOGO = new Integer(242);
    /** ADD MANUFACTURE MODEL REQUST ID */
    public static final Integer ADD_MANUFACTURE_MODEL_REQUST_ID= new Integer(5);
    /** ADD MODEL REQUST ID */
    public static final Integer ADD_MODEL_REQUST_ID= new Integer(3);

    
    /** Remove Plate Logo */
    public static final Integer SVC_REMOVE_PLATE_LOGO = new Integer(243);
    
    /** Change Vehicle Ownership NOC */
    public static final Integer SVC_CHANGE_VEHICLE_OWNERSHIP_NOC = new Integer(246);
    
    public static final Integer SVC_RENEW_LEARNING_PERMITS = new Integer(5);
    
    public static final Integer SVC_REISSUE_LEARNING_PERMITS = new Integer(6);

    /** Cancel CML Trade License */
    public static final Integer SVC_CANCEL_CML_PERMIT = new Integer(806);
    
    /** Reprinting Document Cml */
    public static final Integer SVC_REPRINTING_DOCUMENT_CML = new Integer(807);
    
    /** Issue Rental System Training Application */
    public static final Integer SVC_ISSUE_RNT_SYS_TRAINING_APPLICATION = new Integer(810);

    /** Buy Plate Logo */
    public static final Integer SVC_CHANGE_PLATE_LOGO_SIZE = new Integer(244);

    /** Issue Pro Permit Service Code. */
    public static final Integer SVC_ISSUE_PRO_PERMIT = new Integer(134);
    
    /** Issue learner Permit Service Code. */
    public static final Integer SVC_ISSUE_LEARNER_PERMIT = new Integer(133);
    
    /** Open Traffic File Service Code. */
    public static final Integer SVC_OPEN_TRAFFIC_FILE = new Integer(931);
    
    /** Return from tour Service Code. */
    public static final Integer SVC_CODE_VHL_TOURISM_RETURN_CERT = new Integer(225);    

    /** Modify License Class Service Code. */
    public static final Integer SVC_MODIFY_LICENSE_CLASS = new Integer(4);

    /** Registration Card Replacement Service Code. */
    public static final Integer SVC_REGISTRATION_CARD_REPLACEMENT = new Integer(211);
    
    public static final Integer SVC_CODE_POSSESS_UNREGISTERED_VEHICLE = new Integer(247);  
    
    public static final Integer SVC_CODE_PAY_EMIRATE_FINE_WITH_LICENSE_PRESENTATION = new Integer(314);  
    public static final Integer SVC_CODE_PAY_LOCAL_TICKET = new Integer(315);  
    public static final Integer SVC_CODE_PAY_ALL_TICKET_WITH_ALL_ASSERTIONS = new Integer(316);  
    
    public static final Integer SVC_CODE_OWNERSHIP_CERTIFICATE = new Integer(214);
    public static final Integer SVC_CODE_TOURISM_CERTIFICATE = new  Integer(212);
    
    public static final String  SVC_CODE_ISSUE_NEW_APPOINTMENT = "127";
    public static final Integer SVC_CODE_ADD_REMOVE_MEMBER = new  Integer(814);
    public static final Integer SVC_CODE_CAPITULATE_PERMIT = new  Integer(888);
    public static final Integer SVC_CODE_ADD_PARTNER_SERVICE = new  Integer(895);
    public static final Integer SVC_CODE_ADD_MANAGER_SERVICE = new  Integer(896);
    public static final Integer SVC_CODE_MODIFY_CAPITAL_SHARE_SERVICE = new  Integer(897);
    public static final Integer SVC_CODE_REMOVE_PARTNER_SERVICE = new  Integer(898);
    public static final Integer SVC_CODE_REMOVE_MANAGER_SERVICE = new  Integer(899);
    
    public static final Integer SVC_CODE_ISSUE_DRIVER_PERMIT_SERVICE = new  Integer(604);
    public static final Integer SVC_CODE_RENEW_DRIVER_PERMIT_SERVICE = new  Integer(603);
    public static final Integer SVC_CODE_ISSUE_DRIVER_NOC_SERVICE = new  Integer(816);
    
    /** Service Code Telematic Device */
    public static final Integer SVC_CODE_TELEMATIC_DEVICE = new  Integer(254);
    
    /** Service Code Issue NFC Cards */
    public static final Integer SVC_CODE_ISSUE_NFC_CARDS = new  Integer(255);
    
    /** Service Code Add Roaming Service */
    public static final Integer SVC_CODE_ADD_ROAMING_SERVICE = new  Integer(259);
    
    /** Service Code Uninstall Telematic Service */
    public static final Integer SVC_CODE_UNINSTALL_TELEMATIC_SERVICE = new  Integer(260);
    
    /** Service Code Reinstall Telematic Service */
    public static final Integer SVC_CODE_REINSTALL_TELEMATIC_SERVICE = new  Integer(261);
    
    /** Service Code Telematic Device */
    public static final Integer SVC_CODE_PAY_REMAIN_BOOKING_PERIOD = new  Integer(312);
    
    /** Service Code Benevolence */
    public static final Integer SVC_CODE_BENEVOLENCE = new  Integer(940);
    
    /** Service Code Import Vehicle */
    public static final Integer SVC_IMPORT_VEHICLE = new Integer(226);
    
    /** Service Code Insurance Claims */
    public static final Integer SVC_CODE_INSURANCE_CLAIMS = 262;
    
    public static final Integer SVC_CODE_VHL_HISTORY_REPORT = 270;
    
    /** ISSUE WOMEN PERMIT */
    public static final Integer ISSUE_WOMEN_PERMIT = 3 ;
    
    /** RETURN LICENSE */
    public static final Integer RETURN_LICENSE = 108 ;
    
    /** RENEW A FILE */
    public static final Integer RENEW_A_FILE = 23 ;
    
    public static final Integer SVC_NO_OWNERSHIP_CERTIFICATE = 215;
    
    /** Immediate Collection Service. */
    public static final Integer SVC_IMMEDIATE_COLLECTION = new Integer(300);
    
    /** Luxury Plate Service. */
    public static final Integer SVC_LUXURY_PLATE = 515;
    
    public static final Integer SALES_TRANSACTION_PERSON = 1;
    public static final Integer SALES_TRANSACTION_ORGANIZATION = 2;
    public static final Integer SALES_TRANSACTION_ALL = 3;
    public static final Integer SALES_TRANSACTION_NOT_REQUIRED = 4;
    
    /** Related Fines Type Values */
    private static final Integer RELATED_FINES_TYPE_VEHICLE_LICENSE_NON_REGISTERED_VEHICLES = 1;
    
    /** Service Code of loss Dmg Telematic device */
    public static final Integer SVC_CODE_LOSS_DMG_TELEMATIC_SERVICE = 258;

    /** Service Code of Sell Plate Through Authorized Sellers. */
    public static final Integer SVC_SELL_PLATE_THROUGH_AUTHORIZED_SELLERS = 418;

    /** Change Possession Owner Ship Service Code*/
    public static final Integer SVC_CODE_CHANGE_POSSESSION_OWNER_SHIP = new  Integer(240);

    /** Convert Auto Driving license to Manual*/
    public static final Integer SVC_CODE_CONVERT_AUTO_LICENSE_TO_MANUAL = new  Integer(26);
    
    /** Refund Insurance Premium Certificate Service Code. */
    public static final Integer REFUND_INSURANCE_PREMIUM_CERTIFICATE = new Integer(216);
    
    public static final Integer TRANSFER_TO_DOCTOR_CERTIFICATE = new Integer(106);
    
    public static final Integer SVC_REISSUE_LICENSE = new Integer(109);
    
    /** To whom it may concern letter */
    public static final Integer SVC_TO_WHOM_IT_MAY_CONCERN_DRL= new Integer(113);
    
    /** Modify Person Info Service Code. */
    public static final Integer SVC_MODIFY_PERSON_INFO = new Integer(139);
    
    /** Instant Theory Or Practical Exam Service */
    public static final Integer SVC_INSTANT_THEORY_OR_PRACTICAL_EXAM_SERVICE = new Integer(944);

    
    /** Eligibility Request Service Code */
    public static final Integer SVC_CODE_ELIGIBILITY_REQUEST = new Integer(14);    

    /** CML pay depresiation fine Service Code. */
    public static final Integer SVC_CODE_PAY_DEPRESIATION_FINE = new Integer(815);
    
    /** New Plate Design Service. */
    public static final Integer SVC_NEW_PLATE_DESIGN = new Integer(263);
    
    /** Upgarde New Plate Design Service. */
    public static final Integer SVC_UPGRADE_NEW_PLATE_DESIGN = new Integer(945);
    
    /** Modify Person Occupation Info */
    public static final Integer SVC_CODE_MODIFY_PERSON_OCCUPATION_INFO = new Integer(143);
    
    /** Issue New Held License */
    public static final Integer SVC_CODE_ISSUE_NEW_HELD_LICENSE = new Integer(140);
    
    /** Direct Test Exemption.*/
    public static final int SVC_CODE_DIRECT_TEST_EXEMPTION = 144;
    
    /** Advertisment New Service */
    public static final int SVC_CODE_ADVERTISMENT_SERVICE = 914;
    
    /** Advertisment Renewal Service */
    public static final int SVC_CODE_ADVERTISMENT_RENEWAL = 915;
    
    public static final int SERVICE_CODE_CHANGE_PLATE_OWNERSHIP = 403;

    
    /*
     * Instance variables.
     */     
    
    /** Service code. */
    @XmlElement(name = "code")
    private Integer code;
    
    /** System code. */
    private String systemCode;
    
    /** Service arabic description. */
    private String descriptionAr;
    
    /** Service english description. */
    private String descriptionEn;
    
    /** Has File Copy */
    private Integer hasFileCopy;
    
    /** Main Service Flag. */
    private Integer mainService;
    
    /** Main Service Flag Description. */
    private String mainServiceDesc;
    
     
    /** Service Category. */
    private Integer serviceCategory;
     
    /** Service Time To Complete In Minutes. */
    private Integer timeToCompleteInMinutes;
     
    /** Service Arabic Details. */
    private String serviceDetailsAr;
     
    /** Service English Details. */
    private String serviceDetailsEn;
     
    /** Available As Auto-Service Flag. */
    private Integer autoService;

    /** Service Has E-Form Flag. */
    private Integer hasEForm;

    /** Service Requires Manual Auditing Flag. */
    private Integer requiresManualAuditing;

    /** Service Requires Security Permission Flag. */
    private Integer requiresPersmissions;

    /** Can Viewed In Catalog Flag. */
    private Integer viewedInCatalog;

    /** Needs Passport Info Flag. */
    private Integer needsPassportInfo;
    
    /** View Location */
    private Long viewLocation;
    
    /** is SDDI Main Service */
    private Integer sddiMainService;
    
    /** Needs Review */
    private Choice needsReview;
    
    /** Service Has E-Certificate Flag. */
    private Choice hasECertificate;    
    
    /** Need To Collect Fee */
    private Choice needToCollectFee;

    /** Validity  Days */
    private Long validityDays;
    
    /** Retention Days */
    private Long retentionDays;
    
    /** Service Has Most Used Flag. */
    private Choice isMostUsed;
 
    /** Service Is Lock Entities. */
    private Choice isLockEntities;
    
    /** Need Receipt */
    private Integer needReceipt;
    
    /** Reserve E-Government Payment */
    private Choice reserveEgovPayment;
    
    /** delivery By Mail */
    private Choice deliveryByMail;
    
    /** delivery By Kiosk */
    private Choice deliveryByKiosk;
    
    /** Service is Courier Delivery Enabled */
    private Choice courierDeliveryEnabled;
    
    /** System DTO to save System Services */
    private Integer kioskAvailability ;
    
    /** Mail Availability */
    private Integer mailAvailability ;
    
    /** selected Codes */
    private String selectedCodes;
 


    /** Service Is Courier And Collection */
    private Choice isCourierAndCollection; 
    
    /** visible */
    private Integer visible;
    
    
    

    
    /** notify Client After Completion Of Approvals */
    private Choice notifyClientAfterCompletionOfApprovals;
    
    /** EID Buffer Days */
    private Integer eidBufferDays;
    
    /** KPI service duration seconds in related transaction wizard. */
    private Integer kpiServiceDurationSeconds;
    
    /** Sales Transaction Type. */
    private Integer salesTransactionType;

    /** Selected Services */
    private List<String> selectedServices = new ArrayList<String>();
    
    /** Can Be Exempted. */
    private Integer canBeExempted;
    
    /** Pay depreciation fines. */
    private Integer payDprFines;
    
    /** Related Fines Type */
    private Integer relatedFinesType;

    /** modifyPersonInfo*/
    private Choice modifyPersonInfo;
  
  
    /** Open File Fee Activation Date*/
    private Date openFileFeeActivationDate;

    /*
     * Constructors
     */
    
    /**
     * Default constructor.
     */
    public ServiceVO() {
        super();
    }
    
    /*
     * Methods
     */

    /**
     * Returns the String representation of this object.
     * 
     * @return String representation of this object.
     */
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("{\n").append(super.toString())
           .append( "\n, code=").append(getCode())
           .append( "\n, systemCode=").append(getSystemCode())
           .append( "\n, descriptionAr=").append(getDescriptionAr())
           .append( "\n, descriptionEn=").append(getDescriptionEn())
           .append( "\n}");

        return buf.toString();
    }

    /**
     * Set Service code.
     * 
     * @param code Service code.
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Get Service code.
     * 
     * @return Service code.
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Set System code.
     * 
     * @param sysCode System code.
     */
    public void setSystemCode(String sysCode) {
        // Valid system code
        this.systemCode = sysCode;
    }

    /**
     * Get System code.
     * 
     * @return System code.
     */
    public String getSystemCode() {
        return systemCode;
    }

    /**
     * Set Service arabic description.
     * 
     * @param descriptionAr Service arabic description.
     */
    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    /**
     * Get Service arabic description.
     * 
     * @return Service arabic description.
     */
    public String getDescriptionAr() {
        return descriptionAr;
    }

    /**
     * Set Service english description.
     * 
     * @param descriptionEn Service english description.
     */
    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    /**
     * Get Service english description.
     * 
     * @return Service english description.
     */
    public String getDescriptionEn() {
        return descriptionEn;
    }

    /**
     * Set Has file copy
     * 
     * @param hasFileCopy Has file copy
     */
    public void setHasFileCopy(Integer hasFileCopy) {
        if(hasFileCopy == null ||
           hasFileCopy.equals(TRUE) ||
           hasFileCopy.equals(FALSE)) {
           this.hasFileCopy = hasFileCopy;
            return;
        }
        throw new IllegalArgumentException("Invalid hasFileCopy: " + hasFileCopy);
    }

    /**
     * Get Has file copy
     * 
     * @return Has file copy
     */
    public Integer getHasFileCopy() {
        return hasFileCopy;
    }
    
    /**
     * Check if the system can print file copy
     * 
     * @return true if yes
     */
    public boolean printFileCopy() {
        return getHasFileCopy()!=null && getHasFileCopy().intValue() == TRUE.intValue();
    }

    /**
     * Set Main Service Flag.
     * 
     * @param mainService Main Service Flag.
     */
    public void setMainService(Integer mainService) {
         if(mainService == null ||
            mainService.equals(TRUE) ||
            mainService.equals(FALSE)) {
            this.mainService = mainService;
            return;
        }
        throw new IllegalArgumentException("Invalid mainService: " + mainService);
    }

    /**
     * Test If This Service Is Main Service.
     * 
     * @return true If This Service Is Main Service.
     */
    public boolean isMainService() {
        return mainService != null && mainService.equals(TRUE);
    }
    
    /**
     * Get Main Service Flag.
     * 
     * @return Main Service Flag.
     */
    public Integer getMainService() {
        return mainService;
    }

    /**
     * Set Main Service Flag Description.
     * 
     * @param mainServiceFlagDesc Main Service Flag Description.
     */
    public void setMainServiceDesc(String mainServiceDesc) {
        this.mainServiceDesc = mainServiceDesc;
    }

    /**
     * Get Main Service Flag Description.
     * 
     * @return Main Service Flag Description.
     */
    public String getMainServiceDesc() {
        return mainServiceDesc;
    }

   
    
    /**
     * Get Service Category.
     * 
     * @return Service Category.
     */
    public Integer getServiceCategory() {
        return serviceCategory;
    }

    /**
     * Set Service Time To Complete In Minutes.
     * 
     * @param timeToCompleteInMinutes Service Time To Complete In Minutes.
     */
    public void setTimeToCompleteInMinutes(Integer timeToCompleteInMinutes) {
        this.timeToCompleteInMinutes = timeToCompleteInMinutes;
    }

    /**
     * Get Service Time To Complete In Minutes.
     * 
     * @return Service Time To Complete In Minutes.
     */
    public Integer getTimeToCompleteInMinutes() {
        return timeToCompleteInMinutes;
    }

    /**
     * Set Service Arabic Details.
     * 
     * @param serviceDetailsAr Service Arabic Details.
     */
    public void setServiceDetailsAr(String serviceDetailsAr) {
        this.serviceDetailsAr = serviceDetailsAr;
    }

    /**
     * Get Service Arabic Details.
     * 
     * @return Service Arabic Details.
     */
    public String getServiceDetailsAr() {
        return serviceDetailsAr;
    }

    /**
     * Set Service English Details.
     * 
     * @param serviceDetailsEn Service English Details.
     */
    public void setServiceDetailsEn(String serviceDetailsEn) {
        this.serviceDetailsEn = serviceDetailsEn;
    }

    /**
     * Get Service English Details.
     * 
     * @return Service English Details.
     */
    public String getServiceDetailsEn() {
        return serviceDetailsEn;
    }

    /**
     * Set Auto-Service Flag.
     * 
     * @param autoService Auto-Service Flag.
     */
    public void setAutoService(Integer autoService) {
         if(autoService == null ||
            autoService.equals(TRUE) ||
            autoService.equals(FALSE)) {
            this.autoService = autoService;
            return;
        }
        throw new IllegalArgumentException("Invalid autoService: " + autoService);
    }

    /**
     * Test If This Service Is Auto-Service.
     * 
     * @return true If This Service Is Auto-Service.
     */
    public boolean isAutoService() {
        return autoService != null && autoService.equals(TRUE);
    }
    
    /**
     * Get Auto-Service Flag.
     * 
     * @return Auto-Service Flag.
     */
    public Integer getAutoService() {
        return autoService;
    }

    /**
     * Set Service Has E-Form Flag.
     * 
     * @param hasEForm Service Has E-Form Flag.
     */
    public void setHasEForm(Integer hasEForm) {
        if(hasEForm == null ||
           hasEForm.equals(TRUE) ||
           hasEForm.equals(FALSE)) {
           this.hasEForm = hasEForm;
            return;
        }
        throw new IllegalArgumentException("Invalid hasEForm: " + hasEForm);
    }

    /**
     * Test If This Service Has E-Form.
     * 
     * @return true If This Service Has E-Form.
     */
    public boolean isHasEForm() {
        return hasEForm != null && hasEForm.equals(TRUE);
    }
    
    /**
     * Get Service Has E-Form Flag.
     * 
     * @return Service Has E-Form Flag.
     */
    public Integer getHasEForm() {
        return hasEForm;
    }

    /**
     * Set Service Requires Manual Auditing Flag.
     * 
     * @param requiresManualAuditing Service Requires Manual Auditing Flag.
     */
    public void setRequiresManualAuditing(Integer requiresManualAuditing) {
        if (requiresManualAuditing == null ||
            requiresManualAuditing.equals(TRUE) ||
            requiresManualAuditing.equals(FALSE)) {
            this.requiresManualAuditing = requiresManualAuditing;
            return;
        }
        throw new IllegalArgumentException("Invalid requiresManualAuditing: " +
                                                        requiresManualAuditing);
    }

    /**
     * Test If This Service Requires Manual Auditing.
     * 
     * @return true If This Service Requires Manual Auditing.
     */
    public boolean isRequiresManualAuditing() {
        return requiresManualAuditing != null && requiresManualAuditing.equals(TRUE);
    }
    
    /**
     * Get Service Requires Manual Auditing Flag.
     * 
     * @return Service Requires Manual Auditing Flag.
     */
    public Integer getRequiresManualAuditing() {
        return requiresManualAuditing;
    }

    /**
     * Set Service Requires Security Permissions Flag.
     * 
     * @param requiresSecurityPersmissions Service Requires Security Permission Flag.
     */
    public void setRequiresPersmissions(Integer requiresPersmissions) {
        if (requiresPersmissions == null ||
            requiresPersmissions.equals(TRUE) ||
            requiresPersmissions.equals(FALSE)) {
            this.requiresPersmissions = requiresPersmissions;
            return;
        }
        throw new IllegalArgumentException("Invalid requiresPersmissions: " +
                                                    requiresPersmissions);
    }

    /**
     * Test If This Service Requires Security Permissions.
     * 
     * @return true If This Service Requires Security Permissions.
     */
    public boolean isRequiresPersmissions() {
        return requiresPersmissions != null && requiresPersmissions.equals(TRUE);
    }
    
    /**
     * Get Service Requires Security Permissions Flag.
     * 
     * @return Service Requires Security Permission Flag.
     */
    public Integer getRequiresPersmissions() {
        return requiresPersmissions;
    }

    /**
     * Test If This Service Has File Copy.
     * 
     * @return true If This Service Has File Copy.
     */
    public boolean isHasFileCopy() {
        return hasFileCopy != null && hasFileCopy.equals(TRUE);
    }
    
    /**
     * Set Can Viewed In Catalog Flag.
     * 
     * @param viewedInCatalog Can Viewed In Catalog Flag.
     */
    public void setViewedInCatalog(Integer viewedInCatalog) {
        if (viewedInCatalog == null ||
            viewedInCatalog.equals(TRUE) ||
            viewedInCatalog.equals(FALSE)) {
            this.viewedInCatalog = viewedInCatalog;
            return;
        }
        throw new IllegalArgumentException("Invalid viewedInCatalog: " +
                                                    viewedInCatalog);
    }

    /**
     * Test If This Service Can Be Viewd In Catalog.
     * 
     * @return true If This Service Can Be Viewd In Catalog.
     */
    public boolean isViewedInCatalog() {
        return viewedInCatalog != null && viewedInCatalog.equals(TRUE);
    }

    /**
     * Get Can Viewed In Catalog Flag.
     * 
     * @return Can Viewed In Catalog Flag.
     */
    public Integer getViewedInCatalog() {
        return viewedInCatalog;
    }

    /**
     * Set Needs Passport Info Flag.
     * 
     * @param needsPassportInfo Needs Passport Info Flag.
     */
    public void setNeedsPassportInfo(Integer needsPassportInfo) {
        if (needsPassportInfo == null ||
            needsPassportInfo.equals(TRUE) ||
            needsPassportInfo.equals(FALSE)) {
            this.needsPassportInfo = needsPassportInfo;
            return;
        }
        throw new IllegalArgumentException("Invalid needsPassportInfo: " +
                                                    needsPassportInfo);
    }

    /**
     * Test If This Service Needs Passport Info.
     * 
     * @return true If This Service Needs Passport Info.
     */
    public boolean isNeedsPassportInfo() {
        return needsPassportInfo != null && needsPassportInfo.equals(TRUE);
    }

    /**
     * Get Needs Passport Info Flag.
     * 
     * @return Needs Passport Info Flag.
     */
    public Integer getNeedsPassportInfo() {
        return needsPassportInfo;
    }

    /**
     * Set View Location
     * 
     * @param viewLocation View Location
     */
    public void setViewLocation(Long viewLocation) {
        this.viewLocation = viewLocation;
    }

    /**
     * Get View Location
     * 
     * @return viewLocation View Location
     */
    public Long getViewLocation() {
        return viewLocation;
    }

    /**
     * Set SDDI Main Service
     * 
     * @param sddiMainService SDDI Main Service
     */
    public void setSddiMainService(Integer sddiMainService) {
        this.sddiMainService = sddiMainService;
    }

    /**
     * Get SDDI Main Service
     * 
     * @return sddiMainService SDDI Main Service
     */
    public Integer getSddiMainService() {
        return sddiMainService;
    } 

    /**
     * Set Needs Review
     * 
     * @param needsReview Needs Review
     */
    public void setNeedsReview(Choice needsReview) {
        this.needsReview = needsReview;
    }

    /**
     * Get Needs Review
     * 
     * @return needsReview Needs Review
     */
    public Choice getNeedsReview() {
        return needsReview;
    }
    
    /**
     * Is Needs Review
     * 
     * @return boolean .. true / false
     */
    public boolean isNeedsReview() {
        return getNeedsReview() != null && getNeedsReview().getBoolean();
    }

    /**
     * Set Has E-Certificate
     * 
     * @param hasECertificate Has E-Certificate
     */
    public void setHasECertificate(Choice hasECertificate) {
        this.hasECertificate = hasECertificate;
    }

    /**
     * Get Has E-Certificate
     * 
     * @return hasECertificate Has E-Certificate
     */
    public Choice getHasECertificate() {
        return hasECertificate;
    }

    /**
     * Is Has E-Certificate
     * 
     * @return boolean .. true / false
     */
    public boolean hasECertificate() {
        return getHasECertificate() != null && getHasECertificate().getBoolean();
    }
    
    /**
     * Is Has Notify Client After Completion Of Approvals.
     * 
     * @return boolean .. true / false
     */
    public boolean hasNotifyClientAfterCompletionOfApprovals() {
        return getNotifyClientAfterCompletionOfApprovals() != null && getNotifyClientAfterCompletionOfApprovals().getBoolean();
    }

    /**
     * Set Need To Collect Fee
     * 
     * @param needToCollectFee Need To Collect Fee
     */
    public void setNeedToCollectFee(Choice needToCollectFee) {
        this.needToCollectFee = needToCollectFee;
    }

    /**
     * Get Need To Collect Fee
     * 
     * @return needToCollectFee Need To Collect Fee
     */
    public Choice getNeedToCollectFee() {
        return needToCollectFee;
    }
    
    /**
     * Is Need To Collect Fee
     * 
     * @return true if Need To Collect Fee = 2, otherwise return false
     */
    public boolean isNeedToCollectFee() {
        return needToCollectFee != null && needToCollectFee.getBoolean();
    }
    
   /**
    * Set Validity days count
    * @param validityDays
    */
    public void setValidityDays(Long validityDays) {
        this.validityDays = validityDays;
    }

   /**
    * Get Validity days count
    * @return validityDays
    */
    public Long getValidityDays() {
        return validityDays;
    }

   /**
    * Set Retention days count
    * @param retentionDays
    */
    public void setRetentionDays(Long retentionDays) {
        this.retentionDays = retentionDays;
    }

   /**
    * Get Retention days count
    * @return retentionDays
    */
    public Long getRetentionDays() {
        return retentionDays;
    }

    /**
     * Is Most Used
     * 
     * @return true if Most Used = 2, otherwise return false
     */
    public boolean isMostUsed() {
        return isMostUsed != null && isMostUsed.getBoolean();
    }

    /**
     * Set Service Most Used Flag
     * @param isMostUsed
     */
    public void setIsMostUsed(Choice isMostUsed) {
        this.isMostUsed = isMostUsed;
    }

    /**
     * Get Service Most Used Flag
     * @return isMostUsed
     */
    public Choice getIsMostUsed() {
        return isMostUsed;
    }
    
    /**
     * Is Lock Entities
     * 
     * @return true if Lock Entities = 2, otherwise return false
     */
    public boolean isLockEntities() {
        return isLockEntities != null && isLockEntities.getBoolean();
    }

    /**
     * Set Service Lock Entities
     * 
     * @param is Lock Entities
     */
    public void setIsLockEntities(Choice isLockEntities) {
        this.isLockEntities = isLockEntities;
    }

    /**
     * Get Service Lock Entities Flag
     * 
     * @return is Lock Entities
     */
    public Choice getIsLockEntities() {
        return isLockEntities;
    }

    /**
     * Set Need Receipt 
     * 
     * @param needReceipt Need Receipt 
     */
    public void setNeedReceipt(Integer needReceipt) {
        this.needReceipt = needReceipt;
    }

    /**
     * Get Need Receipt 
     * 
     * @return Need Receipt 
     */
    public Integer getNeedReceipt() {
        return needReceipt;
    }

    /**
     * Set Reserve E-Government Payment.
     * 
     * @param reserveEgovPayment Reserve E-Government Payment.
     */
    public void setReserveEgovPayment(Choice reserveEgovPayment) {
        this.reserveEgovPayment = reserveEgovPayment;
    }

    /**
     * Get Reserve E-Government Payment.
     * 
     * @return Reserve E-Government Payment.
     */
    public Choice getReserveEgovPayment() {
        return reserveEgovPayment;
    }
    
    /**
     * Check if service is active for Reserve E-Government Payment.
     * 
     * @return true if service is active for Reserve E-Government Payment.
     * otherwise false
     */
    public boolean isReserveEgovPayment() {
        return reserveEgovPayment != null && reserveEgovPayment.getBoolean();
    }

    /**
     * Set Delivery By Mail.
     * 
     * @param deliveryByMail : delivery By Mail.
     */
    public void setDeliveryByMail(Choice deliveryByMail) {
        this.deliveryByMail = deliveryByMail;
    }

    /**
     * Get Delivery By Mail.
     * 
     * @return deliveryByMail.
     */
    public Choice getDeliveryByMail() {
        return deliveryByMail;
    }
    
    /**
     * Is Has Delivery By Mail
     * 
     * @return boolean .. true / false
     */
    public boolean hasDeliveryByMail() {
        return getDeliveryByMail() != null && getDeliveryByMail().getBoolean();
    }
    
     /**
     * Is Has Delivery By Kiosk
     * 
     * @return boolean .. true / false
     */
    public boolean hasDeliveryByKiosk() {
        return getDeliveryByKiosk() != null && getDeliveryByKiosk().getBoolean();
    }

    /**
     * Check If Service Is Allocation Auction Plate Service.
     * 
     * @return true if the service is allocating auction plate.
     */
    public boolean isAllocatingAuctionPlateService() {
        
        return SVC_ALLOCATING_AUCTION_PLATES.equals(getCode());
    }
    
    /**
     * Check If Service Is Resgister to Auction  Service.
     * 
     * @return true if the service is Service Is Resgister.
     */
        public boolean isRegisterToAuctionService() {
        
        return SVC_REGISTER_AUCTION.equals(getCode());
    }
    
     /**
     * Check If Service Is Resgister to Auction  Service.
     * 
     * @return true if the service is Service Is TransferVehicleOwnership.
     */
        public boolean isTransferVehicleOwnershipService() {
        
        return SVC_TRANSFER_VEHICLE_OWNERSHIP.equals(getCode());
    }
    
     /**
     * Check If Service Is Change Plate Number Service.
     * 
     * @return true if the service is Service Is change plate number.
     */
        public boolean isChangePlateNumberService() {
        
        return SVC_CHANGE_PLATE_NUMBER.equals(getCode());
    }

   /**
     *set Delivery By Kiosk
     *
     * @param deliveryByKiosk
     */
    public void setDeliveryByKiosk(Choice deliveryByKiosk) {
        this.deliveryByKiosk = deliveryByKiosk;
    }

    /**
     * get Delivery By Kiosk
     * 
     * @return deliveryByKiosk
     */
    public Choice getDeliveryByKiosk() {
        return deliveryByKiosk;
    }
    
    /**
     * Setter For Is Courier Delivery Enabled.
     * 
     * @param courierDeliveryEnabled : Courier Delivery Enabled.
     */
    public void setCourierDeliveryEnabled(Choice courierDeliveryEnabled) {
        this.courierDeliveryEnabled = courierDeliveryEnabled; 
    }

    /**
     * Getter For Is Courier Delivery Enabled.
     * 
     * @return Courier Delivery Enabled.
     */
    public Choice getCourierDeliveryEnabled() {
        return courierDeliveryEnabled;
    }
    
    /**
     * Setter Kiosk Availability.
     *
     * @param kioskAvailability : Kiosk Availability.
     */
    public void setKioskAvailability(Integer kioskAvailability) {
        this.kioskAvailability = kioskAvailability;
    }

    /**
     * Get Kiosk Availability.
     * 
     * @return Kiosk Availability.
     */
    public Integer getKioskAvailability() {
        return kioskAvailability;
    }
    
    /**
     * Is Courier Delivery Enabled.
     * 
     * @return courierDeliveryEnabled : Courier Delivery Enabled.
     */
    public boolean isCourierDeliveryEnabled() {
        return Choice.getBoolean(courierDeliveryEnabled);
    }
    
    /**
     * Is Service kiosk Availability.
     * 
     * @return True If Service Is kiosk Availability. 
     */
    public boolean isKioskAvailability() {
    
        return IS_KIOSK_AVAILABILITY.equals(kioskAvailability);
    
    }

    /**
     * Setter selectedCodes.
     *
     * @param selectedCodes : selected Codes.
     */
    public void setSelectedCodes(String selectedCodes) {
        this.selectedCodes = selectedCodes;
    }

     /**
     * Get selected Codes.
     * 
     * @return selected Codes.
     */
    public String getSelectedCodes() {
        return selectedCodes;
    }
    
    /**
     * Get is Handbook Service 
     * 
     * @param serviceCode
     * @return true : if the service is handbook Service
     *         false : if service code is null or not handbook
     */
    public static boolean isHandbookService(Integer serviceCode){
        
        if(serviceCode == null){
            return false;
        }
        return isHandbookService(serviceCode.intValue());
    }
    /**
     * Get is Handbook Service 
     * 
     * @param serviceCode
     * @return true : if the service is handbook Service
     *         false : if service code is null or not handbook Service
     */    
    public static boolean isHandbookService(int serviceCode){
        
   
        return serviceCode == SVC_ISSUE_HAND_BOOK.intValue() ?true : false;
    }
    
    /**
     * Get is Handbook Service 
     * 
     * @param serviceCode
     * @return true : if the service is General Permit Service
     *         false : if service code is null or not General Permit Service
     */
    public static boolean isGeneralPermit(Integer serviceCode){
        
        if(serviceCode == null){
            return false;
        }
        return isHandbookService(serviceCode.intValue());
    }
    /**
     * Get is Handbook Service 
     * 
     * @param serviceCode
     * @return true : if the service is handbook Service
     *         false : if service code is null or not handbook
     */    
    public static boolean isGeneralPermit(int serviceCode){
        
   
        return serviceCode == SVC_ISSUE_GENERAL_PERMIT.intValue() ?true : false;
    }    

    /**
     * Setter for mailAvailability
     * 
     * @param mailAvailability
     */
    public void setMailAvailability(Integer mailAvailability) {
        this.mailAvailability = mailAvailability;
    }

    /**
     * Getter for mailAvailability
     * 
     * @return Mail Availability
     */
    public Integer getMailAvailability() {
        return mailAvailability;
    }
    
    public boolean isMailDeliveryAvailable() {
        
        return mailAvailability != null && mailAvailability.intValue() == 2;
    }


    /**
     * Set Is Courier and collection Delivery 
     * 
     * @param isCourierAndCollection Is Courier and collection Delivery 
     */
    public void setIsCourierAndCollection(Choice isCourierAndCollection) {
        this.isCourierAndCollection = isCourierAndCollection;
    }

    /**
     * Get Is Courier and collection Delivery 
     * 
     * @return Is Courier and collection Delivery 
     */
    public Choice getIsCourierAndCollection() {
        return isCourierAndCollection;
    }


    public void setVisible(Integer visible) {
        this.visible = visible;
    }


    public Integer getVisible() {
        return visible;
    }
    
    /**
     * Is Viisble Service
     * 
     * @return true if visible , false in otherwise
     */
    public boolean isVisible() {
        
        if (visible == null) {
            return false;
        }
        return visible.equals(SVC_VISIBLE);
    }
    


    @Override
    public ServiceVO clone() {
        ServiceVO clone = null;
        try {
            clone = (ServiceVO) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        return clone;
    }
    
    /**
     * Setter for notifyClientAfterCompletionOfApprovals.
     * 
     * @param notifyClientAfterCompletionOfApprovals : notify Client After Completion Of Approvals.
     */
    public void setNotifyClientAfterCompletionOfApprovals(Choice notifyClientAfterCompletionOfApprovals) {
        this.notifyClientAfterCompletionOfApprovals = notifyClientAfterCompletionOfApprovals;
    }
    
    /**
     * Setter for notifyClientAfterCompletionOfApprovals.
     * 
     * @param notifyClientAfterCompletionOfApprovals : notify Client After Completion Of Approvals.
     */
    public Choice getNotifyClientAfterCompletionOfApprovals() {
        return notifyClientAfterCompletionOfApprovals;
    }

    public void setSalesTransactionType(Integer salesTransactionType) {
        this.salesTransactionType = salesTransactionType;
    }

    public Integer getSalesTransactionType() {
        return salesTransactionType;
    }

    
    /**
     * Setter For Selected Services.
     * 
     * @param selectedServices
     */
    public void setSelectedServices(List<String> selectedServices) {
        this.selectedServices = selectedServices;
    }
    
    /**
     * Getter For Selected Services.
     * 
     * @return selectedServices
     */
    public List<String> getSelectedServices() {
        return selectedServices;
    }
    /**
     * Gets EID Buffer Days.
     * 
     * @return EID Buffer Days.
     */
    public Integer getEidBufferDays() {
        return eidBufferDays;
    }
    
    /**
     * Sets EID Buffer Days.
     * 
     * @param eidBufferDays : EID Buffer Days.
     */
    public void setEidBufferDays(Integer eidBufferDays) {
        this.eidBufferDays = eidBufferDays;
    }
    
    /**
     * Set kpi service duration seconds
     *
     * @param kpiServiceDurationSeconds Predefined service duration
     */
    public void setKpiServiceDurationSeconds(Integer kpiServiceDurationSeconds) {
        this.kpiServiceDurationSeconds = kpiServiceDurationSeconds;
    }
    
    /**
     * Get kpi service duration seconds
     *
     * @return Predefined service duration
     */
    public Integer getKpiServiceDurationSeconds() {
        return kpiServiceDurationSeconds;
    }

    /**
     * Set Can Be Exempted.
     * 
     * @param canBeExempted
     */
    public void setCanBeExempted(Integer canBeExempted) {
        this.canBeExempted = canBeExempted;
    }
    
    /**
     * Get Can Be Exempted.
     * 
     * @return canBeExempted
     */
    public Integer getCanBeExempted() {
        return canBeExempted;
    }
    
    /**
     * Set Pay Depreciation Fines Flag.
     * 
     * @param PayDprFines Pay Depreciation Fines Flag
     */
    public void setPayDprFines(Integer payDprFines) {
        this.payDprFines = payDprFines;
    }
    
    /**
     * Get Pay Depreciation Fines Flag.
     * 
     * @return payDprFines
     */
    public Integer getPayDprFines() {
        return payDprFines;
    }

    /**
     * Set Related Fines Type
     *
     * @param relatedFinesType Related Fines Type
     */
    public void setRelatedFinesType(Integer relatedFinesType) {
        this.relatedFinesType = relatedFinesType;
    }
    
    /**
     * Get Related Fines Type
     *
     * @return  Related Fines Type
     */
    public Integer getRelatedFinesType() {
        return relatedFinesType;
    }
    
    /**
     * Check if related fines type to check is on vehicle, license or non registered vehicles
     *
     * @return true if yes
     */
    public boolean isRelatedFineTypeVehicleLicenseNotRegisteredVehicles() {
        
        return getRelatedFinesType() != null && getRelatedFinesType()
                                        .equals(RELATED_FINES_TYPE_VEHICLE_LICENSE_NON_REGISTERED_VEHICLES);
    }

    /**
     * Set Open File Fee Activation Date
     *
     * @param Open File Fee Activation Date
     */
    public void setOpenFileFeeActivationDate(Date openFileFeeActivationDate) {
        this.openFileFeeActivationDate = openFileFeeActivationDate;
    }
    
    
    /**
     * Get Open File Fee Activation Date.
     * 
     * @return Open File Fee Activation Date
     */
    public Date getOpenFileFeeActivationDate() {
        return openFileFeeActivationDate;
    }
    



    /**
     * Setter for modifyPersonInfo
     * @param modifyPersonInfo
     */
    public void setModifyPersonInfo(Choice modifyPersonInfo) {
        this.modifyPersonInfo = modifyPersonInfo;
    }

    /**
     * Getter for modifyPersonInfo
     * @return modifyPersonInfo
     */
    public Choice getModifyPersonInfo() {
        return modifyPersonInfo;
    }

}
