/*
 * Copyright (c) i-Soft 2008.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 * * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  12/10/2009  - File created.
 */

package ae.eis.trs.vo;
 
import ae.eis.util.vo.AttachmentVO;
import ae.eis.util.vo.Choice;

/**
 * Transaction attachment value object.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.00
 */
public class TransactionAttachmentVO extends AttachmentVO {
    /*
     * Constants.
     */

    /** Attachment type values. */
    public static final Integer TYPE_VEHICLE_INSURANCE_POLICY = new Integer(1);
    public static final Integer TYPE_PASSPORT_COPY = new Integer(2);
    public static final Integer TYPE_RESIDENCY_COPY = new Integer(3);
    public static final Integer TYPE_MEDICAL_TEST_COPY=new Integer(4);
    public static final Integer TYPE_TRADE_LICENSE = new Integer(6);
    public static final Integer TYPE_TRADE_LICENSE_WITH_MEMBER = new Integer(7);
    public static final Integer TYPE_NOC_FROM_SPONSOR = new Integer(8);    
    public static final Integer TYPE_KHOLASA = new Integer(9);
    public static final Integer ATTACHMENT_TYPE_PERSON_PHOTO = new Integer(10);
    public static final Integer ATTACHMENT_TYPE_LETTER_OF_GOOD_CONDUCT = new Integer(11);
    public static final Integer ATTACHMENT_TYPE_SIGNED_COPY  = new Integer(12);
    public static final Integer ATTACHMENT_TYPE_EMIRATE_ID = new Integer(13);  
    public static final Integer ATTACHMENT_TYPE_GDRF = new Integer(14);
    public static final Integer ATTACHMENT_TYPE_TEMP_PERMIT = new Integer(15);
    public static final Integer ATTACHMENT_TYPE_OFFICIAL_STAMPED_LETTER = new Integer(16);
    public static final Integer ATTACHMENT_TYPE_SIGNATURE_LETTER = new Integer(17);
    public static final Integer ATTACHMENT_TYPE_PARTNER_APPX_LICENSE = new Integer(18);
    public static final Integer ATTACHMENT_TYPE_LETTER_FROM_SPONSOR  = new Integer(19);
    public static final Integer ATTACHMENT_TYPE_REFRESHER_TRAINING_COURSE_COMPLETION  = new Integer(20);
    public static final Integer ATTACHMENT_TYPE_CID_CLEARANCE_CERTIFICATE  = new Integer(21);
    public static final Integer ATTACHMENT_TYPE_DRIVER_TRANINING_CERTIFICATE  = new Integer(22);
    public static final Integer ATTACHMENT_TYPE_NOC_FROM_SPOONSER = new Integer(23);
    public static final Integer ATTACHMENT_TYPE_NOC_FROM_MASTER = new Integer(24);
    public static final Integer ATTACHMENT_TYPE_VISA_COPY = new Integer(25);
    public static final Integer ATTACHMENT_TYPE_DRIVING_LICENSE_COPY = new Integer(26);
    public static final Integer ATTACHMENT_TYPE_OTHER_ATTACHMENT_COPY = new Integer(27);
    public static final Integer ATTACHMENT_TYPE_COMMERCIAL_LICENSE_APPLICATIONS = new Integer(31);
    public static final Integer ATTACHMENT_VEHCILE_MODEL_CLASSIFICATIONS = new Integer(32);
    public static final Integer ATTACHMENT_MODDIFY_PERSON_INFO = new Integer(33);
    public static final Integer ATTACHMENT_TYPE_BUSINESS_CARD = new Integer(34);
    public static final Integer ATTACHMENT_TYPE_OWNER_REASON_FOR_SUSPEND_PRMIT = new Integer(35);
    public static final Integer ATTACHMENT_TYPE_NOC_FROM_DED_FOR0_RESUME_PRMIT = new Integer(36);
    
    public static final Integer ATTACHMENT_TYPE_ADD_VHL_TO_CONTRACT = new Integer(37);
    public static final Integer ATTACHMENT_TYPE_REDUCE_VHL_TO_CONTRACT  = new Integer(38);
    public static final Integer ATTACHMENT_TYPE_RECLASSIFY_CONTRACT = new Integer(42);
    
    /*
     * Fields.
     */

    /** Attachment type. */
    private Integer attachmentType;

    /** Attachment type Arabic description. */
    private String attachmentTypeDescAr;
    
    /** Attachment type English description. */
    private String attachmentTypeDescEn;

    /** Related transaction info. */
    private TransactionVO transaction;



    

    /** Attachment Source Type. */
    private String attachmentSourceType;

    /** Is the attached file is the citizen Photo */
    private Integer isCitizenPhoto=new Integer(1);

    /** Is Viewed */
    private Choice isViewed;

    /** View Arabic Descriptiopn */
    private String viewDescriptionAr;

    /** View English Descriptiopn  */
    private String viewDescriptionEn;
    
    private Long trafficId;
    
    /*
     * Constructors
     */

    /**
     * Default constructor
     */
    public TransactionAttachmentVO() {
        // Empty body
    }

    /**
     * Construct and initialize new value object.
     * 
     * @param attachmentVO Attachment info.
     */
    public TransactionAttachmentVO(AttachmentVO attachmentVO) {
        setAttachmentInfo(attachmentVO);
    }

    /*
     * Methods
     */

    /**
     * Set Related transaction info.
     * 
     * @param transaction Related transaction info.
     */
    public void setTransaction(TransactionVO transaction) {
        this.transaction = transaction;
    }

    /**
     * Get Related transaction info.
     * 
     * @return Related transaction info.
     */
    public TransactionVO getTransaction() {
        return transaction;
    }

    /**
     * Check if attachment type is valid.
     * 
     * @param type Attachment type.
     * @return true if attachment type is valid.
     */
    public static boolean isValidAttachmentType(Integer type) {
        return type == null ||
               type.equals(TYPE_VEHICLE_INSURANCE_POLICY) ||
               type.equals(TYPE_PASSPORT_COPY) ||   
               type.equals(TYPE_TRADE_LICENSE) ||
               type.equals(TYPE_RESIDENCY_COPY) ||
               type.equals(TYPE_MEDICAL_TEST_COPY) ||
               type.equals(TYPE_KHOLASA) ||
               type.equals(TYPE_NOC_FROM_SPONSOR) ||
               type.equals(TYPE_TRADE_LICENSE_WITH_MEMBER) ||
               type.equals(ATTACHMENT_TYPE_PERSON_PHOTO) || 
               type.equals(ATTACHMENT_TYPE_LETTER_OF_GOOD_CONDUCT) || 
               type.equals(ATTACHMENT_TYPE_SIGNED_COPY) ||
               type.equals(ATTACHMENT_TYPE_EMIRATE_ID) ||
               type.equals(ATTACHMENT_TYPE_GDRF) ||
               type.equals(ATTACHMENT_TYPE_PARTNER_APPX_LICENSE) ||
               type.equals(ATTACHMENT_TYPE_TEMP_PERMIT) ||
               type.equals(ATTACHMENT_TYPE_OFFICIAL_STAMPED_LETTER) ||
               type.equals(ATTACHMENT_TYPE_SIGNATURE_LETTER) ||
               type.equals(ATTACHMENT_TYPE_SIGNED_COPY)||
               type.equals(ATTACHMENT_TYPE_REFRESHER_TRAINING_COURSE_COMPLETION)||
               type.equals(ATTACHMENT_TYPE_CID_CLEARANCE_CERTIFICATE)||
               type.equals(ATTACHMENT_TYPE_DRIVER_TRANINING_CERTIFICATE)||
               type.equals(ATTACHMENT_TYPE_NOC_FROM_SPOONSER)||
               type.equals(ATTACHMENT_TYPE_NOC_FROM_MASTER)||
               type.equals(ATTACHMENT_TYPE_VISA_COPY)||
               type.equals(ATTACHMENT_TYPE_DRIVING_LICENSE_COPY)||
               type.equals(ATTACHMENT_TYPE_OTHER_ATTACHMENT_COPY)||
               type.equals(ATTACHMENT_TYPE_LETTER_FROM_SPONSOR)||
               type.equals(ATTACHMENT_TYPE_COMMERCIAL_LICENSE_APPLICATIONS)||
               type.equals(ATTACHMENT_VEHCILE_MODEL_CLASSIFICATIONS)||
               type.equals(ATTACHMENT_TYPE_BUSINESS_CARD)||
               type.equals(ATTACHMENT_MODDIFY_PERSON_INFO)||
               type.equals(ATTACHMENT_TYPE_OWNER_REASON_FOR_SUSPEND_PRMIT) ||
               type.equals(ATTACHMENT_TYPE_NOC_FROM_DED_FOR0_RESUME_PRMIT) ||
               type.equals(ATTACHMENT_TYPE_ADD_VHL_TO_CONTRACT)||
               type.equals(ATTACHMENT_TYPE_REDUCE_VHL_TO_CONTRACT)||
               type.equals(ATTACHMENT_TYPE_RECLASSIFY_CONTRACT)||
               type.equals(ATTACHMENT_TYPE_OWNER_REASON_FOR_SUSPEND_PRMIT);
    }

    /**
     * Set Attachment type.
     * 
     * @param attachmentType Attachment type.
     */
    public void setAttachmentType(Integer attachmentType) {
        if (! isValidAttachmentType(attachmentType)) {
            throw new IllegalArgumentException("Invalid attachment type: "
                + attachmentType);
        }

        this.attachmentType = attachmentType;
    }

    /**
     * Get Attachment type.
     * 
     * @return Attachment type.
     */
    public Integer getAttachmentType() {
        return attachmentType;
    }
    
    /**
     * Check if attachment type is "VehicleInsurancePolicy".
     * 
     * @return true if attachment type is "VehicleInsurancePolicy".
     */
    public boolean isTypeVehicleInsurancePolicy() {
        return attachmentType != null &&
               attachmentType.equals(TYPE_VEHICLE_INSURANCE_POLICY);
    }

    /**
     * Check if attachment type is "PassportCopy".
     * 
     * @return true if attachment type is "PassportCopy".
     */
    public boolean isTypePassportCopy() {
        return attachmentType != null &&
               attachmentType.equals(TYPE_PASSPORT_COPY);
    }
    
    /**
     * Check if attachment type is "ResidencyCopy".
     * 
     * @return true if attachment type is "ResidencyCopy".
     */
    public boolean isTypeResidencyCopy() {
        return attachmentType != null &&
               attachmentType.equals(TYPE_RESIDENCY_COPY);
    }
    
    /**
     * Check if attachment type is "MedicalTestCopy".
     * 
     * @return true if attachment type is "MedicalTestCopy".
     */
    public boolean isTypeMedicalTestCopy() {
        return attachmentType != null &&
               attachmentType.equals(TYPE_MEDICAL_TEST_COPY);
    }
     
    
    /**
     * Check if attachment type is "TradeLicense".
     * 
     * @return true if attachment type is "TradeLicense".
     */
    public boolean isTypeTradeLicense() {
        return attachmentType != null &&
               attachmentType.equals(TYPE_TRADE_LICENSE);
    } 

    /**
     * Check if attachment type is "Kholasa".
     * 
     * @return true if attachment type is "Kholasa".
     */
    public boolean isTypeKholasa() {
        return attachmentType != null &&
               attachmentType.equals(TYPE_KHOLASA);
    }       

    /**
     * Check if attachment type is "NOCFromSponsor".
     * 
     * @return true if attachment type is "NOCFromSponsor".
     */
    public boolean isTypeNOCFromSponsor() {
        return attachmentType != null &&
               attachmentType.equals(TYPE_NOC_FROM_SPONSOR);
    }

    /**
     * Set Attachment type Arabic description.
     * 
     * @param attachmentTypeDescAr Attachment type Arabic description.
     */
    public void setAttachmentTypeDescAr(String attachmentTypeDescAr) {
        this.attachmentTypeDescAr = attachmentTypeDescAr;
    }

    /**
     * Get Attachment type Arabic description.
     * 
     * @return Attachment type Arabic description.
     */
    public String getAttachmentTypeDescAr() {
        return attachmentTypeDescAr;
    }

    /**
     * Get Attachment type English description.
     * 
     * @param attachmentTypeDescEn Attachment type English description.
     */
    public void setAttachmentTypeDescEn(String attachmentTypeDescEn) {
        this.attachmentTypeDescEn = attachmentTypeDescEn;
    }

    /**
     * Get Attachment type English description.
     * 
     * @return Attachment type English description.
     */
    public String getAttachmentTypeDescEn() {
        return attachmentTypeDescEn;
    }


    
    /**
     * Check if attachment type is "PersonPhotoCopy".
     * 
     * @return true if attachment type is "PersonPhotoCopy".
     */
    public boolean isTypePersonPhotoCopy() {
        return attachmentType != null &&
               attachmentType.equals(ATTACHMENT_TYPE_PERSON_PHOTO);
    }



    
    public void setIsCitizenPhoto(Integer isCitizenPhoto) {
        this.isCitizenPhoto = isCitizenPhoto;
    }

    public Integer getIsCitizenPhoto() {
        return isCitizenPhoto;
    }

    /**
     * Set Is Viewed
     * 
     * @param isViewed
     */
    public void setIsViewed(Choice isViewed) {
        this.isViewed = isViewed;
    }

    /**
     * Get Is Viewed 
     * 
     * @return TRUE if attachment is viewed, otherwise return FALSE
     */
    public Choice getIsViewed() {
        return isViewed;
    }    
    
    /**
     * Setter for viewDescriptionAr
     * @param viewDescriptionAr
     */
    public void setViewDescriptionAr(String viewDescriptionAr) {
        this.viewDescriptionAr = viewDescriptionAr;
    }
    
    /**
     * Getter for viewDescriptionAr
     * @return String
     */
    public String getViewDescriptionAr() {
        return viewDescriptionAr;
    }
    
    /**
     * Setter for viewDescriptionEn
     * @param viewDescriptionEn
     */
    public void setViewDescriptionEn(String viewDescriptionEn) {
        this.viewDescriptionEn = viewDescriptionEn;
    }
    
    /**
     * Getter for viewDescriptionEn
     * @return String
     */
    public String getViewDescriptionEn() {
        return viewDescriptionEn;
    }
    /**
     * Setter for Attachment Source Type
     * @param attachmentSourceType
     */
    public void setAttachmentSourceType(String attachmentSourceType) {
        this.attachmentSourceType = attachmentSourceType;
    }
    
    /**
     * Getter for Attachment Source Type
     * @return attachmentSourceType
     */
    public String getAttachmentSourceType() {
        return attachmentSourceType;
    }

    public void setTrafficId(Long trafficId) {
        this.trafficId = trafficId;
    }

    public Long getTrafficId() {
        return trafficId;
    }
}
