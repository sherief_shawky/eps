/*
 * Copyright (c) i-Soft 2003.
 * Ferdous Tower (Takreer Building) , Salam Street
 * Abu Dhabi, United Arab Emirates
 * P.O. Box: 32326
 * All Rights Reserved.
 *
 * ver    Developer          Date        Comments
 * ----- -----------------  ----------  ----------------------------------------
 * 1.00  Eng. Ayman Atiyeh  28/01/2008  - File created.
 * 
 * 1.01  Eng. Ayman Atiyeh  18/02/2008  - Adding commercial licensing system.
 * 
 * 1.02  Moh Fayek          16/07/2012  - TRF-5237
 * 
 * 1.03  Sami Abudayeh      26/06/2012  - Adding isSentDescription property
 *                                      - Adding sentDate propery
 *                                      
 * 1.04  Mohammed Kabeel    31/07/2012  - SDDI-2613                        
 * 
 * 1.05  Ahmed Abdelwahab   21/08/2012  - TRF-7515.
 * 
 * 1.06  Mahmoud Atiyeh     05/11/2012  - Added nationality property
 *                                      - Added plateVO property
 *                                      - Added ticketsList property
 * 
 * 1.07  Ibrahim Alrabea    9/11/2016   - Added FFU Service Code
 */

package ae.eis.trs.vo;
 
import ae.eis.common.vo.TrafficFileVO; 
import ae.eis.util.bus.BusinessException;
import ae.eis.util.vo.BusinessRuleVO;
import ae.eis.util.vo.Choice;
import ae.eis.util.vo.ValueObject;
import ae.eis.util.web.UserProfile; 

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Traffic transaction value object. Encapsulates common transaction attributes.
 *
 * @author Eng. Ayman Atiyeh
 * @version 1.05
 */
public class TransactionVO extends ValueObject {
    /*
     * Constants and class variables.
     */
    /** Column ( TF_STP_TRANSACTIONS ) */
    public static final String IS_RECEPTION = "IS_RECEPTION";
    public static final String TEMP_ATTACHMENTS_SEQUENCE = "TEMP_ATTACHMENTS_SEQUENCE";
    public static final String ID = "ID";


    /** Table Name */
    public static final String TABLE_NAME = "TF_STP_TRANSACTIONS";
     
     
    /** Transaction status values. */
    public static final int STATUS_ALL = 0;
    public static final int STATUS_NEW = 1;
    public static final int STATUS_CERTIFIED = 2;
    public static final int STATUS_CONFIRMED = 3;
    public static final int STATUS_CANCELED = 4;
    public static final int STATUS_PENDING = 5;
    public static final int STATUS_POSTED = 6;
    public static final int STATUS_REVERSED = 7;

    
    /** Transaction type values. */
    public static final int TYPE_VEHICLES = 1;
    public static final int TYPE_DRIVING_LICENSES = 2;
    public static final int TYPE_FINES = 3;
    public static final int TYPE_SPECIAL_PLATES = 4;
    public static final int TYPE_DRIVING_TEST = 5;
    public static final int TYPE_COMMERCIAL_LICENSING = 6;
    public static final int TYPE_SPECIAL_RECEIPTS = 7;
    public static final int TYPE_REVAMP = 8 ;
    public static final int TYPE_CAR_INSPECTION_SYSTEM = 26;
    public static final int TYPE_GENERAL = 11; // [11] On SDDI.
    
    
    
    /** Delivery Mode Values */
    public static final Integer DELIVERY_MODE_COURIER = new Integer(1);
    public static final Integer DELIVERY_MODE_COLLECTION = new Integer(2);
    public static final Integer DELIVERY_MODE_ECERTIFICATE = new Integer(3);
    public static final Integer DELIVERY_MODE_MAIL = new Integer(4);
    public static final Integer DELIVERY_MODE_IMMIDIATE_CARD_PRINTING = new Integer(5);
    public static final Integer DELIVERY_MODE_PRINTED_KISOK = new Integer(6);
    public static final Integer DELIVERY_MODE_COURIER_AND_COLLECTION = new Integer(7);
    public static final Integer DELIVERY_MODE_IMMEDIATE = 8;  
    public static final Integer DELIVERY_MODE_E_VHL = 9;
                                   
    /** Delivery Method Values */
    public static final Integer DELIVERY_METHOD_FEDEX = new Integer(1);
    public static final Integer DELIVERY_METHOD_MANUAL_DELIVERY = new Integer(3);
    public static final Integer DELIVERY_METHOD_EMAIL = new Integer(2);
    
        
    /** Is Sent Values */
    public static final Integer IS_NOT_SENT = new Integer(1);
    public static final Integer IS_SENT_LOCKED = new Integer(2);
    public static final Integer IS_SENT_SUCCESS = new Integer(3);
    public static final Integer IS_SENT_TO_EWALLET = new Integer(4);
    
    /**Eid DATA SOURCE */
    public static final Integer EID_DATA_SOURCE_CARD_READER = new Integer(1); 
    public static final Integer EID_DATA_SOURCE_MANAUL_FTF = new Integer(2); 
    public static final Integer EID_DATA_SOURCE_MANUAL_SDDI = new Integer(3); 
    public static final Integer EID_DATA_SOURCE_EXTERNAL_INTEGRATION = new Integer(4); 


    /** Session Changed Values */
    public static final Integer SESSION_CHANGED_DATA_CORRECT = new Integer(1);
    public static final Integer SESSION_CHANGED_DUH_IN_PROFILE_NULL = new Integer(2);
    public static final Integer SESSION_CHANGED_DUH_IN_RECEIPT_NULL = new Integer(3);
    public static final Integer SESSION_CHANGED_DUH_DEF_IN_PROF_AND_RECEIPT = new Integer(4);
    public static final Integer SESSION_CHANGED_IP_ADDRESS_IN_PROFILE_NULL = new Integer(5);
    public static final Integer SESSION_CHANGED_IP_ADDRESS_IN_RECEIPT_NULL = new Integer(6);
    public static final Integer SESSION_CHANGED_IP_ADDRESS_DEF_IN_PROF_AND_RECEIPT = new Integer(7);
    public static final Integer SESSION_CHANGED_SESSION_ID_IN_PROFILE_NULL = new Integer(8);
    public static final Integer SESSION_CHANGED_SESSION_ID_IN_RECEIPT_NULL = new Integer(9);
    public static final Integer SESSION_CHANGED_SESSION_ID_DEF_IN_PROF_AND_RECEIPT = new Integer(10);
    public static final Integer SESSION_CHANGED_ULG_ID_IS_NULL_PROFILE = new Integer(11);
    public static final Integer SESSION_CHANGED_ULG_ID_IS_NOT_EXISTS_ON_DB = new Integer(12);
    public static final Integer SESSION_CHANGED_ULG_DUH_DEF_PROF = new Integer(13);
    public static final Integer SESSION_CHANGED_ULG_DUH_DEF_REC = new Integer(14);
    public static final Integer SESSION_CHANGED_ULG_IP_ADD_DEF_PROF = new Integer(15);
    public static final Integer SESSION_CHANGED_ULG_IP_ADD_DEF_REC = new Integer(16);
    public static final Integer SESSION_CHANGED_ULG_SESSION_ID_DEF_PROF = new Integer(17);
    public static final Integer SESSION_CHANGED_ULG_SESSION_ID_DEF_REC = new Integer(18);
    public static final Integer SESSION_CHANGED_ULG_IP_ADD_NULL = new Integer(19);
    public static final Integer SESSION_CHANGED_ULG_DUH_NULL = new Integer(20);
    public static final Integer SESSION_CHANGED_ULG_SESSION_ID_NULL = new Integer(21);
    public static final int SERVICE_CODE_VHL_HISTORY_REPORT = 270;
    
    /*
     * Instance variables.
     */
    
    /** Transaction type, vehicles, fines... . */
    private Integer transactionType;
    
    
    /** Transaction status. */
    private Integer status;
    
    /** Transaction status date. */
    private Date statusDate;
    
    /** Transaction service code. */
    private Integer serviceCode;
    
    /** Service name. */
    private String serviceName;
    
    
    
    
    
    /** Traffic file value object. */
    private TrafficFileVO trafficFile;
    
    
    /** Transaction remarks. */
    private String remarks;
    
    
    
    /** Service value object */
    private ServiceVO service;

    /** Related transaction receipts. */
    private List receiptsList;
    
    /** E-Services Note Arabic */
    private String eServicesNoteAr;
    
    /** E-Services Note English */
    private String eServicesNoteEn;
    
    
    /** Delivery Mode. */
    private String deliveryModeAr;
    
    
    
    /** Service code vhl. */
    private Integer serviceCodeVhl;
    
    /** Service code Rvp. */
    private Integer serviceCodeRvp;
    
    /** Service code ACT (SPL service Code). */
    private Integer serviceCodeAct;
    
    
    /** Indecator if service info was changes and must be updated. */
    private boolean serviceInfoChanged;
    
    /** Sent to EGov */
    private Integer sentToEGov;
    
    /** Agent Name */
    private String agentName;
    
    /** Attachment Sequence No. */
    private Long attachmentRefNo;
    
    /** List of Transaction Attachments */
    private List transactionAttachments;
    
    
    /** Receipts Total Amount */
    private Long totalAmount;     
    
    /** Number Of Receipts. */
    private Integer numberOfReceipts;
     
    /** Ship Package No */
    private Long shipPackageNo;
    
    /** Sequence In Ship Package */
    private Long sequenceInShipPackage;
    
    /** Transaction Status Arabic Description */
    private String trsStatusDescAr;
    
    /** Transaction Status English Description */
    private String trsStatusDescEn;    
    
    /** DEG Transaction Reference Number. */
    private Long degRefNo;
    
    /** DEG Send Date */
    private Date degSendDate;
    
    /** Is Receiption */
    private Integer isReceiption;    
    
    /** Address */
    private String address;
    
    /** P O Box */
    private String poBox;
    
    /** Phone */
    private String phone;
    
    /** Office Phone */
    private String officePhone;    
    
    /** Mobile */
    private String mobile;
    
    /** Emirates Code */
    private String addressEmiCode;
    
    /** Is Sent */
    private Integer isSent;
    
    
    /** Delivery Mode */
    private Integer deliveryMode;
    
    
    /** Delivery Mode Arabic Description */
    private String deliveryModeDescAr;
    
   /** Delivery Mode English Description */
    private String deliveryModeDescEn;
    
    /** is E-Certificate */
    private Choice isEcertificate;
    
    /** Email */
    private String email;
    
    
    /** Transaction "is Sent" Description. */
    private String isSentDescription;
    
    /** Transaction sent date  **/
    private Date sentDate;
    
    /**  Bank Transaction Id */
    private String bankTranactionId;
    
    /** lan Code */
    private String lanCode;
    
   
    
    /** Related transaction tickets. */
    private List ticketsList;
    
    /** Service code cml. */
    private Integer serviceCodeCml;
    
    
    /** E-Wallet Account Number */
    private String eWalletAccountNo;
    
    /** E-Wallet Sent Id */
    private Long eWalletSentId;
    
    /** E-Wallet Reverse Reference */
    private Long eWalletReverseRef;
    
    /** share Email */
    private String shareEmail;
    
    
    
    /** issue Date From*/
    private Date issueDateFrom;
      
    /** issue Date To*/
    private Date issueDateTo;
    
    /** print Status*/
    private Integer printStatus;
    
    /** List of Allocating Auction Plate Transaction Value Object.*/
    private List allocatingAuctionPlatesList;
    
    
    /** status Arabic Description*/
    private String statusArDesc;
    
    /** status English Description*/
    private String statusEnDesc;
    
    /** Transaction Count */
    private Integer transactionCount;
    

    /** eid Data Source */
    private Integer eidDataSource;
    
    /** eid Data Source */
    private Date eidExpiryDate;
    
    /** eid Number */
    private String eidNumber;


    /** Dsg Sp Code */
    private String dsgSpCode;
    
    /** Has Unprinted Basket Card */
    private Integer hasUnprintedBasketCard;
    /** Dsg Service Code */
    private String dsgServiceCode;
    
    /** Is 3d payment */
    private Choice is3dPayment;
    /** List Of Trs Authority Nocs VO.*/
    private List authorityNocsList;
    
    /** List of Res Application Trainee Value Object */
    private List traineesList;
    

    private Integer parentServiceCode ;
    

    /** public Relations Officer (PRO) */
    private boolean publicRelationsOfficer;
    
    /** permit Category Id */
    private Integer permitCategoryId;
    
    /** need To Be Received By Pro */
    private boolean needToBeReceivedByPro;
    
    /** selected Pro Value */
    private Integer selectedProValue;
    
    /** Payment method */
    private Integer paymentMethod;
    
    /** Auto-Certify flag */
    private boolean autoCertify = true;
    
    /** Has document. */
    private Choice hasDocument;
    
    /** Employee Number. */
    private String employeeNo;

    /** List of Allocating Plate Transaction Value Object.*/
    private List allocatingPlatesList;
    
    /** DEG Custom Transaction Reference Number. */
    private String degRefNoCust;
    
    /** SDDI Logged In Traffic Id */
    private Long sddiLoggedInTrafficId;

    /** Is Sell Package Plate */
    private boolean sellPackagePlate;
    
    
    /** Violations. */
    private BusinessRuleVO[] violations;

    
    /** Is Allocating Auction With Sale. */
    private boolean isAllocatingAuctionWithSale;


    /** Has Zero Total Amount */
    private boolean hasZeroTotalAmount;
    
    
    
    /** Document Type */
    private Integer documentType;
    
    /** Document Number */
    private String documentNumber;
    
    /** license Source Code */
    private String licenseSourceCode;
    
    /** Plate Source */
    private String plateSource;
    
    /** plate Category Id */
    private Integer plateCategoryId;
    
    /** plate Code Id */
    private Long plateCodeId;
    
    /** ticket Number */
    private Long ticketNumber;
    
    /** service Provider */
    private Integer serviceProvider;
    
    /** service Type */
    private Integer serviceType;
    
    /** transaction Form Date */
    private Date transactionFormDate;
    
    /** transaction To Date */
    private Date transactionToDate;
    
    /** send To Smart Government */
    private Integer sendToSmartGovernment;
    
    /** send From Smart Government */
    private String sendFromSmartGovernment;
    
    /** sent Ewallet To */
    private Integer sentEwalletTo;
    
    /** sent From Ewallet */
    private Integer sentFromEwallet;
    
    /** Person Name Arabic */
    private String prsNameA;
    
    /** User Profile */
    private UserProfile userProfile;
    
    /** Traffic Id*/
    private Long trafficId;
    
    /** Service code ffu. */
    private Integer serviceCodeFfu;

    /** Make Shipment Info. As Default Flag. */
    private boolean makeDefaultShippmentInfo;
    
    
    /** Selected Row */
    private boolean selectedRow;
    
    /** Donation Added */
    private boolean donationAdded;
    
    
    /** Value read from eWallet request */
    private String eWalletResponseRef;
    
    /** Certified Count */
    private Integer certifiedCount;
    
    
    /*
     * Constructors.
     */
     
    /**
     * Default constructor
     */
    public TransactionVO() {
        // Empty body
    }


    public void setTransactionType(Integer transactionType) {
        this.transactionType = transactionType;
    }

    public Integer getTransactionType() {
        return transactionType;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setServiceCode(Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    public Integer getServiceCode() {
        return serviceCode;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setTrafficFile(TrafficFileVO trafficFile) {
        this.trafficFile = trafficFile;
    }

    public TrafficFileVO getTrafficFile() {
        return trafficFile;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setService(ServiceVO service) {
        this.service = service;
    }

    public ServiceVO getService() {
        return service;
    }

    public void setReceiptsList(List receiptsList) {
        this.receiptsList = receiptsList;
    }

    public List getReceiptsList() {
        return receiptsList;
    }

    public void setEServicesNoteAr(String eServicesNoteAr) {
        this.eServicesNoteAr = eServicesNoteAr;
    }

    public String getEServicesNoteAr() {
        return eServicesNoteAr;
    }

    public void setEServicesNoteEn(String eServicesNoteEn) {
        this.eServicesNoteEn = eServicesNoteEn;
    }

    public String getEServicesNoteEn() {
        return eServicesNoteEn;
    }

    public void setDeliveryModeAr(String deliveryModeAr) {
        this.deliveryModeAr = deliveryModeAr;
    }

    public String getDeliveryModeAr() {
        return deliveryModeAr;
    }

    public void setServiceCodeVhl(Integer serviceCodeVhl) {
        this.serviceCodeVhl = serviceCodeVhl;
    }

    public Integer getServiceCodeVhl() {
        return serviceCodeVhl;
    }

    public void setServiceCodeRvp(Integer serviceCodeRvp) {
        this.serviceCodeRvp = serviceCodeRvp;
    }

    public Integer getServiceCodeRvp() {
        return serviceCodeRvp;
    }

    public void setServiceCodeAct(Integer serviceCodeAct) {
        this.serviceCodeAct = serviceCodeAct;
    }

    public Integer getServiceCodeAct() {
        return serviceCodeAct;
    }

    public void setServiceInfoChanged(boolean serviceInfoChanged) {
        this.serviceInfoChanged = serviceInfoChanged;
    }

    public boolean isServiceInfoChanged() {
        return serviceInfoChanged;
    }

    public void setSentToEGov(Integer sentToEGov) {
        this.sentToEGov = sentToEGov;
    }

    public Integer getSentToEGov() {
        return sentToEGov;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAttachmentRefNo(Long attachmentRefNo) {
        this.attachmentRefNo = attachmentRefNo;
    }

    public Long getAttachmentRefNo() {
        return attachmentRefNo;
    }

    public void setTransactionAttachments(List transactionAttachments) {
        this.transactionAttachments = transactionAttachments;
    }

    public List getTransactionAttachments() {
        return transactionAttachments;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setNumberOfReceipts(Integer numberOfReceipts) {
        this.numberOfReceipts = numberOfReceipts;
    }

    public Integer getNumberOfReceipts() {
        return numberOfReceipts;
    }

    public void setShipPackageNo(Long shipPackageNo) {
        this.shipPackageNo = shipPackageNo;
    }

    public Long getShipPackageNo() {
        return shipPackageNo;
    }

    public void setSequenceInShipPackage(Long sequenceInShipPackage) {
        this.sequenceInShipPackage = sequenceInShipPackage;
    }

    public Long getSequenceInShipPackage() {
        return sequenceInShipPackage;
    }

    public void setTrsStatusDescAr(String trsStatusDescAr) {
        this.trsStatusDescAr = trsStatusDescAr;
    }

    public String getTrsStatusDescAr() {
        return trsStatusDescAr;
    }

    public void setTrsStatusDescEn(String trsStatusDescEn) {
        this.trsStatusDescEn = trsStatusDescEn;
    }

    public String getTrsStatusDescEn() {
        return trsStatusDescEn;
    }

    public void setDegRefNo(Long degRefNo) {
        this.degRefNo = degRefNo;
    }

    public Long getDegRefNo() {
        return degRefNo;
    }

    public void setDegSendDate(Date degSendDate) {
        this.degSendDate = degSendDate;
    }

    public Date getDegSendDate() {
        return degSendDate;
    }

    public void setIsReceiption(Integer isReceiption) {
        this.isReceiption = isReceiption;
    }

    public Integer getIsReceiption() {
        return isReceiption;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    public String getPoBox() {
        return poBox;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public String getOfficePhone() {
        return officePhone;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setAddressEmiCode(String addressEmiCode) {
        this.addressEmiCode = addressEmiCode;
    }

    public String getAddressEmiCode() {
        return addressEmiCode;
    }

    public void setIsSent(Integer isSent) {
        this.isSent = isSent;
    }

    public Integer getIsSent() {
        return isSent;
    }

    public void setDeliveryMode(Integer deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Integer getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryModeDescAr(String deliveryModeDescAr) {
        this.deliveryModeDescAr = deliveryModeDescAr;
    }

    public String getDeliveryModeDescAr() {
        return deliveryModeDescAr;
    }

    public void setDeliveryModeDescEn(String deliveryModeDescEn) {
        this.deliveryModeDescEn = deliveryModeDescEn;
    }

    public String getDeliveryModeDescEn() {
        return deliveryModeDescEn;
    }

    public void setIsEcertificate(Choice isEcertificate) {
        this.isEcertificate = isEcertificate;
    }

    public Choice getIsEcertificate() {
        return isEcertificate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setIsSentDescription(String isSentDescription) {
        this.isSentDescription = isSentDescription;
    }

    public String getIsSentDescription() {
        return isSentDescription;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setBankTranactionId(String bankTranactionId) {
        this.bankTranactionId = bankTranactionId;
    }

    public String getBankTranactionId() {
        return bankTranactionId;
    }

    public void setLanCode(String lanCode) {
        this.lanCode = lanCode;
    }

    public String getLanCode() {
        return lanCode;
    }

    public void setTicketsList(List ticketsList) {
        this.ticketsList = ticketsList;
    }

    public List getTicketsList() {
        return ticketsList;
    }

    public void setServiceCodeCml(Integer serviceCodeCml) {
        this.serviceCodeCml = serviceCodeCml;
    }

    public Integer getServiceCodeCml() {
        return serviceCodeCml;
    }

    public void setEWalletAccountNo(String eWalletAccountNo) {
        this.eWalletAccountNo = eWalletAccountNo;
    }

    public String getEWalletAccountNo() {
        return eWalletAccountNo;
    }

    public void setEWalletSentId(Long eWalletSentId) {
        this.eWalletSentId = eWalletSentId;
    }

    public Long getEWalletSentId() {
        return eWalletSentId;
    }

    public void setEWalletReverseRef(Long eWalletReverseRef) {
        this.eWalletReverseRef = eWalletReverseRef;
    }

    public Long getEWalletReverseRef() {
        return eWalletReverseRef;
    }

    public void setShareEmail(String shareEmail) {
        this.shareEmail = shareEmail;
    }

    public String getShareEmail() {
        return shareEmail;
    }

    public void setIssueDateFrom(Date issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    public Date getIssueDateFrom() {
        return issueDateFrom;
    }

    public void setIssueDateTo(Date issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    public Date getIssueDateTo() {
        return issueDateTo;
    }

    public void setPrintStatus(Integer printStatus) {
        this.printStatus = printStatus;
    }

    public Integer getPrintStatus() {
        return printStatus;
    }

    public void setAllocatingAuctionPlatesList(List allocatingAuctionPlatesList) {
        this.allocatingAuctionPlatesList = allocatingAuctionPlatesList;
    }

    public List getAllocatingAuctionPlatesList() {
        return allocatingAuctionPlatesList;
    }

    public void setStatusArDesc(String statusArDesc) {
        this.statusArDesc = statusArDesc;
    }

    public String getStatusArDesc() {
        return statusArDesc;
    }

    public void setStatusEnDesc(String statusEnDesc) {
        this.statusEnDesc = statusEnDesc;
    }

    public String getStatusEnDesc() {
        return statusEnDesc;
    }

    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public void setEidDataSource(Integer eidDataSource) {
        this.eidDataSource = eidDataSource;
    }

    public Integer getEidDataSource() {
        return eidDataSource;
    }

    public void setEidExpiryDate(Date eidExpiryDate) {
        this.eidExpiryDate = eidExpiryDate;
    }

    public Date getEidExpiryDate() {
        return eidExpiryDate;
    }

    public void setEidNumber(String eidNumber) {
        this.eidNumber = eidNumber;
    }

    public String getEidNumber() {
        return eidNumber;
    }

    public void setDsgSpCode(String dsgSpCode) {
        this.dsgSpCode = dsgSpCode;
    }

    public String getDsgSpCode() {
        return dsgSpCode;
    }

    public void setHasUnprintedBasketCard(Integer hasUnprintedBasketCard) {
        this.hasUnprintedBasketCard = hasUnprintedBasketCard;
    }

    public Integer getHasUnprintedBasketCard() {
        return hasUnprintedBasketCard;
    }

    public void setDsgServiceCode(String dsgServiceCode) {
        this.dsgServiceCode = dsgServiceCode;
    }

    public String getDsgServiceCode() {
        return dsgServiceCode;
    }

    public void setIs3dPayment(Choice is3dPayment) {
        this.is3dPayment = is3dPayment;
    }

    public Choice getIs3dPayment() {
        return is3dPayment;
    }

    public void setAuthorityNocsList(List authorityNocsList) {
        this.authorityNocsList = authorityNocsList;
    }

    public List getAuthorityNocsList() {
        return authorityNocsList;
    }

    public void setTraineesList(List traineesList) {
        this.traineesList = traineesList;
    }

    public List getTraineesList() {
        return traineesList;
    }

    public void setParentServiceCode(Integer parentServiceCode) {
        this.parentServiceCode = parentServiceCode;
    }

    public Integer getParentServiceCode() {
        return parentServiceCode;
    }

    public void setPublicRelationsOfficer(boolean publicRelationsOfficer) {
        this.publicRelationsOfficer = publicRelationsOfficer;
    }

    public boolean isPublicRelationsOfficer() {
        return publicRelationsOfficer;
    }

    public void setPermitCategoryId(Integer permitCategoryId) {
        this.permitCategoryId = permitCategoryId;
    }

    public Integer getPermitCategoryId() {
        return permitCategoryId;
    }

    public void setNeedToBeReceivedByPro(boolean needToBeReceivedByPro) {
        this.needToBeReceivedByPro = needToBeReceivedByPro;
    }

    public boolean isNeedToBeReceivedByPro() {
        return needToBeReceivedByPro;
    }

    public void setSelectedProValue(Integer selectedProValue) {
        this.selectedProValue = selectedProValue;
    }

    public Integer getSelectedProValue() {
        return selectedProValue;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setAutoCertify(boolean autoCertify) {
        this.autoCertify = autoCertify;
    }

    public boolean isAutoCertify() {
        return autoCertify;
    }

    public void setHasDocument(Choice hasDocument) {
        this.hasDocument = hasDocument;
    }

    public Choice getHasDocument() {
        return hasDocument;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setAllocatingPlatesList(List allocatingPlatesList) {
        this.allocatingPlatesList = allocatingPlatesList;
    }

    public List getAllocatingPlatesList() {
        return allocatingPlatesList;
    }

    public void setDegRefNoCust(String degRefNoCust) {
        this.degRefNoCust = degRefNoCust;
    }

    public String getDegRefNoCust() {
        return degRefNoCust;
    }

    public void setSddiLoggedInTrafficId(Long sddiLoggedInTrafficId) {
        this.sddiLoggedInTrafficId = sddiLoggedInTrafficId;
    }

    public Long getSddiLoggedInTrafficId() {
        return sddiLoggedInTrafficId;
    }

    public void setSellPackagePlate(boolean sellPackagePlate) {
        this.sellPackagePlate = sellPackagePlate;
    }

    public boolean isSellPackagePlate() {
        return sellPackagePlate;
    }

    public void setViolations(BusinessRuleVO[] violations) {
        this.violations = violations;
    }

    public BusinessRuleVO[] getViolations() {
        return violations;
    }

    public void setIsAllocatingAuctionWithSale(boolean isAllocatingAuctionWithSale) {
        this.isAllocatingAuctionWithSale = isAllocatingAuctionWithSale;
    }

    public boolean isIsAllocatingAuctionWithSale() {
        return isAllocatingAuctionWithSale;
    }

    public void setHasZeroTotalAmount(boolean hasZeroTotalAmount) {
        this.hasZeroTotalAmount = hasZeroTotalAmount;
    }

    public boolean isHasZeroTotalAmount() {
        return hasZeroTotalAmount;
    }

    public void setDocumentType(Integer documentType) {
        this.documentType = documentType;
    }

    public Integer getDocumentType() {
        return documentType;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setLicenseSourceCode(String licenseSourceCode) {
        this.licenseSourceCode = licenseSourceCode;
    }

    public String getLicenseSourceCode() {
        return licenseSourceCode;
    }

    public void setPlateSource(String plateSource) {
        this.plateSource = plateSource;
    }

    public String getPlateSource() {
        return plateSource;
    }

    public void setPlateCategoryId(Integer plateCategoryId) {
        this.plateCategoryId = plateCategoryId;
    }

    public Integer getPlateCategoryId() {
        return plateCategoryId;
    }

    public void setPlateCodeId(Long plateCodeId) {
        this.plateCodeId = plateCodeId;
    }

    public Long getPlateCodeId() {
        return plateCodeId;
    }

    public void setTicketNumber(Long ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Long getTicketNumber() {
        return ticketNumber;
    }

    public void setServiceProvider(Integer serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public Integer getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setTransactionFormDate(Date transactionFormDate) {
        this.transactionFormDate = transactionFormDate;
    }

    public Date getTransactionFormDate() {
        return transactionFormDate;
    }

    public void setTransactionToDate(Date transactionToDate) {
        this.transactionToDate = transactionToDate;
    }

    public Date getTransactionToDate() {
        return transactionToDate;
    }

    public void setSendToSmartGovernment(Integer sendToSmartGovernment) {
        this.sendToSmartGovernment = sendToSmartGovernment;
    }

    public Integer getSendToSmartGovernment() {
        return sendToSmartGovernment;
    }

    public void setSendFromSmartGovernment(String sendFromSmartGovernment) {
        this.sendFromSmartGovernment = sendFromSmartGovernment;
    }

    public String getSendFromSmartGovernment() {
        return sendFromSmartGovernment;
    }

    public void setSentEwalletTo(Integer sentEwalletTo) {
        this.sentEwalletTo = sentEwalletTo;
    }

    public Integer getSentEwalletTo() {
        return sentEwalletTo;
    }

    public void setSentFromEwallet(Integer sentFromEwallet) {
        this.sentFromEwallet = sentFromEwallet;
    }

    public Integer getSentFromEwallet() {
        return sentFromEwallet;
    }

    public void setPrsNameA(String prsNameA) {
        this.prsNameA = prsNameA;
    }

    public String getPrsNameA() {
        return prsNameA;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setTrafficId(Long trafficId) {
        this.trafficId = trafficId;
    }

    public Long getTrafficId() {
        return trafficId;
    }

    public void setServiceCodeFfu(Integer serviceCodeFfu) {
        this.serviceCodeFfu = serviceCodeFfu;
    }

    public Integer getServiceCodeFfu() {
        return serviceCodeFfu;
    }

    public void setMakeDefaultShippmentInfo(boolean makeDefaultShippmentInfo) {
        this.makeDefaultShippmentInfo = makeDefaultShippmentInfo;
    }

    public boolean isMakeDefaultShippmentInfo() {
        return makeDefaultShippmentInfo;
    }

    public void setSelectedRow(boolean selectedRow) {
        this.selectedRow = selectedRow;
    }

    public boolean isSelectedRow() {
        return selectedRow;
    }

    public void setDonationAdded(boolean donationAdded) {
        this.donationAdded = donationAdded;
    }

    public boolean isDonationAdded() {
        return donationAdded;
    }

    public void setEWalletResponseRef(String eWalletResponseRef) {
        this.eWalletResponseRef = eWalletResponseRef;
    }

    public String getEWalletResponseRef() {
        return eWalletResponseRef;
    }

    public void setCertifiedCount(Integer certifiedCount) {
        this.certifiedCount = certifiedCount;
    }

    public Integer getCertifiedCount() {
        return certifiedCount;
    }
}
